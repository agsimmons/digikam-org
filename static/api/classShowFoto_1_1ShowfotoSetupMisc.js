var classShowFoto_1_1ShowfotoSetupMisc =
[
    [ "MiscTab", "classShowFoto_1_1ShowfotoSetupMisc.html#a52efa5f16842ddd6c7a64a165339fa2f", [
      [ "Behaviour", "classShowFoto_1_1ShowfotoSetupMisc.html#a52efa5f16842ddd6c7a64a165339fa2fae2c9d96dcafcdc80a4b1a67e72bf5368", null ],
      [ "Appearance", "classShowFoto_1_1ShowfotoSetupMisc.html#a52efa5f16842ddd6c7a64a165339fa2fadf1bb1fcc9edec9b2e62f2d268691b9e", null ],
      [ "System", "classShowFoto_1_1ShowfotoSetupMisc.html#a52efa5f16842ddd6c7a64a165339fa2fa76f935080b882514ac89741626171631", null ]
    ] ],
    [ "SortOrder", "classShowFoto_1_1ShowfotoSetupMisc.html#a80ce30b8c90f958e864d3b52506e971c", [
      [ "SortByDate", "classShowFoto_1_1ShowfotoSetupMisc.html#a80ce30b8c90f958e864d3b52506e971ca617cb5863c6c172d75a3f6662a2d6356", null ],
      [ "SortByName", "classShowFoto_1_1ShowfotoSetupMisc.html#a80ce30b8c90f958e864d3b52506e971ca8974bcb1800c38f39efa975a897f7d74", null ],
      [ "SortByFileSize", "classShowFoto_1_1ShowfotoSetupMisc.html#a80ce30b8c90f958e864d3b52506e971caf20e74039f0e982edaec198fd093db1a", null ]
    ] ],
    [ "ShowfotoSetupMisc", "classShowFoto_1_1ShowfotoSetupMisc.html#a6e5e519d5626be9620ab945248fafe88", null ],
    [ "~ShowfotoSetupMisc", "classShowFoto_1_1ShowfotoSetupMisc.html#a68f262c18fa26b0d48ed3efa79bad35a", null ],
    [ "applySettings", "classShowFoto_1_1ShowfotoSetupMisc.html#a05b7137ddce1f815a22cddf162ff0939", null ],
    [ "checkSettings", "classShowFoto_1_1ShowfotoSetupMisc.html#add4246d016649daf933fe0522d107221", null ]
];