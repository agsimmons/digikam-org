var heif__cxx_8h =
[
    [ "Context", "classheif_1_1Context.html", "classheif_1_1Context" ],
    [ "EncodingOptions", "classheif_1_1Context_1_1EncodingOptions.html", "classheif_1_1Context_1_1EncodingOptions" ],
    [ "Reader", "classheif_1_1Context_1_1Reader.html", "classheif_1_1Context_1_1Reader" ],
    [ "ReadingOptions", "classheif_1_1Context_1_1ReadingOptions.html", null ],
    [ "Writer", "classheif_1_1Context_1_1Writer.html", "classheif_1_1Context_1_1Writer" ],
    [ "Encoder", "classheif_1_1Encoder.html", "classheif_1_1Encoder" ],
    [ "EncoderDescriptor", "classheif_1_1EncoderDescriptor.html", "classheif_1_1EncoderDescriptor" ],
    [ "EncoderParameter", "classheif_1_1EncoderParameter.html", "classheif_1_1EncoderParameter" ],
    [ "Error", "classheif_1_1Error.html", "classheif_1_1Error" ],
    [ "Image", "classheif_1_1Image.html", "classheif_1_1Image" ],
    [ "ScalingOptions", "classheif_1_1Image_1_1ScalingOptions.html", null ],
    [ "ImageHandle", "classheif_1_1ImageHandle.html", "classheif_1_1ImageHandle" ],
    [ "DecodingOptions", "classheif_1_1ImageHandle_1_1DecodingOptions.html", null ],
    [ "heif_reader_trampoline_get_position", "heif__cxx_8h.html#a97708047f89fdec400d9b0d396659eef", null ],
    [ "heif_reader_trampoline_read", "heif__cxx_8h.html#a095b38432ddd3ba241106d0da515772b", null ],
    [ "heif_reader_trampoline_seek", "heif__cxx_8h.html#a6a1bc42522b418ae35b070eaa9b9aed2", null ],
    [ "heif_reader_trampoline_wait_for_file_size", "heif__cxx_8h.html#ae79da4fbcd7d385e34f6e535331482d2", null ],
    [ "heif_writer_trampoline_write", "heif__cxx_8h.html#acd37b14c142eb78b30f9049451328369", null ]
];