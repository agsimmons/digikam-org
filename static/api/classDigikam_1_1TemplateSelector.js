var classDigikam_1_1TemplateSelector =
[
    [ "SelectorItems", "classDigikam_1_1TemplateSelector.html#ac3cf81991237ad66e409d6e7fe315255", [
      [ "REMOVETEMPLATE", "classDigikam_1_1TemplateSelector.html#ac3cf81991237ad66e409d6e7fe315255a51732d6d6fee90a870afd520d1891d9e", null ],
      [ "DONTCHANGE", "classDigikam_1_1TemplateSelector.html#ac3cf81991237ad66e409d6e7fe315255a2f93a5f059c9c9cd5f77cef9046e556a", null ]
    ] ],
    [ "TemplateSelector", "classDigikam_1_1TemplateSelector.html#a081c2a2a262e7916d0724ae14cd12788", null ],
    [ "~TemplateSelector", "classDigikam_1_1TemplateSelector.html#a3e99c1117c5b2669e56936206ee0eb19", null ],
    [ "childEvent", "classDigikam_1_1TemplateSelector.html#a579b04e3ec4519b0ebbaafb6256cdf15", null ],
    [ "getTemplate", "classDigikam_1_1TemplateSelector.html#a5116427dbae3c0faf41667ef12206ba8", null ],
    [ "getTemplateIndex", "classDigikam_1_1TemplateSelector.html#a737637fb82f509c9a2c464d4190b44b2", null ],
    [ "minimumSizeHint", "classDigikam_1_1TemplateSelector.html#a25789a423f4bb2a6458949f1135a5b84", null ],
    [ "setContentsMargins", "classDigikam_1_1TemplateSelector.html#ae1da4ee33a0f131ea0f1855813d4fb86", null ],
    [ "setContentsMargins", "classDigikam_1_1TemplateSelector.html#ae5fcb8ec12518ec7a9dff86dcf027e84", null ],
    [ "setSpacing", "classDigikam_1_1TemplateSelector.html#a7c13f7e941510af04789d6097c878ee1", null ],
    [ "setStretchFactor", "classDigikam_1_1TemplateSelector.html#a6a79fbab0ad275840da007964ea8b5a0", null ],
    [ "setTemplate", "classDigikam_1_1TemplateSelector.html#a5e18016842d55d976f5f2c4c32edc8b3", null ],
    [ "setTemplateIndex", "classDigikam_1_1TemplateSelector.html#acfdae855fefc07212a9e92e6e6fe5d03", null ],
    [ "signalTemplateSelected", "classDigikam_1_1TemplateSelector.html#af38b9ebcff3be7194840005b8c6edcf2", null ],
    [ "sizeHint", "classDigikam_1_1TemplateSelector.html#adfd68279bc71f4b8e91011a8ed733f96", null ]
];