var classDigikam_1_1DHistoryView =
[
    [ "EntryType", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34d", [
      [ "StartingEntry", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34da1f26c313e3080ec1a7741b452eae2f00", null ],
      [ "SuccessEntry", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34da67d2077ff16ae4fe85318649451d1fa2", null ],
      [ "WarningEntry", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34da823bc3a60cae67b6a107e6c9432c653d", null ],
      [ "ErrorEntry", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34da6e5774784bf356716a8d09bd25f657a8", null ],
      [ "ProgressEntry", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34da473fc7ab97bdb24409824f753df949b4", null ],
      [ "CancelEntry", "classDigikam_1_1DHistoryView.html#a107c2d70146ce8c6762efb065f5bf34da2d0912c2793e8f4578dc58ec6ad20fa5", null ]
    ] ],
    [ "DHistoryView", "classDigikam_1_1DHistoryView.html#aa8b165b9fc05a6b1d4cb031bff1e96ee", null ],
    [ "~DHistoryView", "classDigikam_1_1DHistoryView.html#a34ca122546be3529d76cdad039c94433", null ],
    [ "addEntry", "classDigikam_1_1DHistoryView.html#ad01009093ad259cee90243290d25687b", null ],
    [ "signalEntryClicked", "classDigikam_1_1DHistoryView.html#aac0a05fa22095611743ead488f78e05c", null ]
];