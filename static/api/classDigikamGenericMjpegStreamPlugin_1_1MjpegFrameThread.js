var classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread =
[
    [ "MjpegFrameThread", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a92c3d04cad720700affa96342af21afb", null ],
    [ "~MjpegFrameThread", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#ae65c5bbf7a58553a5e8426ff6e3a33cd", null ],
    [ "appendJobs", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a354a7c86ac83b4aa46e723149032a471", null ],
    [ "cancel", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a577d6798624415894fc6c22e85fcd3d3", null ],
    [ "createFrameJob", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a4faea0c0a45aec49ded3ba98a50167be", null ],
    [ "isEmpty", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a61f8a4a39061aed1ec1c105a0eddad67", null ],
    [ "maximumNumberOfThreads", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#ae1dc672ceb1c9f127db1d53fe6d40700", null ],
    [ "pendingCount", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#abf47dc040dce8ea4779ecb8c1fb375d0", null ],
    [ "run", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a2373a97a31a8897956bf86b4abebff09", null ],
    [ "setDefaultMaximumNumberOfThreads", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a99e7e530af4825351ba34362385e08fa", null ],
    [ "setMaximumNumberOfThreads", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a3c5aecc58f8d60ea5d2f8d35ef5649c2", null ],
    [ "signalFrameChanged", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a262cbbfc6f7c5cbf6dc305e9a28b1dc8", null ],
    [ "slotJobFinished", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html#a90f7300fc37ec60e08a3101b68da6409", null ]
];