var dir_9e29f7c4e8715e51342123cc5dcc5ef1 =
[
    [ "applicationsettings.cpp", "applicationsettings_8cpp.html", null ],
    [ "applicationsettings.h", "applicationsettings_8h.html", [
      [ "ApplicationSettings", "classDigikam_1_1ApplicationSettings.html", "classDigikam_1_1ApplicationSettings" ]
    ] ],
    [ "applicationsettings_albums.cpp", "applicationsettings__albums_8cpp.html", null ],
    [ "applicationsettings_database.cpp", "applicationsettings__database_8cpp.html", null ],
    [ "applicationsettings_iconview.cpp", "applicationsettings__iconview_8cpp.html", null ],
    [ "applicationsettings_mime.cpp", "applicationsettings__mime_8cpp.html", null ],
    [ "applicationsettings_miscs.cpp", "applicationsettings__miscs_8cpp.html", null ],
    [ "applicationsettings_p.cpp", "applicationsettings__p_8cpp.html", null ],
    [ "applicationsettings_p.h", "applicationsettings__p_8h.html", [
      [ "Private", "classDigikam_1_1ApplicationSettings_1_1Private.html", "classDigikam_1_1ApplicationSettings_1_1Private" ]
    ] ],
    [ "applicationsettings_tooltips.cpp", "applicationsettings__tooltips_8cpp.html", null ]
];