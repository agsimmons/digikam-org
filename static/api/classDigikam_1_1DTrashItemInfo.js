var classDigikam_1_1DTrashItemInfo =
[
    [ "DTrashItemInfo", "classDigikam_1_1DTrashItemInfo.html#ae5b06ac5b85db0b1f99f4b31b44f4341", null ],
    [ "isNull", "classDigikam_1_1DTrashItemInfo.html#a8868b7653b0ca986a497c2071d887fa2", null ],
    [ "operator==", "classDigikam_1_1DTrashItemInfo.html#a67941131b9bc43251a02cfaa3267ed7a", null ],
    [ "collectionPath", "classDigikam_1_1DTrashItemInfo.html#ab52b2dc599f35623036eb57ae060fa77", null ],
    [ "collectionRelativePath", "classDigikam_1_1DTrashItemInfo.html#a04df6783eb77dd3fd2e88c7bc0c777f8", null ],
    [ "deletionTimestamp", "classDigikam_1_1DTrashItemInfo.html#a15b25bee4bbdaff308e84c6bb43657dd", null ],
    [ "imageId", "classDigikam_1_1DTrashItemInfo.html#a1f8f13feadd37a8f806df297eb04dd7f", null ],
    [ "jsonFilePath", "classDigikam_1_1DTrashItemInfo.html#abe09e4459779c8f6c52f5afb84328308", null ],
    [ "trashPath", "classDigikam_1_1DTrashItemInfo.html#acbcb386fc893d29dfec5ff5f9ff5e8f0", null ]
];