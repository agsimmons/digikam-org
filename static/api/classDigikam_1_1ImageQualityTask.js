var classDigikam_1_1ImageQualityTask =
[
    [ "ImageQualityTask", "classDigikam_1_1ImageQualityTask.html#a5ad4b32b12a139f684c3f43b0dbab2b1", null ],
    [ "~ImageQualityTask", "classDigikam_1_1ImageQualityTask.html#ae8caf1b67605d6a23f27a0ba3b0a8ad2", null ],
    [ "cancel", "classDigikam_1_1ImageQualityTask.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "run", "classDigikam_1_1ImageQualityTask.html#a2bdb0ba78fdc0f58205fc195347f8640", null ],
    [ "setMaintenanceData", "classDigikam_1_1ImageQualityTask.html#a24eedd40726b592b7bc94810a3b2663f", null ],
    [ "setQuality", "classDigikam_1_1ImageQualityTask.html#ac8c0224775bdfc55d6ec89383d55865c", null ],
    [ "signalDone", "classDigikam_1_1ImageQualityTask.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalFinished", "classDigikam_1_1ImageQualityTask.html#a1f217f85501be58ba56fec78f88c943b", null ],
    [ "signalProgress", "classDigikam_1_1ImageQualityTask.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikam_1_1ImageQualityTask.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "slotCancel", "classDigikam_1_1ImageQualityTask.html#acd4e8584f74021709cac3494ce0c488a", null ],
    [ "m_cancel", "classDigikam_1_1ImageQualityTask.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];