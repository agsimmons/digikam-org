var hierarchy =
[
    [ "A", null, [
      [ "Digikam::ParallelAdapter< A >", "classDigikam_1_1ParallelAdapter.html", null ]
    ] ],
    [ "acceleration_functions", "structacceleration__functions.html", null ],
    [ "Digikam::AlbumPointer< Digikam::Album >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Digikam::AlbumPointer< Digikam::SAlbum >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Digikam::AlbumPointer< Digikam::TAlbum >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Algo", "classAlgo.html", [
      [ "Algo_CB", "classAlgo__CB.html", [
        [ "Algo_CB_InterPartMode", "classAlgo__CB__InterPartMode.html", [
          [ "Algo_CB_InterPartMode_Fixed", "classAlgo__CB__InterPartMode__Fixed.html", null ]
        ] ],
        [ "Algo_CB_IntraInter", "classAlgo__CB__IntraInter.html", [
          [ "Algo_CB_IntraInter_BruteForce", "classAlgo__CB__IntraInter__BruteForce.html", null ]
        ] ],
        [ "Algo_CB_IntraPartMode", "classAlgo__CB__IntraPartMode.html", [
          [ "Algo_CB_IntraPartMode_BruteForce", "classAlgo__CB__IntraPartMode__BruteForce.html", null ],
          [ "Algo_CB_IntraPartMode_Fixed", "classAlgo__CB__IntraPartMode__Fixed.html", null ]
        ] ],
        [ "Algo_CB_MergeIndex", "classAlgo__CB__MergeIndex.html", [
          [ "Algo_CB_MergeIndex_Fixed", "classAlgo__CB__MergeIndex__Fixed.html", null ]
        ] ],
        [ "Algo_CB_Skip", "classAlgo__CB__Skip.html", [
          [ "Algo_CB_Skip_BruteForce", "classAlgo__CB__Skip__BruteForce.html", null ]
        ] ],
        [ "Algo_CB_Split", "classAlgo__CB__Split.html", [
          [ "Algo_CB_Split_BruteForce", "classAlgo__CB__Split__BruteForce.html", null ]
        ] ]
      ] ],
      [ "Algo_CTB_QScale", "classAlgo__CTB__QScale.html", [
        [ "Algo_CTB_QScale_Constant", "classAlgo__CTB__QScale__Constant.html", null ]
      ] ],
      [ "Algo_PB", "classAlgo__PB.html", [
        [ "Algo_PB_MV", "classAlgo__PB__MV.html", [
          [ "Algo_PB_MV_Search", "classAlgo__PB__MV__Search.html", null ],
          [ "Algo_PB_MV_Test", "classAlgo__PB__MV__Test.html", null ]
        ] ]
      ] ],
      [ "Algo_TB_IntraPredMode", "classAlgo__TB__IntraPredMode.html", [
        [ "Algo_TB_IntraPredMode_ModeSubset", "classAlgo__TB__IntraPredMode__ModeSubset.html", [
          [ "Algo_TB_IntraPredMode_BruteForce", "classAlgo__TB__IntraPredMode__BruteForce.html", null ],
          [ "Algo_TB_IntraPredMode_FastBrute", "classAlgo__TB__IntraPredMode__FastBrute.html", null ],
          [ "Algo_TB_IntraPredMode_MinResidual", "classAlgo__TB__IntraPredMode__MinResidual.html", null ]
        ] ]
      ] ],
      [ "Algo_TB_RateEstimation", "classAlgo__TB__RateEstimation.html", [
        [ "Algo_TB_RateEstimation_Exact", "classAlgo__TB__RateEstimation__Exact.html", null ],
        [ "Algo_TB_RateEstimation_None", "classAlgo__TB__RateEstimation__None.html", null ]
      ] ],
      [ "Algo_TB_Residual", "classAlgo__TB__Residual.html", [
        [ "Algo_TB_Transform", "classAlgo__TB__Transform.html", null ]
      ] ],
      [ "Algo_TB_Split", "classAlgo__TB__Split.html", [
        [ "Algo_TB_Split_BruteForce", "classAlgo__TB__Split__BruteForce.html", null ]
      ] ]
    ] ],
    [ "Algo_CB_InterPartMode_Fixed::params", "structAlgo__CB__InterPartMode__Fixed_1_1params.html", null ],
    [ "Algo_CB_IntraPartMode_Fixed::params", "structAlgo__CB__IntraPartMode__Fixed_1_1params.html", null ],
    [ "Algo_CTB_QScale_Constant::params", "structAlgo__CTB__QScale__Constant_1_1params.html", null ],
    [ "Algo_PB_MV_Search::params", "structAlgo__PB__MV__Search_1_1params.html", null ],
    [ "Algo_PB_MV_Test::params", "structAlgo__PB__MV__Test_1_1params.html", null ],
    [ "Algo_TB_IntraPredMode_FastBrute::params", "structAlgo__TB__IntraPredMode__FastBrute_1_1params.html", null ],
    [ "Algo_TB_IntraPredMode_MinResidual::params", "structAlgo__TB__IntraPredMode__MinResidual_1_1params.html", null ],
    [ "Algo_TB_Split_BruteForce::params", "structAlgo__TB__Split__BruteForce_1_1params.html", null ],
    [ "alloc_pool", "classalloc__pool.html", null ],
    [ "bitreader", "structbitreader.html", null ],
    [ "boost::default_bfs_visitor", null, [
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::BreadthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1BreadthFirstSearchVisitor.html", null ]
    ] ],
    [ "boost::default_dfs_visitor", null, [
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::DepthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1DepthFirstSearchVisitor.html", null ]
    ] ],
    [ "CABAC_decoder", "structCABAC__decoder.html", null ],
    [ "CABAC_encoder", "classCABAC__encoder.html", [
      [ "CABAC_encoder_bitstream", "classCABAC__encoder__bitstream.html", null ],
      [ "CABAC_encoder_estim", "classCABAC__encoder__estim.html", [
        [ "CABAC_encoder_estim_constant", "classCABAC__encoder__estim__constant.html", null ]
      ] ]
    ] ],
    [ "CB_ref_info", "structCB__ref__info.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::AlbumChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::AlbumRootChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::CollectionImageChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::ImageChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::ImageTagChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::SearchChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< Digikam::TagChangeset >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "CodingOption< node >", "classCodingOption.html", null ],
    [ "CodingOptions< node >", "classCodingOptions.html", null ],
    [ "config_parameters", "classconfig__parameters.html", null ],
    [ "context_model", "structcontext__model.html", null ],
    [ "context_model_table", "classcontext__model__table.html", null ],
    [ "CTB_info", "structCTB__info.html", null ],
    [ "CTBTreeMatrix", "classCTBTreeMatrix.html", null ],
    [ "cv::ParallelLoopBody", null, [
      [ "Digikam::OpenCVDNNFaceRecognizer::Private::ParallelRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelRecognizer.html", null ],
      [ "Digikam::OpenCVDNNFaceRecognizer::Private::ParallelTrainer", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelTrainer.html", null ]
    ] ],
    [ "DigikamGenericHtmlGalleryPlugin::CWrapper< xmlTextWriterPtr, xmlFreeTextWriter >", "classDigikamGenericHtmlGalleryPlugin_1_1CWrapper.html", null ],
    [ "de265_image", "structde265__image.html", null ],
    [ "de265_image_allocation", "structde265__image__allocation.html", null ],
    [ "de265_image_spec", "structde265__image__spec.html", null ],
    [ "de265_progress_lock", "classde265__progress__lock.html", null ],
    [ "decoded_picture_buffer", "classdecoded__picture__buffer.html", null ],
    [ "Digikam::AbstractAlbumTreeView::ContextMenuElement", "classDigikam_1_1AbstractAlbumTreeView_1_1ContextMenuElement.html", null ],
    [ "Digikam::AbstractAlbumTreeView::Private", "classDigikam_1_1AbstractAlbumTreeView_1_1Private.html", null ],
    [ "Digikam::AbstractMarkerTiler::ClickInfo", "classDigikam_1_1AbstractMarkerTiler_1_1ClickInfo.html", null ],
    [ "Digikam::AbstractMarkerTiler::NonEmptyIterator", "classDigikam_1_1AbstractMarkerTiler_1_1NonEmptyIterator.html", null ],
    [ "Digikam::AbstractMarkerTiler::Tile", "classDigikam_1_1AbstractMarkerTiler_1_1Tile.html", null ],
    [ "Digikam::ActionData", "classDigikam_1_1ActionData.html", null ],
    [ "Digikam::Album", "classDigikam_1_1Album.html", [
      [ "Digikam::DAlbum", "classDigikam_1_1DAlbum.html", null ],
      [ "Digikam::PAlbum", "classDigikam_1_1PAlbum.html", null ],
      [ "Digikam::SAlbum", "classDigikam_1_1SAlbum.html", null ],
      [ "Digikam::TAlbum", "classDigikam_1_1TAlbum.html", null ]
    ] ],
    [ "Digikam::AlbumChangeset", "classDigikam_1_1AlbumChangeset.html", null ],
    [ "Digikam::AlbumCopyMoveHint", "classDigikam_1_1AlbumCopyMoveHint.html", null ],
    [ "Digikam::AlbumInfo", "classDigikam_1_1AlbumInfo.html", null ],
    [ "Digikam::AlbumIterator", "classDigikam_1_1AlbumIterator.html", null ],
    [ "Digikam::AlbumManager::Private", "classDigikam_1_1AlbumManager_1_1Private.html", null ],
    [ "Digikam::AlbumManagerCreator", "classDigikam_1_1AlbumManagerCreator.html", null ],
    [ "Digikam::AlbumPointer< T >", "classDigikam_1_1AlbumPointer.html", null ],
    [ "Digikam::AlbumRootChangeset", "classDigikam_1_1AlbumRootChangeset.html", null ],
    [ "Digikam::AlbumRootInfo", "classDigikam_1_1AlbumRootInfo.html", null ],
    [ "Digikam::AlbumShortInfo", "classDigikam_1_1AlbumShortInfo.html", null ],
    [ "Digikam::AlbumSimplified", "classDigikam_1_1AlbumSimplified.html", null ],
    [ "Digikam::AntiVignettingContainer", "classDigikam_1_1AntiVignettingContainer.html", null ],
    [ "Digikam::ApplicationSettings::Private", "classDigikam_1_1ApplicationSettings_1_1Private.html", null ],
    [ "Digikam::AssignedBatchTools", "classDigikam_1_1AssignedBatchTools.html", null ],
    [ "Digikam::AssignNameWidget::Private", "classDigikam_1_1AssignNameWidget_1_1Private.html", null ],
    [ "Digikam::BalooInfo", "classDigikam_1_1BalooInfo.html", null ],
    [ "Digikam::BatchToolSet", "classDigikam_1_1BatchToolSet.html", null ],
    [ "Digikam::BCGContainer", "classDigikam_1_1BCGContainer.html", null ],
    [ "Digikam::BdEngineBackend::QueryState", "classDigikam_1_1BdEngineBackend_1_1QueryState.html", null ],
    [ "Digikam::BdEngineBackendPrivate::AbstractUnlocker", "classDigikam_1_1BdEngineBackendPrivate_1_1AbstractUnlocker.html", [
      [ "Digikam::BdEngineBackendPrivate::AbstractWaitingUnlocker", "classDigikam_1_1BdEngineBackendPrivate_1_1AbstractWaitingUnlocker.html", [
        [ "Digikam::BdEngineBackendPrivate::BusyWaiter", "classDigikam_1_1BdEngineBackendPrivate_1_1BusyWaiter.html", null ],
        [ "Digikam::BdEngineBackendPrivate::ErrorLocker", "classDigikam_1_1BdEngineBackendPrivate_1_1ErrorLocker.html", null ]
      ] ]
    ] ],
    [ "Digikam::BorderContainer", "classDigikam_1_1BorderContainer.html", null ],
    [ "Digikam::BWSepiaContainer", "classDigikam_1_1BWSepiaContainer.html", null ],
    [ "Digikam::CachedPixmapKey", "classDigikam_1_1CachedPixmapKey.html", null ],
    [ "Digikam::CachedPixmaps", "classDigikam_1_1CachedPixmaps.html", null ],
    [ "Digikam::CameraMessageBox", "classDigikam_1_1CameraMessageBox.html", null ],
    [ "Digikam::CameraNameHelper", "classDigikam_1_1CameraNameHelper.html", null ],
    [ "Digikam::CameraType", "classDigikam_1_1CameraType.html", null ],
    [ "Digikam::CamItemInfo", "classDigikam_1_1CamItemInfo.html", null ],
    [ "Digikam::CamItemSortSettings", "classDigikam_1_1CamItemSortSettings.html", null ],
    [ "Digikam::CaptionValues", "classDigikam_1_1CaptionValues.html", null ],
    [ "Digikam::CBContainer", "classDigikam_1_1CBContainer.html", null ],
    [ "Digikam::ChangingDB", "classDigikam_1_1ChangingDB.html", null ],
    [ "Digikam::ChoiceSearchModel::Entry", "classDigikam_1_1ChoiceSearchModel_1_1Entry.html", null ],
    [ "Digikam::CMat", "structDigikam_1_1CMat.html", null ],
    [ "Digikam::CollectionImageChangeset", "classDigikam_1_1CollectionImageChangeset.html", null ],
    [ "Digikam::CollectionLocation", "classDigikam_1_1CollectionLocation.html", [
      [ "Digikam::AlbumRootLocation", "classDigikam_1_1AlbumRootLocation.html", null ]
    ] ],
    [ "Digikam::CollectionManager::Private", "classDigikam_1_1CollectionManager_1_1Private.html", null ],
    [ "Digikam::CollectionScanner::Private", "classDigikam_1_1CollectionScanner_1_1Private.html", null ],
    [ "Digikam::CollectionScannerHintContainer", "classDigikam_1_1CollectionScannerHintContainer.html", [
      [ "Digikam::CollectionScannerHintContainerImplementation", "classDigikam_1_1CollectionScannerHintContainerImplementation.html", null ]
    ] ],
    [ "Digikam::CollectionScannerHints::Album", "classDigikam_1_1CollectionScannerHints_1_1Album.html", null ],
    [ "Digikam::CollectionScannerHints::DstPath", "classDigikam_1_1CollectionScannerHints_1_1DstPath.html", null ],
    [ "Digikam::CollectionScannerHints::Item", "classDigikam_1_1CollectionScannerHints_1_1Item.html", null ],
    [ "Digikam::CollectionScannerObserver", "classDigikam_1_1CollectionScannerObserver.html", [
      [ "Digikam::InitializationObserver", "classDigikam_1_1InitializationObserver.html", [
        [ "Digikam::ScanController", "classDigikam_1_1ScanController.html", null ]
      ] ],
      [ "Digikam::SimpleCollectionScannerObserver", "classDigikam_1_1SimpleCollectionScannerObserver.html", null ]
    ] ],
    [ "Digikam::ColorFXContainer", "classDigikam_1_1ColorFXContainer.html", null ],
    [ "Digikam::CommentInfo", "classDigikam_1_1CommentInfo.html", null ],
    [ "Digikam::ContentAwareContainer", "classDigikam_1_1ContentAwareContainer.html", null ],
    [ "Digikam::ContextMenuHelper::Private", "classDigikam_1_1ContextMenuHelper_1_1Private.html", null ],
    [ "Digikam::CopyrightInfo", "classDigikam_1_1CopyrightInfo.html", null ],
    [ "Digikam::CoreDB", "classDigikam_1_1CoreDB.html", null ],
    [ "Digikam::CoreDbAccess", "classDigikam_1_1CoreDbAccess.html", null ],
    [ "Digikam::CoreDbAccessUnlock", "classDigikam_1_1CoreDbAccessUnlock.html", null ],
    [ "Digikam::CoreDbBackendPrivate::ChangesetContainer< T >", "classDigikam_1_1CoreDbBackendPrivate_1_1ChangesetContainer.html", null ],
    [ "Digikam::CoreDbDownloadHistory", "classDigikam_1_1CoreDbDownloadHistory.html", null ],
    [ "Digikam::CoreDbNameFilter", "classDigikam_1_1CoreDbNameFilter.html", null ],
    [ "Digikam::CoreDbOperationGroup", "classDigikam_1_1CoreDbOperationGroup.html", null ],
    [ "Digikam::CoreDbPrivilegesChecker", "classDigikam_1_1CoreDbPrivilegesChecker.html", null ],
    [ "Digikam::CoreDbSchemaUpdater", "classDigikam_1_1CoreDbSchemaUpdater.html", null ],
    [ "Digikam::CoreDbTransaction", "classDigikam_1_1CoreDbTransaction.html", null ],
    [ "Digikam::CurvesContainer", "classDigikam_1_1CurvesContainer.html", null ],
    [ "Digikam::DAlbumInfo", "classDigikam_1_1DAlbumInfo.html", null ],
    [ "Digikam::DatabaseBlob", "classDigikam_1_1DatabaseBlob.html", null ],
    [ "Digikam::DatabaseFields::DatabaseFieldsEnumIterator< FieldName >", "classDigikam_1_1DatabaseFields_1_1DatabaseFieldsEnumIterator.html", null ],
    [ "Digikam::DatabaseFields::DatabaseFieldsEnumIteratorSetOnly< FieldName >", "classDigikam_1_1DatabaseFields_1_1DatabaseFieldsEnumIteratorSetOnly.html", null ],
    [ "Digikam::DatabaseFields::FieldMetaInfo< FieldName >", "classDigikam_1_1DatabaseFields_1_1FieldMetaInfo.html", null ],
    [ "Digikam::DatabaseFields::Set", "classDigikam_1_1DatabaseFields_1_1Set.html", null ],
    [ "Digikam::DatabaseServerError", "classDigikam_1_1DatabaseServerError.html", null ],
    [ "Digikam::DatabaseSettingsWidget::Private", "classDigikam_1_1DatabaseSettingsWidget_1_1Private.html", null ],
    [ "Digikam::DateFormat", "classDigikam_1_1DateFormat.html", null ],
    [ "Digikam::DbEngineAccess", "classDigikam_1_1DbEngineAccess.html", null ],
    [ "Digikam::DbEngineAction", "classDigikam_1_1DbEngineAction.html", null ],
    [ "Digikam::DbEngineActionElement", "classDigikam_1_1DbEngineActionElement.html", null ],
    [ "Digikam::DbEngineActionType", "classDigikam_1_1DbEngineActionType.html", null ],
    [ "Digikam::DbEngineConfig", "classDigikam_1_1DbEngineConfig.html", null ],
    [ "Digikam::DbEngineConfigSettings", "classDigikam_1_1DbEngineConfigSettings.html", null ],
    [ "Digikam::DbEngineConfigSettingsLoader", "classDigikam_1_1DbEngineConfigSettingsLoader.html", null ],
    [ "Digikam::DbEngineErrorAnswer", "classDigikam_1_1DbEngineErrorAnswer.html", [
      [ "Digikam::BdEngineBackendPrivate", "classDigikam_1_1BdEngineBackendPrivate.html", [
        [ "Digikam::CoreDbBackendPrivate", "classDigikam_1_1CoreDbBackendPrivate.html", null ]
      ] ]
    ] ],
    [ "Digikam::DbEngineLocking", "classDigikam_1_1DbEngineLocking.html", null ],
    [ "Digikam::DbEngineParameters", "classDigikam_1_1DbEngineParameters.html", null ],
    [ "Digikam::DbEngineThreadData", "classDigikam_1_1DbEngineThreadData.html", null ],
    [ "Digikam::DBJobInfo", "classDigikam_1_1DBJobInfo.html", [
      [ "Digikam::AlbumsDBJobInfo", "classDigikam_1_1AlbumsDBJobInfo.html", null ],
      [ "Digikam::DatesDBJobInfo", "classDigikam_1_1DatesDBJobInfo.html", null ],
      [ "Digikam::GPSDBJobInfo", "classDigikam_1_1GPSDBJobInfo.html", null ],
      [ "Digikam::SearchesDBJobInfo", "classDigikam_1_1SearchesDBJobInfo.html", null ],
      [ "Digikam::TagsDBJobInfo", "classDigikam_1_1TagsDBJobInfo.html", null ]
    ] ],
    [ "Digikam::DbKeysCollection", "classDigikam_1_1DbKeysCollection.html", [
      [ "Digikam::CommonKeys", "classDigikam_1_1CommonKeys.html", null ],
      [ "Digikam::MetadataKeys", "classDigikam_1_1MetadataKeys.html", null ],
      [ "Digikam::PositionKeys", "classDigikam_1_1PositionKeys.html", null ]
    ] ],
    [ "Digikam::DCategorizedSortFilterProxyModel::Private", "classDigikam_1_1DCategorizedSortFilterProxyModel_1_1Private.html", null ],
    [ "Digikam::DCategorizedView::Private", "classDigikam_1_1DCategorizedView_1_1Private.html", null ],
    [ "Digikam::DCategorizedView::Private::ElementInfo", "classDigikam_1_1DCategorizedView_1_1Private_1_1ElementInfo.html", null ],
    [ "Digikam::DColor", "classDigikam_1_1DColor.html", null ],
    [ "Digikam::DColorComposer", "classDigikam_1_1DColorComposer.html", null ],
    [ "Digikam::DConfigDlgModelPrivate", "classDigikam_1_1DConfigDlgModelPrivate.html", [
      [ "Digikam::DConfigDlgWdgModelPrivate", "classDigikam_1_1DConfigDlgWdgModelPrivate.html", null ]
    ] ],
    [ "Digikam::DConfigDlgTitle::Private", "classDigikam_1_1DConfigDlgTitle_1_1Private.html", null ],
    [ "Digikam::DConfigDlgViewPrivate", "classDigikam_1_1DConfigDlgViewPrivate.html", [
      [ "Digikam::DConfigDlgWdgPrivate", "classDigikam_1_1DConfigDlgWdgPrivate.html", null ]
    ] ],
    [ "Digikam::DDatePicker::Private", "classDigikam_1_1DDatePicker_1_1Private.html", null ],
    [ "Digikam::DDateTable::Private::DatePaintingMode", "classDigikam_1_1DDateTable_1_1Private_1_1DatePaintingMode.html", null ],
    [ "Digikam::DeltaTime", "classDigikam_1_1DeltaTime.html", null ],
    [ "Digikam::DFileOperations", "classDigikam_1_1DFileOperations.html", null ],
    [ "Digikam::DigikamApp::Private", "classDigikam_1_1DigikamApp_1_1Private.html", null ],
    [ "Digikam::DImageHistory", "classDigikam_1_1DImageHistory.html", null ],
    [ "Digikam::DImageHistory::Entry", "classDigikam_1_1DImageHistory_1_1Entry.html", null ],
    [ "Digikam::DImgBuiltinFilter", "classDigikam_1_1DImgBuiltinFilter.html", null ],
    [ "Digikam::DImgFilterGenerator", "classDigikam_1_1DImgFilterGenerator.html", [
      [ "Digikam::BasicDImgFilterGenerator< T >", "classDigikam_1_1BasicDImgFilterGenerator.html", null ],
      [ "Digikam::DImgFilterManager", "classDigikam_1_1DImgFilterManager.html", null ]
    ] ],
    [ "Digikam::DImgLoader", "classDigikam_1_1DImgLoader.html", [
      [ "Digikam::DImgHEIFLoader", "classDigikam_1_1DImgHEIFLoader.html", null ],
      [ "Digikam::DImgPGFLoader", "classDigikam_1_1DImgPGFLoader.html", null ],
      [ "DigikamImageMagickDImgPlugin::DImgImageMagickLoader", "classDigikamImageMagickDImgPlugin_1_1DImgImageMagickLoader.html", null ],
      [ "DigikamJPEG2000DImgPlugin::DImgJPEG2000Loader", "classDigikamJPEG2000DImgPlugin_1_1DImgJPEG2000Loader.html", null ],
      [ "DigikamJPEGDImgPlugin::DImgJPEGLoader", "classDigikamJPEGDImgPlugin_1_1DImgJPEGLoader.html", null ],
      [ "DigikamPNGDImgPlugin::DImgPNGLoader", "classDigikamPNGDImgPlugin_1_1DImgPNGLoader.html", null ],
      [ "DigikamQImageDImgPlugin::DImgQImageLoader", "classDigikamQImageDImgPlugin_1_1DImgQImageLoader.html", null ],
      [ "DigikamRAWDImgPlugin::DImgRAWLoader", "classDigikamRAWDImgPlugin_1_1DImgRAWLoader.html", null ],
      [ "DigikamTIFFDImgPlugin::DImgTIFFLoader", "classDigikamTIFFDImgPlugin_1_1DImgTIFFLoader.html", null ]
    ] ],
    [ "Digikam::DImgLoaderObserver", "classDigikam_1_1DImgLoaderObserver.html", [
      [ "Digikam::IccTransformFilter", "classDigikam_1_1IccTransformFilter.html", null ],
      [ "Digikam::LoadingTask", "classDigikam_1_1LoadingTask.html", [
        [ "Digikam::SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", [
          [ "Digikam::PreviewLoadingTask", "classDigikam_1_1PreviewLoadingTask.html", null ],
          [ "Digikam::ThumbnailLoadingTask", "classDigikam_1_1ThumbnailLoadingTask.html", null ]
        ] ]
      ] ],
      [ "Digikam::SavingTask", "classDigikam_1_1SavingTask.html", null ]
    ] ],
    [ "Digikam::DImgStaticPriv", "classDigikam_1_1DImgStaticPriv.html", null ],
    [ "Digikam::DisjointMetadataDataFields", "classDigikam_1_1DisjointMetadataDataFields.html", [
      [ "Digikam::DisjointMetadata::Private", "classDigikam_1_1DisjointMetadata_1_1Private.html", null ]
    ] ],
    [ "Digikam::DItemInfo", "classDigikam_1_1DItemInfo.html", null ],
    [ "Digikam::DMessageBox", "classDigikam_1_1DMessageBox.html", null ],
    [ "Digikam::DMetadataSettingsContainer", "classDigikam_1_1DMetadataSettingsContainer.html", null ],
    [ "Digikam::DMultiTabBar::Private", "classDigikam_1_1DMultiTabBar_1_1Private.html", null ],
    [ "Digikam::DMultiTabBarFrame::Private", "classDigikam_1_1DMultiTabBarFrame_1_1Private.html", null ],
    [ "Digikam::DMultiTabBarTab::Private", "classDigikam_1_1DMultiTabBarTab_1_1Private.html", null ],
    [ "Digikam::DNGWriter", "classDigikam_1_1DNGWriter.html", null ],
    [ "Digikam::DNGWriter::Private", "classDigikam_1_1DNGWriter_1_1Private.html", null ],
    [ "Digikam::DNNFaceDetectorBase", "classDigikam_1_1DNNFaceDetectorBase.html", [
      [ "Digikam::DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html", null ],
      [ "Digikam::DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html", null ]
    ] ],
    [ "Digikam::DNNFaceExtractor", "classDigikam_1_1DNNFaceExtractor.html", null ],
    [ "Digikam::DownloadInfo", "classDigikam_1_1DownloadInfo.html", null ],
    [ "Digikam::DownloadSettings", "classDigikam_1_1DownloadSettings.html", null ],
    [ "Digikam::DPixelsAliasFilter", "classDigikam_1_1DPixelsAliasFilter.html", null ],
    [ "Digikam::DPluginAuthor", "classDigikam_1_1DPluginAuthor.html", null ],
    [ "Digikam::DPluginLoader::Private", "classDigikam_1_1DPluginLoader_1_1Private.html", null ],
    [ "Digikam::DragDropModelImplementation", "classDigikam_1_1DragDropModelImplementation.html", [
      [ "Digikam::ImportItemModel", "classDigikam_1_1ImportItemModel.html", [
        [ "Digikam::ImportThumbnailModel", "classDigikam_1_1ImportThumbnailModel.html", null ]
      ] ],
      [ "Digikam::ItemHistoryGraphModel", "classDigikam_1_1ItemHistoryGraphModel.html", null ],
      [ "Digikam::ItemModel", "classDigikam_1_1ItemModel.html", [
        [ "Digikam::ItemThumbnailModel", "classDigikam_1_1ItemThumbnailModel.html", [
          [ "Digikam::ItemAlbumModel", "classDigikam_1_1ItemAlbumModel.html", null ],
          [ "Digikam::ItemListModel", "classDigikam_1_1ItemListModel.html", null ]
        ] ]
      ] ],
      [ "ShowFoto::ShowfotoItemModel", "classShowFoto_1_1ShowfotoItemModel.html", [
        [ "ShowFoto::ShowfotoThumbnailModel", "classShowFoto_1_1ShowfotoThumbnailModel.html", null ]
      ] ]
    ] ],
    [ "Digikam::DragDropViewImplementation", "classDigikam_1_1DragDropViewImplementation.html", [
      [ "Digikam::ItemViewCategorized", "classDigikam_1_1ItemViewCategorized.html", [
        [ "Digikam::ImportCategorizedView", "classDigikam_1_1ImportCategorizedView.html", [
          [ "Digikam::ImportIconView", "classDigikam_1_1ImportIconView.html", null ],
          [ "Digikam::ImportThumbnailBar", "classDigikam_1_1ImportThumbnailBar.html", null ]
        ] ],
        [ "Digikam::ItemCategorizedView", "classDigikam_1_1ItemCategorizedView.html", [
          [ "Digikam::DigikamItemView", "classDigikam_1_1DigikamItemView.html", null ],
          [ "Digikam::ItemThumbnailBar", "classDigikam_1_1ItemThumbnailBar.html", [
            [ "Digikam::LightTableThumbBar", "classDigikam_1_1LightTableThumbBar.html", null ]
          ] ]
        ] ],
        [ "ShowFoto::ShowfotoCategorizedView", "classShowFoto_1_1ShowfotoCategorizedView.html", [
          [ "ShowFoto::ShowfotoThumbnailBar", "classShowFoto_1_1ShowfotoThumbnailBar.html", null ]
        ] ]
      ] ],
      [ "Digikam::TableViewTreeView", "classDigikam_1_1TableViewTreeView.html", null ],
      [ "Digikam::VersionsTreeView", "classDigikam_1_1VersionsTreeView.html", null ]
    ] ],
    [ "Digikam::DRawDecoder::Private", "classDigikam_1_1DRawDecoder_1_1Private.html", null ],
    [ "Digikam::DRawDecoderSettings", "classDigikam_1_1DRawDecoderSettings.html", null ],
    [ "Digikam::DRawDecoding", "classDigikam_1_1DRawDecoding.html", null ],
    [ "Digikam::DRawInfo", "classDigikam_1_1DRawInfo.html", null ],
    [ "Digikam::DServiceMenu", "classDigikam_1_1DServiceMenu.html", null ],
    [ "Digikam::DToolTipStyleSheet", "classDigikam_1_1DToolTipStyleSheet.html", null ],
    [ "Digikam::DTrash", "classDigikam_1_1DTrash.html", null ],
    [ "Digikam::DTrashItemInfo", "classDigikam_1_1DTrashItemInfo.html", null ],
    [ "Digikam::DWItemDelegatePool", "classDigikam_1_1DWItemDelegatePool.html", null ],
    [ "Digikam::DWItemDelegatePoolPrivate", "classDigikam_1_1DWItemDelegatePoolPrivate.html", null ],
    [ "Digikam::DXmlGuiWindow::Private", "classDigikam_1_1DXmlGuiWindow_1_1Private.html", null ],
    [ "Digikam::EditorCore::Private", "classDigikam_1_1EditorCore_1_1Private.html", null ],
    [ "Digikam::EditorCore::Private::FileToSave", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html", null ],
    [ "Digikam::EditorWindow::Private", "classDigikam_1_1EditorWindow_1_1Private.html", null ],
    [ "Digikam::EffectMngr", "classDigikam_1_1EffectMngr.html", null ],
    [ "Digikam::EffectMngr::Private", "classDigikam_1_1EffectMngr_1_1Private.html", null ],
    [ "Digikam::Ellipsoid", "classDigikam_1_1Ellipsoid.html", null ],
    [ "Digikam::ExifToolParser::Private", "classDigikam_1_1ExifToolParser_1_1Private.html", null ],
    [ "Digikam::ExifToolProcess::Private", "classDigikam_1_1ExifToolProcess_1_1Private.html", null ],
    [ "Digikam::ExifToolProcess::Private::Command", "classDigikam_1_1ExifToolProcess_1_1Private_1_1Command.html", null ],
    [ "Digikam::ExposureSettingsContainer", "classDigikam_1_1ExposureSettingsContainer.html", null ],
    [ "Digikam::FaceDb", "classDigikam_1_1FaceDb.html", null ],
    [ "Digikam::FaceDb::Private", "classDigikam_1_1FaceDb_1_1Private.html", null ],
    [ "Digikam::FaceDbAccess", "classDigikam_1_1FaceDbAccess.html", null ],
    [ "Digikam::FaceDbAccessUnlock", "classDigikam_1_1FaceDbAccessUnlock.html", null ],
    [ "Digikam::FaceDbOperationGroup", "classDigikam_1_1FaceDbOperationGroup.html", null ],
    [ "Digikam::FaceDbSchemaUpdater", "classDigikam_1_1FaceDbSchemaUpdater.html", null ],
    [ "Digikam::FaceDetector", "classDigikam_1_1FaceDetector.html", null ],
    [ "Digikam::FaceGroup::Private", "classDigikam_1_1FaceGroup_1_1Private.html", null ],
    [ "Digikam::FaceItemRetriever", "classDigikam_1_1FaceItemRetriever.html", null ],
    [ "Digikam::FacePipelinePackage", "classDigikam_1_1FacePipelinePackage.html", [
      [ "Digikam::FacePipelineExtendedPackage", "classDigikam_1_1FacePipelineExtendedPackage.html", null ]
    ] ],
    [ "Digikam::FacePreprocessor", "classDigikam_1_1FacePreprocessor.html", [
      [ "Digikam::RecognitionPreprocessor", "classDigikam_1_1RecognitionPreprocessor.html", null ]
    ] ],
    [ "Digikam::FaceScanSettings", "classDigikam_1_1FaceScanSettings.html", null ],
    [ "Digikam::FaceScanWidget::Private", "classDigikam_1_1FaceScanWidget_1_1Private.html", null ],
    [ "Digikam::FaceTags", "classDigikam_1_1FaceTags.html", null ],
    [ "Digikam::FaceTagsEditor", "classDigikam_1_1FaceTagsEditor.html", [
      [ "Digikam::FaceUtils", "classDigikam_1_1FaceUtils.html", null ]
    ] ],
    [ "Digikam::FaceTagsIface", "classDigikam_1_1FaceTagsIface.html", [
      [ "Digikam::FacePipelineFaceTagsIface", "classDigikam_1_1FacePipelineFaceTagsIface.html", null ]
    ] ],
    [ "Digikam::FacialRecognitionWrapper", "classDigikam_1_1FacialRecognitionWrapper.html", null ],
    [ "Digikam::FacialRecognitionWrapper::Private", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html", null ],
    [ "Digikam::FieldQueryBuilder", "classDigikam_1_1FieldQueryBuilder.html", null ],
    [ "Digikam::FileActionProgressItemCreator", "classDigikam_1_1FileActionProgressItemCreator.html", [
      [ "Digikam::PrivateProgressItemCreator", "classDigikam_1_1PrivateProgressItemCreator.html", null ]
    ] ],
    [ "Digikam::FileReadLocker", "classDigikam_1_1FileReadLocker.html", null ],
    [ "Digikam::FileReadWriteLockKey", "classDigikam_1_1FileReadWriteLockKey.html", null ],
    [ "Digikam::FileWriteLocker", "classDigikam_1_1FileWriteLocker.html", null ],
    [ "Digikam::FilmContainer", "classDigikam_1_1FilmContainer.html", null ],
    [ "Digikam::FilmContainer::Private", "classDigikam_1_1FilmContainer_1_1Private.html", null ],
    [ "Digikam::FilmFilter::Private", "classDigikam_1_1FilmFilter_1_1Private.html", null ],
    [ "Digikam::FilmGrainContainer", "classDigikam_1_1FilmGrainContainer.html", null ],
    [ "Digikam::FilmProfile", "classDigikam_1_1FilmProfile.html", null ],
    [ "Digikam::Filter", "classDigikam_1_1Filter.html", null ],
    [ "Digikam::FilterAction", "classDigikam_1_1FilterAction.html", [
      [ "Digikam::DImgThreadedFilter::DefaultFilterAction< Filter >", "classDigikam_1_1DImgThreadedFilter_1_1DefaultFilterAction.html", null ]
    ] ],
    [ "Digikam::FocusPoint", "classDigikam_1_1FocusPoint.html", null ],
    [ "Digikam::FocusPointGroup::Private", "classDigikam_1_1FocusPointGroup_1_1Private.html", null ],
    [ "Digikam::FrameUtils", "classDigikam_1_1FrameUtils.html", null ],
    [ "Digikam::FreeRotationContainer", "classDigikam_1_1FreeRotationContainer.html", null ],
    [ "Digikam::FullObjectDetection", "classDigikam_1_1FullObjectDetection.html", null ],
    [ "Digikam::FuzzySearchView::Private", "classDigikam_1_1FuzzySearchView_1_1Private.html", null ],
    [ "Digikam::GeoCoordinates", "classDigikam_1_1GeoCoordinates.html", null ],
    [ "Digikam::GeodeticCalculator", "classDigikam_1_1GeodeticCalculator.html", null ],
    [ "Digikam::GeoIfaceCluster", "classDigikam_1_1GeoIfaceCluster.html", null ],
    [ "Digikam::GeoIfaceInternalWidgetInfo", "classDigikam_1_1GeoIfaceInternalWidgetInfo.html", null ],
    [ "Digikam::GPSDataContainer", "classDigikam_1_1GPSDataContainer.html", null ],
    [ "Digikam::GPSItemContainer", "classDigikam_1_1GPSItemContainer.html", [
      [ "Digikam::ItemGPS", "classDigikam_1_1ItemGPS.html", null ]
    ] ],
    [ "Digikam::GPSItemInfo", "classDigikam_1_1GPSItemInfo.html", null ],
    [ "Digikam::GPSUndoCommand::UndoInfo", "classDigikam_1_1GPSUndoCommand_1_1UndoInfo.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >", "classDigikam_1_1Graph.html", [
      [ "Digikam::ItemHistoryGraphData", "classDigikam_1_1ItemHistoryGraphData.html", null ]
    ] ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::DominatorTree", "classDigikam_1_1Graph_1_1DominatorTree.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::Edge", "classDigikam_1_1Graph_1_1Edge.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch", "classDigikam_1_1Graph_1_1GraphSearch.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::CommonVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1CommonVisitor.html", [
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::BreadthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1BreadthFirstSearchVisitor.html", null ],
      [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::DepthFirstSearchVisitor", "classDigikam_1_1Graph_1_1GraphSearch_1_1DepthFirstSearchVisitor.html", null ]
    ] ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::GraphSearch::lessThanMapEdgeToTarget< GraphType, VertexLessThan >", "classDigikam_1_1Graph_1_1GraphSearch_1_1lessThanMapEdgeToTarget.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::Path", "classDigikam_1_1Graph_1_1Path.html", null ],
    [ "Digikam::Graph< VertexProperties, EdgeProperties >::Vertex", "classDigikam_1_1Graph_1_1Vertex.html", null ],
    [ "Digikam::GreycstorationContainer", "classDigikam_1_1GreycstorationContainer.html", null ],
    [ "Digikam::GroupedImagesFinder", "classDigikam_1_1GroupedImagesFinder.html", null ],
    [ "Digikam::GroupingViewImplementation", "classDigikam_1_1GroupingViewImplementation.html", [
      [ "Digikam::DigikamItemView", "classDigikam_1_1DigikamItemView.html", null ],
      [ "Digikam::TableViewTreeView", "classDigikam_1_1TableViewTreeView.html", null ]
    ] ],
    [ "Digikam::GroupItemFilterSettings", "classDigikam_1_1GroupItemFilterSettings.html", null ],
    [ "Digikam::GroupStateComputer", "classDigikam_1_1GroupStateComputer.html", null ],
    [ "Digikam::Haar::Calculator", "classDigikam_1_1Haar_1_1Calculator.html", null ],
    [ "Digikam::Haar::ImageData", "classDigikam_1_1Haar_1_1ImageData.html", null ],
    [ "Digikam::Haar::SignatureData", "classDigikam_1_1Haar_1_1SignatureData.html", null ],
    [ "Digikam::Haar::SignatureMap", "classDigikam_1_1Haar_1_1SignatureMap.html", null ],
    [ "Digikam::Haar::WeightBin", "classDigikam_1_1Haar_1_1WeightBin.html", null ],
    [ "Digikam::Haar::Weights", "classDigikam_1_1Haar_1_1Weights.html", null ],
    [ "Digikam::HaarIface", "classDigikam_1_1HaarIface.html", null ],
    [ "Digikam::HaarIface::Private", "classDigikam_1_1HaarIface_1_1Private.html", null ],
    [ "Digikam::HaarProgressObserver", "classDigikam_1_1HaarProgressObserver.html", [
      [ "Digikam::DuplicatesProgressObserver", "classDigikam_1_1DuplicatesProgressObserver.html", null ]
    ] ],
    [ "Digikam::HistoryEdgeProperties", "classDigikam_1_1HistoryEdgeProperties.html", null ],
    [ "Digikam::HistoryImageId", "classDigikam_1_1HistoryImageId.html", null ],
    [ "Digikam::HistoryVertexProperties", "classDigikam_1_1HistoryVertexProperties.html", null ],
    [ "Digikam::HotPixelContainer", "classDigikam_1_1HotPixelContainer.html", null ],
    [ "Digikam::HotPixelProps", "classDigikam_1_1HotPixelProps.html", null ],
    [ "Digikam::HotPixelsWeights", "classDigikam_1_1HotPixelsWeights.html", null ],
    [ "Digikam::HSLContainer", "classDigikam_1_1HSLContainer.html", null ],
    [ "Digikam::IccManager", "classDigikam_1_1IccManager.html", [
      [ "Digikam::IccPostLoadingManager", "classDigikam_1_1IccPostLoadingManager.html", null ]
    ] ],
    [ "Digikam::IccProfile", "classDigikam_1_1IccProfile.html", null ],
    [ "Digikam::IccSettings::Private", "classDigikam_1_1IccSettings_1_1Private.html", null ],
    [ "Digikam::ICCSettingsContainer", "classDigikam_1_1ICCSettingsContainer.html", null ],
    [ "Digikam::IccTransform", "classDigikam_1_1IccTransform.html", null ],
    [ "Digikam::Identity", "classDigikam_1_1Identity.html", null ],
    [ "Digikam::ImageChangeset", "classDigikam_1_1ImageChangeset.html", null ],
    [ "Digikam::ImageCommonContainer", "classDigikam_1_1ImageCommonContainer.html", null ],
    [ "Digikam::ImageCurves", "classDigikam_1_1ImageCurves.html", null ],
    [ "Digikam::ImageHistoryEntry", "classDigikam_1_1ImageHistoryEntry.html", null ],
    [ "Digikam::ImageIface", "classDigikam_1_1ImageIface.html", null ],
    [ "Digikam::ImageLevels", "classDigikam_1_1ImageLevels.html", null ],
    [ "Digikam::ImageListProvider", "classDigikam_1_1ImageListProvider.html", [
      [ "Digikam::EmptyImageListProvider", "classDigikam_1_1EmptyImageListProvider.html", null ],
      [ "Digikam::QListImageListProvider", "classDigikam_1_1QListImageListProvider.html", null ]
    ] ],
    [ "Digikam::ImageMetadataContainer", "classDigikam_1_1ImageMetadataContainer.html", null ],
    [ "Digikam::ImageQualityCalculator", "classDigikam_1_1ImageQualityCalculator.html", null ],
    [ "Digikam::ImageQualityCalculator::ResultDetection", "structDigikam_1_1ImageQualityCalculator_1_1ResultDetection.html", null ],
    [ "Digikam::ImageQualityContainer", "classDigikam_1_1ImageQualityContainer.html", null ],
    [ "Digikam::ImageQualityParser::Private", "classDigikam_1_1ImageQualityParser_1_1Private.html", null ],
    [ "Digikam::ImageRelation", "classDigikam_1_1ImageRelation.html", null ],
    [ "Digikam::ImageTagChangeset", "classDigikam_1_1ImageTagChangeset.html", null ],
    [ "Digikam::ImageTagProperty", "classDigikam_1_1ImageTagProperty.html", null ],
    [ "Digikam::ImageTagPropertyName", "classDigikam_1_1ImageTagPropertyName.html", null ],
    [ "Digikam::ImageWindow::Private", "classDigikam_1_1ImageWindow_1_1Private.html", null ],
    [ "Digikam::ImageZoomSettings", "classDigikam_1_1ImageZoomSettings.html", null ],
    [ "Digikam::ImportUI::Private", "classDigikam_1_1ImportUI_1_1Private.html", null ],
    [ "Digikam::InfraredContainer", "classDigikam_1_1InfraredContainer.html", null ],
    [ "Digikam::InternalTagName", "classDigikam_1_1InternalTagName.html", null ],
    [ "Digikam::IOFileSettings", "classDigikam_1_1IOFileSettings.html", null ],
    [ "Digikam::IOJobData", "classDigikam_1_1IOJobData.html", null ],
    [ "Digikam::IptcCoreContactInfo", "classDigikam_1_1IptcCoreContactInfo.html", null ],
    [ "Digikam::IptcCoreLocationInfo", "classDigikam_1_1IptcCoreLocationInfo.html", null ],
    [ "Digikam::ItemChangeHint", "classDigikam_1_1ItemChangeHint.html", null ],
    [ "Digikam::ItemComments", "classDigikam_1_1ItemComments.html", null ],
    [ "Digikam::ItemCopyMoveHint", "classDigikam_1_1ItemCopyMoveHint.html", null ],
    [ "Digikam::ItemCopyright", "classDigikam_1_1ItemCopyright.html", null ],
    [ "Digikam::ItemDelegateOverlayContainer", "classDigikam_1_1ItemDelegateOverlayContainer.html", [
      [ "Digikam::ItemViewDelegate", "classDigikam_1_1ItemViewDelegate.html", [
        [ "Digikam::ItemDelegate", "classDigikam_1_1ItemDelegate.html", [
          [ "Digikam::DigikamItemDelegate", "classDigikam_1_1DigikamItemDelegate.html", [
            [ "Digikam::ItemFaceDelegate", "classDigikam_1_1ItemFaceDelegate.html", null ]
          ] ],
          [ "Digikam::ItemThumbnailDelegate", "classDigikam_1_1ItemThumbnailDelegate.html", null ]
        ] ]
      ] ],
      [ "Digikam::ItemViewImportDelegate", "classDigikam_1_1ItemViewImportDelegate.html", [
        [ "Digikam::ImportDelegate", "classDigikam_1_1ImportDelegate.html", [
          [ "Digikam::ImportNormalDelegate", "classDigikam_1_1ImportNormalDelegate.html", null ],
          [ "Digikam::ImportThumbnailDelegate", "classDigikam_1_1ImportThumbnailDelegate.html", null ]
        ] ]
      ] ],
      [ "Digikam::VersionsDelegate", "classDigikam_1_1VersionsDelegate.html", null ],
      [ "ShowFoto::ShowfotoItemViewDelegate", "classShowFoto_1_1ShowfotoItemViewDelegate.html", [
        [ "ShowFoto::ShowfotoDelegate", "classShowFoto_1_1ShowfotoDelegate.html", [
          [ "ShowFoto::ShowfotoNormalDelegate", "classShowFoto_1_1ShowfotoNormalDelegate.html", null ],
          [ "ShowFoto::ShowfotoThumbnailDelegate", "classShowFoto_1_1ShowfotoThumbnailDelegate.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::ItemExtendedProperties", "classDigikam_1_1ItemExtendedProperties.html", null ],
    [ "Digikam::ItemFilterModelPrepareHook", "classDigikam_1_1ItemFilterModelPrepareHook.html", null ],
    [ "Digikam::ItemFilterModelTodoPackage", "classDigikam_1_1ItemFilterModelTodoPackage.html", null ],
    [ "Digikam::ItemFilterSettings", "classDigikam_1_1ItemFilterSettings.html", null ],
    [ "Digikam::ItemFiltersHistoryTreeItem", "classDigikam_1_1ItemFiltersHistoryTreeItem.html", null ],
    [ "Digikam::ItemHistoryGraph", "classDigikam_1_1ItemHistoryGraph.html", null ],
    [ "Digikam::ItemIconView::Private", "classDigikam_1_1ItemIconView_1_1Private.html", null ],
    [ "Digikam::ItemInfo", "classDigikam_1_1ItemInfo.html", null ],
    [ "Digikam::ItemInfoSet", "classDigikam_1_1ItemInfoSet.html", null ],
    [ "Digikam::ItemInfoStatic", "classDigikam_1_1ItemInfoStatic.html", null ],
    [ "Digikam::ItemLister", "classDigikam_1_1ItemLister.html", null ],
    [ "Digikam::ItemLister::Private", "classDigikam_1_1ItemLister_1_1Private.html", null ],
    [ "Digikam::ItemListerReceiver", "classDigikam_1_1ItemListerReceiver.html", [
      [ "Digikam::ItemListerValueListReceiver", "classDigikam_1_1ItemListerValueListReceiver.html", [
        [ "Digikam::ItemListerJobReceiver", "classDigikam_1_1ItemListerJobReceiver.html", [
          [ "Digikam::ItemListerJobPartsSendingReceiver", "classDigikam_1_1ItemListerJobPartsSendingReceiver.html", [
            [ "Digikam::ItemListerJobGrowingPartsSendingReceiver", "classDigikam_1_1ItemListerJobGrowingPartsSendingReceiver.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::ItemListerRecord", "classDigikam_1_1ItemListerRecord.html", null ],
    [ "Digikam::ItemMetadataAdjustmentHint", "classDigikam_1_1ItemMetadataAdjustmentHint.html", null ],
    [ "Digikam::ItemPosition", "classDigikam_1_1ItemPosition.html", null ],
    [ "Digikam::ItemQueryBuilder", "classDigikam_1_1ItemQueryBuilder.html", null ],
    [ "Digikam::ItemQueryPostHook", "classDigikam_1_1ItemQueryPostHook.html", null ],
    [ "Digikam::ItemQueryPostHooks", "classDigikam_1_1ItemQueryPostHooks.html", null ],
    [ "Digikam::ItemScanInfo", "classDigikam_1_1ItemScanInfo.html", null ],
    [ "Digikam::ItemScanner", "classDigikam_1_1ItemScanner.html", null ],
    [ "Digikam::ItemScanner::Private", "classDigikam_1_1ItemScanner_1_1Private.html", null ],
    [ "Digikam::ItemScannerCommit", "classDigikam_1_1ItemScannerCommit.html", null ],
    [ "Digikam::ItemShortInfo", "classDigikam_1_1ItemShortInfo.html", null ],
    [ "Digikam::ItemSortSettings", "classDigikam_1_1ItemSortSettings.html", null ],
    [ "Digikam::ItemTagPair", "classDigikam_1_1ItemTagPair.html", null ],
    [ "Digikam::ItemViewDelegatePrivate", "classDigikam_1_1ItemViewDelegatePrivate.html", [
      [ "Digikam::ItemDelegate::ItemDelegatePrivate", "classDigikam_1_1ItemDelegate_1_1ItemDelegatePrivate.html", [
        [ "Digikam::DigikamItemDelegatePrivate", "classDigikam_1_1DigikamItemDelegatePrivate.html", [
          [ "Digikam::ItemFaceDelegatePrivate", "classDigikam_1_1ItemFaceDelegatePrivate.html", null ]
        ] ],
        [ "Digikam::ItemThumbnailDelegatePrivate", "classDigikam_1_1ItemThumbnailDelegatePrivate.html", null ]
      ] ]
    ] ],
    [ "Digikam::ItemViewImportDelegatePrivate", "classDigikam_1_1ItemViewImportDelegatePrivate.html", [
      [ "Digikam::ImportDelegate::ImportDelegatePrivate", "classDigikam_1_1ImportDelegate_1_1ImportDelegatePrivate.html", [
        [ "Digikam::ImportNormalDelegatePrivate", "classDigikam_1_1ImportNormalDelegatePrivate.html", null ],
        [ "Digikam::ImportThumbnailDelegatePrivate", "classDigikam_1_1ImportThumbnailDelegatePrivate.html", null ]
      ] ]
    ] ],
    [ "Digikam::JPEGUtils::digikam_source_mgr", "structDigikam_1_1JPEGUtils_1_1digikam__source__mgr.html", null ],
    [ "Digikam::JPEGUtils::JpegRotator", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html", null ],
    [ "Digikam::KDNode", "classDigikam_1_1KDNode.html", null ],
    [ "Digikam::KDTree", "classDigikam_1_1KDTree.html", null ],
    [ "Digikam::KMemoryInfo", "classDigikam_1_1KMemoryInfo.html", null ],
    [ "Digikam::LcmsLock", "classDigikam_1_1LcmsLock.html", null ],
    [ "Digikam::LensDistortionPixelAccess", "classDigikam_1_1LensDistortionPixelAccess.html", null ],
    [ "Digikam::LensFunContainer", "classDigikam_1_1LensFunContainer.html", null ],
    [ "Digikam::LensFunIface", "classDigikam_1_1LensFunIface.html", null ],
    [ "Digikam::LessThanByProximityToSubject", "classDigikam_1_1LessThanByProximityToSubject.html", null ],
    [ "Digikam::LevelsContainer", "classDigikam_1_1LevelsContainer.html", null ],
    [ "Digikam::LightTableWindow::Private", "classDigikam_1_1LightTableWindow_1_1Private.html", null ],
    [ "Digikam::LoadingCache::CacheLock", "classDigikam_1_1LoadingCache_1_1CacheLock.html", null ],
    [ "Digikam::LoadingCacheInterface", "classDigikam_1_1LoadingCacheInterface.html", null ],
    [ "Digikam::LoadingDescription", "classDigikam_1_1LoadingDescription.html", null ],
    [ "Digikam::LoadingDescription::PostProcessingParameters", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html", null ],
    [ "Digikam::LoadingDescription::PreviewParameters", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html", null ],
    [ "Digikam::LoadingProcess", "classDigikam_1_1LoadingProcess.html", [
      [ "Digikam::SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", null ]
    ] ],
    [ "Digikam::LoadingProcessListener", "classDigikam_1_1LoadingProcessListener.html", [
      [ "Digikam::SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", null ]
    ] ],
    [ "Digikam::LoadSaveFileInfoProvider", "classDigikam_1_1LoadSaveFileInfoProvider.html", [
      [ "Digikam::DatabaseLoadSaveFileInfoProvider", "classDigikam_1_1DatabaseLoadSaveFileInfoProvider.html", null ]
    ] ],
    [ "Digikam::LoadSaveNotifier", "classDigikam_1_1LoadSaveNotifier.html", [
      [ "Digikam::LoadSaveThread", "classDigikam_1_1LoadSaveThread.html", [
        [ "Digikam::ManagedLoadSaveThread", "classDigikam_1_1ManagedLoadSaveThread.html", [
          [ "Digikam::PreviewLoadThread", "classDigikam_1_1PreviewLoadThread.html", [
            [ "Digikam::FacePreviewLoader", "classDigikam_1_1FacePreviewLoader.html", null ]
          ] ],
          [ "Digikam::SharedLoadSaveThread", "classDigikam_1_1SharedLoadSaveThread.html", null ],
          [ "Digikam::ThumbnailLoadThread", "classDigikam_1_1ThumbnailLoadThread.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::LoadSaveTask", "classDigikam_1_1LoadSaveTask.html", [
      [ "Digikam::LoadingTask", "classDigikam_1_1LoadingTask.html", null ],
      [ "Digikam::SavingTask", "classDigikam_1_1SavingTask.html", null ]
    ] ],
    [ "Digikam::LocalContrastContainer", "classDigikam_1_1LocalContrastContainer.html", null ],
    [ "Digikam::LookupAltitude::Request", "classDigikam_1_1LookupAltitude_1_1Request.html", null ],
    [ "Digikam::LookupFactory", "classDigikam_1_1LookupFactory.html", null ],
    [ "Digikam::MaintenanceData", "classDigikam_1_1MaintenanceData.html", null ],
    [ "Digikam::MaintenanceSettings", "classDigikam_1_1MaintenanceSettings.html", null ],
    [ "Digikam::Mat", "structDigikam_1_1Mat.html", null ],
    [ "Digikam::MetadataHub", "classDigikam_1_1MetadataHub.html", null ],
    [ "Digikam::MetaEngine", "classDigikam_1_1MetaEngine.html", [
      [ "Digikam::DMetadata", "classDigikam_1_1DMetadata.html", null ]
    ] ],
    [ "Digikam::MetaEngine::Private", "classDigikam_1_1MetaEngine_1_1Private.html", null ],
    [ "Digikam::MetaEngineData", "classDigikam_1_1MetaEngineData.html", null ],
    [ "Digikam::MetaEngineMergeHelper< Data, Key, KeyString, KeyStringList >", "classDigikam_1_1MetaEngineMergeHelper.html", null ],
    [ "Digikam::MetaEnginePreviews", "classDigikam_1_1MetaEnginePreviews.html", null ],
    [ "Digikam::MetaEngineRotation", "classDigikam_1_1MetaEngineRotation.html", null ],
    [ "Digikam::MetaEngineSettingsContainer", "classDigikam_1_1MetaEngineSettingsContainer.html", null ],
    [ "Digikam::MixerContainer", "classDigikam_1_1MixerContainer.html", null ],
    [ "Digikam::NamespaceEntry", "classDigikam_1_1NamespaceEntry.html", null ],
    [ "Digikam::NewlyAppearedFile", "classDigikam_1_1NewlyAppearedFile.html", null ],
    [ "Digikam::NRContainer", "classDigikam_1_1NRContainer.html", null ],
    [ "Digikam::OpenCVDNNFaceDetector", "classDigikam_1_1OpenCVDNNFaceDetector.html", null ],
    [ "Digikam::OpenCVDNNFaceRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer.html", null ],
    [ "Digikam::OpenCVDNNFaceRecognizer::Private", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html", null ],
    [ "Digikam::OpenfacePreprocessor", "classDigikam_1_1OpenfacePreprocessor.html", null ],
    [ "Digikam::PageItem", "classDigikam_1_1PageItem.html", null ],
    [ "Digikam::PAlbumPath", "classDigikam_1_1PAlbumPath.html", null ],
    [ "Digikam::ParallelWorkers", "classDigikam_1_1ParallelWorkers.html", [
      [ "Digikam::ParallelAdapter< Digikam::FileWorkerInterface >", "classDigikam_1_1ParallelAdapter.html", null ],
      [ "Digikam::ParallelAdapter< A >", "classDigikam_1_1ParallelAdapter.html", null ]
    ] ],
    [ "Digikam::Parser", "classDigikam_1_1Parser.html", [
      [ "Digikam::DefaultRenameParser", "classDigikam_1_1DefaultRenameParser.html", null ],
      [ "Digikam::ImportRenameParser", "classDigikam_1_1ImportRenameParser.html", null ]
    ] ],
    [ "Digikam::ParseResults", "classDigikam_1_1ParseResults.html", null ],
    [ "Digikam::ParseSettings", "classDigikam_1_1ParseSettings.html", null ],
    [ "Digikam::PhotoInfoContainer", "classDigikam_1_1PhotoInfoContainer.html", null ],
    [ "Digikam::PointTransformAffine", "classDigikam_1_1PointTransformAffine.html", null ],
    [ "Digikam::PreviewSettings", "classDigikam_1_1PreviewSettings.html", null ],
    [ "Digikam::ProgressEntry", "classDigikam_1_1ProgressEntry.html", null ],
    [ "Digikam::PTOFile", "classDigikam_1_1PTOFile.html", null ],
    [ "Digikam::PTOType", "structDigikam_1_1PTOType.html", null ],
    [ "Digikam::PTOType::ControlPoint", "structDigikam_1_1PTOType_1_1ControlPoint.html", null ],
    [ "Digikam::PTOType::Image", "structDigikam_1_1PTOType_1_1Image.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< T >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Digikam::PTOType::Mask", "structDigikam_1_1PTOType_1_1Mask.html", null ],
    [ "Digikam::PTOType::Optimization", "structDigikam_1_1PTOType_1_1Optimization.html", null ],
    [ "Digikam::PTOType::Project", "structDigikam_1_1PTOType_1_1Project.html", null ],
    [ "Digikam::PTOType::Project::FileFormat", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html", null ],
    [ "Digikam::PTOType::Stitcher", "structDigikam_1_1PTOType_1_1Stitcher.html", null ],
    [ "Digikam::QueueMgrWindow::Private", "classDigikam_1_1QueueMgrWindow_1_1Private.html", null ],
    [ "Digikam::QueueSettings", "classDigikam_1_1QueueSettings.html", null ],
    [ "Digikam::RandomNumberGenerator", "classDigikam_1_1RandomNumberGenerator.html", null ],
    [ "Digikam::RatingStarDrawer", "classDigikam_1_1RatingStarDrawer.html", [
      [ "Digikam::RatingComboBoxDelegate", "classDigikam_1_1RatingComboBoxDelegate.html", null ],
      [ "Digikam::RatingComboBoxWidget", "classDigikam_1_1RatingComboBoxWidget.html", null ]
    ] ],
    [ "Digikam::RecognitionBenchmarker::Statistics", "classDigikam_1_1RecognitionBenchmarker_1_1Statistics.html", null ],
    [ "Digikam::RedEye::RegressionTree", "structDigikam_1_1RedEye_1_1RegressionTree.html", null ],
    [ "Digikam::RedEye::ShapePredictor", "classDigikam_1_1RedEye_1_1ShapePredictor.html", null ],
    [ "Digikam::RedEye::SplitFeature", "structDigikam_1_1RedEye_1_1SplitFeature.html", null ],
    [ "Digikam::RedEyeCorrectionContainer", "classDigikam_1_1RedEyeCorrectionContainer.html", null ],
    [ "Digikam::RefocusMatrix", "classDigikam_1_1RefocusMatrix.html", null ],
    [ "Digikam::RGInfo", "classDigikam_1_1RGInfo.html", null ],
    [ "Digikam::RuleType", "classDigikam_1_1RuleType.html", null ],
    [ "Digikam::RuleTypeForConversion", "classDigikam_1_1RuleTypeForConversion.html", null ],
    [ "Digikam::SaveProperties", "classDigikam_1_1SaveProperties.html", null ],
    [ "Digikam::SavingContext", "classDigikam_1_1SavingContext.html", null ],
    [ "Digikam::ScanController::FileMetadataWrite", "classDigikam_1_1ScanController_1_1FileMetadataWrite.html", null ],
    [ "Digikam::ScanController::Private", "classDigikam_1_1ScanController_1_1Private.html", null ],
    [ "Digikam::ScanControllerCreator", "classDigikam_1_1ScanControllerCreator.html", null ],
    [ "Digikam::SchemeManager", "classDigikam_1_1SchemeManager.html", null ],
    [ "Digikam::SearchChangeset", "classDigikam_1_1SearchChangeset.html", null ],
    [ "Digikam::SearchInfo", "classDigikam_1_1SearchInfo.html", null ],
    [ "Digikam::SearchTextSettings", "classDigikam_1_1SearchTextSettings.html", [
      [ "Digikam::SearchTextFilterSettings", "classDigikam_1_1SearchTextFilterSettings.html", null ]
    ] ],
    [ "Digikam::SearchViewThemedPartsCache", "classDigikam_1_1SearchViewThemedPartsCache.html", [
      [ "Digikam::SearchView", "classDigikam_1_1SearchView.html", null ]
    ] ],
    [ "Digikam::SetupCollectionModel::Item", "classDigikam_1_1SetupCollectionModel_1_1Item.html", null ],
    [ "Digikam::SetupMetadata::Private", "classDigikam_1_1SetupMetadata_1_1Private.html", null ],
    [ "Digikam::SharpContainer", "classDigikam_1_1SharpContainer.html", null ],
    [ "Digikam::Sidebar::Private", "classDigikam_1_1Sidebar_1_1Private.html", null ],
    [ "Digikam::SidebarSplitter::Private", "classDigikam_1_1SidebarSplitter_1_1Private.html", null ],
    [ "Digikam::SidebarState", "classDigikam_1_1SidebarState.html", null ],
    [ "Digikam::SidecarFinder", "classDigikam_1_1SidecarFinder.html", null ],
    [ "Digikam::SimilarityDb", "classDigikam_1_1SimilarityDb.html", null ],
    [ "Digikam::SimilarityDbAccess", "classDigikam_1_1SimilarityDbAccess.html", null ],
    [ "Digikam::SimilarityDbSchemaUpdater", "classDigikam_1_1SimilarityDbSchemaUpdater.html", null ],
    [ "Digikam::SimpleTreeModel::Item", "classDigikam_1_1SimpleTreeModel_1_1Item.html", null ],
    [ "Digikam::SolidVolumeInfo", "classDigikam_1_1SolidVolumeInfo.html", null ],
    [ "Digikam::State", "structDigikam_1_1State.html", null ],
    [ "Digikam::StateSavingObject", "classDigikam_1_1StateSavingObject.html", [
      [ "Digikam::AbstractAlbumTreeView", "classDigikam_1_1AbstractAlbumTreeView.html", [
        [ "Digikam::AbstractCountingAlbumTreeView", "classDigikam_1_1AbstractCountingAlbumTreeView.html", [
          [ "Digikam::AbstractCheckableAlbumTreeView", "classDigikam_1_1AbstractCheckableAlbumTreeView.html", [
            [ "Digikam::AlbumTreeView", "classDigikam_1_1AlbumTreeView.html", [
              [ "Digikam::AlbumSelectTreeView", "classDigikam_1_1AlbumSelectTreeView.html", null ],
              [ "Digikam::AlbumSelectionTreeView", "classDigikam_1_1AlbumSelectionTreeView.html", null ]
            ] ],
            [ "Digikam::SearchTreeView", "classDigikam_1_1SearchTreeView.html", [
              [ "Digikam::EditableSearchTreeView", "classDigikam_1_1EditableSearchTreeView.html", [
                [ "Digikam::NormalSearchTreeView", "classDigikam_1_1NormalSearchTreeView.html", null ]
              ] ]
            ] ],
            [ "Digikam::TagTreeView", "classDigikam_1_1TagTreeView.html", [
              [ "Digikam::TagFolderView", "classDigikam_1_1TagFolderView.html", [
                [ "Digikam::TagCheckView", "classDigikam_1_1TagCheckView.html", [
                  [ "Digikam::TagFilterView", "classDigikam_1_1TagFilterView.html", null ]
                ] ],
                [ "Digikam::TagMngrTreeView", "classDigikam_1_1TagMngrTreeView.html", null ]
              ] ]
            ] ]
          ] ],
          [ "Digikam::DateTreeView", "classDigikam_1_1DateTreeView.html", null ]
        ] ]
      ] ],
      [ "Digikam::DateFolderView", "classDigikam_1_1DateFolderView.html", null ],
      [ "Digikam::FaceScanWidget", "classDigikam_1_1FaceScanWidget.html", null ],
      [ "Digikam::FilterSideBarWidget", "classDigikam_1_1FilterSideBarWidget.html", null ],
      [ "Digikam::FuzzySearchView", "classDigikam_1_1FuzzySearchView.html", null ],
      [ "Digikam::GPSSearchView", "classDigikam_1_1GPSSearchView.html", null ],
      [ "Digikam::LabelsTreeView", "classDigikam_1_1LabelsTreeView.html", null ],
      [ "Digikam::MapWidgetView", "classDigikam_1_1MapWidgetView.html", null ],
      [ "Digikam::SearchTextBar", "classDigikam_1_1SearchTextBar.html", [
        [ "Digikam::SearchTextBarDb", "classDigikam_1_1SearchTextBarDb.html", null ]
      ] ],
      [ "Digikam::Sidebar", "classDigikam_1_1Sidebar.html", [
        [ "Digikam::ImportItemPropertiesSideBarImport", "classDigikam_1_1ImportItemPropertiesSideBarImport.html", null ],
        [ "Digikam::ItemPropertiesSideBar", "classDigikam_1_1ItemPropertiesSideBar.html", [
          [ "Digikam::ItemPropertiesSideBarDB", "classDigikam_1_1ItemPropertiesSideBarDB.html", null ]
        ] ]
      ] ],
      [ "Digikam::SidebarWidget", "classDigikam_1_1SidebarWidget.html", [
        [ "Digikam::AlbumFolderViewSideBarWidget", "classDigikam_1_1AlbumFolderViewSideBarWidget.html", null ],
        [ "Digikam::DateFolderViewSideBarWidget", "classDigikam_1_1DateFolderViewSideBarWidget.html", null ],
        [ "Digikam::FuzzySearchSideBarWidget", "classDigikam_1_1FuzzySearchSideBarWidget.html", null ],
        [ "Digikam::GPSSearchSideBarWidget", "classDigikam_1_1GPSSearchSideBarWidget.html", null ],
        [ "Digikam::LabelsSideBarWidget", "classDigikam_1_1LabelsSideBarWidget.html", null ],
        [ "Digikam::PeopleSideBarWidget", "classDigikam_1_1PeopleSideBarWidget.html", null ],
        [ "Digikam::SearchSideBarWidget", "classDigikam_1_1SearchSideBarWidget.html", null ],
        [ "Digikam::TagViewSideBarWidget", "classDigikam_1_1TagViewSideBarWidget.html", null ],
        [ "Digikam::TimelineSideBarWidget", "classDigikam_1_1TimelineSideBarWidget.html", null ]
      ] ],
      [ "Digikam::TableView", "classDigikam_1_1TableView.html", null ],
      [ "Digikam::TagsManager", "classDigikam_1_1TagsManager.html", null ],
      [ "ShowFoto::ShowfotoFolderViewSideBar", "classShowFoto_1_1ShowfotoFolderViewSideBar.html", null ],
      [ "ShowFoto::ShowfotoStackViewSideBar", "classShowFoto_1_1ShowfotoStackViewSideBar.html", null ]
    ] ],
    [ "Digikam::SubjectData", "classDigikam_1_1SubjectData.html", null ],
    [ "Digikam::SubQueryBuilder", "classDigikam_1_1SubQueryBuilder.html", null ],
    [ "Digikam::SystemSettings", "classDigikam_1_1SystemSettings.html", null ],
    [ "Digikam::TableViewColumnConfiguration", "classDigikam_1_1TableViewColumnConfiguration.html", null ],
    [ "Digikam::TableViewColumnDescription", "classDigikam_1_1TableViewColumnDescription.html", null ],
    [ "Digikam::TableViewColumnProfile", "classDigikam_1_1TableViewColumnProfile.html", null ],
    [ "Digikam::TableViewModel::Item", "classDigikam_1_1TableViewModel_1_1Item.html", null ],
    [ "Digikam::TableViewShared", "classDigikam_1_1TableViewShared.html", null ],
    [ "Digikam::TagChangeset", "classDigikam_1_1TagChangeset.html", null ],
    [ "Digikam::TagData", "structDigikam_1_1TagData.html", null ],
    [ "Digikam::TaggingAction", "classDigikam_1_1TaggingAction.html", null ],
    [ "Digikam::TaggingActionFactory", "classDigikam_1_1TaggingActionFactory.html", null ],
    [ "Digikam::TaggingActionFactory::ConstraintInterface", "classDigikam_1_1TaggingActionFactory_1_1ConstraintInterface.html", null ],
    [ "Digikam::TagInfo", "classDigikam_1_1TagInfo.html", null ],
    [ "Digikam::TagProperties", "classDigikam_1_1TagProperties.html", null ],
    [ "Digikam::TagProperty", "classDigikam_1_1TagProperty.html", null ],
    [ "Digikam::TagPropertyName", "classDigikam_1_1TagPropertyName.html", null ],
    [ "Digikam::TagRegion", "classDigikam_1_1TagRegion.html", null ],
    [ "Digikam::TagShortInfo", "classDigikam_1_1TagShortInfo.html", null ],
    [ "Digikam::Template", "classDigikam_1_1Template.html", null ],
    [ "Digikam::TextureContainer", "classDigikam_1_1TextureContainer.html", null ],
    [ "Digikam::ThemeManager::Private", "classDigikam_1_1ThemeManager_1_1Private.html", null ],
    [ "Digikam::ThumbnailCreator", "classDigikam_1_1ThumbnailCreator.html", null ],
    [ "Digikam::ThumbnailCreator::Private", "classDigikam_1_1ThumbnailCreator_1_1Private.html", null ],
    [ "Digikam::ThumbnailIdentifier", "classDigikam_1_1ThumbnailIdentifier.html", [
      [ "Digikam::ThumbnailInfo", "classDigikam_1_1ThumbnailInfo.html", null ]
    ] ],
    [ "Digikam::ThumbnailImage", "classDigikam_1_1ThumbnailImage.html", null ],
    [ "Digikam::ThumbnailImageCatcher::Private", "classDigikam_1_1ThumbnailImageCatcher_1_1Private.html", null ],
    [ "Digikam::ThumbnailImageCatcher::Private::CatcherResult", "classDigikam_1_1ThumbnailImageCatcher_1_1Private_1_1CatcherResult.html", null ],
    [ "Digikam::ThumbnailInfoProvider", "classDigikam_1_1ThumbnailInfoProvider.html", [
      [ "Digikam::ThumbsDbInfoProvider", "classDigikam_1_1ThumbsDbInfoProvider.html", null ]
    ] ],
    [ "Digikam::ThumbnailLoadThread::Private", "classDigikam_1_1ThumbnailLoadThread_1_1Private.html", null ],
    [ "Digikam::ThumbnailLoadThreadStaticPriv", "classDigikam_1_1ThumbnailLoadThreadStaticPriv.html", null ],
    [ "Digikam::ThumbnailResult", "classDigikam_1_1ThumbnailResult.html", null ],
    [ "Digikam::ThumbnailSize", "classDigikam_1_1ThumbnailSize.html", null ],
    [ "Digikam::ThumbsDb", "classDigikam_1_1ThumbsDb.html", null ],
    [ "Digikam::ThumbsDbAccess", "classDigikam_1_1ThumbsDbAccess.html", null ],
    [ "Digikam::ThumbsDbInfo", "classDigikam_1_1ThumbsDbInfo.html", null ],
    [ "Digikam::ThumbsDbSchemaUpdater", "classDigikam_1_1ThumbsDbSchemaUpdater.html", null ],
    [ "Digikam::TileIndex", "classDigikam_1_1TileIndex.html", null ],
    [ "Digikam::TimeAdjustContainer", "classDigikam_1_1TimeAdjustContainer.html", null ],
    [ "Digikam::TonalityContainer", "classDigikam_1_1TonalityContainer.html", null ],
    [ "Digikam::TooltipCreator", "classDigikam_1_1TooltipCreator.html", null ],
    [ "Digikam::TrackCorrelator::Correlation", "classDigikam_1_1TrackCorrelator_1_1Correlation.html", null ],
    [ "Digikam::TrackCorrelator::CorrelationOptions", "classDigikam_1_1TrackCorrelator_1_1CorrelationOptions.html", null ],
    [ "Digikam::TrackManager::Track", "classDigikam_1_1TrackManager_1_1Track.html", null ],
    [ "Digikam::TrackManager::TrackPoint", "classDigikam_1_1TrackManager_1_1TrackPoint.html", null ],
    [ "Digikam::TrackReader::TrackReadResult", "classDigikam_1_1TrackReader_1_1TrackReadResult.html", null ],
    [ "Digikam::TrainingDataProvider", "classDigikam_1_1TrainingDataProvider.html", [
      [ "Digikam::RecognitionTrainingProvider", "classDigikam_1_1RecognitionTrainingProvider.html", null ]
    ] ],
    [ "Digikam::TransitionMngr", "classDigikam_1_1TransitionMngr.html", null ],
    [ "Digikam::TransitionMngr::Private", "classDigikam_1_1TransitionMngr_1_1Private.html", null ],
    [ "Digikam::TreeBranch", "classDigikam_1_1TreeBranch.html", null ],
    [ "Digikam::UndoAction", "classDigikam_1_1UndoAction.html", [
      [ "Digikam::UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html", null ],
      [ "Digikam::UndoActionReversible", "classDigikam_1_1UndoActionReversible.html", null ]
    ] ],
    [ "Digikam::UndoCache", "classDigikam_1_1UndoCache.html", null ],
    [ "Digikam::UndoManager", "classDigikam_1_1UndoManager.html", null ],
    [ "Digikam::UndoMetadataContainer", "classDigikam_1_1UndoMetadataContainer.html", null ],
    [ "Digikam::UndoState", "classDigikam_1_1UndoState.html", null ],
    [ "Digikam::VersionFileInfo", "classDigikam_1_1VersionFileInfo.html", null ],
    [ "Digikam::VersionFileOperation", "classDigikam_1_1VersionFileOperation.html", null ],
    [ "Digikam::VersionItemFilterSettings", "classDigikam_1_1VersionItemFilterSettings.html", null ],
    [ "Digikam::VersionManager", "classDigikam_1_1VersionManager.html", [
      [ "Digikam::DatabaseVersionManager", "classDigikam_1_1DatabaseVersionManager.html", null ]
    ] ],
    [ "Digikam::VersionManagerSettings", "classDigikam_1_1VersionManagerSettings.html", null ],
    [ "Digikam::VersionNamingScheme", "classDigikam_1_1VersionNamingScheme.html", [
      [ "Digikam::DefaultVersionNamingScheme", "classDigikam_1_1DefaultVersionNamingScheme.html", null ]
    ] ],
    [ "Digikam::VideoDecoder", "classDigikam_1_1VideoDecoder.html", null ],
    [ "Digikam::VideoDecoder::Private", "classDigikam_1_1VideoDecoder_1_1Private.html", null ],
    [ "Digikam::VideoFrame", "classDigikam_1_1VideoFrame.html", null ],
    [ "Digikam::VideoInfoContainer", "classDigikam_1_1VideoInfoContainer.html", null ],
    [ "Digikam::VideoMetadataContainer", "classDigikam_1_1VideoMetadataContainer.html", null ],
    [ "Digikam::VideoStripFilter", "classDigikam_1_1VideoStripFilter.html", null ],
    [ "Digikam::VideoThumbnailer", "classDigikam_1_1VideoThumbnailer.html", null ],
    [ "Digikam::VideoThumbWriter", "classDigikam_1_1VideoThumbWriter.html", null ],
    [ "Digikam::VidSlideSettings", "classDigikam_1_1VidSlideSettings.html", null ],
    [ "Digikam::VisibilityObject", "classDigikam_1_1VisibilityObject.html", [
      [ "Digikam::SearchField", "classDigikam_1_1SearchField.html", [
        [ "Digikam::SearchFieldAlbum", "classDigikam_1_1SearchFieldAlbum.html", null ],
        [ "Digikam::SearchFieldCheckBox", "classDigikam_1_1SearchFieldCheckBox.html", null ],
        [ "Digikam::SearchFieldChoice", "classDigikam_1_1SearchFieldChoice.html", null ],
        [ "Digikam::SearchFieldComboBox", "classDigikam_1_1SearchFieldComboBox.html", [
          [ "Digikam::SearchFieldColorDepth", "classDigikam_1_1SearchFieldColorDepth.html", null ],
          [ "Digikam::SearchFieldPageOrientation", "classDigikam_1_1SearchFieldPageOrientation.html", null ]
        ] ],
        [ "Digikam::SearchFieldLabels", "classDigikam_1_1SearchFieldLabels.html", null ],
        [ "Digikam::SearchFieldMonthDay", "classDigikam_1_1SearchFieldMonthDay.html", null ],
        [ "Digikam::SearchFieldRangeDate", "classDigikam_1_1SearchFieldRangeDate.html", null ],
        [ "Digikam::SearchFieldRangeDouble", "classDigikam_1_1SearchFieldRangeDouble.html", null ],
        [ "Digikam::SearchFieldRangeInt", "classDigikam_1_1SearchFieldRangeInt.html", null ],
        [ "Digikam::SearchFieldRating", "classDigikam_1_1SearchFieldRating.html", null ],
        [ "Digikam::SearchFieldText", "classDigikam_1_1SearchFieldText.html", [
          [ "Digikam::SearchFieldKeyword", "classDigikam_1_1SearchFieldKeyword.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Digikam::WBContainer", "classDigikam_1_1WBContainer.html", null ],
    [ "Digikam::Workflow", "classDigikam_1_1Workflow.html", null ],
    [ "Digikam::WSAlbum", "classDigikam_1_1WSAlbum.html", [
      [ "DigikamGenericFaceBookPlugin::FbAlbum", "classDigikamGenericFaceBookPlugin_1_1FbAlbum.html", null ]
    ] ],
    [ "Digikam::WSToolUtils", "classDigikam_1_1WSToolUtils.html", null ],
    [ "DigikamEditorPerspectiveToolPlugin::PerspectiveMatrix", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html", null ],
    [ "DigikamEditorPerspectiveToolPlugin::PerspectiveTriangle", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTriangle.html", null ],
    [ "DigikamEditorPrintToolPlugin::PrintHelper", "classDigikamEditorPrintToolPlugin_1_1PrintHelper.html", null ],
    [ "DigikamGenericBoxPlugin::BOXFolder", "classDigikamGenericBoxPlugin_1_1BOXFolder.html", null ],
    [ "DigikamGenericBoxPlugin::BOXPhoto", "classDigikamGenericBoxPlugin_1_1BOXPhoto.html", null ],
    [ "DigikamGenericCalendarPlugin::CalParams", "structDigikamGenericCalendarPlugin_1_1CalParams.html", null ],
    [ "DigikamGenericCalendarPlugin::CalSystem", "classDigikamGenericCalendarPlugin_1_1CalSystem.html", null ],
    [ "DigikamGenericDNGConverterPlugin::DNGConverterActionData", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html", null ],
    [ "DigikamGenericDropBoxPlugin::DBFolder", "classDigikamGenericDropBoxPlugin_1_1DBFolder.html", null ],
    [ "DigikamGenericDropBoxPlugin::DBMPForm", "classDigikamGenericDropBoxPlugin_1_1DBMPForm.html", null ],
    [ "DigikamGenericDropBoxPlugin::DBPhoto", "classDigikamGenericDropBoxPlugin_1_1DBPhoto.html", null ],
    [ "DigikamGenericExpoBlendingPlugin::EnfuseSettings", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseSettings.html", null ],
    [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingActionData", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingActionData.html", null ],
    [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingItemPreprocessedUrls", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingItemPreprocessedUrls.html", null ],
    [ "DigikamGenericFaceBookPlugin::FbMPForm", "classDigikamGenericFaceBookPlugin_1_1FbMPForm.html", null ],
    [ "DigikamGenericFaceBookPlugin::FbPhoto", "classDigikamGenericFaceBookPlugin_1_1FbPhoto.html", null ],
    [ "DigikamGenericFaceBookPlugin::FbUser", "classDigikamGenericFaceBookPlugin_1_1FbUser.html", null ],
    [ "DigikamGenericFileCopyPlugin::FCContainer", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html", null ],
    [ "DigikamGenericFlickrPlugin::FlickrMPForm", "classDigikamGenericFlickrPlugin_1_1FlickrMPForm.html", null ],
    [ "DigikamGenericFlickrPlugin::FlickrWidget::Private", "classDigikamGenericFlickrPlugin_1_1FlickrWidget_1_1Private.html", null ],
    [ "DigikamGenericFlickrPlugin::FPhotoInfo", "classDigikamGenericFlickrPlugin_1_1FPhotoInfo.html", null ],
    [ "DigikamGenericFlickrPlugin::FPhotoSet", "classDigikamGenericFlickrPlugin_1_1FPhotoSet.html", null ],
    [ "DigikamGenericGeolocationEditPlugin::GeoDataContainer", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataContainer.html", null ],
    [ "DigikamGenericGeolocationEditPlugin::GeoDataParser", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataParser.html", [
      [ "DigikamGenericGeolocationEditPlugin::KMLGeoDataParser", "classDigikamGenericGeolocationEditPlugin_1_1KMLGeoDataParser.html", null ]
    ] ],
    [ "DigikamGenericGeolocationEditPlugin::SearchBackend::SearchResult", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html", null ],
    [ "DigikamGenericGeolocationEditPlugin::SearchResultModel::SearchResultItem", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel_1_1SearchResultItem.html", null ],
    [ "DigikamGenericGLViewerPlugin::GLViewerTimer", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GDMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GDMPForm.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GPMPForm", "classDigikamGenericGoogleServicesPlugin_1_1GPMPForm.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GSFolder", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html", null ],
    [ "DigikamGenericGoogleServicesPlugin::GSPhoto", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::AbstractThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1AbstractThemeParameter.html", [
      [ "DigikamGenericHtmlGalleryPlugin::ColorThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1ColorThemeParameter.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::IntThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1IntThemeParameter.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::ListThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1ListThemeParameter.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::StringThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1StringThemeParameter.html", null ]
    ] ],
    [ "DigikamGenericHtmlGalleryPlugin::CWrapper< Ptr, freeFcn >", "classDigikamGenericHtmlGalleryPlugin_1_1CWrapper.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryConfig::EnumFullFormat", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig_1_1EnumFullFormat.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryConfig::EnumThumbnailFormat", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig_1_1EnumThumbnailFormat.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryElement", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElement.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryElementFunctor", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElementFunctor.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryNameHelper", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryNameHelper.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::GalleryTheme", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryTheme.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::XMLAttributeList", "classDigikamGenericHtmlGalleryPlugin_1_1XMLAttributeList.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::XMLElement", "classDigikamGenericHtmlGalleryPlugin_1_1XMLElement.html", null ],
    [ "DigikamGenericHtmlGalleryPlugin::XMLWriter", "classDigikamGenericHtmlGalleryPlugin_1_1XMLWriter.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackGallery", "classDigikamGenericImageShackPlugin_1_1ImageShackGallery.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackMPForm", "classDigikamGenericImageShackPlugin_1_1ImageShackMPForm.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackPhoto", "classDigikamGenericImageShackPlugin_1_1ImageShackPhoto.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackSession", "classDigikamGenericImageShackPlugin_1_1ImageShackSession.html", null ],
    [ "DigikamGenericImageShackPlugin::ImageShackWidget::Private", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerAction", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerAction.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerResult", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerResult::ImgurAccount", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurAccount.html", null ],
    [ "DigikamGenericImgUrPlugin::ImgurTalkerResult::ImgurImage", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html", null ],
    [ "DigikamGenericINatPlugin::ComputerVisionScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html", null ],
    [ "DigikamGenericINatPlugin::INatTalker::NearbyObservation", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html", null ],
    [ "DigikamGenericINatPlugin::INatTalker::PhotoUploadRequest", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadRequest.html", null ],
    [ "DigikamGenericINatPlugin::INatTalker::PhotoUploadResult", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html", null ],
    [ "DigikamGenericINatPlugin::INatWidget::Private", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html", null ],
    [ "DigikamGenericINatPlugin::Taxon", "classDigikamGenericINatPlugin_1_1Taxon.html", null ],
    [ "DigikamGenericIpfsPlugin::IpfsTalkerAction", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction.html", null ],
    [ "DigikamGenericIpfsPlugin::IpfsTalkerResult", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult.html", null ],
    [ "DigikamGenericIpfsPlugin::IpfsTalkerResult::IPFSImage", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult_1_1IPFSImage.html", null ],
    [ "DigikamGenericJAlbumPlugin::JAlbumSettings", "classDigikamGenericJAlbumPlugin_1_1JAlbumSettings.html", null ],
    [ "DigikamGenericMjpegStreamPlugin::MjpegFrameOsd", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameOsd.html", null ],
    [ "DigikamGenericMjpegStreamPlugin::MjpegStreamDlg::Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg_1_1Private.html", null ],
    [ "DigikamGenericMjpegStreamPlugin::MjpegStreamSettings", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamSettings.html", null ],
    [ "DigikamGenericOneDrivePlugin::ODFolder", "classDigikamGenericOneDrivePlugin_1_1ODFolder.html", null ],
    [ "DigikamGenericOneDrivePlugin::ODMPForm", "classDigikamGenericOneDrivePlugin_1_1ODMPForm.html", null ],
    [ "DigikamGenericOneDrivePlugin::ODPhoto", "classDigikamGenericOneDrivePlugin_1_1ODPhoto.html", null ],
    [ "DigikamGenericPanoramaPlugin::PanoActionData", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html", null ],
    [ "DigikamGenericPanoramaPlugin::PanoramaPreprocessedUrls", "classDigikamGenericPanoramaPlugin_1_1PanoramaPreprocessedUrls.html", null ],
    [ "DigikamGenericPinterestPlugin::PFolder", "classDigikamGenericPinterestPlugin_1_1PFolder.html", null ],
    [ "DigikamGenericPinterestPlugin::PPhoto", "classDigikamGenericPinterestPlugin_1_1PPhoto.html", null ],
    [ "DigikamGenericPiwigoPlugin::PiwigoAlbum", "classDigikamGenericPiwigoPlugin_1_1PiwigoAlbum.html", null ],
    [ "DigikamGenericPiwigoPlugin::PiwigoSession", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html", null ],
    [ "DigikamGenericPresentationPlugin::KBEffect", "classDigikamGenericPresentationPlugin_1_1KBEffect.html", [
      [ "DigikamGenericPresentationPlugin::BlendKBEffect", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html", null ],
      [ "DigikamGenericPresentationPlugin::FadeKBEffect", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html", null ]
    ] ],
    [ "DigikamGenericPresentationPlugin::KBImage", "classDigikamGenericPresentationPlugin_1_1KBImage.html", null ],
    [ "DigikamGenericPresentationPlugin::KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html", null ],
    [ "DigikamGenericPresentationPlugin::PresentationContainer", "classDigikamGenericPresentationPlugin_1_1PresentationContainer.html", null ],
    [ "DigikamGenericPresentationPlugin::PresentationKB::Private", "classDigikamGenericPresentationPlugin_1_1PresentationKB_1_1Private.html", null ],
    [ "DigikamGenericPresentationPlugin::PresentationLoader", "classDigikamGenericPresentationPlugin_1_1PresentationLoader.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintAdditionalInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAdditionalInfo.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintCaptionInfo", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionInfo.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhoto.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintPhotoSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoSize.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AdvPrintSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AtkinsPageLayout", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayout.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::AtkinsPageLayoutTree", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree.html", null ],
    [ "DigikamGenericPrintCreatorPlugin::TemplateIcon", "classDigikamGenericPrintCreatorPlugin_1_1TemplateIcon.html", null ],
    [ "DigikamGenericRajcePlugin::RajceAlbum", "structDigikamGenericRajcePlugin_1_1RajceAlbum.html", null ],
    [ "DigikamGenericRajcePlugin::RajceMPForm", "classDigikamGenericRajcePlugin_1_1RajceMPForm.html", null ],
    [ "DigikamGenericRajcePlugin::RajceSession", "classDigikamGenericRajcePlugin_1_1RajceSession.html", null ],
    [ "DigikamGenericSendByMailPlugin::MailSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html", null ],
    [ "DigikamGenericSlideShowPlugin::SlideShowSettings", "classDigikamGenericSlideShowPlugin_1_1SlideShowSettings.html", null ],
    [ "DigikamGenericSmugPlugin::SmugAlbum", "classDigikamGenericSmugPlugin_1_1SmugAlbum.html", null ],
    [ "DigikamGenericSmugPlugin::SmugAlbumTmpl", "classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl.html", null ],
    [ "DigikamGenericSmugPlugin::SmugCategory", "classDigikamGenericSmugPlugin_1_1SmugCategory.html", null ],
    [ "DigikamGenericSmugPlugin::SmugMPForm", "classDigikamGenericSmugPlugin_1_1SmugMPForm.html", null ],
    [ "DigikamGenericSmugPlugin::SmugPhoto", "classDigikamGenericSmugPlugin_1_1SmugPhoto.html", null ],
    [ "DigikamGenericSmugPlugin::SmugUser", "classDigikamGenericSmugPlugin_1_1SmugUser.html", null ],
    [ "DigikamGenericTwitterPlugin::TwAlbum", "classDigikamGenericTwitterPlugin_1_1TwAlbum.html", null ],
    [ "DigikamGenericTwitterPlugin::TwMPForm", "classDigikamGenericTwitterPlugin_1_1TwMPForm.html", null ],
    [ "DigikamGenericTwitterPlugin::TwPhoto", "classDigikamGenericTwitterPlugin_1_1TwPhoto.html", null ],
    [ "DigikamGenericTwitterPlugin::TwUser", "classDigikamGenericTwitterPlugin_1_1TwUser.html", null ],
    [ "DigikamGenericVKontaktePlugin::VKNewAlbumDlg::AlbumProperties", "structDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg_1_1AlbumProperties.html", null ],
    [ "DigikamGenericYFPlugin::YandexFotkiAlbum", "classDigikamGenericYFPlugin_1_1YandexFotkiAlbum.html", null ],
    [ "DigikamGenericYFPlugin::YFPhoto", "classDigikamGenericYFPlugin_1_1YFPhoto.html", null ],
    [ "DImgPreviewItemPrivate public GraphicsDImgItem::GraphicsDImgItemPrivate", null, [
      [ "Digikam::DImgPreviewItem", "classDigikam_1_1DImgPreviewItem.html", [
        [ "Digikam::ItemPreviewCanvas", "classDigikam_1_1ItemPreviewCanvas.html", null ]
      ] ]
    ] ],
    [ "dng_host", null, [
      [ "Digikam::DNGWriterHost", "classDigikam_1_1DNGWriterHost.html", null ]
    ] ],
    [ "en265_packet", "structen265__packet.html", null ],
    [ "enc_node", "classenc__node.html", [
      [ "enc_cb", "classenc__cb.html", null ],
      [ "enc_tb", "classenc__tb.html", null ]
    ] ],
    [ "enc_pb_inter", "structenc__pb__inter.html", null ],
    [ "encoder_params", "structencoder__params.html", null ],
    [ "encoder_picture_buffer", "classencoder__picture__buffer.html", null ],
    [ "EncoderCore", "classEncoderCore.html", [
      [ "EncoderCore_Custom", "classEncoderCore__Custom.html", null ]
    ] ],
    [ "error_queue", "classerror__queue.html", [
      [ "base_context", "classbase__context.html", [
        [ "decoder_context", "classdecoder__context.html", null ],
        [ "encoder_context", "classencoder__context.html", null ]
      ] ]
    ] ],
    [ "GraphicsDImgItemPrivate", null, [
      [ "Digikam::GraphicsDImgItem", "classDigikam_1_1GraphicsDImgItem.html", [
        [ "Digikam::DImgPreviewItem", "classDigikam_1_1DImgPreviewItem.html", null ],
        [ "Digikam::ImagePreviewItem", "classDigikam_1_1ImagePreviewItem.html", null ],
        [ "Digikam::ImageRegionItem", "classDigikam_1_1ImageRegionItem.html", null ]
      ] ]
    ] ],
    [ "heif::BitReader", "classheif_1_1BitReader.html", null ],
    [ "heif::BitstreamRange", "classheif_1_1BitstreamRange.html", null ],
    [ "heif::Box_grpl::EntityGroup", "structheif_1_1Box__grpl_1_1EntityGroup.html", null ],
    [ "heif::Box_hvcC::configuration", "structheif_1_1Box__hvcC_1_1configuration.html", null ],
    [ "heif::Box_iloc::Extent", "structheif_1_1Box__iloc_1_1Extent.html", null ],
    [ "heif::Box_iloc::Item", "structheif_1_1Box__iloc_1_1Item.html", null ],
    [ "heif::Box_ipco::Property", "structheif_1_1Box__ipco_1_1Property.html", null ],
    [ "heif::Box_ipma::Entry", "structheif_1_1Box__ipma_1_1Entry.html", null ],
    [ "heif::Box_ipma::PropertyAssociation", "structheif_1_1Box__ipma_1_1PropertyAssociation.html", null ],
    [ "heif::Box_iref::Reference", "structheif_1_1Box__iref_1_1Reference.html", null ],
    [ "heif::BoxHeader", "classheif_1_1BoxHeader.html", [
      [ "heif::Box", "classheif_1_1Box.html", [
        [ "heif::Box_auxC", "classheif_1_1Box__auxC.html", null ],
        [ "heif::Box_clap", "classheif_1_1Box__clap.html", null ],
        [ "heif::Box_colr", "classheif_1_1Box__colr.html", null ],
        [ "heif::Box_dinf", "classheif_1_1Box__dinf.html", null ],
        [ "heif::Box_dref", "classheif_1_1Box__dref.html", null ],
        [ "heif::Box_ftyp", "classheif_1_1Box__ftyp.html", null ],
        [ "heif::Box_grpl", "classheif_1_1Box__grpl.html", null ],
        [ "heif::Box_hdlr", "classheif_1_1Box__hdlr.html", null ],
        [ "heif::Box_hvcC", "classheif_1_1Box__hvcC.html", null ],
        [ "heif::Box_idat", "classheif_1_1Box__idat.html", null ],
        [ "heif::Box_iinf", "classheif_1_1Box__iinf.html", null ],
        [ "heif::Box_iloc", "classheif_1_1Box__iloc.html", null ],
        [ "heif::Box_imir", "classheif_1_1Box__imir.html", null ],
        [ "heif::Box_infe", "classheif_1_1Box__infe.html", null ],
        [ "heif::Box_ipco", "classheif_1_1Box__ipco.html", null ],
        [ "heif::Box_ipma", "classheif_1_1Box__ipma.html", null ],
        [ "heif::Box_iprp", "classheif_1_1Box__iprp.html", null ],
        [ "heif::Box_iref", "classheif_1_1Box__iref.html", null ],
        [ "heif::Box_irot", "classheif_1_1Box__irot.html", null ],
        [ "heif::Box_ispe", "classheif_1_1Box__ispe.html", null ],
        [ "heif::Box_meta", "classheif_1_1Box__meta.html", null ],
        [ "heif::Box_pitm", "classheif_1_1Box__pitm.html", null ],
        [ "heif::Box_pixi", "classheif_1_1Box__pixi.html", null ],
        [ "heif::Box_url", "classheif_1_1Box__url.html", null ]
      ] ]
    ] ],
    [ "heif::color_profile", "classheif_1_1color__profile.html", [
      [ "heif::color_profile_nclx", "classheif_1_1color__profile__nclx.html", null ],
      [ "heif::color_profile_raw", "classheif_1_1color__profile__raw.html", null ]
    ] ],
    [ "heif::ColorConversionCosts", "structheif_1_1ColorConversionCosts.html", null ],
    [ "heif::ColorConversionOperation", "classheif_1_1ColorConversionOperation.html", null ],
    [ "heif::ColorConversionOptions", "structheif_1_1ColorConversionOptions.html", null ],
    [ "heif::ColorConversionPipeline", "classheif_1_1ColorConversionPipeline.html", null ],
    [ "heif::ColorState", "structheif_1_1ColorState.html", null ],
    [ "heif::ColorStateWithCost", "structheif_1_1ColorStateWithCost.html", null ],
    [ "heif::Context", "classheif_1_1Context.html", null ],
    [ "heif::Context::Reader", "classheif_1_1Context_1_1Reader.html", null ],
    [ "heif::Context::ReadingOptions", "classheif_1_1Context_1_1ReadingOptions.html", null ],
    [ "heif::Context::Writer", "classheif_1_1Context_1_1Writer.html", null ],
    [ "heif::Encoder", "classheif_1_1Encoder.html", null ],
    [ "heif::EncoderDescriptor", "classheif_1_1EncoderDescriptor.html", null ],
    [ "heif::EncoderParameter", "classheif_1_1EncoderParameter.html", null ],
    [ "heif::Error", "classheif_1_1Error.html", null ],
    [ "heif::ErrorBuffer", "classheif_1_1ErrorBuffer.html", [
      [ "heif::HeifContext", "classheif_1_1HeifContext.html", null ],
      [ "heif::HeifContext::Image", "classheif_1_1HeifContext_1_1Image.html", null ],
      [ "heif::HeifPixelImage", "classheif_1_1HeifPixelImage.html", null ]
    ] ],
    [ "heif::Fraction", "classheif_1_1Fraction.html", null ],
    [ "heif::HeifFile", "classheif_1_1HeifFile.html", null ],
    [ "heif::Image", "classheif_1_1Image.html", null ],
    [ "heif::Image::ScalingOptions", "classheif_1_1Image_1_1ScalingOptions.html", null ],
    [ "heif::ImageHandle", "classheif_1_1ImageHandle.html", null ],
    [ "heif::ImageHandle::DecodingOptions", "classheif_1_1ImageHandle_1_1DecodingOptions.html", null ],
    [ "heif::ImageMetadata", "classheif_1_1ImageMetadata.html", null ],
    [ "heif::Indent", "classheif_1_1Indent.html", null ],
    [ "heif::SEIMessage", "classheif_1_1SEIMessage.html", [
      [ "heif::SEIMessage_depth_representation_info", "classheif_1_1SEIMessage__depth__representation__info.html", null ]
    ] ],
    [ "heif::StreamReader", "classheif_1_1StreamReader.html", [
      [ "heif::StreamReader_CApi", "classheif_1_1StreamReader__CApi.html", null ],
      [ "heif::StreamReader_istream", "classheif_1_1StreamReader__istream.html", null ],
      [ "heif::StreamReader_memory", "classheif_1_1StreamReader__memory.html", null ]
    ] ],
    [ "heif::StreamWriter", "classheif_1_1StreamWriter.html", null ],
    [ "heif_color_profile_nclx", "structheif__color__profile__nclx.html", null ],
    [ "heif_context", "structheif__context.html", null ],
    [ "heif_decoder_plugin", "structheif__decoder__plugin.html", null ],
    [ "heif_decoding_options", "structheif__decoding__options.html", null ],
    [ "heif_depth_representation_info", "structheif__depth__representation__info.html", [
      [ "heif::SEIMessage_depth_representation_info", "classheif_1_1SEIMessage__depth__representation__info.html", null ]
    ] ],
    [ "heif_encoder", "structheif__encoder.html", null ],
    [ "heif_encoder_descriptor", "structheif__encoder__descriptor.html", null ],
    [ "heif_encoder_parameter", "structheif__encoder__parameter.html", null ],
    [ "heif_encoder_plugin", "structheif__encoder__plugin.html", null ],
    [ "heif_encoding_options", "structheif__encoding__options.html", [
      [ "heif::Context::EncodingOptions", "classheif_1_1Context_1_1EncodingOptions.html", null ]
    ] ],
    [ "heif_error", "structheif__error.html", null ],
    [ "heif_image", "structheif__image.html", null ],
    [ "heif_image_handle", "structheif__image__handle.html", null ],
    [ "heif_reader", "structheif__reader.html", null ],
    [ "heif_writer", "structheif__writer.html", null ],
    [ "image_data", "structimage__data.html", null ],
    [ "image_unit", "classimage__unit.html", null ],
    [ "ImageSink", "classImageSink.html", [
      [ "ImageSink_YUV", "classImageSink__YUV.html", null ]
    ] ],
    [ "ImageSource", "classImageSource.html", [
      [ "ImageSource_YUV", "classImageSource__YUV.html", null ]
    ] ],
    [ "intra_border_computer< pixel_t >", "classintra__border__computer.html", null ],
    [ "ItemFilterModelPrivate public QObject", null, [
      [ "Digikam::ItemFilterModel", "classDigikam_1_1ItemFilterModel.html", [
        [ "Digikam::ItemAlbumFilterModel", "classDigikam_1_1ItemAlbumFilterModel.html", null ]
      ] ]
    ] ],
    [ "KConfigSkeleton", null, [
      [ "DigikamEditorPrintToolPlugin::PrintConfig", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::GalleryConfig", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig.html", [
        [ "DigikamGenericHtmlGalleryPlugin::GalleryInfo", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryInfo.html", null ]
      ] ]
    ] ],
    [ "KJob", null, [
      [ "DigikamGenericMediaWikiPlugin::MediaWikiTalker", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiTalker.html", null ]
    ] ],
    [ "KXmlGuiWindow", null, [
      [ "Digikam::DXmlGuiWindow", "classDigikam_1_1DXmlGuiWindow.html", [
        [ "Digikam::DigikamApp", "classDigikam_1_1DigikamApp.html", null ],
        [ "Digikam::EditorWindow", "classDigikam_1_1EditorWindow.html", [
          [ "Digikam::ImageWindow", "classDigikam_1_1ImageWindow.html", null ],
          [ "ShowFoto::Showfoto", "classShowFoto_1_1Showfoto.html", null ]
        ] ],
        [ "Digikam::ImportUI", "classDigikam_1_1ImportUI.html", null ],
        [ "Digikam::LightTableWindow", "classDigikam_1_1LightTableWindow.html", null ],
        [ "Digikam::QueueMgrWindow", "classDigikam_1_1QueueMgrWindow.html", null ]
      ] ]
    ] ],
    [ "layer_data", "structlayer__data.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< double >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< int >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Digikam::PTOType::Image::LensParameter< VignettingMode >", "structDigikam_1_1PTOType_1_1Image_1_1LensParameter.html", null ],
    [ "Logging", "classLogging.html", null ],
    [ "Marble::LayerInterface", null, [
      [ "Digikam::BackendMarbleLayer", "classDigikam_1_1BackendMarbleLayer.html", null ]
    ] ],
    [ "MD5_CTX", "structMD5__CTX.html", null ],
    [ "MetaDataArray< DataUnit >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< CB_ref_info >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< CTB_info >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< PBMotion >", "classMetaDataArray.html", null ],
    [ "MetaDataArray< uint8_t >", "classMetaDataArray.html", null ],
    [ "Digikam::MetaEngineMergeHelper< Exiv2::ExifData, Exiv2::ExifKey, QLatin1String >", "classDigikam_1_1MetaEngineMergeHelper.html", [
      [ "Digikam::ExifMetaEngineMergeHelper", "classDigikam_1_1ExifMetaEngineMergeHelper.html", null ]
    ] ],
    [ "Digikam::MetaEngineMergeHelper< Exiv2::IptcData, Exiv2::IptcKey, QLatin1String >", "classDigikam_1_1MetaEngineMergeHelper.html", [
      [ "Digikam::IptcMetaEngineMergeHelper", "classDigikam_1_1IptcMetaEngineMergeHelper.html", null ]
    ] ],
    [ "MotionVector", "classMotionVector.html", null ],
    [ "MotionVectorAccess", "classMotionVectorAccess.html", null ],
    [ "nal_header", "structnal__header.html", null ],
    [ "NAL_Parser", "classNAL__Parser.html", null ],
    [ "NAL_unit", "classNAL__unit.html", null ],
    [ "option_base", "classoption__base.html", [
      [ "choice_option_base", "classchoice__option__base.html", [
        [ "choice_option< enum SOP_Structure >", "classchoice__option.html", [
          [ "option_SOP_Structure", "classoption__SOP__Structure.html", null ]
        ] ],
        [ "choice_option< enum MVTestMode >", "classchoice__option.html", [
          [ "option_MVTestMode", "classoption__MVTestMode.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_Split_BruteForce_ZeroBlockPrune >", "classchoice__option.html", [
          [ "option_ALGO_TB_Split_BruteForce_ZeroBlockPrune", "classoption__ALGO__TB__Split__BruteForce__ZeroBlockPrune.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_IntraPredMode_Subset >", "classchoice__option.html", [
          [ "option_ALGO_TB_IntraPredMode_Subset", "classoption__ALGO__TB__IntraPredMode__Subset.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_IntraPredMode >", "classchoice__option.html", [
          [ "option_ALGO_TB_IntraPredMode", "classoption__ALGO__TB__IntraPredMode.html", null ]
        ] ],
        [ "choice_option< enum ALGO_CB_IntraPartMode >", "classchoice__option.html", [
          [ "option_ALGO_CB_IntraPartMode", "classoption__ALGO__CB__IntraPartMode.html", null ]
        ] ],
        [ "choice_option< enum TBBitrateEstimMethod >", "classchoice__option.html", [
          [ "option_TBBitrateEstimMethod", "classoption__TBBitrateEstimMethod.html", null ]
        ] ],
        [ "choice_option< enum ALGO_TB_RateEstimation >", "classchoice__option.html", [
          [ "option_ALGO_TB_RateEstimation", "classoption__ALGO__TB__RateEstimation.html", null ]
        ] ],
        [ "choice_option< enum MVSearchAlgo >", "classchoice__option.html", [
          [ "option_MVSearchAlgo", "classoption__MVSearchAlgo.html", null ]
        ] ],
        [ "choice_option< enum PartMode >", "classchoice__option.html", [
          [ "option_InterPartMode", "classoption__InterPartMode.html", null ],
          [ "option_PartMode", "classoption__PartMode.html", null ]
        ] ],
        [ "choice_option< enum MEMode >", "classchoice__option.html", [
          [ "option_MEMode", "classoption__MEMode.html", null ]
        ] ],
        [ "choice_option< T >", "classchoice__option.html", null ]
      ] ],
      [ "option_bool", "classoption__bool.html", null ],
      [ "option_int", "classoption__int.html", null ],
      [ "option_string", "classoption__string.html", null ]
    ] ],
    [ "PacketSink", "classPacketSink.html", [
      [ "PacketSink_File", "classPacketSink__File.html", null ]
    ] ],
    [ "PBMotion", "classPBMotion.html", null ],
    [ "PBMotionCoding", "classPBMotionCoding.html", null ],
    [ "pic_order_counter", "classpic__order__counter.html", [
      [ "sop_creator", "classsop__creator.html", [
        [ "sop_creator_intra_only", "classsop__creator__intra__only.html", null ],
        [ "sop_creator_trivial_low_delay", "classsop__creator__trivial__low__delay.html", null ]
      ] ]
    ] ],
    [ "pic_parameter_set", "classpic__parameter__set.html", null ],
    [ "PixelAccessor", "classPixelAccessor.html", null ],
    [ "PLT_MediaServer", null, [
      [ "DigikamGenericMediaServerPlugin::DLNAMediaServer", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServer.html", null ]
    ] ],
    [ "PLT_MediaServerDelegate", null, [
      [ "DigikamGenericMediaServerPlugin::DLNAMediaServerDelegate", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServerDelegate.html", [
        [ "DigikamGenericMediaServerPlugin::DLNAMediaServer", "classDigikamGenericMediaServerPlugin_1_1DLNAMediaServer.html", null ]
      ] ]
    ] ],
    [ "position", "structposition.html", null ],
    [ "pps_range_extension", "classpps__range__extension.html", null ],
    [ "Private public QSharedData", null, [
      [ "Digikam::DImg", "classDigikam_1_1DImg.html", null ]
    ] ],
    [ "profile_data", "classprofile__data.html", null ],
    [ "profile_tier_level", "classprofile__tier__level.html", null ],
    [ "pt_point", "structpt__point.html", null ],
    [ "pt_point_double", "structpt__point__double.html", null ],
    [ "pt_script", "structpt__script.html", null ],
    [ "pt_script_ctrl_point", "structpt__script__ctrl__point.html", null ],
    [ "pt_script_image", "structpt__script__image.html", null ],
    [ "pt_script_mask", "structpt__script__mask.html", null ],
    [ "pt_script_optimize", "structpt__script__optimize.html", null ],
    [ "pt_script_optimize_var", "structpt__script__optimize__var.html", null ],
    [ "pt_script_pano", "structpt__script__pano.html", null ],
    [ "QAbstractButton", null, [
      [ "Digikam::CoordinatesOverlayWidget", "classDigikam_1_1CoordinatesOverlayWidget.html", null ],
      [ "Digikam::GroupIndicatorOverlayWidget", "classDigikam_1_1GroupIndicatorOverlayWidget.html", null ],
      [ "Digikam::ImportOverlayWidget", "classDigikam_1_1ImportOverlayWidget.html", null ],
      [ "Digikam::ItemViewHoverButton", "classDigikam_1_1ItemViewHoverButton.html", [
        [ "Digikam::FaceRejectionOverlayButton", "classDigikam_1_1FaceRejectionOverlayButton.html", null ],
        [ "Digikam::ImportRotateOverlayButton", "classDigikam_1_1ImportRotateOverlayButton.html", null ],
        [ "Digikam::ItemFullScreenOverlayButton", "classDigikam_1_1ItemFullScreenOverlayButton.html", null ],
        [ "Digikam::ItemRotateOverlayButton", "classDigikam_1_1ItemRotateOverlayButton.html", null ],
        [ "Digikam::ItemSelectionOverlayButton", "classDigikam_1_1ItemSelectionOverlayButton.html", null ]
      ] ],
      [ "ShowFoto::ShowfotoCoordinatesOverlayWidget", "classShowFoto_1_1ShowfotoCoordinatesOverlayWidget.html", null ]
    ] ],
    [ "QAbstractItemDelegate", null, [
      [ "Digikam::ComboBoxDelegate", "classDigikam_1_1ComboBoxDelegate.html", null ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgListViewDelegate", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewDelegate.html", null ],
      [ "Digikam::DItemDelegate", "classDigikam_1_1DItemDelegate.html", [
        [ "Digikam::ItemViewDelegate", "classDigikam_1_1ItemViewDelegate.html", null ],
        [ "Digikam::ItemViewImportDelegate", "classDigikam_1_1ItemViewImportDelegate.html", null ],
        [ "ShowFoto::ShowfotoItemViewDelegate", "classShowFoto_1_1ShowfotoItemViewDelegate.html", null ]
      ] ],
      [ "Digikam::DWItemDelegate", "classDigikam_1_1DWItemDelegate.html", [
        [ "Digikam::SetupCollectionDelegate", "classDigikam_1_1SetupCollectionDelegate.html", null ]
      ] ]
    ] ],
    [ "QAbstractItemModel", null, [
      [ "Digikam::AbstractAlbumModel", "classDigikam_1_1AbstractAlbumModel.html", [
        [ "Digikam::AbstractSpecificAlbumModel", "classDigikam_1_1AbstractSpecificAlbumModel.html", [
          [ "Digikam::AbstractCountingAlbumModel", "classDigikam_1_1AbstractCountingAlbumModel.html", [
            [ "Digikam::AbstractCheckableAlbumModel", "classDigikam_1_1AbstractCheckableAlbumModel.html", [
              [ "Digikam::AlbumModel", "classDigikam_1_1AlbumModel.html", null ],
              [ "Digikam::SearchModel", "classDigikam_1_1SearchModel.html", null ],
              [ "Digikam::TagModel", "classDigikam_1_1TagModel.html", null ]
            ] ],
            [ "Digikam::DateAlbumModel", "classDigikam_1_1DateAlbumModel.html", null ]
          ] ]
        ] ]
      ] ],
      [ "Digikam::BookmarksModel", "classDigikam_1_1BookmarksModel.html", null ],
      [ "Digikam::DConfigDlgModel", "classDigikam_1_1DConfigDlgModel.html", [
        [ "Digikam::DConfigDlgWdgModel", "classDigikam_1_1DConfigDlgWdgModel.html", null ]
      ] ],
      [ "Digikam::GPSItemModel", "classDigikam_1_1GPSItemModel.html", null ],
      [ "Digikam::ItemFiltersHistoryModel", "classDigikam_1_1ItemFiltersHistoryModel.html", null ],
      [ "Digikam::ItemHistoryGraphModel", "classDigikam_1_1ItemHistoryGraphModel.html", null ],
      [ "Digikam::RGTagModel", "classDigikam_1_1RGTagModel.html", null ],
      [ "Digikam::SetupCollectionModel", "classDigikam_1_1SetupCollectionModel.html", null ],
      [ "Digikam::SimpleTreeModel", "classDigikam_1_1SimpleTreeModel.html", null ],
      [ "Digikam::TableViewModel", "classDigikam_1_1TableViewModel.html", null ],
      [ "Digikam::TagMngrListModel", "classDigikam_1_1TagMngrListModel.html", null ],
      [ "Digikam::TrackListModel", "classDigikam_1_1TrackListModel.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::SearchResultModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html", null ]
    ] ],
    [ "QAbstractItemView", null, [
      [ "Digikam::DConfigDlgInternal::DConfigDlgPlainView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgPlainView.html", null ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgTabbedView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTabbedView.html", null ]
    ] ],
    [ "QAbstractListModel", null, [
      [ "Digikam::ChoiceSearchModel", "classDigikam_1_1ChoiceSearchModel.html", null ],
      [ "Digikam::ImportItemModel", "classDigikam_1_1ImportItemModel.html", null ],
      [ "Digikam::ItemModel", "classDigikam_1_1ItemModel.html", null ],
      [ "Digikam::ItemVersionsModel", "classDigikam_1_1ItemVersionsModel.html", null ],
      [ "Digikam::RatingComboBoxModel", "classDigikam_1_1RatingComboBoxModel.html", null ],
      [ "ShowFoto::ShowfotoItemModel", "classShowFoto_1_1ShowfotoItemModel.html", null ]
    ] ],
    [ "QAbstractProxyModel", null, [
      [ "Digikam::DConfigDlgInternal::DConfigDlgListViewProxy", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListViewProxy.html", null ]
    ] ],
    [ "QAbstractSlider", null, [
      [ "Digikam::DSelector", "classDigikam_1_1DSelector.html", [
        [ "Digikam::DColorValueSelector", "classDigikam_1_1DColorValueSelector.html", null ]
      ] ]
    ] ],
    [ "QAbstractTableModel", null, [
      [ "Digikam::DTrashItemModel", "classDigikam_1_1DTrashItemModel.html", null ]
    ] ],
    [ "QAction", null, [
      [ "Digikam::DPluginAction", "classDigikam_1_1DPluginAction.html", null ],
      [ "Digikam::RemoveFilterAction", "classDigikam_1_1RemoveFilterAction.html", null ],
      [ "Digikam::RemoveFilterAction", "classDigikam_1_1RemoveFilterAction.html", null ]
    ] ],
    [ "QByteArray", null, [
      [ "Digikam::NonDeterministicRandomData", "classDigikam_1_1NonDeterministicRandomData.html", null ]
    ] ],
    [ "QCheckBox", null, [
      [ "DigikamGenericMetadataEditPlugin::MetadataCheckBox", "classDigikamGenericMetadataEditPlugin_1_1MetadataCheckBox.html", null ]
    ] ],
    [ "QComboBox", null, [
      [ "Digikam::AdvancedRenameInput", "classDigikam_1_1AdvancedRenameInput.html", null ],
      [ "Digikam::CountrySelector", "classDigikam_1_1CountrySelector.html", null ],
      [ "Digikam::DDateEdit", "classDigikam_1_1DDateEdit.html", null ],
      [ "Digikam::GeolocationFilter", "classDigikam_1_1GeolocationFilter.html", null ],
      [ "Digikam::IccRenderingIntentComboBox", "classDigikam_1_1IccRenderingIntentComboBox.html", null ],
      [ "Digikam::ImportFilterComboBox", "classDigikam_1_1ImportFilterComboBox.html", null ],
      [ "Digikam::MimeFilter", "classDigikam_1_1MimeFilter.html", null ],
      [ "Digikam::ModelIndexBasedComboBox", "classDigikam_1_1ModelIndexBasedComboBox.html", [
        [ "Digikam::RatingComboBox", "classDigikam_1_1RatingComboBox.html", null ],
        [ "Digikam::StayPoppedUpComboBox", "classDigikam_1_1StayPoppedUpComboBox.html", [
          [ "Digikam::ListViewComboBox", "classDigikam_1_1ListViewComboBox.html", [
            [ "Digikam::ChoiceSearchComboBox", "classDigikam_1_1ChoiceSearchComboBox.html", null ]
          ] ],
          [ "Digikam::TreeViewComboBox", "classDigikam_1_1TreeViewComboBox.html", [
            [ "Digikam::TreeViewLineEditComboBox", "classDigikam_1_1TreeViewLineEditComboBox.html", [
              [ "Digikam::AlbumSelectComboBox", "classDigikam_1_1AlbumSelectComboBox.html", [
                [ "Digikam::AbstractAlbumTreeViewSelectComboBox", "classDigikam_1_1AbstractAlbumTreeViewSelectComboBox.html", [
                  [ "Digikam::AlbumTreeViewSelectComboBox", "classDigikam_1_1AlbumTreeViewSelectComboBox.html", null ],
                  [ "Digikam::TagTreeViewSelectComboBox", "classDigikam_1_1TagTreeViewSelectComboBox.html", [
                    [ "Digikam::AddTagsComboBox", "classDigikam_1_1AddTagsComboBox.html", null ]
                  ] ]
                ] ]
              ] ]
            ] ]
          ] ]
        ] ]
      ] ],
      [ "Digikam::SqueezedComboBox", "classDigikam_1_1SqueezedComboBox.html", [
        [ "Digikam::IccProfilesComboBox", "classDigikam_1_1IccProfilesComboBox.html", null ]
      ] ],
      [ "Digikam::TimeZoneComboBox", "classDigikam_1_1TimeZoneComboBox.html", null ],
      [ "Digikam::WSComboBoxIntermediate", "classDigikam_1_1WSComboBoxIntermediate.html", null ]
    ] ],
    [ "QCompleter", null, [
      [ "Digikam::ModelCompleter", "classDigikam_1_1ModelCompleter.html", null ],
      [ "Digikam::TagCompleter", "classDigikam_1_1TagCompleter.html", null ]
    ] ],
    [ "QDBusAbstractAdaptor", null, [
      [ "CoreDbWatchAdaptor", "classCoreDbWatchAdaptor.html", null ]
    ] ],
    [ "QDialog", null, [
      [ "Digikam::AddBookmarkDialog", "classDigikam_1_1AddBookmarkDialog.html", null ],
      [ "Digikam::AdvancedRenameDialog", "classDigikam_1_1AdvancedRenameDialog.html", null ],
      [ "Digikam::AlbumPropsEdit", "classDigikam_1_1AlbumPropsEdit.html", null ],
      [ "Digikam::AlbumSelectDialog", "classDigikam_1_1AlbumSelectDialog.html", null ],
      [ "Digikam::BookmarksDialog", "classDigikam_1_1BookmarksDialog.html", null ],
      [ "Digikam::CameraFolderDialog", "classDigikam_1_1CameraFolderDialog.html", null ],
      [ "Digikam::CameraInfoDialog", "classDigikam_1_1CameraInfoDialog.html", null ],
      [ "Digikam::CameraSelection", "classDigikam_1_1CameraSelection.html", null ],
      [ "Digikam::CaptureDlg", "classDigikam_1_1CaptureDlg.html", null ],
      [ "Digikam::ClockPhotoDialog", "classDigikam_1_1ClockPhotoDialog.html", null ],
      [ "Digikam::ColorCorrectionDlg", "classDigikam_1_1ColorCorrectionDlg.html", null ],
      [ "Digikam::DConfigDlg", "classDigikam_1_1DConfigDlg.html", [
        [ "Digikam::Setup", "classDigikam_1_1Setup.html", null ],
        [ "ShowFoto::ShowfotoSetup", "classShowFoto_1_1ShowfotoSetup.html", null ]
      ] ],
      [ "Digikam::DPluginAboutDlg", "classDigikam_1_1DPluginAboutDlg.html", null ],
      [ "Digikam::DPluginDialog", "classDigikam_1_1DPluginDialog.html", [
        [ "Digikam::WSToolDialog", "classDigikam_1_1WSToolDialog.html", [
          [ "DigikamGenericBoxPlugin::BOXWindow", "classDigikamGenericBoxPlugin_1_1BOXWindow.html", null ],
          [ "DigikamGenericDropBoxPlugin::DBWindow", "classDigikamGenericDropBoxPlugin_1_1DBWindow.html", null ],
          [ "DigikamGenericFaceBookPlugin::FbWindow", "classDigikamGenericFaceBookPlugin_1_1FbWindow.html", null ],
          [ "DigikamGenericFileCopyPlugin::FCExportWindow", "classDigikamGenericFileCopyPlugin_1_1FCExportWindow.html", null ],
          [ "DigikamGenericFileTransferPlugin::FTExportWindow", "classDigikamGenericFileTransferPlugin_1_1FTExportWindow.html", null ],
          [ "DigikamGenericFileTransferPlugin::FTImportWindow", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html", null ],
          [ "DigikamGenericFlickrPlugin::FlickrWindow", "classDigikamGenericFlickrPlugin_1_1FlickrWindow.html", null ],
          [ "DigikamGenericGoogleServicesPlugin::GSWindow", "classDigikamGenericGoogleServicesPlugin_1_1GSWindow.html", null ],
          [ "DigikamGenericINatPlugin::INatWindow", "classDigikamGenericINatPlugin_1_1INatWindow.html", null ],
          [ "DigikamGenericImageShackPlugin::ImageShackWindow", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html", null ],
          [ "DigikamGenericImgUrPlugin::ImgurWindow", "classDigikamGenericImgUrPlugin_1_1ImgurWindow.html", null ],
          [ "DigikamGenericIpfsPlugin::IpfsWindow", "classDigikamGenericIpfsPlugin_1_1IpfsWindow.html", null ],
          [ "DigikamGenericMediaWikiPlugin::MediaWikiWindow", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiWindow.html", null ],
          [ "DigikamGenericOneDrivePlugin::ODWindow", "classDigikamGenericOneDrivePlugin_1_1ODWindow.html", null ],
          [ "DigikamGenericPinterestPlugin::PWindow", "classDigikamGenericPinterestPlugin_1_1PWindow.html", null ],
          [ "DigikamGenericPiwigoPlugin::PiwigoWindow", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html", null ],
          [ "DigikamGenericRajcePlugin::RajceWindow", "classDigikamGenericRajcePlugin_1_1RajceWindow.html", null ],
          [ "DigikamGenericSmugPlugin::SmugWindow", "classDigikamGenericSmugPlugin_1_1SmugWindow.html", null ],
          [ "DigikamGenericTwitterPlugin::TwWindow", "classDigikamGenericTwitterPlugin_1_1TwWindow.html", null ],
          [ "DigikamGenericVKontaktePlugin::VKWindow", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html", null ],
          [ "DigikamGenericYFPlugin::YFWindow", "classDigikamGenericYFPlugin_1_1YFWindow.html", null ]
        ] ],
        [ "DigikamGenericDNGConverterPlugin::DNGConverterDialog", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html", null ],
        [ "DigikamGenericDScannerPlugin::ScanDialog", "classDigikamGenericDScannerPlugin_1_1ScanDialog.html", null ],
        [ "DigikamGenericGeolocationEditPlugin::GeolocationEdit", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEdit.html", null ],
        [ "DigikamGenericMediaServerPlugin::DMediaServerDlg", "classDigikamGenericMediaServerPlugin_1_1DMediaServerDlg.html", null ],
        [ "DigikamGenericMetadataEditPlugin::MetadataEditDialog", "classDigikamGenericMetadataEditPlugin_1_1MetadataEditDialog.html", null ],
        [ "DigikamGenericMjpegStreamPlugin::MjpegStreamDlg", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html", null ],
        [ "DigikamGenericPresentationPlugin::PresentationDlg", "classDigikamGenericPresentationPlugin_1_1PresentationDlg.html", null ],
        [ "DigikamGenericSlideShowPlugin::SetupSlideShowDialog", "classDigikamGenericSlideShowPlugin_1_1SetupSlideShowDialog.html", null ],
        [ "DigikamGenericTimeAdjustPlugin::TimeAdjustDialog", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html", null ],
        [ "DigikamGenericWallpaperPlugin::WallpaperPluginDlg", "classDigikamGenericWallpaperPlugin_1_1WallpaperPluginDlg.html", null ]
      ] ],
      [ "Digikam::DProgressDlg", "classDigikam_1_1DProgressDlg.html", [
        [ "Digikam::AdvancedRenameProcessDialog", "classDigikam_1_1AdvancedRenameProcessDialog.html", null ]
      ] ],
      [ "Digikam::DatabaseMigrationDialog", "classDigikam_1_1DatabaseMigrationDialog.html", null ],
      [ "Digikam::DbShrinkDialog", "classDigikam_1_1DbShrinkDialog.html", null ],
      [ "Digikam::DeleteDialog", "classDigikam_1_1DeleteDialog.html", null ],
      [ "Digikam::FaceManagementHelpDlg", "classDigikam_1_1FaceManagementHelpDlg.html", null ],
      [ "Digikam::FileSaveOptionsDlg", "classDigikam_1_1FileSaveOptionsDlg.html", null ],
      [ "Digikam::FilesDownloader", "classDigikam_1_1FilesDownloader.html", null ],
      [ "Digikam::ICCProfileInfoDlg", "classDigikam_1_1ICCProfileInfoDlg.html", null ],
      [ "Digikam::ImportFilterDlg", "classDigikam_1_1ImportFilterDlg.html", null ],
      [ "Digikam::InfoDlg", "classDigikam_1_1InfoDlg.html", [
        [ "Digikam::DBStatDlg", "classDigikam_1_1DBStatDlg.html", null ],
        [ "Digikam::LibsInfoDlg", "classDigikam_1_1LibsInfoDlg.html", null ],
        [ "Digikam::RawCameraDlg", "classDigikam_1_1RawCameraDlg.html", null ],
        [ "Digikam::SolidHardwareDlg", "classDigikam_1_1SolidHardwareDlg.html", null ]
      ] ],
      [ "Digikam::MaintenanceDlg", "classDigikam_1_1MaintenanceDlg.html", null ],
      [ "Digikam::NamespaceEditDlg", "classDigikam_1_1NamespaceEditDlg.html", null ],
      [ "Digikam::OnlineVersionDlg", "classDigikam_1_1OnlineVersionDlg.html", null ],
      [ "Digikam::RuleDialog", "classDigikam_1_1RuleDialog.html", [
        [ "Digikam::DatabaseOptionDialog", "classDigikam_1_1DatabaseOptionDialog.html", null ],
        [ "Digikam::DateOptionDialog", "classDigikam_1_1DateOptionDialog.html", null ],
        [ "Digikam::DefaultValueDialog", "classDigikam_1_1DefaultValueDialog.html", null ],
        [ "Digikam::MetadataOptionDialog", "classDigikam_1_1MetadataOptionDialog.html", null ],
        [ "Digikam::RangeDialog", "classDigikam_1_1RangeDialog.html", null ],
        [ "Digikam::ReplaceDialog", "classDigikam_1_1ReplaceDialog.html", null ],
        [ "Digikam::SequenceNumberDialog", "classDigikam_1_1SequenceNumberDialog.html", null ]
      ] ],
      [ "Digikam::SoftProofDialog", "classDigikam_1_1SoftProofDialog.html", null ],
      [ "Digikam::TableViewConfigurationDialog", "classDigikam_1_1TableViewConfigurationDialog.html", null ],
      [ "Digikam::TagEditDlg", "classDigikam_1_1TagEditDlg.html", null ],
      [ "Digikam::TooltipDialog", "classDigikam_1_1TooltipDialog.html", null ],
      [ "Digikam::VersioningPromptUserSaveDialog", "classDigikam_1_1VersioningPromptUserSaveDialog.html", null ],
      [ "Digikam::VidPlayerDlg", "classDigikam_1_1VidPlayerDlg.html", null ],
      [ "Digikam::WSLoginDialog", "classDigikam_1_1WSLoginDialog.html", null ],
      [ "Digikam::WSNewAlbumDialog", "classDigikam_1_1WSNewAlbumDialog.html", [
        [ "DigikamGenericBoxPlugin::BOXNewAlbumDlg", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html", null ],
        [ "DigikamGenericDropBoxPlugin::DBNewAlbumDlg", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html", null ],
        [ "DigikamGenericFaceBookPlugin::FbNewAlbumDlg", "classDigikamGenericFaceBookPlugin_1_1FbNewAlbumDlg.html", null ],
        [ "DigikamGenericFlickrPlugin::FlickrNewAlbumDlg", "classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg.html", null ],
        [ "DigikamGenericGoogleServicesPlugin::GSNewAlbumDlg", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html", null ],
        [ "DigikamGenericImageShackPlugin::ImageShackNewAlbumDlg", "classDigikamGenericImageShackPlugin_1_1ImageShackNewAlbumDlg.html", null ],
        [ "DigikamGenericOneDrivePlugin::ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html", null ],
        [ "DigikamGenericPinterestPlugin::PNewAlbumDlg", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html", null ],
        [ "DigikamGenericRajcePlugin::RajceNewAlbumDlg", "classDigikamGenericRajcePlugin_1_1RajceNewAlbumDlg.html", null ],
        [ "DigikamGenericTwitterPlugin::TwNewAlbumDlg", "classDigikamGenericTwitterPlugin_1_1TwNewAlbumDlg.html", null ],
        [ "DigikamGenericYFPlugin::YFNewAlbumDlg", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html", null ]
      ] ],
      [ "Digikam::WSSelectUserDlg", "classDigikam_1_1WSSelectUserDlg.html", null ],
      [ "Digikam::WebBrowserDlg", "classDigikam_1_1WebBrowserDlg.html", null ],
      [ "Digikam::WorkflowDlg", "classDigikam_1_1WorkflowDlg.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingDlg", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingDlg.html", null ],
      [ "DigikamGenericGoogleServicesPlugin::ReplaceDialog", "classDigikamGenericGoogleServicesPlugin_1_1ReplaceDialog.html", null ],
      [ "DigikamGenericINatPlugin::INatBrowserDlg", "classDigikamGenericINatPlugin_1_1INatBrowserDlg.html", null ],
      [ "DigikamGenericPiwigoPlugin::PiwigoLoginDlg", "classDigikamGenericPiwigoPlugin_1_1PiwigoLoginDlg.html", null ],
      [ "DigikamGenericPresentationPlugin::SoundtrackPreview", "classDigikamGenericPresentationPlugin_1_1SoundtrackPreview.html", null ],
      [ "DigikamGenericPrintCreatorPlugin::AdvPrintCustomLayoutDlg", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg.html", null ],
      [ "DigikamGenericSmugPlugin::SmugNewAlbumDlg", "classDigikamGenericSmugPlugin_1_1SmugNewAlbumDlg.html", null ],
      [ "DigikamGenericVKontaktePlugin::VKNewAlbumDlg", "classDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg.html", null ],
      [ "ShowFoto::ShowfotoFolderViewBookmarkDlg", "classShowFoto_1_1ShowfotoFolderViewBookmarkDlg.html", null ],
      [ "ShowFoto::ShowfotoStackViewFavoriteItemDlg", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html", null ]
    ] ],
    [ "QDockWidget", null, [
      [ "Digikam::ThumbBarDock", "classDigikam_1_1ThumbBarDock.html", null ]
    ] ],
    [ "QDoubleSpinBox", null, [
      [ "Digikam::CustomStepsDoubleSpinBox", "classDigikam_1_1CustomStepsDoubleSpinBox.html", null ]
    ] ],
    [ "QFileDialog", null, [
      [ "Digikam::DFileDialog", "classDigikam_1_1DFileDialog.html", null ]
    ] ],
    [ "QFileIconProvider", null, [
      [ "Digikam::ImageDialogIconProvider", "classDigikam_1_1ImageDialogIconProvider.html", null ]
    ] ],
    [ "QFileSystemModel", null, [
      [ "ShowFoto::ShowfotoFolderViewModel", "classShowFoto_1_1ShowfotoFolderViewModel.html", null ]
    ] ],
    [ "QFrame", null, [
      [ "Digikam::AssignNameWidget", "classDigikam_1_1AssignNameWidget.html", null ],
      [ "Digikam::DDatePicker", "classDigikam_1_1DDatePicker.html", null ],
      [ "Digikam::DHBox", "classDigikam_1_1DHBox.html", [
        [ "Digikam::DDateTimeEdit", "classDigikam_1_1DDateTimeEdit.html", null ],
        [ "Digikam::DFileSelector", "classDigikam_1_1DFileSelector.html", null ],
        [ "Digikam::DFontSelect", "classDigikam_1_1DFontSelect.html", null ],
        [ "Digikam::DVBox", "classDigikam_1_1DVBox.html", [
          [ "Digikam::CaptionEdit", "classDigikam_1_1CaptionEdit.html", null ],
          [ "Digikam::ColorLabelWidget", "classDigikam_1_1ColorLabelWidget.html", [
            [ "Digikam::ColorLabelFilter", "classDigikam_1_1ColorLabelFilter.html", null ]
          ] ],
          [ "Digikam::DateFolderView", "classDigikam_1_1DateFolderView.html", null ],
          [ "Digikam::FilterSideBarWidget", "classDigikam_1_1FilterSideBarWidget.html", null ],
          [ "Digikam::IccProfilesSettings", "classDigikam_1_1IccProfilesSettings.html", null ],
          [ "Digikam::ItemDescEditTab", "classDigikam_1_1ItemDescEditTab.html", null ],
          [ "Digikam::PickLabelWidget", "classDigikam_1_1PickLabelWidget.html", [
            [ "Digikam::PickLabelFilter", "classDigikam_1_1PickLabelFilter.html", null ]
          ] ],
          [ "Digikam::RatingBox", "classDigikam_1_1RatingBox.html", null ],
          [ "Digikam::TransactionItem", "classDigikam_1_1TransactionItem.html", null ],
          [ "ShowFoto::ShowfotoFolderViewBar", "classShowFoto_1_1ShowfotoFolderViewBar.html", null ]
        ] ],
        [ "Digikam::DZoomBar", "classDigikam_1_1DZoomBar.html", null ],
        [ "Digikam::ImportView", "classDigikam_1_1ImportView.html", null ],
        [ "Digikam::ItemIconView", "classDigikam_1_1ItemIconView.html", null ],
        [ "Digikam::OverlayWidget", "classDigikam_1_1OverlayWidget.html", [
          [ "Digikam::ProgressView", "classDigikam_1_1ProgressView.html", null ]
        ] ],
        [ "Digikam::RatingFilter", "classDigikam_1_1RatingFilter.html", null ],
        [ "Digikam::TemplateSelector", "classDigikam_1_1TemplateSelector.html", null ],
        [ "Digikam::TextFilter", "classDigikam_1_1TextFilter.html", null ],
        [ "DigikamGenericSlideShowPlugin::SlideToolBar", "classDigikamGenericSlideShowPlugin_1_1SlideToolBar.html", null ]
      ] ],
      [ "Digikam::DLineWidget", "classDigikam_1_1DLineWidget.html", null ],
      [ "Digikam::DMultiTabBarFrame", "classDigikam_1_1DMultiTabBarFrame.html", null ],
      [ "Digikam::DNotificationPopup", "classDigikam_1_1DNotificationPopup.html", null ],
      [ "Digikam::DNotificationWidget", "classDigikam_1_1DNotificationWidget.html", null ],
      [ "Digikam::DPopupFrame", "classDigikam_1_1DPopupFrame.html", null ],
      [ "Digikam::LightTableView", "classDigikam_1_1LightTableView.html", null ],
      [ "Digikam::PanIconFrame", "classDigikam_1_1PanIconFrame.html", null ],
      [ "Digikam::PlaceholderWidget", "classDigikam_1_1PlaceholderWidget.html", null ],
      [ "Digikam::StatusbarProgressWidget", "classDigikam_1_1StatusbarProgressWidget.html", null ]
    ] ],
    [ "QGraphicsItem", null, [
      [ "Digikam::DSelectionItem", "classDigikam_1_1DSelectionItem.html", null ]
    ] ],
    [ "QGraphicsObject", null, [
      [ "Digikam::ClickDragReleaseItem", "classDigikam_1_1ClickDragReleaseItem.html", null ],
      [ "Digikam::DImgChildItem", "classDigikam_1_1DImgChildItem.html", [
        [ "Digikam::RegionFrameItem", "classDigikam_1_1RegionFrameItem.html", [
          [ "Digikam::FaceItem", "classDigikam_1_1FaceItem.html", null ],
          [ "Digikam::FocusPointItem", "classDigikam_1_1FocusPointItem.html", null ],
          [ "Digikam::RubberItem", "classDigikam_1_1RubberItem.html", null ]
        ] ]
      ] ],
      [ "Digikam::GraphicsDImgItem", "classDigikam_1_1GraphicsDImgItem.html", null ]
    ] ],
    [ "QGraphicsView", null, [
      [ "Digikam::DPreviewImage", "classDigikam_1_1DPreviewImage.html", null ],
      [ "Digikam::GraphicsDImgView", "classDigikam_1_1GraphicsDImgView.html", [
        [ "Digikam::Canvas", "classDigikam_1_1Canvas.html", null ],
        [ "Digikam::ImageRegionWidget", "classDigikam_1_1ImageRegionWidget.html", [
          [ "DigikamEditorHealingCloneToolPlugin::HealingCloneToolWidget", "classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneToolWidget.html", null ]
        ] ],
        [ "Digikam::ImportPreviewView", "classDigikam_1_1ImportPreviewView.html", null ],
        [ "Digikam::ItemPreviewView", "classDigikam_1_1ItemPreviewView.html", [
          [ "Digikam::LightTablePreview", "classDigikam_1_1LightTablePreview.html", null ]
        ] ],
        [ "DigikamRawImportNativePlugin::RawPreview", "classDigikamRawImportNativePlugin_1_1RawPreview.html", null ]
      ] ]
    ] ],
    [ "QGroupBox", null, [
      [ "Digikam::FullScreenSettings", "classDigikam_1_1FullScreenSettings.html", null ],
      [ "DigikamGenericVKontaktePlugin::VKAlbumChooser", "classDigikamGenericVKontaktePlugin_1_1VKAlbumChooser.html", null ],
      [ "DigikamGenericVKontaktePlugin::VKAuthWidget", "classDigikamGenericVKontaktePlugin_1_1VKAuthWidget.html", null ]
    ] ],
    [ "QHash", null, [
      [ "Digikam::DatabaseFields::Hash< QVariant >", "classDigikam_1_1DatabaseFields_1_1Hash.html", null ],
      [ "Digikam::DatabaseFields::Hash< T >", "classDigikam_1_1DatabaseFields_1_1Hash.html", null ]
    ] ],
    [ "QHBoxLayout", null, [
      [ "Digikam::RadioButtonHBox", "classDigikam_1_1RadioButtonHBox.html", null ]
    ] ],
    [ "QItemDelegate", null, [
      [ "Digikam::GPSItemDelegate", "classDigikam_1_1GPSItemDelegate.html", null ],
      [ "Digikam::RatingComboBoxDelegate", "classDigikam_1_1RatingComboBoxDelegate.html", null ],
      [ "Digikam::TableViewItemDelegate", "classDigikam_1_1TableViewItemDelegate.html", null ]
    ] ],
    [ "QItemSelectionModel", null, [
      [ "Digikam::DConfigDlgInternal::SelectionModel", "classDigikam_1_1DConfigDlgInternal_1_1SelectionModel.html", null ],
      [ "Digikam::GPSLinkItemSelectionModel", "classDigikam_1_1GPSLinkItemSelectionModel.html", null ]
    ] ],
    [ "QLabel", null, [
      [ "Digikam::DActiveLabel", "classDigikam_1_1DActiveLabel.html", null ],
      [ "Digikam::DAdjustableLabel", "classDigikam_1_1DAdjustableLabel.html", [
        [ "Digikam::DSqueezedClickLabel", "classDigikam_1_1DSqueezedClickLabel.html", null ],
        [ "Digikam::DTextLabelName", "classDigikam_1_1DTextLabelName.html", null ],
        [ "Digikam::DTextLabelValue", "classDigikam_1_1DTextLabelValue.html", null ]
      ] ],
      [ "Digikam::DClickLabel", "classDigikam_1_1DClickLabel.html", null ],
      [ "Digikam::DCursorTracker", "classDigikam_1_1DCursorTracker.html", null ],
      [ "Digikam::DItemToolTip", "classDigikam_1_1DItemToolTip.html", [
        [ "Digikam::BlackFrameToolTip", "classDigikam_1_1BlackFrameToolTip.html", null ],
        [ "Digikam::FreeSpaceToolTip", "classDigikam_1_1FreeSpaceToolTip.html", null ],
        [ "Digikam::ImageDialogToolTip", "classDigikam_1_1ImageDialogToolTip.html", null ],
        [ "Digikam::ItemViewToolTip", "classDigikam_1_1ItemViewToolTip.html", null ],
        [ "Digikam::QueueToolTip", "classDigikam_1_1QueueToolTip.html", null ],
        [ "ShowFoto::ShowfotoFolderViewToolTip", "classShowFoto_1_1ShowfotoFolderViewToolTip.html", null ],
        [ "ShowFoto::ShowfotoStackViewToolTip", "classShowFoto_1_1ShowfotoStackViewToolTip.html", null ]
      ] ],
      [ "Digikam::EffectPreview", "classDigikam_1_1EffectPreview.html", null ],
      [ "Digikam::TransitionPreview", "classDigikam_1_1TransitionPreview.html", null ],
      [ "Digikam::WorkingWidget", "classDigikam_1_1WorkingWidget.html", null ]
    ] ],
    [ "QLayout", null, [
      [ "Digikam::DynamicLayout", "classDigikam_1_1DynamicLayout.html", null ]
    ] ],
    [ "QLineEdit", null, [
      [ "Digikam::AddTagsLineEdit", "classDigikam_1_1AddTagsLineEdit.html", null ],
      [ "Digikam::DatePickerYearSelector", "classDigikam_1_1DatePickerYearSelector.html", null ],
      [ "Digikam::ProxyLineEdit", "classDigikam_1_1ProxyLineEdit.html", [
        [ "Digikam::ProxyClickLineEdit", "classDigikam_1_1ProxyClickLineEdit.html", null ]
      ] ],
      [ "Digikam::SearchTextBar", "classDigikam_1_1SearchTextBar.html", null ],
      [ "DigikamGenericINatPlugin::TaxonEdit", "classDigikamGenericINatPlugin_1_1TaxonEdit.html", null ]
    ] ],
    [ "QList< T >", "classQList.html", null ],
    [ "QList< AbstractThemeParameter * >", "classQList.html", null ],
    [ "QList< Album * >", "classQList.html", null ],
    [ "QList< AlbumPointer< Album > >", "classQList.html", [
      [ "Digikam::AlbumPointerList< T >", "classDigikam_1_1AlbumPointerList.html", null ]
    ] ],
    [ "QList< AnimationControl * >", "classQList.html", null ],
    [ "QList< BatchTool * >", "classQList.html", null ],
    [ "QList< BatchToolSet >", "classQList.html", null ],
    [ "QList< bool >", "classQList.html", null ],
    [ "QList< CameraCommand * >", "classQList.html", null ],
    [ "QList< CamItemInfo >", "classQList.html", null ],
    [ "QList< CHUpdateItem >", "classQList.html", null ],
    [ "QList< Correlation >", "classQList.html", null ],
    [ "QList< CropHandle >", "classQList.html", null ],
    [ "QList< DateFormatDescriptor >", "classQList.html", null ],
    [ "QList< Digikam::AbstractAlbumTreeView::ContextMenuElement * >", "classQList.html", null ],
    [ "QList< Digikam::Album * >", "classQList.html", null ],
    [ "QList< Digikam::AlbumShortInfo >", "classQList.html", null ],
    [ "QList< Digikam::BatchTool * >", "classQList.html", null ],
    [ "QList< Digikam::BookmarkNode * >", "classQList.html", null ],
    [ "QList< Digikam::CameraType * >", "classQList.html", null ],
    [ "QList< Digikam::CamItemInfo >", "classQList.html", null ],
    [ "QList< Digikam::ChoiceSearchModel::Entry >", "classQList.html", null ],
    [ "QList< Digikam::CommentInfo >", "classQList.html", null ],
    [ "QList< Digikam::CopyrightInfo >", "classQList.html", null ],
    [ "QList< Digikam::DbEngineActionElement >", "classQList.html", null ],
    [ "QList< Digikam::DImageHistory::Entry >", "classQList.html", null ],
    [ "QList< Digikam::DLabelExpander * >", "classQList.html", null ],
    [ "QList< Digikam::DMultiTabBarButton * >", "classQList.html", null ],
    [ "QList< Digikam::DMultiTabBarTab * >", "classQList.html", null ],
    [ "QList< Digikam::DownloadInfo >", "classQList.html", null ],
    [ "QList< Digikam::DPlugin * >", "classQList.html", null ],
    [ "QList< Digikam::DPluginAction * >", "classQList.html", null ],
    [ "QList< Digikam::EditorCore::Private::FileToSave >", "classQList.html", null ],
    [ "QList< Digikam::ExifToolProcess::Private::Command >", "classQList.html", null ],
    [ "QList< Digikam::FaceItem * >", "classQList.html", null ],
    [ "QList< Digikam::Filter * >", "classQList.html", null ],
    [ "QList< Digikam::FilterAction >", "classQList.html", null ],
    [ "QList< Digikam::FocusPointItem * >", "classQList.html", null ],
    [ "QList< Digikam::GeoIfaceInternalWidgetInfo >", "classQList.html", null ],
    [ "QList< Digikam::GeoModelHelper * >", "classQList.html", null ],
    [ "QList< Digikam::GPSItemContainer * >", "classQList.html", null ],
    [ "QList< Digikam::GPSItemInfo >", "classQList.html", null ],
    [ "QList< Digikam::Graph::Vertex >", "classQList.html", null ],
    [ "QList< Digikam::HistoryImageId >", "classQList.html", null ],
    [ "QList< Digikam::HotPixelProps >", "classQList.html", null ],
    [ "QList< Digikam::HotPixelsWeights >", "classQList.html", null ],
    [ "QList< Digikam::IccProfile >", "classQList.html", null ],
    [ "QList< Digikam::Identity >", "classQList.html", null ],
    [ "QList< Digikam::ImageQualityCalculator::ResultDetection >", "classQList.html", null ],
    [ "QList< Digikam::ImageQualityThread * >", "classQList.html", null ],
    [ "QList< Digikam::ItemDelegateOverlay * >", "classQList.html", null ],
    [ "QList< Digikam::ItemFilterModelPrepareHook * >", "classQList.html", null ],
    [ "QList< Digikam::ItemFiltersHistoryTreeItem * >", "classQList.html", null ],
    [ "QList< Digikam::ItemInfo >", "classQList.html", null ],
    [ "QList< Digikam::ItemListerRecord >", "classQList.html", null ],
    [ "QList< Digikam::ItemQueryPostHook * >", "classQList.html", null ],
    [ "QList< Digikam::ListItem * >", "classQList.html", null ],
    [ "QList< Digikam::LoadingDescription >", "classQList.html", null ],
    [ "QList< Digikam::LoadingProcessListener * >", "classQList.html", null ],
    [ "QList< Digikam::LoadSaveTask * >", "classQList.html", null ],
    [ "QList< Digikam::MapBackend * >", "classQList.html", null ],
    [ "QList< Digikam::PageItem * >", "classQList.html", null ],
    [ "QList< Digikam::PTOType::ControlPoint >", "classQList.html", null ],
    [ "QList< Digikam::PTOType::Mask >", "classQList.html", null ],
    [ "QList< Digikam::PTOType::Optimization >", "classQList.html", null ],
    [ "QList< Digikam::RGBackend * >", "classQList.html", null ],
    [ "QList< Digikam::RGInfo >", "classQList.html", null ],
    [ "QList< Digikam::SearchField * >", "classQList.html", null ],
    [ "QList< Digikam::SearchFieldGroup * >", "classQList.html", null ],
    [ "QList< Digikam::SearchFieldGroupLabel * >", "classQList.html", null ],
    [ "QList< Digikam::SearchGroup * >", "classQList.html", null ],
    [ "QList< Digikam::SetupCollectionModel::Item >", "classQList.html", null ],
    [ "QList< Digikam::Sidebar * >", "classQList.html", null ],
    [ "QList< Digikam::SidebarWidget * >", "classQList.html", null ],
    [ "QList< Digikam::SimpleTreeModel::Item * >", "classQList.html", null ],
    [ "QList< Digikam::SolidVolumeInfo >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumn * >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumnConfiguration >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumnDescription >", "classQList.html", null ],
    [ "QList< Digikam::TableViewColumnProfile >", "classQList.html", null ],
    [ "QList< Digikam::TableViewModel::Item * >", "classQList.html", null ],
    [ "QList< Digikam::TaggingAction >", "classQList.html", null ],
    [ "QList< Digikam::TagProperty >", "classQList.html", null ],
    [ "QList< Digikam::TagShortInfo >", "classQList.html", null ],
    [ "QList< Digikam::TAlbum * >", "classQList.html", null ],
    [ "QList< Digikam::Template >", "classQList.html", null ],
    [ "QList< Digikam::ThumbnailImageCatcher::Private::CatcherResult >", "classQList.html", null ],
    [ "QList< Digikam::ThumbnailLoadThread * >", "classQList.html", null ],
    [ "QList< Digikam::TileIndex >", "classQList.html", null ],
    [ "QList< Digikam::TrackManager::TrackPoint >", "classQList.html", null ],
    [ "QList< Digikam::TreeBranch * >", "classQList.html", null ],
    [ "QList< Digikam::UndoAction * >", "classQList.html", null ],
    [ "QList< Digikam::VisibilityObject * >", "classQList.html", null ],
    [ "QList< Digikam::WorkerObject * >", "classQList.html", null ],
    [ "QList< Digikam::Workflow >", "classQList.html", null ],
    [ "QList< DigikamGenericFlickrPlugin::FPhotoSet >", "classQList.html", null ],
    [ "QList< DigikamGenericGeolocationEditPlugin::SearchResultModel::SearchResultItem >", "classQList.html", null ],
    [ "QList< DigikamGenericGoogleServicesPlugin::GSFolder >", "classQList.html", null ],
    [ "QList< DigikamGenericGoogleServicesPlugin::GSPhoto >", "classQList.html", null ],
    [ "QList< DigikamGenericINatPlugin::Taxon >", "classQList.html", null ],
    [ "QList< DigikamGenericPrintCreatorPlugin::AdvPrintPhoto * >", "classQList.html", null ],
    [ "QList< DigikamGenericPrintCreatorPlugin::AdvPrintPhotoSize * >", "classQList.html", null ],
    [ "QList< DigikamGenericYFPlugin::YandexFotkiAlbum >", "classQList.html", null ],
    [ "QList< DigikamGenericYFPlugin::YFPhoto >", "classQList.html", null ],
    [ "QList< double >", "classQList.html", null ],
    [ "QList< DPluginAction * >", "classQList.html", null ],
    [ "QList< DPluginCB * >", "classQList.html", null ],
    [ "QList< DrawEvent >", "classQList.html", null ],
    [ "QList< DTrashItemInfo >", "classQList.html", null ],
    [ "QList< Exiv2::PreviewProperties >", "classQList.html", null ],
    [ "QList< FacePipelineExtendedPackage::Ptr >", "classQList.html", [
      [ "Digikam::PackageLoadingDescriptionList", "classDigikam_1_1PackageLoadingDescriptionList.html", null ]
    ] ],
    [ "QList< FacePipelineFaceTagsIface >", "classQList.html", [
      [ "Digikam::FacePipelineFaceTagsIfaceList", "classDigikam_1_1FacePipelineFaceTagsIfaceList.html", null ]
    ] ],
    [ "QList< Filter * >", "classQList.html", null ],
    [ "QList< FocusPoint >", "classQList.html", null ],
    [ "QList< GeoIfaceCluster >", "classQList.html", null ],
    [ "QList< GeonamesInternalJobs >", "classQList.html", null ],
    [ "QList< GeonamesUSInternalJobs >", "classQList.html", null ],
    [ "QList< GPSItemInfo >", "classQList.html", null ],
    [ "QList< HistoryItem >", "classQList.html", null ],
    [ "QList< HistoryTreeItem * >", "classQList.html", null ],
    [ "QList< ImgFilterPtr >", "classQList.html", null ],
    [ "QList< int >", "classQList.html", null ],
    [ "QList< InternalJobs >", "classQList.html", null ],
    [ "QList< ItemInfo >", "classQList.html", [
      [ "Digikam::FileActionItemInfoList", "classDigikam_1_1FileActionItemInfoList.html", [
        [ "Digikam::ItemInfoTaskSplitter", "classDigikam_1_1ItemInfoTaskSplitter.html", null ]
      ] ],
      [ "Digikam::ItemInfoList", "classDigikam_1_1ItemInfoList.html", null ]
    ] ],
    [ "QList< KActionCollection * >", "classQList.html", null ],
    [ "QList< KeyString >", "classQList.html", null ],
    [ "QList< KJob * >", "classQList.html", null ],
    [ "QList< MergedRequests >", "classQList.html", null ],
    [ "QList< NewNameInfo >", "classQList.html", null ],
    [ "QList< OsmInternalJobs >", "classQList.html", null ],
    [ "QList< QAction * >", "classQList.html", null ],
    [ "QList< QByteArray >", "classQList.html", null ],
    [ "QList< QDateTime >", "classQList.html", null ],
    [ "QList< QEventLoop * >", "classQList.html", null ],
    [ "QList< QImage * >", "classQList.html", null ],
    [ "QList< QLayoutItem * >", "classQList.html", null ],
    [ "QList< QList >", "classQList.html", null ],
    [ "QList< QList< Digikam::TagData > >", "classQList.html", null ],
    [ "QList< QList< QWidget * > >", "classQList.html", null ],
    [ "QList< qlonglong >", "classQList.html", null ],
    [ "QList< QMap< int, QVariant > >", "classQList.html", null ],
    [ "QList< QMetaMethod >", "classQList.html", null ],
    [ "QList< QMetaObject::Connection >", "classQList.html", null ],
    [ "QList< QModelIndex >", "classQList.html", null ],
    [ "QList< QNetworkCookie >", "classQList.html", null ],
    [ "QList< QObject * >", "classQList.html", null ],
    [ "QList< QPair< Digikam::GeoCoordinates, QList > >", "classQList.html", null ],
    [ "QList< QPair< Digikam::TileIndex, Digikam::TileIndex > >", "classQList.html", null ],
    [ "QList< QPair< QString, int > >", "classQList.html", null ],
    [ "QList< QPair< QString, QString > >", "classQList.html", null ],
    [ "QList< QPair< QUrl, DigikamGenericFlickrPlugin::FPhotoInfo > >", "classQList.html", null ],
    [ "QList< QPair< QUrl, DigikamGenericGoogleServicesPlugin::GSPhoto > >", "classQList.html", null ],
    [ "QList< QPair< QUrl, QString > >", "classQList.html", null ],
    [ "QList< QPersistentModelIndex >", "classQList.html", null ],
    [ "QList< QPoint >", "classQList.html", null ],
    [ "QList< QPointer< const QAbstractProxyModel > >", "classQList.html", null ],
    [ "QList< QPointer< Digikam::MapWidget > >", "classQList.html", null ],
    [ "QList< QPrinterInfo >", "classQList.html", null ],
    [ "QList< QRect * >", "classQList.html", null ],
    [ "QList< QRectF >", "classQList.html", null ],
    [ "QList< QRegularExpression >", "classQList.html", null ],
    [ "QList< QStandardItemModel * >", "classQList.html", null ],
    [ "QList< QString >", "classQList.html", null ],
    [ "QList< QTcpSocket * >", "classQList.html", null ],
    [ "QList< QToolButton * >", "classQList.html", null ],
    [ "QList< QUrl >", "classQList.html", null ],
    [ "QList< QVariant >", "classQList.html", null ],
    [ "QList< RatingComboBox::RatingValue >", "classQList.html", null ],
    [ "QList< Rule * >", "classQList.html", null ],
    [ "QList< SearchResult >", "classQList.html", null ],
    [ "QList< ShowfotoItemInfo >", "classQList.html", null ],
    [ "QList< Task * >", "classQList.html", null ],
    [ "QList< TaxonAndFlags >", "classQList.html", null ],
    [ "QList< TileIndex >", "classQList.html", null ],
    [ "QList< TodoPair >", "classQList.html", null ],
    [ "QList< Token * >", "classQList.html", null ],
    [ "QList< Track >", "classQList.html", null ],
    [ "QList< TrackManager::TrackChanges >", "classQList.html", null ],
    [ "QList< Type >", "classQList.html", null ],
    [ "QList< UndoInfo >", "classQList.html", null ],
    [ "QList< VertexItem * >", "classQList.html", null ],
    [ "QListView", null, [
      [ "Digikam::DCategorizedView", "classDigikam_1_1DCategorizedView.html", [
        [ "Digikam::ActionCategorizedView", "classDigikam_1_1ActionCategorizedView.html", null ],
        [ "Digikam::ItemViewCategorized", "classDigikam_1_1ItemViewCategorized.html", null ]
      ] ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgListView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgListView.html", null ],
      [ "Digikam::NamespaceListView", "classDigikam_1_1NamespaceListView.html", null ]
    ] ],
    [ "QListWidget", null, [
      [ "Digikam::DTextList", "classDigikam_1_1DTextList.html", null ],
      [ "Digikam::PreviewList", "classDigikam_1_1PreviewList.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAudioList", "classDigikamGenericPresentationPlugin_1_1PresentationAudioList.html", null ]
    ] ],
    [ "QListWidgetItem", null, [
      [ "Digikam::FilmContainer::ListItem", "classDigikam_1_1FilmContainer_1_1ListItem.html", null ],
      [ "Digikam::PreviewListItem", "classDigikam_1_1PreviewListItem.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::ThemeListBoxItem", "classDigikamGenericHtmlGalleryPlugin_1_1ThemeListBoxItem.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAudioListItem", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "Digikam::TagsManager", "classDigikam_1_1TagsManager.html", null ]
    ] ],
    [ "QMap", null, [
      [ "Digikam::QMapForAdaptors< Vertex, int >", "classDigikam_1_1QMapForAdaptors.html", null ],
      [ "Digikam::QMapForAdaptors< Vertex, Vertex >", "classDigikam_1_1QMapForAdaptors.html", null ],
      [ "Digikam::CaptionsMap", "classDigikam_1_1CaptionsMap.html", null ],
      [ "Digikam::QMapForAdaptors< Key, Value >", "classDigikam_1_1QMapForAdaptors.html", null ]
    ] ],
    [ "QMenu", null, [
      [ "Digikam::ColorLabelMenuAction", "classDigikam_1_1ColorLabelMenuAction.html", null ],
      [ "Digikam::DDatePickerPopup", "classDigikam_1_1DDatePickerPopup.html", null ],
      [ "Digikam::IccProfilesMenuAction", "classDigikam_1_1IccProfilesMenuAction.html", null ],
      [ "Digikam::ModelMenu", "classDigikam_1_1ModelMenu.html", [
        [ "Digikam::BookmarksMenu", "classDigikam_1_1BookmarksMenu.html", null ]
      ] ],
      [ "Digikam::PickLabelMenuAction", "classDigikam_1_1PickLabelMenuAction.html", null ],
      [ "Digikam::RatingMenuAction", "classDigikam_1_1RatingMenuAction.html", null ],
      [ "Digikam::TagsPopupMenu", "classDigikam_1_1TagsPopupMenu.html", null ]
    ] ],
    [ "QMimeData", null, [
      [ "Digikam::DAlbumDrag", "classDigikam_1_1DAlbumDrag.html", null ],
      [ "Digikam::DCameraDragObject", "classDigikam_1_1DCameraDragObject.html", null ],
      [ "Digikam::DCameraItemListDrag", "classDigikam_1_1DCameraItemListDrag.html", null ],
      [ "Digikam::DItemDrag", "classDigikam_1_1DItemDrag.html", null ],
      [ "Digikam::DTagListDrag", "classDigikam_1_1DTagListDrag.html", null ],
      [ "Digikam::MapDragData", "classDigikam_1_1MapDragData.html", null ]
    ] ],
    [ "QObject", null, [
      [ "Digikam::AbstractItemDragDropHandler", "classDigikam_1_1AbstractItemDragDropHandler.html", [
        [ "Digikam::ImportDragDropHandler", "classDigikam_1_1ImportDragDropHandler.html", null ],
        [ "Digikam::ItemDragDropHandler", "classDigikam_1_1ItemDragDropHandler.html", null ],
        [ "ShowFoto::ShowfotoDragDropHandler", "classShowFoto_1_1ShowfotoDragDropHandler.html", null ]
      ] ],
      [ "Digikam::AbstractMarkerTiler", "classDigikam_1_1AbstractMarkerTiler.html", [
        [ "Digikam::GPSMarkerTiler", "classDigikam_1_1GPSMarkerTiler.html", null ],
        [ "Digikam::ItemMarkerTiler", "classDigikam_1_1ItemMarkerTiler.html", null ]
      ] ],
      [ "Digikam::ActionJob", "classDigikam_1_1ActionJob.html", [
        [ "Digikam::DBJob", "classDigikam_1_1DBJob.html", [
          [ "Digikam::AlbumsJob", "classDigikam_1_1AlbumsJob.html", null ],
          [ "Digikam::DatesJob", "classDigikam_1_1DatesJob.html", null ],
          [ "Digikam::GPSJob", "classDigikam_1_1GPSJob.html", null ],
          [ "Digikam::SearchesJob", "classDigikam_1_1SearchesJob.html", null ],
          [ "Digikam::TagsJob", "classDigikam_1_1TagsJob.html", null ]
        ] ],
        [ "Digikam::DatabaseTask", "classDigikam_1_1DatabaseTask.html", null ],
        [ "Digikam::FingerprintsTask", "classDigikam_1_1FingerprintsTask.html", null ],
        [ "Digikam::IOJob", "classDigikam_1_1IOJob.html", [
          [ "Digikam::CopyOrMoveJob", "classDigikam_1_1CopyOrMoveJob.html", null ],
          [ "Digikam::DTrashItemsListingJob", "classDigikam_1_1DTrashItemsListingJob.html", null ],
          [ "Digikam::DeleteJob", "classDigikam_1_1DeleteJob.html", null ],
          [ "Digikam::EmptyDTrashItemsJob", "classDigikam_1_1EmptyDTrashItemsJob.html", null ],
          [ "Digikam::RenameFileJob", "classDigikam_1_1RenameFileJob.html", null ],
          [ "Digikam::RestoreDTrashItemsJob", "classDigikam_1_1RestoreDTrashItemsJob.html", null ]
        ] ],
        [ "Digikam::ImageQualityTask", "classDigikam_1_1ImageQualityTask.html", null ],
        [ "Digikam::MetadataTask", "classDigikam_1_1MetadataTask.html", null ],
        [ "Digikam::Task", "classDigikam_1_1Task.html", null ],
        [ "Digikam::ThumbsTask", "classDigikam_1_1ThumbsTask.html", null ],
        [ "Digikam::VidSlideTask", "classDigikam_1_1VidSlideTask.html", null ],
        [ "DigikamGenericDNGConverterPlugin::DNGConverterTask", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html", null ],
        [ "DigikamGenericFileCopyPlugin::FCTask", "classDigikamGenericFileCopyPlugin_1_1FCTask.html", null ],
        [ "DigikamGenericMjpegStreamPlugin::MjpegFrameTask", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintTask", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintTask.html", null ],
        [ "DigikamGenericSendByMailPlugin::ImageResizeJob", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html", null ],
        [ "DigikamGenericTimeAdjustPlugin::TimeAdjustTask", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html", null ],
        [ "DigikamGenericTimeAdjustPlugin::TimePreviewTask", "classDigikamGenericTimeAdjustPlugin_1_1TimePreviewTask.html", null ]
      ] ],
      [ "Digikam::AdvancedRenameManager", "classDigikam_1_1AdvancedRenameManager.html", null ],
      [ "Digikam::AkonadiIface", "classDigikam_1_1AkonadiIface.html", null ],
      [ "Digikam::AlbumHistory", "classDigikam_1_1AlbumHistory.html", null ],
      [ "Digikam::AlbumLabelsSearchHandler", "classDigikam_1_1AlbumLabelsSearchHandler.html", null ],
      [ "Digikam::AlbumManager", "classDigikam_1_1AlbumManager.html", null ],
      [ "Digikam::AlbumModelDragDropHandler", "classDigikam_1_1AlbumModelDragDropHandler.html", [
        [ "Digikam::AlbumDragDropHandler", "classDigikam_1_1AlbumDragDropHandler.html", null ],
        [ "Digikam::TagDragDropHandler", "classDigikam_1_1TagDragDropHandler.html", null ]
      ] ],
      [ "Digikam::AlbumModificationHelper", "classDigikam_1_1AlbumModificationHelper.html", null ],
      [ "Digikam::AlbumThumbnailLoader", "classDigikam_1_1AlbumThumbnailLoader.html", null ],
      [ "Digikam::AlbumWatch", "classDigikam_1_1AlbumWatch.html", null ],
      [ "Digikam::ApplicationSettings", "classDigikam_1_1ApplicationSettings.html", null ],
      [ "Digikam::BalooWrap", "classDigikam_1_1BalooWrap.html", null ],
      [ "Digikam::BatchTool", "classDigikam_1_1BatchTool.html", [
        [ "Digikam::Restoration", "classDigikam_1_1Restoration.html", null ],
        [ "DigikamBqmAntiVignettingPlugin::AntiVignetting", "classDigikamBqmAntiVignettingPlugin_1_1AntiVignetting.html", null ],
        [ "DigikamBqmAssignTemplatePlugin::AssignTemplate", "classDigikamBqmAssignTemplatePlugin_1_1AssignTemplate.html", null ],
        [ "DigikamBqmAutoCorrectionPlugin::AutoCorrection", "classDigikamBqmAutoCorrectionPlugin_1_1AutoCorrection.html", null ],
        [ "DigikamBqmBCGCorrectionPlugin::BCGCorrection", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrection.html", null ],
        [ "DigikamBqmBWConvertPlugin::BWConvert", "classDigikamBqmBWConvertPlugin_1_1BWConvert.html", null ],
        [ "DigikamBqmBlurPlugin::Blur", "classDigikamBqmBlurPlugin_1_1Blur.html", null ],
        [ "DigikamBqmBorderPlugin::Border", "classDigikamBqmBorderPlugin_1_1Border.html", null ],
        [ "DigikamBqmChannelMixerPlugin::ChannelMixer", "classDigikamBqmChannelMixerPlugin_1_1ChannelMixer.html", null ],
        [ "DigikamBqmColorBalancePlugin::ColorBalance", "classDigikamBqmColorBalancePlugin_1_1ColorBalance.html", null ],
        [ "DigikamBqmColorFXPlugin::ColorFX", "classDigikamBqmColorFXPlugin_1_1ColorFX.html", null ],
        [ "DigikamBqmConvert16To8Plugin::Convert16to8", "classDigikamBqmConvert16To8Plugin_1_1Convert16to8.html", null ],
        [ "DigikamBqmConvert8To16Plugin::Convert8to16", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html", null ],
        [ "DigikamBqmConvertToDngPlugin::ConvertToDNG", "classDigikamBqmConvertToDngPlugin_1_1ConvertToDNG.html", null ],
        [ "DigikamBqmConvertToHeifPlugin::ConvertToHEIF", "classDigikamBqmConvertToHeifPlugin_1_1ConvertToHEIF.html", null ],
        [ "DigikamBqmConvertToJp2Plugin::ConvertToJP2", "classDigikamBqmConvertToJp2Plugin_1_1ConvertToJP2.html", null ],
        [ "DigikamBqmConvertToJpegPlugin::ConvertToJPEG", "classDigikamBqmConvertToJpegPlugin_1_1ConvertToJPEG.html", null ],
        [ "DigikamBqmConvertToPgfPlugin::ConvertToPGF", "classDigikamBqmConvertToPgfPlugin_1_1ConvertToPGF.html", null ],
        [ "DigikamBqmConvertToPngPlugin::ConvertToPNG", "classDigikamBqmConvertToPngPlugin_1_1ConvertToPNG.html", null ],
        [ "DigikamBqmConvertToTiffPlugin::ConvertToTIFF", "classDigikamBqmConvertToTiffPlugin_1_1ConvertToTIFF.html", null ],
        [ "DigikamBqmCropPlugin::Crop", "classDigikamBqmCropPlugin_1_1Crop.html", null ],
        [ "DigikamBqmCurvesAdjustPlugin::CurvesAdjust", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjust.html", null ],
        [ "DigikamBqmFilmGrainPlugin::FilmGrain", "classDigikamBqmFilmGrainPlugin_1_1FilmGrain.html", null ],
        [ "DigikamBqmFlipPlugin::Flip", "classDigikamBqmFlipPlugin_1_1Flip.html", null ],
        [ "DigikamBqmHSLCorrectionPlugin::HSLCorrection", "classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrection.html", null ],
        [ "DigikamBqmHotPixelsPlugin::HotPixels", "classDigikamBqmHotPixelsPlugin_1_1HotPixels.html", null ],
        [ "DigikamBqmIccConvertPlugin::IccConvert", "classDigikamBqmIccConvertPlugin_1_1IccConvert.html", null ],
        [ "DigikamBqmInvertPlugin::Invert", "classDigikamBqmInvertPlugin_1_1Invert.html", null ],
        [ "DigikamBqmLensAutoFixPlugin::LensAutoFix", "classDigikamBqmLensAutoFixPlugin_1_1LensAutoFix.html", null ],
        [ "DigikamBqmLocalContrastPlugin::LocalContrast", "classDigikamBqmLocalContrastPlugin_1_1LocalContrast.html", null ],
        [ "DigikamBqmNoiseReductionPlugin::NoiseReduction", "classDigikamBqmNoiseReductionPlugin_1_1NoiseReduction.html", null ],
        [ "DigikamBqmRemoveMetadataPlugin::RemoveMetadata", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadata.html", null ],
        [ "DigikamBqmResizePlugin::Resize", "classDigikamBqmResizePlugin_1_1Resize.html", null ],
        [ "DigikamBqmRestorationPlugin::RedEyeCorrection", "classDigikamBqmRestorationPlugin_1_1RedEyeCorrection.html", null ],
        [ "DigikamBqmRotatePlugin::Rotate", "classDigikamBqmRotatePlugin_1_1Rotate.html", null ],
        [ "DigikamBqmSharpenPlugin::Sharpen", "classDigikamBqmSharpenPlugin_1_1Sharpen.html", null ],
        [ "DigikamBqmTexturePlugin::Texture", "classDigikamBqmTexturePlugin_1_1Texture.html", null ],
        [ "DigikamBqmTimeAdjustPlugin::TimeAdjust", "classDigikamBqmTimeAdjustPlugin_1_1TimeAdjust.html", null ],
        [ "DigikamBqmUserScriptPlugin::UserScript", "classDigikamBqmUserScriptPlugin_1_1UserScript.html", null ],
        [ "DigikamBqmWatermarkPlugin::WaterMark", "classDigikamBqmWatermarkPlugin_1_1WaterMark.html", null ],
        [ "DigikamBqmWhiteBalancePlugin::WhiteBalance", "classDigikamBqmWhiteBalancePlugin_1_1WhiteBalance.html", null ]
      ] ],
      [ "Digikam::BatchToolsFactory", "classDigikam_1_1BatchToolsFactory.html", null ],
      [ "Digikam::BdEngineBackend", "classDigikam_1_1BdEngineBackend.html", [
        [ "Digikam::CoreDbBackend", "classDigikam_1_1CoreDbBackend.html", null ],
        [ "Digikam::FaceDbBackend", "classDigikam_1_1FaceDbBackend.html", null ],
        [ "Digikam::SimilarityDbBackend", "classDigikam_1_1SimilarityDbBackend.html", null ],
        [ "Digikam::ThumbsDbBackend", "classDigikam_1_1ThumbsDbBackend.html", null ]
      ] ],
      [ "Digikam::BlackFrameListViewItem", "classDigikam_1_1BlackFrameListViewItem.html", null ],
      [ "Digikam::BlackFrameParser", "classDigikam_1_1BlackFrameParser.html", null ],
      [ "Digikam::BookmarkNode", "classDigikam_1_1BookmarkNode.html", null ],
      [ "Digikam::BookmarksManager", "classDigikam_1_1BookmarksManager.html", null ],
      [ "Digikam::CameraList", "classDigikam_1_1CameraList.html", null ],
      [ "Digikam::CameraThumbsCtrl", "classDigikam_1_1CameraThumbsCtrl.html", null ],
      [ "Digikam::CollectionManager", "classDigikam_1_1CollectionManager.html", null ],
      [ "Digikam::CollectionScanner", "classDigikam_1_1CollectionScanner.html", null ],
      [ "Digikam::ContextMenuHelper", "classDigikam_1_1ContextMenuHelper.html", null ],
      [ "Digikam::CoreDbCopyManager", "classDigikam_1_1CoreDbCopyManager.html", null ],
      [ "Digikam::CoreDbWatch", "classDigikam_1_1CoreDbWatch.html", null ],
      [ "Digikam::DAboutData", "classDigikam_1_1DAboutData.html", null ],
      [ "Digikam::DBJobsManager", "classDigikam_1_1DBJobsManager.html", null ],
      [ "Digikam::DBinaryIface", "classDigikam_1_1DBinaryIface.html", [
        [ "Digikam::ExifToolBinary", "classDigikam_1_1ExifToolBinary.html", null ],
        [ "Digikam::MysqlAdminBinary", "classDigikam_1_1MysqlAdminBinary.html", null ],
        [ "Digikam::MysqlInitBinary", "classDigikam_1_1MysqlInitBinary.html", null ],
        [ "Digikam::MysqlServBinary", "classDigikam_1_1MysqlServBinary.html", null ],
        [ "Digikam::OutlookBinary", "classDigikam_1_1OutlookBinary.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::AlignBinary", "classDigikamGenericExpoBlendingPlugin_1_1AlignBinary.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::EnfuseBinary", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseBinary.html", null ],
        [ "DigikamGenericJAlbumPlugin::JalbumJar", "classDigikamGenericJAlbumPlugin_1_1JalbumJar.html", null ],
        [ "DigikamGenericJAlbumPlugin::JalbumJava", "classDigikamGenericJAlbumPlugin_1_1JalbumJava.html", null ],
        [ "DigikamGenericPanoramaPlugin::AutoOptimiserBinary", "classDigikamGenericPanoramaPlugin_1_1AutoOptimiserBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::CPCleanBinary", "classDigikamGenericPanoramaPlugin_1_1CPCleanBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::CPFindBinary", "classDigikamGenericPanoramaPlugin_1_1CPFindBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::EnblendBinary", "classDigikamGenericPanoramaPlugin_1_1EnblendBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::HuginExecutorBinary", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::MakeBinary", "classDigikamGenericPanoramaPlugin_1_1MakeBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::NonaBinary", "classDigikamGenericPanoramaPlugin_1_1NonaBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoModifyBinary", "classDigikamGenericPanoramaPlugin_1_1PanoModifyBinary.html", null ],
        [ "DigikamGenericPanoramaPlugin::Pto2MkBinary", "classDigikamGenericPanoramaPlugin_1_1Pto2MkBinary.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::GimpBinary", "classDigikamGenericPrintCreatorPlugin_1_1GimpBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::BalsaBinary", "classDigikamGenericSendByMailPlugin_1_1BalsaBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::ClawsMailBinary", "classDigikamGenericSendByMailPlugin_1_1ClawsMailBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::EvolutionBinary", "classDigikamGenericSendByMailPlugin_1_1EvolutionBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::KmailBinary", "classDigikamGenericSendByMailPlugin_1_1KmailBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::NetscapeBinary", "classDigikamGenericSendByMailPlugin_1_1NetscapeBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::SylpheedBinary", "classDigikamGenericSendByMailPlugin_1_1SylpheedBinary.html", null ],
        [ "DigikamGenericSendByMailPlugin::ThunderbirdBinary", "classDigikamGenericSendByMailPlugin_1_1ThunderbirdBinary.html", null ]
      ] ],
      [ "Digikam::DCategoryDrawer", "classDigikam_1_1DCategoryDrawer.html", [
        [ "Digikam::ImportCategoryDrawer", "classDigikam_1_1ImportCategoryDrawer.html", null ],
        [ "Digikam::ItemCategoryDrawer", "classDigikam_1_1ItemCategoryDrawer.html", null ]
      ] ],
      [ "Digikam::DConfigDlgMngr", "classDigikam_1_1DConfigDlgMngr.html", null ],
      [ "Digikam::DConfigDlgWdgItem", "classDigikam_1_1DConfigDlgWdgItem.html", null ],
      [ "Digikam::DDateTable::Private", "classDigikam_1_1DDateTable_1_1Private.html", null ],
      [ "Digikam::DIO", "classDigikam_1_1DIO.html", null ],
      [ "Digikam::DInfoInterface", "classDigikam_1_1DInfoInterface.html", [
        [ "Digikam::DBInfoIface", "classDigikam_1_1DBInfoIface.html", null ],
        [ "Digikam::DMetaInfoIface", "classDigikam_1_1DMetaInfoIface.html", [
          [ "ShowFoto::ShowfotoInfoIface", "classShowFoto_1_1ShowfotoInfoIface.html", null ]
        ] ]
      ] ],
      [ "Digikam::DKCamera", "classDigikam_1_1DKCamera.html", [
        [ "Digikam::GPCamera", "classDigikam_1_1GPCamera.html", null ],
        [ "Digikam::UMSCamera", "classDigikam_1_1UMSCamera.html", null ]
      ] ],
      [ "Digikam::DMetadataSettings", "classDigikam_1_1DMetadataSettings.html", null ],
      [ "Digikam::DModelFactory", "classDigikam_1_1DModelFactory.html", null ],
      [ "Digikam::DNotificationWidget::Private", "classDigikam_1_1DNotificationWidget_1_1Private.html", null ],
      [ "Digikam::DPlugin", "classDigikam_1_1DPlugin.html", [
        [ "Digikam::DPluginBqm", "classDigikam_1_1DPluginBqm.html", [
          [ "Digikam::RestorationPlugin", "classDigikam_1_1RestorationPlugin.html", null ],
          [ "DigikamBqmAntiVignettingPlugin::AntiVignettingPlugin", "classDigikamBqmAntiVignettingPlugin_1_1AntiVignettingPlugin.html", null ],
          [ "DigikamBqmAssignTemplatePlugin::AssignTemplatePlugin", "classDigikamBqmAssignTemplatePlugin_1_1AssignTemplatePlugin.html", null ],
          [ "DigikamBqmAutoCorrectionPlugin::AutoCorrectionPlugin", "classDigikamBqmAutoCorrectionPlugin_1_1AutoCorrectionPlugin.html", null ],
          [ "DigikamBqmBCGCorrectionPlugin::BCGCorrectionPlugin", "classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrectionPlugin.html", null ],
          [ "DigikamBqmBWConvertPlugin::BWConvertPlugin", "classDigikamBqmBWConvertPlugin_1_1BWConvertPlugin.html", null ],
          [ "DigikamBqmBlurPlugin::BlurPlugin", "classDigikamBqmBlurPlugin_1_1BlurPlugin.html", null ],
          [ "DigikamBqmBorderPlugin::BorderPlugin", "classDigikamBqmBorderPlugin_1_1BorderPlugin.html", null ],
          [ "DigikamBqmChannelMixerPlugin::ChannelMixerPlugin", "classDigikamBqmChannelMixerPlugin_1_1ChannelMixerPlugin.html", null ],
          [ "DigikamBqmColorBalancePlugin::ColorBalancePlugin", "classDigikamBqmColorBalancePlugin_1_1ColorBalancePlugin.html", null ],
          [ "DigikamBqmColorFXPlugin::ColorFXPlugin", "classDigikamBqmColorFXPlugin_1_1ColorFXPlugin.html", null ],
          [ "DigikamBqmConvert16To8Plugin::Convert16To8Plugin", "classDigikamBqmConvert16To8Plugin_1_1Convert16To8Plugin.html", null ],
          [ "DigikamBqmConvert8To16Plugin::Convert8To16Plugin", "classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html", null ],
          [ "DigikamBqmConvertToDngPlugin::ConvertToDngPlugin", "classDigikamBqmConvertToDngPlugin_1_1ConvertToDngPlugin.html", null ],
          [ "DigikamBqmConvertToHeifPlugin::ConvertToHeifPlugin", "classDigikamBqmConvertToHeifPlugin_1_1ConvertToHeifPlugin.html", null ],
          [ "DigikamBqmConvertToJp2Plugin::ConvertToJp2Plugin", "classDigikamBqmConvertToJp2Plugin_1_1ConvertToJp2Plugin.html", null ],
          [ "DigikamBqmConvertToJpegPlugin::ConvertToJpegPlugin", "classDigikamBqmConvertToJpegPlugin_1_1ConvertToJpegPlugin.html", null ],
          [ "DigikamBqmConvertToPgfPlugin::ConvertToPgfPlugin", "classDigikamBqmConvertToPgfPlugin_1_1ConvertToPgfPlugin.html", null ],
          [ "DigikamBqmConvertToPngPlugin::ConvertToPngPlugin", "classDigikamBqmConvertToPngPlugin_1_1ConvertToPngPlugin.html", null ],
          [ "DigikamBqmConvertToTiffPlugin::ConvertToTiffPlugin", "classDigikamBqmConvertToTiffPlugin_1_1ConvertToTiffPlugin.html", null ],
          [ "DigikamBqmCropPlugin::CropPlugin", "classDigikamBqmCropPlugin_1_1CropPlugin.html", null ],
          [ "DigikamBqmCurvesAdjustPlugin::CurvesAdjustPlugin", "classDigikamBqmCurvesAdjustPlugin_1_1CurvesAdjustPlugin.html", null ],
          [ "DigikamBqmFilmGrainPlugin::FilmGrainPlugin", "classDigikamBqmFilmGrainPlugin_1_1FilmGrainPlugin.html", null ],
          [ "DigikamBqmFlipPlugin::FlipPlugin", "classDigikamBqmFlipPlugin_1_1FlipPlugin.html", null ],
          [ "DigikamBqmHSLCorrectionPlugin::HSLCorrectionPlugin", "classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrectionPlugin.html", null ],
          [ "DigikamBqmHotPixelsPlugin::HotPixelsPlugin", "classDigikamBqmHotPixelsPlugin_1_1HotPixelsPlugin.html", null ],
          [ "DigikamBqmIccConvertPlugin::IccConvertPlugin", "classDigikamBqmIccConvertPlugin_1_1IccConvertPlugin.html", null ],
          [ "DigikamBqmInvertPlugin::InvertPlugin", "classDigikamBqmInvertPlugin_1_1InvertPlugin.html", null ],
          [ "DigikamBqmLensAutoFixPlugin::LensAutoFixPlugin", "classDigikamBqmLensAutoFixPlugin_1_1LensAutoFixPlugin.html", null ],
          [ "DigikamBqmLocalContrastPlugin::LocalContrastPlugin", "classDigikamBqmLocalContrastPlugin_1_1LocalContrastPlugin.html", null ],
          [ "DigikamBqmNoiseReductionPlugin::NoiseReductionPlugin", "classDigikamBqmNoiseReductionPlugin_1_1NoiseReductionPlugin.html", null ],
          [ "DigikamBqmRemoveMetadataPlugin::RemoveMetadataPlugin", "classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadataPlugin.html", null ],
          [ "DigikamBqmResizePlugin::ResizePlugin", "classDigikamBqmResizePlugin_1_1ResizePlugin.html", null ],
          [ "DigikamBqmRestorationPlugin::RedEyeCorrectionPlugin", "classDigikamBqmRestorationPlugin_1_1RedEyeCorrectionPlugin.html", null ],
          [ "DigikamBqmRotatePlugin::RotatePlugin", "classDigikamBqmRotatePlugin_1_1RotatePlugin.html", null ],
          [ "DigikamBqmSharpenPlugin::SharpenPlugin", "classDigikamBqmSharpenPlugin_1_1SharpenPlugin.html", null ],
          [ "DigikamBqmTexturePlugin::TexturePlugin", "classDigikamBqmTexturePlugin_1_1TexturePlugin.html", null ],
          [ "DigikamBqmTimeAdjustPlugin::TimeAdjustPlugin", "classDigikamBqmTimeAdjustPlugin_1_1TimeAdjustPlugin.html", null ],
          [ "DigikamBqmUserScriptPlugin::UserScriptPlugin", "classDigikamBqmUserScriptPlugin_1_1UserScriptPlugin.html", null ],
          [ "DigikamBqmWatermarkPlugin::WaterMarkPlugin", "classDigikamBqmWatermarkPlugin_1_1WaterMarkPlugin.html", null ],
          [ "DigikamBqmWhiteBalancePlugin::WhiteBalancePlugin", "classDigikamBqmWhiteBalancePlugin_1_1WhiteBalancePlugin.html", null ]
        ] ],
        [ "Digikam::DPluginDImg", "classDigikam_1_1DPluginDImg.html", [
          [ "DigikamHEIFDImgPlugin::DImgHEIFPlugin", "classDigikamHEIFDImgPlugin_1_1DImgHEIFPlugin.html", null ],
          [ "DigikamImageMagickDImgPlugin::DImgImageMagickPlugin", "classDigikamImageMagickDImgPlugin_1_1DImgImageMagickPlugin.html", null ],
          [ "DigikamJPEG2000DImgPlugin::DImgJPEG2000Plugin", "classDigikamJPEG2000DImgPlugin_1_1DImgJPEG2000Plugin.html", null ],
          [ "DigikamJPEGDImgPlugin::DImgJPEGPlugin", "classDigikamJPEGDImgPlugin_1_1DImgJPEGPlugin.html", null ],
          [ "DigikamPGFDImgPlugin::DImgPGFPlugin", "classDigikamPGFDImgPlugin_1_1DImgPGFPlugin.html", null ],
          [ "DigikamPNGDImgPlugin::DImgPNGPlugin", "classDigikamPNGDImgPlugin_1_1DImgPNGPlugin.html", null ],
          [ "DigikamQImageDImgPlugin::DImgQImagePlugin", "classDigikamQImageDImgPlugin_1_1DImgQImagePlugin.html", null ],
          [ "DigikamRAWDImgPlugin::DImgRAWPlugin", "classDigikamRAWDImgPlugin_1_1DImgRAWPlugin.html", null ],
          [ "DigikamTIFFDImgPlugin::DImgTIFFPlugin", "classDigikamTIFFDImgPlugin_1_1DImgTIFFPlugin.html", null ]
        ] ],
        [ "Digikam::DPluginEditor", "classDigikam_1_1DPluginEditor.html", [
          [ "DigikamEditorAdjustCurvesToolPlugin::AdjustCurvesToolPlugin", "classDigikamEditorAdjustCurvesToolPlugin_1_1AdjustCurvesToolPlugin.html", null ],
          [ "DigikamEditorAdjustLevelsToolPlugin::AdjustLevelsToolPlugin", "classDigikamEditorAdjustLevelsToolPlugin_1_1AdjustLevelsToolPlugin.html", null ],
          [ "DigikamEditorAntivignettingToolPlugin::AntiVignettingToolPlugin", "classDigikamEditorAntivignettingToolPlugin_1_1AntiVignettingToolPlugin.html", null ],
          [ "DigikamEditorAutoCorrectionToolPlugin::AutoCorrectionToolPlugin", "classDigikamEditorAutoCorrectionToolPlugin_1_1AutoCorrectionToolPlugin.html", null ],
          [ "DigikamEditorAutoCropToolPlugin::AutoCropToolPlugin", "classDigikamEditorAutoCropToolPlugin_1_1AutoCropToolPlugin.html", null ],
          [ "DigikamEditorBCGToolPlugin::BCGToolPlugin", "classDigikamEditorBCGToolPlugin_1_1BCGToolPlugin.html", null ],
          [ "DigikamEditorBWSepiaToolPlugin::BWSepiaToolPlugin", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaToolPlugin.html", null ],
          [ "DigikamEditorBlurFxToolPlugin::BlurFXToolPlugin", "classDigikamEditorBlurFxToolPlugin_1_1BlurFXToolPlugin.html", null ],
          [ "DigikamEditorBlurToolPlugin::BlurToolPlugin", "classDigikamEditorBlurToolPlugin_1_1BlurToolPlugin.html", null ],
          [ "DigikamEditorBorderToolPlugin::BorderToolPlugin", "classDigikamEditorBorderToolPlugin_1_1BorderToolPlugin.html", null ],
          [ "DigikamEditorChannelMixerToolPlugin::ChannelMixerToolPlugin", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerToolPlugin.html", null ],
          [ "DigikamEditorCharcoalToolPlugin::CharcoalToolPlugin", "classDigikamEditorCharcoalToolPlugin_1_1CharcoalToolPlugin.html", null ],
          [ "DigikamEditorColorBalanceToolPlugin::CBToolPlugin", "classDigikamEditorColorBalanceToolPlugin_1_1CBToolPlugin.html", null ],
          [ "DigikamEditorColorFxToolPlugin::ColorFXToolPlugin", "classDigikamEditorColorFxToolPlugin_1_1ColorFXToolPlugin.html", null ],
          [ "DigikamEditorContentAwareResizeToolPlugin::ContentAwareResizeToolPlugin", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeToolPlugin.html", null ],
          [ "DigikamEditorConvert16To8ToolPlugin::Convert16To8ToolPlugin", "classDigikamEditorConvert16To8ToolPlugin_1_1Convert16To8ToolPlugin.html", null ],
          [ "DigikamEditorConvert8To16ToolPlugin::Convert8To16ToolPlugin", "classDigikamEditorConvert8To16ToolPlugin_1_1Convert8To16ToolPlugin.html", null ],
          [ "DigikamEditorDistortionFxToolPlugin::DistortionFXToolPlugin", "classDigikamEditorDistortionFxToolPlugin_1_1DistortionFXToolPlugin.html", null ],
          [ "DigikamEditorEmbossToolPlugin::EmbossToolPlugin", "classDigikamEditorEmbossToolPlugin_1_1EmbossToolPlugin.html", null ],
          [ "DigikamEditorFilmGrainToolPlugin::FilmGrainToolPlugin", "classDigikamEditorFilmGrainToolPlugin_1_1FilmGrainToolPlugin.html", null ],
          [ "DigikamEditorFilmToolPlugin::FilmToolPlugin", "classDigikamEditorFilmToolPlugin_1_1FilmToolPlugin.html", null ],
          [ "DigikamEditorFreeRotationToolPlugin::FreeRotationToolPlugin", "classDigikamEditorFreeRotationToolPlugin_1_1FreeRotationToolPlugin.html", null ],
          [ "DigikamEditorHSLToolPlugin::HSLToolPlugin", "classDigikamEditorHSLToolPlugin_1_1HSLToolPlugin.html", null ],
          [ "DigikamEditorHealingCloneToolPlugin::HealingCloneToolPlugin", "classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneToolPlugin.html", null ],
          [ "DigikamEditorHotPixelsToolPlugin::HotPixelsToolPlugin", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsToolPlugin.html", null ],
          [ "DigikamEditorInsertTextToolPlugin::InsertTextToolPlugin", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextToolPlugin.html", null ],
          [ "DigikamEditorInvertToolPlugin::InvertToolPlugin", "classDigikamEditorInvertToolPlugin_1_1InvertToolPlugin.html", null ],
          [ "DigikamEditorLensAutoFixToolPlugin::LensAutoFixToolPlugin", "classDigikamEditorLensAutoFixToolPlugin_1_1LensAutoFixToolPlugin.html", null ],
          [ "DigikamEditorLensDistortionToolPlugin::LensDistortionToolPlugin", "classDigikamEditorLensDistortionToolPlugin_1_1LensDistortionToolPlugin.html", null ],
          [ "DigikamEditorLocalContrastToolPlugin::LocalContrastToolPlugin", "classDigikamEditorLocalContrastToolPlugin_1_1LocalContrastToolPlugin.html", null ],
          [ "DigikamEditorNoiseReductionToolPlugin::LocalContrastToolPlugin", "classDigikamEditorNoiseReductionToolPlugin_1_1LocalContrastToolPlugin.html", null ],
          [ "DigikamEditorOilPaintToolPlugin::OilPaintToolPlugin", "classDigikamEditorOilPaintToolPlugin_1_1OilPaintToolPlugin.html", null ],
          [ "DigikamEditorPerspectiveToolPlugin::PerspectiveToolPlugin", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveToolPlugin.html", null ],
          [ "DigikamEditorPrintToolPlugin::PrintToolPlugin", "classDigikamEditorPrintToolPlugin_1_1PrintToolPlugin.html", null ],
          [ "DigikamEditorProfileConversionToolPlugin::ProfileConversionToolPlugin", "classDigikamEditorProfileConversionToolPlugin_1_1ProfileConversionToolPlugin.html", null ],
          [ "DigikamEditorRainDropToolPlugin::RainDropToolPlugin", "classDigikamEditorRainDropToolPlugin_1_1RainDropToolPlugin.html", null ],
          [ "DigikamEditorRatioCropToolPlugin::RatioCropToolPlugin", "classDigikamEditorRatioCropToolPlugin_1_1RatioCropToolPlugin.html", null ],
          [ "DigikamEditorRedEyeToolPlugin::RedEyeToolPlugin", "classDigikamEditorRedEyeToolPlugin_1_1RedEyeToolPlugin.html", null ],
          [ "DigikamEditorResizeToolPlugin::ResizeToolPlugin", "classDigikamEditorResizeToolPlugin_1_1ResizeToolPlugin.html", null ],
          [ "DigikamEditorRestorationToolPlugin::RestoreToolPlugin", "classDigikamEditorRestorationToolPlugin_1_1RestoreToolPlugin.html", null ],
          [ "DigikamEditorSharpenToolPlugin::SharpenToolPlugin", "classDigikamEditorSharpenToolPlugin_1_1SharpenToolPlugin.html", null ],
          [ "DigikamEditorShearToolPlugin::ShearToolPlugin", "classDigikamEditorShearToolPlugin_1_1ShearToolPlugin.html", null ],
          [ "DigikamEditorTextureToolPlugin::TextureToolPlugin", "classDigikamEditorTextureToolPlugin_1_1TextureToolPlugin.html", null ],
          [ "DigikamEditorWhiteBalanceToolPlugin::WhiteBalanceToolPlugin", "classDigikamEditorWhiteBalanceToolPlugin_1_1WhiteBalanceToolPlugin.html", null ]
        ] ],
        [ "Digikam::DPluginGeneric", "classDigikam_1_1DPluginGeneric.html", [
          [ "DigikamGenericBoxPlugin::BoxPlugin", "classDigikamGenericBoxPlugin_1_1BoxPlugin.html", null ],
          [ "DigikamGenericCalendarPlugin::CalendarPlugin", "classDigikamGenericCalendarPlugin_1_1CalendarPlugin.html", null ],
          [ "DigikamGenericDNGConverterPlugin::DNGConverterPlugin", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterPlugin.html", null ],
          [ "DigikamGenericDScannerPlugin::DigitalScannerPlugin", "classDigikamGenericDScannerPlugin_1_1DigitalScannerPlugin.html", null ],
          [ "DigikamGenericDropBoxPlugin::DBPlugin", "classDigikamGenericDropBoxPlugin_1_1DBPlugin.html", null ],
          [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingPlugin", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingPlugin.html", null ],
          [ "DigikamGenericFaceBookPlugin::FbPlugin", "classDigikamGenericFaceBookPlugin_1_1FbPlugin.html", null ],
          [ "DigikamGenericFileCopyPlugin::FCPlugin", "classDigikamGenericFileCopyPlugin_1_1FCPlugin.html", null ],
          [ "DigikamGenericFileTransferPlugin::FTPlugin", "classDigikamGenericFileTransferPlugin_1_1FTPlugin.html", null ],
          [ "DigikamGenericFlickrPlugin::FlickrPlugin", "classDigikamGenericFlickrPlugin_1_1FlickrPlugin.html", null ],
          [ "DigikamGenericGLViewerPlugin::GLViewerPlugin", "classDigikamGenericGLViewerPlugin_1_1GLViewerPlugin.html", null ],
          [ "DigikamGenericGeolocationEditPlugin::GeolocationEditPlugin", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEditPlugin.html", null ],
          [ "DigikamGenericGoogleServicesPlugin::GSPlugin", "classDigikamGenericGoogleServicesPlugin_1_1GSPlugin.html", null ],
          [ "DigikamGenericHtmlGalleryPlugin::HtmlGalleryPlugin", "classDigikamGenericHtmlGalleryPlugin_1_1HtmlGalleryPlugin.html", null ],
          [ "DigikamGenericINatPlugin::INatPlugin", "classDigikamGenericINatPlugin_1_1INatPlugin.html", null ],
          [ "DigikamGenericImageShackPlugin::ImageShackPlugin", "classDigikamGenericImageShackPlugin_1_1ImageShackPlugin.html", null ],
          [ "DigikamGenericImgUrPlugin::ImgUrPlugin", "classDigikamGenericImgUrPlugin_1_1ImgUrPlugin.html", null ],
          [ "DigikamGenericIpfsPlugin::IpfsPlugin", "classDigikamGenericIpfsPlugin_1_1IpfsPlugin.html", null ],
          [ "DigikamGenericJAlbumPlugin::JAlbumPlugin", "classDigikamGenericJAlbumPlugin_1_1JAlbumPlugin.html", null ],
          [ "DigikamGenericMediaServerPlugin::MediaServerPlugin", "classDigikamGenericMediaServerPlugin_1_1MediaServerPlugin.html", null ],
          [ "DigikamGenericMediaWikiPlugin::MediaWikiPlugin", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiPlugin.html", null ],
          [ "DigikamGenericMetadataEditPlugin::MetadataEditPlugin", "classDigikamGenericMetadataEditPlugin_1_1MetadataEditPlugin.html", null ],
          [ "DigikamGenericMjpegStreamPlugin::MjpegStreamPlugin", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamPlugin.html", null ],
          [ "DigikamGenericOneDrivePlugin::ODPlugin", "classDigikamGenericOneDrivePlugin_1_1ODPlugin.html", null ],
          [ "DigikamGenericPanoramaPlugin::PanoramaPlugin", "classDigikamGenericPanoramaPlugin_1_1PanoramaPlugin.html", null ],
          [ "DigikamGenericPinterestPlugin::PPlugin", "classDigikamGenericPinterestPlugin_1_1PPlugin.html", null ],
          [ "DigikamGenericPiwigoPlugin::PiwigoPlugin", "classDigikamGenericPiwigoPlugin_1_1PiwigoPlugin.html", null ],
          [ "DigikamGenericPresentationPlugin::PresentationPlugin", "classDigikamGenericPresentationPlugin_1_1PresentationPlugin.html", null ],
          [ "DigikamGenericPrintCreatorPlugin::PrintCreatorPlugin", "classDigikamGenericPrintCreatorPlugin_1_1PrintCreatorPlugin.html", null ],
          [ "DigikamGenericRajcePlugin::RajcePlugin", "classDigikamGenericRajcePlugin_1_1RajcePlugin.html", null ],
          [ "DigikamGenericSendByMailPlugin::SendByMailPlugin", "classDigikamGenericSendByMailPlugin_1_1SendByMailPlugin.html", null ],
          [ "DigikamGenericSlideShowPlugin::SlideShowPlugin", "classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin.html", null ],
          [ "DigikamGenericSmugPlugin::SmugPlugin", "classDigikamGenericSmugPlugin_1_1SmugPlugin.html", null ],
          [ "DigikamGenericTimeAdjustPlugin::TimeAdjustPlugin", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustPlugin.html", null ],
          [ "DigikamGenericTwitterPlugin::TwitterPlugin", "classDigikamGenericTwitterPlugin_1_1TwitterPlugin.html", null ],
          [ "DigikamGenericUnifiedPlugin::UnifiedPlugin", "classDigikamGenericUnifiedPlugin_1_1UnifiedPlugin.html", null ],
          [ "DigikamGenericVKontaktePlugin::VKontaktePlugin", "classDigikamGenericVKontaktePlugin_1_1VKontaktePlugin.html", null ],
          [ "DigikamGenericVideoSlideShowPlugin::VideoSlideShowPlugin", "classDigikamGenericVideoSlideShowPlugin_1_1VideoSlideShowPlugin.html", null ],
          [ "DigikamGenericWallpaperPlugin::WallpaperPlugin", "classDigikamGenericWallpaperPlugin_1_1WallpaperPlugin.html", null ],
          [ "DigikamGenericYFPlugin::YFPlugin", "classDigikamGenericYFPlugin_1_1YFPlugin.html", null ]
        ] ],
        [ "Digikam::DPluginRawImport", "classDigikam_1_1DPluginRawImport.html", [
          [ "DigikamRawImportDarkTablePlugin::DarkTableRawImportPlugin", "classDigikamRawImportDarkTablePlugin_1_1DarkTableRawImportPlugin.html", null ],
          [ "DigikamRawImportNativePlugin::RawImportNativePlugin", "classDigikamRawImportNativePlugin_1_1RawImportNativePlugin.html", null ],
          [ "DigikamRawImportRawTherapeePlugin::RawTherapeeRawImportPlugin", "classDigikamRawImportRawTherapeePlugin_1_1RawTherapeeRawImportPlugin.html", null ],
          [ "DigikamRawImportUFRawPlugin::UFRawRawImportPlugin", "classDigikamRawImportUFRawPlugin_1_1UFRawRawImportPlugin.html", null ]
        ] ]
      ] ],
      [ "Digikam::DPluginLoader", "classDigikam_1_1DPluginLoader.html", null ],
      [ "Digikam::DRawDecoder", "classDigikam_1_1DRawDecoder.html", [
        [ "DigikamRAWDImgPlugin::DImgRAWLoader", "classDigikamRAWDImgPlugin_1_1DImgRAWLoader.html", null ]
      ] ],
      [ "Digikam::DWItemDelegatePrivate", "classDigikam_1_1DWItemDelegatePrivate.html", null ],
      [ "Digikam::DWorkingPixmap", "classDigikam_1_1DWorkingPixmap.html", null ],
      [ "Digikam::DatabaseServerStarter", "classDigikam_1_1DatabaseServerStarter.html", null ],
      [ "Digikam::DbEngineErrorHandler", "classDigikam_1_1DbEngineErrorHandler.html", [
        [ "Digikam::DbEngineGuiErrorHandler", "classDigikam_1_1DbEngineGuiErrorHandler.html", null ]
      ] ],
      [ "Digikam::DbHeaderListItem", "classDigikam_1_1DbHeaderListItem.html", null ],
      [ "Digikam::DetectorDistortion", "classDigikam_1_1DetectorDistortion.html", [
        [ "Digikam::BlurDetector", "classDigikam_1_1BlurDetector.html", null ],
        [ "Digikam::CompressionDetector", "classDigikam_1_1CompressionDetector.html", null ],
        [ "Digikam::ExposureDetector", "classDigikam_1_1ExposureDetector.html", null ],
        [ "Digikam::NoiseDetector", "classDigikam_1_1NoiseDetector.html", null ]
      ] ],
      [ "Digikam::DigikamItemView::Private", "classDigikam_1_1DigikamItemView_1_1Private.html", null ],
      [ "Digikam::DisjointMetadata", "classDigikam_1_1DisjointMetadata.html", null ],
      [ "Digikam::DynamicThread", "classDigikam_1_1DynamicThread.html", [
        [ "Digikam::DImgThreadedFilter", "classDigikam_1_1DImgThreadedFilter.html", [
          [ "Digikam::AntiVignettingFilter", "classDigikam_1_1AntiVignettingFilter.html", null ],
          [ "Digikam::AutoLevelsFilter", "classDigikam_1_1AutoLevelsFilter.html", null ],
          [ "Digikam::BCGFilter", "classDigikam_1_1BCGFilter.html", null ],
          [ "Digikam::BWSepiaFilter", "classDigikam_1_1BWSepiaFilter.html", null ],
          [ "Digikam::BlurFXFilter", "classDigikam_1_1BlurFXFilter.html", null ],
          [ "Digikam::BlurFilter", "classDigikam_1_1BlurFilter.html", null ],
          [ "Digikam::BorderFilter", "classDigikam_1_1BorderFilter.html", null ],
          [ "Digikam::CBFilter", "classDigikam_1_1CBFilter.html", null ],
          [ "Digikam::CharcoalFilter", "classDigikam_1_1CharcoalFilter.html", null ],
          [ "Digikam::ColorFXFilter", "classDigikam_1_1ColorFXFilter.html", null ],
          [ "Digikam::ContentAwareFilter", "classDigikam_1_1ContentAwareFilter.html", null ],
          [ "Digikam::CurvesFilter", "classDigikam_1_1CurvesFilter.html", null ],
          [ "Digikam::DImgThreadedAnalyser", "classDigikam_1_1DImgThreadedAnalyser.html", [
            [ "Digikam::AutoCrop", "classDigikam_1_1AutoCrop.html", null ],
            [ "Digikam::NREstimate", "classDigikam_1_1NREstimate.html", null ]
          ] ],
          [ "Digikam::DistortionFXFilter", "classDigikam_1_1DistortionFXFilter.html", null ],
          [ "Digikam::EmbossFilter", "classDigikam_1_1EmbossFilter.html", null ],
          [ "Digikam::EqualizeFilter", "classDigikam_1_1EqualizeFilter.html", null ],
          [ "Digikam::FilmFilter", "classDigikam_1_1FilmFilter.html", null ],
          [ "Digikam::FilmGrainFilter", "classDigikam_1_1FilmGrainFilter.html", null ],
          [ "Digikam::FilterActionFilter", "classDigikam_1_1FilterActionFilter.html", null ],
          [ "Digikam::FreeRotationFilter", "classDigikam_1_1FreeRotationFilter.html", null ],
          [ "Digikam::GreycstorationFilter", "classDigikam_1_1GreycstorationFilter.html", null ],
          [ "Digikam::HSLFilter", "classDigikam_1_1HSLFilter.html", null ],
          [ "Digikam::HotPixelFixer", "classDigikam_1_1HotPixelFixer.html", null ],
          [ "Digikam::IccTransformFilter", "classDigikam_1_1IccTransformFilter.html", null ],
          [ "Digikam::InfraredFilter", "classDigikam_1_1InfraredFilter.html", null ],
          [ "Digikam::InvertFilter", "classDigikam_1_1InvertFilter.html", null ],
          [ "Digikam::LensDistortionFilter", "classDigikam_1_1LensDistortionFilter.html", null ],
          [ "Digikam::LensFunFilter", "classDigikam_1_1LensFunFilter.html", null ],
          [ "Digikam::LevelsFilter", "classDigikam_1_1LevelsFilter.html", null ],
          [ "Digikam::LocalContrastFilter", "classDigikam_1_1LocalContrastFilter.html", null ],
          [ "Digikam::MixerFilter", "classDigikam_1_1MixerFilter.html", null ],
          [ "Digikam::NRFilter", "classDigikam_1_1NRFilter.html", null ],
          [ "Digikam::NormalizeFilter", "classDigikam_1_1NormalizeFilter.html", null ],
          [ "Digikam::OilPaintFilter", "classDigikam_1_1OilPaintFilter.html", null ],
          [ "Digikam::RainDropFilter", "classDigikam_1_1RainDropFilter.html", null ],
          [ "Digikam::RawProcessingFilter", "classDigikam_1_1RawProcessingFilter.html", null ],
          [ "Digikam::RedEyeCorrectionFilter", "classDigikam_1_1RedEyeCorrectionFilter.html", null ],
          [ "Digikam::RefocusFilter", "classDigikam_1_1RefocusFilter.html", null ],
          [ "Digikam::SharpenFilter", "classDigikam_1_1SharpenFilter.html", null ],
          [ "Digikam::ShearFilter", "classDigikam_1_1ShearFilter.html", null ],
          [ "Digikam::StretchFilter", "classDigikam_1_1StretchFilter.html", null ],
          [ "Digikam::TextureFilter", "classDigikam_1_1TextureFilter.html", null ],
          [ "Digikam::TonalityFilter", "classDigikam_1_1TonalityFilter.html", null ],
          [ "Digikam::UnsharpMaskFilter", "classDigikam_1_1UnsharpMaskFilter.html", null ],
          [ "Digikam::WBFilter", "classDigikam_1_1WBFilter.html", [
            [ "Digikam::AutoExpoFilter", "classDigikam_1_1AutoExpoFilter.html", null ]
          ] ],
          [ "DigikamRawImportNativePlugin::RawPostProcessing", "classDigikamRawImportNativePlugin_1_1RawPostProcessing.html", null ]
        ] ],
        [ "Digikam::ImageHistogram", "classDigikam_1_1ImageHistogram.html", null ],
        [ "Digikam::LoadSaveThread", "classDigikam_1_1LoadSaveThread.html", null ],
        [ "Digikam::ScanStateFilter", "classDigikam_1_1ScanStateFilter.html", null ]
      ] ],
      [ "Digikam::EditorCore", "classDigikam_1_1EditorCore.html", null ],
      [ "Digikam::EditorTool", "classDigikam_1_1EditorTool.html", [
        [ "Digikam::EditorToolThreaded", "classDigikam_1_1EditorToolThreaded.html", [
          [ "DigikamEditorAdjustCurvesToolPlugin::AdjustCurvesTool", "classDigikamEditorAdjustCurvesToolPlugin_1_1AdjustCurvesTool.html", null ],
          [ "DigikamEditorAdjustLevelsToolPlugin::AdjustLevelsTool", "classDigikamEditorAdjustLevelsToolPlugin_1_1AdjustLevelsTool.html", null ],
          [ "DigikamEditorAntivignettingToolPlugin::AntiVignettingTool", "classDigikamEditorAntivignettingToolPlugin_1_1AntiVignettingTool.html", null ],
          [ "DigikamEditorAutoCorrectionToolPlugin::AutoCorrectionTool", "classDigikamEditorAutoCorrectionToolPlugin_1_1AutoCorrectionTool.html", null ],
          [ "DigikamEditorBCGToolPlugin::BCGTool", "classDigikamEditorBCGToolPlugin_1_1BCGTool.html", null ],
          [ "DigikamEditorBWSepiaToolPlugin::BWSepiaTool", "classDigikamEditorBWSepiaToolPlugin_1_1BWSepiaTool.html", null ],
          [ "DigikamEditorBlurFxToolPlugin::BlurFXTool", "classDigikamEditorBlurFxToolPlugin_1_1BlurFXTool.html", null ],
          [ "DigikamEditorBlurToolPlugin::BlurTool", "classDigikamEditorBlurToolPlugin_1_1BlurTool.html", null ],
          [ "DigikamEditorBorderToolPlugin::BorderTool", "classDigikamEditorBorderToolPlugin_1_1BorderTool.html", null ],
          [ "DigikamEditorChannelMixerToolPlugin::ChannelMixerTool", "classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerTool.html", null ],
          [ "DigikamEditorCharcoalToolPlugin::CharcoalTool", "classDigikamEditorCharcoalToolPlugin_1_1CharcoalTool.html", null ],
          [ "DigikamEditorColorBalanceToolPlugin::CBTool", "classDigikamEditorColorBalanceToolPlugin_1_1CBTool.html", null ],
          [ "DigikamEditorColorFxToolPlugin::ColorFxTool", "classDigikamEditorColorFxToolPlugin_1_1ColorFxTool.html", null ],
          [ "DigikamEditorContentAwareResizeToolPlugin::ContentAwareResizeTool", "classDigikamEditorContentAwareResizeToolPlugin_1_1ContentAwareResizeTool.html", null ],
          [ "DigikamEditorDistortionFxToolPlugin::DistortionFXTool", "classDigikamEditorDistortionFxToolPlugin_1_1DistortionFXTool.html", null ],
          [ "DigikamEditorEmbossToolPlugin::EmbossTool", "classDigikamEditorEmbossToolPlugin_1_1EmbossTool.html", null ],
          [ "DigikamEditorFilmGrainToolPlugin::FilmGrainTool", "classDigikamEditorFilmGrainToolPlugin_1_1FilmGrainTool.html", null ],
          [ "DigikamEditorFilmToolPlugin::FilmTool", "classDigikamEditorFilmToolPlugin_1_1FilmTool.html", null ],
          [ "DigikamEditorFreeRotationToolPlugin::FreeRotationTool", "classDigikamEditorFreeRotationToolPlugin_1_1FreeRotationTool.html", null ],
          [ "DigikamEditorHSLToolPlugin::HSLTool", "classDigikamEditorHSLToolPlugin_1_1HSLTool.html", null ],
          [ "DigikamEditorHotPixelsToolPlugin::HotPixelsTool", "classDigikamEditorHotPixelsToolPlugin_1_1HotPixelsTool.html", null ],
          [ "DigikamEditorLensAutoFixToolPlugin::LensAutoFixTool", "classDigikamEditorLensAutoFixToolPlugin_1_1LensAutoFixTool.html", null ],
          [ "DigikamEditorLensDistortionToolPlugin::LensDistortionTool", "classDigikamEditorLensDistortionToolPlugin_1_1LensDistortionTool.html", null ],
          [ "DigikamEditorLocalContrastToolPlugin::LocalContrastTool", "classDigikamEditorLocalContrastToolPlugin_1_1LocalContrastTool.html", null ],
          [ "DigikamEditorNoiseReductionToolPlugin::NoiseReductionTool", "classDigikamEditorNoiseReductionToolPlugin_1_1NoiseReductionTool.html", null ],
          [ "DigikamEditorOilPaintToolPlugin::OilPaintTool", "classDigikamEditorOilPaintToolPlugin_1_1OilPaintTool.html", null ],
          [ "DigikamEditorProfileConversionToolPlugin::ProfileConversionTool", "classDigikamEditorProfileConversionToolPlugin_1_1ProfileConversionTool.html", null ],
          [ "DigikamEditorRainDropToolPlugin::RainDropTool", "classDigikamEditorRainDropToolPlugin_1_1RainDropTool.html", null ],
          [ "DigikamEditorRedEyeToolPlugin::RedEyeTool", "classDigikamEditorRedEyeToolPlugin_1_1RedEyeTool.html", null ],
          [ "DigikamEditorResizeToolPlugin::ResizeTool", "classDigikamEditorResizeToolPlugin_1_1ResizeTool.html", null ],
          [ "DigikamEditorRestorationToolPlugin::RestorationTool", "classDigikamEditorRestorationToolPlugin_1_1RestorationTool.html", null ],
          [ "DigikamEditorSharpenToolPlugin::SharpenTool", "classDigikamEditorSharpenToolPlugin_1_1SharpenTool.html", null ],
          [ "DigikamEditorShearToolPlugin::ShearTool", "classDigikamEditorShearToolPlugin_1_1ShearTool.html", null ],
          [ "DigikamEditorTextureToolPlugin::TextureTool", "classDigikamEditorTextureToolPlugin_1_1TextureTool.html", null ],
          [ "DigikamEditorWhiteBalanceToolPlugin::WhiteBalanceTool", "classDigikamEditorWhiteBalanceToolPlugin_1_1WhiteBalanceTool.html", null ],
          [ "DigikamRawImportNativePlugin::RawImport", "classDigikamRawImportNativePlugin_1_1RawImport.html", null ]
        ] ],
        [ "DigikamEditorHealingCloneToolPlugin::HealingCloneTool", "classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneTool.html", null ],
        [ "DigikamEditorInsertTextToolPlugin::InsertTextTool", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextTool.html", null ],
        [ "DigikamEditorPerspectiveToolPlugin::PerspectiveTool", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTool.html", null ],
        [ "DigikamEditorRatioCropToolPlugin::RatioCropTool", "classDigikamEditorRatioCropToolPlugin_1_1RatioCropTool.html", null ]
      ] ],
      [ "Digikam::EditorToolIface", "classDigikam_1_1EditorToolIface.html", null ],
      [ "Digikam::ExifToolParser", "classDigikam_1_1ExifToolParser.html", null ],
      [ "Digikam::ExifToolProcess", "classDigikam_1_1ExifToolProcess.html", null ],
      [ "Digikam::FaceGroup", "classDigikam_1_1FaceGroup.html", null ],
      [ "Digikam::FacePipeline", "classDigikam_1_1FacePipeline.html", null ],
      [ "Digikam::FacePipeline::Private", "classDigikam_1_1FacePipeline_1_1Private.html", null ],
      [ "Digikam::FaceUtils", "classDigikam_1_1FaceUtils.html", null ],
      [ "Digikam::FileActionMngr", "classDigikam_1_1FileActionMngr.html", null ],
      [ "Digikam::FileActionMngr::Private", "classDigikam_1_1FileActionMngr_1_1Private.html", null ],
      [ "Digikam::FileActionProgressItemContainer", "classDigikam_1_1FileActionProgressItemContainer.html", null ],
      [ "Digikam::FocusPointGroup", "classDigikam_1_1FocusPointGroup.html", null ],
      [ "Digikam::FocusPointsExtractor", "classDigikam_1_1FocusPointsExtractor.html", null ],
      [ "Digikam::FocusPointsWriter", "classDigikam_1_1FocusPointsWriter.html", null ],
      [ "Digikam::GPSBookmarkOwner", "classDigikam_1_1GPSBookmarkOwner.html", null ],
      [ "Digikam::GPSItemInfoSorter", "classDigikam_1_1GPSItemInfoSorter.html", null ],
      [ "Digikam::GPSItemListContextMenu", "classDigikam_1_1GPSItemListContextMenu.html", null ],
      [ "Digikam::GPSModelIndexProxyMapper", "classDigikam_1_1GPSModelIndexProxyMapper.html", null ],
      [ "Digikam::GeoDragDropHandler", "classDigikam_1_1GeoDragDropHandler.html", [
        [ "Digikam::MapDragDropHandler", "classDigikam_1_1MapDragDropHandler.html", null ]
      ] ],
      [ "Digikam::GeoIfaceGlobalObject", "classDigikam_1_1GeoIfaceGlobalObject.html", null ],
      [ "Digikam::GeoModelHelper", "classDigikam_1_1GeoModelHelper.html", [
        [ "Digikam::GPSBookmarkModelHelper", "classDigikam_1_1GPSBookmarkModelHelper.html", null ],
        [ "Digikam::GPSGeoIfaceModelHelper", "classDigikam_1_1GPSGeoIfaceModelHelper.html", null ],
        [ "Digikam::ItemGPSModelHelper", "classDigikam_1_1ItemGPSModelHelper.html", null ],
        [ "Digikam::MapViewModelHelper", "classDigikam_1_1MapViewModelHelper.html", null ],
        [ "DigikamGenericGeolocationEditPlugin::SearchResultModelHelper", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html", null ]
      ] ],
      [ "Digikam::GreycstorationSettings", "classDigikam_1_1GreycstorationSettings.html", null ],
      [ "Digikam::HistogramPainter", "classDigikam_1_1HistogramPainter.html", null ],
      [ "Digikam::IOJobsManager", "classDigikam_1_1IOJobsManager.html", null ],
      [ "Digikam::IccSettings", "classDigikam_1_1IccSettings.html", null ],
      [ "Digikam::ImageDialog", "classDigikam_1_1ImageDialog.html", null ],
      [ "Digikam::ImageQualityParser", "classDigikam_1_1ImageQualityParser.html", null ],
      [ "Digikam::ImageQualityThreadPool", "classDigikam_1_1ImageQualityThreadPool.html", null ],
      [ "Digikam::ImportContextMenuHelper", "classDigikam_1_1ImportContextMenuHelper.html", null ],
      [ "Digikam::ImportIconView::Private", "classDigikam_1_1ImportIconView_1_1Private.html", null ],
      [ "Digikam::ImportSettings", "classDigikam_1_1ImportSettings.html", null ],
      [ "Digikam::ItemAttributesWatch", "classDigikam_1_1ItemAttributesWatch.html", null ],
      [ "Digikam::ItemDelegateOverlay", "classDigikam_1_1ItemDelegateOverlay.html", [
        [ "Digikam::AbstractWidgetDelegateOverlay", "classDigikam_1_1AbstractWidgetDelegateOverlay.html", [
          [ "Digikam::GroupIndicatorOverlay", "classDigikam_1_1GroupIndicatorOverlay.html", null ],
          [ "Digikam::HoverButtonDelegateOverlay", "classDigikam_1_1HoverButtonDelegateOverlay.html", [
            [ "Digikam::ActionVersionsOverlay", "classDigikam_1_1ActionVersionsOverlay.html", null ],
            [ "Digikam::FaceRejectionOverlay", "classDigikam_1_1FaceRejectionOverlay.html", null ],
            [ "Digikam::ImportRotateOverlay", "classDigikam_1_1ImportRotateOverlay.html", null ],
            [ "Digikam::ItemFullScreenOverlay", "classDigikam_1_1ItemFullScreenOverlay.html", null ],
            [ "Digikam::ItemRotateOverlay", "classDigikam_1_1ItemRotateOverlay.html", null ],
            [ "Digikam::ItemSelectionOverlay", "classDigikam_1_1ItemSelectionOverlay.html", null ],
            [ "Digikam::ShowHideVersionsOverlay", "classDigikam_1_1ShowHideVersionsOverlay.html", null ]
          ] ],
          [ "Digikam::ImportCoordinatesOverlay", "classDigikam_1_1ImportCoordinatesOverlay.html", null ],
          [ "Digikam::ImportDownloadOverlay", "classDigikam_1_1ImportDownloadOverlay.html", null ],
          [ "Digikam::ImportLockOverlay", "classDigikam_1_1ImportLockOverlay.html", null ],
          [ "Digikam::ImportRatingOverlay", "classDigikam_1_1ImportRatingOverlay.html", null ],
          [ "Digikam::ItemCoordinatesOverlay", "classDigikam_1_1ItemCoordinatesOverlay.html", null ],
          [ "Digikam::ItemRatingOverlay", "classDigikam_1_1ItemRatingOverlay.html", null ],
          [ "Digikam::PersistentWidgetDelegateOverlay", "classDigikam_1_1PersistentWidgetDelegateOverlay.html", [
            [ "Digikam::AssignNameOverlay", "classDigikam_1_1AssignNameOverlay.html", null ]
          ] ],
          [ "Digikam::TagsLineEditOverlay", "classDigikam_1_1TagsLineEditOverlay.html", null ],
          [ "ShowFoto::ShowfotoCoordinatesOverlay", "classShowFoto_1_1ShowfotoCoordinatesOverlay.html", null ]
        ] ]
      ] ],
      [ "Digikam::ItemInfoAlbumsJob", "classDigikam_1_1ItemInfoAlbumsJob.html", null ],
      [ "Digikam::ItemInfoCache", "classDigikam_1_1ItemInfoCache.html", null ],
      [ "Digikam::ItemInfoJob", "classDigikam_1_1ItemInfoJob.html", null ],
      [ "Digikam::ItemListDragDropHandler", "classDigikam_1_1ItemListDragDropHandler.html", [
        [ "Digikam::GPSItemListDragDropHandler", "classDigikam_1_1GPSItemListDragDropHandler.html", null ]
      ] ],
      [ "Digikam::ItemSortCollator", "classDigikam_1_1ItemSortCollator.html", null ],
      [ "Digikam::ItemViewUtilities", "classDigikam_1_1ItemViewUtilities.html", null ],
      [ "Digikam::ItemVisibilityController", "classDigikam_1_1ItemVisibilityController.html", [
        [ "Digikam::HidingStateChanger", "classDigikam_1_1HidingStateChanger.html", [
          [ "Digikam::AssignNameWidgetStates", "classDigikam_1_1AssignNameWidgetStates.html", null ]
        ] ]
      ] ],
      [ "Digikam::ItemVisibilityControllerPropertyObject", "classDigikam_1_1ItemVisibilityControllerPropertyObject.html", [
        [ "Digikam::AnimatedVisibility", "classDigikam_1_1AnimatedVisibility.html", null ]
      ] ],
      [ "Digikam::ListItem", "classDigikam_1_1ListItem.html", null ],
      [ "Digikam::LoadingCache", "classDigikam_1_1LoadingCache.html", null ],
      [ "Digikam::LoadingCacheFileWatch", "classDigikam_1_1LoadingCacheFileWatch.html", [
        [ "Digikam::ScanControllerLoadingCacheFileWatch", "classDigikam_1_1ScanControllerLoadingCacheFileWatch.html", null ]
      ] ],
      [ "Digikam::LookupAltitude", "classDigikam_1_1LookupAltitude.html", [
        [ "Digikam::LookupAltitudeGeonames", "classDigikam_1_1LookupAltitudeGeonames.html", null ]
      ] ],
      [ "Digikam::MaintenanceMngr", "classDigikam_1_1MaintenanceMngr.html", null ],
      [ "Digikam::MapBackend", "classDigikam_1_1MapBackend.html", [
        [ "Digikam::BackendGoogleMaps", "classDigikam_1_1BackendGoogleMaps.html", null ],
        [ "Digikam::BackendMarble", "classDigikam_1_1BackendMarble.html", null ]
      ] ],
      [ "Digikam::MdKeyListViewItem", "classDigikam_1_1MdKeyListViewItem.html", null ],
      [ "Digikam::MetaEngineSettings", "classDigikam_1_1MetaEngineSettings.html", null ],
      [ "Digikam::MetadataHubMngr", "classDigikam_1_1MetadataHubMngr.html", null ],
      [ "Digikam::MetadataPanel", "classDigikam_1_1MetadataPanel.html", null ],
      [ "Digikam::OnlineVersionChecker", "classDigikam_1_1OnlineVersionChecker.html", null ],
      [ "Digikam::OnlineVersionDwnl", "classDigikam_1_1OnlineVersionDwnl.html", null ],
      [ "Digikam::ParallelPipes", "classDigikam_1_1ParallelPipes.html", null ],
      [ "Digikam::PreviewThreadWrapper", "classDigikam_1_1PreviewThreadWrapper.html", null ],
      [ "Digikam::PrivateProgressItemCreator", "classDigikam_1_1PrivateProgressItemCreator.html", null ],
      [ "Digikam::ProgressItem", "classDigikam_1_1ProgressItem.html", [
        [ "Digikam::AlbumParser", "classDigikam_1_1AlbumParser.html", null ],
        [ "Digikam::FileActionProgress", "classDigikam_1_1FileActionProgress.html", null ],
        [ "Digikam::MaintenanceTool", "classDigikam_1_1MaintenanceTool.html", [
          [ "Digikam::DbCleaner", "classDigikam_1_1DbCleaner.html", null ],
          [ "Digikam::DuplicatesFinder", "classDigikam_1_1DuplicatesFinder.html", null ],
          [ "Digikam::FacesDetector", "classDigikam_1_1FacesDetector.html", null ],
          [ "Digikam::FingerPrintsGenerator", "classDigikam_1_1FingerPrintsGenerator.html", null ],
          [ "Digikam::ImageQualitySorter", "classDigikam_1_1ImageQualitySorter.html", null ],
          [ "Digikam::MetadataSynchronizer", "classDigikam_1_1MetadataSynchronizer.html", null ],
          [ "Digikam::NewItemsFinder", "classDigikam_1_1NewItemsFinder.html", null ],
          [ "Digikam::ThumbsGenerator", "classDigikam_1_1ThumbsGenerator.html", null ]
        ] ]
      ] ],
      [ "Digikam::ProgressManager", "classDigikam_1_1ProgressManager.html", null ],
      [ "Digikam::RGBackend", "classDigikam_1_1RGBackend.html", [
        [ "Digikam::BackendGeonamesRG", "classDigikam_1_1BackendGeonamesRG.html", null ],
        [ "Digikam::BackendGeonamesUSRG", "classDigikam_1_1BackendGeonamesUSRG.html", null ],
        [ "Digikam::BackendOsmRG", "classDigikam_1_1BackendOsmRG.html", null ]
      ] ],
      [ "Digikam::Rule", "classDigikam_1_1Rule.html", [
        [ "Digikam::Modifier", "classDigikam_1_1Modifier.html", [
          [ "Digikam::CaseModifier", "classDigikam_1_1CaseModifier.html", null ],
          [ "Digikam::DefaultValueModifier", "classDigikam_1_1DefaultValueModifier.html", null ],
          [ "Digikam::RangeModifier", "classDigikam_1_1RangeModifier.html", null ],
          [ "Digikam::RemoveDoublesModifier", "classDigikam_1_1RemoveDoublesModifier.html", null ],
          [ "Digikam::ReplaceModifier", "classDigikam_1_1ReplaceModifier.html", null ],
          [ "Digikam::TrimmedModifier", "classDigikam_1_1TrimmedModifier.html", null ],
          [ "Digikam::UniqueModifier", "classDigikam_1_1UniqueModifier.html", null ]
        ] ],
        [ "Digikam::Option", "classDigikam_1_1Option.html", [
          [ "Digikam::CameraNameOption", "classDigikam_1_1CameraNameOption.html", null ],
          [ "Digikam::DatabaseOption", "classDigikam_1_1DatabaseOption.html", null ],
          [ "Digikam::DateOption", "classDigikam_1_1DateOption.html", null ],
          [ "Digikam::DirectoryNameOption", "classDigikam_1_1DirectoryNameOption.html", null ],
          [ "Digikam::FilePropertiesOption", "classDigikam_1_1FilePropertiesOption.html", null ],
          [ "Digikam::MetadataOption", "classDigikam_1_1MetadataOption.html", null ],
          [ "Digikam::SequenceNumberOption", "classDigikam_1_1SequenceNumberOption.html", null ]
        ] ]
      ] ],
      [ "Digikam::SearchField", "classDigikam_1_1SearchField.html", null ],
      [ "Digikam::SearchModificationHelper", "classDigikam_1_1SearchModificationHelper.html", null ],
      [ "Digikam::SetupRaw", "classDigikam_1_1SetupRaw.html", null ],
      [ "Digikam::SinglePhotoPreviewLayout", "classDigikam_1_1SinglePhotoPreviewLayout.html", null ],
      [ "Digikam::SyncJob", "classDigikam_1_1SyncJob.html", null ],
      [ "Digikam::TableViewColumn", "classDigikam_1_1TableViewColumn.html", [
        [ "Digikam::TableViewColumns::ColumnAudioVideoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnDigikamProperties", "classDigikam_1_1TableViewColumns_1_1ColumnDigikamProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnFileProperties", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnGeoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnItemProperties", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnPhotoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoProperties.html", null ],
        [ "Digikam::TableViewColumns::ColumnThumbnail", "classDigikam_1_1TableViewColumns_1_1ColumnThumbnail.html", null ]
      ] ],
      [ "Digikam::TableViewColumnFactory", "classDigikam_1_1TableViewColumnFactory.html", null ],
      [ "Digikam::TableViewSelectionModelSyncer", "classDigikam_1_1TableViewSelectionModelSyncer.html", null ],
      [ "Digikam::TagModificationHelper", "classDigikam_1_1TagModificationHelper.html", null ],
      [ "Digikam::TagsActionMngr", "classDigikam_1_1TagsActionMngr.html", null ],
      [ "Digikam::TagsCache", "classDigikam_1_1TagsCache.html", null ],
      [ "Digikam::TemplateManager", "classDigikam_1_1TemplateManager.html", null ],
      [ "Digikam::ThemeManager", "classDigikam_1_1ThemeManager.html", null ],
      [ "Digikam::ThreadManager", "classDigikam_1_1ThreadManager.html", null ],
      [ "Digikam::ThumbnailImageCatcher", "classDigikam_1_1ThumbnailImageCatcher.html", null ],
      [ "Digikam::TileGrouper", "classDigikam_1_1TileGrouper.html", null ],
      [ "Digikam::Token", "classDigikam_1_1Token.html", null ],
      [ "Digikam::TrackCorrelator", "classDigikam_1_1TrackCorrelator.html", null ],
      [ "Digikam::TrackManager", "classDigikam_1_1TrackManager.html", null ],
      [ "Digikam::VisibilityController", "classDigikam_1_1VisibilityController.html", null ],
      [ "Digikam::WSSettings", "classDigikam_1_1WSSettings.html", null ],
      [ "Digikam::WorkerObject", "classDigikam_1_1WorkerObject.html", [
        [ "Digikam::DatabaseWorkerInterface", "classDigikam_1_1DatabaseWorkerInterface.html", [
          [ "Digikam::FileActionMngrDatabaseWorker", "classDigikam_1_1FileActionMngrDatabaseWorker.html", null ]
        ] ],
        [ "Digikam::DatabaseWriter", "classDigikam_1_1DatabaseWriter.html", null ],
        [ "Digikam::DetectionBenchmarker", "classDigikam_1_1DetectionBenchmarker.html", null ],
        [ "Digikam::DetectionWorker", "classDigikam_1_1DetectionWorker.html", null ],
        [ "Digikam::FileWorkerInterface", "classDigikam_1_1FileWorkerInterface.html", [
          [ "Digikam::ParallelAdapter< Digikam::FileWorkerInterface >", "classDigikam_1_1ParallelAdapter.html", null ],
          [ "Digikam::FileActionMngrFileWorker", "classDigikam_1_1FileActionMngrFileWorker.html", null ]
        ] ],
        [ "Digikam::ItemFilterModelWorker", "classDigikam_1_1ItemFilterModelWorker.html", [
          [ "Digikam::ItemFilterModelFilterer", "classDigikam_1_1ItemFilterModelFilterer.html", null ],
          [ "Digikam::ItemFilterModelPreparer", "classDigikam_1_1ItemFilterModelPreparer.html", null ]
        ] ],
        [ "Digikam::RecognitionBenchmarker", "classDigikam_1_1RecognitionBenchmarker.html", null ],
        [ "Digikam::RecognitionWorker", "classDigikam_1_1RecognitionWorker.html", null ],
        [ "Digikam::TrainerWorker", "classDigikam_1_1TrainerWorker.html", null ]
      ] ],
      [ "Digikam::WorkflowManager", "classDigikam_1_1WorkflowManager.html", null ],
      [ "DigikamGenericBoxPlugin::BOXTalker", "classDigikamGenericBoxPlugin_1_1BOXTalker.html", null ],
      [ "DigikamGenericCalendarPlugin::CalPainter", "classDigikamGenericCalendarPlugin_1_1CalPainter.html", null ],
      [ "DigikamGenericCalendarPlugin::CalSettings", "classDigikamGenericCalendarPlugin_1_1CalSettings.html", null ],
      [ "DigikamGenericDropBoxPlugin::DBTalker", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingManager", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingManager.html", null ],
      [ "DigikamGenericFaceBookPlugin::FbTalker", "classDigikamGenericFaceBookPlugin_1_1FbTalker.html", null ],
      [ "DigikamGenericFlickrPlugin::FlickrTalker", "classDigikamGenericFlickrPlugin_1_1FlickrTalker.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::KmlExport", "classDigikamGenericGeolocationEditPlugin_1_1KmlExport.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::SearchBackend", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html", null ],
      [ "DigikamGenericGoogleServicesPlugin::GSTalkerBase", "classDigikamGenericGoogleServicesPlugin_1_1GSTalkerBase.html", [
        [ "DigikamGenericGoogleServicesPlugin::GDTalker", "classDigikamGenericGoogleServicesPlugin_1_1GDTalker.html", null ],
        [ "DigikamGenericGoogleServicesPlugin::GPTalker", "classDigikamGenericGoogleServicesPlugin_1_1GPTalker.html", null ]
      ] ],
      [ "DigikamGenericHtmlGalleryPlugin::GalleryGenerator", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryGenerator.html", null ],
      [ "DigikamGenericINatPlugin::INatTalker", "classDigikamGenericINatPlugin_1_1INatTalker.html", null ],
      [ "DigikamGenericINatPlugin::SuggestTaxonCompletion", "classDigikamGenericINatPlugin_1_1SuggestTaxonCompletion.html", null ],
      [ "DigikamGenericImageShackPlugin::ImageShackTalker", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html", null ],
      [ "DigikamGenericImgUrPlugin::ImgurTalker", "classDigikamGenericImgUrPlugin_1_1ImgurTalker.html", null ],
      [ "DigikamGenericIpfsPlugin::IpfsTalker", "classDigikamGenericIpfsPlugin_1_1IpfsTalker.html", null ],
      [ "DigikamGenericJAlbumPlugin::JAlbumGenerator", "classDigikamGenericJAlbumPlugin_1_1JAlbumGenerator.html", null ],
      [ "DigikamGenericMediaServerPlugin::DMediaServer", "classDigikamGenericMediaServerPlugin_1_1DMediaServer.html", null ],
      [ "DigikamGenericMediaServerPlugin::DMediaServerMngr", "classDigikamGenericMediaServerPlugin_1_1DMediaServerMngr.html", null ],
      [ "DigikamGenericMjpegStreamPlugin::MjpegServer", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html", null ],
      [ "DigikamGenericMjpegStreamPlugin::MjpegServer::Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html", null ],
      [ "DigikamGenericMjpegStreamPlugin::MjpegServerMngr", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html", null ],
      [ "DigikamGenericOneDrivePlugin::ODTalker", "classDigikamGenericOneDrivePlugin_1_1ODTalker.html", null ],
      [ "DigikamGenericPanoramaPlugin::PanoActionThread", "classDigikamGenericPanoramaPlugin_1_1PanoActionThread.html", null ],
      [ "DigikamGenericPanoramaPlugin::PanoManager", "classDigikamGenericPanoramaPlugin_1_1PanoManager.html", null ],
      [ "DigikamGenericPinterestPlugin::PTalker", "classDigikamGenericPinterestPlugin_1_1PTalker.html", null ],
      [ "DigikamGenericPiwigoPlugin::PiwigoTalker", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAudioListItem", "classDigikamGenericPresentationPlugin_1_1PresentationAudioListItem.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationMngr", "classDigikamGenericPresentationPlugin_1_1PresentationMngr.html", null ],
      [ "DigikamGenericRajcePlugin::RajceCommand", "classDigikamGenericRajcePlugin_1_1RajceCommand.html", [
        [ "DigikamGenericRajcePlugin::AddPhotoCommand", "classDigikamGenericRajcePlugin_1_1AddPhotoCommand.html", null ],
        [ "DigikamGenericRajcePlugin::AlbumListCommand", "classDigikamGenericRajcePlugin_1_1AlbumListCommand.html", null ],
        [ "DigikamGenericRajcePlugin::CloseAlbumCommand", "classDigikamGenericRajcePlugin_1_1CloseAlbumCommand.html", null ],
        [ "DigikamGenericRajcePlugin::CreateAlbumCommand", "classDigikamGenericRajcePlugin_1_1CreateAlbumCommand.html", null ],
        [ "DigikamGenericRajcePlugin::LoginCommand", "classDigikamGenericRajcePlugin_1_1LoginCommand.html", null ],
        [ "DigikamGenericRajcePlugin::OpenAlbumCommand", "classDigikamGenericRajcePlugin_1_1OpenAlbumCommand.html", null ]
      ] ],
      [ "DigikamGenericRajcePlugin::RajceTalker", "classDigikamGenericRajcePlugin_1_1RajceTalker.html", null ],
      [ "DigikamGenericSendByMailPlugin::MailProcess", "classDigikamGenericSendByMailPlugin_1_1MailProcess.html", null ],
      [ "DigikamGenericSmugPlugin::SmugTalker", "classDigikamGenericSmugPlugin_1_1SmugTalker.html", null ],
      [ "DigikamGenericTwitterPlugin::TwTalker", "classDigikamGenericTwitterPlugin_1_1TwTalker.html", null ],
      [ "DigikamGenericUnifiedPlugin::WSAuthentication", "classDigikamGenericUnifiedPlugin_1_1WSAuthentication.html", null ],
      [ "DigikamGenericUnifiedPlugin::WSTalker", "classDigikamGenericUnifiedPlugin_1_1WSTalker.html", null ],
      [ "DigikamGenericYFPlugin::YFTalker", "classDigikamGenericYFPlugin_1_1YFTalker.html", null ],
      [ "ShowFoto::ShowfotoKineticScroller", "classShowFoto_1_1ShowfotoKineticScroller.html", null ],
      [ "ShowFoto::ShowfotoSettings", "classShowFoto_1_1ShowfotoSettings.html", null ]
    ] ],
    [ "QOpenGLTexture", null, [
      [ "DigikamGenericGLViewerPlugin::GLViewerTexture", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html", null ]
    ] ],
    [ "QOpenGLWidget", null, [
      [ "DigikamGenericGLViewerPlugin::GLViewerWidget", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationKB", "classDigikamGenericPresentationPlugin_1_1PresentationKB.html", null ]
    ] ],
    [ "QPainter", null, [
      [ "DigikamGenericCalendarPlugin::CalPainter", "classDigikamGenericCalendarPlugin_1_1CalPainter.html", null ]
    ] ],
    [ "QPlainTextEdit", null, [
      [ "Digikam::AdvancedRenameLineEdit", "classDigikam_1_1AdvancedRenameLineEdit.html", null ],
      [ "DigikamGenericMetadataEditPlugin::LimitedTextEdit", "classDigikamGenericMetadataEditPlugin_1_1LimitedTextEdit.html", null ]
    ] ],
    [ "QProgressBar", null, [
      [ "Digikam::DProgressWdg", "classDigikam_1_1DProgressWdg.html", null ]
    ] ],
    [ "QProgressDialog", null, [
      [ "Digikam::DBusyDlg", "classDigikam_1_1DBusyDlg.html", null ]
    ] ],
    [ "QPushButton", null, [
      [ "Digikam::ColorLabelSelector", "classDigikam_1_1ColorLabelSelector.html", null ],
      [ "Digikam::CtrlButton", "classDigikam_1_1CtrlButton.html", null ],
      [ "Digikam::DColorSelector", "classDigikam_1_1DColorSelector.html", null ],
      [ "Digikam::DMultiTabBarButton", "classDigikam_1_1DMultiTabBarButton.html", [
        [ "Digikam::DMultiTabBarTab", "classDigikam_1_1DMultiTabBarTab.html", null ]
      ] ],
      [ "Digikam::DetByClockPhotoButton", "classDigikam_1_1DetByClockPhotoButton.html", null ],
      [ "Digikam::PickLabelSelector", "classDigikam_1_1PickLabelSelector.html", null ],
      [ "DigikamGenericCalendarPlugin::CalMonthWidget", "classDigikamGenericCalendarPlugin_1_1CalMonthWidget.html", null ]
    ] ],
    [ "QReadLocker", null, [
      [ "Digikam::ItemInfoReadLocker", "classDigikam_1_1ItemInfoReadLocker.html", null ]
    ] ],
    [ "QRunnable", null, [
      [ "Digikam::ActionJob", "classDigikam_1_1ActionJob.html", null ]
    ] ],
    [ "QScrollArea", null, [
      [ "Digikam::DExpanderBox", "classDigikam_1_1DExpanderBox.html", [
        [ "Digikam::DExpanderBoxExclusive", "classDigikam_1_1DExpanderBoxExclusive.html", null ],
        [ "Digikam::DRawDecoderWidget", "classDigikam_1_1DRawDecoderWidget.html", null ],
        [ "Digikam::ImportItemPropertiesTab", "classDigikam_1_1ImportItemPropertiesTab.html", null ],
        [ "Digikam::ItemPropertiesTab", "classDigikam_1_1ItemPropertiesTab.html", null ],
        [ "Digikam::ItemSelectionPropertiesTab", "classDigikam_1_1ItemSelectionPropertiesTab.html", null ],
        [ "Digikam::TemplateViewer", "classDigikam_1_1TemplateViewer.html", null ]
      ] ],
      [ "Digikam::EditorToolSettings", "classDigikam_1_1EditorToolSettings.html", [
        [ "DigikamRawImportNativePlugin::RawSettingsBox", "classDigikamRawImportNativePlugin_1_1RawSettingsBox.html", null ]
      ] ],
      [ "Digikam::FuzzySearchView", "classDigikam_1_1FuzzySearchView.html", null ],
      [ "Digikam::ICCPreviewWidget", "classDigikam_1_1ICCPreviewWidget.html", null ],
      [ "Digikam::ImageDialogPreview", "classDigikam_1_1ImageDialogPreview.html", null ],
      [ "Digikam::SetupAlbumView", "classDigikam_1_1SetupAlbumView.html", null ],
      [ "Digikam::SetupCamera", "classDigikam_1_1SetupCamera.html", null ],
      [ "Digikam::SetupCategory", "classDigikam_1_1SetupCategory.html", null ],
      [ "Digikam::SetupCollections", "classDigikam_1_1SetupCollections.html", null ],
      [ "Digikam::SetupDatabase", "classDigikam_1_1SetupDatabase.html", null ],
      [ "Digikam::SetupEditor", "classDigikam_1_1SetupEditor.html", null ],
      [ "Digikam::SetupEditorIface", "classDigikam_1_1SetupEditorIface.html", null ],
      [ "Digikam::SetupICC", "classDigikam_1_1SetupICC.html", null ],
      [ "Digikam::SetupIOFiles", "classDigikam_1_1SetupIOFiles.html", null ],
      [ "Digikam::SetupImageQualitySorter", "classDigikam_1_1SetupImageQualitySorter.html", null ],
      [ "Digikam::SetupLightTable", "classDigikam_1_1SetupLightTable.html", null ],
      [ "Digikam::SetupMetadata", "classDigikam_1_1SetupMetadata.html", null ],
      [ "Digikam::SetupMime", "classDigikam_1_1SetupMime.html", null ],
      [ "Digikam::SetupMisc", "classDigikam_1_1SetupMisc.html", null ],
      [ "Digikam::SetupPlugins", "classDigikam_1_1SetupPlugins.html", null ],
      [ "Digikam::SetupTemplate", "classDigikam_1_1SetupTemplate.html", null ],
      [ "Digikam::SetupToolTip", "classDigikam_1_1SetupToolTip.html", null ],
      [ "Digikam::SetupVersioning", "classDigikam_1_1SetupVersioning.html", null ],
      [ "Digikam::TimeAdjustSettings", "classDigikam_1_1TimeAdjustSettings.html", null ],
      [ "Digikam::TransactionItemView", "classDigikam_1_1TransactionItemView.html", null ],
      [ "ShowFoto::ShowfotoSetupMetadata", "classShowFoto_1_1ShowfotoSetupMetadata.html", null ],
      [ "ShowFoto::ShowfotoSetupMisc", "classShowFoto_1_1ShowfotoSetupMisc.html", null ],
      [ "ShowFoto::ShowfotoSetupPlugins", "classShowFoto_1_1ShowfotoSetupPlugins.html", null ],
      [ "ShowFoto::ShowfotoSetupRaw", "classShowFoto_1_1ShowfotoSetupRaw.html", null ],
      [ "ShowFoto::ShowfotoSetupToolTip", "classShowFoto_1_1ShowfotoSetupToolTip.html", null ]
    ] ],
    [ "QSharedData", null, [
      [ "Digikam::FacePipelineExtendedPackage", "classDigikam_1_1FacePipelineExtendedPackage.html", null ],
      [ "Digikam::GeoIfaceSharedData", "classDigikam_1_1GeoIfaceSharedData.html", null ],
      [ "Digikam::ItemHistoryGraphData", "classDigikam_1_1ItemHistoryGraphData.html", null ],
      [ "Digikam::ItemInfoData", "classDigikam_1_1ItemInfoData.html", null ],
      [ "Digikam::MetaEngineData::Private", "classDigikam_1_1MetaEngineData_1_1Private.html", null ],
      [ "Digikam::TwoProgressItemsContainer", "classDigikam_1_1TwoProgressItemsContainer.html", [
        [ "Digikam::FileActionProgressItemContainer", "classDigikam_1_1FileActionProgressItemContainer.html", null ]
      ] ]
    ] ],
    [ "QSortFilterProxyModel", null, [
      [ "Digikam::AddBookmarkProxyModel", "classDigikam_1_1AddBookmarkProxyModel.html", null ],
      [ "Digikam::AlbumFilterModel", "classDigikam_1_1AlbumFilterModel.html", [
        [ "Digikam::CheckableAlbumFilterModel", "classDigikam_1_1CheckableAlbumFilterModel.html", [
          [ "Digikam::SearchFilterModel", "classDigikam_1_1SearchFilterModel.html", null ],
          [ "Digikam::TagPropertiesFilterModel", "classDigikam_1_1TagPropertiesFilterModel.html", [
            [ "Digikam::TagsManagerFilterModel", "classDigikam_1_1TagsManagerFilterModel.html", null ]
          ] ]
        ] ]
      ] ],
      [ "Digikam::DCategorizedSortFilterProxyModel", "classDigikam_1_1DCategorizedSortFilterProxyModel.html", [
        [ "Digikam::ActionSortFilterProxyModel", "classDigikam_1_1ActionSortFilterProxyModel.html", null ],
        [ "Digikam::ImageSortFilterModel", "classDigikam_1_1ImageSortFilterModel.html", [
          [ "Digikam::ItemFilterModel", "classDigikam_1_1ItemFilterModel.html", null ],
          [ "Digikam::NoDuplicatesItemFilterModel", "classDigikam_1_1NoDuplicatesItemFilterModel.html", null ]
        ] ],
        [ "Digikam::ImportSortFilterModel", "classDigikam_1_1ImportSortFilterModel.html", [
          [ "Digikam::ImportFilterModel", "classDigikam_1_1ImportFilterModel.html", null ],
          [ "Digikam::NoDuplicatesImportFilterModel", "classDigikam_1_1NoDuplicatesImportFilterModel.html", null ]
        ] ],
        [ "ShowFoto::ShowfotoSortFilterModel", "classShowFoto_1_1ShowfotoSortFilterModel.html", [
          [ "ShowFoto::NoDuplicatesShowfotoFilterModel", "classShowFoto_1_1NoDuplicatesShowfotoFilterModel.html", null ],
          [ "ShowFoto::ShowfotoFilterModel", "classShowFoto_1_1ShowfotoFilterModel.html", null ]
        ] ]
      ] ],
      [ "Digikam::GPSItemSortProxyModel", "classDigikam_1_1GPSItemSortProxyModel.html", null ],
      [ "Digikam::TreeProxyModel", "classDigikam_1_1TreeProxyModel.html", null ]
    ] ],
    [ "QSpinBox", null, [
      [ "Digikam::CustomStepsIntSpinBox", "classDigikam_1_1CustomStepsIntSpinBox.html", null ]
    ] ],
    [ "QSplashScreen", null, [
      [ "Digikam::DSplashScreen", "classDigikam_1_1DSplashScreen.html", null ]
    ] ],
    [ "QSplitter", null, [
      [ "Digikam::SidebarSplitter", "classDigikam_1_1SidebarSplitter.html", null ]
    ] ],
    [ "QSqlQuery", null, [
      [ "Digikam::DbEngineSqlQuery", "classDigikam_1_1DbEngineSqlQuery.html", null ]
    ] ],
    [ "QStackedWidget", null, [
      [ "Digikam::DConfigDlgStackedWidget", "classDigikam_1_1DConfigDlgStackedWidget.html", null ],
      [ "Digikam::DPreviewManager", "classDigikam_1_1DPreviewManager.html", null ],
      [ "Digikam::EditorStackView", "classDigikam_1_1EditorStackView.html", null ],
      [ "Digikam::ExifToolWidget", "classDigikam_1_1ExifToolWidget.html", null ],
      [ "Digikam::FileSaveOptionsBox", "classDigikam_1_1FileSaveOptionsBox.html", null ],
      [ "Digikam::ImportStackedView", "classDigikam_1_1ImportStackedView.html", null ],
      [ "Digikam::MediaPlayerView", "classDigikam_1_1MediaPlayerView.html", null ],
      [ "Digikam::StackedView", "classDigikam_1_1StackedView.html", null ],
      [ "Digikam::StatusProgressBar", "classDigikam_1_1StatusProgressBar.html", null ],
      [ "Digikam::ToolSettingsView", "classDigikam_1_1ToolSettingsView.html", null ],
      [ "DigikamGenericSlideShowPlugin::SlideShowLoader", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html", null ]
    ] ],
    [ "QStandardItemModel", null, [
      [ "Digikam::CategorizedItemModel", "classDigikam_1_1CategorizedItemModel.html", [
        [ "Digikam::ActionItemModel", "classDigikam_1_1ActionItemModel.html", null ]
      ] ]
    ] ],
    [ "QStyledItemDelegate", null, [
      [ "Digikam::AlbumTreeViewDelegate", "classDigikam_1_1AlbumTreeViewDelegate.html", null ],
      [ "Digikam::ItemFiltersHistoryItemDelegate", "classDigikam_1_1ItemFiltersHistoryItemDelegate.html", null ],
      [ "Digikam::ThumbnailAligningDelegate", "classDigikam_1_1ThumbnailAligningDelegate.html", null ],
      [ "Digikam::VersionsDelegate", "classDigikam_1_1VersionsDelegate.html", null ]
    ] ],
    [ "QSyntaxHighlighter", null, [
      [ "Digikam::Highlighter", "classDigikam_1_1Highlighter.html", null ]
    ] ],
    [ "QTabBar", null, [
      [ "Digikam::QueuePoolBar", "classDigikam_1_1QueuePoolBar.html", null ]
    ] ],
    [ "QTabWidget", null, [
      [ "Digikam::AlbumSelectTabs", "classDigikam_1_1AlbumSelectTabs.html", null ],
      [ "Digikam::FaceScanWidget", "classDigikam_1_1FaceScanWidget.html", null ],
      [ "Digikam::ItemPropertiesColorsTab", "classDigikam_1_1ItemPropertiesColorsTab.html", null ],
      [ "Digikam::ItemPropertiesMetadataTab", "classDigikam_1_1ItemPropertiesMetadataTab.html", null ],
      [ "Digikam::ItemPropertiesVersionsTab", "classDigikam_1_1ItemPropertiesVersionsTab.html", null ],
      [ "Digikam::QueuePool", "classDigikam_1_1QueuePool.html", null ],
      [ "Digikam::QueueSettingsView", "classDigikam_1_1QueueSettingsView.html", null ],
      [ "Digikam::TemplatePanel", "classDigikam_1_1TemplatePanel.html", null ],
      [ "Digikam::ToolsView", "classDigikam_1_1ToolsView.html", null ]
    ] ],
    [ "QTemporaryFile", null, [
      [ "Digikam::SafeTemporaryFile", "classDigikam_1_1SafeTemporaryFile.html", null ]
    ] ],
    [ "QTextBrowser", null, [
      [ "Digikam::DTextBrowser", "classDigikam_1_1DTextBrowser.html", null ]
    ] ],
    [ "QThread", null, [
      [ "Digikam::ActionThreadBase", "classDigikam_1_1ActionThreadBase.html", [
        [ "Digikam::ActionThread", "classDigikam_1_1ActionThread.html", null ],
        [ "Digikam::DBJobsThread", "classDigikam_1_1DBJobsThread.html", [
          [ "Digikam::AlbumsDBJobsThread", "classDigikam_1_1AlbumsDBJobsThread.html", null ],
          [ "Digikam::DatesDBJobsThread", "classDigikam_1_1DatesDBJobsThread.html", null ],
          [ "Digikam::GPSDBJobsThread", "classDigikam_1_1GPSDBJobsThread.html", null ],
          [ "Digikam::SearchesDBJobsThread", "classDigikam_1_1SearchesDBJobsThread.html", null ],
          [ "Digikam::TagsDBJobsThread", "classDigikam_1_1TagsDBJobsThread.html", null ]
        ] ],
        [ "Digikam::IOJobsThread", "classDigikam_1_1IOJobsThread.html", null ],
        [ "Digikam::MaintenanceThread", "classDigikam_1_1MaintenanceThread.html", null ],
        [ "Digikam::VidSlideThread", "classDigikam_1_1VidSlideThread.html", null ],
        [ "DigikamGenericDNGConverterPlugin::DNGConverterActionThread", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionThread.html", null ],
        [ "DigikamGenericFileCopyPlugin::FCThread", "classDigikamGenericFileCopyPlugin_1_1FCThread.html", null ],
        [ "DigikamGenericMjpegStreamPlugin::MjpegFrameThread", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintThread", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintThread.html", null ],
        [ "DigikamGenericSendByMailPlugin::ImageResizeThread", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html", null ],
        [ "DigikamGenericTimeAdjustPlugin::TimeAdjustThread", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustThread.html", null ]
      ] ],
      [ "Digikam::CameraController", "classDigikam_1_1CameraController.html", null ],
      [ "Digikam::CameraHistoryUpdater", "classDigikam_1_1CameraHistoryUpdater.html", null ],
      [ "Digikam::DBusyThread", "classDigikam_1_1DBusyThread.html", [
        [ "Digikam::CameraAutoDetectThread", "classDigikam_1_1CameraAutoDetectThread.html", null ]
      ] ],
      [ "Digikam::DatabaseCopyThread", "classDigikam_1_1DatabaseCopyThread.html", null ],
      [ "Digikam::DatabaseServer", "classDigikam_1_1DatabaseServer.html", null ],
      [ "Digikam::DbEngineConnectionChecker", "classDigikam_1_1DbEngineConnectionChecker.html", null ],
      [ "Digikam::ImageQualityThread", "classDigikam_1_1ImageQualityThread.html", null ],
      [ "Digikam::ScanController", "classDigikam_1_1ScanController.html", null ],
      [ "Digikam::TrackCorrelatorThread", "classDigikam_1_1TrackCorrelatorThread.html", null ],
      [ "DigikamGenericCalendarPlugin::CalPrinter", "classDigikamGenericCalendarPlugin_1_1CalPrinter.html", null ],
      [ "DigikamGenericDScannerPlugin::SaveImgThread", "classDigikamGenericDScannerPlugin_1_1SaveImgThread.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingThread", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingThread.html", null ],
      [ "DigikamGenericPresentationPlugin::KBImageLoader", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html", null ]
    ] ],
    [ "QTreeView", null, [
      [ "Digikam::AbstractAlbumTreeView", "classDigikam_1_1AbstractAlbumTreeView.html", null ],
      [ "Digikam::DConfigDlgInternal::DConfigDlgTreeView", "classDigikam_1_1DConfigDlgInternal_1_1DConfigDlgTreeView.html", null ],
      [ "Digikam::GPSItemList", "classDigikam_1_1GPSItemList.html", null ],
      [ "Digikam::SetupCollectionTreeView", "classDigikam_1_1SetupCollectionTreeView.html", null ],
      [ "Digikam::TableViewTreeView", "classDigikam_1_1TableViewTreeView.html", null ],
      [ "Digikam::TagMngrListView", "classDigikam_1_1TagMngrListView.html", null ],
      [ "Digikam::VersionsTreeView", "classDigikam_1_1VersionsTreeView.html", null ],
      [ "ShowFoto::ShowfotoFolderViewList", "classShowFoto_1_1ShowfotoFolderViewList.html", null ]
    ] ],
    [ "QTreeWidget", null, [
      [ "Digikam::AssignedListView", "classDigikam_1_1AssignedListView.html", null ],
      [ "Digikam::BlackFrameListView", "classDigikam_1_1BlackFrameListView.html", null ],
      [ "Digikam::CameraFolderView", "classDigikam_1_1CameraFolderView.html", null ],
      [ "Digikam::CameraItemList", "classDigikam_1_1CameraItemList.html", null ],
      [ "Digikam::DBinarySearch", "classDigikam_1_1DBinarySearch.html", null ],
      [ "Digikam::DHistoryView", "classDigikam_1_1DHistoryView.html", null ],
      [ "Digikam::DItemsListView", "classDigikam_1_1DItemsListView.html", null ],
      [ "Digikam::DPluginConfView", "classDigikam_1_1DPluginConfView.html", [
        [ "Digikam::DPluginConfViewBqm", "classDigikam_1_1DPluginConfViewBqm.html", null ],
        [ "Digikam::DPluginConfViewDImg", "classDigikam_1_1DPluginConfViewDImg.html", null ],
        [ "Digikam::DPluginConfViewEditor", "classDigikam_1_1DPluginConfViewEditor.html", null ],
        [ "Digikam::DPluginConfViewGeneric", "classDigikam_1_1DPluginConfViewGeneric.html", null ]
      ] ],
      [ "Digikam::DbKeySelector", "classDigikam_1_1DbKeySelector.html", null ],
      [ "Digikam::DeleteItemList", "classDigikam_1_1DeleteItemList.html", null ],
      [ "Digikam::ExifToolListView", "classDigikam_1_1ExifToolListView.html", null ],
      [ "Digikam::FindDuplicatesAlbum", "classDigikam_1_1FindDuplicatesAlbum.html", null ],
      [ "Digikam::LabelsTreeView", "classDigikam_1_1LabelsTreeView.html", null ],
      [ "Digikam::MetadataListView", "classDigikam_1_1MetadataListView.html", null ],
      [ "Digikam::MetadataSelector", "classDigikam_1_1MetadataSelector.html", null ],
      [ "Digikam::QueueListView", "classDigikam_1_1QueueListView.html", null ],
      [ "Digikam::TemplateList", "classDigikam_1_1TemplateList.html", null ],
      [ "Digikam::ToolsListView", "classDigikam_1_1ToolsListView.html", null ],
      [ "Digikam::WorkflowList", "classDigikam_1_1WorkflowList.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::BracketStackList", "classDigikamGenericExpoBlendingPlugin_1_1BracketStackList.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::EnfuseStackList", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseStackList.html", null ],
      [ "ShowFoto::ShowfotoFolderViewBookmarkList", "classShowFoto_1_1ShowfotoFolderViewBookmarkList.html", null ],
      [ "ShowFoto::ShowfotoStackViewFavoriteList", "classShowFoto_1_1ShowfotoStackViewFavoriteList.html", null ],
      [ "ShowFoto::ShowfotoStackViewList", "classShowFoto_1_1ShowfotoStackViewList.html", null ]
    ] ],
    [ "QTreeWidgetItem", null, [
      [ "Digikam::AdvancedRenameListItem", "classDigikam_1_1AdvancedRenameListItem.html", null ],
      [ "Digikam::AssignedListViewItem", "classDigikam_1_1AssignedListViewItem.html", null ],
      [ "Digikam::BlackFrameListViewItem", "classDigikam_1_1BlackFrameListViewItem.html", null ],
      [ "Digikam::CameraFolderItem", "classDigikam_1_1CameraFolderItem.html", null ],
      [ "Digikam::CameraItem", "classDigikam_1_1CameraItem.html", null ],
      [ "Digikam::DItemsListViewItem", "classDigikam_1_1DItemsListViewItem.html", [
        [ "DigikamGenericDNGConverterPlugin::DNGConverterListViewItem", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html", null ],
        [ "DigikamGenericFlickrPlugin::FlickrListViewItem", "classDigikamGenericFlickrPlugin_1_1FlickrListViewItem.html", null ],
        [ "DigikamGenericImgUrPlugin::ImgurImageListViewItem", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem.html", null ],
        [ "DigikamGenericIpfsPlugin::IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html", null ]
      ] ],
      [ "Digikam::DbHeaderListItem", "classDigikam_1_1DbHeaderListItem.html", null ],
      [ "Digikam::DbKeySelectorItem", "classDigikam_1_1DbKeySelectorItem.html", null ],
      [ "Digikam::DeleteItem", "classDigikam_1_1DeleteItem.html", null ],
      [ "Digikam::ExifToolListViewGroup", "classDigikam_1_1ExifToolListViewGroup.html", null ],
      [ "Digikam::ExifToolListViewItem", "classDigikam_1_1ExifToolListViewItem.html", null ],
      [ "Digikam::FindDuplicatesAlbumItem", "classDigikam_1_1FindDuplicatesAlbumItem.html", null ],
      [ "Digikam::MdKeyListViewItem", "classDigikam_1_1MdKeyListViewItem.html", null ],
      [ "Digikam::MetadataListViewItem", "classDigikam_1_1MetadataListViewItem.html", null ],
      [ "Digikam::MetadataSelectorItem", "classDigikam_1_1MetadataSelectorItem.html", null ],
      [ "Digikam::QueueListViewItem", "classDigikam_1_1QueueListViewItem.html", null ],
      [ "Digikam::TemplateListItem", "classDigikam_1_1TemplateListItem.html", null ],
      [ "Digikam::ToolListViewGroup", "classDigikam_1_1ToolListViewGroup.html", null ],
      [ "Digikam::ToolListViewItem", "classDigikam_1_1ToolListViewItem.html", null ],
      [ "Digikam::WorkflowItem", "classDigikam_1_1WorkflowItem.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::BracketStackItem", "classDigikamGenericExpoBlendingPlugin_1_1BracketStackItem.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::EnfuseStackItem", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseStackItem.html", null ],
      [ "ShowFoto::ShowfotoFolderViewBookmarkItem", "classShowFoto_1_1ShowfotoFolderViewBookmarkItem.html", null ],
      [ "ShowFoto::ShowfotoStackViewFavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html", null ],
      [ "ShowFoto::ShowfotoStackViewItem", "classShowFoto_1_1ShowfotoStackViewItem.html", null ]
    ] ],
    [ "QUndoCommand", null, [
      [ "Digikam::ChangeBookmarkCommand", "classDigikam_1_1ChangeBookmarkCommand.html", null ],
      [ "Digikam::GPSUndoCommand", "classDigikam_1_1GPSUndoCommand.html", null ],
      [ "Digikam::RemoveBookmarksCommand", "classDigikam_1_1RemoveBookmarksCommand.html", [
        [ "Digikam::InsertBookmarksCommand", "classDigikam_1_1InsertBookmarksCommand.html", null ]
      ] ],
      [ "ShowFoto::ShowfotoFolderViewUndo", "classShowFoto_1_1ShowfotoFolderViewUndo.html", null ]
    ] ],
    [ "QUrl", null, [
      [ "Digikam::CoreDbUrl", "classDigikam_1_1CoreDbUrl.html", null ]
    ] ],
    [ "QValidator", null, [
      [ "Digikam::DatePickerValidator", "classDigikam_1_1DatePickerValidator.html", null ]
    ] ],
    [ "QVector", null, [
      [ "Digikam::SparseModelIndexVector", "classDigikam_1_1SparseModelIndexVector.html", null ]
    ] ],
    [ "QWebEnginePage", null, [
      [ "Digikam::HTMLWidgetPage", "classDigikam_1_1HTMLWidgetPage.html", null ]
    ] ],
    [ "QWebEngineView", null, [
      [ "Digikam::HTMLWidget", "classDigikam_1_1HTMLWidget.html", null ],
      [ "Digikam::WebWidget", "classDigikam_1_1WebWidget.html", null ]
    ] ],
    [ "QWebPage", null, [
      [ "DigikamGenericUnifiedPlugin::WSAuthenticationPage", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage.html", null ]
    ] ],
    [ "QWebView", null, [
      [ "Digikam::HTMLWidget", "classDigikam_1_1HTMLWidget.html", null ],
      [ "Digikam::WebWidget", "classDigikam_1_1WebWidget.html", null ],
      [ "Digikam::WelcomePageView", "classDigikam_1_1WelcomePageView.html", null ],
      [ "DigikamGenericUnifiedPlugin::WSAuthenticationPageView", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "Digikam::AbstractSearchGroupContainer", "classDigikam_1_1AbstractSearchGroupContainer.html", [
        [ "Digikam::SearchGroup", "classDigikam_1_1SearchGroup.html", null ],
        [ "Digikam::SearchView", "classDigikam_1_1SearchView.html", null ]
      ] ],
      [ "Digikam::AdvancedMetadataTab", "classDigikam_1_1AdvancedMetadataTab.html", null ],
      [ "Digikam::AdvancedRenameWidget", "classDigikam_1_1AdvancedRenameWidget.html", null ],
      [ "Digikam::AdvancedSettings", "classDigikam_1_1AdvancedSettings.html", null ],
      [ "Digikam::AlbumCustomizer", "classDigikam_1_1AlbumCustomizer.html", null ],
      [ "Digikam::AlbumSelectWidget", "classDigikam_1_1AlbumSelectWidget.html", null ],
      [ "Digikam::AlbumSelectors", "classDigikam_1_1AlbumSelectors.html", null ],
      [ "Digikam::AltLangStrEdit", "classDigikam_1_1AltLangStrEdit.html", null ],
      [ "Digikam::AnimatedClearButton", "classDigikam_1_1AnimatedClearButton.html", null ],
      [ "Digikam::AntiVignettingSettings", "classDigikam_1_1AntiVignettingSettings.html", null ],
      [ "Digikam::BCGSettings", "classDigikam_1_1BCGSettings.html", null ],
      [ "Digikam::BWSepiaSettings", "classDigikam_1_1BWSepiaSettings.html", null ],
      [ "Digikam::BorderSettings", "classDigikam_1_1BorderSettings.html", null ],
      [ "Digikam::CBSettings", "classDigikam_1_1CBSettings.html", null ],
      [ "Digikam::CIETongueWidget", "classDigikam_1_1CIETongueWidget.html", null ],
      [ "Digikam::CaptureWidget", "classDigikam_1_1CaptureWidget.html", null ],
      [ "Digikam::ColorFXSettings", "classDigikam_1_1ColorFXSettings.html", null ],
      [ "Digikam::ColorGradientWidget", "classDigikam_1_1ColorGradientWidget.html", null ],
      [ "Digikam::CurvesBox", "classDigikam_1_1CurvesBox.html", null ],
      [ "Digikam::CurvesSettings", "classDigikam_1_1CurvesSettings.html", null ],
      [ "Digikam::CurvesWidget", "classDigikam_1_1CurvesWidget.html", null ],
      [ "Digikam::DAbstractSliderSpinBox", "classDigikam_1_1DAbstractSliderSpinBox.html", [
        [ "Digikam::DDoubleSliderSpinBox", "classDigikam_1_1DDoubleSliderSpinBox.html", null ],
        [ "Digikam::DSliderSpinBox", "classDigikam_1_1DSliderSpinBox.html", null ]
      ] ],
      [ "Digikam::DArrowClickLabel", "classDigikam_1_1DArrowClickLabel.html", null ],
      [ "Digikam::DComboBox", "classDigikam_1_1DComboBox.html", null ],
      [ "Digikam::DConfigDlgTitle", "classDigikam_1_1DConfigDlgTitle.html", null ],
      [ "Digikam::DConfigDlgView", "classDigikam_1_1DConfigDlgView.html", [
        [ "Digikam::DConfigDlgWdg", "classDigikam_1_1DConfigDlgWdg.html", [
          [ "DigikamGenericMetadataEditPlugin::EXIFEditWidget", "classDigikamGenericMetadataEditPlugin_1_1EXIFEditWidget.html", null ],
          [ "DigikamGenericMetadataEditPlugin::IPTCEditWidget", "classDigikamGenericMetadataEditPlugin_1_1IPTCEditWidget.html", null ],
          [ "DigikamGenericMetadataEditPlugin::XMPEditWidget", "classDigikamGenericMetadataEditPlugin_1_1XMPEditWidget.html", null ]
        ] ]
      ] ],
      [ "Digikam::DDateTable", "classDigikam_1_1DDateTable.html", null ],
      [ "Digikam::DDoubleNumInput", "classDigikam_1_1DDoubleNumInput.html", null ],
      [ "Digikam::DFontProperties", "classDigikam_1_1DFontProperties.html", null ],
      [ "Digikam::DGradientSlider", "classDigikam_1_1DGradientSlider.html", null ],
      [ "Digikam::DIntNumInput", "classDigikam_1_1DIntNumInput.html", null ],
      [ "Digikam::DIntRangeBox", "classDigikam_1_1DIntRangeBox.html", null ],
      [ "Digikam::DItemsList", "classDigikam_1_1DItemsList.html", [
        [ "DigikamGenericDNGConverterPlugin::DNGConverterList", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterList.html", null ],
        [ "DigikamGenericFlickrPlugin::FlickrList", "classDigikamGenericFlickrPlugin_1_1FlickrList.html", null ],
        [ "DigikamGenericImgUrPlugin::ImgurImagesList", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html", null ],
        [ "DigikamGenericIpfsPlugin::IpfsImagesList", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList.html", null ],
        [ "DigikamGenericTimeAdjustPlugin::TimeAdjustList", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList.html", null ]
      ] ],
      [ "Digikam::DLabelExpander", "classDigikam_1_1DLabelExpander.html", null ],
      [ "Digikam::DMultiTabBar", "classDigikam_1_1DMultiTabBar.html", [
        [ "Digikam::Sidebar", "classDigikam_1_1Sidebar.html", null ]
      ] ],
      [ "Digikam::DNGConvertSettings", "classDigikam_1_1DNGConvertSettings.html", null ],
      [ "Digikam::DNGSettings", "classDigikam_1_1DNGSettings.html", null ],
      [ "Digikam::DPluginSetup", "classDigikam_1_1DPluginSetup.html", null ],
      [ "Digikam::DPointSelect", "classDigikam_1_1DPointSelect.html", [
        [ "Digikam::DHueSaturationSelector", "classDigikam_1_1DHueSaturationSelector.html", null ]
      ] ],
      [ "Digikam::DSaveSettingsWidget", "classDigikam_1_1DSaveSettingsWidget.html", null ],
      [ "Digikam::DatabaseSettingsWidget", "classDigikam_1_1DatabaseSettingsWidget.html", null ],
      [ "Digikam::DbKeySelectorView", "classDigikam_1_1DbKeySelectorView.html", null ],
      [ "Digikam::DeleteWidget", "classDigikam_1_1DeleteWidget.html", null ],
      [ "Digikam::DragHandle", "classDigikam_1_1DragHandle.html", null ],
      [ "Digikam::ExifToolConfPanel", "classDigikam_1_1ExifToolConfPanel.html", null ],
      [ "Digikam::ExifToolErrorView", "classDigikam_1_1ExifToolErrorView.html", null ],
      [ "Digikam::ExifToolLoadingView", "classDigikam_1_1ExifToolLoadingView.html", null ],
      [ "Digikam::FileSaveConflictBox", "classDigikam_1_1FileSaveConflictBox.html", null ],
      [ "Digikam::FilmGrainSettings", "classDigikam_1_1FilmGrainSettings.html", null ],
      [ "Digikam::FilterStatusBar", "classDigikam_1_1FilterStatusBar.html", null ],
      [ "Digikam::FiltersHistoryWidget", "classDigikam_1_1FiltersHistoryWidget.html", null ],
      [ "Digikam::FindDuplicatesView", "classDigikam_1_1FindDuplicatesView.html", null ],
      [ "Digikam::FreeRotationSettings", "classDigikam_1_1FreeRotationSettings.html", null ],
      [ "Digikam::FreeSpaceWidget", "classDigikam_1_1FreeSpaceWidget.html", null ],
      [ "Digikam::GPSCorrelatorWidget", "classDigikam_1_1GPSCorrelatorWidget.html", null ],
      [ "Digikam::GPSSearchView", "classDigikam_1_1GPSSearchView.html", null ],
      [ "Digikam::HEIFSettings", "classDigikam_1_1HEIFSettings.html", null ],
      [ "Digikam::HSLSettings", "classDigikam_1_1HSLSettings.html", null ],
      [ "Digikam::HSPreviewWidget", "classDigikam_1_1HSPreviewWidget.html", null ],
      [ "Digikam::HistogramBox", "classDigikam_1_1HistogramBox.html", null ],
      [ "Digikam::HistogramWidget", "classDigikam_1_1HistogramWidget.html", null ],
      [ "Digikam::HotPixelSettings", "classDigikam_1_1HotPixelSettings.html", null ],
      [ "Digikam::ImageGuideWidget", "classDigikam_1_1ImageGuideWidget.html", null ],
      [ "Digikam::ImageQualitySettings", "classDigikam_1_1ImageQualitySettings.html", null ],
      [ "Digikam::ItemPropertiesGPSTab", "classDigikam_1_1ItemPropertiesGPSTab.html", null ],
      [ "Digikam::ItemPropertiesHistoryTab", "classDigikam_1_1ItemPropertiesHistoryTab.html", null ],
      [ "Digikam::JP2KSettings", "classDigikam_1_1JP2KSettings.html", null ],
      [ "Digikam::JPEGSettings", "classDigikam_1_1JPEGSettings.html", null ],
      [ "Digikam::LensFunCameraSelector", "classDigikam_1_1LensFunCameraSelector.html", null ],
      [ "Digikam::LensFunSettings", "classDigikam_1_1LensFunSettings.html", null ],
      [ "Digikam::LocalContrastSettings", "classDigikam_1_1LocalContrastSettings.html", null ],
      [ "Digikam::MapWidget", "classDigikam_1_1MapWidget.html", null ],
      [ "Digikam::MapWidgetView", "classDigikam_1_1MapWidgetView.html", null ],
      [ "Digikam::MetadataSelectorView", "classDigikam_1_1MetadataSelectorView.html", null ],
      [ "Digikam::MetadataStatusBar", "classDigikam_1_1MetadataStatusBar.html", null ],
      [ "Digikam::MetadataWidget", "classDigikam_1_1MetadataWidget.html", [
        [ "Digikam::ExifWidget", "classDigikam_1_1ExifWidget.html", null ],
        [ "Digikam::ICCProfileWidget", "classDigikam_1_1ICCProfileWidget.html", null ],
        [ "Digikam::IptcWidget", "classDigikam_1_1IptcWidget.html", null ],
        [ "Digikam::MakerNoteWidget", "classDigikam_1_1MakerNoteWidget.html", null ],
        [ "Digikam::XmpWidget", "classDigikam_1_1XmpWidget.html", null ]
      ] ],
      [ "Digikam::MixerSettings", "classDigikam_1_1MixerSettings.html", null ],
      [ "Digikam::MonthWidget", "classDigikam_1_1MonthWidget.html", null ],
      [ "Digikam::NRSettings", "classDigikam_1_1NRSettings.html", null ],
      [ "Digikam::PGFSettings", "classDigikam_1_1PGFSettings.html", null ],
      [ "Digikam::PNGSettings", "classDigikam_1_1PNGSettings.html", null ],
      [ "Digikam::PanIconWidget", "classDigikam_1_1PanIconWidget.html", null ],
      [ "Digikam::PreviewToolBar", "classDigikam_1_1PreviewToolBar.html", null ],
      [ "Digikam::RGWidget", "classDigikam_1_1RGWidget.html", null ],
      [ "Digikam::RatingWidget", "classDigikam_1_1RatingWidget.html", [
        [ "Digikam::RatingComboBoxWidget", "classDigikam_1_1RatingComboBoxWidget.html", null ],
        [ "Digikam::RatingFilterWidget", "classDigikam_1_1RatingFilterWidget.html", null ]
      ] ],
      [ "Digikam::RedEyeCorrectionSettings", "classDigikam_1_1RedEyeCorrectionSettings.html", null ],
      [ "Digikam::RenameCustomizer", "classDigikam_1_1RenameCustomizer.html", null ],
      [ "Digikam::ScriptingSettings", "classDigikam_1_1ScriptingSettings.html", null ],
      [ "Digikam::SearchFieldGroup", "classDigikam_1_1SearchFieldGroup.html", null ],
      [ "Digikam::SearchFieldGroupLabel", "classDigikam_1_1SearchFieldGroupLabel.html", null ],
      [ "Digikam::SearchGroupLabel", "classDigikam_1_1SearchGroupLabel.html", null ],
      [ "Digikam::SearchTabHeader", "classDigikam_1_1SearchTabHeader.html", null ],
      [ "Digikam::SearchViewBottomBar", "classDigikam_1_1SearchViewBottomBar.html", null ],
      [ "Digikam::SearchWindow", "classDigikam_1_1SearchWindow.html", null ],
      [ "Digikam::SharpSettings", "classDigikam_1_1SharpSettings.html", null ],
      [ "Digikam::SidebarWidget", "classDigikam_1_1SidebarWidget.html", null ],
      [ "Digikam::SketchWidget", "classDigikam_1_1SketchWidget.html", null ],
      [ "Digikam::SlideVideo", "classDigikam_1_1SlideVideo.html", null ],
      [ "Digikam::StyleSheetDebugger", "classDigikam_1_1StyleSheetDebugger.html", null ],
      [ "Digikam::SubjectWidget", "classDigikam_1_1SubjectWidget.html", [
        [ "Digikam::SubjectEdit", "classDigikam_1_1SubjectEdit.html", null ],
        [ "DigikamGenericMetadataEditPlugin::IPTCSubjects", "classDigikamGenericMetadataEditPlugin_1_1IPTCSubjects.html", null ],
        [ "DigikamGenericMetadataEditPlugin::XMPSubjects", "classDigikamGenericMetadataEditPlugin_1_1XMPSubjects.html", null ]
      ] ],
      [ "Digikam::SystemSettingsWidget", "classDigikam_1_1SystemSettingsWidget.html", null ],
      [ "Digikam::TIFFSettings", "classDigikam_1_1TIFFSettings.html", null ],
      [ "Digikam::TableView", "classDigikam_1_1TableView.html", null ],
      [ "Digikam::TableViewColumnConfigurationWidget", "classDigikam_1_1TableViewColumnConfigurationWidget.html", [
        [ "Digikam::TableViewColumns::ColumnFileConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnFileConfigurationWidget.html", null ],
        [ "Digikam::TableViewColumns::ColumnGeoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnGeoConfigurationWidget.html", null ],
        [ "Digikam::TableViewColumns::ColumnPhotoConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnPhotoConfigurationWidget.html", null ]
      ] ],
      [ "Digikam::TagList", "classDigikam_1_1TagList.html", null ],
      [ "Digikam::TagPropWidget", "classDigikam_1_1TagPropWidget.html", null ],
      [ "Digikam::TextureSettings", "classDigikam_1_1TextureSettings.html", null ],
      [ "Digikam::TimeLineWidget", "classDigikam_1_1TimeLineWidget.html", null ],
      [ "Digikam::TrashView", "classDigikam_1_1TrashView.html", null ],
      [ "Digikam::VersionsWidget", "classDigikam_1_1VersionsWidget.html", null ],
      [ "Digikam::WBSettings", "classDigikam_1_1WBSettings.html", null ],
      [ "Digikam::WSSettingsWidget", "classDigikam_1_1WSSettingsWidget.html", [
        [ "DigikamGenericBoxPlugin::BOXWidget", "classDigikamGenericBoxPlugin_1_1BOXWidget.html", null ],
        [ "DigikamGenericDropBoxPlugin::DBWidget", "classDigikamGenericDropBoxPlugin_1_1DBWidget.html", null ],
        [ "DigikamGenericFaceBookPlugin::FbWidget", "classDigikamGenericFaceBookPlugin_1_1FbWidget.html", null ],
        [ "DigikamGenericFlickrPlugin::FlickrWidget", "classDigikamGenericFlickrPlugin_1_1FlickrWidget.html", null ],
        [ "DigikamGenericGoogleServicesPlugin::GSWidget", "classDigikamGenericGoogleServicesPlugin_1_1GSWidget.html", null ],
        [ "DigikamGenericINatPlugin::INatWidget", "classDigikamGenericINatPlugin_1_1INatWidget.html", null ],
        [ "DigikamGenericImageShackPlugin::ImageShackWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget.html", null ],
        [ "DigikamGenericOneDrivePlugin::ODWidget", "classDigikamGenericOneDrivePlugin_1_1ODWidget.html", null ],
        [ "DigikamGenericPinterestPlugin::PWidget", "classDigikamGenericPinterestPlugin_1_1PWidget.html", null ],
        [ "DigikamGenericRajcePlugin::RajceWidget", "classDigikamGenericRajcePlugin_1_1RajceWidget.html", null ],
        [ "DigikamGenericTwitterPlugin::TwWidget", "classDigikamGenericTwitterPlugin_1_1TwWidget.html", null ],
        [ "DigikamGenericYFPlugin::YFWidget", "classDigikamGenericYFPlugin_1_1YFWidget.html", null ]
      ] ],
      [ "DigikamEditorInsertTextToolPlugin::InsertTextWidget", "classDigikamEditorInsertTextToolPlugin_1_1InsertTextWidget.html", null ],
      [ "DigikamEditorPerspectiveToolPlugin::PerspectiveWidget", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveWidget.html", null ],
      [ "DigikamEditorPrintToolPlugin::PrintOptionsPage", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html", null ],
      [ "DigikamEditorRatioCropToolPlugin::RatioCropWidget", "classDigikamEditorRatioCropToolPlugin_1_1RatioCropWidget.html", null ],
      [ "DigikamGenericCalendarPlugin::CalTemplate", "classDigikamGenericCalendarPlugin_1_1CalTemplate.html", null ],
      [ "DigikamGenericCalendarPlugin::CalWidget", "classDigikamGenericCalendarPlugin_1_1CalWidget.html", null ],
      [ "DigikamGenericExpoBlendingPlugin::EnfuseSettingsWidget", "classDigikamGenericExpoBlendingPlugin_1_1EnfuseSettingsWidget.html", null ],
      [ "DigikamGenericFileCopyPlugin::FCExportWidget", "classDigikamGenericFileCopyPlugin_1_1FCExportWidget.html", null ],
      [ "DigikamGenericFileTransferPlugin::FTExportWidget", "classDigikamGenericFileTransferPlugin_1_1FTExportWidget.html", null ],
      [ "DigikamGenericFileTransferPlugin::FTImportWidget", "classDigikamGenericFileTransferPlugin_1_1FTImportWidget.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::GPSItemDetails", "classDigikamGenericGeolocationEditPlugin_1_1GPSItemDetails.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::KmlWidget", "classDigikamGenericGeolocationEditPlugin_1_1KmlWidget.html", null ],
      [ "DigikamGenericGeolocationEditPlugin::SearchWidget", "classDigikamGenericGeolocationEditPlugin_1_1SearchWidget.html", null ],
      [ "DigikamGenericHtmlGalleryPlugin::InvisibleButtonGroup", "classDigikamGenericHtmlGalleryPlugin_1_1InvisibleButtonGroup.html", null ],
      [ "DigikamGenericMediaWikiPlugin::MediaWikiWidget", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiWidget.html", null ],
      [ "DigikamGenericMetadataEditPlugin::AltLangStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html", null ],
      [ "DigikamGenericMetadataEditPlugin::EXIFAdjust", "classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust.html", null ],
      [ "DigikamGenericMetadataEditPlugin::EXIFCaption", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html", null ],
      [ "DigikamGenericMetadataEditPlugin::EXIFDateTime", "classDigikamGenericMetadataEditPlugin_1_1EXIFDateTime.html", null ],
      [ "DigikamGenericMetadataEditPlugin::EXIFDevice", "classDigikamGenericMetadataEditPlugin_1_1EXIFDevice.html", null ],
      [ "DigikamGenericMetadataEditPlugin::EXIFLens", "classDigikamGenericMetadataEditPlugin_1_1EXIFLens.html", null ],
      [ "DigikamGenericMetadataEditPlugin::EXIFLight", "classDigikamGenericMetadataEditPlugin_1_1EXIFLight.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCCategories", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCContent", "classDigikamGenericMetadataEditPlugin_1_1IPTCContent.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCCredits", "classDigikamGenericMetadataEditPlugin_1_1IPTCCredits.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCEnvelope", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCKeywords", "classDigikamGenericMetadataEditPlugin_1_1IPTCKeywords.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCOrigin", "classDigikamGenericMetadataEditPlugin_1_1IPTCOrigin.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCProperties", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html", null ],
      [ "DigikamGenericMetadataEditPlugin::IPTCStatus", "classDigikamGenericMetadataEditPlugin_1_1IPTCStatus.html", null ],
      [ "DigikamGenericMetadataEditPlugin::MultiStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html", null ],
      [ "DigikamGenericMetadataEditPlugin::MultiValuesEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiValuesEdit.html", null ],
      [ "DigikamGenericMetadataEditPlugin::ObjectAttributesEdit", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPCategories", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPContent", "classDigikamGenericMetadataEditPlugin_1_1XMPContent.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPCredits", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPKeywords", "classDigikamGenericMetadataEditPlugin_1_1XMPKeywords.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPOrigin", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPProperties", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html", null ],
      [ "DigikamGenericMetadataEditPlugin::XMPStatus", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAudioPage", "classDigikamGenericPresentationPlugin_1_1PresentationAudioPage.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationAudioWidget", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationCaptionPage", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationMainPage", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html", null ],
      [ "DigikamGenericPresentationPlugin::PresentationWidget", "classDigikamGenericPresentationPlugin_1_1PresentationWidget.html", null ],
      [ "DigikamGenericPrintCreatorPlugin::AdvPrintCropFrame", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropFrame.html", null ],
      [ "DigikamGenericSlideShowPlugin::SlideEnd", "classDigikamGenericSlideShowPlugin_1_1SlideEnd.html", null ],
      [ "DigikamGenericSlideShowPlugin::SlideError", "classDigikamGenericSlideShowPlugin_1_1SlideError.html", null ],
      [ "DigikamGenericSlideShowPlugin::SlideImage", "classDigikamGenericSlideShowPlugin_1_1SlideImage.html", null ],
      [ "DigikamGenericSlideShowPlugin::SlideOSD", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html", null ],
      [ "DigikamGenericSlideShowPlugin::SlideProperties", "classDigikamGenericSlideShowPlugin_1_1SlideProperties.html", null ],
      [ "DigikamGenericSmugPlugin::SmugWidget", "classDigikamGenericSmugPlugin_1_1SmugWidget.html", null ],
      [ "ShowFoto::ShowfotoFolderViewBookmarks", "classShowFoto_1_1ShowfotoFolderViewBookmarks.html", null ],
      [ "ShowFoto::ShowfotoFolderViewSideBar", "classShowFoto_1_1ShowfotoFolderViewSideBar.html", null ],
      [ "ShowFoto::ShowfotoStackViewFavorites", "classShowFoto_1_1ShowfotoStackViewFavorites.html", null ],
      [ "ShowFoto::ShowfotoStackViewSideBar", "classShowFoto_1_1ShowfotoStackViewSideBar.html", null ]
    ] ],
    [ "QWidgetAction", null, [
      [ "Digikam::DLogoAction", "classDigikam_1_1DLogoAction.html", null ]
    ] ],
    [ "QWizard", null, [
      [ "Digikam::DWizardDlg", "classDigikam_1_1DWizardDlg.html", [
        [ "DigikamGenericCalendarPlugin::CalWizard", "classDigikamGenericCalendarPlugin_1_1CalWizard.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingWizard", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingWizard.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLWizard", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html", null ],
        [ "DigikamGenericJAlbumPlugin::JAlbumWizard", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoWizard", "classDigikamGenericPanoramaPlugin_1_1PanoWizard.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintWizard", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintWizard.html", null ],
        [ "DigikamGenericSendByMailPlugin::MailWizard", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSWizard", "classDigikamGenericUnifiedPlugin_1_1WSWizard.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideWizard", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideWizard.html", null ]
      ] ],
      [ "Digikam::FirstRunDlg", "classDigikam_1_1FirstRunDlg.html", null ]
    ] ],
    [ "QWizardPage", null, [
      [ "Digikam::DWizardPage", "classDigikam_1_1DWizardPage.html", [
        [ "Digikam::CollectionPage", "classDigikam_1_1CollectionPage.html", null ],
        [ "Digikam::DatabasePage", "classDigikam_1_1DatabasePage.html", null ],
        [ "Digikam::MetadataPage", "classDigikam_1_1MetadataPage.html", null ],
        [ "Digikam::MigrateFromDigikam4Page", "classDigikam_1_1MigrateFromDigikam4Page.html", null ],
        [ "Digikam::OpenFilePage", "classDigikam_1_1OpenFilePage.html", null ],
        [ "Digikam::PreviewPage", "classDigikam_1_1PreviewPage.html", null ],
        [ "Digikam::RawPage", "classDigikam_1_1RawPage.html", null ],
        [ "Digikam::StartScanPage", "classDigikam_1_1StartScanPage.html", null ],
        [ "Digikam::TooltipsPage", "classDigikam_1_1TooltipsPage.html", null ],
        [ "Digikam::WelcomePage", "classDigikam_1_1WelcomePage.html", null ],
        [ "DigikamGenericCalendarPlugin::CalIntroPage", "classDigikamGenericCalendarPlugin_1_1CalIntroPage.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingIntroPage", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingIntroPage.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingLastPage", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingLastPage.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::ExpoBlendingPreProcessPage", "classDigikamGenericExpoBlendingPlugin_1_1ExpoBlendingPreProcessPage.html", null ],
        [ "DigikamGenericExpoBlendingPlugin::ItemsPage", "classDigikamGenericExpoBlendingPlugin_1_1ItemsPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLFinalPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLFinalPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLImageSettingsPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLImageSettingsPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLIntroPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLIntroPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLOutputPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLOutputPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLParametersPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLParametersPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLSelectionPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLSelectionPage.html", null ],
        [ "DigikamGenericHtmlGalleryPlugin::HTMLThemePage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLThemePage.html", null ],
        [ "DigikamGenericJAlbumPlugin::JAlbumFinalPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html", null ],
        [ "DigikamGenericJAlbumPlugin::JAlbumIntroPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumIntroPage.html", null ],
        [ "DigikamGenericJAlbumPlugin::JAlbumOutputPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumOutputPage.html", null ],
        [ "DigikamGenericJAlbumPlugin::JAlbumSelectionPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumSelectionPage.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoIntroPage", "classDigikamGenericPanoramaPlugin_1_1PanoIntroPage.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoItemsPage", "classDigikamGenericPanoramaPlugin_1_1PanoItemsPage.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoLastPage", "classDigikamGenericPanoramaPlugin_1_1PanoLastPage.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoOptimizePage", "classDigikamGenericPanoramaPlugin_1_1PanoOptimizePage.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoPreProcessPage", "classDigikamGenericPanoramaPlugin_1_1PanoPreProcessPage.html", null ],
        [ "DigikamGenericPanoramaPlugin::PanoPreviewPage", "classDigikamGenericPanoramaPlugin_1_1PanoPreviewPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintAlbumsPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintAlbumsPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintCaptionPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCaptionPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintCropPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintFinalPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintFinalPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintIntroPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintIntroPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintOutputPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintOutputPage.html", null ],
        [ "DigikamGenericPrintCreatorPlugin::AdvPrintPhotoPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintPhotoPage.html", null ],
        [ "DigikamGenericSendByMailPlugin::MailAlbumsPage", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html", null ],
        [ "DigikamGenericSendByMailPlugin::MailFinalPage", "classDigikamGenericSendByMailPlugin_1_1MailFinalPage.html", null ],
        [ "DigikamGenericSendByMailPlugin::MailImagesPage", "classDigikamGenericSendByMailPlugin_1_1MailImagesPage.html", null ],
        [ "DigikamGenericSendByMailPlugin::MailIntroPage", "classDigikamGenericSendByMailPlugin_1_1MailIntroPage.html", null ],
        [ "DigikamGenericSendByMailPlugin::MailSettingsPage", "classDigikamGenericSendByMailPlugin_1_1MailSettingsPage.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSAlbumsPage", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSAuthenticationWizard", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationWizard.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSFinalPage", "classDigikamGenericUnifiedPlugin_1_1WSFinalPage.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSImagesPage", "classDigikamGenericUnifiedPlugin_1_1WSImagesPage.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSIntroPage", "classDigikamGenericUnifiedPlugin_1_1WSIntroPage.html", null ],
        [ "DigikamGenericUnifiedPlugin::WSSettingsPage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideAlbumsPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideAlbumsPage.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideFinalPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideFinalPage.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideImagesPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideImagesPage.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideIntroPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideIntroPage.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideOutputPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideOutputPage.html", null ],
        [ "DigikamGenericVideoSlideShowPlugin::VidSlideVideoPage", "classDigikamGenericVideoSlideShowPlugin_1_1VidSlideVideoPage.html", null ]
      ] ]
    ] ],
    [ "QWriteLocker", null, [
      [ "Digikam::ItemInfoWriteLocker", "classDigikam_1_1ItemInfoWriteLocker.html", null ]
    ] ],
    [ "QXmlDefaultHandler", null, [
      [ "Digikam::TrackReader", "classDigikam_1_1TrackReader.html", null ]
    ] ],
    [ "QXmlStreamReader", null, [
      [ "Digikam::SearchXmlReader", "classDigikam_1_1SearchXmlReader.html", [
        [ "Digikam::KeywordSearchReader", "classDigikam_1_1KeywordSearchReader.html", null ],
        [ "Digikam::SearchXmlCachingReader", "classDigikam_1_1SearchXmlCachingReader.html", null ]
      ] ],
      [ "Digikam::XbelReader", "classDigikam_1_1XbelReader.html", null ]
    ] ],
    [ "QXmlStreamWriter", null, [
      [ "Digikam::SearchXmlWriter", "classDigikam_1_1SearchXmlWriter.html", [
        [ "Digikam::KeywordSearchWriter", "classDigikam_1_1KeywordSearchWriter.html", null ]
      ] ],
      [ "Digikam::XbelWriter", "classDigikam_1_1XbelWriter.html", null ]
    ] ],
    [ "ref_pic_set", "classref__pic__set.html", null ],
    [ "sao_info", "structsao__info.html", null ],
    [ "scaling_list_data", "structscaling__list__data.html", null ],
    [ "scan_position", "structscan__position.html", null ],
    [ "sei_decoded_picture_hash", "structsei__decoded__picture__hash.html", null ],
    [ "sei_message", "structsei__message.html", null ],
    [ "seq_parameter_set", "classseq__parameter__set.html", null ],
    [ "ShowFoto::Showfoto::Private", "classShowFoto_1_1Showfoto_1_1Private.html", null ],
    [ "ShowFoto::ShowfotoItemInfo", "classShowFoto_1_1ShowfotoItemInfo.html", null ],
    [ "ShowFoto::ShowfotoItemSortSettings", "classShowFoto_1_1ShowfotoItemSortSettings.html", null ],
    [ "ShowFoto::ShowfotoItemViewDelegatePrivate", "classShowFoto_1_1ShowfotoItemViewDelegatePrivate.html", [
      [ "ShowFoto::ShowfotoDelegate::ShowfotoDelegatePrivate", "classShowFoto_1_1ShowfotoDelegate_1_1ShowfotoDelegatePrivate.html", [
        [ "ShowFoto::ShowfotoNormalDelegatePrivate", "classShowFoto_1_1ShowfotoNormalDelegatePrivate.html", null ],
        [ "ShowFoto::ShowfotoThumbnailDelegatePrivate", "classShowFoto_1_1ShowfotoThumbnailDelegatePrivate.html", null ]
      ] ]
    ] ],
    [ "slice_segment_header", "classslice__segment__header.html", null ],
    [ "slice_unit", "classslice__unit.html", null ],
    [ "small_image_buffer", "classsmall__image__buffer.html", null ],
    [ "sop_creator_trivial_low_delay::params", "structsop__creator__trivial__low__delay_1_1params.html", null ],
    [ "sps_range_extension", "classsps__range__extension.html", null ],
    [ "std::enable_shared_from_this", null, [
      [ "heif::HeifPixelImage", "classheif_1_1HeifPixelImage.html", null ]
    ] ],
    [ "thread_context", "classthread__context.html", null ],
    [ "thread_pool", "classthread__pool.html", null ],
    [ "thread_task", "classthread__task.html", [
      [ "thread_task_ctb_row", "classthread__task__ctb__row.html", null ],
      [ "thread_task_slice_segment", "classthread__task__slice__segment.html", null ]
    ] ],
    [ "ThreadWeaver::Job", null, [
      [ "DigikamGenericPanoramaPlugin::PanoTask", "classDigikamGenericPanoramaPlugin_1_1PanoTask.html", [
        [ "DigikamGenericPanoramaPlugin::CommandTask", "classDigikamGenericPanoramaPlugin_1_1CommandTask.html", [
          [ "DigikamGenericPanoramaPlugin::AutoCropTask", "classDigikamGenericPanoramaPlugin_1_1AutoCropTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CompileMKStepTask", "classDigikamGenericPanoramaPlugin_1_1CompileMKStepTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CompileMKTask", "classDigikamGenericPanoramaPlugin_1_1CompileMKTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CpCleanTask", "classDigikamGenericPanoramaPlugin_1_1CpCleanTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CpFindTask", "classDigikamGenericPanoramaPlugin_1_1CpFindTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::CreateMKTask", "classDigikamGenericPanoramaPlugin_1_1CreateMKTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::HuginExecutorTask", "classDigikamGenericPanoramaPlugin_1_1HuginExecutorTask.html", null ],
          [ "DigikamGenericPanoramaPlugin::OptimisationTask", "classDigikamGenericPanoramaPlugin_1_1OptimisationTask.html", null ]
        ] ],
        [ "DigikamGenericPanoramaPlugin::CopyFilesTask", "classDigikamGenericPanoramaPlugin_1_1CopyFilesTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::CreateFinalPtoTask", "classDigikamGenericPanoramaPlugin_1_1CreateFinalPtoTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::CreatePreviewTask", "classDigikamGenericPanoramaPlugin_1_1CreatePreviewTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::CreatePtoTask", "classDigikamGenericPanoramaPlugin_1_1CreatePtoTask.html", null ],
        [ "DigikamGenericPanoramaPlugin::PreProcessTask", "classDigikamGenericPanoramaPlugin_1_1PreProcessTask.html", null ]
      ] ]
    ] ],
    [ "Ui::AdvPrintCustomLayout", null, [
      [ "DigikamGenericPrintCreatorPlugin::AdvPrintCustomLayoutDlg", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCustomLayoutDlg.html", null ]
    ] ],
    [ "Ui::PresentationAdvPage", null, [
      [ "DigikamGenericPresentationPlugin::PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html", null ]
    ] ],
    [ "Ui::PresentationAudioPage", null, [
      [ "DigikamGenericPresentationPlugin::PresentationAudioPage", "classDigikamGenericPresentationPlugin_1_1PresentationAudioPage.html", null ]
    ] ],
    [ "Ui::PresentationAudioWidget", null, [
      [ "DigikamGenericPresentationPlugin::PresentationAudioWidget", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html", null ]
    ] ],
    [ "Ui::PresentationCaptionPage", null, [
      [ "DigikamGenericPresentationPlugin::PresentationCaptionPage", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage.html", null ]
    ] ],
    [ "Ui::PresentationCtrlWidget", null, [
      [ "DigikamGenericPresentationPlugin::PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html", null ]
    ] ],
    [ "Ui::PresentationMainPage", null, [
      [ "DigikamGenericPresentationPlugin::PresentationMainPage", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html", null ]
    ] ],
    [ "video_parameter_set", "classvideo__parameter__set.html", null ],
    [ "video_usability_information", "classvideo__usability__information.html", null ],
    [ "WSTalker", null, [
      [ "DigikamGenericFaceBookPlugin::FbTalker", "classDigikamGenericFaceBookPlugin_1_1FbTalker.html", null ]
    ] ],
    [ "YFAuth::CCryptoProviderRSA", "classYFAuth_1_1CCryptoProviderRSA.html", null ],
    [ "YFAuth::public_key", "classYFAuth_1_1public__key.html", [
      [ "YFAuth::private_key", "classYFAuth_1_1private__key.html", null ]
    ] ],
    [ "YFAuth::vlong", "classYFAuth_1_1vlong.html", null ]
];