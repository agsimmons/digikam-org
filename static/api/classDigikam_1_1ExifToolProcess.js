var classDigikam_1_1ExifToolProcess =
[
    [ "Private", "classDigikam_1_1ExifToolProcess_1_1Private.html", "classDigikam_1_1ExifToolProcess_1_1Private" ],
    [ "Action", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781", [
      [ "LOAD_METADATA", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781ad7a1da63059b3908ad7eb9306de7282b", null ],
      [ "LOAD_CHUNKS", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a17d7a97506af98f91683605fbf32c2bc", null ],
      [ "APPLY_CHANGES", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a8b243224f93e8e37d8ca3a367d4287c3", null ],
      [ "APPLY_CHANGES_EXV", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a87ff6591ec046607af7f8cfabb3214bf", null ],
      [ "READ_FORMATS", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781ac466135e00b7bd21597ef3e5cadf906b", null ],
      [ "WRITE_FORMATS", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781ae3cfdcc734d95e3d685973a48c3e6211", null ],
      [ "TRANSLATIONS_LIST", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781acc129359de26a996bad01a0fba7511b9", null ],
      [ "TAGS_DATABASE", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781ad6af5c22c92a2ba2a28a767ccf5a4594", null ],
      [ "VERSION_STRING", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a39e5f979a8b97dc0bc3d39ce1ee44f9b", null ],
      [ "COPY_TAGS", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a6b4ffd8ec926ad80ddb0599e6a6b6991", null ],
      [ "TRANS_TAGS", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a97345c919d0e1efd211ee86f27242230", null ],
      [ "NO_ACTION", "classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781ae2764b2767b142b1da9eabab32852a24", null ]
    ] ],
    [ "CopyTagsSource", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08", [
      [ "COPY_EXIF", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08a2729e89172a94c268ffca224826f8c66", null ],
      [ "COPY_MAKERNOTES", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08a81ea8200e7a965799978a91357e38302", null ],
      [ "COPY_IPTC", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08a97c8c15188396e6b08563b9014452419", null ],
      [ "COPY_XMP", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08a78901f2c4ce3a17b80c4c86eea5c7e0f", null ],
      [ "COPY_ICC", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08acb56e84c1cd372a5a16da913b36cf967", null ],
      [ "COPY_ALL", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08a8d6cb8a373f46114c5a062e4df4c9615", null ],
      [ "COPY_NONE", "classDigikam_1_1ExifToolProcess.html#a3f3c84617a132e8f2c727ac8ffebbe08a141c501329934b871fd0fab11b99056a", null ]
    ] ],
    [ "TranslateTagsOps", "classDigikam_1_1ExifToolProcess.html#a25cf608f2b94bca9264aafcedb5c57b2", [
      [ "TRANS_ALL_XMP", "classDigikam_1_1ExifToolProcess.html#a25cf608f2b94bca9264aafcedb5c57b2ab622bce9fb2b7a408294de502e4a6285", null ],
      [ "TRANS_ALL_IPTC", "classDigikam_1_1ExifToolProcess.html#a25cf608f2b94bca9264aafcedb5c57b2ae830e1e748d29518379d3e869c04cace", null ],
      [ "TRANS_ALL_EXIF", "classDigikam_1_1ExifToolProcess.html#a25cf608f2b94bca9264aafcedb5c57b2a620c28711b2a49947eeec87025503483", null ]
    ] ],
    [ "WritingTagsMode", "classDigikam_1_1ExifToolProcess.html#a4ac57f58c5dad60127527f9a650ccd3d", [
      [ "WRITE_EXISTING_TAGS", "classDigikam_1_1ExifToolProcess.html#a4ac57f58c5dad60127527f9a650ccd3dae660fb67315540571d59f65951faf420", null ],
      [ "CREATE_NEW_TAGS", "classDigikam_1_1ExifToolProcess.html#a4ac57f58c5dad60127527f9a650ccd3da8b85abf8db1b8d5d0849a7258eb7baae", null ],
      [ "CREATE_NEW_GROUPS", "classDigikam_1_1ExifToolProcess.html#a4ac57f58c5dad60127527f9a650ccd3dac99d5627765d3d2f33067705d35fd1bc", null ],
      [ "ALL_MODES", "classDigikam_1_1ExifToolProcess.html#a4ac57f58c5dad60127527f9a650ccd3daa9d74d7472644d0d84530d7067daa589", null ]
    ] ],
    [ "ExifToolProcess", "classDigikam_1_1ExifToolProcess.html#a88d7ec0889b48ce603b013658b55c4c3", null ],
    [ "~ExifToolProcess", "classDigikam_1_1ExifToolProcess.html#a50d47061bca9419ba813dda4feddf768", null ],
    [ "checkExifToolProgram", "classDigikam_1_1ExifToolProcess.html#a1d3bf14cf34f50c794369f85301d129a", null ],
    [ "command", "classDigikam_1_1ExifToolProcess.html#a91f8b75ba49eeffbe0361764f59de62c", null ],
    [ "error", "classDigikam_1_1ExifToolProcess.html#afd6ad38eb726f432855c83d9ce12de85", null ],
    [ "errorString", "classDigikam_1_1ExifToolProcess.html#a2bc7185deb5043bac4ef14b49a0892bb", null ],
    [ "exitCode", "classDigikam_1_1ExifToolProcess.html#a7fa559dad9ec81ee379ee4d1d65f387a", null ],
    [ "exitStatus", "classDigikam_1_1ExifToolProcess.html#ac57fe0950507fa9fdb49ac101c94ecbc", null ],
    [ "isBusy", "classDigikam_1_1ExifToolProcess.html#a643180ffa0983a29ac477ec67e78ed6a", null ],
    [ "isRunning", "classDigikam_1_1ExifToolProcess.html#a3f1d5827b321a1c0b21d53422127833f", null ],
    [ "kill", "classDigikam_1_1ExifToolProcess.html#a4c3c8a17162b0370ff1bd29482101adb", null ],
    [ "processId", "classDigikam_1_1ExifToolProcess.html#a29a65c714e3b0985b29c2c224ff7ed41", null ],
    [ "program", "classDigikam_1_1ExifToolProcess.html#a2e5243553831f957f03593ca494e5a69", null ],
    [ "setProgram", "classDigikam_1_1ExifToolProcess.html#a5fb3ae36468e934c21131bde68037c50", null ],
    [ "signalCmdCompleted", "classDigikam_1_1ExifToolProcess.html#aa834a761e2c84592ad93f47c6baa5716", null ],
    [ "signalErrorOccurred", "classDigikam_1_1ExifToolProcess.html#a7f84dfca7f52d83a5d035f4a5a656810", null ],
    [ "signalFinished", "classDigikam_1_1ExifToolProcess.html#a9af8499fdbddf6e259e2969e0b929b7c", null ],
    [ "signalStarted", "classDigikam_1_1ExifToolProcess.html#a6ad18842f6a985ae79fc65fc5a236931", null ],
    [ "signalStateChanged", "classDigikam_1_1ExifToolProcess.html#a7e09d62869d82a54ed2403db09ac01c4", null ],
    [ "start", "classDigikam_1_1ExifToolProcess.html#ab3c2dae29662ee63318a9e3a309b4a95", null ],
    [ "state", "classDigikam_1_1ExifToolProcess.html#af43963b887808d95ea2c0a5e67f109a9", null ],
    [ "terminate", "classDigikam_1_1ExifToolProcess.html#ad580c012d56dd639d8f7f46ad4031bef", null ],
    [ "waitForFinished", "classDigikam_1_1ExifToolProcess.html#a25c78d5e416a03275feda1fbb694b50d", null ],
    [ "waitForStarted", "classDigikam_1_1ExifToolProcess.html#a70c72e1693645fdf76d25c99c7f6650c", null ]
];