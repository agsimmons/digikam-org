var classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard =
[
    [ "HTMLWizard", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#a572148dc4b7a899ec22711ab286fe5a5", null ],
    [ "~HTMLWizard", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#abf0f955f4fe9317cdc3f6bbcc4961ef8", null ],
    [ "galleryInfo", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#ab83dc092e41f91ae915fc4576998c20f", null ],
    [ "galleryTheme", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#a2942827824a02982801e72c73ac4a58a", null ],
    [ "nextId", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#ad92163c377a752b941a2b7f92070400f", null ],
    [ "restoreDialogSize", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#a99a0591ddddda89952710d912c0a8e68", null ],
    [ "saveDialogSize", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#a37a3b930d03a859f95dfef14004c3448", null ],
    [ "setItemsList", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#ac0091e3a1080cb04df561c890b23c96a", null ],
    [ "setPlugin", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#a1aa458a15664294147f3b6cb10c9323f", null ],
    [ "validateCurrentPage", "classDigikamGenericHtmlGalleryPlugin_1_1HTMLWizard.html#a84617108d189af950c591cb0bdeb752a", null ]
];