var classDigikam_1_1FileSaveConflictBox =
[
    [ "ConflictRule", "classDigikam_1_1FileSaveConflictBox.html#aeaa5356c5e312b07e8d5ad3eab0581b0", [
      [ "OVERWRITE", "classDigikam_1_1FileSaveConflictBox.html#aeaa5356c5e312b07e8d5ad3eab0581b0a9384c39a1c0906d8c2540df472f70ecd", null ],
      [ "DIFFNAME", "classDigikam_1_1FileSaveConflictBox.html#aeaa5356c5e312b07e8d5ad3eab0581b0af247eea161ca64d5cdd0f17ed0dfa51a", null ],
      [ "SKIPFILE", "classDigikam_1_1FileSaveConflictBox.html#aeaa5356c5e312b07e8d5ad3eab0581b0a49c39b4f9c30d5d550b6d308c8f80350", null ]
    ] ],
    [ "FileSaveConflictBox", "classDigikam_1_1FileSaveConflictBox.html#a212eb56ba29b4157dcf2a81c311a64ab", null ],
    [ "~FileSaveConflictBox", "classDigikam_1_1FileSaveConflictBox.html#ac9b6ae4104f4a2992f63e500987278ed", null ],
    [ "conflictRule", "classDigikam_1_1FileSaveConflictBox.html#a8d3ecb6a4a500e00b0a8b2703438f4e5", null ],
    [ "readSettings", "classDigikam_1_1FileSaveConflictBox.html#ac6d70118c80f5f4da86792e69945030c", null ],
    [ "resetToDefault", "classDigikam_1_1FileSaveConflictBox.html#a109865b1419e80fc99819068a02982e3", null ],
    [ "setConflictRule", "classDigikam_1_1FileSaveConflictBox.html#a245fa2124449f411e1aa26d0b667fae2", null ],
    [ "signalConflictButtonChanged", "classDigikam_1_1FileSaveConflictBox.html#a9b62367178a22dcf7108a58c0fb8add4", null ],
    [ "writeSettings", "classDigikam_1_1FileSaveConflictBox.html#ac99e2d2c506062c219b2f7838616dec4", null ]
];