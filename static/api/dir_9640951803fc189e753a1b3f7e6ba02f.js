var dir_9640951803fc189e753a1b3f7e6ba02f =
[
    [ "ftexportwidget.cpp", "ftexportwidget_8cpp.html", null ],
    [ "ftexportwidget.h", "ftexportwidget_8h.html", [
      [ "FTExportWidget", "classDigikamGenericFileTransferPlugin_1_1FTExportWidget.html", "classDigikamGenericFileTransferPlugin_1_1FTExportWidget" ]
    ] ],
    [ "ftexportwindow.cpp", "ftexportwindow_8cpp.html", null ],
    [ "ftexportwindow.h", "ftexportwindow_8h.html", [
      [ "FTExportWindow", "classDigikamGenericFileTransferPlugin_1_1FTExportWindow.html", "classDigikamGenericFileTransferPlugin_1_1FTExportWindow" ]
    ] ],
    [ "ftimportwidget.cpp", "ftimportwidget_8cpp.html", null ],
    [ "ftimportwidget.h", "ftimportwidget_8h.html", [
      [ "FTImportWidget", "classDigikamGenericFileTransferPlugin_1_1FTImportWidget.html", "classDigikamGenericFileTransferPlugin_1_1FTImportWidget" ]
    ] ],
    [ "ftimportwindow.cpp", "ftimportwindow_8cpp.html", null ],
    [ "ftimportwindow.h", "ftimportwindow_8h.html", [
      [ "FTImportWindow", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow" ]
    ] ],
    [ "ftplugin.cpp", "ftplugin_8cpp.html", null ],
    [ "ftplugin.h", "ftplugin_8h.html", "ftplugin_8h" ]
];