var classheif_1_1SEIMessage__depth__representation__info =
[
    [ "d_max", "classheif_1_1SEIMessage__depth__representation__info.html#a74f7897befdbc4fa1f427cc851c29d14", null ],
    [ "d_min", "classheif_1_1SEIMessage__depth__representation__info.html#a6258dcedda0fc0a431ecc10f84db0ef0", null ],
    [ "depth_nonlinear_representation_model", "classheif_1_1SEIMessage__depth__representation__info.html#a2b4f86c20f5b42334d0fe435f0bba2be", null ],
    [ "depth_nonlinear_representation_model_size", "classheif_1_1SEIMessage__depth__representation__info.html#ab10a685fb083e7aece3b1713ca460912", null ],
    [ "depth_representation_type", "classheif_1_1SEIMessage__depth__representation__info.html#a871076650a97181782424d670df42586", null ],
    [ "disparity_reference_view", "classheif_1_1SEIMessage__depth__representation__info.html#a96fc8f95e1a2a7acd026ce4f8b39015d", null ],
    [ "has_d_max", "classheif_1_1SEIMessage__depth__representation__info.html#af52326cc62903f35d879724a94f9df70", null ],
    [ "has_d_min", "classheif_1_1SEIMessage__depth__representation__info.html#adb3fc51a06e1860c4059823bca6e7aa9", null ],
    [ "has_z_far", "classheif_1_1SEIMessage__depth__representation__info.html#a884da9942a316a791d2d07e566bf44a6", null ],
    [ "has_z_near", "classheif_1_1SEIMessage__depth__representation__info.html#ab7d13310225990d17e1b0fd6a091e43e", null ],
    [ "version", "classheif_1_1SEIMessage__depth__representation__info.html#af10fe5c8d0e27164a3af8e00fe434b86", null ],
    [ "z_far", "classheif_1_1SEIMessage__depth__representation__info.html#a9f8ac199dc94579316c6ee54dd4ab7b8", null ],
    [ "z_near", "classheif_1_1SEIMessage__depth__representation__info.html#a39744077569afb11fe5dec8214d752ac", null ]
];