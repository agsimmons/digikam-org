var classDigikam_1_1GPSItemInfoSorter =
[
    [ "SortOption", "classDigikam_1_1GPSItemInfoSorter.html#aaf77de813e48a512d46ff0251959d5cc", [
      [ "SortYoungestFirst", "classDigikam_1_1GPSItemInfoSorter.html#aaf77de813e48a512d46ff0251959d5cca86e91153072af3ba88907b9f411bc0a4", null ],
      [ "SortOldestFirst", "classDigikam_1_1GPSItemInfoSorter.html#aaf77de813e48a512d46ff0251959d5ccae4f25d83006f581b0764e3ae89ea1eb0", null ],
      [ "SortRating", "classDigikam_1_1GPSItemInfoSorter.html#aaf77de813e48a512d46ff0251959d5cca5dfeb598cfa15bffe52a664d20fbcb7e", null ]
    ] ],
    [ "GPSItemInfoSorter", "classDigikam_1_1GPSItemInfoSorter.html#aff0d61d6517498293ae8e874be4466fd", null ],
    [ "~GPSItemInfoSorter", "classDigikam_1_1GPSItemInfoSorter.html#a653c24822c975f9b54b70cfe7f6e6e30", null ],
    [ "addToMapWidget", "classDigikam_1_1GPSItemInfoSorter.html#a9af6b124e1aafb1a7d55f2bc958e8f0f", null ],
    [ "getSortOptions", "classDigikam_1_1GPSItemInfoSorter.html#a0749fb0a828e66c988d673632330350c", null ],
    [ "setSortOptions", "classDigikam_1_1GPSItemInfoSorter.html#ab281225056553ec2d9b8073190163c5f", null ]
];