var dir_fbe4a3fa0dbb75f94171d46c7a1f8211 =
[
    [ "panointropage.cpp", "panointropage_8cpp.html", null ],
    [ "panointropage.h", "panointropage_8h.html", [
      [ "PanoIntroPage", "classDigikamGenericPanoramaPlugin_1_1PanoIntroPage.html", "classDigikamGenericPanoramaPlugin_1_1PanoIntroPage" ]
    ] ],
    [ "panoitemspage.cpp", "panoitemspage_8cpp.html", null ],
    [ "panoitemspage.h", "panoitemspage_8h.html", [
      [ "PanoItemsPage", "classDigikamGenericPanoramaPlugin_1_1PanoItemsPage.html", "classDigikamGenericPanoramaPlugin_1_1PanoItemsPage" ]
    ] ],
    [ "panolastpage.cpp", "panolastpage_8cpp.html", null ],
    [ "panolastpage.h", "panolastpage_8h.html", [
      [ "PanoLastPage", "classDigikamGenericPanoramaPlugin_1_1PanoLastPage.html", "classDigikamGenericPanoramaPlugin_1_1PanoLastPage" ]
    ] ],
    [ "panooptimizepage.cpp", "panooptimizepage_8cpp.html", null ],
    [ "panooptimizepage.h", "panooptimizepage_8h.html", [
      [ "PanoOptimizePage", "classDigikamGenericPanoramaPlugin_1_1PanoOptimizePage.html", "classDigikamGenericPanoramaPlugin_1_1PanoOptimizePage" ]
    ] ],
    [ "panopreprocesspage.cpp", "panopreprocesspage_8cpp.html", null ],
    [ "panopreprocesspage.h", "panopreprocesspage_8h.html", [
      [ "PanoPreProcessPage", "classDigikamGenericPanoramaPlugin_1_1PanoPreProcessPage.html", "classDigikamGenericPanoramaPlugin_1_1PanoPreProcessPage" ]
    ] ],
    [ "panopreviewpage.cpp", "panopreviewpage_8cpp.html", null ],
    [ "panopreviewpage.h", "panopreviewpage_8h.html", [
      [ "PanoPreviewPage", "classDigikamGenericPanoramaPlugin_1_1PanoPreviewPage.html", "classDigikamGenericPanoramaPlugin_1_1PanoPreviewPage" ]
    ] ],
    [ "panowizard.cpp", "panowizard_8cpp.html", null ],
    [ "panowizard.h", "panowizard_8h.html", [
      [ "PanoWizard", "classDigikamGenericPanoramaPlugin_1_1PanoWizard.html", "classDigikamGenericPanoramaPlugin_1_1PanoWizard" ]
    ] ]
];