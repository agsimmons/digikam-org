var dir_4ceddd37124f80619b31fd28ba9f1d3d =
[
    [ "models", "dir_8c1af42b7ca16790ffaa23067b2d0c80.html", "dir_8c1af42b7ca16790ffaa23067b2d0c80" ],
    [ "taglist.cpp", "taglist_8cpp.html", null ],
    [ "taglist.h", "taglist_8h.html", [
      [ "TagList", "classDigikam_1_1TagList.html", "classDigikam_1_1TagList" ]
    ] ],
    [ "tagmngrtreeview.cpp", "tagmngrtreeview_8cpp.html", null ],
    [ "tagmngrtreeview.h", "tagmngrtreeview_8h.html", [
      [ "TagMngrTreeView", "classDigikam_1_1TagMngrTreeView.html", "classDigikam_1_1TagMngrTreeView" ]
    ] ],
    [ "tagpropwidget.cpp", "tagpropwidget_8cpp.html", null ],
    [ "tagpropwidget.h", "tagpropwidget_8h.html", [
      [ "TagPropWidget", "classDigikam_1_1TagPropWidget.html", "classDigikam_1_1TagPropWidget" ]
    ] ],
    [ "tagsmanager.cpp", "tagsmanager_8cpp.html", null ],
    [ "tagsmanager.h", "tagsmanager_8h.html", [
      [ "TagsManager", "classDigikam_1_1TagsManager.html", "classDigikam_1_1TagsManager" ]
    ] ]
];