var dir_0a8f89dbc52d9b1b4a4c462352a55429 =
[
    [ "dbmigrationdlg.cpp", "dbmigrationdlg_8cpp.html", null ],
    [ "dbmigrationdlg.h", "dbmigrationdlg_8h.html", [
      [ "DatabaseCopyThread", "classDigikam_1_1DatabaseCopyThread.html", "classDigikam_1_1DatabaseCopyThread" ],
      [ "DatabaseMigrationDialog", "classDigikam_1_1DatabaseMigrationDialog.html", "classDigikam_1_1DatabaseMigrationDialog" ]
    ] ],
    [ "dbsettingswidget.cpp", "dbsettingswidget_8cpp.html", null ],
    [ "dbsettingswidget.h", "dbsettingswidget_8h.html", [
      [ "DatabaseSettingsWidget", "classDigikam_1_1DatabaseSettingsWidget.html", "classDigikam_1_1DatabaseSettingsWidget" ]
    ] ],
    [ "dbsettingswidget_p.h", "dbsettingswidget__p_8h.html", [
      [ "Private", "classDigikam_1_1DatabaseSettingsWidget_1_1Private.html", "classDigikam_1_1DatabaseSettingsWidget_1_1Private" ]
    ] ],
    [ "dbstatdlg.cpp", "dbstatdlg_8cpp.html", null ],
    [ "dbstatdlg.h", "dbstatdlg_8h.html", [
      [ "DBStatDlg", "classDigikam_1_1DBStatDlg.html", "classDigikam_1_1DBStatDlg" ]
    ] ],
    [ "mysqladminbinary.cpp", "mysqladminbinary_8cpp.html", null ],
    [ "mysqladminbinary.h", "mysqladminbinary_8h.html", [
      [ "MysqlAdminBinary", "classDigikam_1_1MysqlAdminBinary.html", "classDigikam_1_1MysqlAdminBinary" ]
    ] ],
    [ "mysqlinitbinary.cpp", "mysqlinitbinary_8cpp.html", null ],
    [ "mysqlinitbinary.h", "mysqlinitbinary_8h.html", [
      [ "MysqlInitBinary", "classDigikam_1_1MysqlInitBinary.html", "classDigikam_1_1MysqlInitBinary" ]
    ] ],
    [ "mysqlservbinary.cpp", "mysqlservbinary_8cpp.html", null ],
    [ "mysqlservbinary.h", "mysqlservbinary_8h.html", [
      [ "MysqlServBinary", "classDigikam_1_1MysqlServBinary.html", "classDigikam_1_1MysqlServBinary" ]
    ] ],
    [ "searchtextbardb.cpp", "searchtextbardb_8cpp.html", null ],
    [ "searchtextbardb.h", "searchtextbardb_8h.html", [
      [ "SearchTextBarDb", "classDigikam_1_1SearchTextBarDb.html", "classDigikam_1_1SearchTextBarDb" ]
    ] ]
];