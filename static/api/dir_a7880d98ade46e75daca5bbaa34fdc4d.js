var dir_a7880d98ade46e75daca5bbaa34fdc4d =
[
    [ "xmpcategories.cpp", "xmpcategories_8cpp.html", null ],
    [ "xmpcategories.h", "xmpcategories_8h.html", [
      [ "XMPCategories", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories" ]
    ] ],
    [ "xmpcontent.cpp", "xmpcontent_8cpp.html", null ],
    [ "xmpcontent.h", "xmpcontent_8h.html", [
      [ "XMPContent", "classDigikamGenericMetadataEditPlugin_1_1XMPContent.html", "classDigikamGenericMetadataEditPlugin_1_1XMPContent" ]
    ] ],
    [ "xmpcredits.cpp", "xmpcredits_8cpp.html", null ],
    [ "xmpcredits.h", "xmpcredits_8h.html", [
      [ "XMPCredits", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits" ]
    ] ],
    [ "xmpeditwidget.cpp", "xmpeditwidget_8cpp.html", null ],
    [ "xmpeditwidget.h", "xmpeditwidget_8h.html", [
      [ "XMPEditWidget", "classDigikamGenericMetadataEditPlugin_1_1XMPEditWidget.html", "classDigikamGenericMetadataEditPlugin_1_1XMPEditWidget" ]
    ] ],
    [ "xmpkeywords.cpp", "xmpkeywords_8cpp.html", null ],
    [ "xmpkeywords.h", "xmpkeywords_8h.html", [
      [ "XMPKeywords", "classDigikamGenericMetadataEditPlugin_1_1XMPKeywords.html", "classDigikamGenericMetadataEditPlugin_1_1XMPKeywords" ]
    ] ],
    [ "xmporigin.cpp", "xmporigin_8cpp.html", null ],
    [ "xmporigin.h", "xmporigin_8h.html", [
      [ "XMPOrigin", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin" ]
    ] ],
    [ "xmpproperties.cpp", "xmpproperties_8cpp.html", null ],
    [ "xmpproperties.h", "xmpproperties_8h.html", [
      [ "XMPProperties", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties.html", "classDigikamGenericMetadataEditPlugin_1_1XMPProperties" ]
    ] ],
    [ "xmpstatus.cpp", "xmpstatus_8cpp.html", null ],
    [ "xmpstatus.h", "xmpstatus_8h.html", [
      [ "XMPStatus", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus.html", "classDigikamGenericMetadataEditPlugin_1_1XMPStatus" ]
    ] ],
    [ "xmpsubjects.cpp", "xmpsubjects_8cpp.html", null ],
    [ "xmpsubjects.h", "xmpsubjects_8h.html", [
      [ "XMPSubjects", "classDigikamGenericMetadataEditPlugin_1_1XMPSubjects.html", "classDigikamGenericMetadataEditPlugin_1_1XMPSubjects" ]
    ] ]
];