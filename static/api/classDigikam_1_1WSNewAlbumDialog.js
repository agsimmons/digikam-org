var classDigikam_1_1WSNewAlbumDialog =
[
    [ "WSNewAlbumDialog", "classDigikam_1_1WSNewAlbumDialog.html#a68bf3d910ac437eb9fe1768006644b63", null ],
    [ "~WSNewAlbumDialog", "classDigikam_1_1WSNewAlbumDialog.html#a2dd70428a43115b3455f5fd6603529e3", null ],
    [ "addToMainLayout", "classDigikam_1_1WSNewAlbumDialog.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikam_1_1WSNewAlbumDialog.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getBaseAlbumProperties", "classDigikam_1_1WSNewAlbumDialog.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikam_1_1WSNewAlbumDialog.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikam_1_1WSNewAlbumDialog.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikam_1_1WSNewAlbumDialog.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getLocEdit", "classDigikam_1_1WSNewAlbumDialog.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikam_1_1WSNewAlbumDialog.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikam_1_1WSNewAlbumDialog.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikam_1_1WSNewAlbumDialog.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikam_1_1WSNewAlbumDialog.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikam_1_1WSNewAlbumDialog.html#a335987a85da0b9b0bd083f3b85250370", null ]
];