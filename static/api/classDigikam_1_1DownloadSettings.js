var classDigikam_1_1DownloadSettings =
[
    [ "DownloadSettings", "classDigikam_1_1DownloadSettings.html#a92c37f2f7810a174bc68237177d7903a", null ],
    [ "~DownloadSettings", "classDigikam_1_1DownloadSettings.html#a918265c87d709569bad69879ff2c73b3", null ],
    [ "autoRotate", "classDigikam_1_1DownloadSettings.html#ab66203f14eac65b8e45be9d6eb35b552", null ],
    [ "backupRaw", "classDigikam_1_1DownloadSettings.html#a87d24a04a8ac2161fe3553b6c7e0bfa4", null ],
    [ "colorLabel", "classDigikam_1_1DownloadSettings.html#a6ae0a399cfc664b85b35a269c48f0dbe", null ],
    [ "compressDng", "classDigikam_1_1DownloadSettings.html#a68446f4d0fef394d439797b0982ef10e", null ],
    [ "convertDng", "classDigikam_1_1DownloadSettings.html#abd4bb63ac50877cb6b4814317a0da890", null ],
    [ "convertJpeg", "classDigikam_1_1DownloadSettings.html#ac13ed92991bfc508fa6bfaf6dc5e6ce6", null ],
    [ "dest", "classDigikam_1_1DownloadSettings.html#a81f274dd5f5407783e8df22da59d7bca", null ],
    [ "documentName", "classDigikam_1_1DownloadSettings.html#ad82f30393ddade41faac96b97cea5bd2", null ],
    [ "file", "classDigikam_1_1DownloadSettings.html#a5d651c4eb46ce273930ea4ceb5ec2041", null ],
    [ "fixDateTime", "classDigikam_1_1DownloadSettings.html#ac7d4ef0b37b7311c053f4d53cd77cb70", null ],
    [ "folder", "classDigikam_1_1DownloadSettings.html#a1eef096b5edd5e636ec283463b6a397a", null ],
    [ "losslessFormat", "classDigikam_1_1DownloadSettings.html#adfa763eb29cbf845673455dd7d1b909c", null ],
    [ "mime", "classDigikam_1_1DownloadSettings.html#a34ff06aa50bcccf56c1eee71344f5602", null ],
    [ "newDateTime", "classDigikam_1_1DownloadSettings.html#a4c8ab5d44b2f289cc2041bf91dd0855d", null ],
    [ "pickLabel", "classDigikam_1_1DownloadSettings.html#a604402c3bf45f7ba17c0ec1261b09b28", null ],
    [ "previewMode", "classDigikam_1_1DownloadSettings.html#afba21dea66b23a829846fb802bb93262", null ],
    [ "rating", "classDigikam_1_1DownloadSettings.html#a3928ccb7551e204da0598a0ca0f29c95", null ],
    [ "script", "classDigikam_1_1DownloadSettings.html#aeece4c410326f677b55eec95d217e2b8", null ],
    [ "tagIds", "classDigikam_1_1DownloadSettings.html#aaf575e9cc729becfaea7001b0993fee0", null ],
    [ "templateTitle", "classDigikam_1_1DownloadSettings.html#a48fad0e649a684cbd4f9924209f1ac6c", null ]
];