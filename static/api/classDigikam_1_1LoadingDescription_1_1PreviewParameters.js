var classDigikam_1_1LoadingDescription_1_1PreviewParameters =
[
    [ "PreviewFlag", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a2c380301bb592632b113c25e12fc7282", [
      [ "NoFlags", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a2c380301bb592632b113c25e12fc7282af7507cd768ce024b3c21cf082e333e1a", null ],
      [ "OnlyPregenerate", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a2c380301bb592632b113c25e12fc7282ad25f816a8bb3f4214b42be17e024591b", null ]
    ] ],
    [ "PreviewType", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ab2683695a1a53eb40cef2ff90636a6b3", [
      [ "NoPreview", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ab2683695a1a53eb40cef2ff90636a6b3a6e9e72feebb65f145739d5e67c49f1b0", null ],
      [ "PreviewImage", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ab2683695a1a53eb40cef2ff90636a6b3a5b2ea6d93f4107ffdb3bbe09bf6611c3", null ],
      [ "Thumbnail", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ab2683695a1a53eb40cef2ff90636a6b3a96330195ab5c05a7b20d43b36129c17e", null ],
      [ "DetailThumbnail", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ab2683695a1a53eb40cef2ff90636a6b3afec485faf9ffb62e8e28ccab7693bc63", null ]
    ] ],
    [ "PreviewParameters", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ac31c21eb428900c383320ceff9b6c644", null ],
    [ "onlyPregenerate", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a9e0646eebee63fb6bd079af516c2bfc1", null ],
    [ "operator==", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#ac4417357781bcfc4007ed117b473e4ba", null ],
    [ "extraParameter", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a6a12844d1f03a6a60b756543de3455c6", null ],
    [ "flags", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a9b2b4ac6e8ca8647952a1ae0c3b91e76", null ],
    [ "previewSettings", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a221519f941cf379c7c605445f7659abf", null ],
    [ "size", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a7c6ba02d7bd93f1b46bbd5831e73d6ed", null ],
    [ "storageReference", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a809233eb0aaaaf4dada7b639fbb35af3", null ],
    [ "type", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#afd867fd8e184f4e5a84ab0e91883c2e1", null ]
];