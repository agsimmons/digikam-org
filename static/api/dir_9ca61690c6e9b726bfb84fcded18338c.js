var dir_9ca61690c6e9b726bfb84fcded18338c =
[
    [ "detector", "dir_16cf9e2a59f90956b15c67cec0e63655.html", "dir_16cf9e2a59f90956b15c67cec0e63655" ],
    [ "imagequalitycalculator.cpp", "imagequalitycalculator_8cpp.html", null ],
    [ "imagequalitycalculator.h", "imagequalitycalculator_8h.html", [
      [ "ImageQualityCalculator", "classDigikam_1_1ImageQualityCalculator.html", "classDigikam_1_1ImageQualityCalculator" ],
      [ "ResultDetection", "structDigikam_1_1ImageQualityCalculator_1_1ResultDetection.html", "structDigikam_1_1ImageQualityCalculator_1_1ResultDetection" ]
    ] ],
    [ "imagequalitycontainer.cpp", "imagequalitycontainer_8cpp.html", "imagequalitycontainer_8cpp" ],
    [ "imagequalitycontainer.h", "imagequalitycontainer_8h.html", "imagequalitycontainer_8h" ],
    [ "imagequalityparser.cpp", "imagequalityparser_8cpp.html", null ],
    [ "imagequalityparser.h", "imagequalityparser_8h.html", [
      [ "ImageQualityParser", "classDigikam_1_1ImageQualityParser.html", "classDigikam_1_1ImageQualityParser" ]
    ] ],
    [ "imagequalityparser_p.h", "imagequalityparser__p_8h.html", [
      [ "Private", "classDigikam_1_1ImageQualityParser_1_1Private.html", "classDigikam_1_1ImageQualityParser_1_1Private" ]
    ] ],
    [ "imagequalitysettings.cpp", "imagequalitysettings_8cpp.html", null ],
    [ "imagequalitysettings.h", "imagequalitysettings_8h.html", [
      [ "ImageQualitySettings", "classDigikam_1_1ImageQualitySettings.html", "classDigikam_1_1ImageQualitySettings" ]
    ] ],
    [ "imagequalitythread.cpp", "imagequalitythread_8cpp.html", null ],
    [ "imagequalitythread.h", "imagequalitythread_8h.html", [
      [ "ImageQualityThread", "classDigikam_1_1ImageQualityThread.html", "classDigikam_1_1ImageQualityThread" ],
      [ "ImageQualityThreadPool", "classDigikam_1_1ImageQualityThreadPool.html", "classDigikam_1_1ImageQualityThreadPool" ]
    ] ]
];