var classDigikam_1_1HSLSettings =
[
    [ "HSLSettings", "classDigikam_1_1HSLSettings.html#a4bb8029322791d3b89ccb2f1f5830e6b", null ],
    [ "~HSLSettings", "classDigikam_1_1HSLSettings.html#a01dc09ede082010fb02432db4c6a612f", null ],
    [ "defaultSettings", "classDigikam_1_1HSLSettings.html#a7c5a8f6eb4f6e8070d983485cd7d3655", null ],
    [ "readSettings", "classDigikam_1_1HSLSettings.html#a1a062c267879e660d6a7b90c79aa357d", null ],
    [ "resetToDefault", "classDigikam_1_1HSLSettings.html#a5530295636f691079eacda38812df643", null ],
    [ "setSettings", "classDigikam_1_1HSLSettings.html#a15008cfd6cbb104b47856c8148c22191", null ],
    [ "settings", "classDigikam_1_1HSLSettings.html#a331fb5709e6b07a90e253c08146dec61", null ],
    [ "signalSettingsChanged", "classDigikam_1_1HSLSettings.html#a92bf72acccc311b694f5ec6da6002597", null ],
    [ "writeSettings", "classDigikam_1_1HSLSettings.html#a8125e52385444c10a88a7f9934db0bdd", null ]
];