var dir_1b72b980fd40039b4544a7fa5ccd8372 =
[
    [ "facedb.cpp", "facedb_8cpp.html", null ],
    [ "facedb.h", "facedb_8h.html", [
      [ "FaceDb", "classDigikam_1_1FaceDb.html", "classDigikam_1_1FaceDb" ]
    ] ],
    [ "facedb_dnn.cpp", "facedb__dnn_8cpp.html", null ],
    [ "facedb_dnn_spatial.cpp", "facedb__dnn__spatial_8cpp.html", null ],
    [ "facedb_identity.cpp", "facedb__identity_8cpp.html", null ],
    [ "facedb_p.h", "facedb__p_8h.html", [
      [ "Private", "classDigikam_1_1FaceDb_1_1Private.html", "classDigikam_1_1FaceDb_1_1Private" ]
    ] ],
    [ "facedbaccess.cpp", "facedbaccess_8cpp.html", null ],
    [ "facedbaccess.h", "facedbaccess_8h.html", [
      [ "FaceDbAccess", "classDigikam_1_1FaceDbAccess.html", "classDigikam_1_1FaceDbAccess" ],
      [ "FaceDbAccessUnlock", "classDigikam_1_1FaceDbAccessUnlock.html", "classDigikam_1_1FaceDbAccessUnlock" ]
    ] ],
    [ "facedbbackend.cpp", "facedbbackend_8cpp.html", null ],
    [ "facedbbackend.h", "facedbbackend_8h.html", [
      [ "FaceDbBackend", "classDigikam_1_1FaceDbBackend.html", "classDigikam_1_1FaceDbBackend" ]
    ] ],
    [ "facedboperationgroup.cpp", "facedboperationgroup_8cpp.html", null ],
    [ "facedboperationgroup.h", "facedboperationgroup_8h.html", [
      [ "FaceDbOperationGroup", "classDigikam_1_1FaceDbOperationGroup.html", "classDigikam_1_1FaceDbOperationGroup" ]
    ] ],
    [ "facedbschemaupdater.cpp", "facedbschemaupdater_8cpp.html", null ],
    [ "facedbschemaupdater.h", "facedbschemaupdater_8h.html", [
      [ "FaceDbSchemaUpdater", "classDigikam_1_1FaceDbSchemaUpdater.html", "classDigikam_1_1FaceDbSchemaUpdater" ]
    ] ]
];