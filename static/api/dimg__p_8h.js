var dimg__p_8h =
[
    [ "DImg", "classDigikam_1_1DImg.html", "classDigikam_1_1DImg" ],
    [ "DImgStaticPriv", "classDigikam_1_1DImgStaticPriv.html", null ],
    [ "LANCZOS_DATA_ONE", "dimg__p_8h.html#a02f1a64d1ddecb9b4807a573443462e6", null ],
    [ "LANCZOS_DATA_TYPE", "dimg__p_8h.html#a68fb470fa8c75d3fc5ac61c2233eeba0", null ],
    [ "LANCZOS_SUPPORT", "dimg__p_8h.html#a2be1f3b052dba61d7726633293330d2b", null ],
    [ "LANCZOS_TABLE_RES", "dimg__p_8h.html#a07765bf31e5594708ebb04e6ff4df5c3", null ],
    [ "llong", "dimg__p_8h.html#a2261a9f81a626bc300628b9ca69585cb", null ],
    [ "ullong", "dimg__p_8h.html#a213f91808724512f621e2af7e2de2c2d", null ]
];