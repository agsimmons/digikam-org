var gpsitemcontainer_8h =
[
    [ "GPSItemContainer", "classDigikam_1_1GPSItemContainer.html", "classDigikam_1_1GPSItemContainer" ],
    [ "SaveProperties", "classDigikam_1_1SaveProperties.html", "classDigikam_1_1SaveProperties" ],
    [ "TagData", "structDigikam_1_1TagData.html", "structDigikam_1_1TagData" ],
    [ "TagData", "gpsitemcontainer_8h.html#a541dcfe77e8a924a33ba6b9da3da1b64", null ],
    [ "Type", "gpsitemcontainer_8h.html#ad027192976a908682b94ea153880cd6f", [
      [ "TypeChild", "gpsitemcontainer_8h.html#ad027192976a908682b94ea153880cd6fa19dd7b094208a29e592427f6845d306c", null ],
      [ "TypeSpacer", "gpsitemcontainer_8h.html#ad027192976a908682b94ea153880cd6faeb9f40fd0dd1ff44780fe423b2ae0f36", null ],
      [ "TypeNewChild", "gpsitemcontainer_8h.html#ad027192976a908682b94ea153880cd6fa6637b3fec6ff27263f755e050fb1d45e", null ]
    ] ]
];