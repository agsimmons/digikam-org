var NAVTREEINDEX267 =
{
"namespaceDigikamEditorAdjustCurvesToolPlugin.html":[1,0,41],
"namespaceDigikamEditorAdjustLevelsToolPlugin.html":[1,0,42],
"namespaceDigikamEditorAntivignettingToolPlugin.html":[1,0,43],
"namespaceDigikamEditorAutoCorrectionToolPlugin.html":[1,0,44],
"namespaceDigikamEditorAutoCropToolPlugin.html":[1,0,45],
"namespaceDigikamEditorBCGToolPlugin.html":[1,0,46],
"namespaceDigikamEditorBWSepiaToolPlugin.html":[1,0,50],
"namespaceDigikamEditorBlurFxToolPlugin.html":[1,0,47],
"namespaceDigikamEditorBlurToolPlugin.html":[1,0,48],
"namespaceDigikamEditorBorderToolPlugin.html":[1,0,49],
"namespaceDigikamEditorChannelMixerToolPlugin.html":[1,0,51],
"namespaceDigikamEditorCharcoalToolPlugin.html":[1,0,52],
"namespaceDigikamEditorColorBalanceToolPlugin.html":[1,0,53],
"namespaceDigikamEditorColorFxToolPlugin.html":[1,0,54],
"namespaceDigikamEditorContentAwareResizeToolPlugin.html":[1,0,55],
"namespaceDigikamEditorConvert16To8ToolPlugin.html":[1,0,56],
"namespaceDigikamEditorConvert8To16ToolPlugin.html":[1,0,57],
"namespaceDigikamEditorDistortionFxToolPlugin.html":[1,0,58],
"namespaceDigikamEditorEmbossToolPlugin.html":[1,0,59],
"namespaceDigikamEditorFilmGrainToolPlugin.html":[1,0,60],
"namespaceDigikamEditorFilmToolPlugin.html":[1,0,61],
"namespaceDigikamEditorFreeRotationToolPlugin.html":[1,0,62],
"namespaceDigikamEditorHSLToolPlugin.html":[1,0,65],
"namespaceDigikamEditorHealingCloneToolPlugin.html":[1,0,63],
"namespaceDigikamEditorHotPixelsToolPlugin.html":[1,0,64],
"namespaceDigikamEditorInsertTextToolPlugin.html":[1,0,66],
"namespaceDigikamEditorInvertToolPlugin.html":[1,0,67],
"namespaceDigikamEditorLensAutoFixToolPlugin.html":[1,0,68],
"namespaceDigikamEditorLensDistortionToolPlugin.html":[1,0,69],
"namespaceDigikamEditorLocalContrastToolPlugin.html":[1,0,70],
"namespaceDigikamEditorNoiseReductionToolPlugin.html":[1,0,71],
"namespaceDigikamEditorOilPaintToolPlugin.html":[1,0,72],
"namespaceDigikamEditorPerspectiveToolPlugin.html":[1,0,73],
"namespaceDigikamEditorPrintToolPlugin.html":[1,0,74],
"namespaceDigikamEditorProfileConversionToolPlugin.html":[1,0,75],
"namespaceDigikamEditorRainDropToolPlugin.html":[1,0,76],
"namespaceDigikamEditorRatioCropToolPlugin.html":[1,0,77],
"namespaceDigikamEditorRedEyeToolPlugin.html":[1,0,78],
"namespaceDigikamEditorResizeToolPlugin.html":[1,0,79],
"namespaceDigikamEditorRestorationToolPlugin.html":[1,0,80],
"namespaceDigikamEditorSharpenToolPlugin.html":[1,0,81],
"namespaceDigikamEditorShearToolPlugin.html":[1,0,82],
"namespaceDigikamEditorTextureToolPlugin.html":[1,0,83],
"namespaceDigikamEditorWhiteBalanceToolPlugin.html":[1,0,84],
"namespaceDigikamGenericBoxPlugin.html":[1,0,85],
"namespaceDigikamGenericCalendarPlugin.html":[1,0,86],
"namespaceDigikamGenericCalendarPlugin.html#aae2a8a7810c2cd0ce52105c289e975a6":[1,0,86,11],
"namespaceDigikamGenericDNGConverterPlugin.html":[1,0,87],
"namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89ed":[1,0,87,7],
"namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89eda0f6f9db07384471e319e35b8755b827a":[1,0,87,7,0],
"namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89eda222cecf912baaae3948428bb1a0e4c1c":[1,0,87,7,1],
"namespaceDigikamGenericDNGConverterPlugin.html#a973d39362c08f3b46e502615f60c89eda544697836ba26d4044b358cf1f65d1cd":[1,0,87,7,2],
"namespaceDigikamGenericDScannerPlugin.html":[1,0,89],
"namespaceDigikamGenericDropBoxPlugin.html":[1,0,88],
"namespaceDigikamGenericExpoBlendingPlugin.html":[1,0,90],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38":[1,0,90,20],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38a020780e2918139f36a2efe4cbceccde5":[1,0,90,20,2],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38a21fb342794d6ceed7f67d0de708a1f64":[1,0,90,20,5],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38a71f6feb6d5664f9dff97f4fa55ee3bc5":[1,0,90,20,1],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38a7b7b25246aba105f053cdaebdea2454b":[1,0,90,20,3],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38a9f86f382bfde87264fc4c43cd2e98788":[1,0,90,20,0],
"namespaceDigikamGenericExpoBlendingPlugin.html#a7a490b8223a2dfff09bfeacdcf253d38ae1c226da801b737974729f7f04167053":[1,0,90,20,4],
"namespaceDigikamGenericExpoBlendingPlugin.html#a8941f526bfc27c23c005f78f0593bbc9":[1,0,90,19],
"namespaceDigikamGenericFaceBookPlugin.html":[1,0,91],
"namespaceDigikamGenericFaceBookPlugin.html#a56be7743462491beeff907c417b3a3b4":[1,0,91,9],
"namespaceDigikamGenericFaceBookPlugin.html#a56be7743462491beeff907c417b3a3b4a0d23620069146fa6e99e4bed54cd38f3":[1,0,91,9,1],
"namespaceDigikamGenericFaceBookPlugin.html#a56be7743462491beeff907c417b3a3b4a38093c05070640edd83c4cd08c39fe18":[1,0,91,9,2],
"namespaceDigikamGenericFaceBookPlugin.html#a56be7743462491beeff907c417b3a3b4a9e61c61354e447c8dce4c81065b8d460":[1,0,91,9,0],
"namespaceDigikamGenericFaceBookPlugin.html#a56be7743462491beeff907c417b3a3b4ab094b6c4903b9fa73d0c6cc9378d447b":[1,0,91,9,3],
"namespaceDigikamGenericFaceBookPlugin.html#a56be7743462491beeff907c417b3a3b4adaccc790ab37ac96c371ccbd8f6ab476":[1,0,91,9,4],
"namespaceDigikamGenericFaceBookPlugin.html#a90509de35008e096aabf46d764fa27cb":[1,0,91,11],
"namespaceDigikamGenericFaceBookPlugin.html#afa0fda76dfcf2db57da870cd276feb57":[1,0,91,10],
"namespaceDigikamGenericFileCopyPlugin.html":[1,0,92],
"namespaceDigikamGenericFileTransferPlugin.html":[1,0,93],
"namespaceDigikamGenericFlickrPlugin.html":[1,0,94],
"namespaceDigikamGenericGLViewerPlugin.html":[1,0,96],
"namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026e":[1,0,96,4],
"namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026ea41886f62e0bae782b4af8ffa98f73899":[1,0,96,4,2],
"namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026eaa7614f8f5a987cced8a01da729a55696":[1,0,96,4,1],
"namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026eadd18aedcb5dbd0387f7ad536b9e88387":[1,0,96,4,0],
"namespaceDigikamGenericGeolocationEditPlugin.html":[1,0,95],
"namespaceDigikamGenericGeolocationEditPlugin.html#a0f584882438fc7478cee687fdb4b0fd9":[1,0,95,12],
"namespaceDigikamGenericGoogleServicesPlugin.html":[1,0,97],
"namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296":[1,0,97,12],
"namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296a377b5da2eb07d57046c08bd143f97ff9":[1,0,97,12,1],
"namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296aae0f1cc2b990bb7a68dc5737ce0409fa":[1,0,97,12,2],
"namespaceDigikamGenericGoogleServicesPlugin.html#a0771e4280da801e307777a48826af296ad431a13f1bdc0ce053d6bb0f1d56ad39":[1,0,97,12,0],
"namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943":[1,0,97,13],
"namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943abe37d07ad01b70550a20be3d9a489ecd":[1,0,97,13,0],
"namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943ae7895ba44b9ba18dada7c6970d21ac58":[1,0,97,13,1],
"namespaceDigikamGenericGoogleServicesPlugin.html#a3267ee910bc207f9d99cb7670881b943af8e9912514fb3fc208a6e3e848129c81":[1,0,97,13,2],
"namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9b":[1,0,97,14],
"namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba0956ce1c1a4b569221e586c1190fd734":[1,0,97,14,4],
"namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba0bc26ec8fa418b38ba41f2af2c03c973":[1,0,97,14,2],
"namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba259400ee1de50944de227b70d34b0598":[1,0,97,14,1],
"namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9ba8e6b6cb45830b68a1293c11d117090d4":[1,0,97,14,3],
"namespaceDigikamGenericGoogleServicesPlugin.html#a86445e451cc62b27cdf92f8af8abdb9baad57562461bb7bb88e783299213a4691":[1,0,97,14,0],
"namespaceDigikamGenericHtmlGalleryPlugin.html":[1,0,98],
"namespaceDigikamGenericHtmlGalleryPlugin.html#a49bc8f7c0f27c4b1421c25da9bc84303":[1,0,98,27],
"namespaceDigikamGenericHtmlGalleryPlugin.html#af4138662f8baba11e64c152c685e037b":[1,0,98,28],
"namespaceDigikamGenericINatPlugin.html":[1,0,101],
"namespaceDigikamGenericINatPlugin.html#a06025b675885734746e2bf9dc61bfbff":[1,0,101,14],
"namespaceDigikamGenericINatPlugin.html#a10e8e435e72e7d13e83fe99b66c4f127":[1,0,101,9],
"namespaceDigikamGenericINatPlugin.html#a2ca329d1f3e380fe62d3358d87a0f349":[1,0,101,13],
"namespaceDigikamGenericINatPlugin.html#a2d7087e47425ddfc73b0c445769ce302":[1,0,101,19],
"namespaceDigikamGenericINatPlugin.html#a483b748a6e419fe99111f5f515b2eaa3":[1,0,101,11],
"namespaceDigikamGenericINatPlugin.html#a5b3d49852e4ca42f7f5c0583ec06e26a":[1,0,101,22],
"namespaceDigikamGenericINatPlugin.html#a5f98370b4f3e35164514d2dfb91aabf7":[1,0,101,10],
"namespaceDigikamGenericINatPlugin.html#a66571804855113c35119775f779d2cc1":[1,0,101,12],
"namespaceDigikamGenericINatPlugin.html#a6703bcf40d2d03b2e4e9e854f8f85bef":[1,0,101,20],
"namespaceDigikamGenericINatPlugin.html#a68dadffdb84c12e9cf5d18aaa72628a5":[1,0,101,23],
"namespaceDigikamGenericINatPlugin.html#a697fbaf82733b13012db07121bb68d75":[1,0,101,16],
"namespaceDigikamGenericINatPlugin.html#a6aff4802136384c68741e6b4cc14725f":[1,0,101,17],
"namespaceDigikamGenericINatPlugin.html#abf57298aba21d977da347177479c7e2d":[1,0,101,18],
"namespaceDigikamGenericINatPlugin.html#ad6ce7e407ef4eeade9e203f44b423f13":[1,0,101,21],
"namespaceDigikamGenericINatPlugin.html#aee4d34158fc02d00c818ea6a46a8a481":[1,0,101,15],
"namespaceDigikamGenericImageShackPlugin.html":[1,0,99],
"namespaceDigikamGenericImgUrPlugin.html":[1,0,100],
"namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1":[1,0,100,7],
"namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1a24f90f807f8e88369a150919f60e2097":[1,0,100,7,1],
"namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1a75430febbb2095058bd164955fb19f3d":[1,0,100,7,0],
"namespaceDigikamGenericImgUrPlugin.html#adb4f09fb742804c207446a8217c933b1aa90a0fdb9c039b11fd5bf66c5c9557e1":[1,0,100,7,2],
"namespaceDigikamGenericIpfsPlugin.html":[1,0,102],
"namespaceDigikamGenericIpfsPlugin.html#a47b24e81da9f533fd531416fc5c00c14":[1,0,102,7],
"namespaceDigikamGenericIpfsPlugin.html#a47b24e81da9f533fd531416fc5c00c14a24f90f807f8e88369a150919f60e2097":[1,0,102,7,0],
"namespaceDigikamGenericJAlbumPlugin.html":[1,0,103],
"namespaceDigikamGenericJAlbumPlugin.html#aa806c364a5bb00635362dc41d21fdaa9":[1,0,103,10],
"namespaceDigikamGenericMediaServerPlugin.html":[1,0,104],
"namespaceDigikamGenericMediaServerPlugin.html#a0ca42b772d3f321ab707da97f8c99fec":[1,0,104,6],
"namespaceDigikamGenericMediaWikiPlugin.html":[1,0,105],
"namespaceDigikamGenericMediaWikiPlugin.html#a2db873ecec4fa7eaf3f8875a65329ba0":[1,0,105,4],
"namespaceDigikamGenericMediaWikiPlugin.html#a2db873ecec4fa7eaf3f8875a65329ba0abc7b3b4b9b41191f9f06c3155ded73e0":[1,0,105,4,2],
"namespaceDigikamGenericMediaWikiPlugin.html#a2db873ecec4fa7eaf3f8875a65329ba0ac11d328295b4bf21f9c449985b7a318f":[1,0,105,4,3],
"namespaceDigikamGenericMediaWikiPlugin.html#a2db873ecec4fa7eaf3f8875a65329ba0ae3d284bb6aba7b89c33a905ca732a952":[1,0,105,4,1],
"namespaceDigikamGenericMediaWikiPlugin.html#a2db873ecec4fa7eaf3f8875a65329ba0ae91417caca828be8a94db228fa125df3":[1,0,105,4,0],
"namespaceDigikamGenericMetadataEditPlugin.html":[1,0,106],
"namespaceDigikamGenericMjpegStreamPlugin.html":[1,0,107],
"namespaceDigikamGenericMjpegStreamPlugin.html#ad10ca514383bc308fc710c01f66e40ea":[1,0,107,8],
"namespaceDigikamGenericOneDrivePlugin.html":[1,0,108],
"namespaceDigikamGenericPanoramaPlugin.html":[1,0,109],
"namespaceDigikamGenericPanoramaPlugin.html#a11206854557bf007056b9e4159a98a11":[1,0,109,38],
"namespaceDigikamGenericPanoramaPlugin.html#a11206854557bf007056b9e4159a98a11aa8d6a54ea3b30483893a85170b37fcba":[1,0,109,38,2],
"namespaceDigikamGenericPanoramaPlugin.html#a11206854557bf007056b9e4159a98a11acc602c3e08b6e938c4fe2b07d2b6d2eb":[1,0,109,38,0],
"namespaceDigikamGenericPanoramaPlugin.html#a11206854557bf007056b9e4159a98a11adb025fc2d1ec594177b8ebf365cdeaec":[1,0,109,38,1],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722":[1,0,109,37],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a031b8663650bac994cfe2f5591d67c83":[1,0,109,37,9],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a0969ec228f1229e6bcc79ce855ac6aed":[1,0,109,37,6],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a1914b433ed46dafedd2a68e1a86263aa":[1,0,109,37,4],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a4126810bfec617b4e880a55f3aebeca2":[1,0,109,37,14],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a43664bba5332e596bc9b73f9df43242a":[1,0,109,37,0],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a47d5a415b6aec69adb818a34412a667a":[1,0,109,37,17],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a4a2814633ccc7b79037f63d9d88562e4":[1,0,109,37,1],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a5de39d213f18e985d37d1ed238c2c56e":[1,0,109,37,5],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a6d577ff1466df384cac14a0a9078803b":[1,0,109,37,15],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a78db7f68c629a05a039e939ea9b07b89":[1,0,109,37,12],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a7efa4fb2b2a8de1edc8fa59da56a6475":[1,0,109,37,16],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722a8aaf72b9fdf052d657623d18a05c56ba":[1,0,109,37,11],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722aa8e5b06c0a7eb6140cd65f6b5324ef6f":[1,0,109,37,8],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722aae14876bfd76423cfb31282f5a6a2633":[1,0,109,37,7],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722ab6cb9d0fe042b32930c3ba584d85aa5b":[1,0,109,37,10],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722ac41b9cb406a2f3b8053413e0015597e4":[1,0,109,37,13],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722ae04913639b8620672760751ca553cb75":[1,0,109,37,2],
"namespaceDigikamGenericPanoramaPlugin.html#a16637a5b27722b25bdf213184ea12722af253807e24dfec621b39c7a3a818fc48":[1,0,109,37,3],
"namespaceDigikamGenericPanoramaPlugin.html#a3cd88edfce8112c283866f63b4f1cf63":[1,0,109,36],
"namespaceDigikamGenericPinterestPlugin.html":[1,0,110],
"namespaceDigikamGenericPiwigoPlugin.html":[1,0,111],
"namespaceDigikamGenericPresentationPlugin.html":[1,0,112],
"namespaceDigikamGenericPresentationPlugin.html#a193147273bf9f084ac3ad2af71546384":[1,0,112,23],
"namespaceDigikamGenericPresentationPlugin.html#a738dd94d56edaab610af184eeba8dde3":[1,0,112,24],
"namespaceDigikamGenericPrintCreatorPlugin.html":[1,0,113],
"namespaceDigikamGenericRajcePlugin.html":[1,0,114],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76":[1,0,114,16],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a28a742f49f408651576f12344aa743f4":[1,0,114,16,11],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a4b89c70bd4936ef6307bb05b1f3bd14b":[1,0,114,16,2],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a5faed831b053d871f45c35a3067ffe03":[1,0,114,16,16],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a6a5767f069f3ff2479da9d9c120639e7":[1,0,114,16,18],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a76bdb5430279087287c08aa166e952c1":[1,0,114,16,5],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a870238a32f4e44f31b62e3c670f4844e":[1,0,114,16,13],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a8864077221ddc91ae5eb8d619c697e63":[1,0,114,16,8],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76a8ad96d13f75330c1f6b830139ce9a4f4":[1,0,114,16,9],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76aaaf9fae5aa5ac9e422ec4a4c6452b9be":[1,0,114,16,0],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76aacfa1e860d363c99b456ac50664f3aac":[1,0,114,16,3],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76aae8a7977362204a23b40d7e7d281520f":[1,0,114,16,17],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76ab759cafd9595421867801741ebc40d2d":[1,0,114,16,14],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76ac65962418564a781fe32df7f08609571":[1,0,114,16,6],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76acbd39b17d2ad4cc026515347ebe44d16":[1,0,114,16,7],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76adf1e856d65b85f9263b60c74e4cc24e2":[1,0,114,16,12],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76ae246529b0de231f06be70bace8876b91":[1,0,114,16,15],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76ae4d7bb0bd7b3e7728162663d3a43b48b":[1,0,114,16,4],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76af2cc8cf5f764f923d1bf8f39b4c5e086":[1,0,114,16,1],
"namespaceDigikamGenericRajcePlugin.html#a387dd84a90c0e86b0e60881e07581a76af52b30fff3208cfc6532e607e789b317":[1,0,114,16,10],
"namespaceDigikamGenericRajcePlugin.html#a599822370205672e7626a6bef1d50f9f":[1,0,114,17],
"namespaceDigikamGenericRajcePlugin.html#a5e0400c20e9f43e9013708f04d12c2ce":[1,0,114,19],
"namespaceDigikamGenericRajcePlugin.html#a743b210267327ad3d7b54b6527217860":[1,0,114,18],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89":[1,0,114,15],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89a07b95ccb63d9a5e82a329ce09c08b0c8":[1,0,114,15,6],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89a3d47336804ef7f55643473e0e89b1667":[1,0,114,15,5],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89a5d2cde347790dcc9e7d01b4e4c014c60":[1,0,114,15,3],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89a98cf3652c496766dfc331021ffeab2db":[1,0,114,15,0],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89aa2b8965eb1316ad495a526603e02c91d":[1,0,114,15,2],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89aa6a03f0546fc9217cb391d569873ab9e":[1,0,114,15,4],
"namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89aa81addba5f0f32b351c44934762388af":[1,0,114,15,1],
"namespaceDigikamGenericSendByMailPlugin.html":[1,0,115],
"namespaceDigikamGenericSlideShowPlugin.html":[1,0,116],
"namespaceDigikamGenericSmugPlugin.html":[1,0,117],
"namespaceDigikamGenericTimeAdjustPlugin.html":[1,0,118],
"namespaceDigikamGenericTwitterPlugin.html":[1,0,119],
"namespaceDigikamGenericTwitterPlugin.html#ae32b96d258117b2bfee60537dbfa8548":[1,0,119,9],
"namespaceDigikamGenericUnifiedPlugin.html":[1,0,120],
"namespaceDigikamGenericVKontaktePlugin.html":[1,0,122],
"namespaceDigikamGenericVideoSlideShowPlugin.html":[1,0,121],
"namespaceDigikamGenericWallpaperPlugin.html":[1,0,123],
"namespaceDigikamGenericWallpaperPlugin.html#a39dad453ee9b6ffd6e1874e64e516aac":[1,0,123,2],
"namespaceDigikamGenericYFPlugin.html":[1,0,124],
"namespaceDigikamGenericYFPlugin.html#a357a39bb2b8a12e3837a8d2922d35e78":[1,0,124,7],
"namespaceDigikamGenericYFPlugin.html#a830c2685e2882f346fb51809fc526a05":[1,0,124,8],
"namespaceDigikamHEIFDImgPlugin.html":[1,0,125],
"namespaceDigikamImageMagickDImgPlugin.html":[1,0,126],
"namespaceDigikamJPEG2000DImgPlugin.html":[1,0,127],
"namespaceDigikamJPEGDImgPlugin.html":[1,0,128],
"namespaceDigikamPGFDImgPlugin.html":[1,0,129],
"namespaceDigikamPNGDImgPlugin.html":[1,0,130],
"namespaceDigikamPNGDImgPlugin.html#af928d5678e07f261e9d0b1d548b174f1":[1,0,130,2],
"namespaceDigikamQImageDImgPlugin.html":[1,0,131],
"namespaceDigikamRAWDImgPlugin.html":[1,0,132],
"namespaceDigikamRawImportDarkTablePlugin.html":[1,0,133],
"namespaceDigikamRawImportNativePlugin.html":[1,0,134],
"namespaceDigikamRawImportRawTherapeePlugin.html":[1,0,135],
"namespaceDigikamRawImportUFRawPlugin.html":[1,0,136],
"namespaceDigikamTIFFDImgPlugin.html":[1,0,137],
"namespaceDigikam_1_1AlbumRoot.html":[1,0,0,0],
"namespaceDigikam_1_1AlbumRoot.html#ad2ec6653a725e0c937f00e2909b6aa22":[1,0,0,0,0],
"namespaceDigikam_1_1AlbumRoot.html#ad2ec6653a725e0c937f00e2909b6aa22a041cb3acce368b4394c24cae3c03dcda":[1,0,0,0,0,2],
"namespaceDigikam_1_1AlbumRoot.html#ad2ec6653a725e0c937f00e2909b6aa22a65fc1d1a421c7b29a600362afcd6cf47":[1,0,0,0,0,0],
"namespaceDigikam_1_1AlbumRoot.html#ad2ec6653a725e0c937f00e2909b6aa22a8313aad6b2a80bdf0d28f889080bac6e":[1,0,0,0,0,1],
"namespaceDigikam_1_1AlbumRoot.html#ad2ec6653a725e0c937f00e2909b6aa22aeabacafa7f4bbcb190d2360079250131":[1,0,0,0,0,3],
"namespaceDigikam_1_1CanonInternal.html":[1,0,0,1],
"namespaceDigikam_1_1CanonInternal.html#a27fca8687a505aaaba6eeb8573738caf":[1,0,0,1,0],
"namespaceDigikam_1_1CanonInternal.html#a2a0bf66255e73a895d8bbc0ece129097":[1,0,0,1,1],
"namespaceDigikam_1_1CanonInternal.html#a2a58fd696f5300701d062daacaea20ab":[1,0,0,1,2],
"namespaceDigikam_1_1CanonInternal.html#aa215756f65eb28f1eda9e281cf16bd3a":[1,0,0,1,3],
"namespaceDigikam_1_1CollectionScannerHints.html":[1,0,0,2],
"namespaceDigikam_1_1CollectionScannerHints.html#a1e8ccabb6a9d0632d9322391dca50ca6":[1,0,0,2,5],
"namespaceDigikam_1_1CollectionScannerHints.html#a8fda9fd2e80e1d7e805e5e48a287e67e":[1,0,0,2,3],
"namespaceDigikam_1_1CollectionScannerHints.html#a9da597118d5dc02da57050ead23793aa":[1,0,0,2,4],
"namespaceDigikam_1_1ColorTools.html":[1,0,0,3],
"namespaceDigikam_1_1ColorTools.html#a0df84e0dfac2ee091bd2bad5e59ce789":[1,0,0,3,3],
"namespaceDigikam_1_1ColorTools.html#a47014e9496fab9c1e1d2abe34023d1fe":[1,0,0,3,4],
"namespaceDigikam_1_1ColorTools.html#a4f99843a37770491bfdcdeeeee0dd1f8":[1,0,0,3,5],
"namespaceDigikam_1_1ColorTools.html#a99020af4c4090f80147f86c39e3a6e0f":[1,0,0,3,0]
};
