var classDigikamGenericINatPlugin_1_1INatWidget_1_1Private =
[
    [ "Private", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a5839c5983a81dc3f3939ea50604e8160", null ],
    [ "accountIcon", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#ad27c4d79ee21b76a42f969cf048ef31b", null ],
    [ "closestKnownObservation", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a24bb472333550614b36ebb3fa463fcef", null ],
    [ "closestObservationMaxSpB", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a15c857c1d35e48cd217d10e6727ad0d4", null ],
    [ "identificationEdit", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a5f62e8a4408c8b84e2faa2ffeda73840", null ],
    [ "identificationImage", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a8b14552e30f87f5ef3c1aba3cb873b4f", null ],
    [ "identificationLabel", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a4e7277246704c0d7f708644c5b33ebfc", null ],
    [ "imglst", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a7e8d30f2d52ef239f63948a212614f2f", null ],
    [ "moreOptionsButton", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a968d1d533e5367d415684fe38e401b90", null ],
    [ "moreOptionsWidget", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#adad2a9367c370251abc92736b5b297aa", null ],
    [ "observationDescription", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#aa82aa7de8c453c1d7d547d7bec2ab825", null ],
    [ "photoMaxDistanceSpB", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#ae0a00b199f07976d41b2295b50060878", null ],
    [ "photoMaxTimeDiffSpB", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a86a00b8ed40c676cd07d76d6f2843bfa", null ],
    [ "placesComboBox", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#ae21a9a55b56351d1b7fe1b0e3fd09dbd", null ],
    [ "removeAccount", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a5eae8b01c5916fc6790dc686caee52c7", null ],
    [ "serviceName", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#afd438292598521473dbbfb326d777672", null ],
    [ "taxonPopup", "classDigikamGenericINatPlugin_1_1INatWidget_1_1Private.html#a449213d89503c670acba97a9e120a5ba", null ]
];