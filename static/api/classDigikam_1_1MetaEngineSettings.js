var classDigikam_1_1MetaEngineSettings =
[
    [ "exifRotate", "classDigikam_1_1MetaEngineSettings.html#aa9c2f36eaa3a427a804708f007c83dc3", null ],
    [ "setSettings", "classDigikam_1_1MetaEngineSettings.html#af8ed7bdad06ce0a2f1d266aefb7652ae", null ],
    [ "settings", "classDigikam_1_1MetaEngineSettings.html#ab9f4398726b67c0fbf1c1787e270ab82", null ],
    [ "signalMetaEngineSettingsChanged", "classDigikam_1_1MetaEngineSettings.html#a8993f41db38a6bb759905a79b16fc22e", null ],
    [ "signalSettingsChanged", "classDigikam_1_1MetaEngineSettings.html#a47abfdb77a50bf868fc58454652dbb1a", null ],
    [ "MetaEngineSettingsCreator", "classDigikam_1_1MetaEngineSettings.html#a3ac6e2b8ac0d2bc7db125d487c0e364c", null ]
];