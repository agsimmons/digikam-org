var vectoroperations_8h =
[
    [ "length_squared", "vectoroperations_8h.html#a6083dd76a1a61e8e4f307cc42c51ae9c", null ],
    [ "operator*", "vectoroperations_8h.html#ae04e0246823b30d99ad9fcce6c46ae84", null ],
    [ "operator*", "vectoroperations_8h.html#a1ba3f8f0090007b03c263f830eb4e518", null ],
    [ "operator*", "vectoroperations_8h.html#a86ca42ea9fdc2de98dbbc5aeeafe5034", null ],
    [ "operator*", "vectoroperations_8h.html#a10e0e6e5ebc7e19805ae47bd0111882c", null ],
    [ "operator+", "vectoroperations_8h.html#a389465692942afe92e5af0f1cbb97bde", null ],
    [ "operator+", "vectoroperations_8h.html#a597b500e4442ce1411cce83b011fdd74", null ],
    [ "operator+", "vectoroperations_8h.html#a8f5a5a43e54ce3f19bbd911ef20c00b0", null ],
    [ "operator-", "vectoroperations_8h.html#abac8727ba70820734b281f442c9a6a1c", null ],
    [ "operator-", "vectoroperations_8h.html#a73b2685c1a6203f05cbb2523379b3b0e", null ],
    [ "operator/", "vectoroperations_8h.html#a8d406c04fde62047839855cbfa8dd8ff", null ],
    [ "operator/", "vectoroperations_8h.html#ab9c71ea464b366b03fce3fd7a709c012", null ]
];