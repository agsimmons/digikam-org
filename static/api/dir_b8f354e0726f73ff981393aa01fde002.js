var dir_b8f354e0726f73ff981393aa01fde002 =
[
    [ "itempropertieshistorytab.cpp", "itempropertieshistorytab_8cpp.html", null ],
    [ "itempropertieshistorytab.h", "itempropertieshistorytab_8h.html", [
      [ "ItemPropertiesHistoryTab", "classDigikam_1_1ItemPropertiesHistoryTab.html", "classDigikam_1_1ItemPropertiesHistoryTab" ],
      [ "RemoveFilterAction", "classDigikam_1_1RemoveFilterAction.html", "classDigikam_1_1RemoveFilterAction" ]
    ] ],
    [ "itempropertiesversionstab.cpp", "itempropertiesversionstab_8cpp.html", null ],
    [ "itempropertiesversionstab.h", "itempropertiesversionstab_8h.html", [
      [ "ItemPropertiesVersionsTab", "classDigikam_1_1ItemPropertiesVersionsTab.html", "classDigikam_1_1ItemPropertiesVersionsTab" ]
    ] ],
    [ "versionsdelegate.cpp", "versionsdelegate_8cpp.html", null ],
    [ "versionsdelegate.h", "versionsdelegate_8h.html", [
      [ "VersionsDelegate", "classDigikam_1_1VersionsDelegate.html", "classDigikam_1_1VersionsDelegate" ]
    ] ],
    [ "versionsoverlays.cpp", "versionsoverlays_8cpp.html", null ],
    [ "versionsoverlays.h", "versionsoverlays_8h.html", [
      [ "ActionVersionsOverlay", "classDigikam_1_1ActionVersionsOverlay.html", "classDigikam_1_1ActionVersionsOverlay" ],
      [ "ShowHideVersionsOverlay", "classDigikam_1_1ShowHideVersionsOverlay.html", "classDigikam_1_1ShowHideVersionsOverlay" ]
    ] ],
    [ "versionstreeview.cpp", "versionstreeview_8cpp.html", null ],
    [ "versionstreeview.h", "versionstreeview_8h.html", [
      [ "VersionsTreeView", "classDigikam_1_1VersionsTreeView.html", "classDigikam_1_1VersionsTreeView" ]
    ] ]
];