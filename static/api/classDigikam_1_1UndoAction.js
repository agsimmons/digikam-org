var classDigikam_1_1UndoAction =
[
    [ "UndoAction", "classDigikam_1_1UndoAction.html#ad21e272fe3d26c2362b0a44b71461e47", null ],
    [ "~UndoAction", "classDigikam_1_1UndoAction.html#a474b1fd13c61815cdeeaa6a569bf0a67", null ],
    [ "fileOriginData", "classDigikam_1_1UndoAction.html#a832a7070cde0b021738d3d59b497324b", null ],
    [ "fileOriginResolvedHistory", "classDigikam_1_1UndoAction.html#af3932452421bbf2532063d836e1a412f", null ],
    [ "getMetadata", "classDigikam_1_1UndoAction.html#adbfd2809dfe36bd4c09246f02f9ad0e9", null ],
    [ "getTitle", "classDigikam_1_1UndoAction.html#a68fcd30401a224212d7d6cbd97be72f0", null ],
    [ "hasFileOriginData", "classDigikam_1_1UndoAction.html#ae7699bd585c82d1beb91d85a34b42e17", null ],
    [ "setFileOriginData", "classDigikam_1_1UndoAction.html#ab6dac017863266442c9d910f18ad5bab", null ],
    [ "setMetadata", "classDigikam_1_1UndoAction.html#a2d9079762b333054ab52818a1b66fca1", null ],
    [ "setTitle", "classDigikam_1_1UndoAction.html#a099f5350e231a4196a2198d22833d565", null ]
];