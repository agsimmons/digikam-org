var dir_ebf7a013b3a1d3f726c01d38054b7e85 =
[
    [ "scancontroller.cpp", "scancontroller_8cpp.html", null ],
    [ "scancontroller.h", "scancontroller_8h.html", [
      [ "ScanController", "classDigikam_1_1ScanController.html", "classDigikam_1_1ScanController" ],
      [ "FileMetadataWrite", "classDigikam_1_1ScanController_1_1FileMetadataWrite.html", "classDigikam_1_1ScanController_1_1FileMetadataWrite" ]
    ] ],
    [ "scancontroller_p.cpp", "scancontroller__p_8cpp.html", null ],
    [ "scancontroller_p.h", "scancontroller__p_8h.html", [
      [ "Private", "classDigikam_1_1ScanController_1_1Private.html", "classDigikam_1_1ScanController_1_1Private" ],
      [ "ScanControllerCreator", "classDigikam_1_1ScanControllerCreator.html", "classDigikam_1_1ScanControllerCreator" ],
      [ "ScanControllerLoadingCacheFileWatch", "classDigikam_1_1ScanControllerLoadingCacheFileWatch.html", "classDigikam_1_1ScanControllerLoadingCacheFileWatch" ],
      [ "SimpleCollectionScannerObserver", "classDigikam_1_1SimpleCollectionScannerObserver.html", "classDigikam_1_1SimpleCollectionScannerObserver" ]
    ] ],
    [ "scancontroller_progress.cpp", "scancontroller__progress_8cpp.html", null ],
    [ "scancontroller_start.cpp", "scancontroller__start_8cpp.html", null ],
    [ "scancontroller_stop.cpp", "scancontroller__stop_8cpp.html", null ]
];