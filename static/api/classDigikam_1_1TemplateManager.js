var classDigikam_1_1TemplateManager =
[
    [ "clear", "classDigikam_1_1TemplateManager.html#a9e40c8aabcba1d3b8c75ed548f708062", null ],
    [ "findByContents", "classDigikam_1_1TemplateManager.html#ac2b55175dce38469023d921ff926adb7", null ],
    [ "findByTitle", "classDigikam_1_1TemplateManager.html#a1338a1145ed6210496f1410a785dbe6d", null ],
    [ "fromIndex", "classDigikam_1_1TemplateManager.html#af6932bdf8e32c33ed0e9ccaec001988f", null ],
    [ "insert", "classDigikam_1_1TemplateManager.html#ae8ae0619ce57c81a2d8996e970a82a89", null ],
    [ "load", "classDigikam_1_1TemplateManager.html#adcb3c492f153cd95aa7b50a6ad20a9a8", null ],
    [ "remove", "classDigikam_1_1TemplateManager.html#a916546b2086d805919e4bb643afd6b5e", null ],
    [ "save", "classDigikam_1_1TemplateManager.html#a62e90ebea56f1f2cc1455a5d96b57de2", null ],
    [ "signalTemplateAdded", "classDigikam_1_1TemplateManager.html#a4ac5a85e63dd34664bcfb026c22fdb96", null ],
    [ "signalTemplateRemoved", "classDigikam_1_1TemplateManager.html#a2af2cb29d448b2148bb2c0750556e759", null ],
    [ "templateList", "classDigikam_1_1TemplateManager.html#a7f5ddbc28be8dd9b5975bfae42ad19a3", null ],
    [ "TemplateManagerCreator", "classDigikam_1_1TemplateManager.html#ab26171a9f2340b256fbdb3d435e2504d", null ]
];