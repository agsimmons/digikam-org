var classDigikam_1_1TrackManager =
[
    [ "Track", "classDigikam_1_1TrackManager_1_1Track.html", "classDigikam_1_1TrackManager_1_1Track" ],
    [ "TrackPoint", "classDigikam_1_1TrackManager_1_1TrackPoint.html", "classDigikam_1_1TrackManager_1_1TrackPoint" ],
    [ "Id", "classDigikam_1_1TrackManager.html#ada98b9b8360d39dd5f19040b23f6bb62", null ],
    [ "TrackChanges", "classDigikam_1_1TrackManager.html#a34f76ecb7e4c007aa5b0686b14a51814", null ],
    [ "ChangeFlag", "classDigikam_1_1TrackManager.html#ac842a0f2d575d8484bd1c30eb6ee50a1", [
      [ "ChangeTrackPoints", "classDigikam_1_1TrackManager.html#ac842a0f2d575d8484bd1c30eb6ee50a1abc2f6e1de8912f617a17f8b6f04cbcdb", null ],
      [ "ChangeMetadata", "classDigikam_1_1TrackManager.html#ac842a0f2d575d8484bd1c30eb6ee50a1a7f3bd495c70f9841f145d69b2ae1ac3a", null ],
      [ "ChangeRemoved", "classDigikam_1_1TrackManager.html#ac842a0f2d575d8484bd1c30eb6ee50a1a87afaefdccb04fa543e07c0eb297e965", null ],
      [ "ChangeAdd", "classDigikam_1_1TrackManager.html#ac842a0f2d575d8484bd1c30eb6ee50a1ac28fed943fea0b3bf8c9f0f008ae6cd9", null ]
    ] ],
    [ "TrackManager", "classDigikam_1_1TrackManager.html#a12be8be5ff4c111379d32f50cbfec5db", null ],
    [ "~TrackManager", "classDigikam_1_1TrackManager.html#ae532527908814ec45433ab6b22428767", null ],
    [ "clear", "classDigikam_1_1TrackManager.html#a726ecacdeafd3f1e966fbf0fa6d50507", null ],
    [ "getNextFreeTrackColor", "classDigikam_1_1TrackManager.html#ac053b29102bdcf4456938ac935a027ef", null ],
    [ "getNextFreeTrackId", "classDigikam_1_1TrackManager.html#aae57303ddc98ef6034a51a6c5132c12d", null ],
    [ "getTrack", "classDigikam_1_1TrackManager.html#a330b148be1bf9cfc11933581d1bf63f7", null ],
    [ "getTrackById", "classDigikam_1_1TrackManager.html#a83870370b0d9dbfeda44aaf0a2e44d46", null ],
    [ "getTrackList", "classDigikam_1_1TrackManager.html#abbfd265f9d01c0ca1a9e186491dcd9f4", null ],
    [ "getVisibility", "classDigikam_1_1TrackManager.html#a363b77463490e4b7d3925ef2bdae79cc", null ],
    [ "loadTrackFiles", "classDigikam_1_1TrackManager.html#a865167007cd8c861d58438e5be61e693", null ],
    [ "readLoadErrors", "classDigikam_1_1TrackManager.html#af5999020fa96d7a9814f51052698e650", null ],
    [ "setVisibility", "classDigikam_1_1TrackManager.html#a65d8dc4b5b67283e71695160979faf1d", null ],
    [ "signalAllTrackFilesReady", "classDigikam_1_1TrackManager.html#a6ca7437ba0eed33ea4a75f6e99c0c70f", null ],
    [ "signalTrackFilesReadyAt", "classDigikam_1_1TrackManager.html#a39bf8810e9d1e248d5e996441e053bd2", null ],
    [ "signalTracksChanged", "classDigikam_1_1TrackManager.html#aec4d198fbdbf50a2f973a7557d6c7e14", null ],
    [ "signalVisibilityChanged", "classDigikam_1_1TrackManager.html#a2dc97c98deefafd11054fd67698dfaa0", null ],
    [ "trackCount", "classDigikam_1_1TrackManager.html#a0c7f93c687b5fb74310e8c4e4f04c7f4", null ]
];