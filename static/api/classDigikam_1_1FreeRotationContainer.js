var classDigikam_1_1FreeRotationContainer =
[
    [ "AutoCropTypes", "classDigikam_1_1FreeRotationContainer.html#ab820d7c67b425b31ffc379de64d65f6b", [
      [ "NoAutoCrop", "classDigikam_1_1FreeRotationContainer.html#ab820d7c67b425b31ffc379de64d65f6baa13a98a9fcf8685953a135706d0c0560", null ],
      [ "WidestArea", "classDigikam_1_1FreeRotationContainer.html#ab820d7c67b425b31ffc379de64d65f6ba88767d30940bc8219b85b17f2785bc17", null ],
      [ "LargestArea", "classDigikam_1_1FreeRotationContainer.html#ab820d7c67b425b31ffc379de64d65f6baca890c6b25800be5b7e50406afe797a3", null ]
    ] ],
    [ "FreeRotationContainer", "classDigikam_1_1FreeRotationContainer.html#af6df2c8a30d6e57c6d310dbfe76388a8", null ],
    [ "~FreeRotationContainer", "classDigikam_1_1FreeRotationContainer.html#a5af8256bdf775bfa0c221e54e6625656", null ],
    [ "angle", "classDigikam_1_1FreeRotationContainer.html#a251524b99c645381277498eedb3822d8", null ],
    [ "antiAlias", "classDigikam_1_1FreeRotationContainer.html#ab0356431fb0c433f10f43998c54476b7", null ],
    [ "autoCrop", "classDigikam_1_1FreeRotationContainer.html#ae43ba6531865b481f9f5a118ae7de91f", null ],
    [ "backgroundColor", "classDigikam_1_1FreeRotationContainer.html#aae90ebc0eafa066a5afffc473a6f730e", null ],
    [ "newSize", "classDigikam_1_1FreeRotationContainer.html#a82645dc85203f7568fb64980c8326527", null ],
    [ "orgH", "classDigikam_1_1FreeRotationContainer.html#a48b479910cf7c859e3238190e3d0ac52", null ],
    [ "orgW", "classDigikam_1_1FreeRotationContainer.html#a07ddb904d9b53361e5edd8585bb052af", null ]
];