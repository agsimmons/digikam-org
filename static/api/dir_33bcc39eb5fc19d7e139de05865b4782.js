var dir_33bcc39eb5fc19d7e139de05865b4782 =
[
    [ "detectionbenchmarker.cpp", "detectionbenchmarker_8cpp.html", null ],
    [ "detectionbenchmarker.h", "detectionbenchmarker_8h.html", [
      [ "DetectionBenchmarker", "classDigikam_1_1DetectionBenchmarker.html", "classDigikam_1_1DetectionBenchmarker" ]
    ] ],
    [ "recognitionbenchmarker.cpp", "recognitionbenchmarker_8cpp.html", null ],
    [ "recognitionbenchmarker.h", "recognitionbenchmarker_8h.html", [
      [ "RecognitionBenchmarker", "classDigikam_1_1RecognitionBenchmarker.html", "classDigikam_1_1RecognitionBenchmarker" ],
      [ "Statistics", "classDigikam_1_1RecognitionBenchmarker_1_1Statistics.html", "classDigikam_1_1RecognitionBenchmarker_1_1Statistics" ]
    ] ]
];