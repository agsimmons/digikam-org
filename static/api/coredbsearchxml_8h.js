var coredbsearchxml_8h =
[
    [ "KeywordSearchReader", "classDigikam_1_1KeywordSearchReader.html", "classDigikam_1_1KeywordSearchReader" ],
    [ "KeywordSearchWriter", "classDigikam_1_1KeywordSearchWriter.html", "classDigikam_1_1KeywordSearchWriter" ],
    [ "SearchXmlCachingReader", "classDigikam_1_1SearchXmlCachingReader.html", "classDigikam_1_1SearchXmlCachingReader" ],
    [ "SearchXmlReader", "classDigikam_1_1SearchXmlReader.html", "classDigikam_1_1SearchXmlReader" ],
    [ "SearchXmlWriter", "classDigikam_1_1SearchXmlWriter.html", "classDigikam_1_1SearchXmlWriter" ],
    [ "Element", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98", [
      [ "Search", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98a31e8cf30632f43328d12790b47508b99", null ],
      [ "Group", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98a66eea0b86179e31d8e8be06c417a6113", null ],
      [ "GroupEnd", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98aaa4ca569d6891dc40df09158a16392e9", null ],
      [ "Field", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98a39e8449c7a21c4bce5a2fc90f66a2f76", null ],
      [ "FieldEnd", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98aace5de0b20c6a3f0cbd5784b6012b99f", null ],
      [ "End", "coredbsearchxml_8h.html#a59fa980f3f104d44e6da9a7c087c8a98a503a4bc24f55bb42c747ae94db93ed86", null ]
    ] ],
    [ "Operator", "coredbsearchxml_8h.html#a9375180c0f0d224f75ef7865ebc1b45f", [
      [ "And", "coredbsearchxml_8h.html#a9375180c0f0d224f75ef7865ebc1b45fa9b7231abdc69db3dd8d7b32218f0e187", null ],
      [ "Or", "coredbsearchxml_8h.html#a9375180c0f0d224f75ef7865ebc1b45fa0473837bb41a68a13b890c40450e240a", null ],
      [ "AndNot", "coredbsearchxml_8h.html#a9375180c0f0d224f75ef7865ebc1b45fa70b96d8575b8828cccd9aee21baa325e", null ],
      [ "OrNot", "coredbsearchxml_8h.html#a9375180c0f0d224f75ef7865ebc1b45fa0f0ce1f1de34e5e66aaee3a3be292380", null ]
    ] ],
    [ "Relation", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770dae", [
      [ "Equal", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea6d41d9fe1eac81855e1b9d2339c83be0", null ],
      [ "Unequal", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeabcc87e1dfb2571f81be2cae57c0f8478", null ],
      [ "Like", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea2fe6feb154c62c702e47b8bdf4dda83d", null ],
      [ "NotLike", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea03a12282f295fe8b6f6217b232af7af5", null ],
      [ "LessThan", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeab1d4fa924602bc2b562b14878bd1ece4", null ],
      [ "GreaterThan", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeaf2788debf2fa05c014db5f6bb064dfb2", null ],
      [ "LessThanOrEqual", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea3c4ccc2e516a49b8543924991dcc11bc", null ],
      [ "GreaterThanOrEqual", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeadcfc9bb60802d02c992cefc5c5ae4e8c", null ],
      [ "Interval", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeac72014b2b3091a9cd89dd6f3e33f052b", null ],
      [ "IntervalOpen", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea9b0440ff3335037477d8729a3df61682", null ],
      [ "OneOf", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea0d448537158db8ca50cc84aadfb04581", null ],
      [ "AllOf", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea09e9fc3c7af8cfe5e0d122fc58526a28", null ],
      [ "InTree", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea5f26fc24006044aec00595bf6fb9debc", null ],
      [ "NotInTree", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeae8368e0e6dd3cb2664071db8c50831ca", null ],
      [ "Near", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daea541899c69ba6c02eadbbfae3ffa3dfce", null ],
      [ "Inside", "coredbsearchxml_8h.html#a15c0ad590e52942210ba80d980770daeaa50f5a9672285eec7f7d410548188ff7", null ]
    ] ],
    [ "merge", "coredbsearchxml_8h.html#a43f651d9d398a64b29eac267a352aab4", null ],
    [ "merge", "coredbsearchxml_8h.html#ae29b5a8077fc38049650bc021201481d", null ],
    [ "split", "coredbsearchxml_8h.html#a00d8781cb83f9450b3dba447320698d5", null ],
    [ "standardFieldOperator", "coredbsearchxml_8h.html#aa17cddf317c1e9971538a5c9db6584ba", null ],
    [ "standardFieldRelation", "coredbsearchxml_8h.html#a0e1215140d70f8d731c2cdc13034a896", null ],
    [ "standardGroupOperator", "coredbsearchxml_8h.html#a71db85c1bf046c109600c7e5af5befb9", null ],
    [ "testRelation", "coredbsearchxml_8h.html#a68882fec71f058b8e333652f7dd46280", null ]
];