var classDigikam_1_1HistogramBox =
[
    [ "HistogramBox", "classDigikam_1_1HistogramBox.html#a42871802e4258665b539532712729bfa", null ],
    [ "~HistogramBox", "classDigikam_1_1HistogramBox.html#a19374c061d0bc48c6f1a73d8444388ec", null ],
    [ "channel", "classDigikam_1_1HistogramBox.html#ab4c35386a00248453873fa3684492599", null ],
    [ "histogram", "classDigikam_1_1HistogramBox.html#a8033b687d174c0f1db28bd951613aaf5", null ],
    [ "scale", "classDigikam_1_1HistogramBox.html#a1a80143b892f052f00c72045002d104f", null ],
    [ "setChannel", "classDigikam_1_1HistogramBox.html#a62d391fe9ae7d4d091695ccd5d3d3023", null ],
    [ "setChannelEnabled", "classDigikam_1_1HistogramBox.html#ad3cbc9f122571094aa2c40c37d1c06d3", null ],
    [ "setGradientColors", "classDigikam_1_1HistogramBox.html#a8e6139639592512ecdfe3a388807120d", null ],
    [ "setGradientVisible", "classDigikam_1_1HistogramBox.html#a84a49acdc7f3720f96565b2fb5f8209d", null ],
    [ "setHistogramMargin", "classDigikam_1_1HistogramBox.html#a13e5a9cc5487c0448d8979b41fdb8e58", null ],
    [ "setHistogramType", "classDigikam_1_1HistogramBox.html#ae9cd868a16c3e3ff902f9eb7a0188642", null ],
    [ "setScale", "classDigikam_1_1HistogramBox.html#ae61689eaef515d431f009f61a955d706", null ],
    [ "setStatisticsVisible", "classDigikam_1_1HistogramBox.html#a31b920f0e52a0402bc32cfac291de367", null ],
    [ "signalChannelChanged", "classDigikam_1_1HistogramBox.html#a7d864309f37bcbd89b17af77fb7b183e", null ],
    [ "signalScaleChanged", "classDigikam_1_1HistogramBox.html#a12e512efac68b069cc3cda64374757a5", null ],
    [ "slotChannelChanged", "classDigikam_1_1HistogramBox.html#a87ad0913bab640270a7f99ab4d3a37f1", null ],
    [ "slotScaleChanged", "classDigikam_1_1HistogramBox.html#a60445f3019205ddc034330d26b256bfb", null ]
];