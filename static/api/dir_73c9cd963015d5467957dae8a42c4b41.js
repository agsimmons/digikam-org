var dir_73c9cd963015d5467957dae8a42c4b41 =
[
    [ "cameracontroller.cpp", "cameracontroller_8cpp.html", null ],
    [ "cameracontroller.h", "cameracontroller_8h.html", [
      [ "CameraController", "classDigikam_1_1CameraController.html", "classDigikam_1_1CameraController" ]
    ] ],
    [ "camerahistoryupdater.cpp", "camerahistoryupdater_8cpp.html", null ],
    [ "camerahistoryupdater.h", "camerahistoryupdater_8h.html", "camerahistoryupdater_8h" ],
    [ "camerathumbsctrl.cpp", "camerathumbsctrl_8cpp.html", null ],
    [ "camerathumbsctrl.h", "camerathumbsctrl_8h.html", "camerathumbsctrl_8h" ],
    [ "camiteminfo.cpp", "camiteminfo_8cpp.html", "camiteminfo_8cpp" ],
    [ "camiteminfo.h", "camiteminfo_8h.html", "camiteminfo_8h" ],
    [ "dkcamera.cpp", "dkcamera_8cpp.html", null ],
    [ "dkcamera.h", "dkcamera_8h.html", [
      [ "DKCamera", "classDigikam_1_1DKCamera.html", "classDigikam_1_1DKCamera" ]
    ] ],
    [ "downloadsettings.h", "downloadsettings_8h.html", "downloadsettings_8h" ],
    [ "gpcamera.cpp", "gpcamera_8cpp.html", null ],
    [ "gpcamera.h", "gpcamera_8h.html", [
      [ "GPCamera", "classDigikam_1_1GPCamera.html", "classDigikam_1_1GPCamera" ]
    ] ],
    [ "umscamera.cpp", "umscamera_8cpp.html", null ],
    [ "umscamera.h", "umscamera_8h.html", [
      [ "UMSCamera", "classDigikam_1_1UMSCamera.html", "classDigikam_1_1UMSCamera" ]
    ] ]
];