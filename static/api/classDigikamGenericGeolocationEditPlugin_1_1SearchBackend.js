var classDigikamGenericGeolocationEditPlugin_1_1SearchBackend =
[
    [ "SearchResult", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend_1_1SearchResult" ],
    [ "SearchBackend", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#a15c2d71a7775dbffd536291187e2f5b4", null ],
    [ "~SearchBackend", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#a38d0c834c5c6a4020d77b55f4c85bc83", null ],
    [ "getBackends", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#a989dfa8f4b4e6828b2f5d76d9f9fc721", null ],
    [ "getErrorMessage", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#a5663dcb971b5cfc93b7a6c0a2598331f", null ],
    [ "getResults", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#afd8eb2727641f98493649a5659f7fbcf", null ],
    [ "search", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#a14c8c8fd8bfc12dd184ec86d9ef64b34", null ],
    [ "signalSearchCompleted", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html#ac45f444575a5374d63996c43e0aeb1bc", null ]
];