var classDigikam_1_1BCGSettings =
[
    [ "BCGSettings", "classDigikam_1_1BCGSettings.html#adb15989965a592425666128a57acf5e6", null ],
    [ "~BCGSettings", "classDigikam_1_1BCGSettings.html#aafac0ae226ffea215b9cba0472ac8996", null ],
    [ "defaultSettings", "classDigikam_1_1BCGSettings.html#a55d897a16d48523f3c24c04e49ad4019", null ],
    [ "readSettings", "classDigikam_1_1BCGSettings.html#a0696d3452dcd9796ed07fdb40441c7d9", null ],
    [ "resetToDefault", "classDigikam_1_1BCGSettings.html#a7f323d05a38778ac69df216c1fc0970a", null ],
    [ "setSettings", "classDigikam_1_1BCGSettings.html#a8244a05ea20c3fe78f9cc359ebd93893", null ],
    [ "settings", "classDigikam_1_1BCGSettings.html#a88b3bbeb00ef996e51de1082ae467990", null ],
    [ "signalSettingsChanged", "classDigikam_1_1BCGSettings.html#ac119eef42059fc04f67985b4c3d18301", null ],
    [ "writeSettings", "classDigikam_1_1BCGSettings.html#a41dca7ed1a9f54ff659a972c1cad30e7", null ]
];