var dir_8b1be23f0460599755dc8ea7a40e8520 =
[
    [ "databasetask.cpp", "databasetask_8cpp.html", null ],
    [ "databasetask.h", "databasetask_8h.html", [
      [ "DatabaseTask", "classDigikam_1_1DatabaseTask.html", "classDigikam_1_1DatabaseTask" ]
    ] ],
    [ "dbcleaner.cpp", "dbcleaner_8cpp.html", null ],
    [ "dbcleaner.h", "dbcleaner_8h.html", [
      [ "DbCleaner", "classDigikam_1_1DbCleaner.html", "classDigikam_1_1DbCleaner" ],
      [ "DbShrinkDialog", "classDigikam_1_1DbShrinkDialog.html", "classDigikam_1_1DbShrinkDialog" ]
    ] ],
    [ "duplicatesfinder.cpp", "duplicatesfinder_8cpp.html", null ],
    [ "duplicatesfinder.h", "duplicatesfinder_8h.html", [
      [ "DuplicatesFinder", "classDigikam_1_1DuplicatesFinder.html", "classDigikam_1_1DuplicatesFinder" ]
    ] ],
    [ "facesdetector.cpp", "facesdetector_8cpp.html", null ],
    [ "facesdetector.h", "facesdetector_8h.html", [
      [ "FacesDetector", "classDigikam_1_1FacesDetector.html", "classDigikam_1_1FacesDetector" ]
    ] ],
    [ "fingerprintsgenerator.cpp", "fingerprintsgenerator_8cpp.html", null ],
    [ "fingerprintsgenerator.h", "fingerprintsgenerator_8h.html", [
      [ "FingerPrintsGenerator", "classDigikam_1_1FingerPrintsGenerator.html", "classDigikam_1_1FingerPrintsGenerator" ]
    ] ],
    [ "fingerprintstask.cpp", "fingerprintstask_8cpp.html", null ],
    [ "fingerprintstask.h", "fingerprintstask_8h.html", [
      [ "FingerprintsTask", "classDigikam_1_1FingerprintsTask.html", "classDigikam_1_1FingerprintsTask" ]
    ] ],
    [ "imagequalitysorter.cpp", "imagequalitysorter_8cpp.html", null ],
    [ "imagequalitysorter.h", "imagequalitysorter_8h.html", [
      [ "ImageQualitySorter", "classDigikam_1_1ImageQualitySorter.html", "classDigikam_1_1ImageQualitySorter" ]
    ] ],
    [ "imagequalitytask.cpp", "imagequalitytask_8cpp.html", null ],
    [ "imagequalitytask.h", "imagequalitytask_8h.html", [
      [ "ImageQualityTask", "classDigikam_1_1ImageQualityTask.html", "classDigikam_1_1ImageQualityTask" ]
    ] ],
    [ "iteminfoalbumsjob.cpp", "iteminfoalbumsjob_8cpp.html", null ],
    [ "iteminfoalbumsjob.h", "iteminfoalbumsjob_8h.html", [
      [ "ItemInfoAlbumsJob", "classDigikam_1_1ItemInfoAlbumsJob.html", "classDigikam_1_1ItemInfoAlbumsJob" ]
    ] ],
    [ "iteminfojob.cpp", "iteminfojob_8cpp.html", null ],
    [ "iteminfojob.h", "iteminfojob_8h.html", [
      [ "ItemInfoJob", "classDigikam_1_1ItemInfoJob.html", "classDigikam_1_1ItemInfoJob" ]
    ] ],
    [ "maintenancedata.cpp", "maintenancedata_8cpp.html", null ],
    [ "maintenancedata.h", "maintenancedata_8h.html", [
      [ "MaintenanceData", "classDigikam_1_1MaintenanceData.html", "classDigikam_1_1MaintenanceData" ]
    ] ],
    [ "maintenancedlg.cpp", "maintenancedlg_8cpp.html", null ],
    [ "maintenancedlg.h", "maintenancedlg_8h.html", [
      [ "MaintenanceDlg", "classDigikam_1_1MaintenanceDlg.html", "classDigikam_1_1MaintenanceDlg" ]
    ] ],
    [ "maintenancemngr.cpp", "maintenancemngr_8cpp.html", null ],
    [ "maintenancemngr.h", "maintenancemngr_8h.html", [
      [ "MaintenanceMngr", "classDigikam_1_1MaintenanceMngr.html", "classDigikam_1_1MaintenanceMngr" ]
    ] ],
    [ "maintenancesettings.cpp", "maintenancesettings_8cpp.html", "maintenancesettings_8cpp" ],
    [ "maintenancesettings.h", "maintenancesettings_8h.html", "maintenancesettings_8h" ],
    [ "maintenancethread.cpp", "maintenancethread_8cpp.html", null ],
    [ "maintenancethread.h", "maintenancethread_8h.html", [
      [ "MaintenanceThread", "classDigikam_1_1MaintenanceThread.html", "classDigikam_1_1MaintenanceThread" ]
    ] ],
    [ "maintenancetool.cpp", "maintenancetool_8cpp.html", null ],
    [ "maintenancetool.h", "maintenancetool_8h.html", [
      [ "MaintenanceTool", "classDigikam_1_1MaintenanceTool.html", "classDigikam_1_1MaintenanceTool" ]
    ] ],
    [ "metadatasynchronizer.cpp", "metadatasynchronizer_8cpp.html", null ],
    [ "metadatasynchronizer.h", "metadatasynchronizer_8h.html", [
      [ "MetadataSynchronizer", "classDigikam_1_1MetadataSynchronizer.html", "classDigikam_1_1MetadataSynchronizer" ]
    ] ],
    [ "metadatatask.cpp", "metadatatask_8cpp.html", null ],
    [ "metadatatask.h", "metadatatask_8h.html", [
      [ "MetadataTask", "classDigikam_1_1MetadataTask.html", "classDigikam_1_1MetadataTask" ]
    ] ],
    [ "newitemsfinder.cpp", "newitemsfinder_8cpp.html", null ],
    [ "newitemsfinder.h", "newitemsfinder_8h.html", [
      [ "NewItemsFinder", "classDigikam_1_1NewItemsFinder.html", "classDigikam_1_1NewItemsFinder" ]
    ] ],
    [ "thumbsgenerator.cpp", "thumbsgenerator_8cpp.html", null ],
    [ "thumbsgenerator.h", "thumbsgenerator_8h.html", [
      [ "ThumbsGenerator", "classDigikam_1_1ThumbsGenerator.html", "classDigikam_1_1ThumbsGenerator" ]
    ] ],
    [ "thumbstask.cpp", "thumbstask_8cpp.html", null ],
    [ "thumbstask.h", "thumbstask_8h.html", [
      [ "ThumbsTask", "classDigikam_1_1ThumbsTask.html", "classDigikam_1_1ThumbsTask" ]
    ] ]
];