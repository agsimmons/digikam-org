var regionframeitem_8cpp =
[
    [ "OptimalPosition", "regionframeitem_8cpp.html#aecf41f93f910a37c29f90b8ff3f63a9d", null ],
    [ "CropHandleFlag", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776", [
      [ "CH_None", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776a94bde4ab8180aae684a508da30ab1baa", null ],
      [ "CH_Top", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776aec830391f35318b5dabad3d350f93443", null ],
      [ "CH_Left", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776abe30abef4287fb3355a73e47fbe15f3a", null ],
      [ "CH_Right", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776a8a4f087d09024dbcef82899301e356a4", null ],
      [ "CH_Bottom", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776adaf6d73eb30a894dd677eeb68f863445", null ],
      [ "CH_TopLeft", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776a8b0c7c95a0b7ab1efbc402edb4d13c56", null ],
      [ "CH_BottomLeft", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776ac96163338b8d7b1fd9c10e59d38f1869", null ],
      [ "CH_TopRight", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776acd315c6d57c25a166e45c354772a7419", null ],
      [ "CH_BottomRight", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776a10efcbf77ec27edbcada960de283cc76", null ],
      [ "CH_Content", "regionframeitem_8cpp.html#a1f74686f2a16d3fea99b5694078c1776acec16481b507f812276f8e478248fe18", null ]
    ] ],
    [ "HudSide", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981", [
      [ "HS_None", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981a31c811c27354ebef98c5545f768f4259", null ],
      [ "HS_Top", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981a522578b8b0e3c23155a525089bbdfc28", null ],
      [ "HS_Bottom", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981af09eb64527cce574411a62b603d744da", null ],
      [ "HS_Inside", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981a97473562d3b3b651daba26b33244103a", null ],
      [ "HS_TopInside", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981ae17172351312ce4bf3fb4acc6004e49a", null ],
      [ "HS_BottomInside", "regionframeitem_8cpp.html#a237c6daebf394d401ed937526e839981afca6dfecac895710d746c3d5958e06e1", null ]
    ] ]
];