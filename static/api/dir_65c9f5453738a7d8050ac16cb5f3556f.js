var dir_65c9f5453738a7d8050ac16cb5f3556f =
[
    [ "dbinfoiface.cpp", "dbinfoiface_8cpp.html", null ],
    [ "dbinfoiface.h", "dbinfoiface_8h.html", [
      [ "DBInfoIface", "classDigikam_1_1DBInfoIface.html", "classDigikam_1_1DBInfoIface" ]
    ] ],
    [ "dio.cpp", "dio_8cpp.html", null ],
    [ "dio.h", "dio_8h.html", [
      [ "DIO", "classDigikam_1_1DIO.html", "classDigikam_1_1DIO" ]
    ] ],
    [ "diofinders.cpp", "diofinders_8cpp.html", null ],
    [ "diofinders.h", "diofinders_8h.html", [
      [ "GroupedImagesFinder", "classDigikam_1_1GroupedImagesFinder.html", "classDigikam_1_1GroupedImagesFinder" ],
      [ "SidecarFinder", "classDigikam_1_1SidecarFinder.html", "classDigikam_1_1SidecarFinder" ]
    ] ],
    [ "syncjob.cpp", "syncjob_8cpp.html", null ],
    [ "syncjob.h", "syncjob_8h.html", [
      [ "SyncJob", "classDigikam_1_1SyncJob.html", null ]
    ] ]
];