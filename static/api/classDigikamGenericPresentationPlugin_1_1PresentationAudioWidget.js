var classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget =
[
    [ "PresentationAudioWidget", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a4c9f7c820425e65dcfe40679baf044aa", null ],
    [ "~PresentationAudioWidget", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a7a9255a961420d06a8f605be969c498c", null ],
    [ "canHide", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#ac1f4f3da837e4ee98b8c2909eb747bdd", null ],
    [ "enqueue", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a6cb795c7cccf3a581212f87d5c72a1cf", null ],
    [ "isPaused", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a4221015db681459974ff44c8488b8e2f", null ],
    [ "keyPressEvent", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a5d624be565f746b6ffd4f75287dda9a6", null ],
    [ "setPaused", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a038797ef1e1938ef9243485c52135b73", null ],
    [ "signalPause", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#aa261c42bf391688562a098a097fd7ef5", null ],
    [ "signalPlay", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#a736ade4f63020f6e7792bc92e50d77a9", null ],
    [ "slotPlay", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#aaadbac40df0ab5731ceeba1c39d50934", null ],
    [ "slotStop", "classDigikamGenericPresentationPlugin_1_1PresentationAudioWidget.html#ab633e2f1cb9b4d291153e15749744c15", null ]
];