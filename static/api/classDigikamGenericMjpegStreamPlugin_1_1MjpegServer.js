var classDigikamGenericMjpegStreamPlugin_1_1MjpegServer =
[
    [ "Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private" ],
    [ "MjpegServer", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a372802c836c01652c9c6d14d51f83a3e", null ],
    [ "~MjpegServer", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a888afd74718d74899d1acc1fc3d14c99", null ],
    [ "blackList", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a047517b85f0f69af394ef77b0950d5b9", null ],
    [ "maxClients", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a7ee4d9a50fb64ea3f3595c69362bcefd", null ],
    [ "rate", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a4d43152f822d4637bc20b76c0cedd927", null ],
    [ "setBlackList", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#ae1f57c7da887709c52b0598812e07456", null ],
    [ "setMaxClients", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a1a895993e593d32ab8230107079b4fbe", null ],
    [ "setRate", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a20fdbe55d9302e02d2f2cc96132b6ef7", null ],
    [ "slotWriteFrame", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a51a4305d50c984c087a050c31fd0a2fb", null ],
    [ "start", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#ac755592d7b932c222717add95f379238", null ],
    [ "stop", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html#a5e025ea49a8d263e6b29cc4bf5010676", null ]
];