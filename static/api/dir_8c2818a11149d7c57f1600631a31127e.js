var dir_8c2818a11149d7c57f1600631a31127e =
[
    [ "taggingaction.cpp", "taggingaction_8cpp.html", null ],
    [ "taggingaction.h", "taggingaction_8h.html", [
      [ "TaggingAction", "classDigikam_1_1TaggingAction.html", "classDigikam_1_1TaggingAction" ]
    ] ],
    [ "taggingactionfactory.cpp", "taggingactionfactory_8cpp.html", null ],
    [ "taggingactionfactory.h", "taggingactionfactory_8h.html", [
      [ "TaggingActionFactory", "classDigikam_1_1TaggingActionFactory.html", "classDigikam_1_1TaggingActionFactory" ],
      [ "ConstraintInterface", "classDigikam_1_1TaggingActionFactory_1_1ConstraintInterface.html", "classDigikam_1_1TaggingActionFactory_1_1ConstraintInterface" ]
    ] ],
    [ "tagmodificationhelper.cpp", "tagmodificationhelper_8cpp.html", null ],
    [ "tagmodificationhelper.h", "tagmodificationhelper_8h.html", [
      [ "TagModificationHelper", "classDigikam_1_1TagModificationHelper.html", "classDigikam_1_1TagModificationHelper" ]
    ] ],
    [ "tagsactionmngr.cpp", "tagsactionmngr_8cpp.html", null ],
    [ "tagsactionmngr.h", "tagsactionmngr_8h.html", [
      [ "TagsActionMngr", "classDigikam_1_1TagsActionMngr.html", "classDigikam_1_1TagsActionMngr" ]
    ] ],
    [ "tagscompleter.cpp", "tagscompleter_8cpp.html", null ],
    [ "tagscompleter.h", "tagscompleter_8h.html", [
      [ "TagCompleter", "classDigikam_1_1TagCompleter.html", "classDigikam_1_1TagCompleter" ]
    ] ]
];