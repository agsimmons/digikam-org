var classheif_1_1Box__ipco =
[
    [ "Property", "structheif_1_1Box__ipco_1_1Property.html", "structheif_1_1Box__ipco_1_1Property" ],
    [ "Box_ipco", "classheif_1_1Box__ipco.html#ac2dd96ef72dbf5a599d6725245fe72cf", null ],
    [ "Box_ipco", "classheif_1_1Box__ipco.html#a727524f5c3f041a2fbe2173cf0ca16e9", null ],
    [ "append_child_box", "classheif_1_1Box__ipco.html#a777e921b2e983bafa7177e170af05368", null ],
    [ "derive_box_version", "classheif_1_1Box__ipco.html#a7c814668cf10733af001f7f2da6aa800", null ],
    [ "derive_box_version_recursive", "classheif_1_1Box__ipco.html#a9a24ecf54de3afb3b5c379d6b306c294", null ],
    [ "dump", "classheif_1_1Box__ipco.html#a11a0d65d84375c2538a92fae07369f55", null ],
    [ "dump_children", "classheif_1_1Box__ipco.html#a62bfb8c9a45448ebd331e93b4cc7abad", null ],
    [ "get_all_child_boxes", "classheif_1_1Box__ipco.html#a36bd665677bbc55989d2afefc016e364", null ],
    [ "get_box_size", "classheif_1_1Box__ipco.html#a7007aba2ce43a233beef58e09b7e181c", null ],
    [ "get_child_box", "classheif_1_1Box__ipco.html#a676046405af7d8d95cb4e1c175fb23b9", null ],
    [ "get_child_boxes", "classheif_1_1Box__ipco.html#aab070b2abc0332a322f51c9ebd386bad", null ],
    [ "get_flags", "classheif_1_1Box__ipco.html#a39d39e81b35c6e87a9928e7ddcbd3673", null ],
    [ "get_header_size", "classheif_1_1Box__ipco.html#a7abc183ab5226b5771ab9db93082508a", null ],
    [ "get_properties_for_item_ID", "classheif_1_1Box__ipco.html#a1e676df040b7e15eda1b08bbc45d8d72", null ],
    [ "get_property_for_item_ID", "classheif_1_1Box__ipco.html#af93b2b30576c4ef5c9cf073bbb91c324", null ],
    [ "get_short_type", "classheif_1_1Box__ipco.html#a63f22499fada881eef508cf96e9b51a7", null ],
    [ "get_type", "classheif_1_1Box__ipco.html#ab8a2b8029ba44809e9255b4a867c6d9c", null ],
    [ "get_type_string", "classheif_1_1Box__ipco.html#afe8f50ff1d59612204f5f0add1e1fd05", null ],
    [ "get_version", "classheif_1_1Box__ipco.html#ac75c1cfd9aabffe0c361d70c91c7b21c", null ],
    [ "is_full_box_header", "classheif_1_1Box__ipco.html#adecf2125f8dba3b2d1d369c9318c8122", null ],
    [ "parse", "classheif_1_1Box__ipco.html#ab0008c64524090c24fb816bd22b6f785", null ],
    [ "parse_full_box_header", "classheif_1_1Box__ipco.html#ad9050dc3e81b9a63c656753ff83b9a56", null ],
    [ "prepend_header", "classheif_1_1Box__ipco.html#a3fd5d77a13b528eeaba0118bbd828605", null ],
    [ "read_children", "classheif_1_1Box__ipco.html#ac9a3e82020676b575430383713af6800", null ],
    [ "reserve_box_header_space", "classheif_1_1Box__ipco.html#a053aeb7c2970e8a5344de7c93d6cbf71", null ],
    [ "set_flags", "classheif_1_1Box__ipco.html#ad0e2c3e8d6ef42c9a25cd452dd2511af", null ],
    [ "set_is_full_box", "classheif_1_1Box__ipco.html#a6140d66f43364c6f0a6bf53a16066fe2", null ],
    [ "set_short_type", "classheif_1_1Box__ipco.html#a181d3407828fba6fa48c40d4edeb2400", null ],
    [ "set_version", "classheif_1_1Box__ipco.html#ab1a9ccf0766e5db004e6bf7f70f142c0", null ],
    [ "write", "classheif_1_1Box__ipco.html#a0a5b312a39cfbaed4492ec27dcf44ea6", null ],
    [ "write_children", "classheif_1_1Box__ipco.html#a886ed0a0a458470382cb31c040850d49", null ],
    [ "m_children", "classheif_1_1Box__ipco.html#aa2cdb7880c4467e84abe03cc46a53066", null ]
];