var classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask =
[
    [ "TimeAdjustTask", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#a70cd09d1dea94248eb94f829da5868f3", null ],
    [ "~TimeAdjustTask", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#a23639902fd1742c9d725e0f946cfc82b", null ],
    [ "cancel", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "run", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#a71ce8a3ebf5eb219abbe41276b7a0b7e", null ],
    [ "setSettings", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#ae162c559764ebb9653ff9c30d0559f95", null ],
    [ "signalDateTimeForUrl", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#ab98e2fa367c3ddb2c06f25141de129cd", null ],
    [ "signalDone", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalProcessEnded", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#aa0002a91026244e97f84070cc3fa1fc7", null ],
    [ "signalProcessStarted", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#adaef8518120f65e17088509d74cd960a", null ],
    [ "signalProgress", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "m_cancel", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustTask.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];