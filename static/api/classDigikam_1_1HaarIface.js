var classDigikam_1_1HaarIface =
[
    [ "Private", "classDigikam_1_1HaarIface_1_1Private.html", "classDigikam_1_1HaarIface_1_1Private" ],
    [ "DuplicatesResultsMap", "classDigikam_1_1HaarIface.html#a66f5d746090ee5d490bd92848254f7cd", null ],
    [ "AlbumTagRelation", "classDigikam_1_1HaarIface.html#ab13e5c67ab452672b248e0085c8a4a8b", [
      [ "NoMix", "classDigikam_1_1HaarIface.html#ab13e5c67ab452672b248e0085c8a4a8baf3a6230e6782cc5557993d50c4688ced", null ],
      [ "Union", "classDigikam_1_1HaarIface.html#ab13e5c67ab452672b248e0085c8a4a8ba082fed1cca500019688ea9717f331b0b", null ],
      [ "Intersection", "classDigikam_1_1HaarIface.html#ab13e5c67ab452672b248e0085c8a4a8ba54082fa28a2c346e98b41391e7a644b1", null ],
      [ "AlbumExclusive", "classDigikam_1_1HaarIface.html#ab13e5c67ab452672b248e0085c8a4a8bac86bf9d74c56d32dcd1c1dd7bf659c74", null ],
      [ "TagExclusive", "classDigikam_1_1HaarIface.html#ab13e5c67ab452672b248e0085c8a4a8ba234fab56cdf8317e2848986adab0d058", null ]
    ] ],
    [ "DuplicatesSearchRestrictions", "classDigikam_1_1HaarIface.html#af9fbd0e2a641b46decc7c9810cec1d59", [
      [ "None", "classDigikam_1_1HaarIface.html#af9fbd0e2a641b46decc7c9810cec1d59ab39415f6c93140461be8b3e27b7226b0", null ],
      [ "SameAlbum", "classDigikam_1_1HaarIface.html#af9fbd0e2a641b46decc7c9810cec1d59a1d4b10cc7067719610f9364c63b25867", null ],
      [ "DifferentAlbum", "classDigikam_1_1HaarIface.html#af9fbd0e2a641b46decc7c9810cec1d59ac8ca00e9cff5944c83a9c0febbcbaacd", null ]
    ] ],
    [ "SketchType", "classDigikam_1_1HaarIface.html#afbc6f37c1b5a1440f83c9d6a88c3539e", [
      [ "ScannedSketch", "classDigikam_1_1HaarIface.html#afbc6f37c1b5a1440f83c9d6a88c3539ea62f5ba88c7cdbfa43947498ec605887a", null ],
      [ "HanddrawnSketch", "classDigikam_1_1HaarIface.html#afbc6f37c1b5a1440f83c9d6a88c3539ea2550b575a3518319baeaca1b26e9b9ef", null ]
    ] ],
    [ "HaarIface", "classDigikam_1_1HaarIface.html#ad7ed5207e9045fc35a4c8d28228c2888", null ],
    [ "HaarIface", "classDigikam_1_1HaarIface.html#a31500fb2cdf87ec4d1529fe407dc2f80", null ],
    [ "~HaarIface", "classDigikam_1_1HaarIface.html#a8110aedca5fff447c64f0b346921700e", null ],
    [ "bestMatchesForImageWithThreshold", "classDigikam_1_1HaarIface.html#a0e0748ed4b046eaf409a2e7b7f0b4c08", null ],
    [ "bestMatchesForImageWithThreshold", "classDigikam_1_1HaarIface.html#a5720e97ce6fc85443b87f152b6e299b3", null ],
    [ "bestMatchesForSignature", "classDigikam_1_1HaarIface.html#a3c371fcf781f6c398ef68f3e120f3b49", null ],
    [ "findDuplicates", "classDigikam_1_1HaarIface.html#ac47fbf9d7b6db5efdd5baee22d8188df", null ],
    [ "fulfillsRestrictions", "classDigikam_1_1HaarIface.html#ad9d1105d17ca6f9dc51eb70c72dd4b69", null ],
    [ "getBestAndWorstPossibleScore", "classDigikam_1_1HaarIface.html#a9780cff5963cbf94eca966c70cd5e0f8", null ],
    [ "indexImage", "classDigikam_1_1HaarIface.html#a2ce755181f3e418d523aabae4ec9a589", null ],
    [ "indexImage", "classDigikam_1_1HaarIface.html#a183b96b251de84bd22130b314783f142", null ],
    [ "indexImage", "classDigikam_1_1HaarIface.html#ac29b2391c7d3612068d872ecccabab24", null ],
    [ "indexImage", "classDigikam_1_1HaarIface.html#a070ab921f86338f93187993c53741edb", null ],
    [ "indexImage", "classDigikam_1_1HaarIface.html#ae0173e095b3904d2fbd42d06b128cba5", null ],
    [ "loadQImage", "classDigikam_1_1HaarIface.html#a9e1ca3406ba4a1fa4e6139422766cc34", null ],
    [ "retrieveSignatureFromDB", "classDigikam_1_1HaarIface.html#a4388d5affab8ac59f584b5d04c3cca69", null ],
    [ "setAlbumRootsToSearch", "classDigikam_1_1HaarIface.html#aa4b4972bcb5e5a8c569f7862c0b873d1", null ],
    [ "setAlbumRootsToSearch", "classDigikam_1_1HaarIface.html#ad19d159259ac69301ea9902b55767139", null ],
    [ "signatureAsText", "classDigikam_1_1HaarIface.html#a808bafedc100f8584d2811b9b85e811d", null ]
];