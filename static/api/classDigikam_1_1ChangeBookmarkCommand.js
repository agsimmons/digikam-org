var classDigikam_1_1ChangeBookmarkCommand =
[
    [ "BookmarkData", "classDigikam_1_1ChangeBookmarkCommand.html#a2e8607a693dae805e48b13ee5d70aad4", [
      [ "Url", "classDigikam_1_1ChangeBookmarkCommand.html#a2e8607a693dae805e48b13ee5d70aad4a5c8746d5850e882214c125f24cd3ff06", null ],
      [ "Title", "classDigikam_1_1ChangeBookmarkCommand.html#a2e8607a693dae805e48b13ee5d70aad4a26c56798fbfe453b1681d6b7722b2f27", null ],
      [ "Desc", "classDigikam_1_1ChangeBookmarkCommand.html#a2e8607a693dae805e48b13ee5d70aad4a0182f6479a9d83582a66dc68bca8f3fe", null ]
    ] ],
    [ "ChangeBookmarkCommand", "classDigikam_1_1ChangeBookmarkCommand.html#add0fdd3ecb7b436c7ec022903c6c8195", null ],
    [ "~ChangeBookmarkCommand", "classDigikam_1_1ChangeBookmarkCommand.html#a71e96308b4680ab11fb37c4c1c59fec5", null ],
    [ "redo", "classDigikam_1_1ChangeBookmarkCommand.html#a971e4f74ba34f948d86a3cc77a46f981", null ],
    [ "undo", "classDigikam_1_1ChangeBookmarkCommand.html#a1b20cab5ab5e7865c76da85a0185ebfc", null ]
];