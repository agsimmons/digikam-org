var classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask =
[
    [ "DNGConverterTask", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a975c6b1f2487605fe615e4e7a14bb790", null ],
    [ "~DNGConverterTask", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a1f0bd6194ae451e85f147398f4241fee", null ],
    [ "cancel", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "run", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a4dc942e52875d4d9d2cbe44c94b76817", null ],
    [ "setBackupOriginalRawFile", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a75ecf60487456d526c7ce98c867f2607", null ],
    [ "setCompressLossLess", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#acad32ba839c48da95cfa3ad46fa08194", null ],
    [ "setPreviewMode", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#aa8f60175b1ea86152b698870faf8d41f", null ],
    [ "setUpdateFileDate", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a23443ad2c119e42547cfa6b2feeeb07d", null ],
    [ "signalDone", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalFinished", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#aaa62df53041d43bce23313694e93af2a", null ],
    [ "signalProgress", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "signalStarting", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a2054091d8f47db73b0f108c12c0c944a", null ],
    [ "slotCancel", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a58c592097d4cc337c6e2035a9fae150b", null ],
    [ "m_cancel", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterTask.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];