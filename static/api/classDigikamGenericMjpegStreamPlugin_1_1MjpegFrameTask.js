var classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask =
[
    [ "MjpegFrameTask", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#af92a273262628ce691a9bbbd9a863956", null ],
    [ "~MjpegFrameTask", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#ac2311f2e61e5fe9facb32823561d84d7", null ],
    [ "cancel", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "signalDone", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalFrameChanged", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#a2c56c0670ab2b5fa1e69d60b1c770ec1", null ],
    [ "signalProgress", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "m_cancel", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];