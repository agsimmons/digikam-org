var classDigikam_1_1DImageHistory =
[
    [ "Entry", "classDigikam_1_1DImageHistory_1_1Entry.html", "classDigikam_1_1DImageHistory_1_1Entry" ],
    [ "DImageHistory", "classDigikam_1_1DImageHistory.html#a6c78ebafcb37b9ac57325a0fab0d44fd", null ],
    [ "DImageHistory", "classDigikam_1_1DImageHistory.html#a5e2684b5aafac530ae92c520add21d37", null ],
    [ "~DImageHistory", "classDigikam_1_1DImageHistory.html#a74842614f1005a96662693db0af33419", null ],
    [ "action", "classDigikam_1_1DImageHistory.html#a435f15ecba3019125c9d07b7098f413e", null ],
    [ "actionCount", "classDigikam_1_1DImageHistory.html#acaadfafd7a59a9050046b3f5c12e5890", null ],
    [ "adjustCurrentUuid", "classDigikam_1_1DImageHistory.html#a5606239e72ad849a98ddc9609ad821d3", null ],
    [ "adjustReferredImages", "classDigikam_1_1DImageHistory.html#acaf295524f06ea1bb73a20550237899e", null ],
    [ "allActions", "classDigikam_1_1DImageHistory.html#a4e21035242b55e4b9e3910a4e54582db", null ],
    [ "allReferredImages", "classDigikam_1_1DImageHistory.html#a8b61e4da862cfb81a278c307fb33361d", null ],
    [ "appendReferredImage", "classDigikam_1_1DImageHistory.html#a534e87b70d0aa4dd628729fbc25cd502", null ],
    [ "clearReferredImages", "classDigikam_1_1DImageHistory.html#af531ad1405aac53fafb62ba4294bae0b", null ],
    [ "currentReferredImage", "classDigikam_1_1DImageHistory.html#a4fe617cbdd64200c6d48dcf12fc8ebd7", null ],
    [ "entries", "classDigikam_1_1DImageHistory.html#ab3e52f73d97902e9b03e2c868f030295", null ],
    [ "entries", "classDigikam_1_1DImageHistory.html#a06ac246bc29bec8374febf4189d9e13e", null ],
    [ "hasActions", "classDigikam_1_1DImageHistory.html#a2004360ffb18e33fe54b6581059c601d", null ],
    [ "hasCurrentReferredImage", "classDigikam_1_1DImageHistory.html#aeb7ce584a9f7b24d6caac6819678ca2c", null ],
    [ "hasFilters", "classDigikam_1_1DImageHistory.html#a8c73c05e2cacc869b66e95bcef46060a", null ],
    [ "hasOriginalReferredImage", "classDigikam_1_1DImageHistory.html#a69757bf4953a12be22a5bbfde5e74fa8", null ],
    [ "hasReferredImageOfType", "classDigikam_1_1DImageHistory.html#a8293c5abe4fb2a6e8e03f8d2b84ebcdf", null ],
    [ "hasReferredImages", "classDigikam_1_1DImageHistory.html#ae70f5b1652e3ab57ace6c00069db1aa1", null ],
    [ "insertReferredImage", "classDigikam_1_1DImageHistory.html#ab49d137e0fe09e82975f2667a5fa2da8", null ],
    [ "isEmpty", "classDigikam_1_1DImageHistory.html#a9b68649b765f0f64bb4c049cc7e3e037", null ],
    [ "isNull", "classDigikam_1_1DImageHistory.html#a51283a46c0e86ee1a0adfe3ffe96f3f4", null ],
    [ "isValid", "classDigikam_1_1DImageHistory.html#ae8f5c29043138f92ce23e59f9ca00736", null ],
    [ "moveCurrentReferredImage", "classDigikam_1_1DImageHistory.html#a31a426736a47d9616eedd56587bffdc4", null ],
    [ "operator!=", "classDigikam_1_1DImageHistory.html#a58c0d56f3ce8acf2d160c6831f7da8b1", null ],
    [ "operator<", "classDigikam_1_1DImageHistory.html#a7a148dded03c83a813880564da42baf8", null ],
    [ "operator<<", "classDigikam_1_1DImageHistory.html#acf9f081bd6d573b21efb520fdb564d67", null ],
    [ "operator<<", "classDigikam_1_1DImageHistory.html#ad6dfcfb8a40ace928b56df0a2b6e5ec5", null ],
    [ "operator=", "classDigikam_1_1DImageHistory.html#afc06a7d2827dd4d833c87ee178d1633c", null ],
    [ "operator==", "classDigikam_1_1DImageHistory.html#a4f49fae569f8a3ccf6161d9bf3ee6e56", null ],
    [ "operator>", "classDigikam_1_1DImageHistory.html#a9ecd79aa2580ed79b2dfc20ee4199eb9", null ],
    [ "operator[]", "classDigikam_1_1DImageHistory.html#aa070cfd5d6c92c169c541f434d458630", null ],
    [ "operator[]", "classDigikam_1_1DImageHistory.html#a6def1281c8725487e75464f9116c2b88", null ],
    [ "originalReferredImage", "classDigikam_1_1DImageHistory.html#aea97b51384f09062fd889bbe71675d42", null ],
    [ "purgePathFromReferredImages", "classDigikam_1_1DImageHistory.html#a0f17da7c9f80d6a4f8d56b82663c8214", null ],
    [ "referredImages", "classDigikam_1_1DImageHistory.html#aed1deb000be313bc1762fd42cf4c6166", null ],
    [ "referredImages", "classDigikam_1_1DImageHistory.html#a0fe18af4ca3e11de15a1393f941be184", null ],
    [ "referredImagesOfType", "classDigikam_1_1DImageHistory.html#a9d1f31c6dbc12bb73a4f99e2b11d5139", null ],
    [ "removeLast", "classDigikam_1_1DImageHistory.html#a06c9e7cf6f8763f1c4279d29a2861601", null ],
    [ "size", "classDigikam_1_1DImageHistory.html#abdeac7d66bc5b92fb2b6254a940df0b3", null ],
    [ "toXml", "classDigikam_1_1DImageHistory.html#a88ced7d3f5521469755a361243791c2a", null ]
];