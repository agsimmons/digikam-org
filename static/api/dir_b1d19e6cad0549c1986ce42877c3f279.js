var dir_b1d19e6cad0549c1986ce42877c3f279 =
[
    [ "antivignettingfilter.cpp", "antivignettingfilter_8cpp.html", null ],
    [ "antivignettingfilter.h", "antivignettingfilter_8h.html", [
      [ "AntiVignettingContainer", "classDigikam_1_1AntiVignettingContainer.html", "classDigikam_1_1AntiVignettingContainer" ],
      [ "AntiVignettingFilter", "classDigikam_1_1AntiVignettingFilter.html", "classDigikam_1_1AntiVignettingFilter" ]
    ] ],
    [ "antivignettingsettings.cpp", "antivignettingsettings_8cpp.html", null ],
    [ "antivignettingsettings.h", "antivignettingsettings_8h.html", [
      [ "AntiVignettingSettings", "classDigikam_1_1AntiVignettingSettings.html", "classDigikam_1_1AntiVignettingSettings" ]
    ] ],
    [ "lensdistortionfilter.cpp", "lensdistortionfilter_8cpp.html", null ],
    [ "lensdistortionfilter.h", "lensdistortionfilter_8h.html", [
      [ "LensDistortionFilter", "classDigikam_1_1LensDistortionFilter.html", "classDigikam_1_1LensDistortionFilter" ]
    ] ],
    [ "lensdistortionpixelaccess.cpp", "lensdistortionpixelaccess_8cpp.html", null ],
    [ "lensdistortionpixelaccess.h", "lensdistortionpixelaccess_8h.html", "lensdistortionpixelaccess_8h" ],
    [ "lensfuncameraselector.cpp", "lensfuncameraselector_8cpp.html", null ],
    [ "lensfuncameraselector.h", "lensfuncameraselector_8h.html", [
      [ "LensFunCameraSelector", "classDigikam_1_1LensFunCameraSelector.html", "classDigikam_1_1LensFunCameraSelector" ]
    ] ],
    [ "lensfunfilter.cpp", "lensfunfilter_8cpp.html", null ],
    [ "lensfunfilter.h", "lensfunfilter_8h.html", [
      [ "LensFunContainer", "classDigikam_1_1LensFunContainer.html", "classDigikam_1_1LensFunContainer" ],
      [ "LensFunFilter", "classDigikam_1_1LensFunFilter.html", "classDigikam_1_1LensFunFilter" ]
    ] ],
    [ "lensfuniface.cpp", "lensfuniface_8cpp.html", null ],
    [ "lensfuniface.h", "lensfuniface_8h.html", [
      [ "LensFunIface", "classDigikam_1_1LensFunIface.html", "classDigikam_1_1LensFunIface" ]
    ] ],
    [ "lensfunsettings.cpp", "lensfunsettings_8cpp.html", null ],
    [ "lensfunsettings.h", "lensfunsettings_8h.html", [
      [ "LensFunSettings", "classDigikam_1_1LensFunSettings.html", "classDigikam_1_1LensFunSettings" ]
    ] ]
];