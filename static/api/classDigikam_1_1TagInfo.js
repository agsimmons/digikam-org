var classDigikam_1_1TagInfo =
[
    [ "List", "classDigikam_1_1TagInfo.html#adf558179d57572ff22ad93f16f200b9f", null ],
    [ "TagInfo", "classDigikam_1_1TagInfo.html#a2a5199f788e637c24519942cac7f0f05", null ],
    [ "isNull", "classDigikam_1_1TagInfo.html#ade22bd076f32f1b6e6a5cf718dd91e63", null ],
    [ "operator<", "classDigikam_1_1TagInfo.html#a11df54fdd567a2ffc169f0b53e05206d", null ],
    [ "icon", "classDigikam_1_1TagInfo.html#ae62c5de49a86e7d5e72530cdd2e47261", null ],
    [ "iconId", "classDigikam_1_1TagInfo.html#aab28bfa74a9abcb2e57e766c172830fa", null ],
    [ "id", "classDigikam_1_1TagInfo.html#a8d2f25b86580bb42e9619f9f252611ba", null ],
    [ "name", "classDigikam_1_1TagInfo.html#aa4bfef0a8a0380491365ca309c44504a", null ],
    [ "pid", "classDigikam_1_1TagInfo.html#a17958297bd642fafcc257960cd0a3a4a", null ]
];