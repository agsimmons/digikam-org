var classDigikam_1_1EditorCore_1_1Private_1_1FileToSave =
[
    [ "fileName", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a70f3dbb9967b4b5c3730dca8a12bb08d", null ],
    [ "filePath", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a25bbbdb25c6c229e69dab22c8c742061", null ],
    [ "historyStep", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a4c5a8fbccdbb1a12eec47f82f5554010", null ],
    [ "image", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#ae0f2f4725f04b5cff0b9ca1b4ca4268a", null ],
    [ "intendedFilePath", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a4f08cfe408491770daa76f9bef1fb416", null ],
    [ "ioAttributes", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a87aa9b3c9bacb15d694f1897a89f3fac", null ],
    [ "mimeType", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a2d01c48cee10e0dc883f7abb550218bb", null ],
    [ "setExifOrientationTag", "classDigikam_1_1EditorCore_1_1Private_1_1FileToSave.html#a8577fcb53197affa5c8187636e3a02eb", null ]
];