var dir_49f59ef4b301d55ed5bfdd776d7c9433 =
[
    [ "kbeffect.cpp", "kbeffect_8cpp.html", null ],
    [ "kbeffect.h", "kbeffect_8h.html", [
      [ "BlendKBEffect", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect" ],
      [ "FadeKBEffect", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect" ],
      [ "KBEffect", "classDigikamGenericPresentationPlugin_1_1KBEffect.html", "classDigikamGenericPresentationPlugin_1_1KBEffect" ]
    ] ],
    [ "kbimageloader.cpp", "kbimageloader_8cpp.html", null ],
    [ "kbimageloader.h", "kbimageloader_8h.html", [
      [ "KBImageLoader", "classDigikamGenericPresentationPlugin_1_1KBImageLoader.html", "classDigikamGenericPresentationPlugin_1_1KBImageLoader" ]
    ] ],
    [ "presentationgl.cpp", "presentationgl_8cpp.html", null ],
    [ "presentationgl.h", "presentationgl_8h.html", [
      [ "PresentationGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html", "classDigikamGenericPresentationPlugin_1_1PresentationGL" ]
    ] ],
    [ "presentationkb.cpp", "presentationkb_8cpp.html", null ],
    [ "presentationkb.h", "presentationkb_8h.html", [
      [ "KBImage", "classDigikamGenericPresentationPlugin_1_1KBImage.html", "classDigikamGenericPresentationPlugin_1_1KBImage" ],
      [ "KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html", "classDigikamGenericPresentationPlugin_1_1KBViewTrans" ],
      [ "PresentationKB", "classDigikamGenericPresentationPlugin_1_1PresentationKB.html", "classDigikamGenericPresentationPlugin_1_1PresentationKB" ]
    ] ],
    [ "presentationkb_p.h", "presentationkb__p_8h.html", [
      [ "Private", "classDigikamGenericPresentationPlugin_1_1PresentationKB_1_1Private.html", "classDigikamGenericPresentationPlugin_1_1PresentationKB_1_1Private" ]
    ] ]
];