var classDigikam_1_1UndoActionReversible =
[
    [ "UndoActionReversible", "classDigikam_1_1UndoActionReversible.html#af70b3da47074cb8819b0c644a34e7ef9", null ],
    [ "fileOriginData", "classDigikam_1_1UndoActionReversible.html#a832a7070cde0b021738d3d59b497324b", null ],
    [ "fileOriginResolvedHistory", "classDigikam_1_1UndoActionReversible.html#af3932452421bbf2532063d836e1a412f", null ],
    [ "getFilter", "classDigikam_1_1UndoActionReversible.html#a491c8a7eab4f79323e059092d5f3d8d4", null ],
    [ "getMetadata", "classDigikam_1_1UndoActionReversible.html#adbfd2809dfe36bd4c09246f02f9ad0e9", null ],
    [ "getReverseFilter", "classDigikam_1_1UndoActionReversible.html#ab3be02711c628a0fa1b78e6e918450f7", null ],
    [ "getTitle", "classDigikam_1_1UndoActionReversible.html#a68fcd30401a224212d7d6cbd97be72f0", null ],
    [ "hasFileOriginData", "classDigikam_1_1UndoActionReversible.html#ae7699bd585c82d1beb91d85a34b42e17", null ],
    [ "setFileOriginData", "classDigikam_1_1UndoActionReversible.html#ab6dac017863266442c9d910f18ad5bab", null ],
    [ "setMetadata", "classDigikam_1_1UndoActionReversible.html#a2d9079762b333054ab52818a1b66fca1", null ],
    [ "setTitle", "classDigikam_1_1UndoActionReversible.html#a099f5350e231a4196a2198d22833d565", null ],
    [ "m_filter", "classDigikam_1_1UndoActionReversible.html#ab6258466eb6ec01643766505a67cf345", null ]
];