var namespaceDigikamGenericPiwigoPlugin =
[
    [ "PiwigoAlbum", "classDigikamGenericPiwigoPlugin_1_1PiwigoAlbum.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoAlbum" ],
    [ "PiwigoLoginDlg", "classDigikamGenericPiwigoPlugin_1_1PiwigoLoginDlg.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoLoginDlg" ],
    [ "PiwigoPlugin", "classDigikamGenericPiwigoPlugin_1_1PiwigoPlugin.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoPlugin" ],
    [ "PiwigoSession", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoSession" ],
    [ "PiwigoTalker", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoTalker" ],
    [ "PiwigoWindow", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow.html", "classDigikamGenericPiwigoPlugin_1_1PiwigoWindow" ]
];