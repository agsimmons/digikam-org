var classDigikam_1_1SetupMetadata =
[
    [ "Private", "classDigikam_1_1SetupMetadata_1_1Private.html", "classDigikam_1_1SetupMetadata_1_1Private" ],
    [ "MetadataTab", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087d", [
      [ "Behavior", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087dadd2d62867d69e2d47b66f9fede492a42", null ],
      [ "Sidecars", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087da08b8e89d5251061dd6bf617b8b15163b", null ],
      [ "Rotation", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087daf57a149c7f366ef62e53188d491b256b", null ],
      [ "Display", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087da1999206b8db1705a6ac519ba053443ca", null ],
      [ "ExifTool", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087da6cf6072b5e4733eecf05617099156b0a", null ],
      [ "Baloo", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087da6612b0179bd13c3f7fd2ab253319f32f", null ],
      [ "AdvancedConfig", "classDigikam_1_1SetupMetadata.html#a7a8ce5e725b45bcf1ec334363e5b087da01d2aaefa98aa2074c6ee81e7fd27906", null ]
    ] ],
    [ "SetupMetadata", "classDigikam_1_1SetupMetadata.html#ab51e2c1312aca89489466e781dd3292a", null ],
    [ "~SetupMetadata", "classDigikam_1_1SetupMetadata.html#aaceaeb8a392dd3193d4ed1b165f05f88", null ],
    [ "applySettings", "classDigikam_1_1SetupMetadata.html#a90a9556151267b593e93a46f7bee28ae", null ],
    [ "exifAutoRotateHasChanged", "classDigikam_1_1SetupMetadata.html#ad7a6b8356e4e522c8ecaff327636e35a", null ],
    [ "setActiveMainTab", "classDigikam_1_1SetupMetadata.html#a97d01af50b36f0f245ab145c170be9aa", null ],
    [ "setActiveSubTab", "classDigikam_1_1SetupMetadata.html#a18e8236b48d293a312445c0c2db8baf7", null ]
];