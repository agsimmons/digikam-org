var classDigikam_1_1WorkflowManager =
[
    [ "clear", "classDigikam_1_1WorkflowManager.html#a9ee8114b490e560de6736c9821589e10", null ],
    [ "findByTitle", "classDigikam_1_1WorkflowManager.html#a4277ba1978269615ad2c12369972fc40", null ],
    [ "insert", "classDigikam_1_1WorkflowManager.html#ab8311dcc2d6c04acddeaa1beb964ecce", null ],
    [ "load", "classDigikam_1_1WorkflowManager.html#acb0cf7a9338d7b6e841d4d31a70b6cd2", null ],
    [ "queueSettingsList", "classDigikam_1_1WorkflowManager.html#aafd417f02ed310137070e46d306f8029", null ],
    [ "remove", "classDigikam_1_1WorkflowManager.html#a5ccb0ccb4007c0de4f61a5ce529aa69f", null ],
    [ "save", "classDigikam_1_1WorkflowManager.html#a8dff3fbe5147223e1929e0f4f602ac71", null ],
    [ "signalQueueSettingsAdded", "classDigikam_1_1WorkflowManager.html#a25fc636afc5d4888c77c97d09153bfbd", null ],
    [ "signalQueueSettingsRemoved", "classDigikam_1_1WorkflowManager.html#a0c49622137c2c2a12e24854fc36f3bec", null ],
    [ "WorkflowManagerCreator", "classDigikam_1_1WorkflowManager.html#a6a77e05b9ccee6e470452f09a3b40f03", null ]
];