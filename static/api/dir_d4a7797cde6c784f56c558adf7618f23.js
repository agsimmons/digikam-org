var dir_d4a7797cde6c784f56c558adf7618f23 =
[
    [ "bordercontainer.cpp", "bordercontainer_8cpp.html", null ],
    [ "bordercontainer.h", "bordercontainer_8h.html", [
      [ "BorderContainer", "classDigikam_1_1BorderContainer.html", "classDigikam_1_1BorderContainer" ]
    ] ],
    [ "borderfilter.cpp", "borderfilter_8cpp.html", null ],
    [ "borderfilter.h", "borderfilter_8h.html", [
      [ "BorderFilter", "classDigikam_1_1BorderFilter.html", "classDigikam_1_1BorderFilter" ]
    ] ],
    [ "bordersettings.cpp", "bordersettings_8cpp.html", null ],
    [ "bordersettings.h", "bordersettings_8h.html", [
      [ "BorderSettings", "classDigikam_1_1BorderSettings.html", "classDigikam_1_1BorderSettings" ]
    ] ],
    [ "texturecontainer.cpp", "texturecontainer_8cpp.html", null ],
    [ "texturecontainer.h", "texturecontainer_8h.html", [
      [ "TextureContainer", "classDigikam_1_1TextureContainer.html", "classDigikam_1_1TextureContainer" ]
    ] ],
    [ "texturefilter.cpp", "texturefilter_8cpp.html", null ],
    [ "texturefilter.h", "texturefilter_8h.html", [
      [ "TextureFilter", "classDigikam_1_1TextureFilter.html", "classDigikam_1_1TextureFilter" ]
    ] ],
    [ "texturesettings.cpp", "texturesettings_8cpp.html", null ],
    [ "texturesettings.h", "texturesettings_8h.html", [
      [ "TextureSettings", "classDigikam_1_1TextureSettings.html", "classDigikam_1_1TextureSettings" ]
    ] ]
];