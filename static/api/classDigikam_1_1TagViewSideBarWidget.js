var classDigikam_1_1TagViewSideBarWidget =
[
    [ "StateSavingDepth", "classDigikam_1_1TagViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1TagViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1TagViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1TagViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "TagViewSideBarWidget", "classDigikam_1_1TagViewSideBarWidget.html#a1df2404958821d0deed21a52191a4521", null ],
    [ "~TagViewSideBarWidget", "classDigikam_1_1TagViewSideBarWidget.html#ade3a090bd1511ef04c93cd7667dbb21f", null ],
    [ "applySettings", "classDigikam_1_1TagViewSideBarWidget.html#a0b84dfa5270bf2bd6eccf8b56ccecc7b", null ],
    [ "changeAlbumFromHistory", "classDigikam_1_1TagViewSideBarWidget.html#a5229db6bb37f789e3633e98e74fc28fc", null ],
    [ "currentAlbum", "classDigikam_1_1TagViewSideBarWidget.html#a86f8241c5db42f04aaf2443bd767ffa7", null ],
    [ "doLoadState", "classDigikam_1_1TagViewSideBarWidget.html#a9bb32658941e0e68cf4683c4f63e2aa2", null ],
    [ "doSaveState", "classDigikam_1_1TagViewSideBarWidget.html#a8e898a0489edde2c94ce0fdec3e3c5ce", null ],
    [ "entryName", "classDigikam_1_1TagViewSideBarWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classDigikam_1_1TagViewSideBarWidget.html#a8a5721b7ad6d699bae5b0e5b1405d9da", null ],
    [ "getConfigGroup", "classDigikam_1_1TagViewSideBarWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classDigikam_1_1TagViewSideBarWidget.html#af8ec2474494c226b2c4e291f712e08ad", null ],
    [ "getStateSavingDepth", "classDigikam_1_1TagViewSideBarWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1TagViewSideBarWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "requestActiveTab", "classDigikam_1_1TagViewSideBarWidget.html#a247452550423658b7ca3f5ba97fb1f07", null ],
    [ "saveState", "classDigikam_1_1TagViewSideBarWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1TagViewSideBarWidget.html#a143a3d9d3108a4a918aca77c238abf89", null ],
    [ "setConfigGroup", "classDigikam_1_1TagViewSideBarWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setCurrentAlbum", "classDigikam_1_1TagViewSideBarWidget.html#abc8cc268121d889dc7f88c8a488a5dbd", null ],
    [ "setEntryPrefix", "classDigikam_1_1TagViewSideBarWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1TagViewSideBarWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalFindDuplicates", "classDigikam_1_1TagViewSideBarWidget.html#a875eb9265859018ef63b8be279313568", null ],
    [ "signalNotificationError", "classDigikam_1_1TagViewSideBarWidget.html#a12e1b990cb7a7f652ce6f10df1ccfc11", null ],
    [ "slotOpenTagManager", "classDigikam_1_1TagViewSideBarWidget.html#a2b7918d3e489516c36fc5bb89d1b4ddc", null ],
    [ "slotToggleTagsSelection", "classDigikam_1_1TagViewSideBarWidget.html#a81e73d9272cbd67776be357193aaa943", null ]
];