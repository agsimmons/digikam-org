var classDigikam_1_1GPSUndoCommand =
[
    [ "UndoInfo", "classDigikam_1_1GPSUndoCommand_1_1UndoInfo.html", "classDigikam_1_1GPSUndoCommand_1_1UndoInfo" ],
    [ "GPSUndoCommand", "classDigikam_1_1GPSUndoCommand.html#aff08f13482cc3f2e82b0a573d581b81d", null ],
    [ "addUndoInfo", "classDigikam_1_1GPSUndoCommand.html#a99f1f024f7b8fddd0962437f48ea2686", null ],
    [ "affectedItemCount", "classDigikam_1_1GPSUndoCommand.html#ace5ec5ab3feb9e4ac91ee9803399193f", null ],
    [ "changeItemData", "classDigikam_1_1GPSUndoCommand.html#af161beefc474593fe3c0004207af9de5", null ],
    [ "redo", "classDigikam_1_1GPSUndoCommand.html#ab72b93b06dfcced1280b298467bf681d", null ],
    [ "undo", "classDigikam_1_1GPSUndoCommand.html#a766ed78e8360bc08b22ffd3517ec80e1", null ]
];