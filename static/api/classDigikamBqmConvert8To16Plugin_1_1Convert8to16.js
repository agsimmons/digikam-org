var classDigikamBqmConvert8To16Plugin_1_1Convert8to16 =
[
    [ "BatchToolGroup", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72", [
      [ "BaseTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72abf7d05254a90fb96b64257b37ab2571c", null ],
      [ "CustomTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72a3e0af80bcff0ed3b2a81c1994ebf2d50", null ],
      [ "ColorTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72a678db3327b06483d6eec8601a6b65457", null ],
      [ "EnhanceTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72ac99e79b29944cded7f1466dad3f31c22", null ],
      [ "TransformTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72a5abc81bbd353db5e71868a59ec402d3f", null ],
      [ "DecorateTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72ac275940dc7d00089f0a46924d40413ac", null ],
      [ "FiltersTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72a3f77943d1787b72f8a1c3d5a9a04d4db", null ],
      [ "ConvertTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72a87f1c29bf0d78ea00d5ea67a99dc063d", null ],
      [ "MetadataTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afa76b46ac346747b289ce17be3124a72abb96e2d5f48eeda7bb30755e25cd6756", null ]
    ] ],
    [ "Convert8to16", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a7b8e45f9c3b5702963d67d948010dd1b", null ],
    [ "~Convert8to16", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ae8ffe5d4783e282cca8b2de7e2cb6610", null ],
    [ "apply", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a36d8541d1b9a820534e87902b54d088c", null ],
    [ "applyFilter", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a90a5d88988617961068fb2242dbf88d7", null ],
    [ "applyFilter", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a561e9e51ca9cadf1fecdbf147b5e0588", null ],
    [ "applyFilterChangedProperties", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ac596446d1ca3d457b7a8a6c9f360d763", null ],
    [ "cancel", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a41b4ff36f19166534ce42462d39ceff3", null ],
    [ "clone", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a527843b8d897f19e4b70e07d97bcb6f1", null ],
    [ "defaultSettings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#abbc5859b74be9027535b04250ee72464", null ],
    [ "deleteSettingsWidget", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#abd1e5b945bfc28740f7cd79534d3dabf", null ],
    [ "errorDescription", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a9a6bd8dccc7132093bebf21c0368d8ef", null ],
    [ "getBranchHistory", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aa423a1039987bba7bd0d9369eb233957", null ],
    [ "getNeedResetExifOrientation", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a8d1951cc07e2e31fd6763a8f2abfd4c7", null ],
    [ "getResetExifOrientationAllowed", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a8fba6bed21021f2e8b9d34c5dbe54605", null ],
    [ "image", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a07a6d7854d84caae798143ba79fd777f", null ],
    [ "imageData", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a23eb9f26cab4ebe516e1047b99ec02e9", null ],
    [ "imageInfo", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a74fcc66bd46387a69238cd0b49376d32", null ],
    [ "inputUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aaed484c6d693e73ce09d405803355d04", null ],
    [ "ioFileSettings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a29b94a1be7c1548b30032809a9e0e91a", null ],
    [ "isCancelled", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aec1749bf7cedd5261dcb7caf710b91b3", null ],
    [ "isLastChainedTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#af9c31ea2af0d09924370a483161b1dde", null ],
    [ "isRawFile", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#afd910fab457aa527e72634f2de834c48", null ],
    [ "loadToDImg", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a070c2544bad70f9d7e1f85688de9c27f", null ],
    [ "outputSuffix", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aeee8ce2604a87f131d75e9bea8a595aa", null ],
    [ "outputUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a4703fb10c14dd3b84edba248fed832bd", null ],
    [ "plugin", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a4c4ac6dcb7580e4e23edaf66a713c888", null ],
    [ "rawDecodingSettings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a285e355bed564ca3d4651a6c264bb5d0", null ],
    [ "registerSettingsWidget", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a8f2e313efd27dc6300f64bfe6c92cdc5", null ],
    [ "savefromDImg", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a71045c04fa4bb019c021c9dacd32348d", null ],
    [ "setBranchHistory", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a5003b8ab574560903b0dcffa408d139a", null ],
    [ "setDRawDecoderSettings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a6a3fc7a2cd0c1890543b5a0f3b8d5f59", null ],
    [ "setErrorDescription", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a73db6ff94e50240e68594c8635413659", null ],
    [ "setImageData", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a8547dbb306a9811d0e7242dec09ac68b", null ],
    [ "setInputUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a57e6cc4aa683fa7a83e2ffe3d4651a1b", null ],
    [ "setIOFileSettings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a282091b29cea96aeae8bdb20f6aa73d0", null ],
    [ "setItemInfo", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a9c057fdf245570ab92de8133ce134d70", null ],
    [ "setLastChainedTool", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ae76700064b9ffacb124a8163dc693546", null ],
    [ "setNeedResetExifOrientation", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aa34b534ee277b0d6af52d0821423b5ee", null ],
    [ "setOutputUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a9774274355899f382590dc68d782497a", null ],
    [ "setOutputUrlFromInputUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aacfa495346a2334b263b8c6a425d740a", null ],
    [ "setPlugin", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a3da22fb32151df2f75e1073bb42626db", null ],
    [ "setRawLoadingRules", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ac3f13be06dcb2e9be6af7486154e1ab0", null ],
    [ "setResetExifOrientationAllowed", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#abfbf72c25a9b65b9fb4b572d46fca3ef", null ],
    [ "setSaveAsNewVersion", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a6d9b7644e60bda3e0f033aa3e9c4f8f5", null ],
    [ "setSettings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a0a6c7c5630bd3a81fe65a351114f2b84", null ],
    [ "settings", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a101d327668f9d9f7cc9695b1b9bf0d20", null ],
    [ "settingsWidget", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#acc1ce64746976144f211e2d537d3b851", null ],
    [ "setToolDescription", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a1c9ffb404b2597cdfc08515b3e9f86e7", null ],
    [ "setToolIcon", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a78786c22c95ece3d7ad4abd8b1e7b6d3", null ],
    [ "setToolIconName", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#af339b08111d419eea4a8515c45f3accc", null ],
    [ "setToolTitle", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a4337c17e1bc828dc77eb8dba74099db0", null ],
    [ "setWorkingUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#aae015f01fb686cfec93c764ca2c06013", null ],
    [ "signalAssignSettings2Widget", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#af44500f1c3827c0e643f6494ef49b660", null ],
    [ "signalSettingsChanged", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a77b20102fb03bdb2e388c306df43b044", null ],
    [ "signalVisible", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#acaa2b52161bee3aa9d24db22ff2d72b1", null ],
    [ "slotResetSettingsToDefault", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ab6fc0970f527dadbddc041ef1d9a1d1c", null ],
    [ "slotSettingsChanged", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a3a0cda57c6cb05452e3a1af200c098ea", null ],
    [ "toolDescription", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a2f87e45801f6e53bdbdec85c52240a60", null ],
    [ "toolGroup", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a28d9efb0e9c27c7f53ecf9c0b13a2d26", null ],
    [ "toolGroupToString", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a7a2ffb5cdde6950f6505525546639cba", null ],
    [ "toolIcon", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a847551f8c091a7b598e5782348bafbd8", null ],
    [ "toolTitle", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#add18d9118dc64a85e1b2c7f86c871f86", null ],
    [ "toolVersion", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ac65a61bd8560a8e2a39ea9b9f75eb669", null ],
    [ "workingUrl", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#ac84787fb1a697cdec8c21758896f49a3", null ],
    [ "m_settingsWidget", "classDigikamBqmConvert8To16Plugin_1_1Convert8to16.html#a50a1ed7dcd42d42698fe50fa16eb4868", null ]
];