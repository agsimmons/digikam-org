var dir_523f419f894fa5789d25b58294266e78 =
[
    [ "trackmanager.cpp", "trackmanager_8cpp.html", null ],
    [ "trackmanager.h", "trackmanager_8h.html", [
      [ "TrackManager", "classDigikam_1_1TrackManager.html", "classDigikam_1_1TrackManager" ],
      [ "Track", "classDigikam_1_1TrackManager_1_1Track.html", "classDigikam_1_1TrackManager_1_1Track" ],
      [ "TrackPoint", "classDigikam_1_1TrackManager_1_1TrackPoint.html", "classDigikam_1_1TrackManager_1_1TrackPoint" ]
    ] ],
    [ "trackreader.cpp", "trackreader_8cpp.html", null ],
    [ "trackreader.h", "trackreader_8h.html", [
      [ "TrackReader", "classDigikam_1_1TrackReader.html", "classDigikam_1_1TrackReader" ],
      [ "TrackReadResult", "classDigikam_1_1TrackReader_1_1TrackReadResult.html", "classDigikam_1_1TrackReader_1_1TrackReadResult" ]
    ] ]
];