var dir_6d8f777a105466e186cac4ea0ccac6fb =
[
    [ "itemcomments.cpp", "itemcomments_8cpp.html", null ],
    [ "itemcomments.h", "itemcomments_8h.html", [
      [ "ItemComments", "classDigikam_1_1ItemComments.html", "classDigikam_1_1ItemComments" ]
    ] ],
    [ "itemcopyright.cpp", "itemcopyright_8cpp.html", null ],
    [ "itemcopyright.h", "itemcopyright_8h.html", [
      [ "ItemCopyright", "classDigikam_1_1ItemCopyright.html", "classDigikam_1_1ItemCopyright" ]
    ] ],
    [ "itemextendedproperties.cpp", "itemextendedproperties_8cpp.html", null ],
    [ "itemextendedproperties.h", "itemextendedproperties_8h.html", [
      [ "ItemExtendedProperties", "classDigikam_1_1ItemExtendedProperties.html", "classDigikam_1_1ItemExtendedProperties" ]
    ] ],
    [ "itemgps.cpp", "itemgps_8cpp.html", null ],
    [ "itemgps.h", "itemgps_8h.html", [
      [ "ItemGPS", "classDigikam_1_1ItemGPS.html", "classDigikam_1_1ItemGPS" ]
    ] ],
    [ "iteminfo.cpp", "iteminfo_8cpp.html", "iteminfo_8cpp" ],
    [ "iteminfo.h", "iteminfo_8h.html", "iteminfo_8h" ],
    [ "iteminfo_containers.cpp", "iteminfo__containers_8cpp.html", null ],
    [ "iteminfo_geolocation.cpp", "iteminfo__geolocation_8cpp.html", null ],
    [ "iteminfo_groups.cpp", "iteminfo__groups_8cpp.html", null ],
    [ "iteminfo_history.cpp", "iteminfo__history_8cpp.html", null ],
    [ "iteminfo_labels.cpp", "iteminfo__labels_8cpp.html", null ],
    [ "iteminfo_p.cpp", "iteminfo__p_8cpp.html", "iteminfo__p_8cpp" ],
    [ "iteminfo_p.h", "iteminfo__p_8h.html", "iteminfo__p_8h" ],
    [ "iteminfo_properties.cpp", "iteminfo__properties_8cpp.html", null ],
    [ "iteminfo_similarity.cpp", "iteminfo__similarity_8cpp.html", null ],
    [ "iteminfo_tags.cpp", "iteminfo__tags_8cpp.html", null ],
    [ "iteminfo_thumbnail.cpp", "iteminfo__thumbnail_8cpp.html", null ],
    [ "iteminfocache.cpp", "iteminfocache_8cpp.html", null ],
    [ "iteminfocache.h", "iteminfocache_8h.html", [
      [ "ItemInfoCache", "classDigikam_1_1ItemInfoCache.html", "classDigikam_1_1ItemInfoCache" ]
    ] ],
    [ "iteminfodata.cpp", "iteminfodata_8cpp.html", null ],
    [ "iteminfodata.h", "iteminfodata_8h.html", [
      [ "ItemInfoData", "classDigikam_1_1ItemInfoData.html", "classDigikam_1_1ItemInfoData" ],
      [ "ItemInfoReadLocker", "classDigikam_1_1ItemInfoReadLocker.html", "classDigikam_1_1ItemInfoReadLocker" ],
      [ "ItemInfoStatic", "classDigikam_1_1ItemInfoStatic.html", "classDigikam_1_1ItemInfoStatic" ],
      [ "ItemInfoWriteLocker", "classDigikam_1_1ItemInfoWriteLocker.html", "classDigikam_1_1ItemInfoWriteLocker" ]
    ] ],
    [ "iteminfolist.cpp", "iteminfolist_8cpp.html", null ],
    [ "iteminfolist.h", "iteminfolist_8h.html", "iteminfolist_8h" ],
    [ "itemposition.cpp", "itemposition_8cpp.html", null ],
    [ "itemposition.h", "itemposition_8h.html", [
      [ "ItemPosition", "classDigikam_1_1ItemPosition.html", "classDigikam_1_1ItemPosition" ]
    ] ],
    [ "itemtagpair.cpp", "itemtagpair_8cpp.html", "itemtagpair_8cpp" ],
    [ "itemtagpair.h", "itemtagpair_8h.html", [
      [ "ItemTagPair", "classDigikam_1_1ItemTagPair.html", "classDigikam_1_1ItemTagPair" ]
    ] ]
];