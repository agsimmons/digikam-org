var classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg =
[
    [ "ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a25e4be4be237ac0f05a83a4300e3a843", null ],
    [ "~ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#acf07ed676da0ae8e0a3ae7c1e4b50520", null ],
    [ "addToMainLayout", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getFolderTitle", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#adce904102fbe7538f89821d0256e6db7", null ],
    [ "getLocEdit", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];