var classheif_1_1StreamReader__memory =
[
    [ "grow_status", "classheif_1_1StreamReader__memory.html#a053ecfc73f3aa86fd7124c67e07b293d", [
      [ "size_reached", "classheif_1_1StreamReader__memory.html#a053ecfc73f3aa86fd7124c67e07b293daa01031f5674c88e9c7a7cdf725052206", null ],
      [ "timeout", "classheif_1_1StreamReader__memory.html#a053ecfc73f3aa86fd7124c67e07b293da5c5fb607a90f14d46a00f957d246f2bb", null ],
      [ "size_beyond_eof", "classheif_1_1StreamReader__memory.html#a053ecfc73f3aa86fd7124c67e07b293da02f265b40741b4b568a9e60285098657", null ]
    ] ],
    [ "StreamReader_memory", "classheif_1_1StreamReader__memory.html#a30f5b069c068236e76dff15830a455c6", null ],
    [ "~StreamReader_memory", "classheif_1_1StreamReader__memory.html#a5c1f9ce7bb21b234ebc3e9fe859ed033", null ],
    [ "get_position", "classheif_1_1StreamReader__memory.html#af313b20eec4501200b31de87ade0f38c", null ],
    [ "read", "classheif_1_1StreamReader__memory.html#a4fd84e120fb997e566c30bcd647fb14b", null ],
    [ "seek", "classheif_1_1StreamReader__memory.html#af68f8d2978add3ef67d6f38e377d76b1", null ],
    [ "seek_cur", "classheif_1_1StreamReader__memory.html#a5fef44e1d5988070c93b1cfdd3ad021f", null ],
    [ "wait_for_file_size", "classheif_1_1StreamReader__memory.html#a1a08e279466cec5cb69abec5c31085af", null ]
];