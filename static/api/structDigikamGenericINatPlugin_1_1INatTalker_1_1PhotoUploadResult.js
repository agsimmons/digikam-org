var structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult =
[
    [ "PhotoUploadResult", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html#ae6be110a62c5aef765590ceb4c7b061f", null ],
    [ "PhotoUploadResult", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html#ac375b4f03e61ca511f9e30844ad3b7c6", null ],
    [ "m_observationPhotoId", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html#a1115a4724f801894033ec4f9bf644eef", null ],
    [ "m_photoId", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html#a3fff4e1dcd8ed022e964f37b4f55034e", null ],
    [ "m_request", "structDigikamGenericINatPlugin_1_1INatTalker_1_1PhotoUploadResult.html#a2953a243a7005ccea2a284e6a945b286", null ]
];