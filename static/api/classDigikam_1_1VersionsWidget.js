var classDigikam_1_1VersionsWidget =
[
    [ "VersionsWidget", "classDigikam_1_1VersionsWidget.html#abf8531c483c0da39ed5e736a0cb17155", null ],
    [ "~VersionsWidget", "classDigikam_1_1VersionsWidget.html#a77460acff259556271f1ce1c14a9df20", null ],
    [ "addActionOverlay", "classDigikam_1_1VersionsWidget.html#a489297c472c7816134d1f10b73c74512", null ],
    [ "addShowHideOverlay", "classDigikam_1_1VersionsWidget.html#a28382bd46f37f1f14998fc9c5c48c90f", null ],
    [ "delegate", "classDigikam_1_1VersionsWidget.html#a5595ea72e6b2e20d32c4d566fa175bbc", null ],
    [ "imageSelected", "classDigikam_1_1VersionsWidget.html#a7b0180916d45de4b14eebb243747b12f", null ],
    [ "readSettings", "classDigikam_1_1VersionsWidget.html#a34431085d5a5b5707c6a35ef5f8d9d7d", null ],
    [ "setCurrentItem", "classDigikam_1_1VersionsWidget.html#a0153152aef1653f7a0abfba4a9ceea8f", null ],
    [ "slotSetupChanged", "classDigikam_1_1VersionsWidget.html#aef5b54bb659abad46c9fea261527b9eb", null ],
    [ "slotViewCurrentChanged", "classDigikam_1_1VersionsWidget.html#a742e74a812d57d59d5c2c211f1f1cd58", null ],
    [ "slotViewModeChanged", "classDigikam_1_1VersionsWidget.html#a59f0add4f7a63929540b45fc8d2f8a91", null ],
    [ "view", "classDigikam_1_1VersionsWidget.html#a15a6cd37b65cb170d79052405da56c17", null ],
    [ "writeSettings", "classDigikam_1_1VersionsWidget.html#ab2d08add0a6c2360d54c171b335a4e0f", null ]
];