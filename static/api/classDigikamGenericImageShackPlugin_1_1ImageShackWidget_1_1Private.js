var classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private =
[
    [ "Private", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a72fcd422782f58ab1008822566110bd0", null ],
    [ "accountNameLbl", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a5a67e6651ba8d55c36657701f19f32ad", null ],
    [ "chgRegCodeBtn", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a4eb32b7569ab8ca4c41981aed57212c3", null ],
    [ "galleriesCob", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a57a37b7fd4011399045317dffa672c3c", null ],
    [ "headerLbl", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#ab55532673a5442e68bb01a33bb2c1e40", null ],
    [ "iface", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#acf3fc5f135594765a0694f5613954dfc", null ],
    [ "imgList", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#aa5b5025d1cbf944f917b733cd9c2f08d", null ],
    [ "privateImagesChb", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a6690eda077e1e65fbba25ac8dd00cc7c", null ],
    [ "progressBar", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#aa3bdb65b5b58252071def739484dedd1", null ],
    [ "reloadGalleriesBtn", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a22678cdeb228d213eb4ee39774495323", null ],
    [ "remBarChb", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a3f0b73238c388c5e28e8c1853a1b834b", null ],
    [ "session", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#ac841ba5bff0100074bd712948511baa3", null ],
    [ "tagsFld", "classDigikamGenericImageShackPlugin_1_1ImageShackWidget_1_1Private.html#a9958d6ef210ed23ab11aba61692c3f27", null ]
];