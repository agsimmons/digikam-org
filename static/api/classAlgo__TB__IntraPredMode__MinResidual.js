var classAlgo__TB__IntraPredMode__MinResidual =
[
    [ "params", "structAlgo__TB__IntraPredMode__MinResidual_1_1params.html", "structAlgo__TB__IntraPredMode__MinResidual_1_1params" ],
    [ "analyze", "classAlgo__TB__IntraPredMode__MinResidual.html#af862f4e3b21bc45a20aeac781c8e3658", null ],
    [ "ascend", "classAlgo__TB__IntraPredMode__MinResidual.html#a4923a5065f57eca63f6d783ff0a8306a", null ],
    [ "descend", "classAlgo__TB__IntraPredMode__MinResidual.html#aff32dd1fc142a2d2ad143378ca3f9f7f", null ],
    [ "disableAllIntraPredModes", "classAlgo__TB__IntraPredMode__MinResidual.html#a306349660023eeaaeb797f77a621568f", null ],
    [ "enableAllIntraPredModes", "classAlgo__TB__IntraPredMode__MinResidual.html#a8cac00fd55bf8e670f0329bf15072bf2", null ],
    [ "enableIntraPredMode", "classAlgo__TB__IntraPredMode__MinResidual.html#a4e7336a6f846a461b0a2947cf2d7374d", null ],
    [ "enableIntraPredModeSubset", "classAlgo__TB__IntraPredMode__MinResidual.html#a9a0a95dea0715f8e8fde3ada84983b03", null ],
    [ "enter", "classAlgo__TB__IntraPredMode__MinResidual.html#ace022ffaf8d88aba411ee1b869fe6083", null ],
    [ "getPredMode", "classAlgo__TB__IntraPredMode__MinResidual.html#aecb3056bd2d652ebea3fccf05d12d090", null ],
    [ "isPredModeEnabled", "classAlgo__TB__IntraPredMode__MinResidual.html#a580896647f113b7108d148a4f569d50d", null ],
    [ "leaf", "classAlgo__TB__IntraPredMode__MinResidual.html#a46e2c61af40a6fee5d1850b7aa503033", null ],
    [ "name", "classAlgo__TB__IntraPredMode__MinResidual.html#a8863c5b006597228d6e12b16c7b5d5d2", null ],
    [ "nPredModesEnabled", "classAlgo__TB__IntraPredMode__MinResidual.html#af59dbc57d3344fc641ca65d3c9c96167", null ],
    [ "registerParams", "classAlgo__TB__IntraPredMode__MinResidual.html#a38fcef77bc48c61465d0a8d73a7c094f", null ],
    [ "setChildAlgo", "classAlgo__TB__IntraPredMode__MinResidual.html#ad93362a69307f29fdc737f15ac81df62", null ],
    [ "setParams", "classAlgo__TB__IntraPredMode__MinResidual.html#a3c6bc69ad4d5290ffdf20bd1c532292d", null ],
    [ "mTBSplitAlgo", "classAlgo__TB__IntraPredMode__MinResidual.html#a08cf308ab4c86fe56ceee0ad0c14028e", null ]
];