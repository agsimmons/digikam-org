var classDigikam_1_1TagsPopupMenu =
[
    [ "Mode", "classDigikam_1_1TagsPopupMenu.html#a0a11983a4576e067924659faa2b5d1fe", [
      [ "ASSIGN", "classDigikam_1_1TagsPopupMenu.html#a0a11983a4576e067924659faa2b5d1fead071a53765ea60f28102e84b01ec2773", null ],
      [ "REMOVE", "classDigikam_1_1TagsPopupMenu.html#a0a11983a4576e067924659faa2b5d1fea04007e0551a862f47d7d0bc73bc274f0", null ],
      [ "DISPLAY", "classDigikam_1_1TagsPopupMenu.html#a0a11983a4576e067924659faa2b5d1feab1a63279fa4ff7dcfd97517b37187d19", null ],
      [ "RECENTLYASSIGNED", "classDigikam_1_1TagsPopupMenu.html#a0a11983a4576e067924659faa2b5d1fea311060daf4aa819cf2b5d54498b524e9", null ]
    ] ],
    [ "TagsPopupMenu", "classDigikam_1_1TagsPopupMenu.html#a5f11bb5441d776830d5fafee80d27b47", null ],
    [ "TagsPopupMenu", "classDigikam_1_1TagsPopupMenu.html#a42640928e6e2e76ad17a55eedbd9df08", null ],
    [ "~TagsPopupMenu", "classDigikam_1_1TagsPopupMenu.html#a93227376f17f8db8c971fdc0554b7d58", null ],
    [ "signalPopupTagsView", "classDigikam_1_1TagsPopupMenu.html#a46b324f27ab6c2a1e465519753c31a18", null ],
    [ "signalTagActivated", "classDigikam_1_1TagsPopupMenu.html#a862f70abeb47ff65463df29eaaeef7c7", null ]
];