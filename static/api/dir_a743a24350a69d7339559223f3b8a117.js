var dir_a743a24350a69d7339559223f3b8a117 =
[
    [ "thumbsdb.cpp", "thumbsdb_8cpp.html", null ],
    [ "thumbsdb.h", "thumbsdb_8h.html", "thumbsdb_8h" ],
    [ "thumbsdbaccess.cpp", "thumbsdbaccess_8cpp.html", null ],
    [ "thumbsdbaccess.h", "thumbsdbaccess_8h.html", [
      [ "ThumbsDbAccess", "classDigikam_1_1ThumbsDbAccess.html", "classDigikam_1_1ThumbsDbAccess" ]
    ] ],
    [ "thumbsdbbackend.cpp", "thumbsdbbackend_8cpp.html", null ],
    [ "thumbsdbbackend.h", "thumbsdbbackend_8h.html", [
      [ "ThumbsDbBackend", "classDigikam_1_1ThumbsDbBackend.html", "classDigikam_1_1ThumbsDbBackend" ]
    ] ],
    [ "thumbsdbschemaupdater.cpp", "thumbsdbschemaupdater_8cpp.html", null ],
    [ "thumbsdbschemaupdater.h", "thumbsdbschemaupdater_8h.html", [
      [ "ThumbsDbSchemaUpdater", "classDigikam_1_1ThumbsDbSchemaUpdater.html", "classDigikam_1_1ThumbsDbSchemaUpdater" ]
    ] ]
];