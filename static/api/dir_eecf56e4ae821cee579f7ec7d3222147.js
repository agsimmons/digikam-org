var dir_eecf56e4ae821cee579f7ec7d3222147 =
[
    [ "bitstream.h", "libheif_2bitstream_8h.html", [
      [ "BitReader", "classheif_1_1BitReader.html", "classheif_1_1BitReader" ],
      [ "BitstreamRange", "classheif_1_1BitstreamRange.html", "classheif_1_1BitstreamRange" ],
      [ "StreamReader", "classheif_1_1StreamReader.html", "classheif_1_1StreamReader" ],
      [ "StreamReader_CApi", "classheif_1_1StreamReader__CApi.html", "classheif_1_1StreamReader__CApi" ],
      [ "StreamReader_istream", "classheif_1_1StreamReader__istream.html", "classheif_1_1StreamReader__istream" ],
      [ "StreamReader_memory", "classheif_1_1StreamReader__memory.html", "classheif_1_1StreamReader__memory" ],
      [ "StreamWriter", "classheif_1_1StreamWriter.html", "classheif_1_1StreamWriter" ]
    ] ],
    [ "box.h", "box_8h.html", "box_8h" ],
    [ "error.h", "error_8h.html", "error_8h" ],
    [ "heif.h", "heif_8h.html", "heif_8h" ],
    [ "heif_api_structs.h", "heif__api__structs_8h.html", [
      [ "heif_context", "structheif__context.html", "structheif__context" ],
      [ "heif_encoder", "structheif__encoder.html", "structheif__encoder" ],
      [ "heif_image", "structheif__image.html", "structheif__image" ],
      [ "heif_image_handle", "structheif__image__handle.html", "structheif__image__handle" ]
    ] ],
    [ "heif_colorconversion.h", "heif__colorconversion_8h.html", "heif__colorconversion_8h" ],
    [ "heif_context.h", "heif__context_8h.html", [
      [ "HeifContext", "classheif_1_1HeifContext.html", "classheif_1_1HeifContext" ],
      [ "Image", "classheif_1_1HeifContext_1_1Image.html", "classheif_1_1HeifContext_1_1Image" ],
      [ "ImageMetadata", "classheif_1_1ImageMetadata.html", "classheif_1_1ImageMetadata" ]
    ] ],
    [ "heif_cxx.h", "heif__cxx_8h.html", "heif__cxx_8h" ],
    [ "heif_decoder_libde265.h", "heif__decoder__libde265_8h.html", "heif__decoder__libde265_8h" ],
    [ "heif_encoder_x265.h", "heif__encoder__x265_8h.html", "heif__encoder__x265_8h" ],
    [ "heif_file.h", "heif__file_8h.html", [
      [ "HeifFile", "classheif_1_1HeifFile.html", "classheif_1_1HeifFile" ]
    ] ],
    [ "heif_hevc.h", "heif__hevc_8h.html", "heif__hevc_8h" ],
    [ "heif_image.h", "heif__image_8h.html", "heif__image_8h" ],
    [ "heif_limits.h", "heif__limits_8h.html", null ],
    [ "heif_plugin.h", "heif__plugin_8h.html", "heif__plugin_8h" ],
    [ "heif_plugin_registry.h", "heif__plugin__registry_8h.html", "heif__plugin__registry_8h" ],
    [ "logging.h", "logging_8h.html", "logging_8h" ]
];