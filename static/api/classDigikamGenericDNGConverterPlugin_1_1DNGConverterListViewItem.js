var classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem =
[
    [ "State", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00", [
      [ "Waiting", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00ad8b6f8b9251773ead76ec18aa385d4a9", null ],
      [ "Success", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00a7190ad46a44a198df532d32fa51a5138", null ],
      [ "Failed", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a0738c425eb8ef2492e0db2e2184c0f00a69e88d7c591e2ac31e3b420e2604319d", null ]
    ] ],
    [ "DNGConverterListViewItem", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a7a5e9ffd42bcc0305b2379112704dfe2", null ],
    [ "~DNGConverterListViewItem", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a423ea721776293ba163dc448d07d6ed5", null ],
    [ "comments", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#ae4091d56bd5a7136620d19e58f36df6b", null ],
    [ "destFileName", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a2b26d517e26002833afb2c9707ad186a", null ],
    [ "destPath", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a08e60aee184ef246e44a7d7dd2cc91ee", null ],
    [ "hasValidThumbnail", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a54fbe8eb2409f9f70df4ccad5685e476", null ],
    [ "identity", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a265492d7bc5dab0d9dc125f0e5435c47", null ],
    [ "rating", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#aaeb082f799e8983567c2af996a138a5b", null ],
    [ "setComments", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#ad892aa46c226496f5fe5ab38566ca521", null ],
    [ "setDestFileName", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a0f522ed8a10e84f0cd88dc74e00ffa40", null ],
    [ "setIdentity", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a95cbf8dc7f68c1de534830cc85244c89", null ],
    [ "setIsLessThanHandler", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a24068f26b31d6551d5c867ebb5cc5592", null ],
    [ "setProcessedIcon", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#ac95a737a72e817aeaac73c5ee285bf8d", null ],
    [ "setProgressAnimation", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#aaaa6c90b33cdbb539561019bda338442", null ],
    [ "setRating", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a3f9e7e716209f22574b8a3f62e81ec15", null ],
    [ "setState", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a9b1b696ccb0191e5b7d25ba59784d902", null ],
    [ "setStatus", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a1d8c4363745bb87eeaf56609c6710563", null ],
    [ "setTags", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#aa47ad759c279af82fe2f752d5b9ee7f0", null ],
    [ "setThumb", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#acadca5a7c0b7cbf90a0ab1f7f2688372", null ],
    [ "setUrl", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a8e1693e591371b842c878b1b2dc7f111", null ],
    [ "state", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#af4a13fa6b543e1b6af7c9665414989ec", null ],
    [ "tags", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#afc4a7455b083018334f707479a695800", null ],
    [ "updateInformation", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a69d31e335d852ce1e67f85c560c67aea", null ],
    [ "updateItemWidgets", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#a667b8e722eec6b93aefc8f1e7a60437b", null ],
    [ "url", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#ab9cc1b528f5eddbf5297c36ee34bafae", null ],
    [ "view", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterListViewItem.html#ad4e6b75da8433c542e5fd19a56e869f4", null ]
];