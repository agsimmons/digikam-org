var classDigikam_1_1DNNFaceDetectorYOLO =
[
    [ "DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html#a7c57da144eec7dd31dcbbdb739041700", null ],
    [ "~DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html#a24eda3eb69e2c16e7ea9c9715f1e41f4", null ],
    [ "correctBbox", "classDigikam_1_1DNNFaceDetectorYOLO.html#a0fef3b769989f7e1f752dbf7074697c4", null ],
    [ "detectFaces", "classDigikam_1_1DNNFaceDetectorYOLO.html#ae00aee79bf5de77277dfbde850bc5724", null ],
    [ "loadModels", "classDigikam_1_1DNNFaceDetectorYOLO.html#a119b3c228eacd1c5ef4384c97f4ad844", null ],
    [ "nnInputSizeRequired", "classDigikam_1_1DNNFaceDetectorYOLO.html#a10c2f7b450ebc0a6b0b631e333f4f10d", null ],
    [ "selectBbox", "classDigikam_1_1DNNFaceDetectorYOLO.html#ac162bff0abe3bb46f644651ae04d7026", null ],
    [ "inputImageSize", "classDigikam_1_1DNNFaceDetectorYOLO.html#a57327c3e3bc01d71b2c155cb51f10488", null ],
    [ "meanValToSubtract", "classDigikam_1_1DNNFaceDetectorYOLO.html#a800befeba470c89ae6e2b9cad13bf6f3", null ],
    [ "mutex", "classDigikam_1_1DNNFaceDetectorYOLO.html#add749bb2513feabbf51fbf7a8d368fbf", null ],
    [ "net", "classDigikam_1_1DNNFaceDetectorYOLO.html#a05c55414cc0784bebc3925a6547f8fef", null ],
    [ "scaleFactor", "classDigikam_1_1DNNFaceDetectorYOLO.html#a3c902bace9d612c1990c1eeeec51b22f", null ]
];