var classDigikamGenericSendByMailPlugin_1_1ImageResizeThread =
[
    [ "ImageResizeThread", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#af709fa21f1974694dad678c8e5995509", null ],
    [ "~ImageResizeThread", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a17c473a31b2d32845478108a6f1e4ee0", null ],
    [ "appendJobs", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a354a7c86ac83b4aa46e723149032a471", null ],
    [ "cancel", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a3b2d0aaf6c28a72b9f46a78810d9af33", null ],
    [ "failedResize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#ab01db2b0c01ddc28dcbf913d3d8fa25b", null ],
    [ "finishedResize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a428083cd4ec3bf5b88e5cf7816c4ac00", null ],
    [ "isEmpty", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a61f8a4a39061aed1ec1c105a0eddad67", null ],
    [ "maximumNumberOfThreads", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#ae1dc672ceb1c9f127db1d53fe6d40700", null ],
    [ "pendingCount", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#abf47dc040dce8ea4779ecb8c1fb375d0", null ],
    [ "resize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a1d9f39b777b389b6e02b8dc4a9573db0", null ],
    [ "run", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a2373a97a31a8897956bf86b4abebff09", null ],
    [ "setDefaultMaximumNumberOfThreads", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a99e7e530af4825351ba34362385e08fa", null ],
    [ "setMaximumNumberOfThreads", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a3c5aecc58f8d60ea5d2f8d35ef5649c2", null ],
    [ "slotJobFinished", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#a90f7300fc37ec60e08a3101b68da6409", null ],
    [ "startingResize", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html#abb555a57ec2ff97629000f9ac0dabf5f", null ]
];