var namespaceDigikamGenericYFPlugin =
[
    [ "YandexFotkiAlbum", "classDigikamGenericYFPlugin_1_1YandexFotkiAlbum.html", "classDigikamGenericYFPlugin_1_1YandexFotkiAlbum" ],
    [ "YFNewAlbumDlg", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg" ],
    [ "YFPhoto", "classDigikamGenericYFPlugin_1_1YFPhoto.html", "classDigikamGenericYFPlugin_1_1YFPhoto" ],
    [ "YFPlugin", "classDigikamGenericYFPlugin_1_1YFPlugin.html", "classDigikamGenericYFPlugin_1_1YFPlugin" ],
    [ "YFTalker", "classDigikamGenericYFPlugin_1_1YFTalker.html", "classDigikamGenericYFPlugin_1_1YFTalker" ],
    [ "YFWidget", "classDigikamGenericYFPlugin_1_1YFWidget.html", "classDigikamGenericYFPlugin_1_1YFWidget" ],
    [ "YFWindow", "classDigikamGenericYFPlugin_1_1YFWindow.html", "classDigikamGenericYFPlugin_1_1YFWindow" ],
    [ "operator<<", "namespaceDigikamGenericYFPlugin.html#a357a39bb2b8a12e3837a8d2922d35e78", null ],
    [ "operator<<", "namespaceDigikamGenericYFPlugin.html#a830c2685e2882f346fb51809fc526a05", null ]
];