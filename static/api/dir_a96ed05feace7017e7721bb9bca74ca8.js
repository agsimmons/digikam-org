var dir_a96ed05feace7017e7721bb9bca74ca8 =
[
    [ "autoexpofilter.cpp", "autoexpofilter_8cpp.html", null ],
    [ "autoexpofilter.h", "autoexpofilter_8h.html", [
      [ "AutoExpoFilter", "classDigikam_1_1AutoExpoFilter.html", "classDigikam_1_1AutoExpoFilter" ]
    ] ],
    [ "autolevelsfilter.cpp", "autolevelsfilter_8cpp.html", null ],
    [ "autolevelsfilter.h", "autolevelsfilter_8h.html", [
      [ "AutoLevelsFilter", "classDigikam_1_1AutoLevelsFilter.html", "classDigikam_1_1AutoLevelsFilter" ]
    ] ],
    [ "equalizefilter.cpp", "equalizefilter_8cpp.html", null ],
    [ "equalizefilter.h", "equalizefilter_8h.html", [
      [ "EqualizeFilter", "classDigikam_1_1EqualizeFilter.html", "classDigikam_1_1EqualizeFilter" ]
    ] ],
    [ "normalizefilter.cpp", "normalizefilter_8cpp.html", null ],
    [ "normalizefilter.h", "normalizefilter_8h.html", [
      [ "NormalizeFilter", "classDigikam_1_1NormalizeFilter.html", "classDigikam_1_1NormalizeFilter" ]
    ] ],
    [ "stretchfilter.cpp", "stretchfilter_8cpp.html", null ],
    [ "stretchfilter.h", "stretchfilter_8h.html", [
      [ "StretchFilter", "classDigikam_1_1StretchFilter.html", "classDigikam_1_1StretchFilter" ]
    ] ]
];