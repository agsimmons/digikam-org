var classDigikam_1_1VersionFileOperation =
[
    [ "Task", "classDigikam_1_1VersionFileOperation.html#af5916204db7b10d39ef8dc8ed57bcb5e", [
      [ "NewFile", "classDigikam_1_1VersionFileOperation.html#af5916204db7b10d39ef8dc8ed57bcb5ea2e15d8bb3d2a2c1a65cb84584b1118b7", null ],
      [ "Replace", "classDigikam_1_1VersionFileOperation.html#af5916204db7b10d39ef8dc8ed57bcb5eaa64163838099c385caa02a6e1d4d86dc", null ],
      [ "SaveAndDelete", "classDigikam_1_1VersionFileOperation.html#af5916204db7b10d39ef8dc8ed57bcb5ea68c1a3a9ff13fbd2d4ba965de04d0960", null ],
      [ "MoveToIntermediate", "classDigikam_1_1VersionFileOperation.html#af5916204db7b10d39ef8dc8ed57bcb5ea6b9e6023639419e62ba53a1245491929", null ],
      [ "StoreIntermediates", "classDigikam_1_1VersionFileOperation.html#af5916204db7b10d39ef8dc8ed57bcb5ea36a1aadb2c5af97f05b42b0c6c0d3f2e", null ]
    ] ],
    [ "VersionFileOperation", "classDigikam_1_1VersionFileOperation.html#ab4f6e79dc2f5dd078e9c8fabff317e7e", null ],
    [ "allFilePaths", "classDigikam_1_1VersionFileOperation.html#a978f47264855955734aeae0bc476b58d", null ],
    [ "intermediateForLoadedFile", "classDigikam_1_1VersionFileOperation.html#a6a6b4df478181cd5036c4740ef4482ac", null ],
    [ "intermediates", "classDigikam_1_1VersionFileOperation.html#a5786059376bdbb48fd0668709de18cb8", null ],
    [ "loadedFile", "classDigikam_1_1VersionFileOperation.html#aeea91528f89c800501f6e0f2cb84deaf", null ],
    [ "saveFile", "classDigikam_1_1VersionFileOperation.html#a25a98b5ffa414826d46ea6d30be9e460", null ],
    [ "tasks", "classDigikam_1_1VersionFileOperation.html#af8fce922d60ff393eb58d675f65d4faa", null ]
];