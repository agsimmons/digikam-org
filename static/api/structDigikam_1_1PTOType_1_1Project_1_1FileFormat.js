var structDigikam_1_1PTOType_1_1Project_1_1FileFormat =
[
    [ "CompressionMethod", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a574a845fc8d65ed34404d524bb1dc1f5", [
      [ "PANO_NONE", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a574a845fc8d65ed34404d524bb1dc1f5a11e15668a381de816b3c27ee7b22eaf4", null ],
      [ "LZW", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a574a845fc8d65ed34404d524bb1dc1f5a2b1242b0bcf7bb41e6eb4a4cd32c355c", null ],
      [ "DEFLATE", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a574a845fc8d65ed34404d524bb1dc1f5a3110423f4e3e3b487605106f70d1a49c", null ]
    ] ],
    [ "FileType", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#aded0fc3ebbc334b214e92f92c37752fa", [
      [ "PNG", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#aded0fc3ebbc334b214e92f92c37752faa76666955e8ebe841061762e9a9d0be91", null ],
      [ "TIFF", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#aded0fc3ebbc334b214e92f92c37752faac9b4689d789dd04c27f548e7e1d59c1b", null ],
      [ "TIFF_m", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#aded0fc3ebbc334b214e92f92c37752faa9acc4722e8517248256a14335903f702", null ],
      [ "TIFF_multilayer", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#aded0fc3ebbc334b214e92f92c37752faa3c254e4023b0a0106d6c415bc63814fc", null ],
      [ "JPEG", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#aded0fc3ebbc334b214e92f92c37752faa02fc396418e1bb8346d87d172a5c9b3f", null ]
    ] ],
    [ "FileFormat", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a226cf871b8c68a051a6f658eb382407a", null ],
    [ "compressionMethod", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a2062d0ce376bbf04d8065688cddec73c", null ],
    [ "cropped", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a64ba5b876f249fb219295b6677395e24", null ],
    [ "fileType", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a445df4beecad01e030f44b6a6d634586", null ],
    [ "quality", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a3ac5eb5a4092d5a11f6cb71a315550f8", null ],
    [ "savePositions", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#abd6ffff102b18930a46fa8faf26dea6f", null ]
];