var classDigikam_1_1EditorToolIface =
[
    [ "EditorToolIface", "classDigikam_1_1EditorToolIface.html#ad7de36ce992467b4e8e05459f3a3d637", null ],
    [ "~EditorToolIface", "classDigikam_1_1EditorToolIface.html#aa086463e02c9115317e80e727841423e", null ],
    [ "currentTool", "classDigikam_1_1EditorToolIface.html#a6b04fcccac7b5d362c4b8f17cca32418", null ],
    [ "loadTool", "classDigikam_1_1EditorToolIface.html#a63b6b43e9cdb42993958bef84e7004f6", null ],
    [ "setPreviewModeMask", "classDigikam_1_1EditorToolIface.html#a80542f54f2cedded349ebf2fe000f166", null ],
    [ "setToolInfoMessage", "classDigikam_1_1EditorToolIface.html#a473475077aca6325237b090e743100fa", null ],
    [ "setToolProgress", "classDigikam_1_1EditorToolIface.html#abb286833bc41d25a7db08e26dfa41004", null ],
    [ "setToolsIconView", "classDigikam_1_1EditorToolIface.html#aab823884035d4946cdf9d1b741b57806", null ],
    [ "setToolStartProgress", "classDigikam_1_1EditorToolIface.html#a7db98352b24bdc39fdf1b95e9f967e36", null ],
    [ "setToolStopProgress", "classDigikam_1_1EditorToolIface.html#a0e3c98725792df2b31d09323b7a67847", null ],
    [ "setupICC", "classDigikam_1_1EditorToolIface.html#a68e74f7bc428ac085da0e5aea3b0afc8", null ],
    [ "signalPreviewModeChanged", "classDigikam_1_1EditorToolIface.html#a0c79c8c065b1ab6bd976d77f7c600877", null ],
    [ "slotApplyTool", "classDigikam_1_1EditorToolIface.html#adbba9e04cf43dc5d42e4cd5ec21cb33b", null ],
    [ "slotCloseTool", "classDigikam_1_1EditorToolIface.html#a14657bded3dc9890953def964ed096f6", null ],
    [ "slotToolAborted", "classDigikam_1_1EditorToolIface.html#ad80c46e7e38478d50a853f65ba0256a6", null ],
    [ "slotToolApplied", "classDigikam_1_1EditorToolIface.html#a9aba4ee622292c9ac9edf76019b42589", null ],
    [ "themeChanged", "classDigikam_1_1EditorToolIface.html#ad2baea92cc718e28d118e2ebebcfb19a", null ],
    [ "unLoadTool", "classDigikam_1_1EditorToolIface.html#a29ab51ebe8d174fa30869a82d1205804", null ],
    [ "updateExposureSettings", "classDigikam_1_1EditorToolIface.html#ab1f5b54531e11d6cb5d4297c0c7f5aff", null ],
    [ "updateICCSettings", "classDigikam_1_1EditorToolIface.html#a572d6f7d20582401dcf934b150a34a11", null ]
];