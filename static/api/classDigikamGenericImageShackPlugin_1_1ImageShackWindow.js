var classDigikamGenericImageShackPlugin_1_1ImageShackWindow =
[
    [ "ImageShackWindow", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a77bb076ce06fc257fc02ceee568ac833", null ],
    [ "~ImageShackWindow", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a583f3b7394a790c807a47faca336ac9c", null ],
    [ "addButton", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "getImagesList", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a8d11376218d795dfc663647f294d0ec5", null ],
    [ "restoreDialogSize", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "signalBusy", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a8b77da5a88ffbe8a03e3f968a26fd50a", null ],
    [ "startButton", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericImageShackPlugin_1_1ImageShackWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];