var dir_5242a64008c43907089aeb449c5dadd5 =
[
    [ "colorgradientwidget.cpp", "colorgradientwidget_8cpp.html", null ],
    [ "colorgradientwidget.h", "colorgradientwidget_8h.html", [
      [ "ColorGradientWidget", "classDigikam_1_1ColorGradientWidget.html", "classDigikam_1_1ColorGradientWidget" ]
    ] ],
    [ "dcolorchoosermode.cpp", "dcolorchoosermode_8cpp.html", "dcolorchoosermode_8cpp" ],
    [ "dcolorchoosermode.h", "dcolorchoosermode_8h.html", "dcolorchoosermode_8h" ],
    [ "dcolorchoosermode_p.h", "dcolorchoosermode__p_8h.html", "dcolorchoosermode__p_8h" ],
    [ "dcolorselector.cpp", "dcolorselector_8cpp.html", null ],
    [ "dcolorselector.h", "dcolorselector_8h.html", [
      [ "DColorSelector", "classDigikam_1_1DColorSelector.html", "classDigikam_1_1DColorSelector" ]
    ] ],
    [ "dcolorvalueselector.cpp", "dcolorvalueselector_8cpp.html", null ],
    [ "dcolorvalueselector.h", "dcolorvalueselector_8h.html", [
      [ "DColorValueSelector", "classDigikam_1_1DColorValueSelector.html", "classDigikam_1_1DColorValueSelector" ],
      [ "DSelector", "classDigikam_1_1DSelector.html", "classDigikam_1_1DSelector" ]
    ] ],
    [ "dgradientslider.cpp", "dgradientslider_8cpp.html", "dgradientslider_8cpp" ],
    [ "dgradientslider.h", "dgradientslider_8h.html", [
      [ "DGradientSlider", "classDigikam_1_1DGradientSlider.html", "classDigikam_1_1DGradientSlider" ]
    ] ],
    [ "dhuesaturationselect.cpp", "dhuesaturationselect_8cpp.html", null ],
    [ "dhuesaturationselect.h", "dhuesaturationselect_8h.html", [
      [ "DHueSaturationSelector", "classDigikam_1_1DHueSaturationSelector.html", "classDigikam_1_1DHueSaturationSelector" ],
      [ "DPointSelect", "classDigikam_1_1DPointSelect.html", "classDigikam_1_1DPointSelect" ]
    ] ]
];