var classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage =
[
    [ "JAlbumFinalPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#ae9ec5c4d0cf965b4fc223e5c85969af6", null ],
    [ "~JAlbumFinalPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#aa96e25798440e9467d1a2471093d7487", null ],
    [ "assistant", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "id", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "initializePage", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#aa15cd3f3852686fe0ee1c29c9f8adbf9", null ],
    [ "isComplete", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a0d5210cc79942b916dd707525ad3da78", null ],
    [ "removePageWidget", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "setComplete", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html#a67975edf6041a574e674576a29d606a1", null ]
];