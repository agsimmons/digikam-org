var classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope =
[
    [ "IPTCEnvelope", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html#a5c42f371486a470920af4d289537a6e5", null ],
    [ "~IPTCEnvelope", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html#aebd8dfd9b4ed26fdb3ac216334035aa9", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html#ad3e00fcaae9a6ac58db06c2a25e29947", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html#a7ba09665d06553bc13e46fe8b4d8b5e1", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1IPTCEnvelope.html#a5c6607749f930deb219857de5f0878e4", null ]
];