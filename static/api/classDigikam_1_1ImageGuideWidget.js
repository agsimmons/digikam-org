var classDigikam_1_1ImageGuideWidget =
[
    [ "ColorPointSrc", "classDigikam_1_1ImageGuideWidget.html#a7f3c020c8499f9e42219439ebd09ec31", [
      [ "OriginalImage", "classDigikam_1_1ImageGuideWidget.html#a7f3c020c8499f9e42219439ebd09ec31a63e29ace55e19d96e0fcd2182675bd3a", null ],
      [ "PreviewImage", "classDigikam_1_1ImageGuideWidget.html#a7f3c020c8499f9e42219439ebd09ec31acef4fa7da354b4182b5e19cdf048d64d", null ],
      [ "TargetPreviewImage", "classDigikam_1_1ImageGuideWidget.html#a7f3c020c8499f9e42219439ebd09ec31adf4ae955f7fdf455fb727206236d4294", null ]
    ] ],
    [ "GuideToolMode", "classDigikam_1_1ImageGuideWidget.html#af0a33c1174c0d400dd4046cef4a6be76", [
      [ "HVGuideMode", "classDigikam_1_1ImageGuideWidget.html#af0a33c1174c0d400dd4046cef4a6be76a185b5d2b595c53886fd0e75de3057f26", null ],
      [ "PickColorMode", "classDigikam_1_1ImageGuideWidget.html#af0a33c1174c0d400dd4046cef4a6be76abf39c0c5b4275b74bccaf7c9e7d6cf92", null ]
    ] ],
    [ "ImageGuideWidget", "classDigikam_1_1ImageGuideWidget.html#aeaf58c1d05aea191a43ddaf83311361c", null ],
    [ "~ImageGuideWidget", "classDigikam_1_1ImageGuideWidget.html#a4e2a618d5fdfea107aff019c71b872fc", null ],
    [ "drawLineTo", "classDigikam_1_1ImageGuideWidget.html#aa35207f8134ad3c00bc5f6708547fe76", null ],
    [ "drawLineTo", "classDigikam_1_1ImageGuideWidget.html#aa365e424cf1009510281b81379955c98", null ],
    [ "drawText", "classDigikam_1_1ImageGuideWidget.html#ae42c9d49219a68e7052ab8d797d912b1", null ],
    [ "enterEvent", "classDigikam_1_1ImageGuideWidget.html#a716c7b3415344af25b79310e44a2efb3", null ],
    [ "exposureSettingsChanged", "classDigikam_1_1ImageGuideWidget.html#ace9869ae0eab7240f678a8008d10ab1f", null ],
    [ "getMask", "classDigikam_1_1ImageGuideWidget.html#ad035735c93b3eb63cdd15c9daad7d761", null ],
    [ "getSpotColor", "classDigikam_1_1ImageGuideWidget.html#a3290df8fd0c519bd9be6574fc90b8ed7", null ],
    [ "getSpotPosition", "classDigikam_1_1ImageGuideWidget.html#a4ab45abc19795b8e87bba3e54a9182cf", null ],
    [ "ICCSettingsChanged", "classDigikam_1_1ImageGuideWidget.html#acd403254cf7740105deaea94fca29526", null ],
    [ "imageIface", "classDigikam_1_1ImageGuideWidget.html#a92efd16840fc766476ea5d6819958463", null ],
    [ "leaveEvent", "classDigikam_1_1ImageGuideWidget.html#ac8032baaf083ff613fa86923395f7ed2", null ],
    [ "mouseMoveEvent", "classDigikam_1_1ImageGuideWidget.html#a8014ae0aefe68386238c2017d6c8f70d", null ],
    [ "mousePressEvent", "classDigikam_1_1ImageGuideWidget.html#a0126eca68bfa0cc69d16296d735645e8", null ],
    [ "mouseReleaseEvent", "classDigikam_1_1ImageGuideWidget.html#a1551c20d04003561d2d14045276af249", null ],
    [ "paintEvent", "classDigikam_1_1ImageGuideWidget.html#a1577b0c48bf8349d0a0b768e696e0c34", null ],
    [ "previewMode", "classDigikam_1_1ImageGuideWidget.html#a9034868c506f27e2abeb3f6e885c0c9d", null ],
    [ "resetPoints", "classDigikam_1_1ImageGuideWidget.html#a40f545fcb8f5b7aa1228885a03c151d9", null ],
    [ "resetSpotPosition", "classDigikam_1_1ImageGuideWidget.html#aca8b00c9217c48fd3210bc6bc6b457b4", null ],
    [ "resizeEvent", "classDigikam_1_1ImageGuideWidget.html#a529602aaebd67fcabb6cd5b65e0aab69", null ],
    [ "setBackgroundColor", "classDigikam_1_1ImageGuideWidget.html#ae7a1dd4b96225fe17aaefd2a24ff9bd3", null ],
    [ "setEraseMode", "classDigikam_1_1ImageGuideWidget.html#acb6806f6ea6f5a944415a6e928c2ab89", null ],
    [ "setMaskCursor", "classDigikam_1_1ImageGuideWidget.html#a6951de8df6ca4c0ac933f218f0c39833", null ],
    [ "setMaskEnabled", "classDigikam_1_1ImageGuideWidget.html#a991a8e1e8ea03cbb266044eeaf3f70e3", null ],
    [ "setMaskPenSize", "classDigikam_1_1ImageGuideWidget.html#a7e7169318fb2b6c5e4c4615b214065ac", null ],
    [ "setPaintColor", "classDigikam_1_1ImageGuideWidget.html#a306232f7bb0721cdafd1a964a1c5790f", null ],
    [ "setPoints", "classDigikam_1_1ImageGuideWidget.html#ac37be62e9d44489b3f58ea878666eafe", null ],
    [ "setSpotPosition", "classDigikam_1_1ImageGuideWidget.html#a6e5539e94429da520c70ea31e093b594", null ],
    [ "setSpotVisible", "classDigikam_1_1ImageGuideWidget.html#ad31c08aa807bc96c3e18ce2477abd4fa", null ],
    [ "setSpotVisibleNoUpdate", "classDigikam_1_1ImageGuideWidget.html#a5f938cb0c7ac78554f86a7d99e938281", null ],
    [ "signalResized", "classDigikam_1_1ImageGuideWidget.html#a689f7551cabe52cf9dc82ee45c7a5e8b", null ],
    [ "slotChangeGuideColor", "classDigikam_1_1ImageGuideWidget.html#a0fba8f24e25b14f9a2a1939acf77aa8d", null ],
    [ "slotChangeGuideSize", "classDigikam_1_1ImageGuideWidget.html#a363439898ecce4101fd9956321f67915", null ],
    [ "slotPreviewModeChanged", "classDigikam_1_1ImageGuideWidget.html#a9521b6c0edb8de5a22f4a8bb2815c3b2", null ],
    [ "spotPositionChangedFromOriginal", "classDigikam_1_1ImageGuideWidget.html#a399f5b7ab19fc2f41403562f8e5195a1", null ],
    [ "spotPositionChangedFromTarget", "classDigikam_1_1ImageGuideWidget.html#ad23d62fd5902b5352159923504fa9982", null ],
    [ "timerEvent", "classDigikam_1_1ImageGuideWidget.html#ace286b1ea14d8e8cfe90537b99880b88", null ],
    [ "translateItemPosition", "classDigikam_1_1ImageGuideWidget.html#a85159eea90fd16f080eeca6666dd1893", null ],
    [ "translatePointPosition", "classDigikam_1_1ImageGuideWidget.html#a8db88cf9a66aea899265ec7176c158cc", null ],
    [ "updateMaskCursor", "classDigikam_1_1ImageGuideWidget.html#ac27a41477f77c19bf855a21d7cb2af68", null ],
    [ "updatePixmap", "classDigikam_1_1ImageGuideWidget.html#a0fa02c5b306d6fefa0bc678164238cf9", null ],
    [ "updatePreview", "classDigikam_1_1ImageGuideWidget.html#a9d92242c5181520e7602a7968bb53a12", null ],
    [ "updateSpotPosition", "classDigikam_1_1ImageGuideWidget.html#abe665b31f857b5bc03abaac3c98bd005", null ]
];