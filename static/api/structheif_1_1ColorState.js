var structheif_1_1ColorState =
[
    [ "ColorState", "structheif_1_1ColorState.html#a510a8db63d449be6eb76a23fe3586a96", null ],
    [ "ColorState", "structheif_1_1ColorState.html#a4df76c1b61ceade2d841801b25238f63", null ],
    [ "operator==", "structheif_1_1ColorState.html#aebb565801ea09b24d6272ceb374ee9f4", null ],
    [ "bits_per_pixel", "structheif_1_1ColorState.html#aba16cd5a51b9d6655f26a879476c1e64", null ],
    [ "chroma", "structheif_1_1ColorState.html#a90d316806930305e57f904cd1fe0de9e", null ],
    [ "colorspace", "structheif_1_1ColorState.html#aef101899afd36f786a9a3a5964b18c09", null ],
    [ "has_alpha", "structheif_1_1ColorState.html#ac9643af5e82d30483f40debe824442f5", null ]
];