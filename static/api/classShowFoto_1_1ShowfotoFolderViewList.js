var classShowFoto_1_1ShowfotoFolderViewList =
[
    [ "FolderViewMode", "classShowFoto_1_1ShowfotoFolderViewList.html#aeabb532074aedcdb60e7f1ff763a508f", [
      [ "ShortView", "classShowFoto_1_1ShowfotoFolderViewList.html#aeabb532074aedcdb60e7f1ff763a508fa583ffed658bcb406f4c07243f866dd17", null ],
      [ "DetailledView", "classShowFoto_1_1ShowfotoFolderViewList.html#aeabb532074aedcdb60e7f1ff763a508fae93afb8b41f64a8cc44fbfe580217568", null ]
    ] ],
    [ "FolderViewRole", "classShowFoto_1_1ShowfotoFolderViewList.html#af2a5b0c3f06ca832b25630bdc1c89f0e", [
      [ "FileName", "classShowFoto_1_1ShowfotoFolderViewList.html#af2a5b0c3f06ca832b25630bdc1c89f0eaf1ed06ddc93dc1755550160301ca244b", null ],
      [ "FileSize", "classShowFoto_1_1ShowfotoFolderViewList.html#af2a5b0c3f06ca832b25630bdc1c89f0ea99f6db9452bf4e8dff0383b9ce5ae01e", null ],
      [ "FileType", "classShowFoto_1_1ShowfotoFolderViewList.html#af2a5b0c3f06ca832b25630bdc1c89f0ea1ce485a1ca711af5dd05382a620bd8d3", null ],
      [ "FileDate", "classShowFoto_1_1ShowfotoFolderViewList.html#af2a5b0c3f06ca832b25630bdc1c89f0ea9a1549b81013b00408a1e014ece5c9ad", null ]
    ] ],
    [ "ShowfotoFolderViewList", "classShowFoto_1_1ShowfotoFolderViewList.html#a19328ed73b82eda7a3ef4713c6edc4a8", null ],
    [ "~ShowfotoFolderViewList", "classShowFoto_1_1ShowfotoFolderViewList.html#a558540701c50410a5c96d3f7e1c541a2", null ],
    [ "signalAddBookmark", "classShowFoto_1_1ShowfotoFolderViewList.html#a7668153e468ac77420c5052e470d9e4b", null ],
    [ "slotIconSizeChanged", "classShowFoto_1_1ShowfotoFolderViewList.html#ac0ecaf2be24866e4f4421651d01f473d", null ]
];