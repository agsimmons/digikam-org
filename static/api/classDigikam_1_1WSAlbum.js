var classDigikam_1_1WSAlbum =
[
    [ "WSAlbum", "classDigikam_1_1WSAlbum.html#a710f5ca695d68a1ed87e8ccc9cc05499", null ],
    [ "setBaseAlbum", "classDigikam_1_1WSAlbum.html#a36fdd1f22061bc6135f1b6a7887e95be", null ],
    [ "description", "classDigikam_1_1WSAlbum.html#a5673caf5f00c5aaa4ca9f4b4f3a5f6b8", null ],
    [ "id", "classDigikam_1_1WSAlbum.html#afb750ec19cd9453e0a7c6288e9830623", null ],
    [ "isRoot", "classDigikam_1_1WSAlbum.html#aee74250961b6ef9f3ab7522e4a6b8d6c", null ],
    [ "location", "classDigikam_1_1WSAlbum.html#a7fd45d3c50b51b045cb331450ed27549", null ],
    [ "parentID", "classDigikam_1_1WSAlbum.html#a586a0c1a9222cf838f4cb1b2a631a638", null ],
    [ "title", "classDigikam_1_1WSAlbum.html#a23a3feb6d7ccbec24bdc28fa12da4bd2", null ],
    [ "uploadable", "classDigikam_1_1WSAlbum.html#a461e0d2282a55d51d6cd3409d1ae0940", null ],
    [ "url", "classDigikam_1_1WSAlbum.html#a8a49aed1399b541a655e1dcc52033046", null ]
];