var dir_172299b4a566a17b9925e12c413b5a4a =
[
    [ "jalbumfinalpage.cpp", "jalbumfinalpage_8cpp.html", null ],
    [ "jalbumfinalpage.h", "jalbumfinalpage_8h.html", [
      [ "JAlbumFinalPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumFinalPage" ]
    ] ],
    [ "jalbumintropage.cpp", "jalbumintropage_8cpp.html", null ],
    [ "jalbumintropage.h", "jalbumintropage_8h.html", [
      [ "JAlbumIntroPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumIntroPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumIntroPage" ]
    ] ],
    [ "jalbumoutputpage.cpp", "jalbumoutputpage_8cpp.html", null ],
    [ "jalbumoutputpage.h", "jalbumoutputpage_8h.html", [
      [ "JAlbumOutputPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumOutputPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumOutputPage" ]
    ] ],
    [ "jalbumselectionpage.cpp", "jalbumselectionpage_8cpp.html", null ],
    [ "jalbumselectionpage.h", "jalbumselectionpage_8h.html", [
      [ "JAlbumSelectionPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumSelectionPage.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumSelectionPage" ]
    ] ],
    [ "jalbumwizard.cpp", "jalbumwizard_8cpp.html", null ],
    [ "jalbumwizard.h", "jalbumwizard_8h.html", [
      [ "JAlbumWizard", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard" ]
    ] ]
];