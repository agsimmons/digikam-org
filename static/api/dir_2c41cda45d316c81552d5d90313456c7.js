var dir_2c41cda45d316c81552d5d90313456c7 =
[
    [ "clockphotodialog.cpp", "clockphotodialog_8cpp.html", null ],
    [ "clockphotodialog.h", "clockphotodialog_8h.html", [
      [ "ClockPhotoDialog", "classDigikam_1_1ClockPhotoDialog.html", "classDigikam_1_1ClockPhotoDialog" ]
    ] ],
    [ "detbyclockphotobutton.cpp", "detbyclockphotobutton_8cpp.html", null ],
    [ "detbyclockphotobutton.h", "detbyclockphotobutton_8h.html", [
      [ "DetByClockPhotoButton", "classDigikam_1_1DetByClockPhotoButton.html", "classDigikam_1_1DetByClockPhotoButton" ]
    ] ],
    [ "timeadjustcontainer.cpp", "timeadjustcontainer_8cpp.html", null ],
    [ "timeadjustcontainer.h", "timeadjustcontainer_8h.html", [
      [ "DeltaTime", "classDigikam_1_1DeltaTime.html", "classDigikam_1_1DeltaTime" ],
      [ "TimeAdjustContainer", "classDigikam_1_1TimeAdjustContainer.html", "classDigikam_1_1TimeAdjustContainer" ]
    ] ],
    [ "timeadjustsettings.cpp", "timeadjustsettings_8cpp.html", null ],
    [ "timeadjustsettings.h", "timeadjustsettings_8h.html", [
      [ "TimeAdjustSettings", "classDigikam_1_1TimeAdjustSettings.html", "classDigikam_1_1TimeAdjustSettings" ]
    ] ]
];