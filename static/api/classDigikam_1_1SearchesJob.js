var classDigikam_1_1SearchesJob =
[
    [ "SearchesJob", "classDigikam_1_1SearchesJob.html#ab9f7cfd94a25f2bd69699f619282a7ff", null ],
    [ "SearchesJob", "classDigikam_1_1SearchesJob.html#a7a7b396ee40a86ff9983f32997e8033d", null ],
    [ "~SearchesJob", "classDigikam_1_1SearchesJob.html#ac23a79ffa54ee6bd05af618220da5a9c", null ],
    [ "cancel", "classDigikam_1_1SearchesJob.html#ad801a5f7a879375239c3acc000cb91a4", null ],
    [ "data", "classDigikam_1_1SearchesJob.html#aa9c661dd96c8344f4ecc540ac443cafa", null ],
    [ "error", "classDigikam_1_1SearchesJob.html#aebd9a69e170257878b31fe24ed58347a", null ],
    [ "isCanceled", "classDigikam_1_1SearchesJob.html#a79c029251260e485b9440b7b1b731ea4", null ],
    [ "run", "classDigikam_1_1SearchesJob.html#ad72da6a7118cf57d39fd3257fd3dbf0a", null ],
    [ "signalDone", "classDigikam_1_1SearchesJob.html#aa34d76e1cd1e839039f1b67add81920d", null ],
    [ "signalDuplicatesResults", "classDigikam_1_1SearchesJob.html#ade1a176ebcc02d699fa1c3088647fbce", null ],
    [ "signalImageProcessed", "classDigikam_1_1SearchesJob.html#ad19b6096cb8622fcc4326ba90651e699", null ],
    [ "signalProgress", "classDigikam_1_1SearchesJob.html#a736bc91a70d7892ae9fe8d1bd76b795e", null ],
    [ "signalStarted", "classDigikam_1_1SearchesJob.html#afed71b5e01ec11f40bcb99c9f24b9048", null ],
    [ "m_cancel", "classDigikam_1_1SearchesJob.html#a7b2f25a3878a3a2bcdde09ef10c4345e", null ]
];