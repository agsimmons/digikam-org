var classDigikam_1_1AntiVignettingContainer =
[
    [ "AntiVignettingContainer", "classDigikam_1_1AntiVignettingContainer.html#ac574fbc5d5892076f1547b0ef6a67a55", null ],
    [ "~AntiVignettingContainer", "classDigikam_1_1AntiVignettingContainer.html#ae4bcbae699699ba538e74ee34bb478db", null ],
    [ "addvignetting", "classDigikam_1_1AntiVignettingContainer.html#acbd0add7ef154af43e3e03ab66982d88", null ],
    [ "density", "classDigikam_1_1AntiVignettingContainer.html#aaf5d9a12979089ef8f3889e5ca25e5b9", null ],
    [ "innerradius", "classDigikam_1_1AntiVignettingContainer.html#a8bd9fb6bab68cd63bd787d8885150144", null ],
    [ "outerradius", "classDigikam_1_1AntiVignettingContainer.html#ad2a92afb5e9338b3fb237ba471ca07c4", null ],
    [ "power", "classDigikam_1_1AntiVignettingContainer.html#a6eccf8bccb954c75533808b9e4d9133d", null ],
    [ "xshift", "classDigikam_1_1AntiVignettingContainer.html#a0f383b304d973d97574db36388aedb44", null ],
    [ "yshift", "classDigikam_1_1AntiVignettingContainer.html#a7d9f70e52c72e98dcb293d826336e1d5", null ]
];