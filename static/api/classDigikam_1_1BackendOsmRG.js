var classDigikam_1_1BackendOsmRG =
[
    [ "BackendOsmRG", "classDigikam_1_1BackendOsmRG.html#ace1cdf9784c188b6c70f936d106fbb3a", null ],
    [ "~BackendOsmRG", "classDigikam_1_1BackendOsmRG.html#a72d370a98faea6e467fdc73e8f11ae91", null ],
    [ "backendName", "classDigikam_1_1BackendOsmRG.html#a93648bd99928d77da85db66f6d599e03", null ],
    [ "callRGBackend", "classDigikam_1_1BackendOsmRG.html#a0a77a76cdfb8be56e44349cb726ecc2e", null ],
    [ "cancelRequests", "classDigikam_1_1BackendOsmRG.html#aacfe5c55fb6045ac087854aaccbfd3a3", null ],
    [ "getErrorMessage", "classDigikam_1_1BackendOsmRG.html#aaf290af878840b0c0f25487a47803509", null ],
    [ "makeQMapFromXML", "classDigikam_1_1BackendOsmRG.html#afec1cff5647ed99aacede0e205254ca3", null ],
    [ "signalRGReady", "classDigikam_1_1BackendOsmRG.html#a6ee2a6e36a160142ffa2fe82b07c4695", null ]
];