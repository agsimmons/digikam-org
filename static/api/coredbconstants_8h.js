var coredbconstants_8h =
[
    [ "ImageTagPropertyName", "classDigikam_1_1ImageTagPropertyName.html", null ],
    [ "InternalTagName", "classDigikam_1_1InternalTagName.html", null ],
    [ "TagPropertyName", "classDigikam_1_1TagPropertyName.html", null ],
    [ "Category", "coredbconstants_8h.html#a2679f0badb24c66eb123f31ad9848c75", [
      [ "UndefinedCategory", "coredbconstants_8h.html#a2679f0badb24c66eb123f31ad9848c75ad5fb2969a84db96921b08437bd4ea5a0", null ],
      [ "Image", "coredbconstants_8h.html#a2679f0badb24c66eb123f31ad9848c75aa03f6ca950533a24f9f036c33c8a36ba", null ],
      [ "Video", "coredbconstants_8h.html#a2679f0badb24c66eb123f31ad9848c75ad6042f98afff5d6d9cd7ad7537c4bf38", null ],
      [ "Audio", "coredbconstants_8h.html#a2679f0badb24c66eb123f31ad9848c75a9f872486ae79747ae24deb152e711fe0", null ],
      [ "Other", "coredbconstants_8h.html#a2679f0badb24c66eb123f31ad9848c75ae678a1e1d0357ad0b7a30117348f23fa", null ]
    ] ],
    [ "HaarSearchType", "coredbconstants_8h.html#a9ebb0146badb263fbe1e494e53b4e414", [
      [ "HaarImageSearch", "coredbconstants_8h.html#a9ebb0146badb263fbe1e494e53b4e414a941c06177f973597acd327f93b23aca0", null ],
      [ "HaarSketchSearch", "coredbconstants_8h.html#a9ebb0146badb263fbe1e494e53b4e414a6ebb28c7e0fb74c96385416a6cd9d713", null ]
    ] ],
    [ "Status", "coredbconstants_8h.html#ae445eb6e5547a090155aca3b18e39b54", [
      [ "UndefinedStatus", "coredbconstants_8h.html#ae445eb6e5547a090155aca3b18e39b54abec78a41957868d52c4201546d23f091", null ],
      [ "Visible", "coredbconstants_8h.html#ae445eb6e5547a090155aca3b18e39b54a2362e95fce5771fcb15d92e130105934", null ],
      [ "Hidden", "coredbconstants_8h.html#ae445eb6e5547a090155aca3b18e39b54aa4a57b60e8ed6742a5d04f2eb4542756", null ],
      [ "Trashed", "coredbconstants_8h.html#ae445eb6e5547a090155aca3b18e39b54a32a08ef9fb64e749ec9ec49d6fc992c8", null ],
      [ "Obsolete", "coredbconstants_8h.html#ae445eb6e5547a090155aca3b18e39b54a8575e10aedef9cca9d23450fdadff927", null ]
    ] ],
    [ "Type", "coredbconstants_8h.html#ad2ec6653a725e0c937f00e2909b6aa22", [
      [ "UndefinedType", "coredbconstants_8h.html#ad2ec6653a725e0c937f00e2909b6aa22a65fc1d1a421c7b29a600362afcd6cf47", null ],
      [ "VolumeHardWired", "coredbconstants_8h.html#ad2ec6653a725e0c937f00e2909b6aa22a8313aad6b2a80bdf0d28f889080bac6e", null ],
      [ "VolumeRemovable", "coredbconstants_8h.html#ad2ec6653a725e0c937f00e2909b6aa22a041cb3acce368b4394c24cae3c03dcda", null ],
      [ "Network", "coredbconstants_8h.html#ad2ec6653a725e0c937f00e2909b6aa22aeabacafa7f4bbcb190d2360079250131", null ]
    ] ],
    [ "Type", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46", [
      [ "UndefinedType", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46a3aab449461d65b552aa2691ade0ee387", null ],
      [ "KeywordSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46a06f95e9bd89416644cb52e807f7ec456", null ],
      [ "AdvancedSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46a3f8ad5e58412808db4a3f4243ce71c3d", null ],
      [ "LegacyUrlSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46a3ffb5cf9edb48472fe6e1c22670dc663", null ],
      [ "TimeLineSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46a9696095a44c91fcf9d7757128b40be42", null ],
      [ "HaarSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46a9c21bae2d38f8e10d2af72aa81eae878", null ],
      [ "MapSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46ae719b5e3b3e0f957d64b2a550f022e55", null ],
      [ "DuplicatesSearch", "coredbconstants_8h.html#ab820f1f91c8c246037cb90571385df46af7ec076920d727b983873facbe125c81", null ]
    ] ],
    [ "Type", "coredbconstants_8h.html#af3da5d9dca889955bef730cad516049a", [
      [ "UndefinedType", "coredbconstants_8h.html#af3da5d9dca889955bef730cad516049aa807dc3c136a78ede653dfe5f85b36155", null ],
      [ "DerivedFrom", "coredbconstants_8h.html#af3da5d9dca889955bef730cad516049aa24e3ac25e57ca811889c162367a3051a", null ],
      [ "Grouped", "coredbconstants_8h.html#af3da5d9dca889955bef730cad516049aac690e7286ad81a9e800cde5cf60cfd12", null ]
    ] ],
    [ "Type", "coredbconstants_8h.html#aee02a01a7085d2aecd07ef06e9084c4c", [
      [ "UndefinedType", "coredbconstants_8h.html#aee02a01a7085d2aecd07ef06e9084c4ca10d3a2462ee768343ac2c134daae71a3", null ],
      [ "Comment", "coredbconstants_8h.html#aee02a01a7085d2aecd07ef06e9084c4ca5f087b03a6ff455a3e863e460df1affe", null ],
      [ "Headline", "coredbconstants_8h.html#aee02a01a7085d2aecd07ef06e9084c4ca1a5196ed4f28a6f34ffbb37f00ee666b", null ],
      [ "Title", "coredbconstants_8h.html#aee02a01a7085d2aecd07ef06e9084c4ca038797ebba089cd5c6fc6c7fa7512ca2", null ]
    ] ]
];