var classDigikam_1_1ThemeManager =
[
    [ "Private", "classDigikam_1_1ThemeManager_1_1Private.html", "classDigikam_1_1ThemeManager_1_1Private" ],
    [ "~ThemeManager", "classDigikam_1_1ThemeManager.html#ac57d403d9dc0ff25ec04e2a9cd63a1b9", null ],
    [ "currentThemeName", "classDigikam_1_1ThemeManager.html#a9847698de18302437acd5805b1f7b15a", null ],
    [ "defaultThemeName", "classDigikam_1_1ThemeManager.html#a7e81bab712c0f6cbd93f9882c0b653a2", null ],
    [ "registerThemeActions", "classDigikam_1_1ThemeManager.html#afd01115297e3be1318549b8eeaecdbeb", null ],
    [ "setCurrentTheme", "classDigikam_1_1ThemeManager.html#a4fd01c92f212f06cc5318f99425a432b", null ],
    [ "setThemeMenuAction", "classDigikam_1_1ThemeManager.html#abc05c1cefb7ae7fad69d135ab1603fd8", null ],
    [ "signalThemeChanged", "classDigikam_1_1ThemeManager.html#ae4270230262fad48adc31b3bda0b9537", null ],
    [ "updateThemeMenu", "classDigikam_1_1ThemeManager.html#ac42c700987a503da26f85dfbd6711bd4", null ],
    [ "ThemeManagerCreator", "classDigikam_1_1ThemeManager.html#a9e7573212c08665c099ddf460cb1bd34", null ]
];