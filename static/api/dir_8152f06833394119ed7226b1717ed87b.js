var dir_8152f06833394119ed7226b1717ed87b =
[
    [ "imagewindow.cpp", "imagewindow_8cpp.html", null ],
    [ "imagewindow.h", "imagewindow_8h.html", [
      [ "ImageWindow", "classDigikam_1_1ImageWindow.html", "classDigikam_1_1ImageWindow" ]
    ] ],
    [ "imagewindow_config.cpp", "imagewindow__config_8cpp.html", null ],
    [ "imagewindow_import.cpp", "imagewindow__import_8cpp.html", null ],
    [ "imagewindow_p.h", "imagewindow__p_8h.html", [
      [ "DatabaseVersionManager", "classDigikam_1_1DatabaseVersionManager.html", "classDigikam_1_1DatabaseVersionManager" ],
      [ "Private", "classDigikam_1_1ImageWindow_1_1Private.html", "classDigikam_1_1ImageWindow_1_1Private" ]
    ] ],
    [ "imagewindow_setup.cpp", "imagewindow__setup_8cpp.html", null ]
];