var structDigikam_1_1PTOType_1_1Project =
[
    [ "FileFormat", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html", "structDigikam_1_1PTOType_1_1Project_1_1FileFormat" ],
    [ "BitDepth", "structDigikam_1_1PTOType_1_1Project.html#a8a7ac0658dc61b54cde149e9b3dcf324", [
      [ "UINT8", "structDigikam_1_1PTOType_1_1Project.html#a8a7ac0658dc61b54cde149e9b3dcf324a315690674794664c2a7a5e6c3e777a66", null ],
      [ "UINT16", "structDigikam_1_1PTOType_1_1Project.html#a8a7ac0658dc61b54cde149e9b3dcf324aa9e1ac6aa164ba2d89858a0fa767cca1", null ],
      [ "FLOAT", "structDigikam_1_1PTOType_1_1Project.html#a8a7ac0658dc61b54cde149e9b3dcf324a08e39838836a0dc912ce89b865084d2a", null ]
    ] ],
    [ "ProjectionType", "structDigikam_1_1PTOType_1_1Project.html#a2d2e501daa2ec6f21d12a3c46aa1a815", [
      [ "RECTILINEAR", "structDigikam_1_1PTOType_1_1Project.html#a2d2e501daa2ec6f21d12a3c46aa1a815a4fedfbad0355f10b716621781092915b", null ],
      [ "CYLINDRICAL", "structDigikam_1_1PTOType_1_1Project.html#a2d2e501daa2ec6f21d12a3c46aa1a815aeff18d97cf95c7331f280824db06b43b", null ],
      [ "EQUIRECTANGULAR", "structDigikam_1_1PTOType_1_1Project.html#a2d2e501daa2ec6f21d12a3c46aa1a815a218f90022823fcaeea0b737528d229a4", null ],
      [ "FULLFRAMEFISHEYE", "structDigikam_1_1PTOType_1_1Project.html#a2d2e501daa2ec6f21d12a3c46aa1a815ad493ff4f57cccdd3af6b1f68889a0c4b", null ]
    ] ],
    [ "Project", "structDigikam_1_1PTOType_1_1Project.html#aacab5791d7f9f6c5624483fe2f1135d5", null ],
    [ "bitDepth", "structDigikam_1_1PTOType_1_1Project.html#ae6db0ea8e205f9b8388bdb9b73e47999", null ],
    [ "crop", "structDigikam_1_1PTOType_1_1Project.html#aefdf2d59a3faf6d16eb26d6142aa5fbe", null ],
    [ "exposure", "structDigikam_1_1PTOType_1_1Project.html#ad2e8ecda126c13d36624f0db85379ddb", null ],
    [ "fieldOfView", "structDigikam_1_1PTOType_1_1Project.html#a4552d14e633a363abf81e855b4e32e82", null ],
    [ "fileFormat", "structDigikam_1_1PTOType_1_1Project.html#a5f32aa7c12b1d578fdf38b9cd821c286", null ],
    [ "hdr", "structDigikam_1_1PTOType_1_1Project.html#a9e3e2b805b8260ae8f3c608ba31adcfe", null ],
    [ "photometricReferenceId", "structDigikam_1_1PTOType_1_1Project.html#a0c444cd21dff9d05fc9ff6e0db6ef2ba", null ],
    [ "previousComments", "structDigikam_1_1PTOType_1_1Project.html#aa3e817312e56e5d35e7cc55cb28164a1", null ],
    [ "projection", "structDigikam_1_1PTOType_1_1Project.html#a2cf8b26d2292461a7743564ab71a52e7", null ],
    [ "size", "structDigikam_1_1PTOType_1_1Project.html#a56fb5742f5138da8d8395e8097fc189e", null ],
    [ "unmatchedParameters", "structDigikam_1_1PTOType_1_1Project.html#aed3864689eac0209d31ff0d5375c9370", null ]
];