var classDigikam_1_1PNGSettings =
[
    [ "PNGSettings", "classDigikam_1_1PNGSettings.html#a51e68a04dc0cb9ed8bf3c8ed09707b6b", null ],
    [ "~PNGSettings", "classDigikam_1_1PNGSettings.html#a930fdf0b6be2b775513e046559feb437", null ],
    [ "getCompressionValue", "classDigikam_1_1PNGSettings.html#a075a1bb3da111cfca7fc1c66b4dde124", null ],
    [ "setCompressionValue", "classDigikam_1_1PNGSettings.html#add1c5947b41cf434a1945e2f7cbe57ed", null ],
    [ "signalSettingsChanged", "classDigikam_1_1PNGSettings.html#a8e671aba6ceaa4006eca934e2d8ca67b", null ]
];