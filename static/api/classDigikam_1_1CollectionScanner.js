var classDigikam_1_1CollectionScanner =
[
    [ "Private", "classDigikam_1_1CollectionScanner_1_1Private.html", "classDigikam_1_1CollectionScanner_1_1Private" ],
    [ "FileScanMode", "classDigikam_1_1CollectionScanner.html#aae0c38fcfcaa77b8b988f22eb8d7b4e3", [
      [ "NormalScan", "classDigikam_1_1CollectionScanner.html#aae0c38fcfcaa77b8b988f22eb8d7b4e3a517917e89bc8f8a3686145cc2fc576ef", null ],
      [ "ModifiedScan", "classDigikam_1_1CollectionScanner.html#aae0c38fcfcaa77b8b988f22eb8d7b4e3a02f00a22c46c252a6ec9832d4708f9ca", null ],
      [ "Rescan", "classDigikam_1_1CollectionScanner.html#aae0c38fcfcaa77b8b988f22eb8d7b4e3a7ee3fc7bf0116366a86eb39c2895ac94", null ]
    ] ],
    [ "CollectionScanner", "classDigikam_1_1CollectionScanner.html#a721aa2c82984c16d1f1ab64bf85eed61", null ],
    [ "~CollectionScanner", "classDigikam_1_1CollectionScanner.html#ad429751e32d668ef2d20ca2dbdb526d9", null ],
    [ "cancelled", "classDigikam_1_1CollectionScanner.html#a8ef9bb95ad3c63113b4a00fc1e5ea5d6", null ],
    [ "category", "classDigikam_1_1CollectionScanner.html#aac4d9f60b1a7086714201cd01d29296f", null ],
    [ "checkAlbum", "classDigikam_1_1CollectionScanner.html#ae5cbfe25b707efb7024100dc837af083", null ],
    [ "checkDeleteRemoved", "classDigikam_1_1CollectionScanner.html#aaa8bdab14d180662908cc5f6cc051e06", null ],
    [ "completeHistoryScanning", "classDigikam_1_1CollectionScanner.html#a3357d74ebf3bdc46ca56a908ba4bac29", null ],
    [ "completeScan", "classDigikam_1_1CollectionScanner.html#a5dc317724a84687bd78a482fc7e64024", null ],
    [ "completeScanCleanupPart", "classDigikam_1_1CollectionScanner.html#a19a1a3cae49d2a9c2b844df747f17aab", null ],
    [ "countItemsInFolder", "classDigikam_1_1CollectionScanner.html#acc3ef76aebbf023b9163ae2ac94d63be", null ],
    [ "deferredAlbumPaths", "classDigikam_1_1CollectionScanner.html#af64ffc5d6ae68753553be028f6f3cef6", null ],
    [ "finishCompleteScan", "classDigikam_1_1CollectionScanner.html#a4c71144cbafa03a7d04e1ce5a3911fe9", null ],
    [ "finishedCompleteScan", "classDigikam_1_1CollectionScanner.html#a34d1e467150221b2d2d9135cb992e2bb", null ],
    [ "finishedScanningAlbum", "classDigikam_1_1CollectionScanner.html#af3c33301ae737141d4f0f50d6b69352e", null ],
    [ "finishedScanningAlbumRoot", "classDigikam_1_1CollectionScanner.html#a2dcb7cd35216dc60e316dac0e4b15a88", null ],
    [ "finishedScanningForStaleAlbums", "classDigikam_1_1CollectionScanner.html#ad9f2748a2ced337504490c63a075972c", null ],
    [ "finishHistoryScanning", "classDigikam_1_1CollectionScanner.html#ad33f8da9d4779a5b122b39d27baec0e5", null ],
    [ "getNewIdsList", "classDigikam_1_1CollectionScanner.html#a4cf8ebd87e6280a2308fb5fbd72c6040", null ],
    [ "historyScanningStage2", "classDigikam_1_1CollectionScanner.html#a6aff90d93a588934b852d23754995b50", null ],
    [ "historyScanningStage3", "classDigikam_1_1CollectionScanner.html#ada622830543d667cc240a39fc83636a4", null ],
    [ "incrementDeleteRemovedCompleteScanCount", "classDigikam_1_1CollectionScanner.html#a1e14e827b62e9ee63934e37f3ce447d6", null ],
    [ "itemsWereRemoved", "classDigikam_1_1CollectionScanner.html#a7a4c6af7f2dc63c30200080bb8b6495c", null ],
    [ "loadNameFilters", "classDigikam_1_1CollectionScanner.html#a9686cbc335ca54da038d85bce46f7303", null ],
    [ "mainEntryPoint", "classDigikam_1_1CollectionScanner.html#a0f935f3e07347e9f86b68978b7525877", null ],
    [ "markDatabaseAsScanned", "classDigikam_1_1CollectionScanner.html#a3d691724afce81df704ae344ecedd3e9", null ],
    [ "partialScan", "classDigikam_1_1CollectionScanner.html#ae651bbf9a348c97f74cfc921117f3109", null ],
    [ "partialScan", "classDigikam_1_1CollectionScanner.html#ab78556ca2781ec4b0ca3dc62c44edc06", null ],
    [ "rescanFile", "classDigikam_1_1CollectionScanner.html#ad54924871d39817025b8f6dbf87db611", null ],
    [ "resetDeleteRemovedSettings", "classDigikam_1_1CollectionScanner.html#ab3f87d1bc46ef62985941f63c518d63a", null ],
    [ "safelyRemoveAlbums", "classDigikam_1_1CollectionScanner.html#a4a39cf61973bce9bf4411137e653004e", null ],
    [ "scanAlbum", "classDigikam_1_1CollectionScanner.html#a18f967db44a515a355a48efdfdeb6802", null ],
    [ "scanAlbumRoot", "classDigikam_1_1CollectionScanner.html#a65011fc4b9e17be1d05a0b1d803d9618", null ],
    [ "scanExistingFile", "classDigikam_1_1CollectionScanner.html#a22bd30b0c32d5e67de267c630634695c", null ],
    [ "scanFile", "classDigikam_1_1CollectionScanner.html#a714718eb53f7d34392f6cf0495ef956d", null ],
    [ "scanFile", "classDigikam_1_1CollectionScanner.html#ad3164ebde876c0379018ebba7d8adb2b", null ],
    [ "scanFile", "classDigikam_1_1CollectionScanner.html#a4c711169450ffa70ce249d0da9b08fcd", null ],
    [ "scanFile", "classDigikam_1_1CollectionScanner.html#afab920f74f754a8cc75c6a3f71f2b894", null ],
    [ "scanFileNormal", "classDigikam_1_1CollectionScanner.html#a980c3b3c12180ab8026bdd605b5a4b6c", null ],
    [ "scanFileUpdateHashReuseThumbnail", "classDigikam_1_1CollectionScanner.html#af2f3dc037c68776ee9eef880e8ed1c58", null ],
    [ "scanForStaleAlbums", "classDigikam_1_1CollectionScanner.html#ab098d048e272fe99d4f3aaa58ced593e", null ],
    [ "scanForStaleAlbums", "classDigikam_1_1CollectionScanner.html#ad6b74cf2d8597566b2d6158abf717c9a", null ],
    [ "scanModifiedFile", "classDigikam_1_1CollectionScanner.html#a314fe97282bcbc6b6d6daaa54ddc3803", null ],
    [ "scannedFiles", "classDigikam_1_1CollectionScanner.html#a1c1fa5e377dd5c6efc349d88d76dbdc1", null ],
    [ "scanNewFile", "classDigikam_1_1CollectionScanner.html#ae5f82091a89c01943b4d6583b89ae444", null ],
    [ "scanNewFileFullScan", "classDigikam_1_1CollectionScanner.html#a099852dc0d1a575ab42849a0f8605eba", null ],
    [ "setDeferredFileScanning", "classDigikam_1_1CollectionScanner.html#a560d4459076f6815e76215ca8b65ad57", null ],
    [ "setHintContainer", "classDigikam_1_1CollectionScanner.html#a2772c9bd061ec2ae26c7a06666421fe8", null ],
    [ "setNeedFileCount", "classDigikam_1_1CollectionScanner.html#a2a5d13f6e433fb4d3eec9a0f575c5f67", null ],
    [ "setObserver", "classDigikam_1_1CollectionScanner.html#a5e8d1abb420735b6cbcd5d06b0f59ac5", null ],
    [ "setPerformFastScan", "classDigikam_1_1CollectionScanner.html#a35fed0854a02bb4d42b9824ced9238af", null ],
    [ "setSignalsEnabled", "classDigikam_1_1CollectionScanner.html#ad2d2f7a88f77b9422e302873f1634c71", null ],
    [ "setUpdateHashHint", "classDigikam_1_1CollectionScanner.html#a2754f19d06d744a6d21805e6d793b28f", null ],
    [ "signalScannedNewImage", "classDigikam_1_1CollectionScanner.html#a32a2ba76d7fd06b01886ca270696382f", null ],
    [ "startCompleteScan", "classDigikam_1_1CollectionScanner.html#ac30409f45e508b48ebb038f45789fd54", null ],
    [ "startScanningAlbum", "classDigikam_1_1CollectionScanner.html#a95784557ebcfb3540bf12178e05a85d9", null ],
    [ "startScanningAlbumRoot", "classDigikam_1_1CollectionScanner.html#acc9cbb4f7def99f844d50b430c85c94a", null ],
    [ "startScanningAlbumRoots", "classDigikam_1_1CollectionScanner.html#a080835543fe6b590f77eb5eb451dc732", null ],
    [ "startScanningForStaleAlbums", "classDigikam_1_1CollectionScanner.html#a37d70983e65f6a00330bd8845d060f07", null ],
    [ "totalFilesToScan", "classDigikam_1_1CollectionScanner.html#a514494b884f0d61575daf90e2240dd7e", null ],
    [ "updateRemovedItemsTime", "classDigikam_1_1CollectionScanner.html#a4ddaacc69405f8c7b215b0c2d1ca151c", null ]
];