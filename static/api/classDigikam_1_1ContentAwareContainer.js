var classDigikam_1_1ContentAwareContainer =
[
    [ "EnergyFunction", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32", [
      [ "GradientNorm", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a036e151a985de3bc3c69d8dea1c7e2d9", null ],
      [ "SumOfAbsoluteValues", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a3a8bcf3ca28a1c84eba918175e5e6730", null ],
      [ "XAbsoluteValue", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32afbfd071bb4e61510d936a9db8e7d8d1c", null ],
      [ "LumaGradientNorm", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a45af206be2bffcfa935f0f890c876b99", null ],
      [ "LumaSumOfAbsoluteValues", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a5491d1392abbb3dd3af036b404862bf4", null ],
      [ "LumaXAbsoluteValue", "classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a26744f7a4f8ffd448ba9fc0907ca93c9", null ]
    ] ],
    [ "ContentAwareContainer", "classDigikam_1_1ContentAwareContainer.html#af85a892ea4e12a62d02e137af917ea52", null ],
    [ "~ContentAwareContainer", "classDigikam_1_1ContentAwareContainer.html#ae4a1ddf5af8c96df80f1346fab726f61", null ],
    [ "func", "classDigikam_1_1ContentAwareContainer.html#a6ae441d03e405bc0c8b63c359a9cd0dc", null ],
    [ "height", "classDigikam_1_1ContentAwareContainer.html#a9474300e7292e7b401c388a1afbd7890", null ],
    [ "mask", "classDigikam_1_1ContentAwareContainer.html#a66df236d4d31da12e109563ab1ce952d", null ],
    [ "preserve_skin_tones", "classDigikam_1_1ContentAwareContainer.html#a9b66d733ccabee3a4f07c58de83909c5", null ],
    [ "resize_order", "classDigikam_1_1ContentAwareContainer.html#a407bb6e297bd33bf724cfb50fd51d00e", null ],
    [ "rigidity", "classDigikam_1_1ContentAwareContainer.html#a664c2f8cc7a67213671540d7782ef49d", null ],
    [ "side_switch_freq", "classDigikam_1_1ContentAwareContainer.html#aa15ab4bb3d69f55cd31e9cd2730d6b83", null ],
    [ "step", "classDigikam_1_1ContentAwareContainer.html#a69241705e33eca2ce4e333f15f28cc5e", null ],
    [ "width", "classDigikam_1_1ContentAwareContainer.html#ac37e4dd658d9c3225bfba7886922afb4", null ]
];