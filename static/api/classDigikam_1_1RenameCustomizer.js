var classDigikam_1_1RenameCustomizer =
[
    [ "Case", "classDigikam_1_1RenameCustomizer.html#aebed55b5b897778e0ce127a4799e65b7", [
      [ "NONE", "classDigikam_1_1RenameCustomizer.html#aebed55b5b897778e0ce127a4799e65b7a37ecd45c80d3fd33c593367fda987019", null ],
      [ "UPPER", "classDigikam_1_1RenameCustomizer.html#aebed55b5b897778e0ce127a4799e65b7af23fdd0ef20dfab8763bb322e825e07d", null ],
      [ "LOWER", "classDigikam_1_1RenameCustomizer.html#aebed55b5b897778e0ce127a4799e65b7afd10d27e1eff0cefad7a188230761329", null ]
    ] ],
    [ "RenameCustomizer", "classDigikam_1_1RenameCustomizer.html#acfbefba2d727255f8b9329432bd19be9", null ],
    [ "~RenameCustomizer", "classDigikam_1_1RenameCustomizer.html#abd6e800a56f0b49287113760d3422436", null ],
    [ "changeCase", "classDigikam_1_1RenameCustomizer.html#adeb5637bda161de28daa27ad9b99b23a", null ],
    [ "newName", "classDigikam_1_1RenameCustomizer.html#a09395501b226e3cba7a677d7f6ae2fe3", null ],
    [ "renameManager", "classDigikam_1_1RenameCustomizer.html#aa03f15107eb18068d9ca149189b1646a", null ],
    [ "reset", "classDigikam_1_1RenameCustomizer.html#a5416923c37563c223ce1dade429b8152", null ],
    [ "setChangeCase", "classDigikam_1_1RenameCustomizer.html#aa5347159da06fc9220f92c99ea68ef4a", null ],
    [ "setStartIndex", "classDigikam_1_1RenameCustomizer.html#a1eac8225f50073051bb0c9cce5a2b395", null ],
    [ "setUseDefault", "classDigikam_1_1RenameCustomizer.html#a5e02e15a27a9b27f2b5949f3f3155166", null ],
    [ "signalChanged", "classDigikam_1_1RenameCustomizer.html#a417fe980a76dd50a1d03941ff5650c26", null ],
    [ "startIndex", "classDigikam_1_1RenameCustomizer.html#abe337687cd78c61bff56d0293c78702e", null ],
    [ "useDefault", "classDigikam_1_1RenameCustomizer.html#a6e4c9c5ca231e2017a9672c80ead9f6f", null ]
];