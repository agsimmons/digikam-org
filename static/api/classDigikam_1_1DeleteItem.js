var classDigikam_1_1DeleteItem =
[
    [ "DeleteItem", "classDigikam_1_1DeleteItem.html#a5f935eed4d28631e19fd0e86970e2b75", null ],
    [ "~DeleteItem", "classDigikam_1_1DeleteItem.html#a35f6dfbee5751236311c0ccc9dc2f4f9", null ],
    [ "fileUrl", "classDigikam_1_1DeleteItem.html#af75a88ad195f0e64e2a1700d04341ebb", null ],
    [ "hasValidThumbnail", "classDigikam_1_1DeleteItem.html#aa2a9788141d0e1cdf8bd34f7e07275f7", null ],
    [ "setThumb", "classDigikam_1_1DeleteItem.html#af408bd34dd1ed698c0d4a7028c5db443", null ],
    [ "url", "classDigikam_1_1DeleteItem.html#a268f5e59223d045d4e0ee2d493508510", null ]
];