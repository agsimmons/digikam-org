var classDigikam_1_1LoadSaveTask =
[
    [ "TaskType", "classDigikam_1_1LoadSaveTask.html#a31124b0a1702f3922b442b8efecd4490", [
      [ "TaskTypeLoading", "classDigikam_1_1LoadSaveTask.html#a31124b0a1702f3922b442b8efecd4490a36456327a8b7bf2e3cd7dee8ddfc949c", null ],
      [ "TaskTypeSaving", "classDigikam_1_1LoadSaveTask.html#a31124b0a1702f3922b442b8efecd4490a4e4323e60ccc3a9c630058150c411b3b", null ]
    ] ],
    [ "LoadSaveTask", "classDigikam_1_1LoadSaveTask.html#acbdeedac61bd171264bb13b9654ca67d", null ],
    [ "~LoadSaveTask", "classDigikam_1_1LoadSaveTask.html#a42155617fe97b660370e8e75340290a7", null ],
    [ "continueQuery", "classDigikam_1_1LoadSaveTask.html#a016c2d0a84059d0a719e14e7e1165157", null ],
    [ "execute", "classDigikam_1_1LoadSaveTask.html#af5f621afea3dacd2941154ff9df8e6a0", null ],
    [ "progressInfo", "classDigikam_1_1LoadSaveTask.html#a502508497577d08842c266720f1c2f5f", null ],
    [ "type", "classDigikam_1_1LoadSaveTask.html#ae15afbd0011ee301fc4230ba16477ae0", null ],
    [ "m_thread", "classDigikam_1_1LoadSaveTask.html#abfe53f7642fca9a8e3ed0f7d5438ae7d", null ]
];