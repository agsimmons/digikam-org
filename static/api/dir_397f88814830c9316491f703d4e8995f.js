var dir_397f88814830c9316491f703d4e8995f =
[
    [ "backend", "dir_ac560158bc20145dbdf5c707853cc397.html", "dir_ac560158bc20145dbdf5c707853cc397" ],
    [ "vkalbumchooser.cpp", "vkalbumchooser_8cpp.html", null ],
    [ "vkalbumchooser.h", "vkalbumchooser_8h.html", [
      [ "VKAlbumChooser", "classDigikamGenericVKontaktePlugin_1_1VKAlbumChooser.html", "classDigikamGenericVKontaktePlugin_1_1VKAlbumChooser" ]
    ] ],
    [ "vkauthwidget.cpp", "vkauthwidget_8cpp.html", null ],
    [ "vkauthwidget.h", "vkauthwidget_8h.html", [
      [ "VKAuthWidget", "classDigikamGenericVKontaktePlugin_1_1VKAuthWidget.html", "classDigikamGenericVKontaktePlugin_1_1VKAuthWidget" ]
    ] ],
    [ "vknewalbumdlg.cpp", "vknewalbumdlg_8cpp.html", null ],
    [ "vknewalbumdlg.h", "vknewalbumdlg_8h.html", [
      [ "VKNewAlbumDlg", "classDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg.html", "classDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg" ],
      [ "AlbumProperties", "structDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg_1_1AlbumProperties.html", "structDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg_1_1AlbumProperties" ]
    ] ],
    [ "vkplugin.cpp", "vkplugin_8cpp.html", null ],
    [ "vkplugin.h", "vkplugin_8h.html", "vkplugin_8h" ],
    [ "vkwindow.cpp", "vkwindow_8cpp.html", null ],
    [ "vkwindow.h", "vkwindow_8h.html", [
      [ "VKWindow", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html", "classDigikamGenericVKontaktePlugin_1_1VKWindow" ]
    ] ]
];