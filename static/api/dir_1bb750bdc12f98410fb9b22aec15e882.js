var dir_1bb750bdc12f98410fb9b22aec15e882 =
[
    [ "autocrop.cpp", "autocrop_8cpp.html", null ],
    [ "autocrop.h", "autocrop_8h.html", [
      [ "AutoCrop", "classDigikam_1_1AutoCrop.html", "classDigikam_1_1AutoCrop" ]
    ] ],
    [ "contentawarefilter.cpp", "contentawarefilter_8cpp.html", "contentawarefilter_8cpp" ],
    [ "contentawarefilter.h", "contentawarefilter_8h.html", [
      [ "ContentAwareContainer", "classDigikam_1_1ContentAwareContainer.html", "classDigikam_1_1ContentAwareContainer" ],
      [ "ContentAwareFilter", "classDigikam_1_1ContentAwareFilter.html", "classDigikam_1_1ContentAwareFilter" ]
    ] ],
    [ "freerotationfilter.cpp", "freerotationfilter_8cpp.html", null ],
    [ "freerotationfilter.h", "freerotationfilter_8h.html", [
      [ "FreeRotationContainer", "classDigikam_1_1FreeRotationContainer.html", "classDigikam_1_1FreeRotationContainer" ],
      [ "FreeRotationFilter", "classDigikam_1_1FreeRotationFilter.html", "classDigikam_1_1FreeRotationFilter" ]
    ] ],
    [ "freerotationsettings.cpp", "freerotationsettings_8cpp.html", null ],
    [ "freerotationsettings.h", "freerotationsettings_8h.html", [
      [ "FreeRotationSettings", "classDigikam_1_1FreeRotationSettings.html", "classDigikam_1_1FreeRotationSettings" ]
    ] ],
    [ "shearfilter.cpp", "shearfilter_8cpp.html", null ],
    [ "shearfilter.h", "shearfilter_8h.html", [
      [ "ShearFilter", "classDigikam_1_1ShearFilter.html", "classDigikam_1_1ShearFilter" ]
    ] ]
];