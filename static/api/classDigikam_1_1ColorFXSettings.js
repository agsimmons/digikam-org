var classDigikam_1_1ColorFXSettings =
[
    [ "ColorFXSettings", "classDigikam_1_1ColorFXSettings.html#a14d9b3c26a074366a7a7d5612ffd71e1", null ],
    [ "~ColorFXSettings", "classDigikam_1_1ColorFXSettings.html#a9fc2b3b2f4e162cf2772a6f853d172bd", null ],
    [ "defaultSettings", "classDigikam_1_1ColorFXSettings.html#ae19d2c9d6b712998c29c0ba58a779dde", null ],
    [ "readSettings", "classDigikam_1_1ColorFXSettings.html#a0c3dc532562c36a389e2e5da4c6bfcca", null ],
    [ "resetToDefault", "classDigikam_1_1ColorFXSettings.html#a9cb2f953af0c9b3cd3c1fa1bb8c41a14", null ],
    [ "setSettings", "classDigikam_1_1ColorFXSettings.html#a15b831bd52934af47619084c32142437", null ],
    [ "settings", "classDigikam_1_1ColorFXSettings.html#a9fd998d1e2371cb09937b45b27e17881", null ],
    [ "signalSettingsChanged", "classDigikam_1_1ColorFXSettings.html#af5f27ac10e57f49d779cdea6cd507e86", null ],
    [ "startPreviewFilters", "classDigikam_1_1ColorFXSettings.html#ad0bf0a9ab4d503c130abc831efd264b6", null ],
    [ "writeSettings", "classDigikam_1_1ColorFXSettings.html#a07f8afb57358e1125ca7e19616b0c1e4", null ]
];