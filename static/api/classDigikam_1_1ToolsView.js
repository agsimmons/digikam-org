var classDigikam_1_1ToolsView =
[
    [ "ViewTabs", "classDigikam_1_1ToolsView.html#a4c83be9c87ac5455aff2e54a59901d14", [
      [ "TOOLS", "classDigikam_1_1ToolsView.html#a4c83be9c87ac5455aff2e54a59901d14a2f8d3961c2fd92dea9375fe50d3d09b6", null ],
      [ "WORKFLOW", "classDigikam_1_1ToolsView.html#a4c83be9c87ac5455aff2e54a59901d14ac6706277b1a2aaf16dd2bbe5dfcfbbee", null ],
      [ "HISTORY", "classDigikam_1_1ToolsView.html#a4c83be9c87ac5455aff2e54a59901d14a945a597c138f843214e0d8d55a638419", null ]
    ] ],
    [ "ToolsView", "classDigikam_1_1ToolsView.html#a5c3e71f376f5ec02cd4a44e4906cd16b", null ],
    [ "~ToolsView", "classDigikam_1_1ToolsView.html#a36af37ced5b3e3aeb479e7ab8bde66ed", null ],
    [ "addHistoryEntry", "classDigikam_1_1ToolsView.html#a90562080876b3603183e0fd1f892ce67", null ],
    [ "addTool", "classDigikam_1_1ToolsView.html#a4f005a7313bc65b9d4ea7073926301e4", null ],
    [ "removeTool", "classDigikam_1_1ToolsView.html#a18b9b055a94eca47e3772f1fb8461e2b", null ],
    [ "setBusy", "classDigikam_1_1ToolsView.html#a4c9ee1f6fe133e66ee6c98c47aafcc4a", null ],
    [ "showTab", "classDigikam_1_1ToolsView.html#a8f08433fb2d54af67fe047b7e6e70026", null ],
    [ "signalAssignQueueSettings", "classDigikam_1_1ToolsView.html#ad74a08b9c5685332af7530f2c9ed6e5b", null ],
    [ "signalAssignTools", "classDigikam_1_1ToolsView.html#ae654257020e91062537b234f6e32c727", null ],
    [ "signalHistoryEntryClicked", "classDigikam_1_1ToolsView.html#a8cdcb7cd2d95ef2cd750a5708987bad5", null ]
];