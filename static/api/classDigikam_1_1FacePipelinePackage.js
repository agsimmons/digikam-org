var classDigikam_1_1FacePipelinePackage =
[
    [ "ProcessFlag", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5a", [
      [ "NotProcessed", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5aaeaddd710d3464d76278652ff0831856d", null ],
      [ "PreviewImageLoaded", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5aabbee89ffd0dbb303103c27d79823fa5f", null ],
      [ "ProcessedByDetector", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5aa2ba7c65c014177e4c50ad94ab95ab800", null ],
      [ "ProcessedByRecognizer", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5aab1b1ed0095a93f1773fc2fa42a787a42", null ],
      [ "WrittenToDatabase", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5aa86de2d83e70075cfd08c9063592e2d95", null ],
      [ "ProcessedByTrainer", "classDigikam_1_1FacePipelinePackage.html#a1b058169ac7941d8b80dfdcca0caca5aaa6ecb7fdbf8cac810dba75fde9791886", null ]
    ] ],
    [ "FacePipelinePackage", "classDigikam_1_1FacePipelinePackage.html#af06221aff416669f07417e6b6a0ecdfc", null ],
    [ "~FacePipelinePackage", "classDigikam_1_1FacePipelinePackage.html#ad721a6bcdba230e6a63f4b4e6d10ef29", null ],
    [ "databaseFaces", "classDigikam_1_1FacePipelinePackage.html#a4c3a1cd29456b71a8175863121bf458a", null ],
    [ "detectedFaces", "classDigikam_1_1FacePipelinePackage.html#ad66bafca8f14635bacb979e88b31f1a9", null ],
    [ "image", "classDigikam_1_1FacePipelinePackage.html#a2c2ed058b8b28ca23dd783ea2f5c46d5", null ],
    [ "info", "classDigikam_1_1FacePipelinePackage.html#ab8b7c732bb7e43d4de79d344b70898fe", null ],
    [ "processFlags", "classDigikam_1_1FacePipelinePackage.html#aa4fcdc0f242a1f895ba73960a80479a1", null ],
    [ "recognitionResults", "classDigikam_1_1FacePipelinePackage.html#a32e8cf8b60ae6f43363a06cc100888e6", null ]
];