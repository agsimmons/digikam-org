var dir_71150768acebb4afd51b6f96caaa88ad =
[
    [ "dnotificationpopup.cpp", "dnotificationpopup_8cpp.html", null ],
    [ "dnotificationpopup.h", "dnotificationpopup_8h.html", [
      [ "DNotificationPopup", "classDigikam_1_1DNotificationPopup.html", "classDigikam_1_1DNotificationPopup" ]
    ] ],
    [ "dnotificationwidget.cpp", "dnotificationwidget_8cpp.html", null ],
    [ "dnotificationwidget.h", "dnotificationwidget_8h.html", [
      [ "DNotificationWidget", "classDigikam_1_1DNotificationWidget.html", "classDigikam_1_1DNotificationWidget" ]
    ] ],
    [ "dnotificationwidget_p.cpp", "dnotificationwidget__p_8cpp.html", null ],
    [ "dnotificationwidget_p.h", "dnotificationwidget__p_8h.html", [
      [ "Private", "classDigikam_1_1DNotificationWidget_1_1Private.html", "classDigikam_1_1DNotificationWidget_1_1Private" ]
    ] ],
    [ "dnotificationwrapper.cpp", "dnotificationwrapper_8cpp.html", "dnotificationwrapper_8cpp" ],
    [ "dnotificationwrapper.h", "dnotificationwrapper_8h.html", "dnotificationwrapper_8h" ]
];