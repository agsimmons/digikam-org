var dir_ed298dcb0446a542e1f7617f5410ad00 =
[
    [ "blurfilter.cpp", "blurfilter_8cpp.html", null ],
    [ "blurfilter.h", "blurfilter_8h.html", [
      [ "BlurFilter", "classDigikam_1_1BlurFilter.html", "classDigikam_1_1BlurFilter" ]
    ] ],
    [ "blurfxfilter.cpp", "blurfxfilter_8cpp.html", "blurfxfilter_8cpp" ],
    [ "blurfxfilter.h", "blurfxfilter_8h.html", [
      [ "BlurFXFilter", "classDigikam_1_1BlurFXFilter.html", "classDigikam_1_1BlurFXFilter" ]
    ] ],
    [ "charcoalfilter.cpp", "charcoalfilter_8cpp.html", "charcoalfilter_8cpp" ],
    [ "charcoalfilter.h", "charcoalfilter_8h.html", [
      [ "CharcoalFilter", "classDigikam_1_1CharcoalFilter.html", "classDigikam_1_1CharcoalFilter" ]
    ] ],
    [ "colorfxfilter.cpp", "colorfxfilter_8cpp.html", "colorfxfilter_8cpp" ],
    [ "colorfxfilter.h", "colorfxfilter_8h.html", [
      [ "ColorFXContainer", "classDigikam_1_1ColorFXContainer.html", "classDigikam_1_1ColorFXContainer" ],
      [ "ColorFXFilter", "classDigikam_1_1ColorFXFilter.html", "classDigikam_1_1ColorFXFilter" ]
    ] ],
    [ "colorfxsettings.cpp", "colorfxsettings_8cpp.html", null ],
    [ "colorfxsettings.h", "colorfxsettings_8h.html", [
      [ "ColorFXSettings", "classDigikam_1_1ColorFXSettings.html", "classDigikam_1_1ColorFXSettings" ]
    ] ],
    [ "distortionfxfilter.cpp", "distortionfxfilter_8cpp.html", "distortionfxfilter_8cpp" ],
    [ "distortionfxfilter.h", "distortionfxfilter_8h.html", [
      [ "DistortionFXFilter", "classDigikam_1_1DistortionFXFilter.html", "classDigikam_1_1DistortionFXFilter" ]
    ] ],
    [ "embossfilter.cpp", "embossfilter_8cpp.html", null ],
    [ "embossfilter.h", "embossfilter_8h.html", [
      [ "EmbossFilter", "classDigikam_1_1EmbossFilter.html", "classDigikam_1_1EmbossFilter" ]
    ] ],
    [ "filmgrainfilter.cpp", "filmgrainfilter_8cpp.html", null ],
    [ "filmgrainfilter.h", "filmgrainfilter_8h.html", [
      [ "FilmGrainContainer", "classDigikam_1_1FilmGrainContainer.html", "classDigikam_1_1FilmGrainContainer" ],
      [ "FilmGrainFilter", "classDigikam_1_1FilmGrainFilter.html", "classDigikam_1_1FilmGrainFilter" ]
    ] ],
    [ "filmgrainsettings.cpp", "filmgrainsettings_8cpp.html", null ],
    [ "filmgrainsettings.h", "filmgrainsettings_8h.html", [
      [ "FilmGrainSettings", "classDigikam_1_1FilmGrainSettings.html", "classDigikam_1_1FilmGrainSettings" ]
    ] ],
    [ "invertfilter.cpp", "invertfilter_8cpp.html", null ],
    [ "invertfilter.h", "invertfilter_8h.html", [
      [ "InvertFilter", "classDigikam_1_1InvertFilter.html", "classDigikam_1_1InvertFilter" ]
    ] ],
    [ "oilpaintfilter.cpp", "oilpaintfilter_8cpp.html", null ],
    [ "oilpaintfilter.h", "oilpaintfilter_8h.html", [
      [ "OilPaintFilter", "classDigikam_1_1OilPaintFilter.html", "classDigikam_1_1OilPaintFilter" ]
    ] ],
    [ "raindropfilter.cpp", "raindropfilter_8cpp.html", null ],
    [ "raindropfilter.h", "raindropfilter_8h.html", [
      [ "RainDropFilter", "classDigikam_1_1RainDropFilter.html", "classDigikam_1_1RainDropFilter" ]
    ] ]
];