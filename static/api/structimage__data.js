var structimage__data =
[
    [ "state", "structimage__data.html#a1b0aee4f8f66f558fa667a58f5825700", [
      [ "state_unprocessed", "structimage__data.html#a1b0aee4f8f66f558fa667a58f5825700a2dcf5205019e7805153945c4dde6c3b5", null ],
      [ "state_sop_metadata_available", "structimage__data.html#a1b0aee4f8f66f558fa667a58f5825700ae5372fa0a103544c859b1030d74927c4", null ],
      [ "state_encoding", "structimage__data.html#a1b0aee4f8f66f558fa667a58f5825700a7eb12731e214ef16a4263dc2ef56ffd3", null ],
      [ "state_keep_for_reference", "structimage__data.html#a1b0aee4f8f66f558fa667a58f5825700adc4808eaa8f5d1addc5125b4102cd1ab", null ],
      [ "state_skipped", "structimage__data.html#a1b0aee4f8f66f558fa667a58f5825700a268ab369015af0fd27329da8de1fc82a", null ]
    ] ],
    [ "image_data", "structimage__data.html#a08788bdd0e17d909426f608f49e9fe95", null ],
    [ "~image_data", "structimage__data.html#aae7baf7f76d73692c614db979e5a496e", null ],
    [ "set_intra", "structimage__data.html#aed49d2d5f52eef1e13323f100a3f698a", null ],
    [ "set_NAL_temporal_id", "structimage__data.html#a241b5a75772d01c45387a77d6b2f5a55", null ],
    [ "set_NAL_type", "structimage__data.html#ad50bae47e6e1093b9d9981b2afaa6da7", null ],
    [ "set_references", "structimage__data.html#ab54686b28c2f38d4e33b0a9b5803cf9a", null ],
    [ "set_skip_priority", "structimage__data.html#a1bac586ccfcff299887cc772afb85ef6", null ],
    [ "frame_number", "structimage__data.html#ada10a233a8fa493f606848d33d404aa3", null ],
    [ "input", "structimage__data.html#aa49e33c5e60361578a3a9d7924a68e71", null ],
    [ "is_in_output_queue", "structimage__data.html#a227e5830acf7f0aed5982d9751915235", null ],
    [ "is_intra", "structimage__data.html#a3c41d10fd533967eb89c327fc7f0bce8", null ],
    [ "keep", "structimage__data.html#aec2a9fb1fd9626b6af24fc2f2f983b6f", null ],
    [ "longterm", "structimage__data.html#ab960f8b2652fc8defcd25b7be7600e4e", null ],
    [ "mark_used", "structimage__data.html#a2272d6d2d2c5a54069c0fb3b75231689", null ],
    [ "nal", "structimage__data.html#a1faed9be9742172529d37402a5d35f8c", null ],
    [ "prediction", "structimage__data.html#aaa553294adbcd374a21dffe1c70cf78c", null ],
    [ "reconstruction", "structimage__data.html#a47f079c6ed607bca063c5b1f0e5311aa", null ],
    [ "ref0", "structimage__data.html#a155e3b6a14c22d5f705f04f211f70ccd", null ],
    [ "ref1", "structimage__data.html#ae822a19136456888da8d5d52c27565bb", null ],
    [ "shdr", "structimage__data.html#a75ba65ef2ced00cffe1b456ebc720d02", null ],
    [ "skip_priority", "structimage__data.html#ae2fc709b17573ae130bb3f89642bd252", null ],
    [ "sps_index", "structimage__data.html#af66d52e6b993b56e55467b81e7f0c455", null ],
    [ "state", "structimage__data.html#a782acfef698ea7b1ea3bb77a0f9975fe", null ]
];