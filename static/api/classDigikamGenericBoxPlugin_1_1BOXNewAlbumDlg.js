var classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg =
[
    [ "BOXNewAlbumDlg", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#ac0c9a717ba3ee5d050934b38eafc615c", null ],
    [ "~BOXNewAlbumDlg", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#ae5c0ec8ae68164d6f569486e35f23925", null ],
    [ "addToMainLayout", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getFolderTitle", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a240eed019766006ec233891f70d0f926", null ],
    [ "getLocEdit", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];