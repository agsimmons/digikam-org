var thememanager__p_8cpp =
[
    [ "DECO_DEFAULT", "thememanager__p_8cpp.html#ac9708b48f424559cd4c60461a268361f", null ],
    [ "DEFAULT", "thememanager__p_8cpp.html#ae3f1a325f4ea96a63bce65c10abf595f", null ],
    [ "HCY_REC", "thememanager__p_8cpp.html#a0dcaab1ad25cbf594c69d1fae39fbd72", null ],
    [ "SET_DEFAULT", "thememanager__p_8cpp.html#ad81511d463c2170ece8115c07be98b41", null ],
    [ "contrastRatio", "thememanager__p_8cpp.html#a99020af4c4090f80147f86c39e3a6e0f", null ],
    [ "darken", "thememanager__p_8cpp.html#ae70beb21b179e4d36e6025044dcf2ad1", null ],
    [ "getHcy", "thememanager__p_8cpp.html#ae132cf52b76fe7adfe0bffd82f3049a3", null ],
    [ "lighten", "thememanager__p_8cpp.html#a0df84e0dfac2ee091bd2bad5e59ce789", null ],
    [ "luma", "thememanager__p_8cpp.html#a47014e9496fab9c1e1d2abe34023d1fe", null ],
    [ "mix", "thememanager__p_8cpp.html#a4f99843a37770491bfdcdeeeee0dd1f8", null ],
    [ "overlayColors", "thememanager__p_8cpp.html#abdc99b7a58099d56b46a2a66d29ba54a", null ],
    [ "shade", "thememanager__p_8cpp.html#afd74463f19f5242456b548c7bdc799e6", null ],
    [ "tint", "thememanager__p_8cpp.html#ae2e981f73ce57f2b7300185a547fc37d", null ]
];