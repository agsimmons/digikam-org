var classDigikam_1_1AlbumsDBJobInfo =
[
    [ "AlbumsDBJobInfo", "classDigikam_1_1AlbumsDBJobInfo.html#a8fb0cd0e4cb8b5d27157d0174fd98c6a", null ],
    [ "album", "classDigikam_1_1AlbumsDBJobInfo.html#ac6427c36e96b980cf05cff4cd2d61836", null ],
    [ "albumRootId", "classDigikam_1_1AlbumsDBJobInfo.html#a9e8c26494378d85f4d239b555254a7a2", null ],
    [ "isFoldersJob", "classDigikam_1_1AlbumsDBJobInfo.html#a1b1b009cdf8f611bc10488acb3d37026", null ],
    [ "isListAvailableImagesOnly", "classDigikam_1_1AlbumsDBJobInfo.html#a3dc339cb6285b0debc56a6cb4d844835", null ],
    [ "isRecursive", "classDigikam_1_1AlbumsDBJobInfo.html#a3998e8c5613f83bf74f126fa68ea3e04", null ],
    [ "setAlbum", "classDigikam_1_1AlbumsDBJobInfo.html#a83f5f44a927c0341b6baab4fd4557d32", null ],
    [ "setAlbumRootId", "classDigikam_1_1AlbumsDBJobInfo.html#a5e1bd76c4da6c69676f9667f90541c49", null ],
    [ "setFoldersJob", "classDigikam_1_1AlbumsDBJobInfo.html#afd02b67871ce293b7f6420913a0fa391", null ],
    [ "setListAvailableImagesOnly", "classDigikam_1_1AlbumsDBJobInfo.html#ac844bb2285615c4a3f37902934f72fc4", null ],
    [ "setRecursive", "classDigikam_1_1AlbumsDBJobInfo.html#a15cd3f8162a63be05d794988f1453dd2", null ]
];