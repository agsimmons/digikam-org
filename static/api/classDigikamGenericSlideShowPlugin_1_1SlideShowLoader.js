var classDigikamGenericSlideShowPlugin_1_1SlideShowLoader =
[
    [ "SlideShowViewMode", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#af67bf133fd2dc8067970a3e4aba194cf", [
      [ "ErrorView", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#af67bf133fd2dc8067970a3e4aba194cfa59a4496c758d6ef3725667a66b047869", null ],
      [ "ImageView", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#af67bf133fd2dc8067970a3e4aba194cfacdf449e624d2db46c6cea9d6058fa498", null ],
      [ "VideoView", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#af67bf133fd2dc8067970a3e4aba194cfaccb93e1cf5e9186bd52d8a304c5f9abd", null ],
      [ "EndView", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#af67bf133fd2dc8067970a3e4aba194cfac298b41b60c21f82b06962f1392a33fd", null ]
    ] ],
    [ "SlideShowLoader", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a2bfe6d6b65caf2e06437b34bae9441be", null ],
    [ "~SlideShowLoader", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#aa21bf85c1aba651dea7565c4bc844129", null ],
    [ "currentItem", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a14137196af21b123d5b539640573fd1b", null ],
    [ "keyPressEvent", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a5ae31fd4a8df34b3e62c09d2f04d69c3", null ],
    [ "mousePressEvent", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a4b2aab074b76e82c31e646b32231f7a2", null ],
    [ "setCurrentItem", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a38ea898425ff567d0771d1b9e9f0d1f3", null ],
    [ "setShortCutPrefixes", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a25e769eb9b462cd8d82c4d8c12f4667b", null ],
    [ "signalLastItemUrl", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a3788baf25e897d9e6a3b5061f20ec5f7", null ],
    [ "slotAssignColorLabel", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a93a4d89c11115c57e54fbefac4ace550", null ],
    [ "slotAssignPickLabel", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a54a8ba82030a188698eefadd2a98b75f", null ],
    [ "slotAssignRating", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#aec4e90e3725850cfa59b22ee05c1c79c", null ],
    [ "slotHandleShortcut", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#ab886586df4042f42fcb6514679efc505", null ],
    [ "slotLoadNextItem", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#aa1e74c51b5c357a506e549413bf1235c", null ],
    [ "slotLoadPrevItem", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a2f1f890a4dad3d4c9f0e4f03c556bb13", null ],
    [ "slotPause", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#ab2bf819146788c9cc2f1a8483ea3f212", null ],
    [ "slotPlay", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a4ba8ac98caa65e1597773dee14c41af5", null ],
    [ "slotRemoveImageFromList", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#adf86f40720d08d81984d3d0545ea0761", null ],
    [ "slotToggleTag", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#adbd574a78bca84277bba321dc1e46142", null ],
    [ "wheelEvent", "classDigikamGenericSlideShowPlugin_1_1SlideShowLoader.html#a9090e511a02b041d25127189e5a664b0", null ]
];