var namespaceDigikamGenericVKontaktePlugin =
[
    [ "VKAlbumChooser", "classDigikamGenericVKontaktePlugin_1_1VKAlbumChooser.html", "classDigikamGenericVKontaktePlugin_1_1VKAlbumChooser" ],
    [ "VKAuthWidget", "classDigikamGenericVKontaktePlugin_1_1VKAuthWidget.html", "classDigikamGenericVKontaktePlugin_1_1VKAuthWidget" ],
    [ "VKNewAlbumDlg", "classDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg.html", "classDigikamGenericVKontaktePlugin_1_1VKNewAlbumDlg" ],
    [ "VKontaktePlugin", "classDigikamGenericVKontaktePlugin_1_1VKontaktePlugin.html", "classDigikamGenericVKontaktePlugin_1_1VKontaktePlugin" ],
    [ "VKWindow", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html", "classDigikamGenericVKontaktePlugin_1_1VKWindow" ]
];