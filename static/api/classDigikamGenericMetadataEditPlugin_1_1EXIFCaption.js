var classDigikamGenericMetadataEditPlugin_1_1EXIFCaption =
[
    [ "EXIFCaption", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a5bfcfc5d35388377abc56e26fc248d24", null ],
    [ "~EXIFCaption", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#ae4539a1f6029738bc72cf546b13a333d", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a67cef539676d96a518ee7f8f02adff40", null ],
    [ "getEXIFUserComments", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a67b877634823abb63f0970ab433bab39", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#af789066833d9870ae3f83614a5bdcb70", null ],
    [ "setCheckedSyncIPTCCaption", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a1bf47b5e279fdfcbd0bb0c741e81e695", null ],
    [ "setCheckedSyncJFIFComment", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a42adb32e96cc408c1a03c6cf9597058f", null ],
    [ "setCheckedSyncXMPCaption", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a9a8bb884243539ef1919f32a2e597514", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#aa5c6b978eb0ac5b6803d4dd524142c4a", null ],
    [ "syncIPTCCaptionIsChecked", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a3dc2b0458cf32690ed98482fd24d778e", null ],
    [ "syncJFIFCommentIsChecked", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#a6558d588ff367ba0d625c046c8aa2c8b", null ],
    [ "syncXMPCaptionIsChecked", "classDigikamGenericMetadataEditPlugin_1_1EXIFCaption.html#aad5d35203dcf3d179f9d7cdafb1aa265", null ]
];