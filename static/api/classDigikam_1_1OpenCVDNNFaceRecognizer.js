var classDigikam_1_1OpenCVDNNFaceRecognizer =
[
    [ "Private", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private" ],
    [ "Classifier", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aaa5404fe26c7e3b4591816fff57cf0eb", [
      [ "SVM", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aaa5404fe26c7e3b4591816fff57cf0ebaac02fae66f26544150de47f0d0b019da", null ],
      [ "OpenCV_KNN", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aaa5404fe26c7e3b4591816fff57cf0eba1233516a08235471095d09667ec3591b", null ],
      [ "Tree", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aaa5404fe26c7e3b4591816fff57cf0ebaa247455d36e562aacf20317f35a77357", null ],
      [ "DB", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aaa5404fe26c7e3b4591816fff57cf0eba6c4b4a7064030d7df1fae49472965b90", null ]
    ] ],
    [ "OpenCVDNNFaceRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#ae99a4daaa3db41d2f372394f6268e563", null ],
    [ "~OpenCVDNNFaceRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#ad9e6e78e3371f0c61e51c7eea8a5ea49", null ],
    [ "clearTraining", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aa602df0a46d75471e9e1d0f90216ea62", null ],
    [ "recognize", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#af481b832964eb21ac5cdf428d13d7bef", null ],
    [ "recognize", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#a5da34558f226a526902f436ac8a06518", null ],
    [ "registerTrainingData", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#af99214b4bc9f3f97dd3c21b4aa0691c4", null ],
    [ "setNbNeighBors", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#ad7bcf19a33d763d761578e4157e3bd5c", null ],
    [ "setThreshold", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#ac7d181eae2f413c03fb6f363d3599f41", null ],
    [ "train", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#aca3ce99232e5fa680ee03da6941de74c", null ],
    [ "verifyTestData", "classDigikam_1_1OpenCVDNNFaceRecognizer.html#a1b4593f3f68c40a6126baa8857c3a527", null ]
];