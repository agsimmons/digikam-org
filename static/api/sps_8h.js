var sps_8h =
[
    [ "scaling_list_data", "structscaling__list__data.html", "structscaling__list__data" ],
    [ "seq_parameter_set", "classseq__parameter__set.html", "classseq__parameter__set" ],
    [ "sps_range_extension", "classsps__range__extension.html", "classsps__range__extension" ],
    [ "MAX_NUM_LT_REF_PICS_SPS", "sps_8h.html#a4a1a1475472f3b281594ca6545329d4b", null ],
    [ "MAX_PICTURE_HEIGHT", "sps_8h.html#a53119dcf011272a738dc5f74a87fa0ee", null ],
    [ "MAX_PICTURE_WIDTH", "sps_8h.html#a7c577e51d1fd312b53736e78252310de", null ],
    [ "scaling_list_data", "sps_8h.html#a69b6c9d407b98e3ff66b1782f8106284", null ],
    [ "PresetSet", "sps_8h.html#a81b35c5d2f31e9efe00facd211d00df7", [
      [ "Preset_Default", "sps_8h.html#a81b35c5d2f31e9efe00facd211d00df7a729348ba62df692b4e70582e7fad303c", null ]
    ] ],
    [ "read_scaling_list", "sps_8h.html#a05324dd2e158f57e558d1b354fd1627d", null ],
    [ "set_default_scaling_lists", "sps_8h.html#ae8237cdb34e2ad4a05241e269e9e7ce7", null ],
    [ "write_scaling_list", "sps_8h.html#a0dd21eed8b130b490deed660e417b747", null ]
];