var classDigikam_1_1TaggingActionFactory =
[
    [ "ConstraintInterface", "classDigikam_1_1TaggingActionFactory_1_1ConstraintInterface.html", "classDigikam_1_1TaggingActionFactory_1_1ConstraintInterface" ],
    [ "NameMatchMode", "classDigikam_1_1TaggingActionFactory.html#a6676e2f55f417fc47362279136b712ce", [
      [ "MatchStartingWithFragment", "classDigikam_1_1TaggingActionFactory.html#a6676e2f55f417fc47362279136b712cea9fee0d95c0c1966b22b3167b0d6d0be8", null ],
      [ "MatchContainingFragment", "classDigikam_1_1TaggingActionFactory.html#a6676e2f55f417fc47362279136b712cea2a6ff6357fe725ad5b20c9c5ea9313ba", null ]
    ] ],
    [ "TaggingActionFactory", "classDigikam_1_1TaggingActionFactory.html#a40f5c9ebceeef9c5d03f884dc2f04ab7", null ],
    [ "~TaggingActionFactory", "classDigikam_1_1TaggingActionFactory.html#a6087273aaa48ad82d53536127b5d1486", null ],
    [ "actions", "classDigikam_1_1TaggingActionFactory.html#aafb9a3a4b5883d18d29e247dfd8c8285", null ],
    [ "constraintInterface", "classDigikam_1_1TaggingActionFactory.html#a6c8a4f1c95cbfb9b4fe8a6d5fece910c", null ],
    [ "defaultTaggingAction", "classDigikam_1_1TaggingActionFactory.html#af22bbdf55598b3db40355c940694c955", null ],
    [ "fragment", "classDigikam_1_1TaggingActionFactory.html#a53aee97a47c1503e11819979dd6be68a", null ],
    [ "indexOfDefaultAction", "classDigikam_1_1TaggingActionFactory.html#a548802882ae85c2db1a6ba29154677ce", null ],
    [ "indexOfLastRecentAction", "classDigikam_1_1TaggingActionFactory.html#a725f7b3c82592f2cc27ecdc20635cbe3", null ],
    [ "nameMatchMode", "classDigikam_1_1TaggingActionFactory.html#a27f27e87a38ae651c7ba4111baa33b55", null ],
    [ "parentTagId", "classDigikam_1_1TaggingActionFactory.html#a2693fa34f69cb07465bcc12b525b5154", null ],
    [ "reset", "classDigikam_1_1TaggingActionFactory.html#a1ce9db779bbf6d2c37c03fe90efed5e5", null ],
    [ "setConstraintInterface", "classDigikam_1_1TaggingActionFactory.html#a79564ff205700285d3b6714b664c5e6d", null ],
    [ "setFragment", "classDigikam_1_1TaggingActionFactory.html#afa7c7547ac62a7f5b02d592a9a4d3e14", null ],
    [ "setNameMatchMode", "classDigikam_1_1TaggingActionFactory.html#a731c789abb45564c05d76380f5b5977e", null ],
    [ "setParentTag", "classDigikam_1_1TaggingActionFactory.html#a15aa9dbedcdd649e2f0bb38cdd97ca34", null ],
    [ "suggestedUIString", "classDigikam_1_1TaggingActionFactory.html#ad46104f5e7c5299b78dea4428ab0bda0", null ]
];