var namespaceDigikamGenericGeolocationEditPlugin =
[
    [ "GeoDataContainer", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataContainer.html", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataContainer" ],
    [ "GeoDataParser", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataParser.html", "classDigikamGenericGeolocationEditPlugin_1_1GeoDataParser" ],
    [ "GeolocationEdit", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEdit.html", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEdit" ],
    [ "GeolocationEditPlugin", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEditPlugin.html", "classDigikamGenericGeolocationEditPlugin_1_1GeolocationEditPlugin" ],
    [ "GPSItemDetails", "classDigikamGenericGeolocationEditPlugin_1_1GPSItemDetails.html", "classDigikamGenericGeolocationEditPlugin_1_1GPSItemDetails" ],
    [ "KmlExport", "classDigikamGenericGeolocationEditPlugin_1_1KmlExport.html", "classDigikamGenericGeolocationEditPlugin_1_1KmlExport" ],
    [ "KMLGeoDataParser", "classDigikamGenericGeolocationEditPlugin_1_1KMLGeoDataParser.html", "classDigikamGenericGeolocationEditPlugin_1_1KMLGeoDataParser" ],
    [ "KmlWidget", "classDigikamGenericGeolocationEditPlugin_1_1KmlWidget.html", "classDigikamGenericGeolocationEditPlugin_1_1KmlWidget" ],
    [ "SearchBackend", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchBackend" ],
    [ "SearchResultModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModel" ],
    [ "SearchResultModelHelper", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper" ],
    [ "SearchWidget", "classDigikamGenericGeolocationEditPlugin_1_1SearchWidget.html", "classDigikamGenericGeolocationEditPlugin_1_1SearchWidget" ],
    [ "GeoDataParserParseTime", "namespaceDigikamGenericGeolocationEditPlugin.html#a0f584882438fc7478cee687fdb4b0fd9", null ]
];