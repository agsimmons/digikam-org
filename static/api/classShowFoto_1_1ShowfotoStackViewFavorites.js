var classShowFoto_1_1ShowfotoStackViewFavorites =
[
    [ "ShowfotoStackViewFavorites", "classShowFoto_1_1ShowfotoStackViewFavorites.html#a99d9be182c5b5310a9170414634b6d7f", null ],
    [ "~ShowfotoStackViewFavorites", "classShowFoto_1_1ShowfotoStackViewFavorites.html#aee9ad38c6ae790afc9bdf647845aca8f", null ],
    [ "loadContents", "classShowFoto_1_1ShowfotoStackViewFavorites.html#a705b42014ca2fc03b458aba4b4d48a81", null ],
    [ "pluginActions", "classShowFoto_1_1ShowfotoStackViewFavorites.html#af10f1fe65a533cbb7e632b00da143c07", null ],
    [ "readSettings", "classShowFoto_1_1ShowfotoStackViewFavorites.html#abd0f3ccfe59aed918ea6f9b69f529cdc", null ],
    [ "saveSettings", "classShowFoto_1_1ShowfotoStackViewFavorites.html#aa7ba6aaecea31155bb3bccbc56f65dc4", null ],
    [ "signalLoadContents", "classShowFoto_1_1ShowfotoStackViewFavorites.html#a7f4579fa30966ef75a407156176f966c", null ],
    [ "signalLoadContentsFromFiles", "classShowFoto_1_1ShowfotoStackViewFavorites.html#a29b9744ae989ef13e8e846292a63a143", null ],
    [ "slotItemListChanged", "classShowFoto_1_1ShowfotoStackViewFavorites.html#a81981c05dada5706293dbfa67119cb01", null ],
    [ "toolBarAction", "classShowFoto_1_1ShowfotoStackViewFavorites.html#abbf19f6d8723e9013c53db74b00611f1", null ],
    [ "topFavoritesItem", "classShowFoto_1_1ShowfotoStackViewFavorites.html#a0632b8620e34c05de01ab3550e8198f4", null ]
];