var classDigikam_1_1MetaEngine =
[
    [ "Private", "classDigikam_1_1MetaEngine_1_1Private.html", "classDigikam_1_1MetaEngine_1_1Private" ],
    [ "AltLangMap", "classDigikam_1_1MetaEngine.html#afa8575be16b91f0a2083c52475aafc74", null ],
    [ "MetaDataMap", "classDigikam_1_1MetaEngine.html#a7785bd2707f5d3200fa9efa01bb0bc6d", null ],
    [ "TagsMap", "classDigikam_1_1MetaEngine.html#af2f26ad33c28f4fe90583e0cf4af4148", null ],
    [ "Backend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5", [
      [ "Exiv2Backend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5a97836003ee3a32e49c488f1bb32ae7f1", null ],
      [ "LibRawBackend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5afa52b4b531cb599206fb43be7d7b4d51", null ],
      [ "LibHeifBackend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5af09b03b6315b969b4aab4f116160bb2d", null ],
      [ "ImageMagickBackend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5afd5f691948b4c13596f204c5226e8301", null ],
      [ "FFMpegBackend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5a114dcafd5cd77abd72e1e3f58d3ea636", null ],
      [ "NoBackend", "classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5acd053f34a95a3f23735971fde7d8b4d1", null ]
    ] ],
    [ "ImageColorWorkSpace", "classDigikam_1_1MetaEngine.html#af871001b40853e5d3ebbd8234bbd9e3c", [
      [ "WORKSPACE_UNSPECIFIED", "classDigikam_1_1MetaEngine.html#af871001b40853e5d3ebbd8234bbd9e3caeb7dd0d8a26687e66eb2b833e72e2026", null ],
      [ "WORKSPACE_SRGB", "classDigikam_1_1MetaEngine.html#af871001b40853e5d3ebbd8234bbd9e3cac08fb059654b6c9159d92b99ce7e80d1", null ],
      [ "WORKSPACE_ADOBERGB", "classDigikam_1_1MetaEngine.html#af871001b40853e5d3ebbd8234bbd9e3ca2468733ded827ae4d3be2a70d5fdb65f", null ],
      [ "WORKSPACE_UNCALIBRATED", "classDigikam_1_1MetaEngine.html#af871001b40853e5d3ebbd8234bbd9e3ca5089998d58daa8d396cfadfb01d5455a", null ]
    ] ],
    [ "ImageOrientation", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35", [
      [ "ORIENTATION_UNSPECIFIED", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35a44e12def6ff0e80b19dd72d1e7bb6868", null ],
      [ "ORIENTATION_NORMAL", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35a5dcc92242bd3ac90f56ead9460efa58c", null ],
      [ "ORIENTATION_HFLIP", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35afeb407c34001a5a6d1d1e26c5ecf12fe", null ],
      [ "ORIENTATION_ROT_180", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35a293cd1779c4a608bb3bf099588d0e3d4", null ],
      [ "ORIENTATION_VFLIP", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35aef0efa79e301b7f3926fe0c66f94d912", null ],
      [ "ORIENTATION_ROT_90_HFLIP", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35ac98a2f0879657350d1663632697fbed5", null ],
      [ "ORIENTATION_ROT_90", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35a9ba291fa4cc2af3fb3a836bd861f244c", null ],
      [ "ORIENTATION_ROT_90_VFLIP", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35a4c4bd68d17b215f77e7c0e6f9c64fd69", null ],
      [ "ORIENTATION_ROT_270", "classDigikam_1_1MetaEngine.html#a53d5eae489571b2bcd0395a8b3bd8e35a249b325787917432f057b5200eab3e61", null ]
    ] ],
    [ "MetadataWritingMode", "classDigikam_1_1MetaEngine.html#a853db5d1e7d4dfb543f9179ccf0e055c", [
      [ "WRITE_TO_FILE_ONLY", "classDigikam_1_1MetaEngine.html#a853db5d1e7d4dfb543f9179ccf0e055ca99c4d12bde08934943c68f5725337a97", null ],
      [ "WRITE_TO_SIDECAR_ONLY", "classDigikam_1_1MetaEngine.html#a853db5d1e7d4dfb543f9179ccf0e055ca6f64e72654cf128d6ee22e95eac32b6d", null ],
      [ "WRITE_TO_SIDECAR_AND_FILE", "classDigikam_1_1MetaEngine.html#a853db5d1e7d4dfb543f9179ccf0e055ca8724bbdadd77e3239d1863fad88090e0", null ],
      [ "WRITE_TO_SIDECAR_ONLY_FOR_READ_ONLY_FILES", "classDigikam_1_1MetaEngine.html#a853db5d1e7d4dfb543f9179ccf0e055cad06bfed4d66ca246406546ffc755326d", null ]
    ] ],
    [ "XmpTagType", "classDigikam_1_1MetaEngine.html#a7bcba4e979c2126dc56274e1b546deb2", [
      [ "NormalTag", "classDigikam_1_1MetaEngine.html#a7bcba4e979c2126dc56274e1b546deb2a625a17bae879b29f3ce4ee25a63e5347", null ],
      [ "ArrayBagTag", "classDigikam_1_1MetaEngine.html#a7bcba4e979c2126dc56274e1b546deb2a5221878b775757a16a5fde3f2b2e45ef", null ],
      [ "StructureTag", "classDigikam_1_1MetaEngine.html#a7bcba4e979c2126dc56274e1b546deb2adda1c2e80d1b12ddef1d895f2e4d22b1", null ],
      [ "ArrayLangTag", "classDigikam_1_1MetaEngine.html#a7bcba4e979c2126dc56274e1b546deb2aceaf3e4bca1ede0ce9c18c2a762d2ac7", null ],
      [ "ArraySeqTag", "classDigikam_1_1MetaEngine.html#a7bcba4e979c2126dc56274e1b546deb2a7fdd66d054edebe17bc266c8ae7c4d0e", null ]
    ] ],
    [ "MetaEngine", "classDigikam_1_1MetaEngine.html#af6d7548068fe13f8901dfac00cbc4e44", null ],
    [ "MetaEngine", "classDigikam_1_1MetaEngine.html#aec79901c8319434e857df207d3231571", null ],
    [ "MetaEngine", "classDigikam_1_1MetaEngine.html#af1a966acf098629b9987e4bb291f18f9", null ],
    [ "~MetaEngine", "classDigikam_1_1MetaEngine.html#a8c531d30b61cdf6b53fc74e819373079", null ],
    [ "addToXmpTagStringBag", "classDigikam_1_1MetaEngine.html#a292412c88b2b9e9b4c5998e284693108", null ],
    [ "applyChanges", "classDigikam_1_1MetaEngine.html#a6f5fc9bf2e260044cc78e4503fd35b66", null ],
    [ "clearComments", "classDigikam_1_1MetaEngine.html#a3abf3e4e14f01e4eb9ecdbde15493afb", null ],
    [ "clearExif", "classDigikam_1_1MetaEngine.html#a57afbc55402188110fbdcc1fb4d43043", null ],
    [ "clearIptc", "classDigikam_1_1MetaEngine.html#ac826473aa30dd45b8d5eb1af4577e03f", null ],
    [ "clearXmp", "classDigikam_1_1MetaEngine.html#ad04b482ccc1a61501c0f29989ce3a1c0", null ],
    [ "createExifUserStringFromValue", "classDigikam_1_1MetaEngine.html#a5664c1065873d36888b3e569ec6d1736", null ],
    [ "data", "classDigikam_1_1MetaEngine.html#a1870501585168a5746a140d574186e42", null ],
    [ "exportChanges", "classDigikam_1_1MetaEngine.html#a799ceb8319b196cb632c2da75d2c11ec", null ],
    [ "getComments", "classDigikam_1_1MetaEngine.html#a6ffd54d0d317947f16b55805d08c43c2", null ],
    [ "getCommentsDecoded", "classDigikam_1_1MetaEngine.html#a46ec8f9ceb63637c26a31258ce5ca042", null ],
    [ "getDigitizationDateTime", "classDigikam_1_1MetaEngine.html#a57f15a1e70b23ed17dccb5864e658163", null ],
    [ "getExifComment", "classDigikam_1_1MetaEngine.html#ae2385c9d52f0842542a0d75ae57c39e1", null ],
    [ "getExifEncoded", "classDigikam_1_1MetaEngine.html#a8e34eaf5a9f284459f1121a34fedc5d2", null ],
    [ "getExifTagData", "classDigikam_1_1MetaEngine.html#a3d361e7362ce35d9ca3a2e7cf2b26702", null ],
    [ "getExifTagDescription", "classDigikam_1_1MetaEngine.html#af692fd9e92665fea71df737fb6b3039c", null ],
    [ "getExifTagLong", "classDigikam_1_1MetaEngine.html#a5013752eb2e73d8a5e04802011ab5430", null ],
    [ "getExifTagLong", "classDigikam_1_1MetaEngine.html#adc13b3479a3502283ac7cb68e1a61cf7", null ],
    [ "getExifTagRational", "classDigikam_1_1MetaEngine.html#a147d35202cc30047de3eb0c51c052cee", null ],
    [ "getExifTagsDataList", "classDigikam_1_1MetaEngine.html#a825752323d77c8153d7b7284e7c4ec35", null ],
    [ "getExifTagString", "classDigikam_1_1MetaEngine.html#a226ad808a834c83b798d6b0d4bc9c167", null ],
    [ "getExifTagTitle", "classDigikam_1_1MetaEngine.html#a80fdda0a2e9ab9801e7bfecb21b081d0", null ],
    [ "getExifTagVariant", "classDigikam_1_1MetaEngine.html#a9fa1db9402c2f6e05c884d7845883c49", null ],
    [ "getExifThumbnail", "classDigikam_1_1MetaEngine.html#a3c8cdd09dd5b1b3c7d02a6cc2744cd42", null ],
    [ "getFilePath", "classDigikam_1_1MetaEngine.html#af0c0d293a369234d961da01289a6410c", null ],
    [ "getGPSAltitude", "classDigikam_1_1MetaEngine.html#a74533e8c918fd0144ce660d82f7a4ae7", null ],
    [ "getGPSInfo", "classDigikam_1_1MetaEngine.html#a2e8c450b70c26def7dad388f770801c2", null ],
    [ "getGPSLatitudeNumber", "classDigikam_1_1MetaEngine.html#a0e3e7b971e52ce1bc8b7b27f818e7a8f", null ],
    [ "getGPSLatitudeString", "classDigikam_1_1MetaEngine.html#aa3f3e01e3e626ac0a1c9322c180ab701", null ],
    [ "getGPSLongitudeNumber", "classDigikam_1_1MetaEngine.html#a9f10eff8419134bc2a09977c8730c7bd", null ],
    [ "getGPSLongitudeString", "classDigikam_1_1MetaEngine.html#ae595042b747fad637319339b82a46435", null ],
    [ "getIptc", "classDigikam_1_1MetaEngine.html#af969225ff794e096ec29776722099ee1", null ],
    [ "getIptcKeywords", "classDigikam_1_1MetaEngine.html#ac80bb06d368872a00ada357e5d7004e0", null ],
    [ "getIptcSubCategories", "classDigikam_1_1MetaEngine.html#a2248ea0cae3b02f1c6839585fc428823", null ],
    [ "getIptcSubjects", "classDigikam_1_1MetaEngine.html#aabbe368dc7fb75bebc733d28d8cda9ee", null ],
    [ "getIptcTagData", "classDigikam_1_1MetaEngine.html#a85033bccf129e114624b1c11cffd9948", null ],
    [ "getIptcTagDescription", "classDigikam_1_1MetaEngine.html#a4fa16509f88f9b6f37f6abba410e49a5", null ],
    [ "getIptcTagsDataList", "classDigikam_1_1MetaEngine.html#aa9c0e161d7d86d219a55f93a9e04b484", null ],
    [ "getIptcTagsList", "classDigikam_1_1MetaEngine.html#a3a00f7349570711d76febf709a6149a1", null ],
    [ "getIptcTagsStringList", "classDigikam_1_1MetaEngine.html#a8cc1d59998fbcd80456092bef552d1a7", null ],
    [ "getIptcTagString", "classDigikam_1_1MetaEngine.html#a04d058eeefbc043e1bcf8c7b50419c7e", null ],
    [ "getIptcTagTitle", "classDigikam_1_1MetaEngine.html#af1783be9dfd24fd0998cf6bbbe5d88cc", null ],
    [ "getItemColorWorkSpace", "classDigikam_1_1MetaEngine.html#abf7ac783e753fe5a7b42968780ebe034", null ],
    [ "getItemDateTime", "classDigikam_1_1MetaEngine.html#a701ca102efe1df588eb5cd29f31002ef", null ],
    [ "getItemDimensions", "classDigikam_1_1MetaEngine.html#ad845ceea0c9ca2d4f079b8fca1374a49", null ],
    [ "getItemOrientation", "classDigikam_1_1MetaEngine.html#a365508f7b1be0ad1d3ce3e4691baef4e", null ],
    [ "getItemPreview", "classDigikam_1_1MetaEngine.html#a0dde24a82f8b75b4af139a8e123694e7", null ],
    [ "getMakernoteTagsList", "classDigikam_1_1MetaEngine.html#aaf943ba642bc6c3e1d21d9a4f4b314de", null ],
    [ "getMimeType", "classDigikam_1_1MetaEngine.html#a48f6093ab567038a301a3beef2d3929b", null ],
    [ "getPixelSize", "classDigikam_1_1MetaEngine.html#a806b001b1144bd607414a8cfd88cdae3", null ],
    [ "getStdExifTagsList", "classDigikam_1_1MetaEngine.html#a4a9f9db2f5f036c1013deb134f0c4aca", null ],
    [ "getXmp", "classDigikam_1_1MetaEngine.html#a5727ab4b490c65898f768f726be74362", null ],
    [ "getXmpKeywords", "classDigikam_1_1MetaEngine.html#a743f79ed8c6aed41663c001cb94c60e2", null ],
    [ "getXmpSubCategories", "classDigikam_1_1MetaEngine.html#a901c57ea988313e5792aa9ecca135244", null ],
    [ "getXmpSubjects", "classDigikam_1_1MetaEngine.html#aff7056e3b0b1a3536feb45edde00d345", null ],
    [ "getXmpTagDescription", "classDigikam_1_1MetaEngine.html#ab998305bd3cf531a023f862e4a892370", null ],
    [ "getXmpTagsDataList", "classDigikam_1_1MetaEngine.html#a62ce106e366e62ae9f66946f4cc3fef7", null ],
    [ "getXmpTagsList", "classDigikam_1_1MetaEngine.html#a414572ecef7bc0c929911497efd17b01", null ],
    [ "getXmpTagString", "classDigikam_1_1MetaEngine.html#a7c5cf691310a548b094b6d70955763af", null ],
    [ "getXmpTagStringBag", "classDigikam_1_1MetaEngine.html#a25714c0ae635b4f3fe0a185d12eabfae", null ],
    [ "getXmpTagStringLangAlt", "classDigikam_1_1MetaEngine.html#a07fcfb58607d519444bc3fb834a646d9", null ],
    [ "getXmpTagStringListLangAlt", "classDigikam_1_1MetaEngine.html#acb5996747f3b2fb26969cbf9fab34768", null ],
    [ "getXmpTagStringSeq", "classDigikam_1_1MetaEngine.html#a171ea30e4079fc3db1cd25b83904841c", null ],
    [ "getXmpTagTitle", "classDigikam_1_1MetaEngine.html#aed08ccd7b1779180efab850157b11199", null ],
    [ "getXmpTagVariant", "classDigikam_1_1MetaEngine.html#acd5ce652a9f1ef11e74ae51776cd9265", null ],
    [ "hasComments", "classDigikam_1_1MetaEngine.html#a4dd83ac6f4cad2fff29921c9f8cb804c", null ],
    [ "hasExif", "classDigikam_1_1MetaEngine.html#a00c2b571d9ec94b4f123148bc5bf600b", null ],
    [ "hasIptc", "classDigikam_1_1MetaEngine.html#a5a08b0e32ce12fdeb8dfc0e19815a5a7", null ],
    [ "hasXmp", "classDigikam_1_1MetaEngine.html#aa256a0af9da7116707501fc5019c89b9", null ],
    [ "initializeGPSInfo", "classDigikam_1_1MetaEngine.html#a37420963e679d8ebf2340534bd1fdc34", null ],
    [ "isEmpty", "classDigikam_1_1MetaEngine.html#a8025daa804cd9ab05fcad2c6d82b390f", null ],
    [ "load", "classDigikam_1_1MetaEngine.html#a42854e97b14f4748d46ce807f4afed60", null ],
    [ "loadFromData", "classDigikam_1_1MetaEngine.html#ad4e326e618e1a71716bde53ec716f22e", null ],
    [ "loadFromSidecarAndMerge", "classDigikam_1_1MetaEngine.html#abae2ac3c18ae07db0f39f8da314b4d5c", null ],
    [ "metadataWritingMode", "classDigikam_1_1MetaEngine.html#ac6aeaa445f6e39ca4a461983042bab92", null ],
    [ "removeExifTag", "classDigikam_1_1MetaEngine.html#aa200a8cbf6201574c5e0355f7cee5a85", null ],
    [ "removeExifThumbnail", "classDigikam_1_1MetaEngine.html#ad2739ad4af2b0d2f766168b2f74034b0", null ],
    [ "removeFromXmpTagStringBag", "classDigikam_1_1MetaEngine.html#ae5ab4db46809ac201db512dd04d31e47", null ],
    [ "removeGPSInfo", "classDigikam_1_1MetaEngine.html#a7826619b6183f054adacf44c730df214", null ],
    [ "removeIptcTag", "classDigikam_1_1MetaEngine.html#a6ecad110dd3bf3e4f8b74e098412514c", null ],
    [ "removeXmpKeywords", "classDigikam_1_1MetaEngine.html#a05d4af15196ef59f880728d67dbb7797", null ],
    [ "removeXmpSubCategories", "classDigikam_1_1MetaEngine.html#a658044747371ed4cc806363fcadbd90f", null ],
    [ "removeXmpSubjects", "classDigikam_1_1MetaEngine.html#a1650b60a981366de9af5fc6090be640b", null ],
    [ "removeXmpTag", "classDigikam_1_1MetaEngine.html#a46564ead9781a283152e096366158b2c", null ],
    [ "rotateExifQImage", "classDigikam_1_1MetaEngine.html#ae906afd5b5f1c0e574eab75c166dc972", null ],
    [ "save", "classDigikam_1_1MetaEngine.html#aa6ef2025ca85e0c221164a177b9f3fc5", null ],
    [ "setComments", "classDigikam_1_1MetaEngine.html#a33facfdde31ce101ebd45c245366c67b", null ],
    [ "setData", "classDigikam_1_1MetaEngine.html#a160b6713e771af829af087706eff9658", null ],
    [ "setExif", "classDigikam_1_1MetaEngine.html#ad4dbb9f49d60e715ba2df4025e2c63d4", null ],
    [ "setExifComment", "classDigikam_1_1MetaEngine.html#adfa0e849ae067cecf87ede985263e36b", null ],
    [ "setExifTagData", "classDigikam_1_1MetaEngine.html#aa99e600dc86a78a8f2843be0bf8d8e4e", null ],
    [ "setExifTagLong", "classDigikam_1_1MetaEngine.html#a75638a1f1788a82f435d1aff25b05776", null ],
    [ "setExifTagRational", "classDigikam_1_1MetaEngine.html#a4572792181acc30ba94aeb48f0263b54", null ],
    [ "setExifTagString", "classDigikam_1_1MetaEngine.html#aff20540ae68efab2af67660c31e3a581", null ],
    [ "setExifTagVariant", "classDigikam_1_1MetaEngine.html#a2651624c3554a412f5ea837dca12646e", null ],
    [ "setExifThumbnail", "classDigikam_1_1MetaEngine.html#af784adb86099a6a710c77f4cb21d9321", null ],
    [ "setFilePath", "classDigikam_1_1MetaEngine.html#a96d7dd9eafd14ab5e5cd2b17ec38c977", null ],
    [ "setGPSInfo", "classDigikam_1_1MetaEngine.html#a30d693a0a91d485b5abbb2c1c141a799", null ],
    [ "setGPSInfo", "classDigikam_1_1MetaEngine.html#a058156901dfb3a02c165dd099c9e72c5", null ],
    [ "setGPSInfo", "classDigikam_1_1MetaEngine.html#a918273e79cf2ecd004408e81cdc15eea", null ],
    [ "setImageDateTime", "classDigikam_1_1MetaEngine.html#ae179b32745deb274ede3259a19580269", null ],
    [ "setIptc", "classDigikam_1_1MetaEngine.html#a58b3c2a52933ecfbdc78ccaaa719d380", null ],
    [ "setIptcKeywords", "classDigikam_1_1MetaEngine.html#a29678fcf03700ae810249f767c1424c6", null ],
    [ "setIptcSubCategories", "classDigikam_1_1MetaEngine.html#aa76c2aae65fe1df17d2c7e947c37f6b9", null ],
    [ "setIptcSubjects", "classDigikam_1_1MetaEngine.html#a2130a5e4c7861421c3b817aceac452da", null ],
    [ "setIptcTagData", "classDigikam_1_1MetaEngine.html#a97e62787898b5950333b4a5fd2cb1d0c", null ],
    [ "setIptcTagsStringList", "classDigikam_1_1MetaEngine.html#aa0042c6532898348a2b3a3d701eaab9c", null ],
    [ "setIptcTagString", "classDigikam_1_1MetaEngine.html#a506cf14d535a7a835836b41cc3c33570", null ],
    [ "setItemColorWorkSpace", "classDigikam_1_1MetaEngine.html#ab832fe567f960f3a63c61206d28deb64", null ],
    [ "setItemDimensions", "classDigikam_1_1MetaEngine.html#a3e1aa7130f63d03fb04318668dfc22fa", null ],
    [ "setItemOrientation", "classDigikam_1_1MetaEngine.html#adb1a8b54331367c62f7f8be57b1d5108", null ],
    [ "setItemPreview", "classDigikam_1_1MetaEngine.html#a75945dc0c330eadbdecee9a32b6dc885", null ],
    [ "setItemProgramId", "classDigikam_1_1MetaEngine.html#a806aaf2ec3e8f06a64de37ad82bbb9ac", null ],
    [ "setMetadataWritingMode", "classDigikam_1_1MetaEngine.html#a4570dd8c29426db5a7ff967e67c47aeb", null ],
    [ "setProgramId", "classDigikam_1_1MetaEngine.html#a2e99d4cfe36fb4ecbc7f2d47e8700e2a", null ],
    [ "setTiffThumbnail", "classDigikam_1_1MetaEngine.html#a6391899e32228a88260bd89ccd992168", null ],
    [ "setUpdateFileTimeStamp", "classDigikam_1_1MetaEngine.html#a7dc500d73fdf83eab8ea65ddcfa3a3bf", null ],
    [ "setUseCompatibleFileName", "classDigikam_1_1MetaEngine.html#a6a3bbc8dc3c71ff7dd39dc38ee401ddc", null ],
    [ "setUseXMPSidecar4Reading", "classDigikam_1_1MetaEngine.html#aa1177350df20de5baaeb6efc0cd7dc8b", null ],
    [ "setWriteDngFiles", "classDigikam_1_1MetaEngine.html#ab21fd927f85cf6c1173b50297f462d0d", null ],
    [ "setWriteRawFiles", "classDigikam_1_1MetaEngine.html#afa19ecf9b8d3a34893d0ab5314fb508f", null ],
    [ "setXmp", "classDigikam_1_1MetaEngine.html#af7df7da459c220d8a17fa625c34b7df4", null ],
    [ "setXmpKeywords", "classDigikam_1_1MetaEngine.html#ac8ef5760cd54a3a6591905deae4c2d31", null ],
    [ "setXmpSubCategories", "classDigikam_1_1MetaEngine.html#a5027efe5008ae3ea3f805ed9259a79c9", null ],
    [ "setXmpSubjects", "classDigikam_1_1MetaEngine.html#a244e2e266b60f34ce5d33f4c2736cb6d", null ],
    [ "setXmpTagString", "classDigikam_1_1MetaEngine.html#a2efc98100846f274bb4ef34bded5b7d6", null ],
    [ "setXmpTagString", "classDigikam_1_1MetaEngine.html#a5ddbd4382f009eeacc5345496b5a7b7f", null ],
    [ "setXmpTagStringBag", "classDigikam_1_1MetaEngine.html#a37c7c9d3387333f96ade7e85455d075b", null ],
    [ "setXmpTagStringLangAlt", "classDigikam_1_1MetaEngine.html#a1d39aa239834315c8a41c7660c7224fd", null ],
    [ "setXmpTagStringListLangAlt", "classDigikam_1_1MetaEngine.html#a9fb1851072aeec7f24f2390d14d1e5f0", null ],
    [ "setXmpTagStringSeq", "classDigikam_1_1MetaEngine.html#a7fc5ec9015600d24e60b288b2f52f1d6", null ],
    [ "updateFileTimeStamp", "classDigikam_1_1MetaEngine.html#a8f0829cdc2dda823e68f819623d9de74", null ],
    [ "useCompatibleFileName", "classDigikam_1_1MetaEngine.html#a70d3b2c3192fa6e053706a3878be5ba6", null ],
    [ "useXMPSidecar4Reading", "classDigikam_1_1MetaEngine.html#a76b3c1e9f8af7a087fe5becf8cb5e639", null ],
    [ "writeDngFiles", "classDigikam_1_1MetaEngine.html#a7b62477d7d0c7f78dd2076d8b7f6c268", null ],
    [ "writeRawFiles", "classDigikam_1_1MetaEngine.html#a58258e7a394ac209a08df573d38dfef7", null ],
    [ "MetaEnginePreviews", "classDigikam_1_1MetaEngine.html#a5c73ea8e8f4e4928ffa9717f8c104239", null ]
];