var dir_a5849e805c8cfe97d63d35aec5e9638f =
[
    [ "imageresizejob.cpp", "imageresizejob_8cpp.html", null ],
    [ "imageresizejob.h", "imageresizejob_8h.html", [
      [ "ImageResizeJob", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob.html", "classDigikamGenericSendByMailPlugin_1_1ImageResizeJob" ]
    ] ],
    [ "imageresizethread.cpp", "imageresizethread_8cpp.html", null ],
    [ "imageresizethread.h", "imageresizethread_8h.html", [
      [ "ImageResizeThread", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread.html", "classDigikamGenericSendByMailPlugin_1_1ImageResizeThread" ]
    ] ],
    [ "mailprocess.cpp", "mailprocess_8cpp.html", null ],
    [ "mailprocess.h", "mailprocess_8h.html", [
      [ "MailProcess", "classDigikamGenericSendByMailPlugin_1_1MailProcess.html", "classDigikamGenericSendByMailPlugin_1_1MailProcess" ]
    ] ],
    [ "mailsettings.cpp", "mailsettings_8cpp.html", null ],
    [ "mailsettings.h", "mailsettings_8h.html", [
      [ "MailSettings", "classDigikamGenericSendByMailPlugin_1_1MailSettings.html", "classDigikamGenericSendByMailPlugin_1_1MailSettings" ]
    ] ]
];