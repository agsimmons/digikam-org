var dir_c1f32bf87897dcd792a7e19e5227c81f =
[
    [ "bookmarknode.cpp", "bookmarknode_8cpp.html", null ],
    [ "bookmarknode.h", "bookmarknode_8h.html", [
      [ "BookmarkNode", "classDigikam_1_1BookmarkNode.html", "classDigikam_1_1BookmarkNode" ],
      [ "XbelReader", "classDigikam_1_1XbelReader.html", "classDigikam_1_1XbelReader" ],
      [ "XbelWriter", "classDigikam_1_1XbelWriter.html", "classDigikam_1_1XbelWriter" ]
    ] ],
    [ "bookmarksdlg.cpp", "bookmarksdlg_8cpp.html", null ],
    [ "bookmarksdlg.h", "bookmarksdlg_8h.html", [
      [ "AddBookmarkDialog", "classDigikam_1_1AddBookmarkDialog.html", "classDigikam_1_1AddBookmarkDialog" ],
      [ "BookmarksDialog", "classDigikam_1_1BookmarksDialog.html", "classDigikam_1_1BookmarksDialog" ]
    ] ],
    [ "bookmarksmenu.cpp", "bookmarksmenu_8cpp.html", null ],
    [ "bookmarksmenu.h", "bookmarksmenu_8h.html", [
      [ "BookmarksMenu", "classDigikam_1_1BookmarksMenu.html", "classDigikam_1_1BookmarksMenu" ],
      [ "ModelMenu", "classDigikam_1_1ModelMenu.html", "classDigikam_1_1ModelMenu" ]
    ] ],
    [ "bookmarksmngr.cpp", "bookmarksmngr_8cpp.html", null ],
    [ "bookmarksmngr.h", "bookmarksmngr_8h.html", [
      [ "AddBookmarkProxyModel", "classDigikam_1_1AddBookmarkProxyModel.html", "classDigikam_1_1AddBookmarkProxyModel" ],
      [ "BookmarksManager", "classDigikam_1_1BookmarksManager.html", "classDigikam_1_1BookmarksManager" ],
      [ "BookmarksModel", "classDigikam_1_1BookmarksModel.html", "classDigikam_1_1BookmarksModel" ],
      [ "ChangeBookmarkCommand", "classDigikam_1_1ChangeBookmarkCommand.html", "classDigikam_1_1ChangeBookmarkCommand" ],
      [ "InsertBookmarksCommand", "classDigikam_1_1InsertBookmarksCommand.html", "classDigikam_1_1InsertBookmarksCommand" ],
      [ "RemoveBookmarksCommand", "classDigikam_1_1RemoveBookmarksCommand.html", "classDigikam_1_1RemoveBookmarksCommand" ],
      [ "TreeProxyModel", "classDigikam_1_1TreeProxyModel.html", "classDigikam_1_1TreeProxyModel" ]
    ] ],
    [ "gpsbookmarkmodelhelper.cpp", "gpsbookmarkmodelhelper_8cpp.html", null ],
    [ "gpsbookmarkmodelhelper.h", "gpsbookmarkmodelhelper_8h.html", [
      [ "GPSBookmarkModelHelper", "classDigikam_1_1GPSBookmarkModelHelper.html", "classDigikam_1_1GPSBookmarkModelHelper" ]
    ] ],
    [ "gpsbookmarkowner.cpp", "gpsbookmarkowner_8cpp.html", null ],
    [ "gpsbookmarkowner.h", "gpsbookmarkowner_8h.html", [
      [ "GPSBookmarkOwner", "classDigikam_1_1GPSBookmarkOwner.html", "classDigikam_1_1GPSBookmarkOwner" ]
    ] ]
];