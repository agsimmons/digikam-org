var classDigikam_1_1TableViewColumns_1_1ColumnFileProperties =
[
    [ "ColumnCompareResult", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a0714c73efa682e4bcc6dac00989cabd7", [
      [ "CmpEqual", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a0714c73efa682e4bcc6dac00989cabd7ab1fd7950c9141af71b6d915d7619da20", null ],
      [ "CmpABiggerB", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a0714c73efa682e4bcc6dac00989cabd7a3b46fa13fd837bb5e8303e15579e9c0a", null ],
      [ "CmpALessB", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a0714c73efa682e4bcc6dac00989cabd7af64727fba2a786f83c4032b9ac4e2ac7", null ]
    ] ],
    [ "ColumnFlag", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4", [
      [ "ColumnNoFlags", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a3a1867e93424ceda7439df444b42b7a8", null ],
      [ "ColumnCustomPainting", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a2496490a69825be1607d673758561fea", null ],
      [ "ColumnCustomSorting", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4ab775074b18540b93dbbe923cc7977b0c", null ],
      [ "ColumnHasConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c", null ]
    ] ],
    [ "SubColumn", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#af69c1ae365f38d5c4bd0cc5f0d6a3e0b", [
      [ "SubColumnName", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#af69c1ae365f38d5c4bd0cc5f0d6a3e0bad863ecc732293ba17cfad6e93131c221", null ],
      [ "SubColumnFilePath", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#af69c1ae365f38d5c4bd0cc5f0d6a3e0baa43063d5c2d5c486c992deebd973e8d3", null ],
      [ "SubColumnSize", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#af69c1ae365f38d5c4bd0cc5f0d6a3e0baa07af85700aa1ff3679dd0aab353360d", null ],
      [ "SubColumnLastModified", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#af69c1ae365f38d5c4bd0cc5f0d6a3e0ba484f4393f23da8a81149200fef93c5f7", null ]
    ] ],
    [ "ColumnFileProperties", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a6b2cdeab233a1e2d9d8cf172519ed8a1", null ],
    [ "~ColumnFileProperties", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#aea00e4981e5ba1883e1abd6f1dc66aa6", null ],
    [ "columnAffectedByChangeset", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ac275f599c98cde40ce903a5de6aaf6f4", null ],
    [ "compare", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ae8beaa7ab97f8b30529d140aaaf18964", null ],
    [ "data", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ada9efb1f0206c1484bef92a36fbd7d71", null ],
    [ "getColumnFlags", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ab9a778a8dfe9de45af11d1d9612aff7f", null ],
    [ "getConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ace668cd97cd6d0b584332b803a0ec665", null ],
    [ "getConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a635bde231cb006fa67f67e961b7f5665", null ],
    [ "getTitle", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a57e94d9c8a8f545c5e6a8e035ae83d60", null ],
    [ "paint", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ae53896a52f61680c4263e571e19eec7f", null ],
    [ "setConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a621253bc36d74c2d30ae0765898496fe", null ],
    [ "signalAllDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ab288b91b167fc9f40490e3d41dee38ee", null ],
    [ "signalDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ac52d1ec1e5839d96de9e1b365582fdfc", null ],
    [ "sizeHint", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a3a5f8ab59b570eb69d51b0d01c452190", null ],
    [ "updateThumbnailSize", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#ac4696688718ef915e4fb096ed8a2efe3", null ],
    [ "configuration", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a1e0c6be1da4fa29ddecaf0bc07a87a37", null ],
    [ "s", "classDigikam_1_1TableViewColumns_1_1ColumnFileProperties.html#a90a53ac037c5230322f608a687680efa", null ]
];