var dir_8a95db2a777303f50e32c89a900c94a5 =
[
    [ "contextmenuhelper.cpp", "contextmenuhelper_8cpp.html", null ],
    [ "contextmenuhelper.h", "contextmenuhelper_8h.html", [
      [ "ContextMenuHelper", "classDigikam_1_1ContextMenuHelper.html", "classDigikam_1_1ContextMenuHelper" ]
    ] ],
    [ "contextmenuhelper_actions.cpp", "contextmenuhelper__actions_8cpp.html", null ],
    [ "contextmenuhelper_albums.cpp", "contextmenuhelper__albums_8cpp.html", null ],
    [ "contextmenuhelper_groups.cpp", "contextmenuhelper__groups_8cpp.html", null ],
    [ "contextmenuhelper_p.h", "contextmenuhelper__p_8h.html", [
      [ "Private", "classDigikam_1_1ContextMenuHelper_1_1Private.html", "classDigikam_1_1ContextMenuHelper_1_1Private" ]
    ] ],
    [ "contextmenuhelper_services.cpp", "contextmenuhelper__services_8cpp.html", null ],
    [ "contextmenuhelper_tags.cpp", "contextmenuhelper__tags_8cpp.html", null ],
    [ "contextmenuhelper_tools.cpp", "contextmenuhelper__tools_8cpp.html", null ],
    [ "groupingviewimplementation.cpp", "groupingviewimplementation_8cpp.html", null ],
    [ "groupingviewimplementation.h", "groupingviewimplementation_8h.html", [
      [ "GroupingViewImplementation", "classDigikam_1_1GroupingViewImplementation.html", "classDigikam_1_1GroupingViewImplementation" ]
    ] ],
    [ "itemcategorydrawer.cpp", "itemcategorydrawer_8cpp.html", null ],
    [ "itemcategorydrawer.h", "itemcategorydrawer_8h.html", [
      [ "ItemCategoryDrawer", "classDigikam_1_1ItemCategoryDrawer.html", "classDigikam_1_1ItemCategoryDrawer" ]
    ] ],
    [ "itemviewutilities.cpp", "itemviewutilities_8cpp.html", "itemviewutilities_8cpp" ],
    [ "itemviewutilities.h", "itemviewutilities_8h.html", [
      [ "ItemViewUtilities", "classDigikam_1_1ItemViewUtilities.html", "classDigikam_1_1ItemViewUtilities" ]
    ] ],
    [ "tooltipfiller.cpp", "tooltipfiller_8cpp.html", null ],
    [ "tooltipfiller.h", "tooltipfiller_8h.html", "tooltipfiller_8h" ]
];