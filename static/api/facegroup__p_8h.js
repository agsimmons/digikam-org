var facegroup__p_8h =
[
    [ "Private", "classDigikam_1_1FaceGroup_1_1Private.html", "classDigikam_1_1FaceGroup_1_1Private" ],
    [ "FaceGroupState", "facegroup__p_8h.html#aec4bd35dad3b4d1124651add3107c439", [
      [ "NoFaces", "facegroup__p_8h.html#aec4bd35dad3b4d1124651add3107c439a52f092d2979f635a0cb1caf5f3d386fb", null ],
      [ "LoadingFaces", "facegroup__p_8h.html#aec4bd35dad3b4d1124651add3107c439a9eedc3b9fa49976d3dc9f88063d90f35", null ],
      [ "FacesLoaded", "facegroup__p_8h.html#aec4bd35dad3b4d1124651add3107c439a44f8305a2a098880c7e0d030e71cd074", null ]
    ] ]
];