var dir_6c992dd443a6d10ab21d4402dc91a298 =
[
    [ "dnnfacedetectorbase.cpp", "dnnfacedetectorbase_8cpp.html", null ],
    [ "dnnfacedetectorbase.h", "dnnfacedetectorbase_8h.html", [
      [ "DNNFaceDetectorBase", "classDigikam_1_1DNNFaceDetectorBase.html", "classDigikam_1_1DNNFaceDetectorBase" ]
    ] ],
    [ "dnnfacedetectorssd.cpp", "dnnfacedetectorssd_8cpp.html", null ],
    [ "dnnfacedetectorssd.h", "dnnfacedetectorssd_8h.html", [
      [ "DNNFaceDetectorSSD", "classDigikam_1_1DNNFaceDetectorSSD.html", "classDigikam_1_1DNNFaceDetectorSSD" ]
    ] ],
    [ "dnnfacedetectoryolo.cpp", "dnnfacedetectoryolo_8cpp.html", null ],
    [ "dnnfacedetectoryolo.h", "dnnfacedetectoryolo_8h.html", [
      [ "DNNFaceDetectorYOLO", "classDigikam_1_1DNNFaceDetectorYOLO.html", "classDigikam_1_1DNNFaceDetectorYOLO" ]
    ] ],
    [ "opencvdnnfacedetector.cpp", "opencvdnnfacedetector_8cpp.html", null ],
    [ "opencvdnnfacedetector.h", "opencvdnnfacedetector_8h.html", "opencvdnnfacedetector_8h" ]
];