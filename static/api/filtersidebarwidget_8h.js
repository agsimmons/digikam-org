var filtersidebarwidget_8h =
[
    [ "FilterSideBarWidget", "classDigikam_1_1FilterSideBarWidget.html", "classDigikam_1_1FilterSideBarWidget" ],
    [ "FilterType", "filtersidebarwidget_8h.html#a3f2997dbcc8d0dc23f68b8988bca6553", [
      [ "TEXT", "filtersidebarwidget_8h.html#a3f2997dbcc8d0dc23f68b8988bca6553abb05ae37114a81ec14700568dd918f0c", null ],
      [ "MIME", "filtersidebarwidget_8h.html#a3f2997dbcc8d0dc23f68b8988bca6553a8d57ffc6b19e1dd8e32a3b66fdd8b30a", null ],
      [ "GEOLOCATION", "filtersidebarwidget_8h.html#a3f2997dbcc8d0dc23f68b8988bca6553ad72e1b212a230adbe2b7e140675ece2a", null ],
      [ "TAGS", "filtersidebarwidget_8h.html#a3f2997dbcc8d0dc23f68b8988bca6553a793bcb7fabdb56c5c85f7f9cf7508bf7", null ],
      [ "LABELS", "filtersidebarwidget_8h.html#a3f2997dbcc8d0dc23f68b8988bca6553a4a83d3dc4f61dc59c0e8e3d46855d5ab", null ]
    ] ]
];