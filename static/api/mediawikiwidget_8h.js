var mediawikiwidget_8h =
[
    [ "MediaWikiWidget", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiWidget.html", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiWidget" ],
    [ "MediaWikiDownloadType", "mediawikiwidget_8h.html#a2db873ecec4fa7eaf3f8875a65329ba0", [
      [ "MediaWikiMyAlbum", "mediawikiwidget_8h.html#a2db873ecec4fa7eaf3f8875a65329ba0ae91417caca828be8a94db228fa125df3", null ],
      [ "MediaWikiFriendAlbum", "mediawikiwidget_8h.html#a2db873ecec4fa7eaf3f8875a65329ba0ae3d284bb6aba7b89c33a905ca732a952", null ],
      [ "MediaWikiPhotosMe", "mediawikiwidget_8h.html#a2db873ecec4fa7eaf3f8875a65329ba0abc7b3b4b9b41191f9f06c3155ded73e0", null ],
      [ "MediaWikiPhotosFriend", "mediawikiwidget_8h.html#a2db873ecec4fa7eaf3f8875a65329ba0ac11d328295b4bf21f9c449985b7a318f", null ]
    ] ]
];