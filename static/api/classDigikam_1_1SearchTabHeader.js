var classDigikam_1_1SearchTabHeader =
[
    [ "SearchTabHeader", "classDigikam_1_1SearchTabHeader.html#ab28c27a6fa6b98f7fdad5e538c5fdecf", null ],
    [ "~SearchTabHeader", "classDigikam_1_1SearchTabHeader.html#a2098dfbea75b7ae9ee81e08fc37e61b2", null ],
    [ "editSearch", "classDigikam_1_1SearchTabHeader.html#a03fae830e6603544ffdadc14e4f11b15", null ],
    [ "newAdvancedSearch", "classDigikam_1_1SearchTabHeader.html#a475196bc1effc26576bde29b64a8ba2b", null ],
    [ "newKeywordSearch", "classDigikam_1_1SearchTabHeader.html#ab3e112963d96beb545200cb060011913", null ],
    [ "searchShallBeSelected", "classDigikam_1_1SearchTabHeader.html#a57aca1e817757d3133c9eb4e65a53821", null ],
    [ "selectedSearchChanged", "classDigikam_1_1SearchTabHeader.html#a295c4de5050317b0da9453b722783963", null ]
];