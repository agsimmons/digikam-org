var slice_8h =
[
    [ "sao_info", "structsao__info.html", "structsao__info" ],
    [ "slice_segment_header", "classslice__segment__header.html", "classslice__segment__header" ],
    [ "thread_task_ctb_row", "classthread__task__ctb__row.html", "classthread__task__ctb__row" ],
    [ "thread_task_slice_segment", "classthread__task__slice__segment.html", "classthread__task__slice__segment" ],
    [ "MAX_NUM_REF_PICS", "slice_8h.html#a06b79d93819596b6b6d2d13afb3aa0d0", null ],
    [ "InterPredIdc", "slice_8h.html#a63ab94ad58acadf47ec4ad41ea992fff", [
      [ "PRED_L0", "slice_8h.html#a63ab94ad58acadf47ec4ad41ea992fffa19c897ef997a32d9b0ea1483126a8a9b", null ],
      [ "PRED_L1", "slice_8h.html#a63ab94ad58acadf47ec4ad41ea992fffad7f83d462d2c61f0164727d4c44c88b4", null ],
      [ "PRED_BI", "slice_8h.html#a63ab94ad58acadf47ec4ad41ea992fffac46e96e64c5195a73ee382a0f2f5550e", null ]
    ] ],
    [ "IntraChromaPredMode", "slice_8h.html#aadb6596e97b221951d367a154a09e5d3", [
      [ "INTRA_CHROMA_PLANAR_OR_34", "slice_8h.html#aadb6596e97b221951d367a154a09e5d3af2f295c8ae5fbb1ae064cae21006974f", null ],
      [ "INTRA_CHROMA_ANGULAR_26_OR_34", "slice_8h.html#aadb6596e97b221951d367a154a09e5d3a7f64801f784c2045cae9d6a09a1f3026", null ],
      [ "INTRA_CHROMA_ANGULAR_10_OR_34", "slice_8h.html#aadb6596e97b221951d367a154a09e5d3a845a83fd480d37cc7540118263cdd761", null ],
      [ "INTRA_CHROMA_DC_OR_34", "slice_8h.html#aadb6596e97b221951d367a154a09e5d3a5fe75c092b3bbe5159ad2d515ef805dd", null ],
      [ "INTRA_CHROMA_LIKE_LUMA", "slice_8h.html#aadb6596e97b221951d367a154a09e5d3a4cb732bb2950384da374ebc3326853c8", null ]
    ] ],
    [ "IntraPredMode", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aec", [
      [ "INTRA_PLANAR", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecaed96ded95226db0d7eaefabd3a8105a9", null ],
      [ "INTRA_DC", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca39de8780b7153beb1d2cd0f42b8f5282", null ],
      [ "INTRA_ANGULAR_2", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecac9d594dcea55a118e2e9de9e096264b3", null ],
      [ "INTRA_ANGULAR_3", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca5bc0fa48efafd9b4c230b41d27d0a7ab", null ],
      [ "INTRA_ANGULAR_4", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca32b77323d87bbde8d4940359ac78dc62", null ],
      [ "INTRA_ANGULAR_5", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca57092fbe2f18ddaa7139921fce8f62fe", null ],
      [ "INTRA_ANGULAR_6", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecabe868f3451673ef5200d7ec92696b2a9", null ],
      [ "INTRA_ANGULAR_7", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca0b6c4fa51d1457f378b42a740ea87939", null ],
      [ "INTRA_ANGULAR_8", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca5af09d33cecdea0bace88208e6102edb", null ],
      [ "INTRA_ANGULAR_9", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecad2cb349c5ec762c4bd546d643bd84735", null ],
      [ "INTRA_ANGULAR_10", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca94cdd9483430f4e7377fa01dc211fba9", null ],
      [ "INTRA_ANGULAR_11", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecae0e325f16a97e4c347adc57f4520d74d", null ],
      [ "INTRA_ANGULAR_12", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca95ba7c79aa12e89ce8b2ad3f79a965b4", null ],
      [ "INTRA_ANGULAR_13", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca5c604470e80a17e0b3a6c32e5be78052", null ],
      [ "INTRA_ANGULAR_14", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca4553f59831c9f19f4d4a0a6480e4047c", null ],
      [ "INTRA_ANGULAR_15", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecaec97962be3557adfd70f1f72eb757536", null ],
      [ "INTRA_ANGULAR_16", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecadf578e1841cacc58eb34a5ed71194899", null ],
      [ "INTRA_ANGULAR_17", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca0f8f692429f8bf410a270f4a142707f2", null ],
      [ "INTRA_ANGULAR_18", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecacfcb50cbf248bd351816c17b9ea50ec6", null ],
      [ "INTRA_ANGULAR_19", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca9e9e932a7410b6b44411548a51cbfd0d", null ],
      [ "INTRA_ANGULAR_20", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecacccb566b10cc6bb9b366f01f5bf3615f", null ],
      [ "INTRA_ANGULAR_21", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca71868f1a54dc501bf0fdb4ab65af83c7", null ],
      [ "INTRA_ANGULAR_22", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca0390741c553b867e16668b3b3322e251", null ],
      [ "INTRA_ANGULAR_23", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca8970dd5448e702cc08f4020a12ed5bed", null ],
      [ "INTRA_ANGULAR_24", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecad2993539ce37be31754099b87442e796", null ],
      [ "INTRA_ANGULAR_25", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecacb77fd42a766225c1173b96524a66aee", null ],
      [ "INTRA_ANGULAR_26", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca26fde73122eb4fbd813ad1ff51f22cac", null ],
      [ "INTRA_ANGULAR_27", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecaf2e8f56784bd60641272f892acb62191", null ],
      [ "INTRA_ANGULAR_28", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecae327922b403b215805c30a3f2ca856f5", null ],
      [ "INTRA_ANGULAR_29", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecaa93d601b06696e5b866c6f351d516870", null ],
      [ "INTRA_ANGULAR_30", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecadd6cd39e3239f312a17f7def1e92e422", null ],
      [ "INTRA_ANGULAR_31", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aeca29df32da4a267212bb9d4b4f3530302e", null ],
      [ "INTRA_ANGULAR_32", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecaf0c5aa9569e7af4bdacb22804121670a", null ],
      [ "INTRA_ANGULAR_33", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecaba922dfe959ac59d1a4cd43450c5592e", null ],
      [ "INTRA_ANGULAR_34", "slice_8h.html#a59fe1b30a50c6a1d90c35cb53e8c0aecab389ecbf15aec217dccd06aa6f38d6df", null ]
    ] ],
    [ "PartMode", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406", [
      [ "PART_2Nx2N", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406a3bb7400a61064e012c8d7d7b5bbb8378", null ],
      [ "PART_2NxN", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406a94583e88fe62f7800399169dfe01e419", null ],
      [ "PART_Nx2N", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406a53a0dec3ba58cbeac1bf44832ac937cc", null ],
      [ "PART_NxN", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406a6e92b99db1b193e51d223bb3b2407493", null ],
      [ "PART_2NxnU", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406ab9590380c633822798954a8a42f27b28", null ],
      [ "PART_2NxnD", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406a7055a9b28e3828e8efa1cac2f8fc605d", null ],
      [ "PART_nLx2N", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406a70d9db5f8af42e0e7c2f062e9a2b0d26", null ],
      [ "PART_nRx2N", "slice_8h.html#a34b49f37eb8ff25e85c4c2862841e406ac2db1a3a14e51c380c7790a2c370a4ca", null ]
    ] ],
    [ "PredMode", "slice_8h.html#a33d1666f1cc95252c96e39584255954e", [
      [ "MODE_INTRA", "slice_8h.html#a33d1666f1cc95252c96e39584255954ea42abd999609e7e092f3a02a02d8f6568", null ],
      [ "MODE_INTER", "slice_8h.html#a33d1666f1cc95252c96e39584255954eaba4c54773c9f75ca10798034f712cc5f", null ],
      [ "MODE_SKIP", "slice_8h.html#a33d1666f1cc95252c96e39584255954eadbbc3fc0f847706128368eea7dcc5441", null ]
    ] ],
    [ "SliceType", "slice_8h.html#a8fc5fd31653a387f7430d29863620f71", [
      [ "SLICE_TYPE_B", "slice_8h.html#a8fc5fd31653a387f7430d29863620f71af01d3fe73814ae73c11856780f71867b", null ],
      [ "SLICE_TYPE_P", "slice_8h.html#a8fc5fd31653a387f7430d29863620f71a66eee1f2188c49c514a9a6f513295af2", null ],
      [ "SLICE_TYPE_I", "slice_8h.html#a8fc5fd31653a387f7430d29863620f71a1a6eda0b61624660a13a6d9bc2429ed5", null ]
    ] ],
    [ "alloc_and_init_significant_coeff_ctxIdx_lookupTable", "slice_8h.html#aa56104f617672b84c6e94bcaf3c2d2f0", null ],
    [ "check_CTB_available", "slice_8h.html#af057387777d67334648957574183480c", null ],
    [ "free_significant_coeff_ctxIdx_lookupTable", "slice_8h.html#a79edb5280d40bb5f8186e89a34105011", null ],
    [ "part_mode_name", "slice_8h.html#a58a87e792026b389b9a5fc6934bceebb", null ],
    [ "read_slice_segment_data", "slice_8h.html#a593d13c14fb41bea1188ae661cb15dfb", null ]
];