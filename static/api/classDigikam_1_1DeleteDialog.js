var classDigikam_1_1DeleteDialog =
[
    [ "Mode", "classDigikam_1_1DeleteDialog.html#a50478a9d97579e88492ad83cbe59f3c3", [
      [ "ModeFiles", "classDigikam_1_1DeleteDialog.html#a50478a9d97579e88492ad83cbe59f3c3ae7b41accb50bd259c261028fdb569a8a", null ],
      [ "ModeAlbums", "classDigikam_1_1DeleteDialog.html#a50478a9d97579e88492ad83cbe59f3c3ad5b062251249c62c6555a02a57288290", null ],
      [ "ModeSubalbums", "classDigikam_1_1DeleteDialog.html#a50478a9d97579e88492ad83cbe59f3c3a246e4bf3b2057378d4c97dc42a8a56f2", null ]
    ] ],
    [ "DeleteDialog", "classDigikam_1_1DeleteDialog.html#a7e885d3f0e42d3d4bd9f84ee05d315bb", null ],
    [ "~DeleteDialog", "classDigikam_1_1DeleteDialog.html#a47a6700e9c83d94d64a6d7e6858c0efd", null ],
    [ "confirmDeleteList", "classDigikam_1_1DeleteDialog.html#ac22dc2eb0271b8f02080f88a4768b60f", null ],
    [ "presetDeleteMode", "classDigikam_1_1DeleteDialog.html#a050b4989de55b4091198ffbc6eb8c5a1", null ],
    [ "setListMode", "classDigikam_1_1DeleteDialog.html#aa099d8e0934bdf0540bdf48ca91f045e", null ],
    [ "setUrls", "classDigikam_1_1DeleteDialog.html#ac4293d615f03ab5ba559e913b1c9ce60", null ],
    [ "shouldDelete", "classDigikam_1_1DeleteDialog.html#a55ff954dec4ff8d912a4764f1435b172", null ],
    [ "slotShouldDelete", "classDigikam_1_1DeleteDialog.html#a62f2dafc485899f2c5f101e2214f8749", null ],
    [ "slotUser1Clicked", "classDigikam_1_1DeleteDialog.html#ae88e66ef4e18b9ae3cbf8efc937ed986", null ]
];