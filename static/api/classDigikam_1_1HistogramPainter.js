var classDigikam_1_1HistogramPainter =
[
    [ "HistogramPainter", "classDigikam_1_1HistogramPainter.html#a8662e28141466466d957c9e3612e5bdb", null ],
    [ "~HistogramPainter", "classDigikam_1_1HistogramPainter.html#ae16f26c0b936ce36cf4f745c76bcb934", null ],
    [ "disableHistogramGuide", "classDigikam_1_1HistogramPainter.html#ae522d546f9672ba8937caed08d7c8fa9", null ],
    [ "enableHistogramGuideByColor", "classDigikam_1_1HistogramPainter.html#aeca524991d843a3662d3f6cbc68b3489", null ],
    [ "initFrom", "classDigikam_1_1HistogramPainter.html#adce3dc3c07e96a98d90dedd2c59a6da6", null ],
    [ "render", "classDigikam_1_1HistogramPainter.html#adca91a67893bac88b61d8abac541afc4", null ],
    [ "setChannelType", "classDigikam_1_1HistogramPainter.html#a3d183d7ed45f9af213f4213dbb65fa21", null ],
    [ "setHighlightSelection", "classDigikam_1_1HistogramPainter.html#aa9f743be29b4414d0eb4fd3f52e90eaf", null ],
    [ "setHistogram", "classDigikam_1_1HistogramPainter.html#ab23f29f83f0873ab308640c2d85bf865", null ],
    [ "setRenderXGrid", "classDigikam_1_1HistogramPainter.html#a0ab998f508e758e5511c032c25fadf2e", null ],
    [ "setScale", "classDigikam_1_1HistogramPainter.html#a303851b299a0dfb58c4b240ff8bd6c30", null ],
    [ "setSelection", "classDigikam_1_1HistogramPainter.html#aaf892b988c5b71e0e47c7d55dfff4c57", null ]
];