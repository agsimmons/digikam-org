var searchData=
[
  ['backend_51439',['Backend',['../classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5',1,'Digikam::MetaEngine::Backend()'],['../classDigikam_1_1MetadataSelectorView.html#a5add397420999ffb1b1ec21f3f9a4de3',1,'Digikam::MetadataSelectorView::Backend()']]],
  ['backgroundmode_51440',['BackgroundMode',['../classDigikam_1_1DDateTable.html#abad90fe30bfa61e394d8a63447b6c4ff',1,'Digikam::DDateTable']]],
  ['backgroundrole_51441',['BackgroundRole',['../classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150f',1,'Digikam::SchemeManager']]],
  ['barmode_51442',['BarMode',['../classDigikam_1_1DZoomBar.html#a78d17afad80511fd045c09407f471cac',1,'Digikam::DZoomBar']]],
  ['batchtoolgroup_51443',['BatchToolGroup',['../classDigikam_1_1BatchTool.html#afa76b46ac346747b289ce17be3124a72',1,'Digikam::BatchTool']]],
  ['behaviorenum_51444',['BehaviorEnum',['../classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496',1,'Digikam::ICCSettingsContainer']]],
  ['bitdepth_51445',['BitDepth',['../structDigikam_1_1PTOType_1_1Project.html#a8a7ac0658dc61b54cde149e9b3dcf324',1,'Digikam::PTOType::Project']]],
  ['blackframeconst_51446',['BlackFrameConst',['../classDigikam_1_1BlackFrameListViewItem.html#a370eaf55a663ad069a11777bcd4cda65',1,'Digikam::BlackFrameListViewItem']]],
  ['blackwhiteconversiontype_51447',['BlackWhiteConversionType',['../classDigikam_1_1BWSepiaContainer.html#a21f1935856e3952e3264fd687715bea6',1,'Digikam::BWSepiaContainer']]],
  ['blurfxfiltertypes_51448',['BlurFXFilterTypes',['../classDigikam_1_1BlurFXFilter.html#a649ee415d57bb90ca9e4047e312a8a52',1,'Digikam::BlurFXFilter']]],
  ['bookmarkdata_51449',['BookmarkData',['../classDigikam_1_1ChangeBookmarkCommand.html#a2e8607a693dae805e48b13ee5d70aad4',1,'Digikam::ChangeBookmarkCommand']]],
  ['bordermode_51450',['BorderMode',['../classDigikamEditorInsertTextToolPlugin_1_1InsertTextWidget.html#a11841962311a8b2097f6ff4d2d089941',1,'DigikamEditorInsertTextToolPlugin::InsertTextWidget']]],
  ['bordertypes_51451',['BorderTypes',['../classDigikam_1_1BorderContainer.html#a6506fab8688ffe9502efa1eb43db3895',1,'Digikam::BorderContainer']]],
  ['buttoncode_51452',['ButtonCode',['../classDigikam_1_1EditorToolSettings.html#a4efe6d4d27cde9dfb69ca45119269742',1,'Digikam::EditorToolSettings']]]
];
