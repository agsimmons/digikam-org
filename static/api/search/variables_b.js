var searchData=
[
  ['kbdisablecrossfade_48898',['kbDisableCrossFade',['../classDigikamGenericPresentationPlugin_1_1PresentationContainer.html#a6cae7624de7280a8c7fa77e0c0e71c81',1,'DigikamGenericPresentationPlugin::PresentationContainer']]],
  ['kbdisablefadeinout_48899',['kbDisableFadeInOut',['../classDigikamGenericPresentationPlugin_1_1PresentationContainer.html#a7145b894391f7c68d59342befa3f6e07',1,'DigikamGenericPresentationPlugin::PresentationContainer']]],
  ['kbenablesamespeed_48900',['kbEnableSameSpeed',['../classDigikamGenericPresentationPlugin_1_1PresentationContainer.html#aa0b2feb8e9195cf97f58080e6ddcb405',1,'DigikamGenericPresentationPlugin::PresentationContainer']]],
  ['keep_48901',['keep',['../structimage__data.html#aec2a9fb1fd9626b6af24fc2f2f983b6f',1,'image_data']]],
  ['keepnbest_48902',['keepNBest',['../structAlgo__TB__IntraPredMode__FastBrute_1_1params.html#af475a5d98c4248a6c558e0691fb0fe5f',1,'Algo_TB_IntraPredMode_FastBrute::params']]],
  ['kernel_5fsize_48903',['kernel_size',['../classDigikam_1_1ImageQualityParser_1_1Private.html#a402afb582b1f142ebd44d6922ea6270d',1,'Digikam::ImageQualityParser::Private']]],
  ['key_48904',['key',['../classDigikam_1_1CachedPixmapKey.html#ae349fbe65541a82daf0308bcc64e0eb0',1,'Digikam::CachedPixmapKey::key()'],['../classDigikam_1_1RuleType.html#a51ee8a9833e3676781625b38570324ef',1,'Digikam::RuleType::key()'],['../classDigikam_1_1RuleTypeForConversion.html#acf72b209c75a09b5050ec6e53f308b15',1,'Digikam::RuleTypeForConversion::key()'],['../classDigikamGenericSmugPlugin_1_1SmugPhoto.html#a33a5293698021dd96449616f618dccb0',1,'DigikamGenericSmugPlugin::SmugPhoto::key()'],['../classDigikamGenericSmugPlugin_1_1SmugAlbum.html#a1e7901cfcf0b7c67817963a72d800162',1,'DigikamGenericSmugPlugin::SmugAlbum::key()']]],
  ['keys_48905',['keys',['../classDigikam_1_1MetaEngineMergeHelper.html#a2bf7a539f44cf2cd3d75f1a6ec8ae220',1,'Digikam::MetaEngineMergeHelper::keys()'],['../classDigikam_1_1CachedPixmaps.html#a29bd03c3ecb4652970f83f1fd5be8508',1,'Digikam::CachedPixmaps::keys()']]],
  ['keywords_48906',['keywords',['../classDigikamGenericSmugPlugin_1_1SmugAlbum.html#a61444db7b5ba6ea87461564a41998dc9',1,'DigikamGenericSmugPlugin::SmugAlbum::keywords()'],['../classDigikamGenericSmugPlugin_1_1SmugPhoto.html#a10163c43bbda7f19511d7f4097a42877',1,'DigikamGenericSmugPlugin::SmugPhoto::keywords()']]],
  ['kmlexportconfiglayout_48907',['KMLExportConfigLayout',['../classDigikamGenericGeolocationEditPlugin_1_1KmlWidget.html#aeea55d2fb1118b212289e2564c9af554',1,'DigikamGenericGeolocationEditPlugin::KmlWidget']]],
  ['kneighbors_48908',['kNeighbors',['../classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#adb8295b8cf06268ee8bed3e3547fbf34',1,'Digikam::OpenCVDNNFaceRecognizer::Private']]],
  ['knn_48909',['knn',['../classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html#aa59e934f923b8e461a9c157a85aac9bc',1,'Digikam::OpenCVDNNFaceRecognizer::Private']]],
  ['knownfaces_48910',['knownFaces',['../classDigikam_1_1RecognitionBenchmarker_1_1Statistics.html#a0619e217b1236d87a775b2400edb3e63',1,'Digikam::RecognitionBenchmarker::Statistics']]],
  ['ksuccess_48911',['kSuccess',['../classheif_1_1Error.html#a0da1c2086b153b817021b89b03303abe',1,'heif::Error']]]
];
