var searchData=
[
  ['r_5fval_55167',['R_VAL',['../dimg__scale_8cpp.html#a5fa852af724b8a35fc6dafd70e023bb8',1,'dimg_scale.cpp']]],
  ['r_5fval16_55168',['R_VAL16',['../dimg__scale_8cpp.html#a8db3a8870f021dabca3d4506bb20e98d',1,'dimg_scale.cpp']]],
  ['radian2degree_55169',['RADIAN2DEGREE',['../perspectivetriangle_8cpp.html#a8eb1ffbf3f5a6cd315bf77f6a4f24e7e',1,'perspectivetriangle.cpp']]],
  ['rcol_55170',['RCOL',['../ratiocropwidget_8cpp.html#ab3de5d965e759661a04d7560b2b5ce1f',1,'ratiocropwidget.cpp']]],
  ['readparameter_55171',['ReadParameter',['../drawdecoding_8cpp.html#ac8016df19278a9062214092f832dc2be',1,'drawdecoding.cpp']]],
  ['readparameterenum_55172',['ReadParameterEnum',['../drawdecoding_8cpp.html#aa941de6156622cc7e0849573c69e68aa',1,'drawdecoding.cpp']]],
  ['readparameterenumwithvalue_55173',['ReadParameterEnumWithValue',['../drawdecoding_8cpp.html#a3d4433fc99b77676ab8d70ed830b0738',1,'drawdecoding.cpp']]],
  ['readparameterwithvalue_55174',['ReadParameterWithValue',['../drawdecoding_8cpp.html#a4675ba339b12707a4836d7fb7ceff485',1,'drawdecoding.cpp']]],
  ['rel_5fto_5fabs_55175',['REL_TO_ABS',['../blackframeparser_8cpp.html#ab25c981c2833da4818747d557918743e',1,'blackframeparser.cpp']]],
  ['require_5fdelegate_55176',['REQUIRE_DELEGATE',['../itemdelegateoverlay_8h.html#aed8d79308e627d1bacd5b2a6ddbe15db',1,'itemdelegateoverlay.h']]],
  ['return_5faspectratio_5fif_5fimagesize_5fcached_55177',['RETURN_ASPECTRATIO_IF_IMAGESIZE_CACHED',['../iteminfo__p_8h.html#a5703bfedcf1b63b6936763ee3e68cc7b',1,'iteminfo_p.h']]],
  ['return_5fif_5fcached_55178',['RETURN_IF_CACHED',['../iteminfo__p_8h.html#a9581e9bb5d4ed330a6e10ab339c6f4e9',1,'iteminfo_p.h']]]
];
