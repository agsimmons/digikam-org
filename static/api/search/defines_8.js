var searchData=
[
  ['has_5fbool_5farray_54995',['HAS_BOOL_ARRAY',['../box_8h.html#ad76465503af1a67204dbaddc8c7320f2',1,'box.h']]],
  ['hcy_5frec_54996',['HCY_REC',['../thememanager__p_8cpp.html#a0dcaab1ad25cbf594c69d1fae39fbd72',1,'thememanager_p.cpp']]],
  ['heif_5fchroma_5finterleaved_5f24bit_54997',['heif_chroma_interleaved_24bit',['../heif_8h.html#a9e90c9eba60a62fbedf5e98f71616a62',1,'heif.h']]],
  ['heif_5fchroma_5finterleaved_5f32bit_54998',['heif_chroma_interleaved_32bit',['../heif_8h.html#ae1dc02ae4b4c3e45fad116964f825f4e',1,'heif.h']]],
  ['heif_5fencoder_5fparameter_5fname_5flossless_54999',['heif_encoder_parameter_name_lossless',['../heif__plugin_8h.html#abd7490354006015fcf1d87cdc5d7d606',1,'heif_plugin.h']]],
  ['heif_5fencoder_5fparameter_5fname_5fquality_55000',['heif_encoder_parameter_name_quality',['../heif__plugin_8h.html#a2dd033395e00959a6929b42ce7a36091',1,'heif_plugin.h']]],
  ['heif_5ffourcc_55001',['heif_fourcc',['../heif_8h.html#ac0046411ae08aae6966f6c4efa68ce78',1,'heif.h']]],
  ['hi_55002',['hi',['../yfrsa_8cpp.html#ae88235d9c89d1d2f917247afb3e03ef0',1,'yfrsa.cpp']]],
  ['histogram_5fcalc_5fcutoff_5fheight_55003',['HISTOGRAM_CALC_CUTOFF_HEIGHT',['../histogrampainter_8cpp.html#a234af3e901da9a80730376b3592dfa1f',1,'histogrampainter.cpp']]],
  ['histogram_5fcalc_5fcutoff_5fmax_55004',['HISTOGRAM_CALC_CUTOFF_MAX',['../histogrampainter_8cpp.html#ae7bd40e1361402c476f68f896b7aa4ec',1,'histogrampainter.cpp']]],
  ['histogram_5fcalc_5fcutoff_5fmin_55005',['HISTOGRAM_CALC_CUTOFF_MIN',['../histogrampainter_8cpp.html#a78b87fe7ee9c80a6aa4f5c2f91207180',1,'histogrampainter.cpp']]]
];
