var searchData=
[
  ['labelandvaluewidgetrects_53364',['LabelAndValueWidgetRects',['../classDigikam_1_1SearchField.html#ab55a3e2d7188c11703e21cd7ffd7a5cda25b179a8af9718b2df923a2242204713',1,'Digikam::SearchField']]],
  ['labels_53365',['LABELS',['../namespaceDigikam.html#a3f2997dbcc8d0dc23f68b8988bca6553a4a83d3dc4f61dc59c0e8e3d46855d5ab',1,'Digikam']]],
  ['landscape_53366',['Landscape',['../classDigikamEditorRatioCropToolPlugin_1_1RatioCropWidget.html#a4dd02ad89615ca228be6f4df46d7dad0a74dc6f9399725ac3ccebb9fc7f1fdfb2',1,'DigikamEditorRatioCropToolPlugin::RatioCropWidget']]],
  ['large_53367',['Large',['../classDigikam_1_1ThumbnailSize.html#a88b979cc5b6ecbbf07e60075d7961d9ba7f5f42274fb5124eb2f873443413752b',1,'Digikam::ThumbnailSize']]],
  ['largestarea_53368',['LargestArea',['../classDigikam_1_1FreeRotationContainer.html#ab820d7c67b425b31ffc379de64d65f6baca890c6b25800be5b7e50406afe797a3',1,'Digikam::FreeRotationContainer']]],
  ['lasso_5fclone_53369',['LASSO_CLONE',['../classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneToolWidget.html#a706de8e2fb2bb3db9fabfe666c447b13a0bced0ee51969d82b301d0e6f50c9dcb',1,'DigikamEditorHealingCloneToolPlugin::HealingCloneToolWidget']]],
  ['lasso_5fdraw_5fboundary_53370',['LASSO_DRAW_BOUNDARY',['../classDigikamEditorHealingCloneToolPlugin_1_1HealingCloneToolWidget.html#a706de8e2fb2bb3db9fabfe666c447b13a92da8caef8a3d64d87ec95db23f1e35a',1,'DigikamEditorHealingCloneToolPlugin::HealingCloneToolWidget']]],
  ['lastcalendar_53371',['LastCalendar',['../classDigikamGenericCalendarPlugin_1_1CalSystem.html#a78c28a0478c097e672daed3393acd6e6a4456b412ff8ec86f662d438b29f8fa32',1,'DigikamGenericCalendarPlugin::CalSystem']]],
  ['lastcolorlabel_53372',['LastColorLabel',['../namespaceDigikam.html#ac8132466ce37ded0144ffa0ea3d01b35a0e41a3e745eb1c799d8cd2130ecb0718',1,'Digikam']]],
  ['lastpageused_53373',['LastPageUsed',['../classShowFoto_1_1ShowfotoSetup.html#a266c2be13ba5b63ce9e543963439d4a9ab722c5ad83756ad0b9589701723a23eb',1,'ShowFoto::ShowfotoSetup::LastPageUsed()'],['../classDigikam_1_1Setup.html#a9a178847dc2429f1e7a44e4cbebbefb3a696c93fb19a4aa67657f28b16a5a341a',1,'Digikam::Setup::LastPageUsed()']]],
  ['lastpicklabel_53374',['LastPickLabel',['../namespaceDigikam.html#af3f0d4e1b18c8ca68a3f3c7d5fe7563fa1e0eaaba4762e75eabe2dcc5ed22a9aa',1,'Digikam']]],
  ['latitude_53375',['Latitude',['../namespaceDigikam_1_1MetadataInfo.html#aa919f13795e9cff09f6271192e05d02ea75b9391f1321fb4f938d1804ca186182',1,'Digikam::MetadataInfo::Latitude()'],['../namespaceDigikam_1_1DatabaseFields.html#ad03131aab54d50f7885f5ed1ff3498daa50d67a0e6f95247dc7ccec0cfdd04963',1,'Digikam::DatabaseFields::Latitude()']]],
  ['latitudenumber_53376',['LatitudeNumber',['../namespaceDigikam_1_1MetadataInfo.html#aa919f13795e9cff09f6271192e05d02ea46177dd3f24131c20c9faea78e07d996',1,'Digikam::MetadataInfo::LatitudeNumber()'],['../namespaceDigikam_1_1DatabaseFields.html#ad03131aab54d50f7885f5ed1ff3498daa70619e55045806885f4797f998491cd9',1,'Digikam::DatabaseFields::LatitudeNumber()']]],
  ['layoutcompact_53377',['LayoutCompact',['../classDigikam_1_1AdvancedRenameWidget.html#acc53420d2facce63a63b6791fa4e28c9ae1e092a9354c4360e5971fbb61eb7eb2',1,'Digikam::AdvancedRenameWidget']]],
  ['layoutnormal_53378',['LayoutNormal',['../classDigikam_1_1AdvancedRenameWidget.html#acc53420d2facce63a63b6791fa4e28c9a5b36e13c212138b3c985d88c716ea7ed',1,'Digikam::AdvancedRenameWidget']]],
  ['leaf_53379',['Leaf',['../classimage__unit.html#ab5f639a367e75f35101a24c72458f258a47e5e2f0ab5ecc06faf1837c631d336c',1,'image_unit']]],
  ['leafborder_53380',['LeafBorder',['../classDigikam_1_1BorderContainer.html#a6506fab8688ffe9502efa1eb43db3895a78308ca5012da59ba27b47e1ee977f28',1,'Digikam::BorderContainer']]],
  ['leavefileuntagged_53381',['LeaveFileUntagged',['../classDigikam_1_1ICCSettingsContainer.html#a024c61e48a2d780a1036c7758dac0496a48d147d853de883b1562e5c917b0f06b',1,'Digikam::ICCSettingsContainer']]],
  ['left_53382',['Left',['../structDigikamGenericCalendarPlugin_1_1CalParams.html#a02c255e420217b9b6d87b6d24de4ce7da562e97d8a195dfd2dafd1507fae769b4',1,'DigikamGenericCalendarPlugin::CalParams::Left()'],['../classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8acb8510ed44c53af3ba25c0bd3a963d61',1,'Digikam::DSelectionItem::Left()']]],
  ['legacyurlsearch_53383',['LegacyUrlSearch',['../namespaceDigikam_1_1DatabaseSearch.html#ab820f1f91c8c246037cb90571385df46a3ffb5cf9edb48472fe6e1c22670dc663',1,'Digikam::DatabaseSearch']]],
  ['lens_53384',['Lens',['../namespaceDigikam_1_1DatabaseFields.html#aded14432fbbe68802ba100b85660cda4ac9a82cfdadcddfbc42b25c4840eb775e',1,'Digikam::DatabaseFields::Lens()'],['../namespaceDigikam_1_1MetadataInfo.html#aa919f13795e9cff09f6271192e05d02eacd60ce90620c0cb1379214806a3ddf5d',1,'Digikam::MetadataInfo::Lens()']]],
  ['lensa_53385',['LENSA',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda2e062e09d7bce441b578cb599353a0bb',1,'Digikam::PTOType::Optimization']]],
  ['lensb_53386',['LENSB',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda0dc40df676e014ba15cc80818ec57dfe',1,'Digikam::PTOType::Optimization']]],
  ['lensc_53387',['LENSC',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edac243438801402567088dcb59d4aa6612',1,'Digikam::PTOType::Optimization']]],
  ['lensd_53388',['LENSD',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62eda6066203c8b3cc844a814b99a4ad38230',1,'Digikam::PTOType::Optimization']]],
  ['lense_53389',['LENSE',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edaf316436a5202c243ac0b1cf0ee2e9acd',1,'Digikam::PTOType::Optimization']]],
  ['lenshfov_53390',['LENSHFOV',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edad9463d2b2439bad674a4628f8d8750ba',1,'Digikam::PTOType::Optimization']]],
  ['lenspitch_53391',['LENSPITCH',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edad1e37a1fb84bb045837387bc79ebfac1',1,'Digikam::PTOType::Optimization']]],
  ['lensroll_53392',['LENSROLL',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edaf65b7dac00b2df0faa85b993da376648',1,'Digikam::PTOType::Optimization']]],
  ['lensyaw_53393',['LENSYAW',['../structDigikam_1_1PTOType_1_1Optimization.html#a33473a410cc338c4dbc77ab17f1a62edadbe1394c6d96db41ac1c5545074cf474',1,'Digikam::PTOType::Optimization']]],
  ['lessequalcondition_53394',['LessEqualCondition',['../classDigikam_1_1ItemFilterSettings.html#a7a8d0fb01744ac1621cd9d35f43d011daaccd9bc116d54874a085ac8c13e92436',1,'Digikam::ItemFilterSettings']]],
  ['lessthan_53395',['LessThan',['../namespaceDigikam_1_1SearchXml.html#a15c0ad590e52942210ba80d980770daeab1d4fa924602bc2b562b14878bd1ece4',1,'Digikam::SearchXml']]],
  ['lessthanorequal_53396',['LessThanOrEqual',['../namespaceDigikam_1_1SearchXml.html#a15c0ad590e52942210ba80d980770daea3c4ccc2e516a49b8543924991dcc11bc',1,'Digikam::SearchXml']]],
  ['libheifbackend_53397',['LibHeifBackend',['../classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5af09b03b6315b969b4aab4f116160bb2d',1,'Digikam::MetaEngine']]],
  ['librawbackend_53398',['LibRawBackend',['../classDigikam_1_1MetaEngine.html#ac73cdee8a55f454a4bdd33185e429de5afa52b4b531cb599206fb43be7d7b4d51',1,'Digikam::MetaEngine']]],
  ['lightshade_53399',['LightShade',['../classDigikam_1_1SchemeManager.html#ad2bbe6dffc96d6786b843de2d78b5a57a8320256333cd376e0ef7a9d248978e32',1,'Digikam::SchemeManager']]],
  ['lighttable_53400',['LightTable',['../classDigikam_1_1ApplicationSettings.html#aacfff1cf077fd0c86393da50ac6f9821ab05f240cd8b3111e2df96afcfca57474',1,'Digikam::ApplicationSettings']]],
  ['lighttablepage_53401',['LightTablePage',['../classDigikam_1_1Setup.html#a9a178847dc2429f1e7a44e4cbebbefb3a82c7ee6700a1ef02ddf93b7ca36aec80',1,'Digikam::Setup']]],
  ['lighttablepreview_53402',['LightTablePreview',['../classDigikam_1_1ItemPreviewView.html#a23e3f9945f9048ff0299301f2bfb0495a4c6731c218e5e9bb8b950cba7e169a33',1,'Digikam::ItemPreviewView']]],
  ['like_53403',['Like',['../namespaceDigikam_1_1SearchXml.html#a15c0ad590e52942210ba80d980770daea2fe6feb154c62c702e47b8bdf4dda83d',1,'Digikam::SearchXml']]],
  ['like_53404',['LIKE',['../namespaceDigikam.html#ad433e183fd169903b66270e16b6ada18af9cfe745acd6da49be7c8f1d4dd1eb21',1,'Digikam']]],
  ['linear_53405',['Linear',['../classDigikam_1_1GreycstorationContainer.html#abb96d7c8315bebb0736228b773650eeba38b67edae82c3ce6cf40c8de08e0a4c1',1,'Digikam::GreycstorationContainer']]],
  ['linear_5finterpolation_53406',['LINEAR_INTERPOLATION',['../classDigikam_1_1HotPixelContainer.html#a88c6c556c4a424298666431c21bf8bc5ad6c09cb4b86be94d0ee8bf359804fc1e',1,'Digikam::HotPixelContainer']]],
  ['linearraw_53407',['LinearRaw',['../classDigikam_1_1DNGWriter_1_1Private.html#a590017a36ce8c5c0893d7d18c9b83d26a93de9861ec34be3814642cfe03f81e62',1,'Digikam::DNGWriter::Private']]],
  ['link_53408',['Link',['../classDigikam_1_1DBinarySearch.html#a3018081e9b7f9d611c47ff9051c1fb8faab324411bf6d2724d6e27927e82cb6e8',1,'Digikam::DBinarySearch']]],
  ['linkbackground_53409',['LinkBackground',['../classDigikam_1_1SchemeManager.html#a532e95d6da39f0cbb927b0306df8150fa7acd9442691e9579a54d9e6eb00fb1ce',1,'Digikam::SchemeManager']]],
  ['linktext_53410',['LinkText',['../classDigikam_1_1SchemeManager.html#a90090791f7cd3442b83d9cba3d0f415caccaf121b6600edaaaade58ff80cf7962',1,'Digikam::SchemeManager']]],
  ['linscale_53411',['LinScale',['../classDigikam_1_1TimeLineWidget.html#a4dfaa0ab71a7dfa59b716df7fcda2b7ba58430d5ddf3460a7c5a1716ea4d4353b',1,'Digikam::TimeLineWidget']]],
  ['linscalehistogram_53412',['LinScaleHistogram',['../namespaceDigikam.html#a7252dea9fd7f36c45149da08748a2cb9a3d4480610fb36371f1a7393e780e18f5',1,'Digikam']]],
  ['list_53413',['List',['../classDigikam_1_1DConfigDlgView.html#acbf94364f599dcf27b5f7bc1477c7dcba819e1e9816cc221289c19c09db51a324',1,'Digikam::DConfigDlgView::List()'],['../classDigikam_1_1DConfigDlg.html#afd9f829025448877fda08371cd77d3faadc000fd9961b3cbeaf26806634783767',1,'Digikam::DConfigDlg::List()']]],
  ['listalbums_53414',['LISTALBUMS',['../classDigikamGenericUnifiedPlugin_1_1WSTalker.html#aa90cd32ee635027297ad4ca0b8b66236ae89b11789c47ee6f8d0e8a1e3c4435e6',1,'DigikamGenericUnifiedPlugin::WSTalker']]],
  ['listalbums_53415',['ListAlbums',['../namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89aa2b8965eb1316ad495a526603e02c91d',1,'DigikamGenericRajcePlugin']]],
  ['load_53416',['Load',['../classDigikam_1_1EditorToolSettings.html#a4efe6d4d27cde9dfb69ca45119269742a60b10f373e68e31874c15c8bdf3ed669',1,'Digikam::EditorToolSettings::Load()'],['../classDigikam_1_1DItemsList.html#a7f3cda83c47c4c04663ca722c8d6bb9aacba89c4546a86a29e65fdb34e0cad4ed',1,'Digikam::DItemsList::Load()']]],
  ['load_5fchunks_53417',['LOAD_CHUNKS',['../classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781a17d7a97506af98f91683605fbf32c2bc',1,'Digikam::ExifToolProcess']]],
  ['load_5fmetadata_53418',['LOAD_METADATA',['../classDigikam_1_1ExifToolProcess.html#af42710740c1ed0d654c5e67d6ed0a781ad7a1da63059b3908ad7eb9306de7282b',1,'Digikam::ExifToolProcess']]],
  ['loadall_53419',['LoadAll',['../classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22a1a9d2d8bf1692af4a7c3a16c954055a4',1,'Digikam::ItemHistoryGraph::LoadAll()'],['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dcad957bf24a0effac764624fe13d19b858',1,'Digikam::DImgLoader::LoadAll()']]],
  ['loaders_53420',['Loaders',['../classShowFoto_1_1ShowfotoSetupPlugins.html#a2b97a455bc8ea1ad58b5ddc14f132777ab87dd05e14e934fd092302703725f252',1,'ShowFoto::ShowfotoSetupPlugins::Loaders()'],['../classDigikam_1_1SetupPlugins.html#a193d52e2726be869fe8a2cf233a1ca3ea8c81c3a8a2e79252083d998c7897975d',1,'Digikam::SetupPlugins::Loaders()']]],
  ['loadiccdata_53421',['LoadICCData',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dca1e2268104a5cc7d1f676793aac48c199',1,'Digikam::DImgLoader']]],
  ['loadimagedata_53422',['LoadImageData',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dcaccd0ff209e94ada40a8bb0d21af39a18',1,'Digikam::DImgLoader']]],
  ['loadimagehistory_53423',['LoadImageHistory',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dca90ba4f4b023f1c1fc1de3d1ed346e12e',1,'Digikam::DImgLoader']]],
  ['loading_53424',['Loading',['../classDigikam_1_1DImgPreviewItem.html#a4aabff81ae476bd278eec97eac7e1550a6e814ee27045ac9ff36678b417cff6d0',1,'Digikam::DImgPreviewItem']]],
  ['loadingfaces_53425',['LoadingFaces',['../namespaceDigikam.html#aec4bd35dad3b4d1124651add3107c439a9eedc3b9fa49976d3dc9f88063d90f35',1,'Digikam']]],
  ['loadingmodenormal_53426',['LoadingModeNormal',['../classDigikam_1_1ManagedLoadSaveThread.html#ac3633ba7491538a372bc2624fbcf273ca780f91ca7010145e11cd35cbc0fbe70e',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingmodeshared_53427',['LoadingModeShared',['../classDigikam_1_1ManagedLoadSaveThread.html#ac3633ba7491538a372bc2624fbcf273cac1f09796b4d191c37cab611957ce38b5',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingpoints_53428',['LoadingPoints',['../namespaceDigikam.html#a87f584fc4265f6b781e6f3d79ab3fdb6a24cc3070cc8472ea3a0e9fd6164ed41c',1,'Digikam']]],
  ['loadingpolicyappend_53429',['LoadingPolicyAppend',['../classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1adf582e3d75b8139e8605b12210bcafc7',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingpolicyfirstremoveprevious_53430',['LoadingPolicyFirstRemovePrevious',['../classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1a7c86b50ed4e8629d6065dd7af56b24b0',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingpolicypreload_53431',['LoadingPolicyPreload',['../classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1aca9598bf4570e001a425a140f9c161d5',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingpolicyprepend_53432',['LoadingPolicyPrepend',['../classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1af1f4f39eefe75b9f8a9bbcc9c9483de5',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingpolicysimpleappend_53433',['LoadingPolicySimpleAppend',['../classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1af4bff03b35cad92f8932c7d0fe18d034',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingpolicysimpleprepend_53434',['LoadingPolicySimplePrepend',['../classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1a79542e5934840ce65947bb1e901c6340',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingtaskfilterall_53435',['LoadingTaskFilterAll',['../classDigikam_1_1ManagedLoadSaveThread.html#a68c752ffc42e8feed27b18e651a05466a2a11c078e25e2c9571f857e98b3135b0',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingtaskfilterpreloading_53436',['LoadingTaskFilterPreloading',['../classDigikam_1_1ManagedLoadSaveThread.html#a68c752ffc42e8feed27b18e651a05466abf8ba8a218f63d3d277467517e133571',1,'Digikam::ManagedLoadSaveThread']]],
  ['loadingtaskstatusloading_53437',['LoadingTaskStatusLoading',['../classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255a9fb917335571fcf5a3c92c4929800b17',1,'Digikam::LoadingTask']]],
  ['loadingtaskstatuspreloading_53438',['LoadingTaskStatusPreloading',['../classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255a28d9aec69c9254aabc1a0494204b64d4',1,'Digikam::LoadingTask']]],
  ['loadingtaskstatusstopping_53439',['LoadingTaskStatusStopping',['../classDigikam_1_1LoadingTask.html#aa4131ed076cc79578e21922f88d08255ad8070c2c8f54d077b89a3b211c964b82',1,'Digikam::LoadingTask']]],
  ['loaditeminfo_53440',['LoadItemInfo',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dca43ceb8982f2428dd9ef3a0bbd31c19cc',1,'Digikam::DImgLoader']]],
  ['loadleaveshistory_53441',['LoadLeavesHistory',['../classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22aad858c6a434dfb8fe98793b975c3dc45',1,'Digikam::ItemHistoryGraph']]],
  ['loadmetadata_53442',['LoadMetadata',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dcacc07f37f475e09d400ad580d2eef43f5',1,'Digikam::DImgLoader']]],
  ['loadpreview_53443',['LoadPreview',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dcadf0b72ec7d2af995c84899af7f5a6374',1,'Digikam::DImgLoader']]],
  ['loadrelationcloud_53444',['LoadRelationCloud',['../classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22ac3ec9cb6707fe3a7c940d3d8fb18ba53',1,'Digikam::ItemHistoryGraph']]],
  ['loadsubjecthistory_53445',['LoadSubjectHistory',['../classDigikam_1_1ItemHistoryGraph.html#a1f362f26a7f68d91ff8636eb7386bf22a2d5e6b0408b5fe665232484db74898b3',1,'Digikam::ItemHistoryGraph']]],
  ['loaduniquehash_53446',['LoadUniqueHash',['../classDigikam_1_1DImgLoader.html#a728bc20bc9304c54f14579bc889e41dcafe8857b91e17b47718c02ad27ebb6e44',1,'Digikam::DImgLoader']]],
  ['localdateformat_53447',['LocalDateFormat',['../classDigikam_1_1AlbumCustomizer.html#a696267c6c2b0f89fc3a7b9a3e11c0d6fa0e6b421e4d323f42659a8d1b67b9998c',1,'Digikam::AlbumCustomizer']]],
  ['localizemaps_53448',['LocAlizeMaps',['../classDigikam_1_1ItemPropertiesGPSTab.html#acc07ef506913d644b3127ab8f036e631aa76662c1ddc569e21ae1d762f742ff81',1,'Digikam::ItemPropertiesGPSTab']]],
  ['location_53449',['LOCATION',['../classDigikam_1_1TemplatePanel.html#a89be183792123475c326fa7bd190c8b5ac6b217e487b2e8f62b2f8faeae5e14e8',1,'Digikam::TemplatePanel']]],
  ['locationallright_53450',['LocationAllRight',['../classDigikam_1_1CollectionManager.html#a7588eab9f683286a5140c4b0789ebc40aacc3d09d47672e093154d775b06c79b3',1,'Digikam::CollectionManager']]],
  ['locationavailable_53451',['LocationAvailable',['../classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da91d729b7349c62799d23af1bc1cfb913',1,'Digikam::CollectionLocation']]],
  ['locationdeleted_53452',['LocationDeleted',['../classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da09737556f383ccb9e85afe0f3edede43',1,'Digikam::CollectionLocation']]],
  ['locationhasproblems_53453',['LocationHasProblems',['../classDigikam_1_1CollectionManager.html#a7588eab9f683286a5140c4b0789ebc40a146a8acf2d4fe568a8af5aa94a3615d7',1,'Digikam::CollectionManager']]],
  ['locationhidden_53454',['LocationHidden',['../classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da431831542cb1b9caece19b62515328d4',1,'Digikam::CollectionLocation']]],
  ['locationinvalidcheck_53455',['LocationInvalidCheck',['../classDigikam_1_1CollectionManager.html#a7588eab9f683286a5140c4b0789ebc40ac5d3a4c83e5b706ed07fd1b560698ea9',1,'Digikam::CollectionManager']]],
  ['locationnotallowed_53456',['LocationNotAllowed',['../classDigikam_1_1CollectionManager.html#a7588eab9f683286a5140c4b0789ebc40ac97e41fa0afc5470e7aa8a99378654e3',1,'Digikam::CollectionManager']]],
  ['locationnull_53457',['LocationNull',['../classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da057dbf9b75e101e4178a7d1f3c413157',1,'Digikam::CollectionLocation']]],
  ['locationunavailable_53458',['LocationUnavailable',['../classDigikam_1_1CollectionLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da89f81dd6a9696696e33180ff8e45fa28',1,'Digikam::CollectionLocation']]],
  ['logcabac_53459',['LogCABAC',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42ab4b806f2aa49109d6f7ef896db29ba34',1,'util.h']]],
  ['logdeblock_53460',['LogDeblock',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a973adeea6330f4e4de936130af5cf517',1,'util.h']]],
  ['logdpb_53461',['LogDPB',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a09c775df582cb6af43a86bce78e1b8fc',1,'util.h']]],
  ['logencoder_53462',['LogEncoder',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42ad96ed2619a56329590a49c1bb74c0e12',1,'util.h']]],
  ['logencodermetadata_53463',['LogEncoderMetadata',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a9b4651d18c66fc46fa1008ae6c720244',1,'util.h']]],
  ['logheaders_53464',['LogHeaders',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42ae2f769c93a6500a5bcfbe1eb996f0c52',1,'util.h']]],
  ['loghighlevel_53465',['LogHighlevel',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a220bc7905be39590b99aff1e457c59d8',1,'util.h']]],
  ['login_53466',['Login',['../namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89a98cf3652c496766dfc331021ffeab2db',1,'DigikamGenericRajcePlugin']]],
  ['logintrapred_53467',['LogIntraPred',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a05cd7d1c458f2add35f32a7c0257fc45',1,'util.h']]],
  ['logmotion_53468',['LogMotion',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42adfa3b38b5a9ff858335bf8acb49f1976',1,'util.h']]],
  ['logout_53469',['Logout',['../namespaceDigikamGenericRajcePlugin.html#ac9bcdac96fce90d45cdc2a308b188e89aa81addba5f0f32b351c44934762388af',1,'DigikamGenericRajcePlugin']]],
  ['logpixels_53470',['LogPixels',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a54bd970a79ac2a432c87155462464133',1,'util.h']]],
  ['logsao_53471',['LogSAO',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a03beb060ef765d31c1aaf3cf635ab102',1,'util.h']]],
  ['logscale_53472',['LogScale',['../classDigikam_1_1TimeLineWidget.html#a4dfaa0ab71a7dfa59b716df7fcda2b7ba7a876c27af57a4883e50345e200c7faa',1,'Digikam::TimeLineWidget']]],
  ['logscalehistogram_53473',['LogScaleHistogram',['../namespaceDigikam.html#a7252dea9fd7f36c45149da08748a2cb9a4b51ecacce35001b47229799a27ecc83',1,'Digikam']]],
  ['logsei_53474',['LogSEI',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a72d3e176740277ae9057afc9f237af9e',1,'util.h']]],
  ['logslice_53475',['LogSlice',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a261b3dc8977f2dd66f4391f89e8c143e',1,'util.h']]],
  ['logsymbols_53476',['LogSymbols',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42a0ec90da51b35c102938725abe2b886f1',1,'util.h']]],
  ['logtransform_53477',['LogTransform',['../util_8h.html#a7e4ae98d96e7177e6c476a1bf942ed42aa33b0af36291217819351dda56caf573',1,'util.h']]],
  ['longitude_53478',['Longitude',['../namespaceDigikam_1_1DatabaseFields.html#ad03131aab54d50f7885f5ed1ff3498daa7af911727a0559cb01f587c65d9f14ff',1,'Digikam::DatabaseFields::Longitude()'],['../namespaceDigikam_1_1MetadataInfo.html#aa919f13795e9cff09f6271192e05d02ea3ce1115f7af64c205faffcf8fb72cdf0',1,'Digikam::MetadataInfo::Longitude()']]],
  ['longitudenumber_53479',['LongitudeNumber',['../namespaceDigikam_1_1DatabaseFields.html#ad03131aab54d50f7885f5ed1ff3498daa95fc5b02937d62bed871613ab5d5189f',1,'Digikam::DatabaseFields::LongitudeNumber()'],['../namespaceDigikam_1_1MetadataInfo.html#aa919f13795e9cff09f6271192e05d02ea8469be9658c84ae6322dd6896b07512f',1,'Digikam::MetadataInfo::LongitudeNumber()']]],
  ['lower_53480',['LOWER',['../classDigikam_1_1RenameCustomizer.html#aebed55b5b897778e0ce127a4799e65b7afd10d27e1eff0cefad7a188230761329',1,'Digikam::RenameCustomizer']]],
  ['lrgb_53481',['LRGB',['../namespaceDigikam.html#a3c8781f7dd8b6104606bb7d34f5f4cc6ad3eeb7910b9e4f47cbf16dbe37a82874',1,'Digikam']]],
  ['lrgba_53482',['LRGBA',['../namespaceDigikam.html#a3c8781f7dd8b6104606bb7d34f5f4cc6a8557f36e4bf3ab706698092e00bbf12e',1,'Digikam']]],
  ['lrgbac_53483',['LRGBAC',['../namespaceDigikam.html#a3c8781f7dd8b6104606bb7d34f5f4cc6a68a69c20af1f4b005b62fe4e0e469c02',1,'Digikam']]],
  ['lrgbc_53484',['LRGBC',['../namespaceDigikam.html#a3c8781f7dd8b6104606bb7d34f5f4cc6adbb16c120b8a434d82fa214dd5bb6c1e',1,'Digikam']]],
  ['lt_53485',['LT',['../namespaceDigikam.html#ad433e183fd169903b66270e16b6ada18a647c67dd35b7b624a0ff476468f8e9d7',1,'Digikam']]],
  ['lte_53486',['LTE',['../namespaceDigikam.html#ad433e183fd169903b66270e16b6ada18ad4ac0286f91d8a2315d3a143fe4f8962',1,'Digikam']]],
  ['ltleftpanelrole_53487',['LTLeftPanelRole',['../classDigikam_1_1ItemModel.html#a06821030af2cbd4509682f0d9731fce0af0b5267989478a2c48261aad2bd2fa09',1,'Digikam::ItemModel']]],
  ['ltrightpanelrole_53488',['LTRightPanelRole',['../classDigikam_1_1ItemModel.html#a06821030af2cbd4509682f0d9731fce0a237f6c4f96a7b7b03d6f4e149393da5b',1,'Digikam::ItemModel']]],
  ['lumagradientnorm_53489',['LumaGradientNorm',['../classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a45af206be2bffcfa935f0f890c876b99',1,'Digikam::ContentAwareContainer']]],
  ['lumasumofabsolutevalues_53490',['LumaSumOfAbsoluteValues',['../classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a5491d1392abbb3dd3af036b404862bf4',1,'Digikam::ContentAwareContainer']]],
  ['lumaxabsolutevalue_53491',['LumaXAbsoluteValue',['../classDigikam_1_1ContentAwareContainer.html#a029c2f0701a63bcb1403676b4a214e32a26744f7a4f8ffd448ba9fc0907ca93c9',1,'Digikam::ContentAwareContainer']]],
  ['luminositychannel_53492',['LuminosityChannel',['../namespaceDigikam.html#a107bc68e2a353b42922bb3e983785eb3ac6e68baa1e405e8e84264ac5b54bcf63',1,'Digikam']]],
  ['lut3d_53493',['Lut3D',['../classDigikam_1_1ColorFXFilter.html#a40ef506b2b4375aa9585ac86649dc4faa5780077f5eb5f3d22345293af5e2f055',1,'Digikam::ColorFXFilter']]],
  ['lzw_53494',['LZW',['../structDigikam_1_1PTOType_1_1Project_1_1FileFormat.html#a574a845fc8d65ed34404d524bb1dc1f5a2b1242b0bcf7bb41e6eb4a4cd32c355c',1,'Digikam::PTOType::Project::FileFormat']]]
];
