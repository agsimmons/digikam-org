var searchData=
[
  ['jpeg2000compression_48892',['JPEG2000Compression',['../classDigikam_1_1IOFileSettings.html#adcb31b2b95b0112627e393a956b24e73',1,'Digikam::IOFileSettings']]],
  ['jpeg2000lossless_48893',['JPEG2000LossLess',['../classDigikam_1_1IOFileSettings.html#a425d2924e72c2d8e360e0943c985cce3',1,'Digikam::IOFileSettings']]],
  ['jpegcompression_48894',['JPEGCompression',['../classDigikam_1_1IOFileSettings.html#aa7e24c58678e8df278bfd84cf23b2c78',1,'Digikam::IOFileSettings']]],
  ['jpeglosslesscompression_48895',['jpegLossLessCompression',['../classDigikam_1_1DNGWriter_1_1Private.html#a7de6eec7141d566b5013036028bdcc1e',1,'Digikam::DNGWriter::Private']]],
  ['jpegsubsampling_48896',['JPEGSubSampling',['../classDigikam_1_1IOFileSettings.html#ad734d72f8cb98d780ac0c76f43721bba',1,'Digikam::IOFileSettings']]],
  ['jsonfilepath_48897',['jsonFilePath',['../classDigikam_1_1DTrashItemInfo.html#abe09e4459779c8f6c52f5afb84328308',1,'Digikam::DTrashItemInfo']]]
];
