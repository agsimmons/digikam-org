var classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private =
[
    [ "Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a45120bce28621ff0415b6723a9914fdd", null ],
    [ "~Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#aaf018d727e7fae187e81d2db0746db92", null ],
    [ "clientDescription", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#ad40f3938a11ff218e8e659f8d8ee2e75", null ],
    [ "close", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a5610487954b05c1f8b881f1c874e3334", null ],
    [ "isOpened", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a0610cc0f19cfba6cdc597493450a069c", null ],
    [ "maxClients", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#af6b4ca2a6393fd3ebd95269469702a6b", null ],
    [ "open", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a193fe5ef386a429194855ec2d2d9aa7b", null ],
    [ "setMaxClients", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#aafcb481118702b8b8a199398ff1e5a0f", null ],
    [ "start", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a86ea33486212c3e0037b55cff4839335", null ],
    [ "stop", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a02112403ce428117fdc33ba2f5da087d", null ],
    [ "writeInSocket", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#abc0eae6a5a47def95e9f7d5833b3f8e3", null ],
    [ "blackList", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a08c3d4c164339afd94992d36b8238fd1", null ],
    [ "clients", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a3a7b9c0e3f54c25c20cf23b1f60a8569", null ],
    [ "delay", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#afffb41401f84731f0ce980f32a8da67d", null ],
    [ "lastFrame", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a20421a8b5ae4b03489d6bbc012430be5", null ],
    [ "mutexClients", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a64da40e2ab91c8befd3dfa35a617fe4d", null ],
    [ "mutexFrame", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a280610b42e51054e6f8b185308373305", null ],
    [ "rate", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a6582c36136d21c9ee81a95d2d88a2ea6", null ],
    [ "server", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#abcfcf44c1d0f0f28620f1655b94c52d9", null ],
    [ "srvTask", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html#a27584c69e9da0234ab6025bdfb74b8b8", null ]
];