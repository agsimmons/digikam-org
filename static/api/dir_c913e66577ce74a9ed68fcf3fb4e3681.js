var dir_c913e66577ce74a9ed68fcf3fb4e3681 =
[
    [ "dbitem.h", "dbitem_8h.html", [
      [ "DBFolder", "classDigikamGenericDropBoxPlugin_1_1DBFolder.html", "classDigikamGenericDropBoxPlugin_1_1DBFolder" ],
      [ "DBPhoto", "classDigikamGenericDropBoxPlugin_1_1DBPhoto.html", "classDigikamGenericDropBoxPlugin_1_1DBPhoto" ]
    ] ],
    [ "dbmpform.cpp", "dbmpform_8cpp.html", null ],
    [ "dbmpform.h", "dbmpform_8h.html", [
      [ "DBMPForm", "classDigikamGenericDropBoxPlugin_1_1DBMPForm.html", "classDigikamGenericDropBoxPlugin_1_1DBMPForm" ]
    ] ],
    [ "dbnewalbumdlg.cpp", "dbnewalbumdlg_8cpp.html", null ],
    [ "dbnewalbumdlg.h", "dbnewalbumdlg_8h.html", [
      [ "DBNewAlbumDlg", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg" ]
    ] ],
    [ "dbplugin.cpp", "dbplugin_8cpp.html", null ],
    [ "dbplugin.h", "dbplugin_8h.html", "dbplugin_8h" ],
    [ "dbtalker.cpp", "dbtalker_8cpp.html", null ],
    [ "dbtalker.h", "dbtalker_8h.html", [
      [ "DBTalker", "classDigikamGenericDropBoxPlugin_1_1DBTalker.html", "classDigikamGenericDropBoxPlugin_1_1DBTalker" ]
    ] ],
    [ "dbwidget.cpp", "dbwidget_8cpp.html", null ],
    [ "dbwidget.h", "dbwidget_8h.html", [
      [ "DBWidget", "classDigikamGenericDropBoxPlugin_1_1DBWidget.html", "classDigikamGenericDropBoxPlugin_1_1DBWidget" ]
    ] ],
    [ "dbwindow.cpp", "dbwindow_8cpp.html", null ],
    [ "dbwindow.h", "dbwindow_8h.html", [
      [ "DBWindow", "classDigikamGenericDropBoxPlugin_1_1DBWindow.html", "classDigikamGenericDropBoxPlugin_1_1DBWindow" ]
    ] ]
];