var dir_45bdc5c574da8259a18419618f2e8a23 =
[
    [ "altlangstredit.cpp", "altlangstredit_8cpp.html", null ],
    [ "altlangstredit.h", "altlangstredit_8h.html", [
      [ "AltLangStrEdit", "classDigikam_1_1AltLangStrEdit.html", "classDigikam_1_1AltLangStrEdit" ]
    ] ],
    [ "countryselector.cpp", "countryselector_8cpp.html", null ],
    [ "countryselector.h", "countryselector_8h.html", [
      [ "CountrySelector", "classDigikam_1_1CountrySelector.html", "classDigikam_1_1CountrySelector" ]
    ] ],
    [ "subjectwidget.cpp", "subjectwidget_8cpp.html", null ],
    [ "subjectwidget.h", "subjectwidget_8h.html", [
      [ "SubjectData", "classDigikam_1_1SubjectData.html", "classDigikam_1_1SubjectData" ],
      [ "SubjectWidget", "classDigikam_1_1SubjectWidget.html", "classDigikam_1_1SubjectWidget" ]
    ] ]
];