var dir_868feff924a8bc0f548014414572adcc =
[
    [ "webwidget.cpp", "webwidget_8cpp.html", null ],
    [ "webwidget.h", "webwidget_8h.html", [
      [ "WebWidget", "classDigikam_1_1WebWidget.html", "classDigikam_1_1WebWidget" ]
    ] ],
    [ "webwidget_qwebengine.cpp", "webwidget__qwebengine_8cpp.html", null ],
    [ "webwidget_qwebengine.h", "webwidget__qwebengine_8h.html", [
      [ "WebWidget", "classDigikam_1_1WebWidget.html", "classDigikam_1_1WebWidget" ]
    ] ],
    [ "wscomboboxdelegate.cpp", "wscomboboxdelegate_8cpp.html", null ],
    [ "wscomboboxdelegate.h", "wscomboboxdelegate_8h.html", [
      [ "ComboBoxDelegate", "classDigikam_1_1ComboBoxDelegate.html", "classDigikam_1_1ComboBoxDelegate" ]
    ] ],
    [ "wscomboboxintermediate.cpp", "wscomboboxintermediate_8cpp.html", null ],
    [ "wscomboboxintermediate.h", "wscomboboxintermediate_8h.html", [
      [ "WSComboBoxIntermediate", "classDigikam_1_1WSComboBoxIntermediate.html", "classDigikam_1_1WSComboBoxIntermediate" ]
    ] ],
    [ "wsitem.h", "wsitem_8h.html", [
      [ "AlbumSimplified", "classDigikam_1_1AlbumSimplified.html", "classDigikam_1_1AlbumSimplified" ],
      [ "WSAlbum", "classDigikam_1_1WSAlbum.html", "classDigikam_1_1WSAlbum" ]
    ] ],
    [ "wslogindialog.cpp", "wslogindialog_8cpp.html", null ],
    [ "wslogindialog.h", "wslogindialog_8h.html", [
      [ "WSLoginDialog", "classDigikam_1_1WSLoginDialog.html", "classDigikam_1_1WSLoginDialog" ]
    ] ],
    [ "wsnewalbumdialog.cpp", "wsnewalbumdialog_8cpp.html", null ],
    [ "wsnewalbumdialog.h", "wsnewalbumdialog_8h.html", [
      [ "WSNewAlbumDialog", "classDigikam_1_1WSNewAlbumDialog.html", "classDigikam_1_1WSNewAlbumDialog" ]
    ] ],
    [ "wsselectuserdlg.cpp", "wsselectuserdlg_8cpp.html", null ],
    [ "wsselectuserdlg.h", "wsselectuserdlg_8h.html", [
      [ "WSSelectUserDlg", "classDigikam_1_1WSSelectUserDlg.html", "classDigikam_1_1WSSelectUserDlg" ]
    ] ],
    [ "wssettings.cpp", "wssettings_8cpp.html", null ],
    [ "wssettings.h", "wssettings_8h.html", [
      [ "WSSettings", "classDigikam_1_1WSSettings.html", "classDigikam_1_1WSSettings" ]
    ] ],
    [ "wssettingswidget.cpp", "wssettingswidget_8cpp.html", null ],
    [ "wssettingswidget.h", "wssettingswidget_8h.html", [
      [ "WSSettingsWidget", "classDigikam_1_1WSSettingsWidget.html", "classDigikam_1_1WSSettingsWidget" ]
    ] ],
    [ "wstooldialog.cpp", "wstooldialog_8cpp.html", null ],
    [ "wstooldialog.h", "wstooldialog_8h.html", [
      [ "WSToolDialog", "classDigikam_1_1WSToolDialog.html", "classDigikam_1_1WSToolDialog" ]
    ] ],
    [ "wstoolutils.cpp", "wstoolutils_8cpp.html", null ],
    [ "wstoolutils.h", "wstoolutils_8h.html", [
      [ "WSToolUtils", "classDigikam_1_1WSToolUtils.html", "classDigikam_1_1WSToolUtils" ]
    ] ]
];