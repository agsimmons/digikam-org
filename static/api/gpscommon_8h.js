var gpscommon_8h =
[
    [ "MapLayout", "gpscommon_8h.html#a82b6d92369fccc2448496234a5a21630", [
      [ "MapLayoutOne", "gpscommon_8h.html#a82b6d92369fccc2448496234a5a21630ad8fa75dd9b074bf031700e6e84c8b4dd", null ],
      [ "MapLayoutHorizontal", "gpscommon_8h.html#a82b6d92369fccc2448496234a5a21630abe9d94a0a587ed7d24d1c7b853948b58", null ],
      [ "MapLayoutVertical", "gpscommon_8h.html#a82b6d92369fccc2448496234a5a21630a4d9d2651cc40d71a15b88f10d4d5d6de", null ]
    ] ],
    [ "checkSidecarSettings", "gpscommon_8h.html#aa2f62fa8e41437a7f8f426ad5c34b78d", null ],
    [ "coordinatesToClipboard", "gpscommon_8h.html#a07af5aca89f9613199ee11135bcfabd6", null ],
    [ "getUserAgentName", "gpscommon_8h.html#aeddda8d12af416ae950e97b33044eb23", null ]
];