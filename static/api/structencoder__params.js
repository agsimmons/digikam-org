var structencoder__params =
[
    [ "encoder_params", "structencoder__params.html#acfcf2be514e25978ac622e8cbb5ad331", null ],
    [ "registerParams", "structencoder__params.html#a0124513449d8f4fa9e9f5f4876646067", null ],
    [ "intraPredSearch", "structencoder__params.html#a404dfc9661cb26794d450f16b4dbd501", null ],
    [ "mAlgo_CB_IntraPartMode", "structencoder__params.html#af5d666c845b5e9c1f4975b46612f4a5e", null ],
    [ "mAlgo_MEMode", "structencoder__params.html#ae8362ae9c864d34f6eb481aa24e68668", null ],
    [ "mAlgo_TB_IntraPredMode", "structencoder__params.html#a37d9d16d829111e9c6db491da90d56ae", null ],
    [ "mAlgo_TB_IntraPredMode_Subset", "structencoder__params.html#a32640caddec6556b1be479deb0480598", null ],
    [ "mAlgo_TB_RateEstimation", "structencoder__params.html#a58c3cc69cc647a95ca0d8e5f2ae46809", null ],
    [ "max_cb_size", "structencoder__params.html#ac062eeef0d1fcc1a454b8ed46835d5b6", null ],
    [ "max_tb_size", "structencoder__params.html#a6e3fa2d046eab5d25bf5666720e4a0e7", null ],
    [ "max_transform_hierarchy_depth_inter", "structencoder__params.html#a0b2a39ad314bc107f37eac289f8c0180", null ],
    [ "max_transform_hierarchy_depth_intra", "structencoder__params.html#a5b2b68bc7e4546a178b5d44f40dc60ba", null ],
    [ "min_cb_size", "structencoder__params.html#a5f4d54e31ff61b92466da600213637cb", null ],
    [ "min_tb_size", "structencoder__params.html#ad6ca3b3451a4ba1fb6b3b431edfebd25", null ],
    [ "mSOP_LowDelay", "structencoder__params.html#ae1e7012afaf28f2d062e63ee42ece253", null ],
    [ "rateControlMethod", "structencoder__params.html#ad635369e22b2c3660da997d0a522a9c4", null ],
    [ "sop_structure", "structencoder__params.html#a944c8422dbe893eeb41c362ae68181ba", null ]
];