var structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation =
[
    [ "NearbyObservation", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#aa83efd78cca16513463972e7d8e52451", null ],
    [ "NearbyObservation", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#ae30ea110c6f140282e06824b1c061ffa", null ],
    [ "isValid", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#a77ce0735152ca163d38d75680254c2b2", null ],
    [ "updateObservation", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#ab9430cd1e6892cc04227ee410a8657d5", null ],
    [ "m_distanceMeters", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#a845d0a15da8e1bda22e82296a63a76a1", null ],
    [ "m_latitude", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#a71ed5fbc47f70454dd3c6bd64fbe89e9", null ],
    [ "m_longitude", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#a03156f0ef7f55687563bc3bdff806514", null ],
    [ "m_obscured", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#ac150120d5cacca043152839f29e04c1f", null ],
    [ "m_observationId", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#ab65dafc0f6d1f3c5eb45f5ae49b6378d", null ],
    [ "m_referenceLatitude", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#ac37026e1a235d3e3a34cca9223d7dfee", null ],
    [ "m_referenceLongitude", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#a7ba6704dbd8ec4a56bb44861e268d035", null ],
    [ "m_referenceTaxon", "structDigikamGenericINatPlugin_1_1INatTalker_1_1NearbyObservation.html#ab4a5412b1557b320a53e5861a9fa4847", null ]
];