var namespaceDigikamGenericFileCopyPlugin =
[
    [ "FCContainer", "classDigikamGenericFileCopyPlugin_1_1FCContainer.html", "classDigikamGenericFileCopyPlugin_1_1FCContainer" ],
    [ "FCExportWidget", "classDigikamGenericFileCopyPlugin_1_1FCExportWidget.html", "classDigikamGenericFileCopyPlugin_1_1FCExportWidget" ],
    [ "FCExportWindow", "classDigikamGenericFileCopyPlugin_1_1FCExportWindow.html", "classDigikamGenericFileCopyPlugin_1_1FCExportWindow" ],
    [ "FCPlugin", "classDigikamGenericFileCopyPlugin_1_1FCPlugin.html", "classDigikamGenericFileCopyPlugin_1_1FCPlugin" ],
    [ "FCTask", "classDigikamGenericFileCopyPlugin_1_1FCTask.html", "classDigikamGenericFileCopyPlugin_1_1FCTask" ],
    [ "FCThread", "classDigikamGenericFileCopyPlugin_1_1FCThread.html", "classDigikamGenericFileCopyPlugin_1_1FCThread" ]
];