var glviewerglobal_8h =
[
    [ "CACHESIZE", "glviewerglobal_8h.html#ac6cac52a3e9d61f741714f164dc59c41", null ],
    [ "EMPTY", "glviewerglobal_8h.html#a2b7cf2a3641be7b89138615764d60ba3", null ],
    [ "OGLstate", "glviewerglobal_8h.html#a8abbaa64ef5f2b664f34f54b7632026e", [
      [ "oglOK", "glviewerglobal_8h.html#a8abbaa64ef5f2b664f34f54b7632026eadd18aedcb5dbd0387f7ad536b9e88387", null ],
      [ "oglNoRectangularTexture", "glviewerglobal_8h.html#a8abbaa64ef5f2b664f34f54b7632026eaa7614f8f5a987cced8a01da729a55696", null ],
      [ "oglNoContext", "glviewerglobal_8h.html#a8abbaa64ef5f2b664f34f54b7632026ea41886f62e0bae782b4af8ffa98f73899", null ]
    ] ]
];