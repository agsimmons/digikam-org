var classDigikamGenericCalendarPlugin_1_1CalSettings =
[
    [ "~CalSettings", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#ab6090d7b107805692190d449c1d49415", null ],
    [ "addSpecial", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#ac265351564e5ed5f30dc47697a2c4222", null ],
    [ "clearSpecial", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#ae8aa601d1b945d301796349d956b00d2", null ],
    [ "getDayColor", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a7de545b830ed4db9b1f0c2b5e9a33f58", null ],
    [ "getDayDescr", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#aee9af7b347efcef38b7d04ff0803082d", null ],
    [ "image", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#ad9a6dd598c69ea5524e759760b4326ec", null ],
    [ "isPrayDay", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a5e30fe485983c160237dcc05b00ad996", null ],
    [ "isSpecial", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a5f0848a4ff54703a359e279788673fa9", null ],
    [ "resolution", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a64816142e67b32f34c99bf5e747e36ac", null ],
    [ "setDrawLines", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a13bbe28bb048ef2b08a23ce7f5bab917", null ],
    [ "setFont", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#aa19437adb072e8902fa1496963742e76", null ],
    [ "setImage", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#ac26632ebe0c4a5b6e613567967739c9a", null ],
    [ "setImagePos", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#aef3a165ee5a7f78c98a9716e3b2d5897", null ],
    [ "setPaperSize", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#af76ac71a12d052f8ee563cbce8fe4ed6", null ],
    [ "setRatio", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#ae12eae7c46de35ba555a7558edea5b53", null ],
    [ "setResolution", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#af3d99be02d97e51e43591c54a1a7ec8f", null ],
    [ "settingsChanged", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#aa67693d303082961e7108efd6382acdb", null ],
    [ "setYear", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a927dbef789cfb0ed8d4c1163b35dd7c8", null ],
    [ "year", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a69b5aec6780b806a8cb2ca452179d253", null ],
    [ "params", "classDigikamGenericCalendarPlugin_1_1CalSettings.html#a60a1771fb136b15f65cdcd3bddbd9077", null ]
];