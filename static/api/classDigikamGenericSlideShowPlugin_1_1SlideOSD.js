var classDigikamGenericSlideShowPlugin_1_1SlideOSD =
[
    [ "SlideOSD", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a49b60155fae05620add772a7635cd65a", null ],
    [ "~SlideOSD", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a7d2673e439b632a19dfea6f61fcd1d2a", null ],
    [ "isPaused", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#ad9ea997d197864717b824a41c5256016", null ],
    [ "isUnderMouse", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a96df6f866340764429d9d98246a865eb", null ],
    [ "pause", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a74804099b72023a5c2531d487496aa21", null ],
    [ "setCurrentUrl", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a00457c26f61799b5c0ca03107d2f57b0", null ],
    [ "setLoadingReady", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a691b714b20d0da0fb1e486d52c2acdb2", null ],
    [ "slideShowSize", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#ae6f6c27fc32ef49c5119187e37fb4711", null ],
    [ "toggleProperties", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a113892fddaffa818f85f882e1d7b835f", null ],
    [ "toolBar", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#a054869d1caa0e8a36385228f9140014c", null ],
    [ "video", "classDigikamGenericSlideShowPlugin_1_1SlideOSD.html#af9b112c7ea313feb79192a029b6252a7", null ]
];