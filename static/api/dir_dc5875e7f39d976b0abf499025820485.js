var dir_dc5875e7f39d976b0abf499025820485 =
[
    [ "autocorrection", "dir_8e23def25de1b9246da54f24b6e3cb61.html", "dir_8e23def25de1b9246da54f24b6e3cb61" ],
    [ "bcgcorrection", "dir_d434755b14d528e1ef6276265895b25a.html", "dir_d434755b14d528e1ef6276265895b25a" ],
    [ "bwconvert", "dir_7ff83cdd837b27fd649c7b1344f36c54.html", "dir_7ff83cdd837b27fd649c7b1344f36c54" ],
    [ "channelmixer", "dir_eeac8423ad0d1879ccc742a821b0423d.html", "dir_eeac8423ad0d1879ccc742a821b0423d" ],
    [ "colorbalance", "dir_6b822ec2225b8e633148c7423efd4a30.html", "dir_6b822ec2225b8e633148c7423efd4a30" ],
    [ "convert16to8", "dir_e4728377e720513fd2ae48481e908480.html", "dir_e4728377e720513fd2ae48481e908480" ],
    [ "convert8to16", "dir_5822332c8b7f23c3b7fb83bb281dde26.html", "dir_5822332c8b7f23c3b7fb83bb281dde26" ],
    [ "curvesadjust", "dir_675e605162c1259335e9253ea3ff4f0f.html", "dir_675e605162c1259335e9253ea3ff4f0f" ],
    [ "hslcorrection", "dir_771ca3682a59df9b0fe22ed7fcb6a008.html", "dir_771ca3682a59df9b0fe22ed7fcb6a008" ],
    [ "iccconvert", "dir_4f9152ab3ceb67c9c42ff5b2013350c6.html", "dir_4f9152ab3ceb67c9c42ff5b2013350c6" ],
    [ "invert", "dir_ab55b3455916970003113d71f3f6e1ef.html", "dir_ab55b3455916970003113d71f3f6e1ef" ],
    [ "whitebalance", "dir_f8d82aa6b9dd1c2cfcf7b8c85ba036b9.html", "dir_f8d82aa6b9dd1c2cfcf7b8c85ba036b9" ]
];