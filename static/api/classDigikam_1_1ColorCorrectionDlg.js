var classDigikam_1_1ColorCorrectionDlg =
[
    [ "Mode", "classDigikam_1_1ColorCorrectionDlg.html#a12ffd56358f2deda9773d9b66a14a6f0", [
      [ "ProfileMismatch", "classDigikam_1_1ColorCorrectionDlg.html#a12ffd56358f2deda9773d9b66a14a6f0a72849f1eb5af7ec785afc9092695c18e", null ],
      [ "MissingProfile", "classDigikam_1_1ColorCorrectionDlg.html#a12ffd56358f2deda9773d9b66a14a6f0a1882639b1eac4b0d217b49534346d28b", null ],
      [ "UncalibratedColor", "classDigikam_1_1ColorCorrectionDlg.html#a12ffd56358f2deda9773d9b66a14a6f0a3516a93c7a102d479a623044b846c7d1", null ]
    ] ],
    [ "ColorCorrectionDlg", "classDigikam_1_1ColorCorrectionDlg.html#acb154d19e91c73578dc060ce729926e2", null ],
    [ "~ColorCorrectionDlg", "classDigikam_1_1ColorCorrectionDlg.html#aec7a738c0215befaf4a5a5871ae8464f", null ],
    [ "behavior", "classDigikam_1_1ColorCorrectionDlg.html#abaae223b9ad75d0dcf97d64bb409dccf", null ],
    [ "specifiedProfile", "classDigikam_1_1ColorCorrectionDlg.html#aa35d9a5ed89084632ac677999e74da9e", null ]
];