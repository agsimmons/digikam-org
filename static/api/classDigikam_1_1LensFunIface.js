var classDigikam_1_1LensFunIface =
[
    [ "DevicePtr", "classDigikam_1_1LensFunIface.html#a08dfefb8d782bce2e1897911ec26cf60", null ],
    [ "LensList", "classDigikam_1_1LensFunIface.html#af7ccd56e469e734743f7de6279f7097e", null ],
    [ "LensPtr", "classDigikam_1_1LensFunIface.html#aa9c0580967fdc826c095eaeccf99bbf5", null ],
    [ "MetadataMatch", "classDigikam_1_1LensFunIface.html#aa2ef24b762bd575949d0b990b62a50e2", [
      [ "MetadataUnavailable", "classDigikam_1_1LensFunIface.html#aa2ef24b762bd575949d0b990b62a50e2a86c2758f6fc9fb98ed53fc14bda121b7", null ],
      [ "MetadataNoMatch", "classDigikam_1_1LensFunIface.html#aa2ef24b762bd575949d0b990b62a50e2a126619f46bb583d6909caa358fcd0f29", null ],
      [ "MetadataPartialMatch", "classDigikam_1_1LensFunIface.html#aa2ef24b762bd575949d0b990b62a50e2a0009206abc4d6edd819657472648a33d", null ],
      [ "MetadataExactMatch", "classDigikam_1_1LensFunIface.html#aa2ef24b762bd575949d0b990b62a50e2a1ceaa0c1e7ac82943a22bc556b25d144", null ]
    ] ],
    [ "LensFunIface", "classDigikam_1_1LensFunIface.html#acd81a5cbd847d8fd164419d7e149d6e7", null ],
    [ "~LensFunIface", "classDigikam_1_1LensFunIface.html#a818b67b6a40381147096e90d65c719cf", null ],
    [ "findCamera", "classDigikam_1_1LensFunIface.html#aeee31e9eb8776cc8972d3018ea449b30", null ],
    [ "findFromMetadata", "classDigikam_1_1LensFunIface.html#ad5904b2068a53ce110cb70a096c1f922", null ],
    [ "findLens", "classDigikam_1_1LensFunIface.html#aa238abd5e8f6869b55cdf357b7a62e4f", null ],
    [ "lensDescription", "classDigikam_1_1LensFunIface.html#acc6718a54d992db37fa74dfa4d3883ea", null ],
    [ "lensFunCameras", "classDigikam_1_1LensFunIface.html#a3fb163ceb42d3b1ad27a05c798d3e641", null ],
    [ "lensFunDataBase", "classDigikam_1_1LensFunIface.html#a79e8ee3f115179059b955f79f301b590", null ],
    [ "makeDescription", "classDigikam_1_1LensFunIface.html#aa920a35ece2226d3fe9307f6c90b85af", null ],
    [ "modelDescription", "classDigikam_1_1LensFunIface.html#af220cded649297180363d2a10f7f4fa0", null ],
    [ "setFilterSettings", "classDigikam_1_1LensFunIface.html#a0b1c6bdbc98d700766e7a9c4d9c59725", null ],
    [ "setSettings", "classDigikam_1_1LensFunIface.html#ad16c0cd0e4fcd04b69df57c7d17fdd92", null ],
    [ "settings", "classDigikam_1_1LensFunIface.html#a462dd303c521719e8cd38edc31ccae7b", null ],
    [ "setUsedCamera", "classDigikam_1_1LensFunIface.html#a6d1d8d961c893d6ebcd66a92fc5933e7", null ],
    [ "setUsedLens", "classDigikam_1_1LensFunIface.html#a9488058bbadc2bc2ec71dd1ed54962f9", null ],
    [ "supportsCCA", "classDigikam_1_1LensFunIface.html#a87885f7196b6db89473297ff6ab67f4d", null ],
    [ "supportsDistortion", "classDigikam_1_1LensFunIface.html#a75c7e1ae2ca8c1f1c63da4e04bf191ff", null ],
    [ "supportsGeometry", "classDigikam_1_1LensFunIface.html#ad07c5f7189d18ec4dbcfc58e4f33a1c7", null ],
    [ "supportsVig", "classDigikam_1_1LensFunIface.html#a485bacbcf9807c62fd846c19b72adc8c", null ],
    [ "usedCamera", "classDigikam_1_1LensFunIface.html#a04e7eecf87f79dcbe3fdb7593d44a9c0", null ],
    [ "usedLens", "classDigikam_1_1LensFunIface.html#ab7b3b7170955bac6094ef7c304b8d33f", null ]
];