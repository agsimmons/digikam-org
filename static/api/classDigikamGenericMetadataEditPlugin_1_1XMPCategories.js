var classDigikamGenericMetadataEditPlugin_1_1XMPCategories =
[
    [ "XMPCategories", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html#af8a65c5a3256edf7efee493abd3b48ab", null ],
    [ "~XMPCategories", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html#a1f2ce5114e59d4343a72f33eecfd9012", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html#a8c4ecd1f1646f22ebe44826553883db2", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html#ac33a283cd92d03f4e9c6e8131a6d6dc2", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1XMPCategories.html#a5e8ac680578ad1d440dd32eb6aec9d95", null ]
];