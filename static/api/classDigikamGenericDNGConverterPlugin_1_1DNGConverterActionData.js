var classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData =
[
    [ "DNGConverterActionData", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#ab4764c70ba9bf5c89220790bdbcd7e45", null ],
    [ "action", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#a2d303cad8c3714606c55208c3b689195", null ],
    [ "destPath", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#ad1304f8bb1bde1edc38fc9b4f394daac", null ],
    [ "fileUrl", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#a7f4bf7c1f5f00cc1253c93a5c4463738", null ],
    [ "image", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#a96c53ebf8a4e04116c7699504f3c20bd", null ],
    [ "message", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#aaeaad7943f059c0d415dc4705ed92852", null ],
    [ "result", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#aaf02435e343142ddece9a6216316f769", null ],
    [ "starting", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterActionData.html#ae9a5d3380c69e6bc6cf2511d6d2b87f9", null ]
];