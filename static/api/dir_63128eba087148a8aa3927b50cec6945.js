var dir_63128eba087148a8aa3927b50cec6945 =
[
    [ "databaseserver.cpp", "databaseserver_8cpp.html", null ],
    [ "databaseserver.h", "databaseserver_8h.html", [
      [ "DatabaseServer", "classDigikam_1_1DatabaseServer.html", "classDigikam_1_1DatabaseServer" ]
    ] ],
    [ "databaseservererror.cpp", "databaseservererror_8cpp.html", null ],
    [ "databaseservererror.h", "databaseservererror_8h.html", [
      [ "DatabaseServerError", "classDigikam_1_1DatabaseServerError.html", "classDigikam_1_1DatabaseServerError" ]
    ] ],
    [ "databaseserverstarter.cpp", "databaseserverstarter_8cpp.html", null ],
    [ "databaseserverstarter.h", "databaseserverstarter_8h.html", [
      [ "DatabaseServerStarter", "classDigikam_1_1DatabaseServerStarter.html", "classDigikam_1_1DatabaseServerStarter" ]
    ] ]
];