var classDigikam_1_1ItemComments =
[
    [ "LanguageChoiceBehavior", "classDigikam_1_1ItemComments.html#a2a80729a4aa38869b7aa06ce5289c03a", [
      [ "ReturnMatchingLanguageOnly", "classDigikam_1_1ItemComments.html#a2a80729a4aa38869b7aa06ce5289c03aa7ee01f9f2e731a64e3e980dc57067ccb", null ],
      [ "ReturnMatchingOrDefaultLanguage", "classDigikam_1_1ItemComments.html#a2a80729a4aa38869b7aa06ce5289c03aa31b9b0a94bc2f74fed0929d03e2ed54f", null ],
      [ "ReturnMatchingDefaultOrFirstLanguage", "classDigikam_1_1ItemComments.html#a2a80729a4aa38869b7aa06ce5289c03aa5f4310e11c712f95b0382d2e3bdd63a6", null ]
    ] ],
    [ "UniqueBehavior", "classDigikam_1_1ItemComments.html#a04ba63b0b04ba62ea1a17c628f00233d", [
      [ "UniquePerLanguage", "classDigikam_1_1ItemComments.html#a04ba63b0b04ba62ea1a17c628f00233da139fbce15d81bae2de217eee13043e72", null ],
      [ "UniquePerLanguageAndAuthor", "classDigikam_1_1ItemComments.html#a04ba63b0b04ba62ea1a17c628f00233da3ccd112d7aa5d11329f590db0621a070", null ]
    ] ],
    [ "ItemComments", "classDigikam_1_1ItemComments.html#ae4f48f3f24844df9cb43c97c3a810bf4", null ],
    [ "ItemComments", "classDigikam_1_1ItemComments.html#ad38a7cd71ea97eaab171fa594d63c365", null ],
    [ "ItemComments", "classDigikam_1_1ItemComments.html#aeb3c8873a140c230d1f6e99bd785605d", null ],
    [ "ItemComments", "classDigikam_1_1ItemComments.html#a060288c8ff706bdcf09809299318fbbf", null ],
    [ "~ItemComments", "classDigikam_1_1ItemComments.html#a2bd283ae1d722af18462676c1baefdc2", null ],
    [ "addComment", "classDigikam_1_1ItemComments.html#a1a4d9aae28aa1c767819ea34bdff4693", null ],
    [ "addCommentDirectly", "classDigikam_1_1ItemComments.html#a5f88f75a007a2ef9f7d700c6272c5c9c", null ],
    [ "addHeadline", "classDigikam_1_1ItemComments.html#af15d4dc60cf5509817021dffe14f7155", null ],
    [ "addTitle", "classDigikam_1_1ItemComments.html#abdc4ed71d24436cd50644a6fb3b99eb8", null ],
    [ "apply", "classDigikam_1_1ItemComments.html#abdbcd9bf34b253939430761069a9bbfa", null ],
    [ "apply", "classDigikam_1_1ItemComments.html#ac57f482fa5656a02653e0105ff719bf6", null ],
    [ "author", "classDigikam_1_1ItemComments.html#aef8b6bb1c478787a3b1cd9fc8a37547d", null ],
    [ "changeAuthor", "classDigikam_1_1ItemComments.html#acf1c2b90772aa73714b872ce5d9edcea", null ],
    [ "changeComment", "classDigikam_1_1ItemComments.html#a7927c3446e393a5292d88a489da21919", null ],
    [ "changeDate", "classDigikam_1_1ItemComments.html#a3944022041b6f0cff513f3610a4dcc57", null ],
    [ "changeLanguage", "classDigikam_1_1ItemComments.html#a13725137c1817b1fb6ec3fdb6dfd2f16", null ],
    [ "changeType", "classDigikam_1_1ItemComments.html#aba0f458fb79673bedbbd95ce18510574", null ],
    [ "comment", "classDigikam_1_1ItemComments.html#a7645a821b7877e2bac7f83861720c9eb", null ],
    [ "commentForLanguage", "classDigikam_1_1ItemComments.html#aab66e306821dad16a0a9d9ed577fbd59", null ],
    [ "date", "classDigikam_1_1ItemComments.html#a671a83174ac47d9bfcd6f58f3e804850", null ],
    [ "defaultComment", "classDigikam_1_1ItemComments.html#a22239aa4b2f8de102914a978c813f61c", null ],
    [ "defaultComment", "classDigikam_1_1ItemComments.html#a0ffa686e09690f5ad998fe3274fbc7db", null ],
    [ "isNull", "classDigikam_1_1ItemComments.html#a03a351916052a3d22990873629da5afd", null ],
    [ "language", "classDigikam_1_1ItemComments.html#adafc34f077cbdf85e8bfa0ebf54e14e8", null ],
    [ "numberOfComments", "classDigikam_1_1ItemComments.html#ad3ad56a9a1b90da2d2ff3bfdc1a2b4a4", null ],
    [ "operator=", "classDigikam_1_1ItemComments.html#ade819edf82e4554e9c65cfa4ae92eb01", null ],
    [ "remove", "classDigikam_1_1ItemComments.html#a1d4d166060641291c79d9928e3ec38de", null ],
    [ "removeAll", "classDigikam_1_1ItemComments.html#a04fce4a4d28bf091fd9087fbbfabfad3", null ],
    [ "removeAll", "classDigikam_1_1ItemComments.html#a1111e9f1ca5d0ddb1d8b44a82d8cc98e", null ],
    [ "removeAllComments", "classDigikam_1_1ItemComments.html#a71e6eac7561739e8a6461f04fb12cb70", null ],
    [ "replaceComments", "classDigikam_1_1ItemComments.html#aec5807c93f4e36c65ab98c4b8b7c9947", null ],
    [ "replaceFrom", "classDigikam_1_1ItemComments.html#ab9c28bb60e9de7196494e402671dca1b", null ],
    [ "setUniqueBehavior", "classDigikam_1_1ItemComments.html#a781bb05d47dd3c7bfa9babcc0f3be476", null ],
    [ "toCaptionsMap", "classDigikam_1_1ItemComments.html#a54d919d37e98825b9c9b996ade7b634b", null ],
    [ "type", "classDigikam_1_1ItemComments.html#a54764b3bf2e03d64a4858cd11704f651", null ],
    [ "d", "classDigikam_1_1ItemComments.html#a5752e1a37d96b1dd9ef50b32f9489301", null ]
];