var classDigikamGenericMetadataEditPlugin_1_1XMPOrigin =
[
    [ "XMPOrigin", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#aa58d79bbfa048cc6445982215be861cb", null ],
    [ "~XMPOrigin", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#a04dc8308a545d7b581f92240e003dc4f", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#a1b0303378920f603ed3f73777fbcf858", null ],
    [ "getXMPCreationDate", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#a2e1fdbc93b08114759354472ac5c75f9", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#abef501bdd297c25d8a24d6916609c753", null ],
    [ "setCheckedSyncEXIFDate", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#a6b6baa2e6846d00fa4ccbcdc41ff7fd8", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#a019acb876e9db8319060fbe0ee121b4e", null ],
    [ "syncEXIFDateIsChecked", "classDigikamGenericMetadataEditPlugin_1_1XMPOrigin.html#a1a1b273bf91fcc3f91f383c61aa831ed", null ]
];