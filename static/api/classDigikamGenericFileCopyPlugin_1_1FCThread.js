var classDigikamGenericFileCopyPlugin_1_1FCThread =
[
    [ "FCThread", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#aa8319cbae757d14109d92aad723ca90d", null ],
    [ "~FCThread", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a612ea73d1ba9d23f7e97253c9385f1e2", null ],
    [ "appendJobs", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a354a7c86ac83b4aa46e723149032a471", null ],
    [ "cancel", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#aabfecc7e78e7bff771e9f466ffad011d", null ],
    [ "createCopyJobs", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a1b27e4b1ddebc6319749424ee68ac19b", null ],
    [ "isEmpty", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a61f8a4a39061aed1ec1c105a0eddad67", null ],
    [ "maximumNumberOfThreads", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#ae1dc672ceb1c9f127db1d53fe6d40700", null ],
    [ "pendingCount", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#abf47dc040dce8ea4779ecb8c1fb375d0", null ],
    [ "run", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a2373a97a31a8897956bf86b4abebff09", null ],
    [ "setDefaultMaximumNumberOfThreads", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a99e7e530af4825351ba34362385e08fa", null ],
    [ "setMaximumNumberOfThreads", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a3c5aecc58f8d60ea5d2f8d35ef5649c2", null ],
    [ "signalCancelTask", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a1311d496d0cba757d0916ca70aeaa599", null ],
    [ "signalUrlProcessed", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#ad5168a6748e833ac479acc4537381e13", null ],
    [ "slotJobFinished", "classDigikamGenericFileCopyPlugin_1_1FCThread.html#a90f7300fc37ec60e08a3101b68da6409", null ]
];