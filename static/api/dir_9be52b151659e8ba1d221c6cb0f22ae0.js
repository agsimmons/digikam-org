var dir_9be52b151659e8ba1d221c6cb0f22ae0 =
[
    [ "effectmngr.cpp", "effectmngr_8cpp.html", null ],
    [ "effectmngr.h", "effectmngr_8h.html", [
      [ "EffectMngr", "classDigikam_1_1EffectMngr.html", "classDigikam_1_1EffectMngr" ]
    ] ],
    [ "effectmngr_p.cpp", "effectmngr__p_8cpp.html", null ],
    [ "effectmngr_p.h", "effectmngr__p_8h.html", [
      [ "Private", "classDigikam_1_1EffectMngr_1_1Private.html", "classDigikam_1_1EffectMngr_1_1Private" ]
    ] ],
    [ "effectmngr_p_pan.cpp", "effectmngr__p__pan_8cpp.html", null ],
    [ "effectmngr_p_zoom.cpp", "effectmngr__p__zoom_8cpp.html", null ],
    [ "effectpreview.cpp", "effectpreview_8cpp.html", null ],
    [ "effectpreview.h", "effectpreview_8h.html", [
      [ "EffectPreview", "classDigikam_1_1EffectPreview.html", "classDigikam_1_1EffectPreview" ]
    ] ],
    [ "frameutils.cpp", "frameutils_8cpp.html", null ],
    [ "frameutils.h", "frameutils_8h.html", [
      [ "FrameUtils", "classDigikam_1_1FrameUtils.html", null ]
    ] ],
    [ "transitionmngr.cpp", "transitionmngr_8cpp.html", null ],
    [ "transitionmngr.h", "transitionmngr_8h.html", [
      [ "TransitionMngr", "classDigikam_1_1TransitionMngr.html", "classDigikam_1_1TransitionMngr" ]
    ] ],
    [ "transitionmngr_p.cpp", "transitionmngr__p_8cpp.html", null ],
    [ "transitionmngr_p.h", "transitionmngr__p_8h.html", [
      [ "Private", "classDigikam_1_1TransitionMngr_1_1Private.html", "classDigikam_1_1TransitionMngr_1_1Private" ]
    ] ],
    [ "transitionmngr_p_abstract.cpp", "transitionmngr__p__abstract_8cpp.html", null ],
    [ "transitionmngr_p_blur.cpp", "transitionmngr__p__blur_8cpp.html", null ],
    [ "transitionmngr_p_lines.cpp", "transitionmngr__p__lines_8cpp.html", null ],
    [ "transitionmngr_p_push.cpp", "transitionmngr__p__push_8cpp.html", null ],
    [ "transitionmngr_p_slide.cpp", "transitionmngr__p__slide_8cpp.html", null ],
    [ "transitionmngr_p_squares.cpp", "transitionmngr__p__squares_8cpp.html", null ],
    [ "transitionmngr_p_swap.cpp", "transitionmngr__p__swap_8cpp.html", null ],
    [ "transitionpreview.cpp", "transitionpreview_8cpp.html", null ],
    [ "transitionpreview.h", "transitionpreview_8h.html", [
      [ "TransitionPreview", "classDigikam_1_1TransitionPreview.html", "classDigikam_1_1TransitionPreview" ]
    ] ]
];