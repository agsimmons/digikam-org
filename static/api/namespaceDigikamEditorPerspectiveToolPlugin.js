var namespaceDigikamEditorPerspectiveToolPlugin =
[
    [ "PerspectiveMatrix", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix" ],
    [ "PerspectiveTool", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTool.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTool" ],
    [ "PerspectiveToolPlugin", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveToolPlugin.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveToolPlugin" ],
    [ "PerspectiveTriangle", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTriangle.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTriangle" ],
    [ "PerspectiveWidget", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveWidget.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveWidget" ]
];