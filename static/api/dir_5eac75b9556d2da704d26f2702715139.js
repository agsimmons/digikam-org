var dir_5eac75b9556d2da704d26f2702715139 =
[
    [ "actions.h", "actions_8h.html", [
      [ "ActionData", "classDigikam_1_1ActionData.html", "classDigikam_1_1ActionData" ]
    ] ],
    [ "actionthread.cpp", "actionthread_8cpp.html", null ],
    [ "actionthread.h", "actionthread_8h.html", [
      [ "ActionThread", "classDigikam_1_1ActionThread.html", "classDigikam_1_1ActionThread" ]
    ] ],
    [ "batchtool.cpp", "batchtool_8cpp.html", null ],
    [ "batchtool.h", "batchtool_8h.html", "batchtool_8h" ],
    [ "batchtoolsfactory.cpp", "batchtoolsfactory_8cpp.html", null ],
    [ "batchtoolsfactory.h", "batchtoolsfactory_8h.html", [
      [ "BatchToolsFactory", "classDigikam_1_1BatchToolsFactory.html", "classDigikam_1_1BatchToolsFactory" ]
    ] ],
    [ "batchtoolutils.cpp", "batchtoolutils_8cpp.html", "batchtoolutils_8cpp" ],
    [ "batchtoolutils.h", "batchtoolutils_8h.html", "batchtoolutils_8h" ],
    [ "iteminfoset.h", "iteminfoset_8h.html", "iteminfoset_8h" ],
    [ "queuesettings.cpp", "queuesettings_8cpp.html", null ],
    [ "queuesettings.h", "queuesettings_8h.html", [
      [ "QueueSettings", "classDigikam_1_1QueueSettings.html", "classDigikam_1_1QueueSettings" ]
    ] ],
    [ "task.cpp", "task_8cpp.html", null ],
    [ "task.h", "task_8h.html", [
      [ "Task", "classDigikam_1_1Task.html", "classDigikam_1_1Task" ]
    ] ],
    [ "workflowmanager.cpp", "workflowmanager_8cpp.html", null ],
    [ "workflowmanager.h", "workflowmanager_8h.html", [
      [ "Workflow", "classDigikam_1_1Workflow.html", "classDigikam_1_1Workflow" ],
      [ "WorkflowManager", "classDigikam_1_1WorkflowManager.html", "classDigikam_1_1WorkflowManager" ]
    ] ]
];