var namespaceDigikamGenericFileTransferPlugin =
[
    [ "FTExportWidget", "classDigikamGenericFileTransferPlugin_1_1FTExportWidget.html", "classDigikamGenericFileTransferPlugin_1_1FTExportWidget" ],
    [ "FTExportWindow", "classDigikamGenericFileTransferPlugin_1_1FTExportWindow.html", "classDigikamGenericFileTransferPlugin_1_1FTExportWindow" ],
    [ "FTImportWidget", "classDigikamGenericFileTransferPlugin_1_1FTImportWidget.html", "classDigikamGenericFileTransferPlugin_1_1FTImportWidget" ],
    [ "FTImportWindow", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow.html", "classDigikamGenericFileTransferPlugin_1_1FTImportWindow" ],
    [ "FTPlugin", "classDigikamGenericFileTransferPlugin_1_1FTPlugin.html", "classDigikamGenericFileTransferPlugin_1_1FTPlugin" ]
];