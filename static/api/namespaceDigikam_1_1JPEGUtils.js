var namespaceDigikam_1_1JPEGUtils =
[
    [ "digikam_source_mgr", "structDigikam_1_1JPEGUtils_1_1digikam__source__mgr.html", "structDigikam_1_1JPEGUtils_1_1digikam__source__mgr" ],
    [ "JpegRotator", "classDigikam_1_1JPEGUtils_1_1JpegRotator.html", "classDigikam_1_1JPEGUtils_1_1JpegRotator" ],
    [ "TransformAction", "namespaceDigikam_1_1JPEGUtils.html#a8d0b58b9278013f0286752da00c84b48", null ],
    [ "fill_input_buffer", "namespaceDigikam_1_1JPEGUtils.html#a12fb1931afda411f6d2cb42e71da638a", null ],
    [ "getJpegQuality", "namespaceDigikam_1_1JPEGUtils.html#a21e3fdcb74544837c243205ea939594e", null ],
    [ "init_source", "namespaceDigikam_1_1JPEGUtils.html#a682a9b5df93a1a2c2fcf6a895328444f", null ],
    [ "isJpegImage", "namespaceDigikam_1_1JPEGUtils.html#a0ffb1734e8e905b500c86b5709c8f341", null ],
    [ "jpeg_memory_src", "namespaceDigikam_1_1JPEGUtils.html#a8b7574de419e947429295f1a38583cbe", null ],
    [ "jpegConvert", "namespaceDigikam_1_1JPEGUtils.html#a30d5b3b05392d002a35b599f5ad13fd4", null ],
    [ "loadJPEGScaled", "namespaceDigikam_1_1JPEGUtils.html#aa441a3222bb8a0fb8eb2be8edd40c4cf", null ],
    [ "skip_input_data", "namespaceDigikam_1_1JPEGUtils.html#a5c900838f0a9592c1dcefac7d79af9d1", null ],
    [ "term_source", "namespaceDigikam_1_1JPEGUtils.html#a3d169f0f42c1fe9baf54b01d3491000f", null ]
];