var dir_add1ebd0e0029bd8bbcec2f704a52d67 =
[
    [ "albumfolderviewsidebarwidget.cpp", "albumfolderviewsidebarwidget_8cpp.html", null ],
    [ "albumfolderviewsidebarwidget.h", "albumfolderviewsidebarwidget_8h.html", [
      [ "AlbumFolderViewSideBarWidget", "classDigikam_1_1AlbumFolderViewSideBarWidget.html", "classDigikam_1_1AlbumFolderViewSideBarWidget" ]
    ] ],
    [ "datefolderviewsidebarwidget.cpp", "datefolderviewsidebarwidget_8cpp.html", null ],
    [ "datefolderviewsidebarwidget.h", "datefolderviewsidebarwidget_8h.html", [
      [ "DateFolderViewSideBarWidget", "classDigikam_1_1DateFolderViewSideBarWidget.html", "classDigikam_1_1DateFolderViewSideBarWidget" ]
    ] ],
    [ "fuzzysearchsidebarwidget.cpp", "fuzzysearchsidebarwidget_8cpp.html", null ],
    [ "fuzzysearchsidebarwidget.h", "fuzzysearchsidebarwidget_8h.html", [
      [ "FuzzySearchSideBarWidget", "classDigikam_1_1FuzzySearchSideBarWidget.html", "classDigikam_1_1FuzzySearchSideBarWidget" ]
    ] ],
    [ "gpssearchsidebarwidget.cpp", "gpssearchsidebarwidget_8cpp.html", null ],
    [ "gpssearchsidebarwidget.h", "gpssearchsidebarwidget_8h.html", [
      [ "GPSSearchSideBarWidget", "classDigikam_1_1GPSSearchSideBarWidget.html", "classDigikam_1_1GPSSearchSideBarWidget" ]
    ] ],
    [ "labelssidebarwidget.cpp", "labelssidebarwidget_8cpp.html", null ],
    [ "labelssidebarwidget.h", "labelssidebarwidget_8h.html", [
      [ "LabelsSideBarWidget", "classDigikam_1_1LabelsSideBarWidget.html", "classDigikam_1_1LabelsSideBarWidget" ]
    ] ],
    [ "peoplesidebarwidget.cpp", "peoplesidebarwidget_8cpp.html", null ],
    [ "peoplesidebarwidget.h", "peoplesidebarwidget_8h.html", [
      [ "PeopleSideBarWidget", "classDigikam_1_1PeopleSideBarWidget.html", "classDigikam_1_1PeopleSideBarWidget" ]
    ] ],
    [ "searchsidebarwidget.cpp", "searchsidebarwidget_8cpp.html", null ],
    [ "searchsidebarwidget.h", "searchsidebarwidget_8h.html", [
      [ "SearchSideBarWidget", "classDigikam_1_1SearchSideBarWidget.html", "classDigikam_1_1SearchSideBarWidget" ]
    ] ],
    [ "sidebarwidget.cpp", "sidebarwidget_8cpp.html", null ],
    [ "sidebarwidget.h", "sidebarwidget_8h.html", [
      [ "SidebarWidget", "classDigikam_1_1SidebarWidget.html", "classDigikam_1_1SidebarWidget" ]
    ] ],
    [ "tagviewsidebarwidget.cpp", "tagviewsidebarwidget_8cpp.html", null ],
    [ "tagviewsidebarwidget.h", "tagviewsidebarwidget_8h.html", [
      [ "TagViewSideBarWidget", "classDigikam_1_1TagViewSideBarWidget.html", "classDigikam_1_1TagViewSideBarWidget" ]
    ] ],
    [ "timelinesidebarwidget.cpp", "timelinesidebarwidget_8cpp.html", null ],
    [ "timelinesidebarwidget.h", "timelinesidebarwidget_8h.html", [
      [ "TimelineSideBarWidget", "classDigikam_1_1TimelineSideBarWidget.html", "classDigikam_1_1TimelineSideBarWidget" ]
    ] ]
];