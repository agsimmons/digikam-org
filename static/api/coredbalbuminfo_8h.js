var coredbalbuminfo_8h =
[
    [ "AlbumInfo", "classDigikam_1_1AlbumInfo.html", "classDigikam_1_1AlbumInfo" ],
    [ "AlbumRootInfo", "classDigikam_1_1AlbumRootInfo.html", "classDigikam_1_1AlbumRootInfo" ],
    [ "AlbumShortInfo", "classDigikam_1_1AlbumShortInfo.html", "classDigikam_1_1AlbumShortInfo" ],
    [ "CommentInfo", "classDigikam_1_1CommentInfo.html", "classDigikam_1_1CommentInfo" ],
    [ "CopyrightInfo", "classDigikam_1_1CopyrightInfo.html", "classDigikam_1_1CopyrightInfo" ],
    [ "ImageHistoryEntry", "classDigikam_1_1ImageHistoryEntry.html", "classDigikam_1_1ImageHistoryEntry" ],
    [ "ImageRelation", "classDigikam_1_1ImageRelation.html", "classDigikam_1_1ImageRelation" ],
    [ "ImageTagProperty", "classDigikam_1_1ImageTagProperty.html", "classDigikam_1_1ImageTagProperty" ],
    [ "ItemScanInfo", "classDigikam_1_1ItemScanInfo.html", "classDigikam_1_1ItemScanInfo" ],
    [ "ItemShortInfo", "classDigikam_1_1ItemShortInfo.html", "classDigikam_1_1ItemShortInfo" ],
    [ "SearchInfo", "classDigikam_1_1SearchInfo.html", "classDigikam_1_1SearchInfo" ],
    [ "TagInfo", "classDigikam_1_1TagInfo.html", "classDigikam_1_1TagInfo" ],
    [ "TagProperty", "classDigikam_1_1TagProperty.html", "classDigikam_1_1TagProperty" ],
    [ "TagShortInfo", "classDigikam_1_1TagShortInfo.html", "classDigikam_1_1TagShortInfo" ],
    [ "YearMonth", "coredbalbuminfo_8h.html#ace66dcd10835958ebf8f5cbe93a0df20", null ]
];