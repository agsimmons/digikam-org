var classDigikam_1_1Haar_1_1Weights =
[
    [ "SketchType", "classDigikam_1_1Haar_1_1Weights.html#a7e1e21a4a3cb8af888ad3fafe63614c2", [
      [ "ScannedSketch", "classDigikam_1_1Haar_1_1Weights.html#a7e1e21a4a3cb8af888ad3fafe63614c2aefc7df3aca1a5be6541bc2c2c3044eab", null ],
      [ "PaintedSketch", "classDigikam_1_1Haar_1_1Weights.html#a7e1e21a4a3cb8af888ad3fafe63614c2af2d7a1aaa12303b975856f69c65eb780", null ]
    ] ],
    [ "Weights", "classDigikam_1_1Haar_1_1Weights.html#a3470b55b1ae30bd0eecd309705e2d117", null ],
    [ "weight", "classDigikam_1_1Haar_1_1Weights.html#a37c921d7387d397aee4dda61ce0e60fc", null ],
    [ "weightForAverage", "classDigikam_1_1Haar_1_1Weights.html#ac4bfb7e7628420a13e16b1b3a6e9cc61", null ]
];