var dir_7ce59bfbdebf3f572e6d3f6fe8c541c9 =
[
    [ "loadingcache.cpp", "loadingcache_8cpp.html", null ],
    [ "loadingcache.h", "loadingcache_8h.html", [
      [ "LoadingCache", "classDigikam_1_1LoadingCache.html", "classDigikam_1_1LoadingCache" ],
      [ "CacheLock", "classDigikam_1_1LoadingCache_1_1CacheLock.html", "classDigikam_1_1LoadingCache_1_1CacheLock" ],
      [ "LoadingCacheFileWatch", "classDigikam_1_1LoadingCacheFileWatch.html", "classDigikam_1_1LoadingCacheFileWatch" ],
      [ "LoadingProcess", "classDigikam_1_1LoadingProcess.html", "classDigikam_1_1LoadingProcess" ],
      [ "LoadingProcessListener", "classDigikam_1_1LoadingProcessListener.html", "classDigikam_1_1LoadingProcessListener" ]
    ] ],
    [ "loadingcacheinterface.cpp", "loadingcacheinterface_8cpp.html", null ],
    [ "loadingcacheinterface.h", "loadingcacheinterface_8h.html", [
      [ "LoadingCacheInterface", "classDigikam_1_1LoadingCacheInterface.html", null ]
    ] ],
    [ "loadingdescription.cpp", "loadingdescription_8cpp.html", null ],
    [ "loadingdescription.h", "loadingdescription_8h.html", [
      [ "LoadingDescription", "classDigikam_1_1LoadingDescription.html", "classDigikam_1_1LoadingDescription" ],
      [ "PostProcessingParameters", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters.html", "classDigikam_1_1LoadingDescription_1_1PostProcessingParameters" ],
      [ "PreviewParameters", "classDigikam_1_1LoadingDescription_1_1PreviewParameters.html", "classDigikam_1_1LoadingDescription_1_1PreviewParameters" ]
    ] ],
    [ "loadsavetask.cpp", "loadsavetask_8cpp.html", null ],
    [ "loadsavetask.h", "loadsavetask_8h.html", [
      [ "LoadingTask", "classDigikam_1_1LoadingTask.html", "classDigikam_1_1LoadingTask" ],
      [ "LoadSaveTask", "classDigikam_1_1LoadSaveTask.html", "classDigikam_1_1LoadSaveTask" ],
      [ "SavingTask", "classDigikam_1_1SavingTask.html", "classDigikam_1_1SavingTask" ],
      [ "SharedLoadingTask", "classDigikam_1_1SharedLoadingTask.html", "classDigikam_1_1SharedLoadingTask" ]
    ] ],
    [ "loadsavethread.cpp", "loadsavethread_8cpp.html", null ],
    [ "loadsavethread.h", "loadsavethread_8h.html", [
      [ "LoadSaveFileInfoProvider", "classDigikam_1_1LoadSaveFileInfoProvider.html", "classDigikam_1_1LoadSaveFileInfoProvider" ],
      [ "LoadSaveNotifier", "classDigikam_1_1LoadSaveNotifier.html", "classDigikam_1_1LoadSaveNotifier" ],
      [ "LoadSaveThread", "classDigikam_1_1LoadSaveThread.html", "classDigikam_1_1LoadSaveThread" ]
    ] ]
];