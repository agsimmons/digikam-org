var dir_6757c55dca7f51dc6114bd021b008490 =
[
    [ "mjpegframeosd.cpp", "mjpegframeosd_8cpp.html", null ],
    [ "mjpegframeosd.h", "mjpegframeosd_8h.html", [
      [ "MjpegFrameOsd", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameOsd.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameOsd" ]
    ] ],
    [ "mjpegframetask.cpp", "mjpegframetask_8cpp.html", null ],
    [ "mjpegframetask.h", "mjpegframetask_8h.html", [
      [ "MjpegFrameTask", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask" ]
    ] ],
    [ "mjpegframethread.cpp", "mjpegframethread_8cpp.html", null ],
    [ "mjpegframethread.h", "mjpegframethread_8h.html", [
      [ "MjpegFrameThread", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread" ]
    ] ],
    [ "mjpegserver.cpp", "mjpegserver_8cpp.html", null ],
    [ "mjpegserver.h", "mjpegserver_8h.html", "mjpegserver_8h" ],
    [ "mjpegserver_p.cpp", "mjpegserver__p_8cpp.html", null ],
    [ "mjpegserver_p.h", "mjpegserver__p_8h.html", [
      [ "Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer_1_1Private" ]
    ] ],
    [ "mjpegservermngr.cpp", "mjpegservermngr_8cpp.html", null ],
    [ "mjpegservermngr.h", "mjpegservermngr_8h.html", [
      [ "MjpegServerMngr", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr" ]
    ] ],
    [ "mjpegstreamdlg.cpp", "mjpegstreamdlg_8cpp.html", null ],
    [ "mjpegstreamdlg.h", "mjpegstreamdlg_8h.html", [
      [ "MjpegStreamDlg", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg" ]
    ] ],
    [ "mjpegstreamdlg_p.cpp", "mjpegstreamdlg__p_8cpp.html", null ],
    [ "mjpegstreamdlg_p.h", "mjpegstreamdlg__p_8h.html", [
      [ "Private", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg_1_1Private.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg_1_1Private" ]
    ] ],
    [ "mjpegstreamdlg_server.cpp", "mjpegstreamdlg__server_8cpp.html", null ],
    [ "mjpegstreamdlg_settings.cpp", "mjpegstreamdlg__settings_8cpp.html", null ],
    [ "mjpegstreamdlg_views.cpp", "mjpegstreamdlg__views_8cpp.html", null ],
    [ "mjpegstreamplugin.cpp", "mjpegstreamplugin_8cpp.html", null ],
    [ "mjpegstreamplugin.h", "mjpegstreamplugin_8h.html", "mjpegstreamplugin_8h" ],
    [ "mjpegstreamsettings.cpp", "mjpegstreamsettings_8cpp.html", null ],
    [ "mjpegstreamsettings.h", "mjpegstreamsettings_8h.html", [
      [ "MjpegStreamSettings", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamSettings.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamSettings" ]
    ] ]
];