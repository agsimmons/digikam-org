var classDigikam_1_1DNNFaceExtractor =
[
    [ "DNNFaceExtractor", "classDigikam_1_1DNNFaceExtractor.html#a1ece0834a21c4b04fb763f9b94c630a6", null ],
    [ "DNNFaceExtractor", "classDigikam_1_1DNNFaceExtractor.html#ae65c7336bfeb41295c94347f84a8b77c", null ],
    [ "~DNNFaceExtractor", "classDigikam_1_1DNNFaceExtractor.html#a4a016ac4d131f98b4209494e218486dc", null ],
    [ "alignFace", "classDigikam_1_1DNNFaceExtractor.html#a494df1aa2467702b5d53859ff596de35", null ],
    [ "getFaceEmbedding", "classDigikam_1_1DNNFaceExtractor.html#a7b267d71ab65766f99eea00509a7573b", null ],
    [ "loadModels", "classDigikam_1_1DNNFaceExtractor.html#aa321f51edf9ec0d8cc4caa1b1b2102d1", null ]
];