var classDigikam_1_1GPSDBJobInfo =
[
    [ "GPSDBJobInfo", "classDigikam_1_1GPSDBJobInfo.html#a1fdbb5812054562f27f8cc499437b3c6", null ],
    [ "isDirectQuery", "classDigikam_1_1GPSDBJobInfo.html#aa01ee7282cdfc021abefbaef1b652750", null ],
    [ "isFoldersJob", "classDigikam_1_1GPSDBJobInfo.html#a1b1b009cdf8f611bc10488acb3d37026", null ],
    [ "isListAvailableImagesOnly", "classDigikam_1_1GPSDBJobInfo.html#a3dc339cb6285b0debc56a6cb4d844835", null ],
    [ "isRecursive", "classDigikam_1_1GPSDBJobInfo.html#a3998e8c5613f83bf74f126fa68ea3e04", null ],
    [ "lat1", "classDigikam_1_1GPSDBJobInfo.html#a0e0f4ee41b12fbbffb55a6eb5ebba7f1", null ],
    [ "lat2", "classDigikam_1_1GPSDBJobInfo.html#a275343e88c54cd257ff0ce1dd6e723db", null ],
    [ "lng1", "classDigikam_1_1GPSDBJobInfo.html#a5b848ff3483e1050e176152d2b783134", null ],
    [ "lng2", "classDigikam_1_1GPSDBJobInfo.html#af191a21df9dd88bc16307a04d5f72883", null ],
    [ "setDirectQuery", "classDigikam_1_1GPSDBJobInfo.html#a3f7ad92762d99e2e381acb16aecc832f", null ],
    [ "setFoldersJob", "classDigikam_1_1GPSDBJobInfo.html#afd02b67871ce293b7f6420913a0fa391", null ],
    [ "setLat1", "classDigikam_1_1GPSDBJobInfo.html#a843da8576604a7ed07aba5718a8d203a", null ],
    [ "setLat2", "classDigikam_1_1GPSDBJobInfo.html#a894ebdc655ffc8fd0d5f214d04e77190", null ],
    [ "setListAvailableImagesOnly", "classDigikam_1_1GPSDBJobInfo.html#ac844bb2285615c4a3f37902934f72fc4", null ],
    [ "setLng1", "classDigikam_1_1GPSDBJobInfo.html#aa61959352dcb342a923bdd082c561ea0", null ],
    [ "setLng2", "classDigikam_1_1GPSDBJobInfo.html#a9d6b49beb634f66a3ddd7c11044807a1", null ],
    [ "setRecursive", "classDigikam_1_1GPSDBJobInfo.html#a15cd3f8162a63be05d794988f1453dd2", null ]
];