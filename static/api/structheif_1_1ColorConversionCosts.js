var structheif_1_1ColorConversionCosts =
[
    [ "ColorConversionCosts", "structheif_1_1ColorConversionCosts.html#a6d8ed0fa50999c92f0f8a10af85e7ffa", null ],
    [ "ColorConversionCosts", "structheif_1_1ColorConversionCosts.html#a7fbb77e31b5488056e20b771c68dad14", null ],
    [ "operator+", "structheif_1_1ColorConversionCosts.html#a096b09820b4809d24c4da503173ea9c9", null ],
    [ "total", "structheif_1_1ColorConversionCosts.html#aac0383a8ef83c3eb32f0cb8d4fd8d76f", null ],
    [ "memory", "structheif_1_1ColorConversionCosts.html#a5de567d918662df516358fa9544e33f0", null ],
    [ "quality", "structheif_1_1ColorConversionCosts.html#a5c7d75f4fca80314f927bdfe4b2114f4", null ],
    [ "speed", "structheif_1_1ColorConversionCosts.html#ae14501276d74953dcfff18b3de22d4d5", null ]
];