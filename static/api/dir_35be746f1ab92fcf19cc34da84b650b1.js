var dir_35be746f1ab92fcf19cc34da84b650b1 =
[
    [ "mailalbumspage.cpp", "mailalbumspage_8cpp.html", null ],
    [ "mailalbumspage.h", "mailalbumspage_8h.html", [
      [ "MailAlbumsPage", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage" ]
    ] ],
    [ "mailfinalpage.cpp", "mailfinalpage_8cpp.html", null ],
    [ "mailfinalpage.h", "mailfinalpage_8h.html", [
      [ "MailFinalPage", "classDigikamGenericSendByMailPlugin_1_1MailFinalPage.html", "classDigikamGenericSendByMailPlugin_1_1MailFinalPage" ]
    ] ],
    [ "mailimagespage.cpp", "mailimagespage_8cpp.html", null ],
    [ "mailimagespage.h", "mailimagespage_8h.html", [
      [ "MailImagesPage", "classDigikamGenericSendByMailPlugin_1_1MailImagesPage.html", "classDigikamGenericSendByMailPlugin_1_1MailImagesPage" ]
    ] ],
    [ "mailintropage.cpp", "mailintropage_8cpp.html", null ],
    [ "mailintropage.h", "mailintropage_8h.html", [
      [ "MailIntroPage", "classDigikamGenericSendByMailPlugin_1_1MailIntroPage.html", "classDigikamGenericSendByMailPlugin_1_1MailIntroPage" ]
    ] ],
    [ "mailsettingspage.cpp", "mailsettingspage_8cpp.html", null ],
    [ "mailsettingspage.h", "mailsettingspage_8h.html", [
      [ "MailSettingsPage", "classDigikamGenericSendByMailPlugin_1_1MailSettingsPage.html", "classDigikamGenericSendByMailPlugin_1_1MailSettingsPage" ]
    ] ],
    [ "mailwizard.cpp", "mailwizard_8cpp.html", null ],
    [ "mailwizard.h", "mailwizard_8h.html", [
      [ "MailWizard", "classDigikamGenericSendByMailPlugin_1_1MailWizard.html", "classDigikamGenericSendByMailPlugin_1_1MailWizard" ]
    ] ]
];