var classDigikamGenericMetadataEditPlugin_1_1XMPCredits =
[
    [ "XMPCredits", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#aed227c744473223ca3d54fc7c5935e87", null ],
    [ "~XMPCredits", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#a8f1ace43972b5d78dc2787db50343c9a", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#ab1332966ac6b7dad8eed34ee2138bb5f", null ],
    [ "getXMPByLine", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#a92b44db1deeef6e85fd982c1aa126156", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#adaa7efd6a697818de7494964449dacea", null ],
    [ "setCheckedSyncEXIFArtist", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#aec7e3676bccc81cb7f7fe9a5f4f73fe6", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#a9bf2d6e6d30783645ad0bab63bb273b3", null ],
    [ "syncEXIFArtistIsChecked", "classDigikamGenericMetadataEditPlugin_1_1XMPCredits.html#ae13f525c7a82867f4cd1a748a3c512cd", null ]
];