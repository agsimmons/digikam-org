var dir_8383d193d00e05b2d724be60681cdc59 =
[
    [ "printconfig.cpp", "printconfig_8cpp.html", null ],
    [ "printconfig.h", "printconfig_8h.html", [
      [ "PrintConfig", "classDigikamEditorPrintToolPlugin_1_1PrintConfig.html", "classDigikamEditorPrintToolPlugin_1_1PrintConfig" ]
    ] ],
    [ "printhelper.cpp", "printhelper_8cpp.html", null ],
    [ "printhelper.h", "printhelper_8h.html", [
      [ "PrintHelper", "classDigikamEditorPrintToolPlugin_1_1PrintHelper.html", "classDigikamEditorPrintToolPlugin_1_1PrintHelper" ]
    ] ],
    [ "printoptionspage.cpp", "printoptionspage_8cpp.html", null ],
    [ "printoptionspage.h", "printoptionspage_8h.html", [
      [ "PrintOptionsPage", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage.html", "classDigikamEditorPrintToolPlugin_1_1PrintOptionsPage" ]
    ] ],
    [ "printplugin.cpp", "printplugin_8cpp.html", null ],
    [ "printplugin.h", "printplugin_8h.html", "printplugin_8h" ]
];