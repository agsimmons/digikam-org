var dir_ca80a7bd7fc1cc7bd8c57574fd22c913 =
[
    [ "presentation_advpage.cpp", "presentation__advpage_8cpp.html", null ],
    [ "presentation_advpage.h", "presentation__advpage_8h.html", [
      [ "PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage" ]
    ] ],
    [ "presentation_captionpage.cpp", "presentation__captionpage_8cpp.html", null ],
    [ "presentation_captionpage.h", "presentation__captionpage_8h.html", [
      [ "PresentationCaptionPage", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage.html", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage" ]
    ] ],
    [ "presentation_mainpage.cpp", "presentation__mainpage_8cpp.html", null ],
    [ "presentation_mainpage.h", "presentation__mainpage_8h.html", [
      [ "PresentationMainPage", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage.html", "classDigikamGenericPresentationPlugin_1_1PresentationMainPage" ]
    ] ],
    [ "presentationdlg.cpp", "presentationdlg_8cpp.html", null ],
    [ "presentationdlg.h", "presentationdlg_8h.html", [
      [ "PresentationDlg", "classDigikamGenericPresentationPlugin_1_1PresentationDlg.html", "classDigikamGenericPresentationPlugin_1_1PresentationDlg" ]
    ] ]
];