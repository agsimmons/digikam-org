var heif__colorconversion_8h =
[
    [ "ColorConversionCosts", "structheif_1_1ColorConversionCosts.html", "structheif_1_1ColorConversionCosts" ],
    [ "ColorConversionOperation", "classheif_1_1ColorConversionOperation.html", "classheif_1_1ColorConversionOperation" ],
    [ "ColorConversionOptions", "structheif_1_1ColorConversionOptions.html", "structheif_1_1ColorConversionOptions" ],
    [ "ColorConversionPipeline", "classheif_1_1ColorConversionPipeline.html", "classheif_1_1ColorConversionPipeline" ],
    [ "ColorState", "structheif_1_1ColorState.html", "structheif_1_1ColorState" ],
    [ "ColorStateWithCost", "structheif_1_1ColorStateWithCost.html", "structheif_1_1ColorStateWithCost" ],
    [ "ColorConversionCriterion", "heif__colorconversion_8h.html#a185599a179acc7a495d5b26e1f9c804a", [
      [ "Speed", "heif__colorconversion_8h.html#a185599a179acc7a495d5b26e1f9c804aa44877c6aa8e93fa5a91c9361211464fb", null ],
      [ "Quality", "heif__colorconversion_8h.html#a185599a179acc7a495d5b26e1f9c804aa571094bb27864b600d8e6b561a137a55", null ],
      [ "Memory", "heif__colorconversion_8h.html#a185599a179acc7a495d5b26e1f9c804aa4789f23283b3a61f858b641a1bef19a3", null ],
      [ "Balanced", "heif__colorconversion_8h.html#a185599a179acc7a495d5b26e1f9c804aac6589f5236c0c434d0c63dc5f9a856f2", null ]
    ] ]
];