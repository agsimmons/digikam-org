var dir_118b89c5010a89af97de32ab5a42db14 =
[
    [ "main.cpp", "showfoto_2main_2main_8cpp.html", "showfoto_2main_2main_8cpp" ],
    [ "showfoto.cpp", "showfoto_8cpp.html", null ],
    [ "showfoto.h", "showfoto_8h.html", [
      [ "Showfoto", "classShowFoto_1_1Showfoto.html", "classShowFoto_1_1Showfoto" ]
    ] ],
    [ "showfoto_config.cpp", "showfoto__config_8cpp.html", null ],
    [ "showfoto_import.cpp", "showfoto__import_8cpp.html", null ],
    [ "showfoto_iofiles.cpp", "showfoto__iofiles_8cpp.html", null ],
    [ "showfoto_open.cpp", "showfoto__open_8cpp.html", null ],
    [ "showfoto_p.h", "showfoto__p_8h.html", [
      [ "Private", "classShowFoto_1_1Showfoto_1_1Private.html", "classShowFoto_1_1Showfoto_1_1Private" ]
    ] ],
    [ "showfoto_setup.cpp", "showfoto__setup_8cpp.html", null ],
    [ "showfoto_thumbbar.cpp", "showfoto__thumbbar_8cpp.html", null ],
    [ "showfotoinfoiface.cpp", "showfotoinfoiface_8cpp.html", null ],
    [ "showfotoinfoiface.h", "showfotoinfoiface_8h.html", [
      [ "ShowfotoInfoIface", "classShowFoto_1_1ShowfotoInfoIface.html", "classShowFoto_1_1ShowfotoInfoIface" ]
    ] ],
    [ "showfotosettings.cpp", "showfotosettings_8cpp.html", null ],
    [ "showfotosettings.h", "showfotosettings_8h.html", [
      [ "ShowfotoSettings", "classShowFoto_1_1ShowfotoSettings.html", "classShowFoto_1_1ShowfotoSettings" ]
    ] ]
];