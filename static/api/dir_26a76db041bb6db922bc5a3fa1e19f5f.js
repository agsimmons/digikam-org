var dir_26a76db041bb6db922bc5a3fa1e19f5f =
[
    [ "color", "dir_9c4cb3592602f77c71e58a4b917cce1a.html", "dir_9c4cb3592602f77c71e58a4b917cce1a" ],
    [ "filters", "dir_90ad111ee6b302482582737c0b6fcadd.html", "dir_90ad111ee6b302482582737c0b6fcadd" ],
    [ "history", "dir_2f5e5d58764afb87aa754d16bab28eb1.html", "dir_2f5e5d58764afb87aa754d16bab28eb1" ],
    [ "loaders", "dir_6c499f3d4fc012340ca8bc34de918447.html", "dir_6c499f3d4fc012340ca8bc34de918447" ],
    [ "dimg.cpp", "dimg_8cpp.html", null ],
    [ "dimg.h", "dimg_8h.html", [
      [ "DImg", "classDigikam_1_1DImg.html", "classDigikam_1_1DImg" ]
    ] ],
    [ "dimg_bitsops.cpp", "dimg__bitsops_8cpp.html", null ],
    [ "dimg_colors.cpp", "dimg__colors_8cpp.html", null ],
    [ "dimg_copy.cpp", "dimg__copy_8cpp.html", null ],
    [ "dimg_data.cpp", "dimg__data_8cpp.html", null ],
    [ "dimg_fileio.cpp", "dimg__fileio_8cpp.html", null ],
    [ "dimg_metadata.cpp", "dimg__metadata_8cpp.html", null ],
    [ "dimg_p.h", "dimg__p_8h.html", "dimg__p_8h" ],
    [ "dimg_props.cpp", "dimg__props_8cpp.html", null ],
    [ "dimg_qimage.cpp", "dimg__qimage_8cpp.html", null ],
    [ "dimg_qpixmap.cpp", "dimg__qpixmap_8cpp.html", null ],
    [ "dimg_scale.cpp", "dimg__scale_8cpp.html", "dimg__scale_8cpp" ],
    [ "dimg_transform.cpp", "dimg__transform_8cpp.html", null ],
    [ "exposurecontainer.h", "exposurecontainer_8h.html", [
      [ "ExposureSettingsContainer", "classDigikam_1_1ExposureSettingsContainer.html", "classDigikam_1_1ExposureSettingsContainer" ]
    ] ]
];