var classDigikam_1_1TrackCorrelator_1_1Correlation =
[
    [ "List", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#ad4bd4e017ccc26e6f905928e9f89dd7a", null ],
    [ "Correlation", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#a3a1d2903572798e70a6188fb99412e40", null ],
    [ "coordinates", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#adbdb1fc20fb9fcd9f308a8a55e7132ad", null ],
    [ "dateTime", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#a909718633bcc6882c82041c4c32b1788", null ],
    [ "fixType", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#aea2d0f813a5bf3bf3ab0a579b913a1a2", null ],
    [ "flags", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#ae5d293b1ccb65f2040a444141a8f976f", null ],
    [ "hDop", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#a21442123987933f4148e78c71a790bb1", null ],
    [ "nSatellites", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#a709b0d076dc4921bd8789af87e3e42e0", null ],
    [ "pDop", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#a01a19e993ede18ecab22c135ffa6d284", null ],
    [ "speed", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#aaa0430c88e22fab09b79dff5627e9791", null ],
    [ "userData", "classDigikam_1_1TrackCorrelator_1_1Correlation.html#a5d424d20d3bcda25e8a19321884445fc", null ]
];