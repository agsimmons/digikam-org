var dir_7da7f306837e41c8e24a118b9e028c51 =
[
    [ "glviewerglobal.h", "glviewerglobal_8h.html", "glviewerglobal_8h" ],
    [ "glviewerplugin.cpp", "glviewerplugin_8cpp.html", null ],
    [ "glviewerplugin.h", "glviewerplugin_8h.html", "glviewerplugin_8h" ],
    [ "glviewertexture.cpp", "glviewertexture_8cpp.html", null ],
    [ "glviewertexture.h", "glviewertexture_8h.html", [
      [ "GLViewerTexture", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture" ]
    ] ],
    [ "glviewertimer.cpp", "glviewertimer_8cpp.html", null ],
    [ "glviewertimer.h", "glviewertimer_8h.html", [
      [ "GLViewerTimer", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer" ]
    ] ],
    [ "glviewerwidget.cpp", "glviewerwidget_8cpp.html", "glviewerwidget_8cpp" ],
    [ "glviewerwidget.h", "glviewerwidget_8h.html", [
      [ "GLViewerWidget", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget" ]
    ] ]
];