var dir_a3a606e7ed0e4c82cd14e07bdf5a211e =
[
    [ "dnnfaceextractor.cpp", "dnnfaceextractor_8cpp.html", null ],
    [ "dnnfaceextractor.h", "dnnfaceextractor_8h.html", [
      [ "DNNFaceExtractor", "classDigikam_1_1DNNFaceExtractor.html", "classDigikam_1_1DNNFaceExtractor" ]
    ] ],
    [ "kd_node.cpp", "kd__node_8cpp.html", null ],
    [ "kd_node.h", "kd__node_8h.html", [
      [ "KDNode", "classDigikam_1_1KDNode.html", "classDigikam_1_1KDNode" ]
    ] ],
    [ "kd_tree.cpp", "kd__tree_8cpp.html", null ],
    [ "kd_tree.h", "kd__tree_8h.html", [
      [ "KDTree", "classDigikam_1_1KDTree.html", "classDigikam_1_1KDTree" ]
    ] ],
    [ "opencvdnnfacerecognizer.cpp", "opencvdnnfacerecognizer_8cpp.html", null ],
    [ "opencvdnnfacerecognizer.h", "opencvdnnfacerecognizer_8h.html", [
      [ "OpenCVDNNFaceRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer.html", "classDigikam_1_1OpenCVDNNFaceRecognizer" ]
    ] ],
    [ "opencvdnnfacerecognizer_p.h", "opencvdnnfacerecognizer__p_8h.html", [
      [ "Private", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private.html", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private" ],
      [ "ParallelRecognizer", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelRecognizer.html", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelRecognizer" ],
      [ "ParallelTrainer", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelTrainer.html", "classDigikam_1_1OpenCVDNNFaceRecognizer_1_1Private_1_1ParallelTrainer" ]
    ] ]
];