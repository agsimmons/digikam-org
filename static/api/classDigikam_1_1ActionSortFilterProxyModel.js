var classDigikam_1_1ActionSortFilterProxyModel =
[
    [ "AdditionalRoles", "classDigikam_1_1ActionSortFilterProxyModel.html#a625802b662e24b0b3fcb5f3f8648ae85", [
      [ "CategoryDisplayRole", "classDigikam_1_1ActionSortFilterProxyModel.html#a625802b662e24b0b3fcb5f3f8648ae85a5f2ca32c1bbed81c8055538a76944c8b", null ],
      [ "CategorySortRole", "classDigikam_1_1ActionSortFilterProxyModel.html#a625802b662e24b0b3fcb5f3f8648ae85aaa9634930683596440a38e527bee5137", null ]
    ] ],
    [ "ActionSortFilterProxyModel", "classDigikam_1_1ActionSortFilterProxyModel.html#af44a8737a85419008c2f72f88792088b", null ],
    [ "compareCategories", "classDigikam_1_1ActionSortFilterProxyModel.html#abdad9daadc08e533726cdfcf9612d1f9", null ],
    [ "filterAcceptsRow", "classDigikam_1_1ActionSortFilterProxyModel.html#a2673291c42c96e16c7c9e4a258ade27e", null ],
    [ "isCategorizedModel", "classDigikam_1_1ActionSortFilterProxyModel.html#a4775410a9b1d51e931a393b2c2145e97", null ],
    [ "lessThan", "classDigikam_1_1ActionSortFilterProxyModel.html#ac9863fd832d5d881acfc8d429cf28cfc", null ],
    [ "setCategorizedModel", "classDigikam_1_1ActionSortFilterProxyModel.html#af6b6be9ecaa51b50ab2021bfa185caf8", null ],
    [ "setSortCategoriesByNaturalComparison", "classDigikam_1_1ActionSortFilterProxyModel.html#a7addd0379d074d863835c67b4f117b01", null ],
    [ "sort", "classDigikam_1_1ActionSortFilterProxyModel.html#a7bd5604d1126dad3f7225268026de21c", null ],
    [ "sortCategoriesByNaturalComparison", "classDigikam_1_1ActionSortFilterProxyModel.html#a64fdd7d929a5765d39458da7bdc4f85f", null ],
    [ "sortColumn", "classDigikam_1_1ActionSortFilterProxyModel.html#a423464a75d113e6fc8ce3a78a55cbcec", null ],
    [ "sortOrder", "classDigikam_1_1ActionSortFilterProxyModel.html#a627f37012bcd03451189ab45f51ef201", null ],
    [ "subSortLessThan", "classDigikam_1_1ActionSortFilterProxyModel.html#a86b24c8f86d99f32463bb71a4f2c8d73", null ]
];