var classDigikam_1_1AdvancedRenameManager =
[
    [ "ParserType", "classDigikam_1_1AdvancedRenameManager.html#ac3134ba1eb666ec8dbd1ea07a007a353", [
      [ "DefaultParser", "classDigikam_1_1AdvancedRenameManager.html#ac3134ba1eb666ec8dbd1ea07a007a353a02b811c7d9b1dcce3d3f8da8b59f9a88", null ],
      [ "ImportParser", "classDigikam_1_1AdvancedRenameManager.html#ac3134ba1eb666ec8dbd1ea07a007a353abfcae0ae7cc30b7904f277789beb05f4", null ]
    ] ],
    [ "SortAction", "classDigikam_1_1AdvancedRenameManager.html#a7219e7beb46ed171ddcfdb610d769311", [
      [ "SortName", "classDigikam_1_1AdvancedRenameManager.html#a7219e7beb46ed171ddcfdb610d769311a9a3eba4c8efec5c77bd34215302d397c", null ],
      [ "SortDate", "classDigikam_1_1AdvancedRenameManager.html#a7219e7beb46ed171ddcfdb610d769311a3928013ef6d1c51e2fa19f312046c2d7", null ],
      [ "SortSize", "classDigikam_1_1AdvancedRenameManager.html#a7219e7beb46ed171ddcfdb610d769311aee18262856247f043b298a13e53a1510", null ],
      [ "SortCustom", "classDigikam_1_1AdvancedRenameManager.html#a7219e7beb46ed171ddcfdb610d769311a54fb95129f2fe4315f8fe78fcf35f3d1", null ]
    ] ],
    [ "SortDirection", "classDigikam_1_1AdvancedRenameManager.html#a80aef9f85adcf4c68a065065a15c083d", [
      [ "SortAscending", "classDigikam_1_1AdvancedRenameManager.html#a80aef9f85adcf4c68a065065a15c083da957d5c8f46e32bbf3dee5880d389bf00", null ],
      [ "SortDescending", "classDigikam_1_1AdvancedRenameManager.html#a80aef9f85adcf4c68a065065a15c083daaa4917358b2ae6dfec9d15c880770e86", null ]
    ] ],
    [ "AdvancedRenameManager", "classDigikam_1_1AdvancedRenameManager.html#a71de24fbd93b4f8873cb8c08d8a5aabb", null ],
    [ "AdvancedRenameManager", "classDigikam_1_1AdvancedRenameManager.html#ae33f9e9e53836e2480a48e31feb8788c", null ],
    [ "~AdvancedRenameManager", "classDigikam_1_1AdvancedRenameManager.html#aaabc2c173724b878cdc26404e09d264b", null ],
    [ "addFiles", "classDigikam_1_1AdvancedRenameManager.html#a6a6f65cb33880b507ecea38025f4a801", null ],
    [ "fileList", "classDigikam_1_1AdvancedRenameManager.html#a4f0f3ab6beb6449873dd539df4fe366b", null ],
    [ "getParser", "classDigikam_1_1AdvancedRenameManager.html#adca0545211b85df43142add41c3c94e2", null ],
    [ "indexOfFile", "classDigikam_1_1AdvancedRenameManager.html#ab200a31639f89bfdda45f43927ca73ce", null ],
    [ "indexOfFileGroup", "classDigikam_1_1AdvancedRenameManager.html#a003e9cc94396d4352fac95fc17371c7b", null ],
    [ "indexOfFolder", "classDigikam_1_1AdvancedRenameManager.html#aae8f40254e846580e4a3519fd691d413", null ],
    [ "newFileList", "classDigikam_1_1AdvancedRenameManager.html#a948aa696d03abc0a1c70ede117afea61", null ],
    [ "newName", "classDigikam_1_1AdvancedRenameManager.html#a9554df0c00ae00b63a7fc57970021e47", null ],
    [ "parseFiles", "classDigikam_1_1AdvancedRenameManager.html#a2fe4a9c8a35d6995f512cf202578e36e", null ],
    [ "parseFiles", "classDigikam_1_1AdvancedRenameManager.html#a8106e13088298e651b7f60cfcc546d23", null ],
    [ "parseFiles", "classDigikam_1_1AdvancedRenameManager.html#a75c18c7df071e0f8a871c14bf02e0707", null ],
    [ "parseFiles", "classDigikam_1_1AdvancedRenameManager.html#a059a67672b39b55129c5dadee2d354ce", null ],
    [ "reset", "classDigikam_1_1AdvancedRenameManager.html#a1edd08c0ebe44bc88a79880c76dd1a38", null ],
    [ "setParserType", "classDigikam_1_1AdvancedRenameManager.html#a48058a30c8e6d0a184c3ec2ed1a161c5", null ],
    [ "setSortAction", "classDigikam_1_1AdvancedRenameManager.html#a4f6aadaff066170295b80d6e491407ba", null ],
    [ "setSortDirection", "classDigikam_1_1AdvancedRenameManager.html#a49e454df6a74ce7a9cb1b9fb35c9aaf2", null ],
    [ "setStartIndex", "classDigikam_1_1AdvancedRenameManager.html#a41d8ee445df560d831663a2dc51bb2bb", null ],
    [ "setWidget", "classDigikam_1_1AdvancedRenameManager.html#aeedb101ff5acea139f4544fb73d20d25", null ],
    [ "signalSortingChanged", "classDigikam_1_1AdvancedRenameManager.html#ae6b842b1778178f9a256d1e3e581e58c", null ],
    [ "sortAction", "classDigikam_1_1AdvancedRenameManager.html#a5d291705664ad75b606d6ecf772f30ce", null ],
    [ "sortDirection", "classDigikam_1_1AdvancedRenameManager.html#ab9bfaa5876d6d6b20905f57a1e7e8b50", null ]
];