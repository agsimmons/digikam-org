var classDigikam_1_1VideoFrame =
[
    [ "VideoFrame", "classDigikam_1_1VideoFrame.html#a9abd56f1fed5c65c30b5e2d44cf07345", null ],
    [ "VideoFrame", "classDigikam_1_1VideoFrame.html#add2379c906b454a6c6d2987e1408ba33", null ],
    [ "~VideoFrame", "classDigikam_1_1VideoFrame.html#a952af069e951614dd1ebb2b4100920d9", null ],
    [ "frameData", "classDigikam_1_1VideoFrame.html#a9d529f6cfc378061d6a36b76988af76b", null ],
    [ "height", "classDigikam_1_1VideoFrame.html#a9e543f87d68fd8f5275941a149d456ee", null ],
    [ "lineSize", "classDigikam_1_1VideoFrame.html#a3c27e4d9b7ecb1cf8b550e013362c816", null ],
    [ "width", "classDigikam_1_1VideoFrame.html#a12812b8794d071925b6ab8c07519644c", null ]
];