var itemhistorygraphdata_8h =
[
    [ "HistoryEdgeProperties", "classDigikam_1_1HistoryEdgeProperties.html", "classDigikam_1_1HistoryEdgeProperties" ],
    [ "HistoryVertexProperties", "classDigikam_1_1HistoryVertexProperties.html", "classDigikam_1_1HistoryVertexProperties" ],
    [ "ItemHistoryGraphData", "classDigikam_1_1ItemHistoryGraphData.html", "classDigikam_1_1ItemHistoryGraphData" ],
    [ "HistoryGraph", "itemhistorygraphdata_8h.html#aa5a9dbfb63835fff43d2fe757e0915fa", null ],
    [ "operator<<", "itemhistorygraphdata_8h.html#ae2c58654c3650b6289b1b3cdfe934e73", null ],
    [ "operator<<", "itemhistorygraphdata_8h.html#ac3c9badc7fd3c9c44ead4941f6bd3942", null ]
];