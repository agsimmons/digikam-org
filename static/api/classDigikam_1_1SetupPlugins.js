var classDigikam_1_1SetupPlugins =
[
    [ "PluginTab", "classDigikam_1_1SetupPlugins.html#a193d52e2726be869fe8a2cf233a1ca3e", [
      [ "Generic", "classDigikam_1_1SetupPlugins.html#a193d52e2726be869fe8a2cf233a1ca3eaae19153dfd87adabb784759516dc0c6e", null ],
      [ "Editor", "classDigikam_1_1SetupPlugins.html#a193d52e2726be869fe8a2cf233a1ca3ea278cf9b0a3562d82164fe849aa9e0df0", null ],
      [ "Bqm", "classDigikam_1_1SetupPlugins.html#a193d52e2726be869fe8a2cf233a1ca3eaa9ae8ba24b7c1046a77dc25f28cc28ba", null ],
      [ "Loaders", "classDigikam_1_1SetupPlugins.html#a193d52e2726be869fe8a2cf233a1ca3ea8c81c3a8a2e79252083d998c7897975d", null ]
    ] ],
    [ "SetupPlugins", "classDigikam_1_1SetupPlugins.html#afb98cb1155f4b457ca67f24c5feab120", null ],
    [ "~SetupPlugins", "classDigikam_1_1SetupPlugins.html#af9519b4bfb4ec3d4eba7b8296604cf79", null ],
    [ "applySettings", "classDigikam_1_1SetupPlugins.html#a1f5eeaa6fc23df1220777283028abeec", null ]
];