var classDigikam_1_1WSSettings =
[
    [ "ImageFormat", "classDigikam_1_1WSSettings.html#a5b2d244b4c7b9b177f6d555eeb135ef4", [
      [ "JPEG", "classDigikam_1_1WSSettings.html#a5b2d244b4c7b9b177f6d555eeb135ef4a9b51c2b68fcc08ce29d33d56d8a8825f", null ],
      [ "PNG", "classDigikam_1_1WSSettings.html#a5b2d244b4c7b9b177f6d555eeb135ef4aadb23f3559398aaac037b9431758a3a3", null ]
    ] ],
    [ "Selection", "classDigikam_1_1WSSettings.html#a9b3c071f8a9bd1d998ab9b8ab4f7c7a2", [
      [ "EXPORT", "classDigikam_1_1WSSettings.html#a9b3c071f8a9bd1d998ab9b8ab4f7c7a2a249c296dbef0900a1b4c7ef4443eb6c6", null ],
      [ "IMPORT", "classDigikam_1_1WSSettings.html#a9b3c071f8a9bd1d998ab9b8ab4f7c7a2a7cbf109d6fd636592373a44f11c594fc", null ]
    ] ],
    [ "WebService", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71", [
      [ "FLICKR", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71a37e30d1ead3c3a99561d7739df567a57", null ],
      [ "DROPBOX", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71a3f951ef27c7b68a65477a6796a2414d3", null ],
      [ "IMGUR", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71a89ac685157f59abcaf8fc45433cf5843", null ],
      [ "FACEBOOK", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71a45dcb1e9e9b3e73d6e9f9d57dacdddfb", null ],
      [ "SMUGMUG", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71a0cefa1dffb80d16682ea54654cd38253", null ],
      [ "GDRIVE", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71acec862d448d200ca71df7e2f4f91b7c9", null ],
      [ "GPHOTO", "classDigikam_1_1WSSettings.html#a44416fc1122442a95738571c32cd8a71a0bbbc2d3c137bade763331d7d0bc5435", null ]
    ] ],
    [ "WSSettings", "classDigikam_1_1WSSettings.html#add7250c865d59f814c81e5029859eb4c", null ],
    [ "~WSSettings", "classDigikam_1_1WSSettings.html#a586de86bb470a18d89f0656d2a856396", null ],
    [ "allUserNames", "classDigikam_1_1WSSettings.html#a982cf102178c9fe9aeb19ff883aab3c5", null ],
    [ "format", "classDigikam_1_1WSSettings.html#aa1fa5722b166583dd440148d560453a8", null ],
    [ "readSettings", "classDigikam_1_1WSSettings.html#a10c63ca50edc0336cf91eed66fdfefbd", null ],
    [ "writeSettings", "classDigikam_1_1WSSettings.html#ac1a3917ebdf68f0e913fade7b2c4fd09", null ],
    [ "addFileProperties", "classDigikam_1_1WSSettings.html#af9cfece13ba36b3d8cc9be52f9582051", null ],
    [ "attLimitInMbytes", "classDigikam_1_1WSSettings.html#a62134148ce13a24565d49cc6b5a2fae3", null ],
    [ "currentAlbumId", "classDigikam_1_1WSSettings.html#a327c5542827d805497ee8fc8cad22465", null ],
    [ "imageCompression", "classDigikam_1_1WSSettings.html#a29cd70ef0f801a725f00401e83e3b13f", null ],
    [ "imageFormat", "classDigikam_1_1WSSettings.html#a0d31b8092fad227da880602799a0d601", null ],
    [ "imagesChangeProp", "classDigikam_1_1WSSettings.html#a18c85611923b761902dbd9eb83b07a81", null ],
    [ "imageSize", "classDigikam_1_1WSSettings.html#a8abef97dd40029119469c29bd8830202", null ],
    [ "inputImages", "classDigikam_1_1WSSettings.html#a5da4edb8d3056cf1ddf8cb3208e28e23", null ],
    [ "itemsList", "classDigikam_1_1WSSettings.html#a1d7a34838a5c244476a3eff55bff7c4d", null ],
    [ "oauthSettings", "classDigikam_1_1WSSettings.html#a06f0cd42d95a409f257c515960cf1956", null ],
    [ "oauthSettingsStore", "classDigikam_1_1WSSettings.html#a44f0e1eeb26e59efaa0483d2543af095", null ],
    [ "removeMetadata", "classDigikam_1_1WSSettings.html#acdfae37a6cf54e43eb5285611f38d2ce", null ],
    [ "selMode", "classDigikam_1_1WSSettings.html#ab1b9fa44af21b634b165d8129b9cd2c1", null ],
    [ "userName", "classDigikam_1_1WSSettings.html#ad0202d0052844d76745c3ec10efabbeb", null ],
    [ "webService", "classDigikam_1_1WSSettings.html#aa186ee90adff5173e6aad8ddc4ccaef2", null ]
];