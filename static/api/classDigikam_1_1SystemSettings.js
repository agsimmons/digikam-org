var classDigikam_1_1SystemSettings =
[
    [ "SystemSettings", "classDigikam_1_1SystemSettings.html#a1c1eae07478db088d724f0af733bf348", null ],
    [ "~SystemSettings", "classDigikam_1_1SystemSettings.html#a3ca3115e6d5c56ba5e83ecf6cd9e478c", null ],
    [ "readSettings", "classDigikam_1_1SystemSettings.html#a1776464d4c7f6f850fa268a8da698a08", null ],
    [ "saveSettings", "classDigikam_1_1SystemSettings.html#a22c5dfbad0b9b0caae3f84647b9e5295", null ],
    [ "disableOpenCL", "classDigikam_1_1SystemSettings.html#ac7420e531a2bfeaf737c8f366f29c274", null ],
    [ "enableLogging", "classDigikam_1_1SystemSettings.html#a7bb506f73e1b82034a5a561552158615", null ],
    [ "useHighDpiPixmaps", "classDigikam_1_1SystemSettings.html#a706f18e08a58e985083b9078c5cef35e", null ],
    [ "useHighDpiScaling", "classDigikam_1_1SystemSettings.html#a3b0d2737c399d76228c7ea36d8a38ca2", null ]
];