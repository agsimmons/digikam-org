var namespaceDigikamGenericGLViewerPlugin =
[
    [ "GLViewerPlugin", "classDigikamGenericGLViewerPlugin_1_1GLViewerPlugin.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerPlugin" ],
    [ "GLViewerTexture", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerTexture" ],
    [ "GLViewerTimer", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerTimer" ],
    [ "GLViewerWidget", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget.html", "classDigikamGenericGLViewerPlugin_1_1GLViewerWidget" ],
    [ "OGLstate", "namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026e", [
      [ "oglOK", "namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026eadd18aedcb5dbd0387f7ad536b9e88387", null ],
      [ "oglNoRectangularTexture", "namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026eaa7614f8f5a987cced8a01da729a55696", null ],
      [ "oglNoContext", "namespaceDigikamGenericGLViewerPlugin.html#a8abbaa64ef5f2b664f34f54b7632026ea41886f62e0bae782b4af8ffa98f73899", null ]
    ] ]
];