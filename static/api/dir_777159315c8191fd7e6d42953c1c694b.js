var dir_777159315c8191fd7e6d42953c1c694b =
[
    [ "digikamapp.cpp", "digikamapp_8cpp.html", null ],
    [ "digikamapp.h", "digikamapp_8h.html", [
      [ "DigikamApp", "classDigikam_1_1DigikamApp.html", "classDigikam_1_1DigikamApp" ]
    ] ],
    [ "digikamapp_camera.cpp", "digikamapp__camera_8cpp.html", null ],
    [ "digikamapp_config.cpp", "digikamapp__config_8cpp.html", null ],
    [ "digikamapp_import.cpp", "digikamapp__import_8cpp.html", null ],
    [ "digikamapp_p.h", "digikamapp__p_8h.html", [
      [ "Private", "classDigikam_1_1DigikamApp_1_1Private.html", "classDigikam_1_1DigikamApp_1_1Private" ],
      [ "ProgressEntry", "classDigikam_1_1ProgressEntry.html", "classDigikam_1_1ProgressEntry" ]
    ] ],
    [ "digikamapp_setup.cpp", "digikamapp__setup_8cpp.html", null ],
    [ "digikamapp_solid.cpp", "digikamapp__solid_8cpp.html", "digikamapp__solid_8cpp" ],
    [ "digikamapp_tools.cpp", "digikamapp__tools_8cpp.html", null ],
    [ "main.cpp", "app_2main_2main_8cpp.html", "app_2main_2main_8cpp" ]
];