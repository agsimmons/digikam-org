var classDigikam_1_1OpenCVDNNFaceDetector =
[
    [ "OpenCVDNNFaceDetector", "classDigikam_1_1OpenCVDNNFaceDetector.html#af9ac3b9233391eca73e97ce0f9f5aae0", null ],
    [ "~OpenCVDNNFaceDetector", "classDigikam_1_1OpenCVDNNFaceDetector.html#a35373a183c31bd1c47464d0e56cc56fd", null ],
    [ "cvDetectFaces", "classDigikam_1_1OpenCVDNNFaceDetector.html#ab63779b5c84df47524a81d201839bab6", null ],
    [ "detectFaces", "classDigikam_1_1OpenCVDNNFaceDetector.html#a74cf8604e38c5f1b32941f75c48745e4", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVDNNFaceDetector.html#ab479f56ccde55333105bad6d9eb51034", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVDNNFaceDetector.html#a2614bedd65abe9565fdf370817540eeb", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVDNNFaceDetector.html#a6e98bfed59a8a484b0e52f315033b98b", null ]
];