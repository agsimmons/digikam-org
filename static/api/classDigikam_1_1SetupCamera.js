var classDigikam_1_1SetupCamera =
[
    [ "ConflictRule", "classDigikam_1_1SetupCamera.html#ad5bb0fc07e1af7d0bdf25ca9e1105187", [
      [ "OVERWRITE", "classDigikam_1_1SetupCamera.html#ad5bb0fc07e1af7d0bdf25ca9e1105187af809ab82b7e479e555c93b411a9aaa60", null ],
      [ "DIFFNAME", "classDigikam_1_1SetupCamera.html#ad5bb0fc07e1af7d0bdf25ca9e1105187a1f522764f50dc6c343fe8146789ec870", null ],
      [ "SKIPFILE", "classDigikam_1_1SetupCamera.html#ad5bb0fc07e1af7d0bdf25ca9e1105187ab3633ea1cee4fef592631379f761be0c", null ]
    ] ],
    [ "SetupCamera", "classDigikam_1_1SetupCamera.html#a8c3e4878fbec8a6b8d2681214d9d07e0", null ],
    [ "~SetupCamera", "classDigikam_1_1SetupCamera.html#a046ef6ebacb210f37b948c1c44761e10", null ],
    [ "applySettings", "classDigikam_1_1SetupCamera.html#a39d28fc4ef2cb439bcc9a87ce18a3e12", null ],
    [ "checkSettings", "classDigikam_1_1SetupCamera.html#a7730926a165f35bb534d3ec979577436", null ],
    [ "signalUseFileMetadataChanged", "classDigikam_1_1SetupCamera.html#a2d4656efba47c2073aba3d634e0fad32", null ],
    [ "useFileMetadata", "classDigikam_1_1SetupCamera.html#a5333fa5ed459d7b0e32a34589cce24de", null ]
];