var dir_9e020398151a835d5c6e7ef52e433be1 =
[
    [ "captions", "dir_9188495e614aab9437d55f6d8601f54d.html", "dir_9188495e614aab9437d55f6d8601f54d" ],
    [ "geolocation", "dir_1a602d51da70c05d368326fa1ac7a226.html", "dir_1a602d51da70c05d368326fa1ac7a226" ],
    [ "history", "dir_b8f354e0726f73ff981393aa01fde002.html", "dir_b8f354e0726f73ff981393aa01fde002" ],
    [ "import", "dir_d92ed27a10299f9bcd37288fd766fbfb.html", "dir_d92ed27a10299f9bcd37288fd766fbfb" ],
    [ "itempropertiescolorstab.cpp", "itempropertiescolorstab_8cpp.html", null ],
    [ "itempropertiescolorstab.h", "itempropertiescolorstab_8h.html", [
      [ "ItemPropertiesColorsTab", "classDigikam_1_1ItemPropertiesColorsTab.html", "classDigikam_1_1ItemPropertiesColorsTab" ]
    ] ],
    [ "itempropertiesmetadatatab.cpp", "itempropertiesmetadatatab_8cpp.html", null ],
    [ "itempropertiesmetadatatab.h", "itempropertiesmetadatatab_8h.html", [
      [ "ItemPropertiesMetadataTab", "classDigikam_1_1ItemPropertiesMetadataTab.html", "classDigikam_1_1ItemPropertiesMetadataTab" ]
    ] ],
    [ "itempropertiessidebar.cpp", "itempropertiessidebar_8cpp.html", null ],
    [ "itempropertiessidebar.h", "itempropertiessidebar_8h.html", [
      [ "ItemPropertiesSideBar", "classDigikam_1_1ItemPropertiesSideBar.html", "classDigikam_1_1ItemPropertiesSideBar" ]
    ] ],
    [ "itempropertiessidebardb.cpp", "itempropertiessidebardb_8cpp.html", null ],
    [ "itempropertiessidebardb.h", "itempropertiessidebardb_8h.html", [
      [ "ItemPropertiesSideBarDB", "classDigikam_1_1ItemPropertiesSideBarDB.html", "classDigikam_1_1ItemPropertiesSideBarDB" ]
    ] ],
    [ "itempropertiestab.cpp", "itempropertiestab_8cpp.html", "itempropertiestab_8cpp" ],
    [ "itempropertiestab.h", "itempropertiestab_8h.html", [
      [ "ItemPropertiesTab", "classDigikam_1_1ItemPropertiesTab.html", "classDigikam_1_1ItemPropertiesTab" ]
    ] ],
    [ "itempropertiestxtlabel.cpp", "itempropertiestxtlabel_8cpp.html", null ],
    [ "itempropertiestxtlabel.h", "itempropertiestxtlabel_8h.html", [
      [ "DTextBrowser", "classDigikam_1_1DTextBrowser.html", "classDigikam_1_1DTextBrowser" ],
      [ "DTextLabelName", "classDigikam_1_1DTextLabelName.html", "classDigikam_1_1DTextLabelName" ],
      [ "DTextLabelValue", "classDigikam_1_1DTextLabelValue.html", "classDigikam_1_1DTextLabelValue" ],
      [ "DTextList", "classDigikam_1_1DTextList.html", "classDigikam_1_1DTextList" ]
    ] ],
    [ "itemselectionpropertiestab.cpp", "itemselectionpropertiestab_8cpp.html", null ],
    [ "itemselectionpropertiestab.h", "itemselectionpropertiestab_8h.html", [
      [ "ItemSelectionPropertiesTab", "classDigikam_1_1ItemSelectionPropertiesTab.html", "classDigikam_1_1ItemSelectionPropertiesTab" ]
    ] ]
];