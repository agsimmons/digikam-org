var classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage =
[
    [ "MailAlbumsPage", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a1441754484f8d3089db60435ad66e123", null ],
    [ "~MailAlbumsPage", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#ab74293a0e77cabf7c45eda463c8c68ce", null ],
    [ "assistant", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "id", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "isComplete", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#aac2f6761c3c111708bc9a46c4f3dc2ba", null ],
    [ "removePageWidget", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "setComplete", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a67975edf6041a574e674576a29d606a1", null ],
    [ "validatePage", "classDigikamGenericSendByMailPlugin_1_1MailAlbumsPage.html#a4378df4dbf9d6b941f379814de2a409b", null ]
];