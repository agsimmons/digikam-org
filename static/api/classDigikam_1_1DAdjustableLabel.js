var classDigikam_1_1DAdjustableLabel =
[
    [ "DAdjustableLabel", "classDigikam_1_1DAdjustableLabel.html#a47dd2ed6c2df7a3fb0cc65e680f4f352", null ],
    [ "~DAdjustableLabel", "classDigikam_1_1DAdjustableLabel.html#af250148452c1d2ba7d920f874915889e", null ],
    [ "adjustedText", "classDigikam_1_1DAdjustableLabel.html#a808d91a0b119dc0ea7963c5d9008a22e", null ],
    [ "minimumSizeHint", "classDigikam_1_1DAdjustableLabel.html#ad7094264b03ebf2b1d304b878dcc6fe8", null ],
    [ "setAdjustedText", "classDigikam_1_1DAdjustableLabel.html#ad868928c7eeee8e41b2c2afd37274ec2", null ],
    [ "setAlignment", "classDigikam_1_1DAdjustableLabel.html#a39554a9d00b3390c4348e9094dcf5eca", null ],
    [ "setElideMode", "classDigikam_1_1DAdjustableLabel.html#affa7663c26f07e8bb30abe48180a2c77", null ],
    [ "sizeHint", "classDigikam_1_1DAdjustableLabel.html#a293e6d780f0d118b702ca1fe76820ceb", null ]
];