var dir_7becaf6fa124484042868c9cbefc317a =
[
    [ "databaseworkeriface.cpp", "databaseworkeriface_8cpp.html", null ],
    [ "databaseworkeriface.h", "databaseworkeriface_8h.html", [
      [ "DatabaseWorkerInterface", "classDigikam_1_1DatabaseWorkerInterface.html", "classDigikam_1_1DatabaseWorkerInterface" ],
      [ "FileActionMngrDatabaseWorker", "classDigikam_1_1FileActionMngrDatabaseWorker.html", "classDigikam_1_1FileActionMngrDatabaseWorker" ]
    ] ],
    [ "fileactionimageinfolist.cpp", "fileactionimageinfolist_8cpp.html", null ],
    [ "fileactionimageinfolist.h", "fileactionimageinfolist_8h.html", [
      [ "FileActionItemInfoList", "classDigikam_1_1FileActionItemInfoList.html", "classDigikam_1_1FileActionItemInfoList" ],
      [ "FileActionProgressItemContainer", "classDigikam_1_1FileActionProgressItemContainer.html", "classDigikam_1_1FileActionProgressItemContainer" ],
      [ "FileActionProgressItemCreator", "classDigikam_1_1FileActionProgressItemCreator.html", "classDigikam_1_1FileActionProgressItemCreator" ],
      [ "TwoProgressItemsContainer", "classDigikam_1_1TwoProgressItemsContainer.html", "classDigikam_1_1TwoProgressItemsContainer" ]
    ] ],
    [ "fileactionmngr.cpp", "fileactionmngr_8cpp.html", null ],
    [ "fileactionmngr.h", "fileactionmngr_8h.html", [
      [ "FileActionMngr", "classDigikam_1_1FileActionMngr.html", "classDigikam_1_1FileActionMngr" ]
    ] ],
    [ "fileactionmngr_p.cpp", "fileactionmngr__p_8cpp.html", null ],
    [ "fileactionmngr_p.h", "fileactionmngr__p_8h.html", "fileactionmngr__p_8h" ],
    [ "fileactionprogress.cpp", "fileactionprogress_8cpp.html", null ],
    [ "fileactionprogress.h", "fileactionprogress_8h.html", [
      [ "FileActionProgress", "classDigikam_1_1FileActionProgress.html", "classDigikam_1_1FileActionProgress" ]
    ] ],
    [ "fileworkeriface.cpp", "fileworkeriface_8cpp.html", null ],
    [ "fileworkeriface.h", "fileworkeriface_8h.html", [
      [ "FileActionMngrFileWorker", "classDigikam_1_1FileActionMngrFileWorker.html", "classDigikam_1_1FileActionMngrFileWorker" ],
      [ "FileWorkerInterface", "classDigikam_1_1FileWorkerInterface.html", "classDigikam_1_1FileWorkerInterface" ]
    ] ],
    [ "iteminfotasksplitter.cpp", "iteminfotasksplitter_8cpp.html", null ],
    [ "iteminfotasksplitter.h", "iteminfotasksplitter_8h.html", [
      [ "ItemInfoTaskSplitter", "classDigikam_1_1ItemInfoTaskSplitter.html", "classDigikam_1_1ItemInfoTaskSplitter" ]
    ] ],
    [ "metadatahub.cpp", "metadatahub_8cpp.html", null ],
    [ "metadatahub.h", "metadatahub_8h.html", [
      [ "MetadataHub", "classDigikam_1_1MetadataHub.html", "classDigikam_1_1MetadataHub" ]
    ] ],
    [ "metadatahubmngr.cpp", "metadatahubmngr_8cpp.html", null ],
    [ "metadatahubmngr.h", "metadatahubmngr_8h.html", [
      [ "MetadataHubMngr", "classDigikam_1_1MetadataHubMngr.html", "classDigikam_1_1MetadataHubMngr" ]
    ] ],
    [ "metadatastatusbar.cpp", "metadatastatusbar_8cpp.html", null ],
    [ "metadatastatusbar.h", "metadatastatusbar_8h.html", [
      [ "MetadataStatusBar", "classDigikam_1_1MetadataStatusBar.html", "classDigikam_1_1MetadataStatusBar" ]
    ] ]
];