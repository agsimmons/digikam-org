var dir_80df5644454d183df325d0d336a458e6 =
[
    [ "balsabinary.cpp", "balsabinary_8cpp.html", null ],
    [ "balsabinary.h", "balsabinary_8h.html", [
      [ "BalsaBinary", "classDigikamGenericSendByMailPlugin_1_1BalsaBinary.html", "classDigikamGenericSendByMailPlugin_1_1BalsaBinary" ]
    ] ],
    [ "clawsmailbinary.cpp", "clawsmailbinary_8cpp.html", null ],
    [ "clawsmailbinary.h", "clawsmailbinary_8h.html", [
      [ "ClawsMailBinary", "classDigikamGenericSendByMailPlugin_1_1ClawsMailBinary.html", "classDigikamGenericSendByMailPlugin_1_1ClawsMailBinary" ]
    ] ],
    [ "evolutionbinary.cpp", "evolutionbinary_8cpp.html", null ],
    [ "evolutionbinary.h", "evolutionbinary_8h.html", [
      [ "EvolutionBinary", "classDigikamGenericSendByMailPlugin_1_1EvolutionBinary.html", "classDigikamGenericSendByMailPlugin_1_1EvolutionBinary" ]
    ] ],
    [ "kmailbinary.cpp", "kmailbinary_8cpp.html", null ],
    [ "kmailbinary.h", "kmailbinary_8h.html", [
      [ "KmailBinary", "classDigikamGenericSendByMailPlugin_1_1KmailBinary.html", "classDigikamGenericSendByMailPlugin_1_1KmailBinary" ]
    ] ],
    [ "netscapebinary.cpp", "netscapebinary_8cpp.html", null ],
    [ "netscapebinary.h", "netscapebinary_8h.html", [
      [ "NetscapeBinary", "classDigikamGenericSendByMailPlugin_1_1NetscapeBinary.html", "classDigikamGenericSendByMailPlugin_1_1NetscapeBinary" ]
    ] ],
    [ "outlookbinary.cpp", "outlookbinary_8cpp.html", null ],
    [ "outlookbinary.h", "outlookbinary_8h.html", [
      [ "OutlookBinary", "classDigikam_1_1OutlookBinary.html", "classDigikam_1_1OutlookBinary" ]
    ] ],
    [ "sylpheedbinary.cpp", "sylpheedbinary_8cpp.html", null ],
    [ "sylpheedbinary.h", "sylpheedbinary_8h.html", [
      [ "SylpheedBinary", "classDigikamGenericSendByMailPlugin_1_1SylpheedBinary.html", "classDigikamGenericSendByMailPlugin_1_1SylpheedBinary" ]
    ] ],
    [ "thunderbirdbinary.cpp", "thunderbirdbinary_8cpp.html", null ],
    [ "thunderbirdbinary.h", "thunderbirdbinary_8h.html", [
      [ "ThunderbirdBinary", "classDigikamGenericSendByMailPlugin_1_1ThunderbirdBinary.html", "classDigikamGenericSendByMailPlugin_1_1ThunderbirdBinary" ]
    ] ]
];