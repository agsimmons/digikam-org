var classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust =
[
    [ "EXIFAdjust", "classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust.html#a6b354322d785e0f6923a7f27e920e230", null ],
    [ "~EXIFAdjust", "classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust.html#aea3268b851c3b7cd2663c9e4803a3b56", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust.html#a55ffc86d85a5c54ba9ed9076ba13051d", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust.html#ad5b211b56e0d5a41033a8cedb86042d6", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1EXIFAdjust.html#ac7a07a1acae42abccadc46def818ca80", null ]
];