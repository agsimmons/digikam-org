var classDigikamGenericINatPlugin_1_1ComputerVisionScore =
[
    [ "ComputerVisionScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#ab3e37508ba375aa128dc7623c9eb2916", null ],
    [ "ComputerVisionScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#a34830ef6cfe466417f0c36adc6da8454", null ],
    [ "ComputerVisionScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#a88b032d67e35a873c2919b3789d4ab2f", null ],
    [ "~ComputerVisionScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#abc4337dc4c238443279fe126162e5396", null ],
    [ "getCombinedScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#acc5b7364bfa061f34516b9158e32107e", null ],
    [ "getFrequencyScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#a52a5b5a46d4f3560f70ba6ccb8e270bf", null ],
    [ "getTaxon", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#a6d1c423f2e3c93fc52140404b36a05c8", null ],
    [ "getVisionScore", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#af33a05620df628f8528716a2bd98b8ab", null ],
    [ "isValid", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#ab61147243aaff6af96bda3bf6af737b6", null ],
    [ "operator=", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#a07fbc93f164effc88ae471f492625f7c", null ],
    [ "seenNearby", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#acc7cc76c28ff0c41ea97a20ba9ea371d", null ],
    [ "visuallySimilar", "classDigikamGenericINatPlugin_1_1ComputerVisionScore.html#a376264f3d53f907234e0511f735e5ee3", null ]
];