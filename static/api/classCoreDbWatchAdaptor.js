var classCoreDbWatchAdaptor =
[
    [ "CoreDbWatchAdaptor", "classCoreDbWatchAdaptor.html#a73b45a97d84855442420431ce6c615a4", null ],
    [ "signalAlbumChangeDBus", "classCoreDbWatchAdaptor.html#a842b43e41fef3be3ecf7e8bf2545ac90", null ],
    [ "signalAlbumRootChangeDBus", "classCoreDbWatchAdaptor.html#aaf6ffe49ff9d3e27b0fe4163bd8c0b8c", null ],
    [ "signalCollectionImageChangeDBus", "classCoreDbWatchAdaptor.html#ad6f12580eda7e615c3ea60dcef0d11db", null ],
    [ "signalImageChangeDBus", "classCoreDbWatchAdaptor.html#a92d0918b2466674b66d874a0503a3d0a", null ],
    [ "signalImageTagChangeDBus", "classCoreDbWatchAdaptor.html#ab946ccc969bff31b42b0f04b32e164d1", null ],
    [ "signalSearchChangeDBus", "classCoreDbWatchAdaptor.html#a2dd85cef3d3ae744910ad6d69fc30966", null ],
    [ "signalTagChangeDBus", "classCoreDbWatchAdaptor.html#aa840d64a319df61948735649ae000b05", null ]
];