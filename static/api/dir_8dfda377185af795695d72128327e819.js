var dir_8dfda377185af795695d72128327e819 =
[
    [ "camitemsortsettings.cpp", "camitemsortsettings_8cpp.html", null ],
    [ "camitemsortsettings.h", "camitemsortsettings_8h.html", [
      [ "CamItemSortSettings", "classDigikam_1_1CamItemSortSettings.html", "classDigikam_1_1CamItemSortSettings" ]
    ] ],
    [ "importfiltermodel.cpp", "importfiltermodel_8cpp.html", null ],
    [ "importfiltermodel.h", "importfiltermodel_8h.html", [
      [ "ImportFilterModel", "classDigikam_1_1ImportFilterModel.html", "classDigikam_1_1ImportFilterModel" ],
      [ "ImportSortFilterModel", "classDigikam_1_1ImportSortFilterModel.html", "classDigikam_1_1ImportSortFilterModel" ],
      [ "NoDuplicatesImportFilterModel", "classDigikam_1_1NoDuplicatesImportFilterModel.html", "classDigikam_1_1NoDuplicatesImportFilterModel" ]
    ] ],
    [ "importimagemodel.cpp", "importimagemodel_8cpp.html", null ],
    [ "importimagemodel.h", "importimagemodel_8h.html", [
      [ "ImportItemModel", "classDigikam_1_1ImportItemModel.html", "classDigikam_1_1ImportItemModel" ]
    ] ],
    [ "importthumbnailmodel.cpp", "importthumbnailmodel_8cpp.html", null ],
    [ "importthumbnailmodel.h", "importthumbnailmodel_8h.html", [
      [ "ImportThumbnailModel", "classDigikam_1_1ImportThumbnailModel.html", "classDigikam_1_1ImportThumbnailModel" ]
    ] ]
];