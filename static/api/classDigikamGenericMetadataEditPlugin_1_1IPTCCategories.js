var classDigikamGenericMetadataEditPlugin_1_1IPTCCategories =
[
    [ "IPTCCategories", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html#a297eaec7bebbba7cc4684c6c007125b2", null ],
    [ "~IPTCCategories", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html#a0628ff4717883a8354a9b44132112f43", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html#a466033f02864ff15ec20fb57613a9602", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html#a75a12afa4739cedf3febb665a2585c27", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1IPTCCategories.html#a3674b205476727af6b09f11e41d8dbcc", null ]
];