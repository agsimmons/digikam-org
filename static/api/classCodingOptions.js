var classCodingOptions =
[
    [ "Option", "classCodingOptions.html#a563b35cc908e2c2e2ad74406dae8168f", null ],
    [ "RateEstimationMethod", "classCodingOptions.html#a970d806aa1c0dbf43a9f6effafb5b2c9", [
      [ "Rate_Default", "classCodingOptions.html#a970d806aa1c0dbf43a9f6effafb5b2c9a4c72e73cd6f4f6ce7bb379646f48acfc", null ],
      [ "Rate_AdaptiveContext", "classCodingOptions.html#a970d806aa1c0dbf43a9f6effafb5b2c9a8e6f876a2541a7d4c264170899e3dd08", null ],
      [ "Rate_FixedContext", "classCodingOptions.html#a970d806aa1c0dbf43a9f6effafb5b2c9a27eab71286238027a4190b499f8a171e", null ]
    ] ],
    [ "CodingOptions", "classCodingOptions.html#a0da4a230c0701761422b0ed2c7cc02d9", null ],
    [ "~CodingOptions", "classCodingOptions.html#a4f53646dff3e16d28761f0f5963fccc4", null ],
    [ "compute_rdo_costs", "classCodingOptions.html#abf8d85756f1de5393ca9f7b367c3df35", null ],
    [ "new_option", "classCodingOptions.html#a743fe360d396171c197549d398487f75", null ],
    [ "return_best_rdo_node", "classCodingOptions.html#ada92c559b05b14b271f299fef7813d30", null ],
    [ "start", "classCodingOptions.html#a96fe6ef44ed4b0462ab7ed4acaf7ee58", null ],
    [ "CodingOption< node >", "classCodingOptions.html#a577baba3d16ae4ff6de97321c7755400", null ]
];