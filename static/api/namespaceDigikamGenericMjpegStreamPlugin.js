var namespaceDigikamGenericMjpegStreamPlugin =
[
    [ "MjpegFrameOsd", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameOsd.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameOsd" ],
    [ "MjpegFrameTask", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameTask" ],
    [ "MjpegFrameThread", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegFrameThread" ],
    [ "MjpegServer", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServer" ],
    [ "MjpegServerMngr", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegServerMngr" ],
    [ "MjpegStreamDlg", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamDlg" ],
    [ "MjpegStreamPlugin", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamPlugin.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamPlugin" ],
    [ "MjpegStreamSettings", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamSettings.html", "classDigikamGenericMjpegStreamPlugin_1_1MjpegStreamSettings" ],
    [ "MjpegServerMap", "namespaceDigikamGenericMjpegStreamPlugin.html#ad10ca514383bc308fc710c01f66e40ea", null ]
];