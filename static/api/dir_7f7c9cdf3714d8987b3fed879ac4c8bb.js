var dir_7f7c9cdf3714d8987b3fed879ac4c8bb =
[
    [ "pitem.h", "pitem_8h.html", [
      [ "PFolder", "classDigikamGenericPinterestPlugin_1_1PFolder.html", "classDigikamGenericPinterestPlugin_1_1PFolder" ],
      [ "PPhoto", "classDigikamGenericPinterestPlugin_1_1PPhoto.html", "classDigikamGenericPinterestPlugin_1_1PPhoto" ]
    ] ],
    [ "pnewalbumdlg.cpp", "pnewalbumdlg_8cpp.html", null ],
    [ "pnewalbumdlg.h", "pnewalbumdlg_8h.html", [
      [ "PNewAlbumDlg", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg" ]
    ] ],
    [ "pplugin.cpp", "pplugin_8cpp.html", null ],
    [ "pplugin.h", "pplugin_8h.html", "pplugin_8h" ],
    [ "ptalker.cpp", "ptalker_8cpp.html", null ],
    [ "ptalker.h", "ptalker_8h.html", [
      [ "PTalker", "classDigikamGenericPinterestPlugin_1_1PTalker.html", "classDigikamGenericPinterestPlugin_1_1PTalker" ]
    ] ],
    [ "pwidget.cpp", "pwidget_8cpp.html", null ],
    [ "pwidget.h", "pwidget_8h.html", [
      [ "PWidget", "classDigikamGenericPinterestPlugin_1_1PWidget.html", "classDigikamGenericPinterestPlugin_1_1PWidget" ]
    ] ],
    [ "pwindow.cpp", "pwindow_8cpp.html", null ],
    [ "pwindow.h", "pwindow_8h.html", [
      [ "PWindow", "classDigikamGenericPinterestPlugin_1_1PWindow.html", "classDigikamGenericPinterestPlugin_1_1PWindow" ]
    ] ]
];