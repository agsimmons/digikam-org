var classDigikam_1_1Sidebar_1_1Private =
[
    [ "Private", "classDigikam_1_1Sidebar_1_1Private.html#a3241dcecc033d24bada3689b37795aae", null ],
    [ "activeTab", "classDigikam_1_1Sidebar_1_1Private.html#af8369603fc09cf34f286494793742bb9", null ],
    [ "appendedTabsStateCache", "classDigikam_1_1Sidebar_1_1Private.html#a7ad70c385131ce1e780da2cd442f3cb8", null ],
    [ "dragSwitchId", "classDigikam_1_1Sidebar_1_1Private.html#a14a2fc8a37862f78626e35e60b0fbb29", null ],
    [ "dragSwitchTimer", "classDigikam_1_1Sidebar_1_1Private.html#aab7b4ca79296ba28a9a18ee09f1ab44a", null ],
    [ "isMinimized", "classDigikam_1_1Sidebar_1_1Private.html#aec327058bd646cf5ef22da308f4c0cc4", null ],
    [ "minimized", "classDigikam_1_1Sidebar_1_1Private.html#ab8c5b31cd198564cdb044d414f5c04d6", null ],
    [ "minimizedDefault", "classDigikam_1_1Sidebar_1_1Private.html#a7b5d465ae3c4dd670befc676c353769f", null ],
    [ "optionActiveTabEntry", "classDigikam_1_1Sidebar_1_1Private.html#ab90a3b41c4d174444f43d0d0551b28fd", null ],
    [ "optionMinimizedEntry", "classDigikam_1_1Sidebar_1_1Private.html#add58cc9418a06cf5b78118da7692a987", null ],
    [ "optionRestoreSizeEntry", "classDigikam_1_1Sidebar_1_1Private.html#af8154057da01d21e29e0045b06a296ec", null ],
    [ "restoreSize", "classDigikam_1_1Sidebar_1_1Private.html#a3cdeeb9bbecdb2f45f32dbeaea0e683d", null ],
    [ "splitter", "classDigikam_1_1Sidebar_1_1Private.html#a74bd97fcbefdb958aa701913567ed5dd", null ],
    [ "stack", "classDigikam_1_1Sidebar_1_1Private.html#a25c952debcf8a62fe388f5fa73cf989f", null ],
    [ "tabs", "classDigikam_1_1Sidebar_1_1Private.html#a1413f006e6b771d7ef5e0ee474dfefa0", null ]
];