var dir_6bb51b5ab32be2d76966746bada2bbd1 =
[
    [ "imgurimageslist.cpp", "imgurimageslist_8cpp.html", null ],
    [ "imgurimageslist.h", "imgurimageslist_8h.html", [
      [ "ImgurImageListViewItem", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem.html", "classDigikamGenericImgUrPlugin_1_1ImgurImageListViewItem" ],
      [ "ImgurImagesList", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html", "classDigikamGenericImgUrPlugin_1_1ImgurImagesList" ]
    ] ],
    [ "imgurplugin.cpp", "imgurplugin_8cpp.html", null ],
    [ "imgurplugin.h", "imgurplugin_8h.html", "imgurplugin_8h" ],
    [ "imgurtalker.cpp", "imgurtalker_8cpp.html", null ],
    [ "imgurtalker.h", "imgurtalker_8h.html", "imgurtalker_8h" ],
    [ "imgurwindow.cpp", "imgurwindow_8cpp.html", null ],
    [ "imgurwindow.h", "imgurwindow_8h.html", [
      [ "ImgurWindow", "classDigikamGenericImgUrPlugin_1_1ImgurWindow.html", "classDigikamGenericImgUrPlugin_1_1ImgurWindow" ]
    ] ]
];