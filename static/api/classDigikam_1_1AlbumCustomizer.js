var classDigikam_1_1AlbumCustomizer =
[
    [ "DateFormatOptions", "classDigikam_1_1AlbumCustomizer.html#a696267c6c2b0f89fc3a7b9a3e11c0d6f", [
      [ "IsoDateFormat", "classDigikam_1_1AlbumCustomizer.html#a696267c6c2b0f89fc3a7b9a3e11c0d6fa408b89f78192e8194520ff0849b7d0b1", null ],
      [ "TextDateFormat", "classDigikam_1_1AlbumCustomizer.html#a696267c6c2b0f89fc3a7b9a3e11c0d6fa98ac828f4eba3b64b638d1c474e402ed", null ],
      [ "LocalDateFormat", "classDigikam_1_1AlbumCustomizer.html#a696267c6c2b0f89fc3a7b9a3e11c0d6fa0e6b421e4d323f42659a8d1b67b9998c", null ],
      [ "CustomDateFormat", "classDigikam_1_1AlbumCustomizer.html#a696267c6c2b0f89fc3a7b9a3e11c0d6faa44cb25c9db2d926f2525d2dd72acf4c", null ]
    ] ],
    [ "AlbumCustomizer", "classDigikam_1_1AlbumCustomizer.html#acc65f5867d307735e27f3a94b74c4e59", null ],
    [ "~AlbumCustomizer", "classDigikam_1_1AlbumCustomizer.html#a5f9c78b89be7f6aca57031660e07b115", null ],
    [ "autoAlbumDateEnabled", "classDigikam_1_1AlbumCustomizer.html#a28d03e9423bd862131aa0808e91286af", null ],
    [ "autoAlbumExtEnabled", "classDigikam_1_1AlbumCustomizer.html#a5ba489c8fb2e6e421030e45c89d8a1f2", null ],
    [ "customDateFormat", "classDigikam_1_1AlbumCustomizer.html#a716112964758c17e7cb104cd4cf983f0", null ],
    [ "customDateFormatIsValid", "classDigikam_1_1AlbumCustomizer.html#afe4e9e74a7f3ca965ef538566a663112", null ],
    [ "folderDateFormat", "classDigikam_1_1AlbumCustomizer.html#a0682e92d021f174c756045611912631b", null ],
    [ "readSettings", "classDigikam_1_1AlbumCustomizer.html#a418639879d4a159ffa98b46edfe1ad62", null ],
    [ "saveSettings", "classDigikam_1_1AlbumCustomizer.html#aa75150acec7d1c1cac1bfcbcf71c0ed5", null ]
];