var classsmall__image__buffer =
[
    [ "small_image_buffer", "classsmall__image__buffer.html#aa55f69ff2adfb65b2a4d9cd765cfcd05", null ],
    [ "~small_image_buffer", "classsmall__image__buffer.html#a9937e3dc0ea2c05df764bcffdb6405b2", null ],
    [ "copy_to", "classsmall__image__buffer.html#a33f972d4c9a24b1d02a2d9ebb0b3e097", null ],
    [ "get_buffer", "classsmall__image__buffer.html#a4772fe2c8647321de3b467b00bb0d31d", null ],
    [ "get_buffer_s16", "classsmall__image__buffer.html#a6b20143572b04aa862ed2b38cfce5c54", null ],
    [ "get_buffer_u16", "classsmall__image__buffer.html#afca162f6fe030bc8489f0a1426c1a657", null ],
    [ "get_buffer_u8", "classsmall__image__buffer.html#a53d1c19d2d47d6b2babfd538d3fb7ffc", null ],
    [ "getHeight", "classsmall__image__buffer.html#a53fe1f7e9c1c2cd1c52c15313cd1f86a", null ],
    [ "getStride", "classsmall__image__buffer.html#a827ca621636c6132c943f75864ba5856", null ],
    [ "getWidth", "classsmall__image__buffer.html#ac7f6450cbd1f96085a2656a66f364ed9", null ]
];