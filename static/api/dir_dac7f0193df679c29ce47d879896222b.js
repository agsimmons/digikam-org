var dir_dac7f0193df679c29ce47d879896222b =
[
    [ "dpluginaboutdlg.cpp", "dpluginaboutdlg_8cpp.html", null ],
    [ "dpluginaboutdlg.h", "dpluginaboutdlg_8h.html", [
      [ "DPluginAboutDlg", "classDigikam_1_1DPluginAboutDlg.html", "classDigikam_1_1DPluginAboutDlg" ]
    ] ],
    [ "dpluginconfview.cpp", "dpluginconfview_8cpp.html", null ],
    [ "dpluginconfview.h", "dpluginconfview_8h.html", [
      [ "DPluginConfView", "classDigikam_1_1DPluginConfView.html", "classDigikam_1_1DPluginConfView" ]
    ] ],
    [ "dpluginconfviewdimg.cpp", "dpluginconfviewdimg_8cpp.html", null ],
    [ "dpluginconfviewdimg.h", "dpluginconfviewdimg_8h.html", [
      [ "DPluginConfViewDImg", "classDigikam_1_1DPluginConfViewDImg.html", "classDigikam_1_1DPluginConfViewDImg" ]
    ] ],
    [ "dpluginconfvieweditor.cpp", "dpluginconfvieweditor_8cpp.html", null ],
    [ "dpluginconfvieweditor.h", "dpluginconfvieweditor_8h.html", [
      [ "DPluginConfViewEditor", "classDigikam_1_1DPluginConfViewEditor.html", "classDigikam_1_1DPluginConfViewEditor" ]
    ] ],
    [ "dpluginconfviewgeneric.cpp", "dpluginconfviewgeneric_8cpp.html", null ],
    [ "dpluginconfviewgeneric.h", "dpluginconfviewgeneric_8h.html", [
      [ "DPluginConfViewGeneric", "classDigikam_1_1DPluginConfViewGeneric.html", "classDigikam_1_1DPluginConfViewGeneric" ]
    ] ],
    [ "dpluginloader.cpp", "dpluginloader_8cpp.html", null ],
    [ "dpluginloader.h", "dpluginloader_8h.html", [
      [ "DPluginLoader", "classDigikam_1_1DPluginLoader.html", "classDigikam_1_1DPluginLoader" ]
    ] ],
    [ "dpluginloader_p.cpp", "dpluginloader__p_8cpp.html", null ],
    [ "dpluginloader_p.h", "dpluginloader__p_8h.html", [
      [ "Private", "classDigikam_1_1DPluginLoader_1_1Private.html", "classDigikam_1_1DPluginLoader_1_1Private" ]
    ] ],
    [ "dpluginsetup.cpp", "dpluginsetup_8cpp.html", null ],
    [ "dpluginsetup.h", "dpluginsetup_8h.html", [
      [ "DPluginSetup", "classDigikam_1_1DPluginSetup.html", "classDigikam_1_1DPluginSetup" ]
    ] ]
];