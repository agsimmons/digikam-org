var classDigikamGenericPinterestPlugin_1_1PWindow =
[
    [ "PWindow", "classDigikamGenericPinterestPlugin_1_1PWindow.html#aea631d4f41fca9423c9b31f72ce85268", null ],
    [ "~PWindow", "classDigikamGenericPinterestPlugin_1_1PWindow.html#ab09ebf757baf1672b8cf34feacfdcbe7", null ],
    [ "addButton", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "reactivate", "classDigikamGenericPinterestPlugin_1_1PWindow.html#ace791ca3c4b82068272bb87c4805d473", null ],
    [ "restoreDialogSize", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setItemsList", "classDigikamGenericPinterestPlugin_1_1PWindow.html#af6b3e537e65c508f8414e3473e923397", null ],
    [ "setMainWidget", "classDigikamGenericPinterestPlugin_1_1PWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericPinterestPlugin_1_1PWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];