var classDigikamGenericMetadataEditPlugin_1_1IPTCProperties =
[
    [ "IPTCProperties", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html#acd7ed8c7052d66511a38a145c7752f30", null ],
    [ "~IPTCProperties", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html#a35ef187cf519dbf3ef62b7baed8f29b1", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html#a0f83eb0d217c6d5daa01d92a58407540", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html#a8eceaa35fe7ef83a03a4898085238eb9", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1IPTCProperties.html#ad94d7984679a7df38a938d33495fd529", null ]
];