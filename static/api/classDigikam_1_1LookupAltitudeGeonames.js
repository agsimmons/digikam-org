var classDigikam_1_1LookupAltitudeGeonames =
[
    [ "StatusEnum", "classDigikam_1_1LookupAltitudeGeonames.html#ad3320913e71eebe73799045a4de67f4c", [
      [ "StatusInProgress", "classDigikam_1_1LookupAltitudeGeonames.html#ad3320913e71eebe73799045a4de67f4cac5d94f1275993d0144722e86040514ba", null ],
      [ "StatusSuccess", "classDigikam_1_1LookupAltitudeGeonames.html#ad3320913e71eebe73799045a4de67f4caa61b7ceac9bbd483754033f6fa3cfc87", null ],
      [ "StatusCanceled", "classDigikam_1_1LookupAltitudeGeonames.html#ad3320913e71eebe73799045a4de67f4cada85b12d51a22dcbbf658ab7da15ceab", null ],
      [ "StatusError", "classDigikam_1_1LookupAltitudeGeonames.html#ad3320913e71eebe73799045a4de67f4caacad9eb8c3792cf2414816e3a4699b5b", null ]
    ] ],
    [ "LookupAltitudeGeonames", "classDigikam_1_1LookupAltitudeGeonames.html#a70bb834299d0be872560c9642e063273", null ],
    [ "~LookupAltitudeGeonames", "classDigikam_1_1LookupAltitudeGeonames.html#a27f61a8e73b1b71f463ad9bcc72eba44", null ],
    [ "addRequests", "classDigikam_1_1LookupAltitudeGeonames.html#a28ed6cb449a35faf9d5b31ccacc41a47", null ],
    [ "addRequests", "classDigikam_1_1LookupAltitudeGeonames.html#a806f9bb2496804b7aedc36939444497c", null ],
    [ "backendHumanName", "classDigikam_1_1LookupAltitudeGeonames.html#a5ccd4ab3f3685523029fd06c599c3801", null ],
    [ "backendName", "classDigikam_1_1LookupAltitudeGeonames.html#a1263a1a27636515577d6d14df3134eb6", null ],
    [ "cancel", "classDigikam_1_1LookupAltitudeGeonames.html#a61ca5ff631270ca9f538b7fb6118b16c", null ],
    [ "errorMessage", "classDigikam_1_1LookupAltitudeGeonames.html#ab206d06232920a17ec3844fd94d91eab", null ],
    [ "getRequest", "classDigikam_1_1LookupAltitudeGeonames.html#a0d8fa10affa556f6e191c17ed343b947", null ],
    [ "getRequests", "classDigikam_1_1LookupAltitudeGeonames.html#acc11325715a1c3505ca65e29bcd9d3f1", null ],
    [ "getStatus", "classDigikam_1_1LookupAltitudeGeonames.html#a857972d1fdf9a3e52bc177638c54d7ae", null ],
    [ "signalDone", "classDigikam_1_1LookupAltitudeGeonames.html#a5988d5ffe26c81482d15860f85ccc846", null ],
    [ "signalRequestsReady", "classDigikam_1_1LookupAltitudeGeonames.html#a58e7b77d3097d70978005b3472bd6082", null ],
    [ "startLookup", "classDigikam_1_1LookupAltitudeGeonames.html#aafe436a4396f057586814fe5eb87b05c", null ]
];