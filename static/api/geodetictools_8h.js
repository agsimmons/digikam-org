var geodetictools_8h =
[
    [ "Ellipsoid", "classDigikam_1_1Ellipsoid.html", "classDigikam_1_1Ellipsoid" ],
    [ "GeodeticCalculator", "classDigikam_1_1GeodeticCalculator.html", "classDigikam_1_1GeodeticCalculator" ],
    [ "toDegrees", "geodetictools_8h.html#a0c456f78ec441ced7096e40b1fd3457a", null ],
    [ "toDegreesFactor", "geodetictools_8h.html#ac8999e34537521e5bf9622dd7113d4fa", null ],
    [ "toRadians", "geodetictools_8h.html#a41f745cd4ab5ce72c7a0dde123e346b1", null ],
    [ "toRadiansFactor", "geodetictools_8h.html#ade125bfeb0a3a6c92cd7608d2a19989b", null ]
];