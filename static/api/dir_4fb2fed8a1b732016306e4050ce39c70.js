var dir_4fb2fed8a1b732016306e4050ce39c70 =
[
    [ "itemfiltermodel.cpp", "itemfiltermodel_8cpp.html", null ],
    [ "itemfiltermodel.h", "itemfiltermodel_8h.html", [
      [ "ImageSortFilterModel", "classDigikam_1_1ImageSortFilterModel.html", "classDigikam_1_1ImageSortFilterModel" ],
      [ "ItemFilterModel", "classDigikam_1_1ItemFilterModel.html", "classDigikam_1_1ItemFilterModel" ],
      [ "ItemFilterModelPrepareHook", "classDigikam_1_1ItemFilterModelPrepareHook.html", "classDigikam_1_1ItemFilterModelPrepareHook" ],
      [ "NoDuplicatesItemFilterModel", "classDigikam_1_1NoDuplicatesItemFilterModel.html", "classDigikam_1_1NoDuplicatesItemFilterModel" ]
    ] ],
    [ "itemfiltermodel_p.cpp", "itemfiltermodel__p_8cpp.html", null ],
    [ "itemfiltermodel_p.h", "itemfiltermodel__p_8h.html", "itemfiltermodel__p_8h" ],
    [ "itemfiltermodelthreads.cpp", "itemfiltermodelthreads_8cpp.html", null ],
    [ "itemfiltermodelthreads.h", "itemfiltermodelthreads_8h.html", [
      [ "ItemFilterModelFilterer", "classDigikam_1_1ItemFilterModelFilterer.html", "classDigikam_1_1ItemFilterModelFilterer" ],
      [ "ItemFilterModelPreparer", "classDigikam_1_1ItemFilterModelPreparer.html", "classDigikam_1_1ItemFilterModelPreparer" ],
      [ "ItemFilterModelWorker", "classDigikam_1_1ItemFilterModelWorker.html", "classDigikam_1_1ItemFilterModelWorker" ]
    ] ],
    [ "itemfiltersettings.cpp", "itemfiltersettings_8cpp.html", "itemfiltersettings_8cpp" ],
    [ "itemfiltersettings.h", "itemfiltersettings_8h.html", [
      [ "GroupItemFilterSettings", "classDigikam_1_1GroupItemFilterSettings.html", "classDigikam_1_1GroupItemFilterSettings" ],
      [ "ItemFilterSettings", "classDigikam_1_1ItemFilterSettings.html", "classDigikam_1_1ItemFilterSettings" ],
      [ "SearchTextFilterSettings", "classDigikam_1_1SearchTextFilterSettings.html", "classDigikam_1_1SearchTextFilterSettings" ],
      [ "VersionItemFilterSettings", "classDigikam_1_1VersionItemFilterSettings.html", "classDigikam_1_1VersionItemFilterSettings" ]
    ] ],
    [ "itemlistmodel.cpp", "itemlistmodel_8cpp.html", null ],
    [ "itemlistmodel.h", "itemlistmodel_8h.html", [
      [ "ItemListModel", "classDigikam_1_1ItemListModel.html", "classDigikam_1_1ItemListModel" ]
    ] ],
    [ "itemmodel.cpp", "itemmodel_8cpp.html", "itemmodel_8cpp" ],
    [ "itemmodel.h", "itemmodel_8h.html", [
      [ "ItemModel", "classDigikam_1_1ItemModel.html", "classDigikam_1_1ItemModel" ]
    ] ],
    [ "itemsortcollator.cpp", "itemsortcollator_8cpp.html", null ],
    [ "itemsortcollator.h", "itemsortcollator_8h.html", [
      [ "ItemSortCollator", "classDigikam_1_1ItemSortCollator.html", "classDigikam_1_1ItemSortCollator" ]
    ] ],
    [ "itemsortsettings.cpp", "itemsortsettings_8cpp.html", null ],
    [ "itemsortsettings.h", "itemsortsettings_8h.html", [
      [ "ItemSortSettings", "classDigikam_1_1ItemSortSettings.html", "classDigikam_1_1ItemSortSettings" ]
    ] ],
    [ "itemthumbnailmodel.cpp", "itemthumbnailmodel_8cpp.html", null ],
    [ "itemthumbnailmodel.h", "itemthumbnailmodel_8h.html", [
      [ "ItemThumbnailModel", "classDigikam_1_1ItemThumbnailModel.html", "classDigikam_1_1ItemThumbnailModel" ]
    ] ],
    [ "itemversionsmodel.cpp", "itemversionsmodel_8cpp.html", null ],
    [ "itemversionsmodel.h", "itemversionsmodel_8h.html", [
      [ "ItemVersionsModel", "classDigikam_1_1ItemVersionsModel.html", "classDigikam_1_1ItemVersionsModel" ]
    ] ]
];