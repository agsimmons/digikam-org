var classDigikam_1_1DetectionWorker =
[
    [ "DeactivatingMode", "classDigikam_1_1DetectionWorker.html#a0e4fe71b3aaeaf90979d9c7f19d7b280", [
      [ "FlushSignals", "classDigikam_1_1DetectionWorker.html#a0e4fe71b3aaeaf90979d9c7f19d7b280a020c4869203c0883d0a367a8005b929a", null ],
      [ "KeepSignals", "classDigikam_1_1DetectionWorker.html#a0e4fe71b3aaeaf90979d9c7f19d7b280a985aa0e2416222ee43292f990cb21bce", null ],
      [ "PhaseOut", "classDigikam_1_1DetectionWorker.html#a0e4fe71b3aaeaf90979d9c7f19d7b280a9a11c84626a985c3577959f273ee5f4a", null ]
    ] ],
    [ "State", "classDigikam_1_1DetectionWorker.html#a55d536a66cc80f349aef0bd295db1305", [
      [ "Inactive", "classDigikam_1_1DetectionWorker.html#a55d536a66cc80f349aef0bd295db1305ac9fda253787f588e35a4b3cc7ec18fa1", null ],
      [ "Scheduled", "classDigikam_1_1DetectionWorker.html#a55d536a66cc80f349aef0bd295db1305a76da8b23306c48d505db0a3b3bfe9163", null ],
      [ "Running", "classDigikam_1_1DetectionWorker.html#a55d536a66cc80f349aef0bd295db1305ad0283460e34e14efdedae72159891548", null ],
      [ "Deactivating", "classDigikam_1_1DetectionWorker.html#a55d536a66cc80f349aef0bd295db1305aa885c6ec0c3e4e6c3ec1932e2cc6c2b5", null ]
    ] ],
    [ "DetectionWorker", "classDigikam_1_1DetectionWorker.html#a184be40337ec9a07e6dd019110db7bfa", null ],
    [ "~DetectionWorker", "classDigikam_1_1DetectionWorker.html#ab993e6da003dd43416a749b26da2ab8d", null ],
    [ "aboutToDeactivate", "classDigikam_1_1DetectionWorker.html#ad9b71f4b868bedeaa2072e0bfe213a52", null ],
    [ "aboutToQuitLoop", "classDigikam_1_1DetectionWorker.html#a8360c2a5a4bce223ac31f0967e930825", null ],
    [ "addRunnable", "classDigikam_1_1DetectionWorker.html#ad1d38302e3000c41098994cd507f860c", null ],
    [ "connectAndSchedule", "classDigikam_1_1DetectionWorker.html#a680c211e4c2f88cc558b16fdec211b3f", null ],
    [ "deactivate", "classDigikam_1_1DetectionWorker.html#a8771c7a87b2677254f2c5bb96b586af2", null ],
    [ "event", "classDigikam_1_1DetectionWorker.html#a155f0c3c925c4503df15fdaadd960b49", null ],
    [ "finished", "classDigikam_1_1DetectionWorker.html#a60ca2c7a31c965564e78111c54219267", null ],
    [ "priority", "classDigikam_1_1DetectionWorker.html#a0bb439fc69bf01f6925f77618645189f", null ],
    [ "process", "classDigikam_1_1DetectionWorker.html#aa50e38aaece00c49ebbd076e231a7aee", null ],
    [ "processed", "classDigikam_1_1DetectionWorker.html#a67ba59d5cf33cc73f218cc6b3da51f01", null ],
    [ "removeRunnable", "classDigikam_1_1DetectionWorker.html#ae57b234ee8aadf2cd69a6dd39c2198ac", null ],
    [ "run", "classDigikam_1_1DetectionWorker.html#a25d661b1f4144ca4ae3347174e5a3470", null ],
    [ "scaleForDetection", "classDigikam_1_1DetectionWorker.html#a0b82c1a89a541f81b2de8c73b4659891", null ],
    [ "schedule", "classDigikam_1_1DetectionWorker.html#a840cab83db58ee78572d90a3fd546242", null ],
    [ "setAccuracyAndModel", "classDigikam_1_1DetectionWorker.html#afb334bcfe64e7547bbe198a511ef0d5d", null ],
    [ "setEventLoop", "classDigikam_1_1DetectionWorker.html#a290755ac03dae49e4c6711637024f6d2", null ],
    [ "setPriority", "classDigikam_1_1DetectionWorker.html#a30ce0d8589b3a1144f9dbd065436c15a", null ],
    [ "shutDown", "classDigikam_1_1DetectionWorker.html#a0624d70b2884e2fcf0b7d30db08330bd", null ],
    [ "started", "classDigikam_1_1DetectionWorker.html#a86aef410d35e5186e30f0afd01726835", null ],
    [ "state", "classDigikam_1_1DetectionWorker.html#a988bebe78e1e2155067be62acaf43801", null ],
    [ "transitionToInactive", "classDigikam_1_1DetectionWorker.html#ac9bf9c048edf4ae9b1d7eebc16b86f2c", null ],
    [ "transitionToRunning", "classDigikam_1_1DetectionWorker.html#a2d611107c217f5525c5ea253bbd84a6b", null ],
    [ "wait", "classDigikam_1_1DetectionWorker.html#a47129b8571a7c1ca1d8d5aea8e6c056f", null ],
    [ "d", "classDigikam_1_1DetectionWorker.html#af4193766aa9b84705627ee0363a899c0", null ],
    [ "detector", "classDigikam_1_1DetectionWorker.html#adf96690592fde28727c907f7ec6cc4eb", null ]
];