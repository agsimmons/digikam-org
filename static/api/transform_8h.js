var transform_8h =
[
    [ "decode_quantization_parameters", "transform_8h.html#aa5e1d3a4b1a79f1adba275c674ccb9b4", null ],
    [ "dequant_coefficients", "transform_8h.html#a95e7cdbc15f91a2a05790b56b64cafb6", null ],
    [ "fwd_transform", "transform_8h.html#a5d67174108bc97dcae1f9f9930fd82a6", null ],
    [ "inv_transform", "transform_8h.html#ab78100782f1b254c1eb1dcaa9ceec7b9", null ],
    [ "quant_coefficients", "transform_8h.html#a8ac1af48bb4f5b4336d00e517fcac79d", null ],
    [ "scale_coefficients", "transform_8h.html#abe5f06c671cc49da3f6b207835a798ff", null ],
    [ "tab8_22", "transform_8h.html#ab8114734db79a6589b3e979f44037a02", null ]
];