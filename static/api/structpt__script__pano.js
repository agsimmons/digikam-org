var structpt__script__pano =
[
    [ "bitDepthOutput", "structpt__script__pano.html#a18c6d5429cc5f922a6c1c40492747422", null ],
    [ "cropArea", "structpt__script__pano.html#a9e299e993bc7a6203c82d3e00ab815ae", null ],
    [ "dynamicRangeMode", "structpt__script__pano.html#ada35270ffa2a3ef779da963ff2ccf901", null ],
    [ "exposureValue", "structpt__script__pano.html#ae9f30ada379cc5256c732bdd0ede0868", null ],
    [ "fHorFOV", "structpt__script__pano.html#aff5f8cc1fd8f93ca52fbceb1cb715e6a", null ],
    [ "height", "structpt__script__pano.html#a157580b9c401f0ee543e79bb034c72cc", null ],
    [ "iImagePhotometricReference", "structpt__script__pano.html#afc97d2382a415b1a118299f7c622b7b8", null ],
    [ "outputFormat", "structpt__script__pano.html#a29581fcade1a33eae83cc778175f3039", null ],
    [ "projection", "structpt__script__pano.html#ab23abb7ba8d15e35c7a200d0d41eb6db", null ],
    [ "projectionParms", "structpt__script__pano.html#acd0a69096019258203656261cfc32144", null ],
    [ "projectionParmsCount", "structpt__script__pano.html#a6bd5eebe0d44f8ff4118cc12f775be0e", null ],
    [ "width", "structpt__script__pano.html#a4721f0adf953ac1d7d87317538bd6434", null ]
];