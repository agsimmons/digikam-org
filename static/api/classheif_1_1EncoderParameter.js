var classheif_1_1EncoderParameter =
[
    [ "get_name", "classheif_1_1EncoderParameter.html#a443bea03130c51871a165c3a3e657793", null ],
    [ "get_type", "classheif_1_1EncoderParameter.html#a63a4b3442df715386b27df178976c0af", null ],
    [ "get_valid_integer_range", "classheif_1_1EncoderParameter.html#ae2f2bcc5a2b7203d56628f4debb59fec", null ],
    [ "get_valid_string_values", "classheif_1_1EncoderParameter.html#a103f8ca4a3387fc880c11ed12c7e0eb8", null ],
    [ "is_boolean", "classheif_1_1EncoderParameter.html#a15dbede09240b21de3973679fda1af12", null ],
    [ "is_integer", "classheif_1_1EncoderParameter.html#a62dea12e0d16a59e1d4ddab1cfd16c8b", null ],
    [ "is_string", "classheif_1_1EncoderParameter.html#a62629aa431401d2684677017443d1546", null ],
    [ "Encoder", "classheif_1_1EncoderParameter.html#a9f0ec0ed691ee924e57d5e71c780681a", null ]
];