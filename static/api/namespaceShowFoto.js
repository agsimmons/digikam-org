var namespaceShowFoto =
[
    [ "ShowfotoToolTipFiller", "namespaceShowFoto_1_1ShowfotoToolTipFiller.html", [
      [ "ShowfotoItemInfoTipContents", "namespaceShowFoto_1_1ShowfotoToolTipFiller.html#a0c9205822d7de3c22aa2519fd4cdafb1", null ]
    ] ],
    [ "NoDuplicatesShowfotoFilterModel", "classShowFoto_1_1NoDuplicatesShowfotoFilterModel.html", "classShowFoto_1_1NoDuplicatesShowfotoFilterModel" ],
    [ "Showfoto", "classShowFoto_1_1Showfoto.html", "classShowFoto_1_1Showfoto" ],
    [ "ShowfotoCategorizedView", "classShowFoto_1_1ShowfotoCategorizedView.html", "classShowFoto_1_1ShowfotoCategorizedView" ],
    [ "ShowfotoCoordinatesOverlay", "classShowFoto_1_1ShowfotoCoordinatesOverlay.html", "classShowFoto_1_1ShowfotoCoordinatesOverlay" ],
    [ "ShowfotoCoordinatesOverlayWidget", "classShowFoto_1_1ShowfotoCoordinatesOverlayWidget.html", "classShowFoto_1_1ShowfotoCoordinatesOverlayWidget" ],
    [ "ShowfotoDelegate", "classShowFoto_1_1ShowfotoDelegate.html", "classShowFoto_1_1ShowfotoDelegate" ],
    [ "ShowfotoDragDropHandler", "classShowFoto_1_1ShowfotoDragDropHandler.html", "classShowFoto_1_1ShowfotoDragDropHandler" ],
    [ "ShowfotoFilterModel", "classShowFoto_1_1ShowfotoFilterModel.html", "classShowFoto_1_1ShowfotoFilterModel" ],
    [ "ShowfotoFolderViewBar", "classShowFoto_1_1ShowfotoFolderViewBar.html", "classShowFoto_1_1ShowfotoFolderViewBar" ],
    [ "ShowfotoFolderViewBookmarkDlg", "classShowFoto_1_1ShowfotoFolderViewBookmarkDlg.html", "classShowFoto_1_1ShowfotoFolderViewBookmarkDlg" ],
    [ "ShowfotoFolderViewBookmarkItem", "classShowFoto_1_1ShowfotoFolderViewBookmarkItem.html", "classShowFoto_1_1ShowfotoFolderViewBookmarkItem" ],
    [ "ShowfotoFolderViewBookmarkList", "classShowFoto_1_1ShowfotoFolderViewBookmarkList.html", "classShowFoto_1_1ShowfotoFolderViewBookmarkList" ],
    [ "ShowfotoFolderViewBookmarks", "classShowFoto_1_1ShowfotoFolderViewBookmarks.html", "classShowFoto_1_1ShowfotoFolderViewBookmarks" ],
    [ "ShowfotoFolderViewList", "classShowFoto_1_1ShowfotoFolderViewList.html", "classShowFoto_1_1ShowfotoFolderViewList" ],
    [ "ShowfotoFolderViewModel", "classShowFoto_1_1ShowfotoFolderViewModel.html", "classShowFoto_1_1ShowfotoFolderViewModel" ],
    [ "ShowfotoFolderViewSideBar", "classShowFoto_1_1ShowfotoFolderViewSideBar.html", "classShowFoto_1_1ShowfotoFolderViewSideBar" ],
    [ "ShowfotoFolderViewToolTip", "classShowFoto_1_1ShowfotoFolderViewToolTip.html", "classShowFoto_1_1ShowfotoFolderViewToolTip" ],
    [ "ShowfotoFolderViewUndo", "classShowFoto_1_1ShowfotoFolderViewUndo.html", "classShowFoto_1_1ShowfotoFolderViewUndo" ],
    [ "ShowfotoInfoIface", "classShowFoto_1_1ShowfotoInfoIface.html", "classShowFoto_1_1ShowfotoInfoIface" ],
    [ "ShowfotoItemInfo", "classShowFoto_1_1ShowfotoItemInfo.html", "classShowFoto_1_1ShowfotoItemInfo" ],
    [ "ShowfotoItemModel", "classShowFoto_1_1ShowfotoItemModel.html", "classShowFoto_1_1ShowfotoItemModel" ],
    [ "ShowfotoItemSortSettings", "classShowFoto_1_1ShowfotoItemSortSettings.html", "classShowFoto_1_1ShowfotoItemSortSettings" ],
    [ "ShowfotoItemViewDelegate", "classShowFoto_1_1ShowfotoItemViewDelegate.html", "classShowFoto_1_1ShowfotoItemViewDelegate" ],
    [ "ShowfotoItemViewDelegatePrivate", "classShowFoto_1_1ShowfotoItemViewDelegatePrivate.html", "classShowFoto_1_1ShowfotoItemViewDelegatePrivate" ],
    [ "ShowfotoKineticScroller", "classShowFoto_1_1ShowfotoKineticScroller.html", "classShowFoto_1_1ShowfotoKineticScroller" ],
    [ "ShowfotoNormalDelegate", "classShowFoto_1_1ShowfotoNormalDelegate.html", "classShowFoto_1_1ShowfotoNormalDelegate" ],
    [ "ShowfotoNormalDelegatePrivate", "classShowFoto_1_1ShowfotoNormalDelegatePrivate.html", "classShowFoto_1_1ShowfotoNormalDelegatePrivate" ],
    [ "ShowfotoSettings", "classShowFoto_1_1ShowfotoSettings.html", "classShowFoto_1_1ShowfotoSettings" ],
    [ "ShowfotoSetup", "classShowFoto_1_1ShowfotoSetup.html", "classShowFoto_1_1ShowfotoSetup" ],
    [ "ShowfotoSetupMetadata", "classShowFoto_1_1ShowfotoSetupMetadata.html", "classShowFoto_1_1ShowfotoSetupMetadata" ],
    [ "ShowfotoSetupMisc", "classShowFoto_1_1ShowfotoSetupMisc.html", "classShowFoto_1_1ShowfotoSetupMisc" ],
    [ "ShowfotoSetupPlugins", "classShowFoto_1_1ShowfotoSetupPlugins.html", "classShowFoto_1_1ShowfotoSetupPlugins" ],
    [ "ShowfotoSetupRaw", "classShowFoto_1_1ShowfotoSetupRaw.html", "classShowFoto_1_1ShowfotoSetupRaw" ],
    [ "ShowfotoSetupToolTip", "classShowFoto_1_1ShowfotoSetupToolTip.html", "classShowFoto_1_1ShowfotoSetupToolTip" ],
    [ "ShowfotoSortFilterModel", "classShowFoto_1_1ShowfotoSortFilterModel.html", "classShowFoto_1_1ShowfotoSortFilterModel" ],
    [ "ShowfotoStackViewFavoriteItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItem.html", "classShowFoto_1_1ShowfotoStackViewFavoriteItem" ],
    [ "ShowfotoStackViewFavoriteItemDlg", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg" ],
    [ "ShowfotoStackViewFavoriteList", "classShowFoto_1_1ShowfotoStackViewFavoriteList.html", "classShowFoto_1_1ShowfotoStackViewFavoriteList" ],
    [ "ShowfotoStackViewFavorites", "classShowFoto_1_1ShowfotoStackViewFavorites.html", "classShowFoto_1_1ShowfotoStackViewFavorites" ],
    [ "ShowfotoStackViewItem", "classShowFoto_1_1ShowfotoStackViewItem.html", "classShowFoto_1_1ShowfotoStackViewItem" ],
    [ "ShowfotoStackViewList", "classShowFoto_1_1ShowfotoStackViewList.html", "classShowFoto_1_1ShowfotoStackViewList" ],
    [ "ShowfotoStackViewSideBar", "classShowFoto_1_1ShowfotoStackViewSideBar.html", "classShowFoto_1_1ShowfotoStackViewSideBar" ],
    [ "ShowfotoStackViewToolTip", "classShowFoto_1_1ShowfotoStackViewToolTip.html", "classShowFoto_1_1ShowfotoStackViewToolTip" ],
    [ "ShowfotoThumbnailBar", "classShowFoto_1_1ShowfotoThumbnailBar.html", "classShowFoto_1_1ShowfotoThumbnailBar" ],
    [ "ShowfotoThumbnailDelegate", "classShowFoto_1_1ShowfotoThumbnailDelegate.html", "classShowFoto_1_1ShowfotoThumbnailDelegate" ],
    [ "ShowfotoThumbnailDelegatePrivate", "classShowFoto_1_1ShowfotoThumbnailDelegatePrivate.html", "classShowFoto_1_1ShowfotoThumbnailDelegatePrivate" ],
    [ "ShowfotoThumbnailModel", "classShowFoto_1_1ShowfotoThumbnailModel.html", "classShowFoto_1_1ShowfotoThumbnailModel" ],
    [ "CachedItem", "namespaceShowFoto.html#a2b830db790016eb906dee3a04581a427", null ],
    [ "IntPair", "namespaceShowFoto.html#a99e094d065cb90430aab2e91bc9832f3", null ],
    [ "ShowfotoItemInfoList", "namespaceShowFoto.html#a2effecfa7384a8ac9589e3f585f4bf21", null ],
    [ "operator<<", "namespaceShowFoto.html#a0e6632a23872efcc0a0fa5a125009166", null ],
    [ "operator<<", "namespaceShowFoto.html#a6e2cc44e7fac99e0994c8c2d68fea1ab", null ],
    [ "operator>>", "namespaceShowFoto.html#a5038022ea09b0fdd2b6f2be737242714", null ]
];