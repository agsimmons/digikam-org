var classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage =
[
    [ "WSAlbumsPage", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a6c60315aea6744d43962d0f38bc55598", null ],
    [ "~WSAlbumsPage", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a7fa90c62b9167be04328c22453727645", null ],
    [ "assistant", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "id", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "isComplete", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#ae1cf339314e3405c6a675e3fee61d9a8", null ],
    [ "removePageWidget", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "setComplete", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#a67975edf6041a574e674576a29d606a1", null ],
    [ "validatePage", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html#afb7c96e2ee605b6e4d75f3b5b52f51a7", null ]
];