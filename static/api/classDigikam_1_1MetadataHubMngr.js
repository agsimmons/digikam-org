var classDigikam_1_1MetadataHubMngr =
[
    [ "~MetadataHubMngr", "classDigikam_1_1MetadataHubMngr.html#a9ee264211dfa78e8dc8fec130384697d", null ],
    [ "addPending", "classDigikam_1_1MetadataHubMngr.html#a27b65a6b24a545fac672d2a4d2ee5530", null ],
    [ "requestShutDown", "classDigikam_1_1MetadataHubMngr.html#a16771f99102d428a86eeb132fa92a835", null ],
    [ "signalPendingMetadata", "classDigikam_1_1MetadataHubMngr.html#a5a652d4990769323b75370daf211c418", null ],
    [ "slotApplyPending", "classDigikam_1_1MetadataHubMngr.html#a74c7998a088061b4001a5b9298ca5ebc", null ]
];