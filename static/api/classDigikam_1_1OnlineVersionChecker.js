var classDigikam_1_1OnlineVersionChecker =
[
    [ "OnlineVersionChecker", "classDigikam_1_1OnlineVersionChecker.html#ae82cf73769900f65dcd1b5a81c12c1ae", null ],
    [ "~OnlineVersionChecker", "classDigikam_1_1OnlineVersionChecker.html#a842fed2c87481e7ab582a1ad4872457e", null ],
    [ "cancelCheck", "classDigikam_1_1OnlineVersionChecker.html#a729b1089f11e9c6f09dcaeb726e39a4e", null ],
    [ "checkForNewVersion", "classDigikam_1_1OnlineVersionChecker.html#aea6c5771dfccfba47e358f66c3c98321", null ],
    [ "downloadReleaseNotes", "classDigikam_1_1OnlineVersionChecker.html#ada13baabcf4d6cb526c025bd07dcc324", null ],
    [ "preReleaseFileName", "classDigikam_1_1OnlineVersionChecker.html#a056cdd8d44ff5e86eb6e388cb62162df", null ],
    [ "setCurrentBuildDate", "classDigikam_1_1OnlineVersionChecker.html#aef99c24da78c44a5438a248e6b542c3a", null ],
    [ "setCurrentVersion", "classDigikam_1_1OnlineVersionChecker.html#a74de3de6c496170b37e7b8f56956c83b", null ],
    [ "signalNewVersionAvailable", "classDigikam_1_1OnlineVersionChecker.html#aeeb4402926577d5219451415c1b21407", null ],
    [ "signalNewVersionCheckError", "classDigikam_1_1OnlineVersionChecker.html#acf8187d3934bffb3cb3c86d79c70d1e6", null ],
    [ "signalReleaseNotesData", "classDigikam_1_1OnlineVersionChecker.html#ac43be3e9a11e7dace1ff6619c4a349fc", null ]
];