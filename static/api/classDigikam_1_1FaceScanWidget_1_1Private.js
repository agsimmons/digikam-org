var classDigikam_1_1FaceScanWidget_1_1Private =
[
    [ "Private", "classDigikam_1_1FaceScanWidget_1_1Private.html#a8fedecc14e90320a5180b352ad35e2a1", null ],
    [ "accuracyInput", "classDigikam_1_1FaceScanWidget_1_1Private.html#a7c486a234c95583b7ced600a642b86cd", null ],
    [ "albumSelectors", "classDigikam_1_1FaceScanWidget_1_1Private.html#ac4a02f2ecf3f3ec75c85babd0b3e5d23", null ],
    [ "alreadyScannedBox", "classDigikam_1_1FaceScanWidget_1_1Private.html#a25c8f750eadda44590dce61d0c954a61", null ],
    [ "configAlreadyScannedHandling", "classDigikam_1_1FaceScanWidget_1_1Private.html#a032901421d42d761c2528ccfe5422795", null ],
    [ "configMainTask", "classDigikam_1_1FaceScanWidget_1_1Private.html#ab560a536bebc0e27385455f1edb00e31", null ],
    [ "configName", "classDigikam_1_1FaceScanWidget_1_1Private.html#a3d00467ac7f4ae42125dcffa303c8eab", null ],
    [ "configUseFullCpu", "classDigikam_1_1FaceScanWidget_1_1Private.html#a39e770c908b2b9e6c87991ff4a66baef", null ],
    [ "configValueDetect", "classDigikam_1_1FaceScanWidget_1_1Private.html#a543531aa2b6017d5a771855413021404", null ],
    [ "configValueDetectAndRecognize", "classDigikam_1_1FaceScanWidget_1_1Private.html#a1da3c5b3e6598590dac68da465cdbd3a", null ],
    [ "configValueRecognizedMarkedFaces", "classDigikam_1_1FaceScanWidget_1_1Private.html#a26d5bc2957a40ab0fa6f3dfc43beae51", null ],
    [ "detectAndRecognizeButton", "classDigikam_1_1FaceScanWidget_1_1Private.html#a968e701cd330ea75e555fcc29c66bf7e", null ],
    [ "detectButton", "classDigikam_1_1FaceScanWidget_1_1Private.html#a3520f19ccb5921a5aea8621129e43a2e", null ],
    [ "helpButton", "classDigikam_1_1FaceScanWidget_1_1Private.html#a7e64973d38947fa67d415f47352c6617", null ],
    [ "reRecognizeButton", "classDigikam_1_1FaceScanWidget_1_1Private.html#a23b3cd21d4385d1f3460e9964aa0cb7f", null ],
    [ "settingsConflicted", "classDigikam_1_1FaceScanWidget_1_1Private.html#a8a930ea08617f4c878e86ce0c3b6d867", null ],
    [ "tabWidget", "classDigikam_1_1FaceScanWidget_1_1Private.html#aeeee239f0bfaba880e070c9683b63246", null ],
    [ "useFullCpuButton", "classDigikam_1_1FaceScanWidget_1_1Private.html#a9b171dbc16bd8baae73481126d618e6e", null ],
    [ "useYoloV3Button", "classDigikam_1_1FaceScanWidget_1_1Private.html#aad57ee4b2f161c96c27610adaf636747", null ],
    [ "workflowWidget", "classDigikam_1_1FaceScanWidget_1_1Private.html#a821e1b6497554137dab4a7c485a53e9a", null ]
];