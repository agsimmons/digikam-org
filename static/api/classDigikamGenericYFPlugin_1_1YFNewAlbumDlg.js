var classDigikamGenericYFPlugin_1_1YFNewAlbumDlg =
[
    [ "YFNewAlbumDlg", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a07d5fccec0ece4c756aef947d229305e", null ],
    [ "~YFNewAlbumDlg", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a647935d2dd96e6f8fed2056995677db4", null ],
    [ "addToMainLayout", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "album", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a9bbf33408f405fc7aa383b59123c514b", null ],
    [ "getAlbumBox", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getLocEdit", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericYFPlugin_1_1YFNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];