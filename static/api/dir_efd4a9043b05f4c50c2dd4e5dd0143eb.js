var dir_efd4a9043b05f4c50c2dd4e5dd0143eb =
[
    [ "wsalbumspage.cpp", "wsalbumspage_8cpp.html", null ],
    [ "wsalbumspage.h", "wsalbumspage_8h.html", [
      [ "WSAlbumsPage", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage.html", "classDigikamGenericUnifiedPlugin_1_1WSAlbumsPage" ]
    ] ],
    [ "wsauthenticationpage.cpp", "wsauthenticationpage_8cpp.html", null ],
    [ "wsauthenticationpage.h", "wsauthenticationpage_8h.html", [
      [ "WSAuthenticationPage", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPage" ],
      [ "WSAuthenticationPageView", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationPageView" ],
      [ "WSAuthenticationWizard", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationWizard.html", "classDigikamGenericUnifiedPlugin_1_1WSAuthenticationWizard" ]
    ] ],
    [ "wsfinalpage.cpp", "wsfinalpage_8cpp.html", null ],
    [ "wsfinalpage.h", "wsfinalpage_8h.html", [
      [ "WSFinalPage", "classDigikamGenericUnifiedPlugin_1_1WSFinalPage.html", "classDigikamGenericUnifiedPlugin_1_1WSFinalPage" ]
    ] ],
    [ "wsimagespage.cpp", "wsimagespage_8cpp.html", null ],
    [ "wsimagespage.h", "wsimagespage_8h.html", [
      [ "WSImagesPage", "classDigikamGenericUnifiedPlugin_1_1WSImagesPage.html", "classDigikamGenericUnifiedPlugin_1_1WSImagesPage" ]
    ] ],
    [ "wsintropage.cpp", "wsintropage_8cpp.html", null ],
    [ "wsintropage.h", "wsintropage_8h.html", [
      [ "WSIntroPage", "classDigikamGenericUnifiedPlugin_1_1WSIntroPage.html", "classDigikamGenericUnifiedPlugin_1_1WSIntroPage" ]
    ] ],
    [ "wssettingspage.cpp", "wssettingspage_8cpp.html", null ],
    [ "wssettingspage.h", "wssettingspage_8h.html", [
      [ "WSSettingsPage", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage.html", "classDigikamGenericUnifiedPlugin_1_1WSSettingsPage" ]
    ] ],
    [ "wswizard.cpp", "wswizard_8cpp.html", null ],
    [ "wswizard.h", "wswizard_8h.html", [
      [ "WSWizard", "classDigikamGenericUnifiedPlugin_1_1WSWizard.html", "classDigikamGenericUnifiedPlugin_1_1WSWizard" ]
    ] ]
];