var classDigikam_1_1AlbumFolderViewSideBarWidget =
[
    [ "StateSavingDepth", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "AlbumFolderViewSideBarWidget", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a72c33c67bf96da2273421655f1bee2e8", null ],
    [ "~AlbumFolderViewSideBarWidget", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a283c54876b968d9b964e2b2e77399920", null ],
    [ "applySettings", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a77088f564acadcc4df0925fe46e9093d", null ],
    [ "changeAlbumFromHistory", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#aba5d0e513d597dae51b5a28156515b9f", null ],
    [ "currentAlbum", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a79d2b8e1ba37857a7de52193d91291db", null ],
    [ "doLoadState", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a2f5a8e4091e33ad0aa01a4107da1ef15", null ],
    [ "doSaveState", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#ace1ef4efb3395aa09f4d3bbb181f3516", null ],
    [ "entryName", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#abd19a7f33650f3f129e4d5dbae680a6a", null ],
    [ "getConfigGroup", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#abf6e02f25ad459c06c9ecd8d8c73fe06", null ],
    [ "getStateSavingDepth", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "requestActiveTab", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a247452550423658b7ca3f5ba97fb1f07", null ],
    [ "saveState", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#aa556dcd2191f7aa740a5d42333c1fe87", null ],
    [ "setConfigGroup", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setCurrentAlbum", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a0f9bb2dcd14c276faeef115dcd666730", null ],
    [ "setEntryPrefix", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalFindDuplicates", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#ae3a86668c99de0f7042b8681a71ce510", null ],
    [ "signalNotificationError", "classDigikam_1_1AlbumFolderViewSideBarWidget.html#a12e1b990cb7a7f652ce6f10df1ccfc11", null ]
];