var collectionscannerhints_8h =
[
    [ "AlbumCopyMoveHint", "classDigikam_1_1AlbumCopyMoveHint.html", "classDigikam_1_1AlbumCopyMoveHint" ],
    [ "CollectionScannerHintContainer", "classDigikam_1_1CollectionScannerHintContainer.html", "classDigikam_1_1CollectionScannerHintContainer" ],
    [ "Album", "classDigikam_1_1CollectionScannerHints_1_1Album.html", "classDigikam_1_1CollectionScannerHints_1_1Album" ],
    [ "DstPath", "classDigikam_1_1CollectionScannerHints_1_1DstPath.html", "classDigikam_1_1CollectionScannerHints_1_1DstPath" ],
    [ "Item", "classDigikam_1_1CollectionScannerHints_1_1Item.html", "classDigikam_1_1CollectionScannerHints_1_1Item" ],
    [ "ItemChangeHint", "classDigikam_1_1ItemChangeHint.html", "classDigikam_1_1ItemChangeHint" ],
    [ "ItemCopyMoveHint", "classDigikam_1_1ItemCopyMoveHint.html", "classDigikam_1_1ItemCopyMoveHint" ],
    [ "ItemMetadataAdjustmentHint", "classDigikam_1_1ItemMetadataAdjustmentHint.html", "classDigikam_1_1ItemMetadataAdjustmentHint" ],
    [ "qHash", "collectionscannerhints_8h.html#a8fda9fd2e80e1d7e805e5e48a287e67e", null ],
    [ "qHash", "collectionscannerhints_8h.html#aa4d88505d68eacd63510ae84ed9bebc6", null ],
    [ "qHash", "collectionscannerhints_8h.html#a9da597118d5dc02da57050ead23793aa", null ],
    [ "qHash", "collectionscannerhints_8h.html#a1e8ccabb6a9d0632d9322391dca50ca6", null ]
];