var dir_d36750f7c8b3d32a33248d61d38cb40f =
[
    [ "antivignetting", "dir_c0c328ff2135b8137bcd1618cc75631d.html", "dir_c0c328ff2135b8137bcd1618cc75631d" ],
    [ "blur", "dir_144ac07723ed25ba4c03d4efbc8da356.html", "dir_144ac07723ed25ba4c03d4efbc8da356" ],
    [ "healingclone", "dir_edd5af0a9e516c7ba3aaa198e36c8038.html", "dir_edd5af0a9e516c7ba3aaa198e36c8038" ],
    [ "hotpixels", "dir_c0499ca029924706d422ffd51f5b3521.html", "dir_c0499ca029924706d422ffd51f5b3521" ],
    [ "lensautofix", "dir_b61921ee1477cc0eddc4b4c4e9bf2860.html", "dir_b61921ee1477cc0eddc4b4c4e9bf2860" ],
    [ "lensdistortion", "dir_7767cd69e9dbbf48e95d8482a6abcd86.html", "dir_7767cd69e9dbbf48e95d8482a6abcd86" ],
    [ "localcontrast", "dir_5c1d7667b3ace11f79e1fbfbf3fb9301.html", "dir_5c1d7667b3ace11f79e1fbfbf3fb9301" ],
    [ "noisereduction", "dir_fad7653fb4999890afcef28bfbcffa57.html", "dir_fad7653fb4999890afcef28bfbcffa57" ],
    [ "redeye", "dir_6b662ec413a612e511cc633ad179c8be.html", "dir_6b662ec413a612e511cc633ad179c8be" ],
    [ "restoration", "dir_bff47e1b7785f1a972a9b2e94beaba00.html", "dir_bff47e1b7785f1a972a9b2e94beaba00" ],
    [ "sharpen", "dir_211ab0d3f3d5a4783d44dd0561a9e701.html", "dir_211ab0d3f3d5a4783d44dd0561a9e701" ]
];