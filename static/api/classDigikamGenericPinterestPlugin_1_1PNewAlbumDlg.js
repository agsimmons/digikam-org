var classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg =
[
    [ "PNewAlbumDlg", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#aecf6da52b4f6d671e047e548c9bc31b1", null ],
    [ "~PNewAlbumDlg", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#ac9e962d65c59bdd183a7ab0926d96a68", null ],
    [ "addToMainLayout", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getFolderTitle", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#acef0abf54198b0196ea948f68f02b498", null ],
    [ "getLocEdit", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericPinterestPlugin_1_1PNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];