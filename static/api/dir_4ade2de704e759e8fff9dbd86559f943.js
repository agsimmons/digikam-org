var dir_4ade2de704e759e8fff9dbd86559f943 =
[
    [ "advancedsettings.cpp", "advancedsettings_8cpp.html", null ],
    [ "advancedsettings.h", "advancedsettings_8h.html", [
      [ "AdvancedSettings", "classDigikam_1_1AdvancedSettings.html", "classDigikam_1_1AdvancedSettings" ]
    ] ],
    [ "albumcustomizer.cpp", "albumcustomizer_8cpp.html", null ],
    [ "albumcustomizer.h", "albumcustomizer_8h.html", [
      [ "AlbumCustomizer", "classDigikam_1_1AlbumCustomizer.html", "classDigikam_1_1AlbumCustomizer" ]
    ] ],
    [ "capturewidget.cpp", "capturewidget_8cpp.html", null ],
    [ "capturewidget.h", "capturewidget_8h.html", [
      [ "CaptureWidget", "classDigikam_1_1CaptureWidget.html", "classDigikam_1_1CaptureWidget" ]
    ] ],
    [ "dngconvertsettings.cpp", "dngconvertsettings_8cpp.html", null ],
    [ "dngconvertsettings.h", "dngconvertsettings_8h.html", [
      [ "DNGConvertSettings", "classDigikam_1_1DNGConvertSettings.html", "classDigikam_1_1DNGConvertSettings" ]
    ] ],
    [ "filter.cpp", "filter_8cpp.html", null ],
    [ "filter.h", "filter_8h.html", "filter_8h" ],
    [ "freespacewidget.cpp", "freespacewidget_8cpp.html", null ],
    [ "freespacewidget.h", "freespacewidget_8h.html", [
      [ "FreeSpaceWidget", "classDigikam_1_1FreeSpaceWidget.html", "classDigikam_1_1FreeSpaceWidget" ]
    ] ],
    [ "importcontextmenu.cpp", "importcontextmenu_8cpp.html", null ],
    [ "importcontextmenu.h", "importcontextmenu_8h.html", [
      [ "ImportContextMenuHelper", "classDigikam_1_1ImportContextMenuHelper.html", "classDigikam_1_1ImportContextMenuHelper" ]
    ] ],
    [ "importfiltercombobox.cpp", "importfiltercombobox_8cpp.html", null ],
    [ "importfiltercombobox.h", "importfiltercombobox_8h.html", [
      [ "ImportFilterComboBox", "classDigikam_1_1ImportFilterComboBox.html", "classDigikam_1_1ImportFilterComboBox" ]
    ] ],
    [ "renamecustomizer.cpp", "renamecustomizer_8cpp.html", null ],
    [ "renamecustomizer.h", "renamecustomizer_8h.html", [
      [ "RenameCustomizer", "classDigikam_1_1RenameCustomizer.html", "classDigikam_1_1RenameCustomizer" ]
    ] ],
    [ "scriptingsettings.cpp", "scriptingsettings_8cpp.html", null ],
    [ "scriptingsettings.h", "scriptingsettings_8h.html", [
      [ "ScriptingSettings", "classDigikam_1_1ScriptingSettings.html", "classDigikam_1_1ScriptingSettings" ]
    ] ]
];