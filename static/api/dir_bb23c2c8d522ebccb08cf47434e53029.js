var dir_bb23c2c8d522ebccb08cf47434e53029 =
[
    [ "collectionpage.cpp", "collectionpage_8cpp.html", null ],
    [ "collectionpage.h", "collectionpage_8h.html", [
      [ "CollectionPage", "classDigikam_1_1CollectionPage.html", "classDigikam_1_1CollectionPage" ]
    ] ],
    [ "databasepage.cpp", "databasepage_8cpp.html", null ],
    [ "databasepage.h", "databasepage_8h.html", [
      [ "DatabasePage", "classDigikam_1_1DatabasePage.html", "classDigikam_1_1DatabasePage" ]
    ] ],
    [ "firstrundlg.cpp", "firstrundlg_8cpp.html", null ],
    [ "firstrundlg.h", "firstrundlg_8h.html", [
      [ "FirstRunDlg", "classDigikam_1_1FirstRunDlg.html", "classDigikam_1_1FirstRunDlg" ]
    ] ],
    [ "metadatapage.cpp", "metadatapage_8cpp.html", null ],
    [ "metadatapage.h", "metadatapage_8h.html", [
      [ "MetadataPage", "classDigikam_1_1MetadataPage.html", "classDigikam_1_1MetadataPage" ]
    ] ],
    [ "migratefromdigikam4page.cpp", "migratefromdigikam4page_8cpp.html", null ],
    [ "migratefromdigikam4page.h", "migratefromdigikam4page_8h.html", [
      [ "MigrateFromDigikam4Page", "classDigikam_1_1MigrateFromDigikam4Page.html", "classDigikam_1_1MigrateFromDigikam4Page" ]
    ] ],
    [ "openfilepage.cpp", "openfilepage_8cpp.html", null ],
    [ "openfilepage.h", "openfilepage_8h.html", [
      [ "OpenFilePage", "classDigikam_1_1OpenFilePage.html", "classDigikam_1_1OpenFilePage" ]
    ] ],
    [ "previewpage.cpp", "previewpage_8cpp.html", null ],
    [ "previewpage.h", "previewpage_8h.html", [
      [ "PreviewPage", "classDigikam_1_1PreviewPage.html", "classDigikam_1_1PreviewPage" ]
    ] ],
    [ "rawpage.cpp", "rawpage_8cpp.html", null ],
    [ "rawpage.h", "rawpage_8h.html", [
      [ "RawPage", "classDigikam_1_1RawPage.html", "classDigikam_1_1RawPage" ]
    ] ],
    [ "startscanpage.cpp", "startscanpage_8cpp.html", null ],
    [ "startscanpage.h", "startscanpage_8h.html", [
      [ "StartScanPage", "classDigikam_1_1StartScanPage.html", "classDigikam_1_1StartScanPage" ]
    ] ],
    [ "tooltipspage.cpp", "tooltipspage_8cpp.html", null ],
    [ "tooltipspage.h", "tooltipspage_8h.html", [
      [ "TooltipsPage", "classDigikam_1_1TooltipsPage.html", "classDigikam_1_1TooltipsPage" ]
    ] ],
    [ "welcomepage.cpp", "welcomepage_8cpp.html", null ],
    [ "welcomepage.h", "welcomepage_8h.html", [
      [ "WelcomePage", "classDigikam_1_1WelcomePage.html", "classDigikam_1_1WelcomePage" ]
    ] ]
];