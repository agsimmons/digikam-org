var classDigikamGenericGoogleServicesPlugin_1_1GSPhoto =
[
    [ "GSPhoto", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#aac62019cd964ca5062ab2632394d0349", null ],
    [ "baseUrl", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a83886adc32da6ab23173b77cb39a70d1", null ],
    [ "canComment", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a312692c4f16a7641a75e149d5e9efb14", null ],
    [ "creationTime", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a25ef14c4d130f6af46e23b04cfe5ef05", null ],
    [ "description", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a997e6fd066abe2fc06a50316df1b6d79", null ],
    [ "editUrl", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#af80f3649b6ce231eac8092e7f19a064d", null ],
    [ "gpsLat", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a2d384f425cc4ec7678c4ac6b0c32b0a4", null ],
    [ "gpsLon", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a1a9c557af659970c0f1cda59d52af002", null ],
    [ "height", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#afe200fca0fa2f7fbaabb1adf3cb69ae2", null ],
    [ "id", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a0d3abafc9fbdc4df1be2addcf7d96058", null ],
    [ "location", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#abdab967bb3cb3eb4a9c79fc028046844", null ],
    [ "mimeType", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#ab369c99daabdb9eb989325cb356ebcc6", null ],
    [ "originalURL", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#af47de881220849d360f962c3b82716d0", null ],
    [ "tags", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a437b9e79f9a3548be8f7e53fb7266a82", null ],
    [ "thumbURL", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a14d306bfa2ffd72f82e9349045394709", null ],
    [ "timestamp", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#ad496efa86a754908d75c2bc26031286b", null ],
    [ "title", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a6eda2a47aca307cfa2b9e047c6755756", null ],
    [ "width", "classDigikamGenericGoogleServicesPlugin_1_1GSPhoto.html#a130f9b74b66316f6071c80947f2ad5d2", null ]
];