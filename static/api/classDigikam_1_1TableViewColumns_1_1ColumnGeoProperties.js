var classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties =
[
    [ "ColumnCompareResult", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a0714c73efa682e4bcc6dac00989cabd7", [
      [ "CmpEqual", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a0714c73efa682e4bcc6dac00989cabd7ab1fd7950c9141af71b6d915d7619da20", null ],
      [ "CmpABiggerB", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a0714c73efa682e4bcc6dac00989cabd7a3b46fa13fd837bb5e8303e15579e9c0a", null ],
      [ "CmpALessB", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a0714c73efa682e4bcc6dac00989cabd7af64727fba2a786f83c4032b9ac4e2ac7", null ]
    ] ],
    [ "ColumnFlag", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4", [
      [ "ColumnNoFlags", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a3a1867e93424ceda7439df444b42b7a8", null ],
      [ "ColumnCustomPainting", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a2496490a69825be1607d673758561fea", null ],
      [ "ColumnCustomSorting", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4ab775074b18540b93dbbe923cc7977b0c", null ],
      [ "ColumnHasConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c", null ]
    ] ],
    [ "SubColumn", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a80360bb64e297f2a5dac17eda7229a8b", [
      [ "SubColumnHasCoordinates", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a80360bb64e297f2a5dac17eda7229a8bac6bd43c821aa069b622d0526709ed094", null ],
      [ "SubColumnCoordinates", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a80360bb64e297f2a5dac17eda7229a8ba6a757fca4fa12b613096edd342801446", null ],
      [ "SubColumnAltitude", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a80360bb64e297f2a5dac17eda7229a8ba7e676e6b655ceae060f258e20fc088ca", null ]
    ] ],
    [ "ColumnGeoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#aa37b8bf265920de6b8a62732eca33307", null ],
    [ "~ColumnGeoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#aed593022838060e4904e26bafa7c54e6", null ],
    [ "columnAffectedByChangeset", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ac275f599c98cde40ce903a5de6aaf6f4", null ],
    [ "compare", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ae66150b7163d8be8c3a904c342805d3c", null ],
    [ "data", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a5b8f211bd812677956ff62874599cb99", null ],
    [ "getColumnFlags", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a00fc877db801891533f28c673f3c3667", null ],
    [ "getConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ace668cd97cd6d0b584332b803a0ec665", null ],
    [ "getConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#aab039223998f02ff469862f6062ea77f", null ],
    [ "getTitle", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a23beb61f5bf713700511daa47656af27", null ],
    [ "paint", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ae53896a52f61680c4263e571e19eec7f", null ],
    [ "setConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a9c669dc0dfec16feae03a0ed86372f7a", null ],
    [ "signalAllDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ab288b91b167fc9f40490e3d41dee38ee", null ],
    [ "signalDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ac52d1ec1e5839d96de9e1b365582fdfc", null ],
    [ "sizeHint", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a3a5f8ab59b570eb69d51b0d01c452190", null ],
    [ "updateThumbnailSize", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#ac4696688718ef915e4fb096ed8a2efe3", null ],
    [ "configuration", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a1e0c6be1da4fa29ddecaf0bc07a87a37", null ],
    [ "s", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a90a53ac037c5230322f608a687680efa", null ],
    [ "subColumn", "classDigikam_1_1TableViewColumns_1_1ColumnGeoProperties.html#a5601f8ac4cc77736a35787abc9e977e5", null ]
];