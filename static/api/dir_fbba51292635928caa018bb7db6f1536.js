var dir_fbba51292635928caa018bb7db6f1536 =
[
    [ "findduplicatesalbum.cpp", "findduplicatesalbum_8cpp.html", null ],
    [ "findduplicatesalbum.h", "findduplicatesalbum_8h.html", [
      [ "FindDuplicatesAlbum", "classDigikam_1_1FindDuplicatesAlbum.html", "classDigikam_1_1FindDuplicatesAlbum" ]
    ] ],
    [ "findduplicatesalbumitem.cpp", "findduplicatesalbumitem_8cpp.html", null ],
    [ "findduplicatesalbumitem.h", "findduplicatesalbumitem_8h.html", [
      [ "FindDuplicatesAlbumItem", "classDigikam_1_1FindDuplicatesAlbumItem.html", "classDigikam_1_1FindDuplicatesAlbumItem" ]
    ] ],
    [ "findduplicatesview.cpp", "findduplicatesview_8cpp.html", null ],
    [ "findduplicatesview.h", "findduplicatesview_8h.html", [
      [ "FindDuplicatesView", "classDigikam_1_1FindDuplicatesView.html", "classDigikam_1_1FindDuplicatesView" ]
    ] ],
    [ "fuzzysearchview.cpp", "fuzzysearchview_8cpp.html", null ],
    [ "fuzzysearchview.h", "fuzzysearchview_8h.html", [
      [ "FuzzySearchView", "classDigikam_1_1FuzzySearchView.html", "classDigikam_1_1FuzzySearchView" ]
    ] ],
    [ "fuzzysearchview_p.h", "fuzzysearchview__p_8h.html", [
      [ "Private", "classDigikam_1_1FuzzySearchView_1_1Private.html", "classDigikam_1_1FuzzySearchView_1_1Private" ]
    ] ],
    [ "fuzzysearchview_similar.cpp", "fuzzysearchview__similar_8cpp.html", null ],
    [ "fuzzysearchview_sketch.cpp", "fuzzysearchview__sketch_8cpp.html", null ],
    [ "sketchwidget.cpp", "sketchwidget_8cpp.html", null ],
    [ "sketchwidget.h", "sketchwidget_8h.html", [
      [ "SketchWidget", "classDigikam_1_1SketchWidget.html", "classDigikam_1_1SketchWidget" ]
    ] ]
];