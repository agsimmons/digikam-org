var classShowFoto_1_1ShowfotoStackViewSideBar =
[
    [ "StateSavingDepth", "classShowFoto_1_1ShowfotoStackViewSideBar.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classShowFoto_1_1ShowfotoStackViewSideBar.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classShowFoto_1_1ShowfotoStackViewSideBar.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classShowFoto_1_1ShowfotoStackViewSideBar.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "ShowfotoStackViewSideBar", "classShowFoto_1_1ShowfotoStackViewSideBar.html#ac353142f34207cca5fe840b70d952e85", null ],
    [ "~ShowfotoStackViewSideBar", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a2d274b843024cf97093caab245dc26b3", null ],
    [ "currentUrl", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a5d299c6832858e420a26ac58acab6d70", null ],
    [ "doLoadState", "classShowFoto_1_1ShowfotoStackViewSideBar.html#ace860aa35d9c572e3a4a168e593bc79f", null ],
    [ "doSaveState", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a0846a0d8c6924b98bd12741f41de185a", null ],
    [ "entryName", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getCaption", "classShowFoto_1_1ShowfotoStackViewSideBar.html#ada9b4b0ba87bb565719cb065de9a7b02", null ],
    [ "getConfigGroup", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getIcon", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a764f3682d32e7cc66c6fc25d00e897ec", null ],
    [ "getStateSavingDepth", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "iconSize", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a58686541d7a64b7e67190c4306bd029e", null ],
    [ "loadState", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "pluginActions", "classShowFoto_1_1ShowfotoStackViewSideBar.html#ae87eb33551883ad04bb087abe651b65e", null ],
    [ "registerPluginActions", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a108f4a89114509cbdc3f9c8884a076bc", null ],
    [ "saveState", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setConfigGroup", "classShowFoto_1_1ShowfotoStackViewSideBar.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classShowFoto_1_1ShowfotoStackViewSideBar.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setSortOrder", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a1b4c500e9652dd9aab71d7003da3f96b", null ],
    [ "setSortRole", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a791fb40c01022e8757fcefd138ce403e", null ],
    [ "setStateSavingDepth", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "setThumbbar", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a7f76e643fde64eaa668decc6a4430449", null ],
    [ "signalAddFavorite", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a78101d4c6c38069970f857e1924f81ad", null ],
    [ "signalClearItemsList", "classShowFoto_1_1ShowfotoStackViewSideBar.html#afabf755478b81046497d59f535e36c84", null ],
    [ "signalLoadContentsFromFiles", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a1254b170c7e14cf5fff7a15a21da1e4c", null ],
    [ "signalRemoveItemInfos", "classShowFoto_1_1ShowfotoStackViewSideBar.html#ab1c161a4799a21dc1e88d20253c344c7", null ],
    [ "signalShowfotoItemInfoActivated", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a918c1337231dbe7ef9dae1273ccaaee4", null ],
    [ "sortOrder", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a02a21f1a0bbe8c8ff4e37b49eb00954b", null ],
    [ "sortRole", "classShowFoto_1_1ShowfotoStackViewSideBar.html#aca8e24dae598451f641c40e2d2237038", null ],
    [ "urls", "classShowFoto_1_1ShowfotoStackViewSideBar.html#a124ae9a28d87cb39b1a5ce6fddc21953", null ]
];