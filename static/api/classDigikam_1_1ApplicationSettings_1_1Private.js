var classDigikam_1_1ApplicationSettings_1_1Private =
[
    [ "Private", "classDigikam_1_1ApplicationSettings_1_1Private.html#ac9575310dc1429877110321d1a97aa17", null ],
    [ "~Private", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae2efb71bb056979c75b829cef9b365b8", null ],
    [ "init", "classDigikam_1_1ApplicationSettings_1_1Private.html#aa4d89b4edb15b3a6ed43d30a7def335e", null ],
    [ "albumCategoryNames", "classDigikam_1_1ApplicationSettings_1_1Private.html#afd878f2d20fc40e810e844d5edda2423", null ],
    [ "albumMonitoring", "classDigikam_1_1ApplicationSettings_1_1Private.html#ac0addf8cb80ed12746fd9441d144e76c", null ],
    [ "albumSortChanged", "classDigikam_1_1ApplicationSettings_1_1Private.html#a2f8069aea996baf29b119d5da5d392d1", null ],
    [ "albumSortRole", "classDigikam_1_1ApplicationSettings_1_1Private.html#a74c9be0922bae4ad6137e5f6933522c2", null ],
    [ "allGroupsOpen", "classDigikam_1_1ApplicationSettings_1_1Private.html#a48fe61fbd7347485efa4ffbabbc0bdd5", null ],
    [ "applicationFont", "classDigikam_1_1ApplicationSettings_1_1Private.html#a23420f4a6b72dbded43fc034fa47775d", null ],
    [ "applicationIcon", "classDigikam_1_1ApplicationSettings_1_1Private.html#a12ee168cc379c5a9cee683dca8f32e04", null ],
    [ "applicationStyle", "classDigikam_1_1ApplicationSettings_1_1Private.html#a21e8b593a2777bf2c3c50ea79b73565e", null ],
    [ "cleanAtStart", "classDigikam_1_1ApplicationSettings_1_1Private.html#af2d2e117bd62f3a8118cfbc548c02390", null ],
    [ "config", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae48b35e7757d27743764bf4129e5cd75", null ],
    [ "currentTheme", "classDigikam_1_1ApplicationSettings_1_1Private.html#a13b7115dd3556f4872bb56ee26aae975", null ],
    [ "databaseDirSetAtCmd", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae120727fb798782f9dcc20621bc3dab8", null ],
    [ "databaseParams", "classDigikam_1_1ApplicationSettings_1_1Private.html#a736b139a55e07e1b8954b031e7eb85be", null ],
    [ "detectFacesInNewImages", "classDigikam_1_1ApplicationSettings_1_1Private.html#afd798233d35e572daf817bd963b59397", null ],
    [ "drawFramesToGrouped", "classDigikam_1_1ApplicationSettings_1_1Private.html#a471bb90cb406f1be5935bb0c171b6749", null ],
    [ "duplicatesSearchLastAlbumTagRelation", "classDigikam_1_1ApplicationSettings_1_1Private.html#a091219b61fcfdcc89859f18af12c0eff", null ],
    [ "duplicatesSearchLastMaxSimilarity", "classDigikam_1_1ApplicationSettings_1_1Private.html#a3f43f966800332a4d27cd0aaf8d12ceb", null ],
    [ "duplicatesSearchLastMinSimilarity", "classDigikam_1_1ApplicationSettings_1_1Private.html#a379dde3c14388ab5995453382ece5ed8", null ],
    [ "duplicatesSearchLastRestrictions", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae8f4ff8f3681e40230e3684c87fab3e4", null ],
    [ "expandNewCurrentItem", "classDigikam_1_1ApplicationSettings_1_1Private.html#adc75ca18778493c5aa2fbbc4b35cfc17", null ],
    [ "faceDetectionAccuracy", "classDigikam_1_1ApplicationSettings_1_1Private.html#a3bdd492290e16f125855fac502132760", null ],
    [ "faceDetectionYoloV3", "classDigikam_1_1ApplicationSettings_1_1Private.html#a931001c919ac3c663a8debb5e8253a6e", null ],
    [ "groupingOperateOnAll", "classDigikam_1_1ApplicationSettings_1_1Private.html#a6ec6430d5323fc2061ff3986609e7c40", null ],
    [ "iconShowAspectRatio", "classDigikam_1_1ApplicationSettings_1_1Private.html#a0bbc370fdeba06cc8ee923bb96b00fc8", null ],
    [ "iconShowComments", "classDigikam_1_1ApplicationSettings_1_1Private.html#a84085c6cc8da2d988d3244febf4340a7", null ],
    [ "iconShowCoordinates", "classDigikam_1_1ApplicationSettings_1_1Private.html#a86eb9f96df648aa38e86875c18820fea", null ],
    [ "iconShowDate", "classDigikam_1_1ApplicationSettings_1_1Private.html#a85cd7232a8246785ec3d39760ad60b6e", null ],
    [ "iconShowFullscreen", "classDigikam_1_1ApplicationSettings_1_1Private.html#a012002ae44d348780cfee6cb1018957b", null ],
    [ "iconShowImageFormat", "classDigikam_1_1ApplicationSettings_1_1Private.html#adf8fc44fc19eb9a70dc91d29cb3fc465", null ],
    [ "iconShowModDate", "classDigikam_1_1ApplicationSettings_1_1Private.html#a5e835f7f2af62a6f0f62f0b9ceef8c2b", null ],
    [ "iconShowName", "classDigikam_1_1ApplicationSettings_1_1Private.html#a34419e2d3b720b6e3df58a0d89de8254", null ],
    [ "iconShowOverlays", "classDigikam_1_1ApplicationSettings_1_1Private.html#a9424ca11d66516980f794389a74cab5f", null ],
    [ "iconShowRating", "classDigikam_1_1ApplicationSettings_1_1Private.html#a42240d0982366572a768344ef5df53aa", null ],
    [ "iconShowResolution", "classDigikam_1_1ApplicationSettings_1_1Private.html#a28d0a3d728dfe27fce13359494b4e33b", null ],
    [ "iconShowSize", "classDigikam_1_1ApplicationSettings_1_1Private.html#a53fd51516e70eb0bc55bac8cb58a8cd3", null ],
    [ "iconShowTags", "classDigikam_1_1ApplicationSettings_1_1Private.html#a94d3e99d943087a73345ab1b756b4fdb", null ],
    [ "iconShowTitle", "classDigikam_1_1ApplicationSettings_1_1Private.html#a9c1cb34fbcb911d5adf8468f37c88dbd", null ],
    [ "iconviewFont", "classDigikam_1_1ApplicationSettings_1_1Private.html#a13a3b9ba18a628474eb018c5b450379d", null ],
    [ "imageSeparationMode", "classDigikam_1_1ApplicationSettings_1_1Private.html#a43c3bcb9fa788f9cbc14c57008e7cdca", null ],
    [ "imageSeparationSortOrder", "classDigikam_1_1ApplicationSettings_1_1Private.html#a5edf6b86ca4a90d59287bfb02bd14e2a", null ],
    [ "imageSorting", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae0fd2690ef2f6292a8e56c833e2d35c9", null ],
    [ "imageSortOrder", "classDigikam_1_1ApplicationSettings_1_1Private.html#a250587f73c7e01eb8ab4384237c307b8", null ],
    [ "itemLeftClickAction", "classDigikam_1_1ApplicationSettings_1_1Private.html#a6ee6e538b0113d8c80b121b8ab4806ae", null ],
    [ "minimumSimilarityBound", "classDigikam_1_1ApplicationSettings_1_1Private.html#ab4910762ce59925464189ce6a6522347", null ],
    [ "previewSettings", "classDigikam_1_1ApplicationSettings_1_1Private.html#a75fd4c06bca336675b1fd7c463c05ea5", null ],
    [ "previewShowIcons", "classDigikam_1_1ApplicationSettings_1_1Private.html#a589dc3b68b8839f6eaf30cef98a2e3cf", null ],
    [ "ratingFilterCond", "classDigikam_1_1ApplicationSettings_1_1Private.html#aaecb8d7fe2c675d01c13c290bd819b28", null ],
    [ "recursiveAlbums", "classDigikam_1_1ApplicationSettings_1_1Private.html#a9da6e0511973e22274a52f716b2f40bb", null ],
    [ "recursiveTags", "classDigikam_1_1ApplicationSettings_1_1Private.html#a6c757e25c11fd9adf89f59e64f6906f2", null ],
    [ "scanAtStart", "classDigikam_1_1ApplicationSettings_1_1Private.html#aab96c9fe9179ddb8e7fe6ab8bf50b04f", null ],
    [ "scrollItemToCenter", "classDigikam_1_1ApplicationSettings_1_1Private.html#a98dce5d61696dc9f659eea9ba16ca42c", null ],
    [ "showAlbumToolTips", "classDigikam_1_1ApplicationSettings_1_1Private.html#a4ad2ef412108900df861eadc7db0a2a5", null ],
    [ "showFolderTreeViewItemsCount", "classDigikam_1_1ApplicationSettings_1_1Private.html#a27b718d694cdeba3ef86cf75b29c8c87", null ],
    [ "showOnlyPersonTagsInPeopleSidebar", "classDigikam_1_1ApplicationSettings_1_1Private.html#a828a2f07393e661a6e5c5e986070f983", null ],
    [ "showPermanentDeleteDialog", "classDigikam_1_1ApplicationSettings_1_1Private.html#a05da81e24bd3e70a267602bfc40af0e2", null ],
    [ "showSplash", "classDigikam_1_1ApplicationSettings_1_1Private.html#ab35fb83b2c8357a239edf9aeee5fa5ce", null ],
    [ "showThumbbar", "classDigikam_1_1ApplicationSettings_1_1Private.html#a05800f31164b3d55277fefc474e8a1a9", null ],
    [ "showToolTips", "classDigikam_1_1ApplicationSettings_1_1Private.html#a4a53e1b05bf85a93b7210d187355383c", null ],
    [ "showTrashDeleteDialog", "classDigikam_1_1ApplicationSettings_1_1Private.html#abe6f62842e59f4dcef5ddaaabbf344cb", null ],
    [ "sidebarApplyDirectly", "classDigikam_1_1ApplicationSettings_1_1Private.html#a02026b37b1d6f755a87c50bdc127c2bc", null ],
    [ "sidebarTitleStyle", "classDigikam_1_1ApplicationSettings_1_1Private.html#a3f35783b0201ceda0de2f331da781da7", null ],
    [ "stringComparisonType", "classDigikam_1_1ApplicationSettings_1_1Private.html#af59a1753c38bba37cc707b7fae2d804c", null ],
    [ "syncToBaloo", "classDigikam_1_1ApplicationSettings_1_1Private.html#a362e488babaeaa2065c927945bd43f14", null ],
    [ "syncToDigikam", "classDigikam_1_1ApplicationSettings_1_1Private.html#ada4a39a3014cb5e0a601364ac4f57d61", null ],
    [ "thumbnailSize", "classDigikam_1_1ApplicationSettings_1_1Private.html#a11914e165c94832daf6c712f2c5fa82c", null ],
    [ "toolTipsFont", "classDigikam_1_1ApplicationSettings_1_1Private.html#a1140864d80867be788c64bda514079e9", null ],
    [ "tooltipShowAlbumCaption", "classDigikam_1_1ApplicationSettings_1_1Private.html#a61f29e895cb8b27f3010b299e4e347e5", null ],
    [ "tooltipShowAlbumCategory", "classDigikam_1_1ApplicationSettings_1_1Private.html#ad5fc5b7ac160450d28a81482ef8c6218", null ],
    [ "tooltipShowAlbumCollection", "classDigikam_1_1ApplicationSettings_1_1Private.html#afb37b5a04510883da22c0be8680466de", null ],
    [ "tooltipShowAlbumDate", "classDigikam_1_1ApplicationSettings_1_1Private.html#a39cdfe13e25b2c00fbb21e7f68ab283f", null ],
    [ "tooltipShowAlbumName", "classDigikam_1_1ApplicationSettings_1_1Private.html#a6cf1500f03666c11db4f01228559e913", null ],
    [ "tooltipShowAlbumPreview", "classDigikam_1_1ApplicationSettings_1_1Private.html#aae1923947b2827d46067fd0ba8470558", null ],
    [ "tooltipShowAlbumTitle", "classDigikam_1_1ApplicationSettings_1_1Private.html#a088eee0ef0f9ddac4cae12c92ad7d63d", null ],
    [ "tooltipShowComments", "classDigikam_1_1ApplicationSettings_1_1Private.html#a31f0bb2bb9e05237f8473625a2f14b97", null ],
    [ "tooltipShowFileDate", "classDigikam_1_1ApplicationSettings_1_1Private.html#ad0eb001792a206f01730dd177a60e90f", null ],
    [ "tooltipShowFileName", "classDigikam_1_1ApplicationSettings_1_1Private.html#a714456e1f12d0b9753bfca23cded7b9c", null ],
    [ "tooltipShowFileSize", "classDigikam_1_1ApplicationSettings_1_1Private.html#a017385b714f31efddac3999f9161ebc8", null ],
    [ "tooltipShowImageAR", "classDigikam_1_1ApplicationSettings_1_1Private.html#a8f8324ed892a92e03f2071204cd89dbb", null ],
    [ "tooltipShowImageDim", "classDigikam_1_1ApplicationSettings_1_1Private.html#a5a1906a39aaf8d51b55a3d3eec7d66c3", null ],
    [ "tooltipShowImageType", "classDigikam_1_1ApplicationSettings_1_1Private.html#aa12ede6e7746268031d814a2bef7e0f3", null ],
    [ "tooltipShowLabelRating", "classDigikam_1_1ApplicationSettings_1_1Private.html#a16332b877fcefdaa9fa64e35ae44ceef", null ],
    [ "tooltipShowPhotoDate", "classDigikam_1_1ApplicationSettings_1_1Private.html#a29298c875d709f5dd051b32531ad4c12", null ],
    [ "tooltipShowPhotoExpo", "classDigikam_1_1ApplicationSettings_1_1Private.html#a3781047ef244df6355dbf5f34de4d1b3", null ],
    [ "tooltipShowPhotoFlash", "classDigikam_1_1ApplicationSettings_1_1Private.html#a3acc081ee1876dfc914ed8ce34b692f1", null ],
    [ "tooltipShowPhotoFocal", "classDigikam_1_1ApplicationSettings_1_1Private.html#a699f8f2f6529a40f81bd3f6af64c3faf", null ],
    [ "tooltipShowPhotoLens", "classDigikam_1_1ApplicationSettings_1_1Private.html#ac000dec209db92c06a0c1383165c5ae0", null ],
    [ "tooltipShowPhotoMake", "classDigikam_1_1ApplicationSettings_1_1Private.html#ad9702b10fe303f5518ef80be004c9621", null ],
    [ "tooltipShowPhotoMode", "classDigikam_1_1ApplicationSettings_1_1Private.html#afca691c127a79560079fcd93ac6039ea", null ],
    [ "tooltipShowPhotoWb", "classDigikam_1_1ApplicationSettings_1_1Private.html#aaaf99ab8ec61c874947da831a88e7aee", null ],
    [ "tooltipShowTags", "classDigikam_1_1ApplicationSettings_1_1Private.html#af107d9d92a45644fb6b3d88fb8841c18", null ],
    [ "tooltipShowTitles", "classDigikam_1_1ApplicationSettings_1_1Private.html#aa3629e0cb0433a237f51bdf6a1d852a8", null ],
    [ "tooltipShowVideoAspectRatio", "classDigikam_1_1ApplicationSettings_1_1Private.html#a376ea2668c2e5c422a7f652a9fd3e35d", null ],
    [ "tooltipShowVideoAudioBitRate", "classDigikam_1_1ApplicationSettings_1_1Private.html#abdd424333afa54e80669f273af26ffd6", null ],
    [ "tooltipShowVideoAudioChannelType", "classDigikam_1_1ApplicationSettings_1_1Private.html#a34b3639d43944338f66ad2ae46b0012a", null ],
    [ "tooltipShowVideoAudioCodec", "classDigikam_1_1ApplicationSettings_1_1Private.html#a0785f94ae2ee207e5e969ffc37d410dc", null ],
    [ "tooltipShowVideoDuration", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae5a93f60a88f22fb1ab8dfddebecc1bf", null ],
    [ "tooltipShowVideoFrameRate", "classDigikam_1_1ApplicationSettings_1_1Private.html#aa14ab383c5ee273465616d5f1ee7e97c", null ],
    [ "tooltipShowVideoVideoCodec", "classDigikam_1_1ApplicationSettings_1_1Private.html#af3c3181fe3a7426a5a1823f247ab11f9", null ],
    [ "treeThumbFaceSize", "classDigikam_1_1ApplicationSettings_1_1Private.html#a7e9902a6993d23e587ba46d978dd3b02", null ],
    [ "treeThumbnailSize", "classDigikam_1_1ApplicationSettings_1_1Private.html#a4524579bced108b2865ccd4ff17358fe", null ],
    [ "treeviewFont", "classDigikam_1_1ApplicationSettings_1_1Private.html#a2c56be4be39c6c24af0cf94f5e6bd848", null ],
    [ "updateType", "classDigikam_1_1ApplicationSettings_1_1Private.html#a4dd48ec6cf3e5b671df65f59d07f14dd", null ],
    [ "updateWithDebug", "classDigikam_1_1ApplicationSettings_1_1Private.html#a0f2fe27869214f4c688fea80d5454545", null ],
    [ "useNativeFileDialog", "classDigikam_1_1ApplicationSettings_1_1Private.html#ae71f02775548bb409ff6983497e8a605", null ],
    [ "useTrash", "classDigikam_1_1ApplicationSettings_1_1Private.html#afc7e29e7c39ea7f5a896b94513121a03", null ],
    [ "versionSettings", "classDigikam_1_1ApplicationSettings_1_1Private.html#aea3286c76219f977ab38cf6ecabbe5de", null ]
];