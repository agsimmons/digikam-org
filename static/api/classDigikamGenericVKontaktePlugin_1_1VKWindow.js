var classDigikamGenericVKontaktePlugin_1_1VKWindow =
[
    [ "VKWindow", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a5fc009d577cea9a0a769802d4f87f5f4", null ],
    [ "~VKWindow", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a2fd29a2ada0b6310b782c59ba061340a", null ],
    [ "addButton", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "restoreDialogSize", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setMainWidget", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "signalUpdateBusyStatus", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#ae8fbf9c1dbc19c5f1794dbec90234e21", null ],
    [ "startButton", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "startReactivation", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a9cf652a5a159272f25663a403610d703", null ],
    [ "m_buttons", "classDigikamGenericVKontaktePlugin_1_1VKWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];