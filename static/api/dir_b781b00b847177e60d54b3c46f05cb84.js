var dir_b781b00b847177e60d54b3c46f05cb84 =
[
    [ "backend", "dir_dd53ce6525b4bd4612b20bb357441bef.html", "dir_dd53ce6525b4bd4612b20bb357441bef" ],
    [ "mediawikiplugin.cpp", "mediawikiplugin_8cpp.html", null ],
    [ "mediawikiplugin.h", "mediawikiplugin_8h.html", "mediawikiplugin_8h" ],
    [ "mediawikitalker.cpp", "mediawikitalker_8cpp.html", null ],
    [ "mediawikitalker.h", "mediawikitalker_8h.html", [
      [ "MediaWikiTalker", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiTalker.html", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiTalker" ]
    ] ],
    [ "mediawikiwidget.cpp", "mediawikiwidget_8cpp.html", null ],
    [ "mediawikiwidget.h", "mediawikiwidget_8h.html", "mediawikiwidget_8h" ],
    [ "mediawikiwindow.cpp", "mediawikiwindow_8cpp.html", null ],
    [ "mediawikiwindow.h", "mediawikiwindow_8h.html", [
      [ "MediaWikiWindow", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiWindow.html", "classDigikamGenericMediaWikiPlugin_1_1MediaWikiWindow" ]
    ] ]
];