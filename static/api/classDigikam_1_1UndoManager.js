var classDigikam_1_1UndoManager =
[
    [ "UndoManager", "classDigikam_1_1UndoManager.html#afa0f74ddebff8851658728ab8e79171f", null ],
    [ "~UndoManager", "classDigikam_1_1UndoManager.html#a14ad333ea472402ed1101a528480892d", null ],
    [ "addAction", "classDigikam_1_1UndoManager.html#a48fc8b50c83b3dec7315ad4685a39e70", null ],
    [ "anyMoreRedo", "classDigikam_1_1UndoManager.html#a395bfc524a5ad9d307aa729a2fe33405", null ],
    [ "anyMoreUndo", "classDigikam_1_1UndoManager.html#a2ab569268e5cce0aade71a6b0adfb71b", null ],
    [ "availableRedoSteps", "classDigikam_1_1UndoManager.html#a217f4abf82b650be25ddc31ec69e0cd9", null ],
    [ "availableUndoSteps", "classDigikam_1_1UndoManager.html#a9f90fddba49abd1d158cb0d69453af20", null ],
    [ "clear", "classDigikam_1_1UndoManager.html#abf33c2230af54a591c2c21f439366998", null ],
    [ "clearPreviousOriginData", "classDigikam_1_1UndoManager.html#a7559c9a34ee2501a8a954f15b91a1fa6", null ],
    [ "getImageHistoryOfFullRedo", "classDigikam_1_1UndoManager.html#a9eadff8233c8a8e0358b06defb2d3d05", null ],
    [ "getRedoHistory", "classDigikam_1_1UndoManager.html#a5aeae841643fafdf9ed0cf9ada5fb622", null ],
    [ "getUndoHistory", "classDigikam_1_1UndoManager.html#a2a58e5f3ce51a4684b75e5f537786074", null ],
    [ "hasChanges", "classDigikam_1_1UndoManager.html#ac968081d46a8c45f8d33863eb1c02ff9", null ],
    [ "isAtOrigin", "classDigikam_1_1UndoManager.html#a6f7fac66069e43602aa912eb99808edc", null ],
    [ "putImageDataAndHistory", "classDigikam_1_1UndoManager.html#a76e520b4856ab050b6626b2a985a7536", null ],
    [ "redo", "classDigikam_1_1UndoManager.html#a69be145c501fd025a01e2a373ee5fc51", null ],
    [ "rollbackToOrigin", "classDigikam_1_1UndoManager.html#a808d0061b5aeedf6a80906c4be7d0468", null ],
    [ "setOrigin", "classDigikam_1_1UndoManager.html#a3ae42f80dd4ccbc2a1bd89ddee4f14f3", null ],
    [ "undo", "classDigikam_1_1UndoManager.html#ad850e643573945b74911b105dff88753", null ]
];