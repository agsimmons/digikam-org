var motion_8h =
[
    [ "MotionVector", "classMotionVector.html", "classMotionVector" ],
    [ "MotionVectorAccess", "classMotionVectorAccess.html", "classMotionVectorAccess" ],
    [ "PBMotion", "classPBMotion.html", "classPBMotion" ],
    [ "PBMotionCoding", "classPBMotionCoding.html", "classPBMotionCoding" ],
    [ "decode_prediction_unit", "motion_8h.html#a9fd15c95faa7ed589a11578ed94507d7", null ],
    [ "fill_luma_motion_vector_predictors", "motion_8h.html#af512e9c0c62c423e23dffc44802a24e1", null ],
    [ "generate_inter_prediction_samples", "motion_8h.html#aa9e73df334da7355302aeab8969206c0", null ],
    [ "get_merge_candidate_list", "motion_8h.html#ae5fdb87a596c0657760e5f362c8bba46", null ],
    [ "get_merge_candidate_list_without_step_9", "motion_8h.html#a1c9d248ddfb0526cfa738524270189f6", null ]
];