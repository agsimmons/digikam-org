var classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog =
[
    [ "TimeAdjustDialog", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#a9a6c0577be8e9f231c47d9a06f940ba2", null ],
    [ "~TimeAdjustDialog", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#aea9cf1efa3e27ec13488eb6f8ea6fea5", null ],
    [ "closeEvent", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#ad3989a327114f15fa34b0f3184fde98b", null ],
    [ "restoreDialogSize", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setPlugin", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "m_buttons", "classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustDialog.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];