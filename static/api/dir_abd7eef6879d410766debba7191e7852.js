var dir_abd7eef6879d410766debba7191e7852 =
[
    [ "dwitemdelegate.cpp", "dwitemdelegate_8cpp.html", null ],
    [ "dwitemdelegate.h", "dwitemdelegate_8h.html", [
      [ "DWItemDelegate", "classDigikam_1_1DWItemDelegate.html", "classDigikam_1_1DWItemDelegate" ]
    ] ],
    [ "dwitemdelegate_p.cpp", "dwitemdelegate__p_8cpp.html", null ],
    [ "dwitemdelegate_p.h", "dwitemdelegate__p_8h.html", [
      [ "DWItemDelegatePrivate", "classDigikam_1_1DWItemDelegatePrivate.html", "classDigikam_1_1DWItemDelegatePrivate" ]
    ] ],
    [ "dwitemdelegatepool.cpp", "dwitemdelegatepool_8cpp.html", null ],
    [ "dwitemdelegatepool.h", "dwitemdelegatepool_8h.html", [
      [ "DWItemDelegatePool", "classDigikam_1_1DWItemDelegatePool.html", "classDigikam_1_1DWItemDelegatePool" ],
      [ "DWItemDelegatePoolPrivate", "classDigikam_1_1DWItemDelegatePoolPrivate.html", "classDigikam_1_1DWItemDelegatePoolPrivate" ]
    ] ],
    [ "setupcollections.cpp", "setupcollections_8cpp.html", null ],
    [ "setupcollections.h", "setupcollections_8h.html", [
      [ "SetupCollections", "classDigikam_1_1SetupCollections.html", "classDigikam_1_1SetupCollections" ]
    ] ],
    [ "setupcollectionview.cpp", "setupcollectionview_8cpp.html", "setupcollectionview_8cpp" ],
    [ "setupcollectionview.h", "setupcollectionview_8h.html", [
      [ "SetupCollectionDelegate", "classDigikam_1_1SetupCollectionDelegate.html", "classDigikam_1_1SetupCollectionDelegate" ],
      [ "SetupCollectionModel", "classDigikam_1_1SetupCollectionModel.html", "classDigikam_1_1SetupCollectionModel" ],
      [ "Item", "classDigikam_1_1SetupCollectionModel_1_1Item.html", "classDigikam_1_1SetupCollectionModel_1_1Item" ],
      [ "SetupCollectionTreeView", "classDigikam_1_1SetupCollectionTreeView.html", "classDigikam_1_1SetupCollectionTreeView" ]
    ] ]
];