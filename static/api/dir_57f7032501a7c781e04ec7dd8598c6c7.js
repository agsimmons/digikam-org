var dir_57f7032501a7c781e04ec7dd8598c6c7 =
[
    [ "gpsitemlistdragdrophandler.cpp", "gpsitemlistdragdrophandler_8cpp.html", null ],
    [ "gpsitemlistdragdrophandler.h", "gpsitemlistdragdrophandler_8h.html", [
      [ "GPSItemListDragDropHandler", "classDigikam_1_1GPSItemListDragDropHandler.html", "classDigikam_1_1GPSItemListDragDropHandler" ],
      [ "ItemListDragDropHandler", "classDigikam_1_1ItemListDragDropHandler.html", "classDigikam_1_1ItemListDragDropHandler" ]
    ] ],
    [ "mapdragdrophandler.cpp", "mapdragdrophandler_8cpp.html", null ],
    [ "mapdragdrophandler.h", "mapdragdrophandler_8h.html", [
      [ "MapDragData", "classDigikam_1_1MapDragData.html", "classDigikam_1_1MapDragData" ],
      [ "MapDragDropHandler", "classDigikam_1_1MapDragDropHandler.html", "classDigikam_1_1MapDragDropHandler" ]
    ] ]
];