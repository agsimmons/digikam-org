var classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties =
[
    [ "ColumnCompareResult", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a0714c73efa682e4bcc6dac00989cabd7", [
      [ "CmpEqual", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a0714c73efa682e4bcc6dac00989cabd7ab1fd7950c9141af71b6d915d7619da20", null ],
      [ "CmpABiggerB", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a0714c73efa682e4bcc6dac00989cabd7a3b46fa13fd837bb5e8303e15579e9c0a", null ],
      [ "CmpALessB", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a0714c73efa682e4bcc6dac00989cabd7af64727fba2a786f83c4032b9ac4e2ac7", null ]
    ] ],
    [ "ColumnFlag", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4", [
      [ "ColumnNoFlags", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a3a1867e93424ceda7439df444b42b7a8", null ],
      [ "ColumnCustomPainting", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a2496490a69825be1607d673758561fea", null ],
      [ "ColumnCustomSorting", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4ab775074b18540b93dbbe923cc7977b0c", null ],
      [ "ColumnHasConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c", null ]
    ] ],
    [ "SubColumn", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4e", [
      [ "SubColumnAudioBitRate", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4eaa208f0f0fb9736c3fab98ba201867e0c", null ],
      [ "SubColumnAudioChannelType", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4eaa269291a54a683164f964bfea44f7319", null ],
      [ "SubColumnAudioCodec", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4eab0193c799552d0d5c416be4206c9930e", null ],
      [ "SubColumnDuration", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4ead30fcfd881ec7a771485b6234241c964", null ],
      [ "SubColumnFrameRate", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4ea14aad5ceac278ebd1957b9e4cc2a6d0f", null ],
      [ "SubColumnVideoCodec", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4f271ddb73efc5b5407fc42585e5ed4ea7a6b86471912b9ef37f756a7bc3468af", null ]
    ] ],
    [ "ColumnAudioVideoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a4a4068ce98194f6f8418d4d03f41ac5c", null ],
    [ "~ColumnAudioVideoProperties", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ae46dc09ee9376056707d55563e8f8fb8", null ],
    [ "columnAffectedByChangeset", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ac275f599c98cde40ce903a5de6aaf6f4", null ],
    [ "compare", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a019c8da5db2e64ff3f17895095d84206", null ],
    [ "data", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a6b4ac28b82f714c90eb31c81552946ab", null ],
    [ "getColumnFlags", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a7954bc6cbc719f3d61212ef04007bcee", null ],
    [ "getConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ace668cd97cd6d0b584332b803a0ec665", null ],
    [ "getConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a70ac95911d3bf9ad063cdaed7203a7cd", null ],
    [ "getTitle", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a927e0c088dabcbb8de3ce92b00424182", null ],
    [ "paint", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ae53896a52f61680c4263e571e19eec7f", null ],
    [ "setConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a05c3fb5e8fe8bd6f00c238491bda6256", null ],
    [ "signalAllDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ab288b91b167fc9f40490e3d41dee38ee", null ],
    [ "signalDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ac52d1ec1e5839d96de9e1b365582fdfc", null ],
    [ "sizeHint", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a3a5f8ab59b570eb69d51b0d01c452190", null ],
    [ "updateThumbnailSize", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#ac4696688718ef915e4fb096ed8a2efe3", null ],
    [ "configuration", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a1e0c6be1da4fa29ddecaf0bc07a87a37", null ],
    [ "s", "classDigikam_1_1TableViewColumns_1_1ColumnAudioVideoProperties.html#a90a53ac037c5230322f608a687680efa", null ]
];