var classDigikam_1_1BorderSettings =
[
    [ "BorderSettings", "classDigikam_1_1BorderSettings.html#aea8153e36b889c36e12c56d02bd9974a", null ],
    [ "~BorderSettings", "classDigikam_1_1BorderSettings.html#aa81b3cb9ec4d5cce67d23c58ef10b24e", null ],
    [ "defaultSettings", "classDigikam_1_1BorderSettings.html#a200b137668e56323ec2bfd9ec57b72df", null ],
    [ "readSettings", "classDigikam_1_1BorderSettings.html#a15cd2a539a0e49d02751ad5cfeca0ba6", null ],
    [ "resetToDefault", "classDigikam_1_1BorderSettings.html#aafae16ffcbf5d97fe3800a19ba805189", null ],
    [ "setSettings", "classDigikam_1_1BorderSettings.html#a93491b741b032a6e7513ae0bf0c96409", null ],
    [ "settings", "classDigikam_1_1BorderSettings.html#af523700d749816c0e20bcc7e9c201465", null ],
    [ "signalSettingsChanged", "classDigikam_1_1BorderSettings.html#a8385b2d2063d939414cffb4c931ec658", null ],
    [ "writeSettings", "classDigikam_1_1BorderSettings.html#a475db6bcf2d6569e5cabd218c2527e67", null ]
];