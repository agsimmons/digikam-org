var classDigikam_1_1BWSepiaSettings =
[
    [ "BWSepiaSettings", "classDigikam_1_1BWSepiaSettings.html#a506f4b39a9d94c6f6f4d4b1ce0f839ca", null ],
    [ "~BWSepiaSettings", "classDigikam_1_1BWSepiaSettings.html#a647d9ee083c9586273a5a5d182686209", null ],
    [ "defaultSettings", "classDigikam_1_1BWSepiaSettings.html#a03c7dcaf745b675e42eb12b7e8ce8aa5", null ],
    [ "loadSettings", "classDigikam_1_1BWSepiaSettings.html#acdb0c2ecf8283a61c3ea54468b41f7df", null ],
    [ "readSettings", "classDigikam_1_1BWSepiaSettings.html#a353444508a3fabd6e8170e8eb708b315", null ],
    [ "resetToDefault", "classDigikam_1_1BWSepiaSettings.html#af70bdd11a9728eb3158f8fb4ef4b05a2", null ],
    [ "saveAsSettings", "classDigikam_1_1BWSepiaSettings.html#a8fd21e2444e379a680bcb40b6f5de580", null ],
    [ "setScaleType", "classDigikam_1_1BWSepiaSettings.html#a3821ae85d0ade048c2176b27eb1344c2", null ],
    [ "setSettings", "classDigikam_1_1BWSepiaSettings.html#a0fbdc1297e5eb958f64ed35d364f7cb7", null ],
    [ "settings", "classDigikam_1_1BWSepiaSettings.html#a408b614ec6bbf9200007c651c16e2544", null ],
    [ "signalSettingsChanged", "classDigikam_1_1BWSepiaSettings.html#a146849eb62cf1d260dbd418005781e0c", null ],
    [ "startPreviewFilters", "classDigikam_1_1BWSepiaSettings.html#aacda8f485e03f3fe0fdc0cc9036a6072", null ],
    [ "writeSettings", "classDigikam_1_1BWSepiaSettings.html#ad86837e385753cb917f15ec8b4cf5211", null ]
];