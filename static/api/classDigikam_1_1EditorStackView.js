var classDigikam_1_1EditorStackView =
[
    [ "StackViewMode", "classDigikam_1_1EditorStackView.html#ab7d781d65f8b4fded7694e19b3072c03", [
      [ "CanvasMode", "classDigikam_1_1EditorStackView.html#ab7d781d65f8b4fded7694e19b3072c03a7319fa12d4e0a5476ac01565130e47ba", null ],
      [ "ToolViewMode", "classDigikam_1_1EditorStackView.html#ab7d781d65f8b4fded7694e19b3072c03aeeb6d19666faf6e87baaba0f9d2a3f4b", null ]
    ] ],
    [ "EditorStackView", "classDigikam_1_1EditorStackView.html#ab4c6da57b39325c44e6b48e3f2cc9b5b", null ],
    [ "~EditorStackView", "classDigikam_1_1EditorStackView.html#ac147e16a3857b851a838708c65ab2997", null ],
    [ "canvas", "classDigikam_1_1EditorStackView.html#af970abb92c031e52d0d5272cd6b36308", null ],
    [ "decreaseZoom", "classDigikam_1_1EditorStackView.html#a64d598fa275e5ae018538e4698186e09", null ],
    [ "fitToSelect", "classDigikam_1_1EditorStackView.html#a21729ba7297b93edebddb4cfb8cdc16a", null ],
    [ "increaseZoom", "classDigikam_1_1EditorStackView.html#abff207832e0cf2876830ac670b09abc3", null ],
    [ "isZoomablePreview", "classDigikam_1_1EditorStackView.html#aae315cfc942b60fbb868baab7718bba9", null ],
    [ "setCanvas", "classDigikam_1_1EditorStackView.html#a893403348b2ab0ebad0d35a57af0a415", null ],
    [ "setToolView", "classDigikam_1_1EditorStackView.html#a8c848b2408c5d1bcfdb3259f633e8317", null ],
    [ "setViewMode", "classDigikam_1_1EditorStackView.html#ae7be0cf012172ada6c905b40546561d2", null ],
    [ "setZoomFactor", "classDigikam_1_1EditorStackView.html#a1a116d30280feef384d78350a26beba7", null ],
    [ "signalToggleOffFitToWindow", "classDigikam_1_1EditorStackView.html#ae9eb41c2fcb454acdc4dabe7f37cf6d3", null ],
    [ "signalZoomChanged", "classDigikam_1_1EditorStackView.html#accb23a77e8b30c24f63e7967a5199fb7", null ],
    [ "slotZoomSliderChanged", "classDigikam_1_1EditorStackView.html#aa8ba250daf6d7626b1e8c13463826ed8", null ],
    [ "toggleFitToWindow", "classDigikam_1_1EditorStackView.html#ae1f2b153a890f8a49efc66e77efbd2ed", null ],
    [ "toolView", "classDigikam_1_1EditorStackView.html#a0ecfff65c6a076524c85eb1856543e81", null ],
    [ "viewMode", "classDigikam_1_1EditorStackView.html#a78d441e8217f06cd7931af6c143fcc77", null ],
    [ "zoomMax", "classDigikam_1_1EditorStackView.html#a1df3790e91bd490b5ad624cbf545272d", null ],
    [ "zoomMin", "classDigikam_1_1EditorStackView.html#a24eb008a15dc807c64b5944bf898a346", null ],
    [ "zoomTo100Percent", "classDigikam_1_1EditorStackView.html#a26f00f316345e6ce3438ba2e1a9d8d2d", null ]
];