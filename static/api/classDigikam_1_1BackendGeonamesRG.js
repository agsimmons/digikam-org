var classDigikam_1_1BackendGeonamesRG =
[
    [ "BackendGeonamesRG", "classDigikam_1_1BackendGeonamesRG.html#a3eff519cb5c0a35889a06affee1ef506", null ],
    [ "~BackendGeonamesRG", "classDigikam_1_1BackendGeonamesRG.html#a6e43d2be7f4abf3f8459e48d112c6e98", null ],
    [ "backendName", "classDigikam_1_1BackendGeonamesRG.html#a40668438fddfe9a510aac73f3387c2b1", null ],
    [ "callRGBackend", "classDigikam_1_1BackendGeonamesRG.html#ab591b809ae9294c88e1a83bcedea6d2a", null ],
    [ "cancelRequests", "classDigikam_1_1BackendGeonamesRG.html#ab6322cfea3e4ca4904fa573766b9171a", null ],
    [ "getErrorMessage", "classDigikam_1_1BackendGeonamesRG.html#a9d3d5657ad5544b12b54f1018671f148", null ],
    [ "makeQMapFromXML", "classDigikam_1_1BackendGeonamesRG.html#a9a31e3cc799563c23074d106a42bade1", null ],
    [ "signalRGReady", "classDigikam_1_1BackendGeonamesRG.html#a6ee2a6e36a160142ffa2fe82b07c4695", null ]
];