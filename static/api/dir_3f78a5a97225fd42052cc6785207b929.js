var dir_3f78a5a97225fd42052cc6785207b929 =
[
    [ "cietonguewidget.cpp", "cietonguewidget_8cpp.html", null ],
    [ "cietonguewidget.h", "cietonguewidget_8h.html", [
      [ "CIETongueWidget", "classDigikam_1_1CIETongueWidget.html", "classDigikam_1_1CIETongueWidget" ]
    ] ],
    [ "iccpreviewwidget.cpp", "iccpreviewwidget_8cpp.html", null ],
    [ "iccpreviewwidget.h", "iccpreviewwidget_8h.html", [
      [ "ICCPreviewWidget", "classDigikam_1_1ICCPreviewWidget.html", "classDigikam_1_1ICCPreviewWidget" ]
    ] ],
    [ "iccprofilescombobox.cpp", "iccprofilescombobox_8cpp.html", "iccprofilescombobox_8cpp" ],
    [ "iccprofilescombobox.h", "iccprofilescombobox_8h.html", [
      [ "IccProfilesComboBox", "classDigikam_1_1IccProfilesComboBox.html", "classDigikam_1_1IccProfilesComboBox" ],
      [ "IccProfilesMenuAction", "classDigikam_1_1IccProfilesMenuAction.html", "classDigikam_1_1IccProfilesMenuAction" ],
      [ "IccRenderingIntentComboBox", "classDigikam_1_1IccRenderingIntentComboBox.html", "classDigikam_1_1IccRenderingIntentComboBox" ]
    ] ],
    [ "iccprofilewidget.cpp", "iccprofilewidget_8cpp.html", "iccprofilewidget_8cpp" ],
    [ "iccprofilewidget.h", "iccprofilewidget_8h.html", [
      [ "ICCProfileWidget", "classDigikam_1_1ICCProfileWidget.html", "classDigikam_1_1ICCProfileWidget" ]
    ] ]
];