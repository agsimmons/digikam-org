var classDigikam_1_1DatabaseServerError =
[
    [ "DatabaseServerErrorEnum", "classDigikam_1_1DatabaseServerError.html#a6de91fbbed7a6263a1df259a830dad2e", [
      [ "NoErrors", "classDigikam_1_1DatabaseServerError.html#a6de91fbbed7a6263a1df259a830dad2ea20c00a526edad73040631b82cce4ad29", null ],
      [ "NotSupported", "classDigikam_1_1DatabaseServerError.html#a6de91fbbed7a6263a1df259a830dad2eabb933a3957083ab6b728019fe70a5017", null ],
      [ "StartError", "classDigikam_1_1DatabaseServerError.html#a6de91fbbed7a6263a1df259a830dad2ea9720eaa18ff75f24945412cdaf4c936b", null ]
    ] ],
    [ "DatabaseServerError", "classDigikam_1_1DatabaseServerError.html#a089fd3f1c79566a13b8f0f210c5017da", null ],
    [ "DatabaseServerError", "classDigikam_1_1DatabaseServerError.html#a65b5b8e23f0619c3290f2e2f2e704244", null ],
    [ "~DatabaseServerError", "classDigikam_1_1DatabaseServerError.html#a872e09c45ec064442f38adb256052790", null ],
    [ "getErrorText", "classDigikam_1_1DatabaseServerError.html#a81ccc085426de40cccf6425f068ba223", null ],
    [ "getErrorType", "classDigikam_1_1DatabaseServerError.html#aa411f9a77f075064269260ba5c624221", null ],
    [ "setErrorText", "classDigikam_1_1DatabaseServerError.html#a98d62872402ec1bb4e80caadf6044ef1", null ],
    [ "setErrorType", "classDigikam_1_1DatabaseServerError.html#a8e883a992ce2b31e1f38360a1d4d7786", null ]
];