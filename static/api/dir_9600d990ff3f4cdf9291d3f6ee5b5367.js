var dir_9600d990ff3f4cdf9291d3f6ee5b5367 =
[
    [ "curvesbox.cpp", "curvesbox_8cpp.html", null ],
    [ "curvesbox.h", "curvesbox_8h.html", [
      [ "CurvesBox", "classDigikam_1_1CurvesBox.html", "classDigikam_1_1CurvesBox" ]
    ] ],
    [ "curvescontainer.cpp", "curvescontainer_8cpp.html", null ],
    [ "curvescontainer.h", "curvescontainer_8h.html", [
      [ "CurvesContainer", "classDigikam_1_1CurvesContainer.html", "classDigikam_1_1CurvesContainer" ]
    ] ],
    [ "curvesfilter.cpp", "curvesfilter_8cpp.html", null ],
    [ "curvesfilter.h", "curvesfilter_8h.html", [
      [ "CurvesFilter", "classDigikam_1_1CurvesFilter.html", "classDigikam_1_1CurvesFilter" ]
    ] ],
    [ "curvessettings.cpp", "curvessettings_8cpp.html", null ],
    [ "curvessettings.h", "curvessettings_8h.html", [
      [ "CurvesSettings", "classDigikam_1_1CurvesSettings.html", "classDigikam_1_1CurvesSettings" ]
    ] ],
    [ "curveswidget.cpp", "curveswidget_8cpp.html", null ],
    [ "curveswidget.h", "curveswidget_8h.html", [
      [ "CurvesWidget", "classDigikam_1_1CurvesWidget.html", "classDigikam_1_1CurvesWidget" ]
    ] ],
    [ "imagecurves.cpp", "imagecurves_8cpp.html", "imagecurves_8cpp" ],
    [ "imagecurves.h", "imagecurves_8h.html", [
      [ "ImageCurves", "classDigikam_1_1ImageCurves.html", "classDigikam_1_1ImageCurves" ]
    ] ]
];