var classDigikam_1_1ThumbnailInfo =
[
    [ "ThumbnailInfo", "classDigikam_1_1ThumbnailInfo.html#a0e8c7a7575d1e4862a24ecf44fb4efd4", null ],
    [ "~ThumbnailInfo", "classDigikam_1_1ThumbnailInfo.html#afad597cb63111b6fdfa5de3a1d603a56", null ],
    [ "customIdentifier", "classDigikam_1_1ThumbnailInfo.html#aed503eb286d4ba448068633269b04380", null ],
    [ "fileName", "classDigikam_1_1ThumbnailInfo.html#ad9576397cd0b44a0f39291331836f0eb", null ],
    [ "filePath", "classDigikam_1_1ThumbnailInfo.html#aae5d9dc6e9b56f425fa0112766c7422c", null ],
    [ "fileSize", "classDigikam_1_1ThumbnailInfo.html#af73534244307750a5544373418397fa3", null ],
    [ "id", "classDigikam_1_1ThumbnailInfo.html#ad57c3985e16d3f9329566e5b7565325e", null ],
    [ "isAccessible", "classDigikam_1_1ThumbnailInfo.html#a09d154d4a76d8cb87ff43b16a7e360d1", null ],
    [ "mimeType", "classDigikam_1_1ThumbnailInfo.html#afa275f493f78ae5804788d128d135223", null ],
    [ "modificationDate", "classDigikam_1_1ThumbnailInfo.html#a6bdcdbac21a3fc440fe18a8df07442e7", null ],
    [ "orientationHint", "classDigikam_1_1ThumbnailInfo.html#ae8a5da03823452b8a4caf56f976533d5", null ],
    [ "uniqueHash", "classDigikam_1_1ThumbnailInfo.html#ae62d50389507ce24d358f3878a91c648", null ]
];