var classDigikamGenericJAlbumPlugin_1_1JAlbumWizard =
[
    [ "JAlbumWizard", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#a6e7971a92d55b58d40779b8ca42b6621", null ],
    [ "~JAlbumWizard", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#a8ef6ca55e9a21a2d44beeee51e6ff5e3", null ],
    [ "nextId", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#ab38f18b39180cdecf1eaef8d85009a28", null ],
    [ "restoreDialogSize", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#a99a0591ddddda89952710d912c0a8e68", null ],
    [ "saveDialogSize", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#a37a3b930d03a859f95dfef14004c3448", null ],
    [ "setItemsList", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#ae889a79b15dbae09a67e26c6736e0623", null ],
    [ "setPlugin", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#a1aa458a15664294147f3b6cb10c9323f", null ],
    [ "settings", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#a8e5cb4d46b29dd89eb32c6ee6a12b478", null ],
    [ "validateCurrentPage", "classDigikamGenericJAlbumPlugin_1_1JAlbumWizard.html#af2897495e6d314579c4c2f8e36f26f0a", null ]
];