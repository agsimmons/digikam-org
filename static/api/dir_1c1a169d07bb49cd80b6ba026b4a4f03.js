var dir_1c1a169d07bb49cd80b6ba026b4a4f03 =
[
    [ "casemodifier.cpp", "casemodifier_8cpp.html", null ],
    [ "casemodifier.h", "casemodifier_8h.html", [
      [ "CaseModifier", "classDigikam_1_1CaseModifier.html", "classDigikam_1_1CaseModifier" ]
    ] ],
    [ "defaultvaluemodifier.cpp", "defaultvaluemodifier_8cpp.html", null ],
    [ "defaultvaluemodifier.h", "defaultvaluemodifier_8h.html", [
      [ "DefaultValueDialog", "classDigikam_1_1DefaultValueDialog.html", "classDigikam_1_1DefaultValueDialog" ],
      [ "DefaultValueModifier", "classDigikam_1_1DefaultValueModifier.html", "classDigikam_1_1DefaultValueModifier" ]
    ] ],
    [ "rangemodifier.cpp", "rangemodifier_8cpp.html", null ],
    [ "rangemodifier.h", "rangemodifier_8h.html", [
      [ "RangeDialog", "classDigikam_1_1RangeDialog.html", "classDigikam_1_1RangeDialog" ],
      [ "RangeModifier", "classDigikam_1_1RangeModifier.html", "classDigikam_1_1RangeModifier" ]
    ] ],
    [ "removedoublesmodifier.cpp", "removedoublesmodifier_8cpp.html", null ],
    [ "removedoublesmodifier.h", "removedoublesmodifier_8h.html", [
      [ "RemoveDoublesModifier", "classDigikam_1_1RemoveDoublesModifier.html", "classDigikam_1_1RemoveDoublesModifier" ]
    ] ],
    [ "replacemodifier.cpp", "replacemodifier_8cpp.html", null ],
    [ "replacemodifier.h", "replacemodifier_8h.html", [
      [ "ReplaceDialog", "classDigikam_1_1ReplaceDialog.html", "classDigikam_1_1ReplaceDialog" ],
      [ "ReplaceModifier", "classDigikam_1_1ReplaceModifier.html", "classDigikam_1_1ReplaceModifier" ]
    ] ],
    [ "trimmedmodifier.cpp", "trimmedmodifier_8cpp.html", null ],
    [ "trimmedmodifier.h", "trimmedmodifier_8h.html", [
      [ "TrimmedModifier", "classDigikam_1_1TrimmedModifier.html", "classDigikam_1_1TrimmedModifier" ]
    ] ],
    [ "uniquemodifier.cpp", "uniquemodifier_8cpp.html", null ],
    [ "uniquemodifier.h", "uniquemodifier_8h.html", [
      [ "UniqueModifier", "classDigikam_1_1UniqueModifier.html", "classDigikam_1_1UniqueModifier" ]
    ] ]
];