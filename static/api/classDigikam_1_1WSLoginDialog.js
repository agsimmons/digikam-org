var classDigikam_1_1WSLoginDialog =
[
    [ "WSLoginDialog", "classDigikam_1_1WSLoginDialog.html#a6e2ed3a5e933478148247501c7365981", null ],
    [ "~WSLoginDialog", "classDigikam_1_1WSLoginDialog.html#a6db913b53e19512047a8a7d0519c9a32", null ],
    [ "login", "classDigikam_1_1WSLoginDialog.html#ab921bb33a7ff29a091ae997ebd9826d5", null ],
    [ "password", "classDigikam_1_1WSLoginDialog.html#af078072aa89243842b5277b538f7e22d", null ],
    [ "setLogin", "classDigikam_1_1WSLoginDialog.html#a95d56aebefb7fec873755b2da65d92f0", null ],
    [ "setPassword", "classDigikam_1_1WSLoginDialog.html#a2ff4a412f9cb404172613e6133e8998d", null ],
    [ "slotAccept", "classDigikam_1_1WSLoginDialog.html#ab7f6c1baa109ae39802611f895085b51", null ]
];