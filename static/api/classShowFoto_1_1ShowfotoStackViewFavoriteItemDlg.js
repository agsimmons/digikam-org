var classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg =
[
    [ "ShowfotoStackViewFavoriteItemDlg", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#ad240d8b0448cd60ffe25fd92aa0a6399", null ],
    [ "~ShowfotoStackViewFavoriteItemDlg", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a8d139c5c338e4b5dc308d43b622c0387", null ],
    [ "currentUrl", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a3300d31037d720b08079f70c77bf3eb6", null ],
    [ "date", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#aede289c4ca4d837650b124deb2e6768c", null ],
    [ "description", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#ac3d71b66bee322d5f1e6470ea4a336e0", null ],
    [ "favoriteType", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a34be104e62638a597643728e9ec94046", null ],
    [ "icon", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a7c77a021e294de2b199ade67944cf426", null ],
    [ "name", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#afeff5124b2b790a28244bb200663cc7b", null ],
    [ "setCurrentUrl", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#aae9e82c30c30314e01c5e391ad1826f4", null ],
    [ "setDate", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#af7cd26c546b04e0056035f2b433dfff2", null ],
    [ "setDescription", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#afdf2dae98a94efb026c9fb5bfe33138a", null ],
    [ "setFavoriteType", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#aea4c3ee876a08a49123c3f8502abfae6", null ],
    [ "setIcon", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#ae837ef5084d6fc156ec3dcd45bfdb399", null ],
    [ "setIconSize", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#ad65efb00fab9549409d7397561f27e52", null ],
    [ "setName", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a51f5e025f2a8383eda7e09366e6d3b13", null ],
    [ "setParentItem", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#ad6f9649515becc2e0dea106d801802a5", null ],
    [ "setSortOrder", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a914839101430b3b0927d25885d5b2048", null ],
    [ "setSortRole", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a3ae18c28073ebc31f598fadbce92cc07", null ],
    [ "setUrls", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#a8933bb0f62ba99d35406c566c100b935", null ],
    [ "urls", "classShowFoto_1_1ShowfotoStackViewFavoriteItemDlg.html#ace4f01c7acdfc3af929bc0c7b351ce86", null ]
];