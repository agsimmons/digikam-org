var classDigikam_1_1StateSavingObject =
[
    [ "StateSavingDepth", "classDigikam_1_1StateSavingObject.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1StateSavingObject.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1StateSavingObject.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1StateSavingObject.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "StateSavingObject", "classDigikam_1_1StateSavingObject.html#aa0615ffd2c86cd4684bb5ffc03c30c51", null ],
    [ "~StateSavingObject", "classDigikam_1_1StateSavingObject.html#a9480175bcc1d59d790de363b28ce9833", null ],
    [ "doLoadState", "classDigikam_1_1StateSavingObject.html#a8a44ee68adc0e213b18ec0fd5ae3b08b", null ],
    [ "doSaveState", "classDigikam_1_1StateSavingObject.html#a50b2d0641bfbbef0c2635b3a8cd93c3f", null ],
    [ "entryName", "classDigikam_1_1StateSavingObject.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getConfigGroup", "classDigikam_1_1StateSavingObject.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getStateSavingDepth", "classDigikam_1_1StateSavingObject.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1StateSavingObject.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "saveState", "classDigikam_1_1StateSavingObject.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setConfigGroup", "classDigikam_1_1StateSavingObject.html#aa37c59ac6e91ba60c3c0dd14cc6fa71e", null ],
    [ "setEntryPrefix", "classDigikam_1_1StateSavingObject.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setStateSavingDepth", "classDigikam_1_1StateSavingObject.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ]
];