var classpps__range__extension =
[
    [ "pps_range_extension", "classpps__range__extension.html#a4184cc5c29d3cc01de85d7ee82d79204", null ],
    [ "dump", "classpps__range__extension.html#ad680db5920fe94a0a8ee6f67fbf65ed9", null ],
    [ "read", "classpps__range__extension.html#ad9c4c599e258ff61a1b8a51e0d29057c", null ],
    [ "reset", "classpps__range__extension.html#a8a3b9b9a00379d94bfb5d3b4af345e4a", null ],
    [ "cb_qp_offset_list", "classpps__range__extension.html#aea43f793089de05a0aacd2e6cffe7e78", null ],
    [ "chroma_qp_offset_list_enabled_flag", "classpps__range__extension.html#a5677ce2f4e41dc42640f753bc3c72642", null ],
    [ "chroma_qp_offset_list_len", "classpps__range__extension.html#aa7eec26ffe907b1631d9384bed4cd7fd", null ],
    [ "cr_qp_offset_list", "classpps__range__extension.html#aa02715ed70398869ee535b398e56ce03", null ],
    [ "cross_component_prediction_enabled_flag", "classpps__range__extension.html#a6430fdae85a5209fb7eddcfb48c49010", null ],
    [ "diff_cu_chroma_qp_offset_depth", "classpps__range__extension.html#ae7fc106f0d1b70de031ce848182d4251", null ],
    [ "log2_max_transform_skip_block_size", "classpps__range__extension.html#a6bce3d8553e7adad7b49be27a41bc899", null ],
    [ "log2_sao_offset_scale_chroma", "classpps__range__extension.html#ab5e06f9b200c008da049631e534c0027", null ],
    [ "log2_sao_offset_scale_luma", "classpps__range__extension.html#a57262e53319fe5690ac3a4b10cccc2d5", null ]
];