var dir_2e6d403498f93daea02af4768aee214c =
[
    [ "databasewriter.cpp", "databasewriter_8cpp.html", null ],
    [ "databasewriter.h", "databasewriter_8h.html", [
      [ "DatabaseWriter", "classDigikam_1_1DatabaseWriter.html", "classDigikam_1_1DatabaseWriter" ]
    ] ],
    [ "detectionworker.cpp", "detectionworker_8cpp.html", null ],
    [ "detectionworker.h", "detectionworker_8h.html", [
      [ "DetectionWorker", "classDigikam_1_1DetectionWorker.html", "classDigikam_1_1DetectionWorker" ]
    ] ],
    [ "recognitionworker.cpp", "recognitionworker_8cpp.html", null ],
    [ "recognitionworker.h", "recognitionworker_8h.html", [
      [ "RecognitionWorker", "classDigikam_1_1RecognitionWorker.html", "classDigikam_1_1RecognitionWorker" ]
    ] ],
    [ "trainerworker.cpp", "trainerworker_8cpp.html", null ],
    [ "trainerworker.h", "trainerworker_8h.html", [
      [ "TrainerWorker", "classDigikam_1_1TrainerWorker.html", "classDigikam_1_1TrainerWorker" ]
    ] ]
];