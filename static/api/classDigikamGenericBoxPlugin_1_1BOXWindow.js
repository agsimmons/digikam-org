var classDigikamGenericBoxPlugin_1_1BOXWindow =
[
    [ "BOXWindow", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a83dda81b66374fa488a76487b074c398", null ],
    [ "~BOXWindow", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a2efe455a4baf91e656db4375e9ad3e4d", null ],
    [ "addButton", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a338ad1ca1d39489c83775e3286302c59", null ],
    [ "cancelClicked", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2", null ],
    [ "reactivate", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#aef92987d23ba9fabfffa3127a557da15", null ],
    [ "restoreDialogSize", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setItemsList", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a2595344bf8709cbcdd3667996eb64d89", null ],
    [ "setMainWidget", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#ae956aceb6ae24079791e2f491957d446", null ],
    [ "setPlugin", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "setRejectButtonMode", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a32667efc4f6a02e644886729952fd354", null ],
    [ "startButton", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a34e7c6deb1458c37066862375a487600", null ],
    [ "m_buttons", "classDigikamGenericBoxPlugin_1_1BOXWindow.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];