var classDigikam_1_1AdvancedRenameListItem =
[
    [ "Column", "classDigikam_1_1AdvancedRenameListItem.html#a3482976718400c43abd8b21a7584ecb7", [
      [ "OldName", "classDigikam_1_1AdvancedRenameListItem.html#a3482976718400c43abd8b21a7584ecb7aa00d2dbd1ea566f918c8bac86555fcdd", null ],
      [ "NewName", "classDigikam_1_1AdvancedRenameListItem.html#a3482976718400c43abd8b21a7584ecb7a189f46893ce62d34e15ed4aeed69cc3e", null ]
    ] ],
    [ "AdvancedRenameListItem", "classDigikam_1_1AdvancedRenameListItem.html#ac209a1317b7e6c491bca6af97240dc0e", null ],
    [ "AdvancedRenameListItem", "classDigikam_1_1AdvancedRenameListItem.html#a9d58c2fa486280db8d7006578bb36fe4", null ],
    [ "~AdvancedRenameListItem", "classDigikam_1_1AdvancedRenameListItem.html#a2234ed62ecc918749c9d59ff001652c6", null ],
    [ "imageUrl", "classDigikam_1_1AdvancedRenameListItem.html#ac7182f60084135da40c3c6039c4cbedf", null ],
    [ "isNameEqual", "classDigikam_1_1AdvancedRenameListItem.html#aeb031c4ff2f1322043a62b47f1ff6868", null ],
    [ "markInvalid", "classDigikam_1_1AdvancedRenameListItem.html#a78cddbd0984270bc74f955482a61b123", null ],
    [ "name", "classDigikam_1_1AdvancedRenameListItem.html#af631a01502938c1145df8f3b5be40f68", null ],
    [ "newName", "classDigikam_1_1AdvancedRenameListItem.html#a21335dbfafc51f7e39b3284db1bf0fa0", null ],
    [ "setImageUrl", "classDigikam_1_1AdvancedRenameListItem.html#a70ac1c2a06d613a5512fc99453124202", null ],
    [ "setName", "classDigikam_1_1AdvancedRenameListItem.html#acd08363ff167e19fa83d5167f010d32b", null ],
    [ "setNewName", "classDigikam_1_1AdvancedRenameListItem.html#a962bee2e8a299d5c9ee6ad23d4b32874", null ]
];