var classDigikam_1_1TagRegion =
[
    [ "Type", "classDigikam_1_1TagRegion.html#af1bb6817f0a91f715a08562dcbd85a2b", [
      [ "Invalid", "classDigikam_1_1TagRegion.html#af1bb6817f0a91f715a08562dcbd85a2ba11930701cf12e1ae4511fd6e374b8205", null ],
      [ "Rect", "classDigikam_1_1TagRegion.html#af1bb6817f0a91f715a08562dcbd85a2bac3b4387ac52c5aad1527cbb217c1a240", null ]
    ] ],
    [ "TagRegion", "classDigikam_1_1TagRegion.html#a9108d26ed3e6041891b4ba964c66134e", null ],
    [ "TagRegion", "classDigikam_1_1TagRegion.html#ae87aabd56f15db69ea20ae249ffe0245", null ],
    [ "TagRegion", "classDigikam_1_1TagRegion.html#a6978b043061f00e50f6b17815391ea1d", null ],
    [ "intersects", "classDigikam_1_1TagRegion.html#a7fb2c2cf5d23a766b9602ad79b03053a", null ],
    [ "isValid", "classDigikam_1_1TagRegion.html#a9ebd270109116f38944f62a42bd288fa", null ],
    [ "operator!=", "classDigikam_1_1TagRegion.html#a27abae79d2a08307610ea44fd8f0bc69", null ],
    [ "operator==", "classDigikam_1_1TagRegion.html#a024c4539629825925c35a5e87b27232e", null ],
    [ "toRect", "classDigikam_1_1TagRegion.html#adbe541ae9fb994c57be3a21d16b728e1", null ],
    [ "toVariant", "classDigikam_1_1TagRegion.html#a1461f564f950546547ff78f12dcc7da1", null ],
    [ "toXml", "classDigikam_1_1TagRegion.html#a3411e1eb4a227712c72234a67b056ace", null ],
    [ "type", "classDigikam_1_1TagRegion.html#af768105ccc914aa38697a363591e75c3", null ],
    [ "m_type", "classDigikam_1_1TagRegion.html#a3df83632e71ddab2d9222b8694d1aa76", null ],
    [ "m_value", "classDigikam_1_1TagRegion.html#a01a80c95a9c5b0438ea5e6686f446bd6", null ]
];