var structheif__decoder__plugin =
[
    [ "decode_image", "structheif__decoder__plugin.html#abfa3f4f73f2c31943e4ad427c92e21f1", null ],
    [ "deinit_plugin", "structheif__decoder__plugin.html#a0ee5de1e7908d36cb5dec6bfed2682d0", null ],
    [ "does_support_format", "structheif__decoder__plugin.html#a1981c0f8705b9ede357ed506d5f03568", null ],
    [ "free_decoder", "structheif__decoder__plugin.html#a4145199ba8474496bcaac192f92e77fa", null ],
    [ "get_plugin_name", "structheif__decoder__plugin.html#abbcc51917ce3e496525096e1b94d3a8d", null ],
    [ "init_plugin", "structheif__decoder__plugin.html#af2cdb7a14db620deedb50ef91c0ad86a", null ],
    [ "new_decoder", "structheif__decoder__plugin.html#aefe0854c732861e5dd00bd2218b2a380", null ],
    [ "plugin_api_version", "structheif__decoder__plugin.html#af42d7b317db3a77b8cc789a724e5f9f1", null ],
    [ "push_data", "structheif__decoder__plugin.html#a957b0a040971a3a05f3833debf5788fd", null ]
];