var classDigikam_1_1AlbumRootLocation =
[
    [ "Status", "classDigikam_1_1AlbumRootLocation.html#a9169e6def8b5a55d13a69e1ab614ab2d", [
      [ "LocationNull", "classDigikam_1_1AlbumRootLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da057dbf9b75e101e4178a7d1f3c413157", null ],
      [ "LocationAvailable", "classDigikam_1_1AlbumRootLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da91d729b7349c62799d23af1bc1cfb913", null ],
      [ "LocationHidden", "classDigikam_1_1AlbumRootLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da431831542cb1b9caece19b62515328d4", null ],
      [ "LocationUnavailable", "classDigikam_1_1AlbumRootLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da89f81dd6a9696696e33180ff8e45fa28", null ],
      [ "LocationDeleted", "classDigikam_1_1AlbumRootLocation.html#a9169e6def8b5a55d13a69e1ab614ab2da09737556f383ccb9e85afe0f3edede43", null ]
    ] ],
    [ "Type", "classDigikam_1_1AlbumRootLocation.html#a85099ad3d611d54dcede1412cc802d01", [
      [ "TypeVolumeHardWired", "classDigikam_1_1AlbumRootLocation.html#a85099ad3d611d54dcede1412cc802d01a1474e42c357307d28ff1b7e3d1a2c58b", null ],
      [ "TypeVolumeRemovable", "classDigikam_1_1AlbumRootLocation.html#a85099ad3d611d54dcede1412cc802d01a65b629dff58128a78d92fb324a1beb3d", null ],
      [ "TypeNetwork", "classDigikam_1_1AlbumRootLocation.html#a85099ad3d611d54dcede1412cc802d01ac76af2d83399ce80d362552c9f1bc379", null ]
    ] ],
    [ "AlbumRootLocation", "classDigikam_1_1AlbumRootLocation.html#a94894c116a4d0ca2daa73308788bb648", null ],
    [ "AlbumRootLocation", "classDigikam_1_1AlbumRootLocation.html#abf993e55bd4b35240188d40371d65f05", null ],
    [ "albumRootPath", "classDigikam_1_1AlbumRootLocation.html#a721e46fc67375a1973527813f64c8fba", null ],
    [ "hash", "classDigikam_1_1AlbumRootLocation.html#a287f8c7721c4f5366f4fdacc9cf853df", null ],
    [ "id", "classDigikam_1_1AlbumRootLocation.html#ac06170d5fc9ebac5060bfb8d206c28f1", null ],
    [ "isAvailable", "classDigikam_1_1AlbumRootLocation.html#a724144b9c9515fed0018a7be5178aac0", null ],
    [ "isNull", "classDigikam_1_1AlbumRootLocation.html#ab5274efe3f06df6b3bce4b73ac147373", null ],
    [ "label", "classDigikam_1_1AlbumRootLocation.html#a00fa4f70e23e2ab247888b60d58f9ebc", null ],
    [ "setAbsolutePath", "classDigikam_1_1AlbumRootLocation.html#a900501888d2f9065bbe57dbb709cf358", null ],
    [ "setId", "classDigikam_1_1AlbumRootLocation.html#a694b3e968d9c9bc00a809be708d34cab", null ],
    [ "setLabel", "classDigikam_1_1AlbumRootLocation.html#a7ce69a223753aec2b79c081ac719b4ee", null ],
    [ "setStatus", "classDigikam_1_1AlbumRootLocation.html#a5794205657a9e45f328b18ec59462ff1", null ],
    [ "setStatusFromFlags", "classDigikam_1_1AlbumRootLocation.html#a2b0c887d1a4b0023355059ed72459da6", null ],
    [ "setType", "classDigikam_1_1AlbumRootLocation.html#a6e01c8b019dfe82b7e0785dc98058e54", null ],
    [ "status", "classDigikam_1_1AlbumRootLocation.html#af43fc302968374cd6db23217a1791c8e", null ],
    [ "type", "classDigikam_1_1AlbumRootLocation.html#ac3f77cf399e5a6349206f75f6972e217", null ],
    [ "available", "classDigikam_1_1AlbumRootLocation.html#a279a3f4768deb54c1d7c24bf62fb6eb7", null ],
    [ "hidden", "classDigikam_1_1AlbumRootLocation.html#a346b38077c5b596ce8cc3065080f3fb7", null ],
    [ "identifier", "classDigikam_1_1AlbumRootLocation.html#a0f570762e691e33217a1d8535e4c5603", null ],
    [ "m_id", "classDigikam_1_1AlbumRootLocation.html#a77b7dad959dab06cc5b47fae3a6bf5ef", null ],
    [ "m_label", "classDigikam_1_1AlbumRootLocation.html#a138793e521715cc1267bc4ff53695def", null ],
    [ "m_path", "classDigikam_1_1AlbumRootLocation.html#a717e7401f12dace947b540dbda0dcf6d", null ],
    [ "m_status", "classDigikam_1_1AlbumRootLocation.html#a12651a877f7a3144a3c57b1d4ec8a2b3", null ],
    [ "m_type", "classDigikam_1_1AlbumRootLocation.html#afff9a980cb73a5733df59af3e85a3bf7", null ],
    [ "specificPath", "classDigikam_1_1AlbumRootLocation.html#a7d3469b0f2eb7f229b0bf899cd00d304", null ]
];