var dir_3c0b2cbc5636196bb5f7aec5a1c7f224 =
[
    [ "atkinspagelayout.cpp", "atkinspagelayout_8cpp.html", null ],
    [ "atkinspagelayout.h", "atkinspagelayout_8h.html", [
      [ "AtkinsPageLayout", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayout.html", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayout" ]
    ] ],
    [ "atkinspagelayoutnode.cpp", "atkinspagelayoutnode_8cpp.html", null ],
    [ "atkinspagelayoutnode.h", "atkinspagelayoutnode_8h.html", [
      [ "AtkinsPageLayoutNode", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode.html", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutNode" ]
    ] ],
    [ "atkinspagelayouttree.cpp", "atkinspagelayouttree_8cpp.html", null ],
    [ "atkinspagelayouttree.h", "atkinspagelayouttree_8h.html", [
      [ "AtkinsPageLayoutTree", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree.html", "classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree" ]
    ] ],
    [ "gimpbinary.cpp", "gimpbinary_8cpp.html", null ],
    [ "gimpbinary.h", "gimpbinary_8h.html", [
      [ "GimpBinary", "classDigikamGenericPrintCreatorPlugin_1_1GimpBinary.html", "classDigikamGenericPrintCreatorPlugin_1_1GimpBinary" ]
    ] ],
    [ "templateicon.cpp", "templateicon_8cpp.html", null ],
    [ "templateicon.h", "templateicon_8h.html", [
      [ "TemplateIcon", "classDigikamGenericPrintCreatorPlugin_1_1TemplateIcon.html", "classDigikamGenericPrintCreatorPlugin_1_1TemplateIcon" ]
    ] ]
];