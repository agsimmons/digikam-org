var classDigikam_1_1FaceDetector =
[
    [ "FaceDetector", "classDigikam_1_1FaceDetector.html#abc9d345603daa30418ac3f27e01a2f88", null ],
    [ "FaceDetector", "classDigikam_1_1FaceDetector.html#ae93fbe81c63bdb7cb57f5d3257544cba", null ],
    [ "~FaceDetector", "classDigikam_1_1FaceDetector.html#afa3ae8030b98dcd626625d5d04733cd9", null ],
    [ "backendIdentifier", "classDigikam_1_1FaceDetector.html#a19c6130cbfaafffbaab0bd6aad86dfdb", null ],
    [ "detectFaces", "classDigikam_1_1FaceDetector.html#a021c778bead038dc9a73d9675228579a", null ],
    [ "detectFaces", "classDigikam_1_1FaceDetector.html#aaa25898656bc859c1ca25c3b8137be60", null ],
    [ "detectFaces", "classDigikam_1_1FaceDetector.html#a198378a43fcdde47310fc7f8509e4900", null ],
    [ "operator=", "classDigikam_1_1FaceDetector.html#a8579de1c2d29c850817cf3090d9247dc", null ],
    [ "parameters", "classDigikam_1_1FaceDetector.html#aef97b5d59c59b388e3b6e1ee4df3d33e", null ],
    [ "recommendedImageSize", "classDigikam_1_1FaceDetector.html#a09cf2199255b732ad2b3ccd1b6b95bd5", null ],
    [ "setParameter", "classDigikam_1_1FaceDetector.html#a8f1c90bee307e5b516fed00990800a55", null ],
    [ "setParameters", "classDigikam_1_1FaceDetector.html#a2d63f955adb2df850e6b9e15a155b2d2", null ]
];