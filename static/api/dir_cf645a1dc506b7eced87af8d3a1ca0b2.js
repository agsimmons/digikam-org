var dir_cf645a1dc506b7eced87af8d3a1ca0b2 =
[
    [ "abstractalbummodel.cpp", "abstractalbummodel_8cpp.html", null ],
    [ "abstractalbummodel.h", "abstractalbummodel_8h.html", [
      [ "AbstractAlbumModel", "classDigikam_1_1AbstractAlbumModel.html", "classDigikam_1_1AbstractAlbumModel" ],
      [ "AbstractCheckableAlbumModel", "classDigikam_1_1AbstractCheckableAlbumModel.html", "classDigikam_1_1AbstractCheckableAlbumModel" ],
      [ "AbstractCountingAlbumModel", "classDigikam_1_1AbstractCountingAlbumModel.html", "classDigikam_1_1AbstractCountingAlbumModel" ],
      [ "AbstractSpecificAlbumModel", "classDigikam_1_1AbstractSpecificAlbumModel.html", "classDigikam_1_1AbstractSpecificAlbumModel" ]
    ] ],
    [ "abstractalbummodel_checkable.cpp", "abstractalbummodel__checkable_8cpp.html", null ],
    [ "abstractalbummodel_counting.cpp", "abstractalbummodel__counting_8cpp.html", null ],
    [ "abstractalbummodel_p.h", "abstractalbummodel__p_8h.html", null ],
    [ "abstractalbummodel_specific.cpp", "abstractalbummodel__specific_8cpp.html", null ],
    [ "albumfiltermodel.cpp", "albumfiltermodel_8cpp.html", null ],
    [ "albumfiltermodel.h", "albumfiltermodel_8h.html", [
      [ "AlbumFilterModel", "classDigikam_1_1AlbumFilterModel.html", "classDigikam_1_1AlbumFilterModel" ],
      [ "CheckableAlbumFilterModel", "classDigikam_1_1CheckableAlbumFilterModel.html", "classDigikam_1_1CheckableAlbumFilterModel" ],
      [ "SearchFilterModel", "classDigikam_1_1SearchFilterModel.html", "classDigikam_1_1SearchFilterModel" ],
      [ "TagPropertiesFilterModel", "classDigikam_1_1TagPropertiesFilterModel.html", "classDigikam_1_1TagPropertiesFilterModel" ],
      [ "TagsManagerFilterModel", "classDigikam_1_1TagsManagerFilterModel.html", "classDigikam_1_1TagsManagerFilterModel" ]
    ] ],
    [ "albumfiltermodel_checkable.cpp", "albumfiltermodel__checkable_8cpp.html", null ],
    [ "albumfiltermodel_p.h", "albumfiltermodel__p_8h.html", null ],
    [ "albumfiltermodel_search.cpp", "albumfiltermodel__search_8cpp.html", null ],
    [ "albumfiltermodel_tagproperties.cpp", "albumfiltermodel__tagproperties_8cpp.html", null ],
    [ "albumfiltermodel_tagsmanager.cpp", "albumfiltermodel__tagsmanager_8cpp.html", null ],
    [ "albummodel.cpp", "albummodel_8cpp.html", null ],
    [ "albummodel.h", "albummodel_8h.html", [
      [ "AlbumModel", "classDigikam_1_1AlbumModel.html", "classDigikam_1_1AlbumModel" ],
      [ "DateAlbumModel", "classDigikam_1_1DateAlbumModel.html", "classDigikam_1_1DateAlbumModel" ],
      [ "SearchModel", "classDigikam_1_1SearchModel.html", "classDigikam_1_1SearchModel" ],
      [ "TagModel", "classDigikam_1_1TagModel.html", "classDigikam_1_1TagModel" ]
    ] ],
    [ "albummodel_date.cpp", "albummodel__date_8cpp.html", null ],
    [ "albummodel_p.h", "albummodel__p_8h.html", null ],
    [ "albummodel_search.cpp", "albummodel__search_8cpp.html", null ],
    [ "albummodel_tag.cpp", "albummodel__tag_8cpp.html", null ],
    [ "albummodeldragdrophandler.cpp", "albummodeldragdrophandler_8cpp.html", null ],
    [ "albummodeldragdrophandler.h", "albummodeldragdrophandler_8h.html", [
      [ "AlbumModelDragDropHandler", "classDigikam_1_1AlbumModelDragDropHandler.html", "classDigikam_1_1AlbumModelDragDropHandler" ]
    ] ],
    [ "categorizeditemmodel.cpp", "categorizeditemmodel_8cpp.html", null ],
    [ "categorizeditemmodel.h", "categorizeditemmodel_8h.html", [
      [ "ActionItemModel", "classDigikam_1_1ActionItemModel.html", "classDigikam_1_1ActionItemModel" ],
      [ "CategorizedItemModel", "classDigikam_1_1CategorizedItemModel.html", "classDigikam_1_1CategorizedItemModel" ]
    ] ],
    [ "itemalbumfiltermodel.cpp", "itemalbumfiltermodel_8cpp.html", null ],
    [ "itemalbumfiltermodel.h", "itemalbumfiltermodel_8h.html", [
      [ "ItemAlbumFilterModel", "classDigikam_1_1ItemAlbumFilterModel.html", "classDigikam_1_1ItemAlbumFilterModel" ]
    ] ],
    [ "itemalbummodel.cpp", "itemalbummodel_8cpp.html", null ],
    [ "itemalbummodel.h", "itemalbummodel_8h.html", [
      [ "ItemAlbumModel", "classDigikam_1_1ItemAlbumModel.html", "classDigikam_1_1ItemAlbumModel" ]
    ] ]
];