var shapepredictor_8cpp =
[
    [ "createShapeRelativeEncoding", "shapepredictor_8cpp.html#a4e2a317f92011f1fead4996146d072bf", null ],
    [ "extractFeaturePixelValues", "shapepredictor_8cpp.html#a3587da04ed33dc424cfe49c8eadf5b65", null ],
    [ "findTformBetweenShapes", "shapepredictor_8cpp.html#abb5171a13e2d5c55104428bee3b4fc8b", null ],
    [ "left_child", "shapepredictor_8cpp.html#abbf9b1e16029c2e0bc55f48750868076", null ],
    [ "nearestShapePoint", "shapepredictor_8cpp.html#aa7fc42f5ed25d139f6ee5d22fd0b501e", null ],
    [ "normalizingTform", "shapepredictor_8cpp.html#ab640b94a850c20406eba6b65100a4b3d", null ],
    [ "operator<<", "shapepredictor_8cpp.html#a7a192c9ec9bb11fae00ea107cbaa20ac", null ],
    [ "operator<<", "shapepredictor_8cpp.html#a73339fa424d2adec50d79d4f2d824d16", null ],
    [ "operator<<", "shapepredictor_8cpp.html#a5f71c403d76289e1328372d8a128b539", null ],
    [ "operator>>", "shapepredictor_8cpp.html#af6fa3fed7e7bce338a33b72a883865b2", null ],
    [ "operator>>", "shapepredictor_8cpp.html#a715815411c0ec83d0ca518f7d83ae5c1", null ],
    [ "operator>>", "shapepredictor_8cpp.html#aeedaaedda44373971c1812ab853ab034", null ],
    [ "pointContained", "shapepredictor_8cpp.html#ae72d7bbce0471b8fb6c689774a64cc19", null ],
    [ "right_child", "shapepredictor_8cpp.html#a53a7fa2d39ccc09c61823f2cb90d627f", null ],
    [ "unnormalizingTform", "shapepredictor_8cpp.html#a28807dc792e6bf27e0f1ea8658135c0f", null ]
];