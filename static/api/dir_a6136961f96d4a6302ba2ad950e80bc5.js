var dir_a6136961f96d4a6302ba2ad950e80bc5 =
[
    [ "opencv-dnn", "dir_a3a606e7ed0e4c82cd14e07bdf5a211e.html", "dir_a3a606e7ed0e4c82cd14e07bdf5a211e" ],
    [ "dataproviders.cpp", "dataproviders_8cpp.html", null ],
    [ "dataproviders.h", "dataproviders_8h.html", [
      [ "EmptyImageListProvider", "classDigikam_1_1EmptyImageListProvider.html", "classDigikam_1_1EmptyImageListProvider" ],
      [ "ImageListProvider", "classDigikam_1_1ImageListProvider.html", "classDigikam_1_1ImageListProvider" ],
      [ "QListImageListProvider", "classDigikam_1_1QListImageListProvider.html", "classDigikam_1_1QListImageListProvider" ],
      [ "TrainingDataProvider", "classDigikam_1_1TrainingDataProvider.html", "classDigikam_1_1TrainingDataProvider" ]
    ] ],
    [ "facialrecognition_wrapper.cpp", "facialrecognition__wrapper_8cpp.html", null ],
    [ "facialrecognition_wrapper.h", "facialrecognition__wrapper_8h.html", [
      [ "FacialRecognitionWrapper", "classDigikam_1_1FacialRecognitionWrapper.html", "classDigikam_1_1FacialRecognitionWrapper" ]
    ] ],
    [ "facialrecognition_wrapper_identity.cpp", "facialrecognition__wrapper__identity_8cpp.html", null ],
    [ "facialrecognition_wrapper_p.cpp", "facialrecognition__wrapper__p_8cpp.html", null ],
    [ "facialrecognition_wrapper_p.h", "facialrecognition__wrapper__p_8h.html", [
      [ "Private", "classDigikam_1_1FacialRecognitionWrapper_1_1Private.html", "classDigikam_1_1FacialRecognitionWrapper_1_1Private" ]
    ] ],
    [ "facialrecognition_wrapper_recognize.cpp", "facialrecognition__wrapper__recognize_8cpp.html", null ],
    [ "facialrecognition_wrapper_setup.cpp", "facialrecognition__wrapper__setup_8cpp.html", null ],
    [ "facialrecognition_wrapper_training.cpp", "facialrecognition__wrapper__training_8cpp.html", null ],
    [ "recognitiontrainingprovider.cpp", "recognitiontrainingprovider_8cpp.html", null ],
    [ "recognitiontrainingprovider.h", "recognitiontrainingprovider_8h.html", [
      [ "RecognitionTrainingProvider", "classDigikam_1_1RecognitionTrainingProvider.html", "classDigikam_1_1RecognitionTrainingProvider" ]
    ] ]
];