var similaritydb_8h =
[
    [ "SimilarityDb", "classDigikam_1_1SimilarityDb.html", "classDigikam_1_1SimilarityDb" ],
    [ "FuzzyAlgorithm", "similaritydb_8h.html#a27194741b638e21d88bc343348eaa01a", [
      [ "Unknown", "similaritydb_8h.html#a27194741b638e21d88bc343348eaa01aa88183b946cc5f0e8c96b2e66e1c74a7e", null ],
      [ "Haar", "similaritydb_8h.html#a27194741b638e21d88bc343348eaa01aa7ec04582a27335133048cbe1a9f38517", null ],
      [ "TfIdf", "similaritydb_8h.html#a27194741b638e21d88bc343348eaa01aa522858debd5346510d354a877d6cbab3", null ]
    ] ]
];