var classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog =
[
    [ "DNGConverterDialog", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#ae82de7655794bb623fa6556bfcab19fb", null ],
    [ "~DNGConverterDialog", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#aa657446b38cb79e139d6a366c7a20f82", null ],
    [ "addItems", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#ab603f7028899b051118ce81789640d7f", null ],
    [ "closeEvent", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#aedad42cd9e7a852437250f5148f5b621", null ],
    [ "restoreDialogSize", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setPlugin", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "m_buttons", "classDigikamGenericDNGConverterPlugin_1_1DNGConverterDialog.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];