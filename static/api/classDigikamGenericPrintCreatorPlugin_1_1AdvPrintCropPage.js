var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage =
[
    [ "AdvPrintCropPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a5900ee5ad0be15cd65bb4d6c678c29bf", null ],
    [ "~AdvPrintCropPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a9b9a969a339b1a5a71e8aff872725e59", null ],
    [ "assistant", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a681293182dfaf2919429a4a5aa30dd04", null ],
    [ "id", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a84aa8a0e37d7ae49efc832413764ffa1", null ],
    [ "initializePage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a34666ca46f11775c398486b15b5273c4", null ],
    [ "isComplete", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a77413f486f6d4a405359e095fb0a07a3", null ],
    [ "removePageWidget", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a40cc4abca8695016a1d788f3df1cdd0d", null ],
    [ "setBtnCropEnabled", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#ac5264c761552d2760eb234028258b2af", null ],
    [ "setComplete", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a9e97ef81648ea3f5e33c5a08ae5f0ff0", null ],
    [ "setLeftBottomPix", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a04095257f4a957050e91cd4666f53316", null ],
    [ "setLeftBottomPix", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a2e56503c5324f5551edb8fb1f1156530", null ],
    [ "setPageWidget", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a2815f8463990300af07fe29cbd090308", null ],
    [ "setShowLeftView", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a67975edf6041a574e674576a29d606a1", null ],
    [ "ui", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#aac3264590d82a47b0bc9fd4d4cdb280d", null ],
    [ "validatePage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintCropPage.html#a5bf3cf51cc18b0d0e8cefa1111763bff", null ]
];