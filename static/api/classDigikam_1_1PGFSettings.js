var classDigikam_1_1PGFSettings =
[
    [ "PGFSettings", "classDigikam_1_1PGFSettings.html#a1e2a70d2d03bd67a7d3c47b430dc5958", null ],
    [ "~PGFSettings", "classDigikam_1_1PGFSettings.html#a53624c00ede0f13aef9eb4bc692a64e0", null ],
    [ "getCompressionValue", "classDigikam_1_1PGFSettings.html#a73ca899d83523548c967ae9862dfea60", null ],
    [ "getLossLessCompression", "classDigikam_1_1PGFSettings.html#a6431fddacbfbeff71283a55294cef61b", null ],
    [ "setCompressionValue", "classDigikam_1_1PGFSettings.html#ad2439d1bb9f6883b79650700dafaa7ca", null ],
    [ "setLossLessCompression", "classDigikam_1_1PGFSettings.html#a6505cfa065621e0d16959ba5cea189b9", null ],
    [ "signalSettingsChanged", "classDigikam_1_1PGFSettings.html#a0006a4c8317453778da37bd37cf86c1d", null ]
];