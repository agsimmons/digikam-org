var vps_8h =
[
    [ "layer_data", "structlayer__data.html", "structlayer__data" ],
    [ "profile_data", "classprofile__data.html", "classprofile__data" ],
    [ "profile_tier_level", "classprofile__tier__level.html", "classprofile__tier__level" ],
    [ "video_parameter_set", "classvideo__parameter__set.html", "classvideo__parameter__set" ],
    [ "MAX_TEMPORAL_SUBLAYERS", "vps_8h.html#a8fc51da70a42f7aa52b4e09ed1e8d4df", null ],
    [ "profile_idc", "vps_8h.html#a24298f46583941e8db72ad2e4042808c", [
      [ "Profile_Main", "vps_8h.html#a24298f46583941e8db72ad2e4042808caa0643b832b21da7748ccf653d42337e9", null ],
      [ "Profile_Main10", "vps_8h.html#a24298f46583941e8db72ad2e4042808ca8baa347f6e3cac075bb2a672994d8dda", null ],
      [ "Profile_MainStillPicture", "vps_8h.html#a24298f46583941e8db72ad2e4042808ca1c3d710b9bd9ebf232e318b2794eead8", null ],
      [ "Profile_FormatRangeExtensions", "vps_8h.html#a24298f46583941e8db72ad2e4042808ca60967d21ed669f8f5c1e4ee2d9a1d781", null ]
    ] ]
];