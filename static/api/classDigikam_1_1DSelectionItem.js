var classDigikam_1_1DSelectionItem =
[
    [ "Intersects", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8", [
      [ "None", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8a5dbb22ed731629c69e537148db4bc9e7", null ],
      [ "Top", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8a19f388c50e6c04ca33424281d8644fd3", null ],
      [ "TopRight", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8a838d53d0b1441c43952fc206463689db", null ],
      [ "Right", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8abf7a9b9829d79588158f0fbe99a5e68b", null ],
      [ "BottomRight", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8a8a9d7778ca968737e8b1fc83255e93ea", null ],
      [ "Bottom", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8ac53ba99f3226ad9b2a1918944926d002", null ],
      [ "BottomLeft", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8aafa84b1adb9a69c1ae6b3abbca0c4869", null ],
      [ "Left", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8acb8510ed44c53af3ba25c0bd3a963d61", null ],
      [ "TopLeft", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8a662d64fc1c5845e655e7faaf1446252c", null ],
      [ "Move", "classDigikam_1_1DSelectionItem.html#a5daec1bc003ed1f94710925a417a35e8addaa4545fbb36dfa16558fc90e59227f", null ]
    ] ],
    [ "DSelectionItem", "classDigikam_1_1DSelectionItem.html#aa6a8d26aa54894f7863713838869c698", null ],
    [ "~DSelectionItem", "classDigikam_1_1DSelectionItem.html#aec9374f3db8d9ded142b97a3ad89e6c7", null ],
    [ "boundingRect", "classDigikam_1_1DSelectionItem.html#afc3ffbf5af6fe6296b07783c08e775d9", null ],
    [ "fixTranslation", "classDigikam_1_1DSelectionItem.html#a425ff5bf98cf694b7362fcfe4b5e45dc", null ],
    [ "intersects", "classDigikam_1_1DSelectionItem.html#a9444e8870ab6a77e5aa589cae5f71aa0", null ],
    [ "paint", "classDigikam_1_1DSelectionItem.html#a5a78a7cb23cd72fcf5a0f6933885dede", null ],
    [ "rect", "classDigikam_1_1DSelectionItem.html#a2e042aa3169e9e1ed00042e375cb1c22", null ],
    [ "saveZoom", "classDigikam_1_1DSelectionItem.html#a7612fd9ecf9e37b473e23b3f9b7e1ebb", null ],
    [ "setMaxBottom", "classDigikam_1_1DSelectionItem.html#aaf369c1b935dc992eedda9d5f1512b0b", null ],
    [ "setMaxRight", "classDigikam_1_1DSelectionItem.html#aa0e9b97be411189e38fb4eb2b4ad4b74", null ],
    [ "setRect", "classDigikam_1_1DSelectionItem.html#a7942cc7d95776ad963651355942575b2", null ]
];