var heif__plugin__registry_8h =
[
    [ "heif_encoder_descriptor", "structheif__encoder__descriptor.html", "structheif__encoder__descriptor" ],
    [ "get_decoder", "heif__plugin__registry_8h.html#a1291c85e7826c86ae6e96f698acfdaee", null ],
    [ "get_encoder", "heif__plugin__registry_8h.html#add23281bda02736d785d71184d019202", null ],
    [ "get_filtered_encoder_descriptors", "heif__plugin__registry_8h.html#a5914b1f629119f0fe65b5346558c925b", null ],
    [ "register_decoder", "heif__plugin__registry_8h.html#ac2c6e9438477ae5637c4fc808a5bf2b7", null ],
    [ "register_encoder", "heif__plugin__registry_8h.html#af4bf53d12ee4567e0709a58e3303a078", null ],
    [ "s_decoder_plugins", "heif__plugin__registry_8h.html#a49fc986d9f0469ac7cda07d4fd3fce8f", null ]
];