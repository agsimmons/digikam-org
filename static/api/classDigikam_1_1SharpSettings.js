var classDigikam_1_1SharpSettings =
[
    [ "SharpSettings", "classDigikam_1_1SharpSettings.html#aff08345ad525ecd5d2d76a610d161dac", null ],
    [ "~SharpSettings", "classDigikam_1_1SharpSettings.html#a1bfd9561d74cb24a9e33e622955e392b", null ],
    [ "defaultSettings", "classDigikam_1_1SharpSettings.html#af2e6f30d88b7009d02dc5aaa7dd41e54", null ],
    [ "loadSettings", "classDigikam_1_1SharpSettings.html#a5e870f1ae7239f8ea102ae1a9cd99a0b", null ],
    [ "readSettings", "classDigikam_1_1SharpSettings.html#ab448ca50f49245959ff902d4d3e9e226", null ],
    [ "resetToDefault", "classDigikam_1_1SharpSettings.html#a98574d249a471d0c15b7c3acd7ac537b", null ],
    [ "saveAsSettings", "classDigikam_1_1SharpSettings.html#a583e1ee26f13bfb6cdc34402b9333475", null ],
    [ "setSettings", "classDigikam_1_1SharpSettings.html#a78c304be09b4fb5796cddd70f84fa904", null ],
    [ "settings", "classDigikam_1_1SharpSettings.html#ac4c0e1711cc447d597ee1c3376ed956d", null ],
    [ "signalSettingsChanged", "classDigikam_1_1SharpSettings.html#a5af75e23a220d9dd6d5d8c27ec7aacdb", null ],
    [ "writeSettings", "classDigikam_1_1SharpSettings.html#ab7738ee52031bdc0c32962af9ba6ec6c", null ]
];