var classDigikamGenericPresentationPlugin_1_1PresentationGL =
[
    [ "PresentationGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#aa807101ac82c0ea7211dd61df2f7486d", null ],
    [ "~PresentationGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#ab15554f1ad71d352bda1b001fec0954f", null ],
    [ "checkOpenGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a4d3bca958cad0042f753583d64b73d66", null ],
    [ "initializeGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#aaf1e582f79b526542c226ead1847bf62", null ],
    [ "keyPressEvent", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a008255dd09afd92145b4835184b19df5", null ],
    [ "mouseMoveEvent", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a44a865d01ceb63766ce7de2df145ec79", null ],
    [ "mousePressEvent", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a98996c30a35ab6b99de6410078df90f4", null ],
    [ "paintGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a1952b1027d81f439eaaa0d94a3860731", null ],
    [ "registerEffects", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a4bbd15e3c58c6c5b6f4bcdbed8d4a491", null ],
    [ "resizeGL", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#a3c9e17b8a8e1cd220a069a592820a763", null ],
    [ "wheelEvent", "classDigikamGenericPresentationPlugin_1_1PresentationGL.html#aedbf41fd2a4374d607489e7dcc0ba165", null ]
];