var dir_5bc05b262995ede3fe81a63758a9845e =
[
    [ "altlangstringedit.cpp", "altlangstringedit_8cpp.html", null ],
    [ "altlangstringedit.h", "altlangstringedit_8h.html", [
      [ "AltLangStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit.html", "classDigikamGenericMetadataEditPlugin_1_1AltLangStringsEdit" ]
    ] ],
    [ "limitedtextedit.cpp", "limitedtextedit_8cpp.html", null ],
    [ "limitedtextedit.h", "limitedtextedit_8h.html", [
      [ "LimitedTextEdit", "classDigikamGenericMetadataEditPlugin_1_1LimitedTextEdit.html", "classDigikamGenericMetadataEditPlugin_1_1LimitedTextEdit" ]
    ] ],
    [ "metadatacheckbox.cpp", "metadatacheckbox_8cpp.html", null ],
    [ "metadatacheckbox.h", "metadatacheckbox_8h.html", [
      [ "MetadataCheckBox", "classDigikamGenericMetadataEditPlugin_1_1MetadataCheckBox.html", "classDigikamGenericMetadataEditPlugin_1_1MetadataCheckBox" ]
    ] ],
    [ "metadataedit.cpp", "metadataedit_8cpp.html", null ],
    [ "metadataedit.h", "metadataedit_8h.html", [
      [ "MetadataEditDialog", "classDigikamGenericMetadataEditPlugin_1_1MetadataEditDialog.html", "classDigikamGenericMetadataEditPlugin_1_1MetadataEditDialog" ]
    ] ],
    [ "multistringsedit.cpp", "multistringsedit_8cpp.html", null ],
    [ "multistringsedit.h", "multistringsedit_8h.html", [
      [ "MultiStringsEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit.html", "classDigikamGenericMetadataEditPlugin_1_1MultiStringsEdit" ]
    ] ],
    [ "multivaluesedit.cpp", "multivaluesedit_8cpp.html", null ],
    [ "multivaluesedit.h", "multivaluesedit_8h.html", [
      [ "MultiValuesEdit", "classDigikamGenericMetadataEditPlugin_1_1MultiValuesEdit.html", "classDigikamGenericMetadataEditPlugin_1_1MultiValuesEdit" ]
    ] ],
    [ "objectattributesedit.cpp", "objectattributesedit_8cpp.html", null ],
    [ "objectattributesedit.h", "objectattributesedit_8h.html", [
      [ "ObjectAttributesEdit", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit.html", "classDigikamGenericMetadataEditPlugin_1_1ObjectAttributesEdit" ]
    ] ]
];