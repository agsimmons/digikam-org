var dir_2082e95f8e90b6623d0c4293a2ddae36 =
[
    [ "gdrive", "dir_b96355d9c3aa887ca60a1015fe62191f.html", "dir_b96355d9c3aa887ca60a1015fe62191f" ],
    [ "gphoto", "dir_f15638cef49bb98a467dffdf75ec9c4d.html", "dir_f15638cef49bb98a467dffdf75ec9c4d" ],
    [ "gsitem.h", "gsitem_8h.html", "gsitem_8h" ],
    [ "gsnewalbumdlg.cpp", "gsnewalbumdlg_8cpp.html", null ],
    [ "gsnewalbumdlg.h", "gsnewalbumdlg_8h.html", [
      [ "GSNewAlbumDlg", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg.html", "classDigikamGenericGoogleServicesPlugin_1_1GSNewAlbumDlg" ]
    ] ],
    [ "gsplugin.cpp", "gsplugin_8cpp.html", null ],
    [ "gsplugin.h", "gsplugin_8h.html", "gsplugin_8h" ],
    [ "gsreplacedlg.cpp", "gsreplacedlg_8cpp.html", null ],
    [ "gsreplacedlg.h", "gsreplacedlg_8h.html", "gsreplacedlg_8h" ],
    [ "gstalkerbase.cpp", "gstalkerbase_8cpp.html", null ],
    [ "gstalkerbase.h", "gstalkerbase_8h.html", [
      [ "GSTalkerBase", "classDigikamGenericGoogleServicesPlugin_1_1GSTalkerBase.html", "classDigikamGenericGoogleServicesPlugin_1_1GSTalkerBase" ]
    ] ],
    [ "gswidget.cpp", "gswidget_8cpp.html", null ],
    [ "gswidget.h", "gswidget_8h.html", "gswidget_8h" ],
    [ "gswindow.cpp", "gswindow_8cpp.html", null ],
    [ "gswindow.h", "gswindow_8h.html", [
      [ "GSWindow", "classDigikamGenericGoogleServicesPlugin_1_1GSWindow.html", "classDigikamGenericGoogleServicesPlugin_1_1GSWindow" ]
    ] ]
];