var dir_823920b365b0240a33dfcfaa917cbecf =
[
    [ "twitteritem.h", "twitteritem_8h.html", [
      [ "TwAlbum", "classDigikamGenericTwitterPlugin_1_1TwAlbum.html", "classDigikamGenericTwitterPlugin_1_1TwAlbum" ],
      [ "TwPhoto", "classDigikamGenericTwitterPlugin_1_1TwPhoto.html", "classDigikamGenericTwitterPlugin_1_1TwPhoto" ],
      [ "TwUser", "classDigikamGenericTwitterPlugin_1_1TwUser.html", "classDigikamGenericTwitterPlugin_1_1TwUser" ]
    ] ],
    [ "twittermpform.cpp", "twittermpform_8cpp.html", null ],
    [ "twittermpform.h", "twittermpform_8h.html", "twittermpform_8h" ],
    [ "twitternewalbumdlg.cpp", "twitternewalbumdlg_8cpp.html", null ],
    [ "twitternewalbumdlg.h", "twitternewalbumdlg_8h.html", [
      [ "TwNewAlbumDlg", "classDigikamGenericTwitterPlugin_1_1TwNewAlbumDlg.html", "classDigikamGenericTwitterPlugin_1_1TwNewAlbumDlg" ]
    ] ],
    [ "twitterplugin.cpp", "twitterplugin_8cpp.html", null ],
    [ "twitterplugin.h", "twitterplugin_8h.html", "twitterplugin_8h" ],
    [ "twittertalker.cpp", "twittertalker_8cpp.html", "twittertalker_8cpp" ],
    [ "twittertalker.h", "twittertalker_8h.html", [
      [ "TwTalker", "classDigikamGenericTwitterPlugin_1_1TwTalker.html", "classDigikamGenericTwitterPlugin_1_1TwTalker" ]
    ] ],
    [ "twitterwidget.cpp", "twitterwidget_8cpp.html", null ],
    [ "twitterwidget.h", "twitterwidget_8h.html", [
      [ "TwWidget", "classDigikamGenericTwitterPlugin_1_1TwWidget.html", "classDigikamGenericTwitterPlugin_1_1TwWidget" ]
    ] ],
    [ "twitterwindow.cpp", "twitterwindow_8cpp.html", null ],
    [ "twitterwindow.h", "twitterwindow_8h.html", [
      [ "TwWindow", "classDigikamGenericTwitterPlugin_1_1TwWindow.html", "classDigikamGenericTwitterPlugin_1_1TwWindow" ]
    ] ]
];