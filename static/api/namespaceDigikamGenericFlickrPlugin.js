var namespaceDigikamGenericFlickrPlugin =
[
    [ "FlickrList", "classDigikamGenericFlickrPlugin_1_1FlickrList.html", "classDigikamGenericFlickrPlugin_1_1FlickrList" ],
    [ "FlickrListViewItem", "classDigikamGenericFlickrPlugin_1_1FlickrListViewItem.html", "classDigikamGenericFlickrPlugin_1_1FlickrListViewItem" ],
    [ "FlickrMPForm", "classDigikamGenericFlickrPlugin_1_1FlickrMPForm.html", "classDigikamGenericFlickrPlugin_1_1FlickrMPForm" ],
    [ "FlickrNewAlbumDlg", "classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg.html", "classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg" ],
    [ "FlickrPlugin", "classDigikamGenericFlickrPlugin_1_1FlickrPlugin.html", "classDigikamGenericFlickrPlugin_1_1FlickrPlugin" ],
    [ "FlickrTalker", "classDigikamGenericFlickrPlugin_1_1FlickrTalker.html", "classDigikamGenericFlickrPlugin_1_1FlickrTalker" ],
    [ "FlickrWidget", "classDigikamGenericFlickrPlugin_1_1FlickrWidget.html", "classDigikamGenericFlickrPlugin_1_1FlickrWidget" ],
    [ "FlickrWindow", "classDigikamGenericFlickrPlugin_1_1FlickrWindow.html", "classDigikamGenericFlickrPlugin_1_1FlickrWindow" ],
    [ "FPhotoInfo", "classDigikamGenericFlickrPlugin_1_1FPhotoInfo.html", "classDigikamGenericFlickrPlugin_1_1FPhotoInfo" ],
    [ "FPhotoSet", "classDigikamGenericFlickrPlugin_1_1FPhotoSet.html", "classDigikamGenericFlickrPlugin_1_1FPhotoSet" ]
];