var dir_34d5a3e5a770220ca2e4025239cabe53 =
[
    [ "perspectivematrix.cpp", "perspectivematrix_8cpp.html", null ],
    [ "perspectivematrix.h", "perspectivematrix_8h.html", [
      [ "PerspectiveMatrix", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix" ]
    ] ],
    [ "perspectivetool.cpp", "perspectivetool_8cpp.html", null ],
    [ "perspectivetool.h", "perspectivetool_8h.html", [
      [ "PerspectiveTool", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTool.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTool" ]
    ] ],
    [ "perspectivetoolplugin.cpp", "perspectivetoolplugin_8cpp.html", null ],
    [ "perspectivetoolplugin.h", "perspectivetoolplugin_8h.html", "perspectivetoolplugin_8h" ],
    [ "perspectivetriangle.cpp", "perspectivetriangle_8cpp.html", "perspectivetriangle_8cpp" ],
    [ "perspectivetriangle.h", "perspectivetriangle_8h.html", [
      [ "PerspectiveTriangle", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTriangle.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveTriangle" ]
    ] ],
    [ "perspectivewidget.cpp", "perspectivewidget_8cpp.html", null ],
    [ "perspectivewidget.h", "perspectivewidget_8h.html", [
      [ "PerspectiveWidget", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveWidget.html", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveWidget" ]
    ] ]
];