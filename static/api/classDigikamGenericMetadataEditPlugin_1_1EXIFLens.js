var classDigikamGenericMetadataEditPlugin_1_1EXIFLens =
[
    [ "EXIFLens", "classDigikamGenericMetadataEditPlugin_1_1EXIFLens.html#a67dd67c0dd53e120599131030512fe38", null ],
    [ "~EXIFLens", "classDigikamGenericMetadataEditPlugin_1_1EXIFLens.html#a24fc5ee08bc0ac12f02ea0fa2004970c", null ],
    [ "applyMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFLens.html#a0cdc261be9024c4e1a649593329a1fa1", null ],
    [ "readMetadata", "classDigikamGenericMetadataEditPlugin_1_1EXIFLens.html#adc0665e8575196c1789585d7047b74de", null ],
    [ "signalModified", "classDigikamGenericMetadataEditPlugin_1_1EXIFLens.html#a5f4dc8bbff84a903cca035d945042145", null ]
];