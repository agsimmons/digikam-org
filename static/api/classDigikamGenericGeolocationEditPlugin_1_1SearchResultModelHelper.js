var classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper =
[
    [ "PropertyFlag", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ab61bd82440e1c5e5b0b7365c217ac9d2", [
      [ "FlagNull", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ab61bd82440e1c5e5b0b7365c217ac9d2a295f0558874c38bfbc76a9445df6b34d", null ],
      [ "FlagVisible", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ab61bd82440e1c5e5b0b7365c217ac9d2ace610e1038914b9d87c662ed7c972e08", null ],
      [ "FlagMovable", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ab61bd82440e1c5e5b0b7365c217ac9d2a2639b80d36d4e27d50e8cea1c9277dbc", null ],
      [ "FlagSnaps", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ab61bd82440e1c5e5b0b7365c217ac9d2acb5b1e9b32d61e8fb89c3d03c7ae78fd", null ]
    ] ],
    [ "SearchResultModelHelper", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a48a8e17d3d0792efadc2965cc0a8447b", null ],
    [ "~SearchResultModelHelper", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a73e00f0429901a6626bc2d01ff6aa688", null ],
    [ "bestRepresentativeIndexFromList", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ac0dd6dcde061defafdca124e87376989", null ],
    [ "itemCoordinates", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#aa48cc588245413cd030959dd55468abf", null ],
    [ "itemFlags", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a794062a04b62e865b3899096897f2625", null ],
    [ "itemIcon", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a4273eb8e0e1e2640f52de75525ddc11e", null ],
    [ "model", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a532eb7d20d156f4faf89497dceaa1aaa", null ],
    [ "modelFlags", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a2f7ec8efde7e0c84be60724379bcf5e1", null ],
    [ "onIndicesClicked", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ac12fcaa1fb06a5e70e7e5a8fa3e7fb77", null ],
    [ "onIndicesMoved", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a332d0099d3ad1f2541d47988c55e3329", null ],
    [ "pixmapFromRepresentativeIndex", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a97c97e29a7d9533f884890595e76f068", null ],
    [ "selectionModel", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ad46eea544d555574eed3f696b13bdf52", null ],
    [ "setVisibility", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a73ae6cc8d1d76645dff37a60458ea517", null ],
    [ "signalModelChangedDrastically", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ac2dd9fa6f871867bf4ab5795e6605ae5", null ],
    [ "signalThumbnailAvailableForIndex", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a22293f3431309f21f0ed3b7e85af67ec", null ],
    [ "signalUndoCommand", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a7077077a7ca245582af16f0f8f76884c", null ],
    [ "signalVisibilityChanged", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#ad4ccc06eb719e6311d09e88d85b95126", null ],
    [ "snapItemsTo", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#aea4acb46a64dde44fc471043eeb820e4", null ],
    [ "snapItemsTo", "classDigikamGenericGeolocationEditPlugin_1_1SearchResultModelHelper.html#a284d0b4560522534f2966c90e14bc7f3", null ]
];