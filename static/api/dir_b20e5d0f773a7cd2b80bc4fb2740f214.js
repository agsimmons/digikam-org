var dir_b20e5d0f773a7cd2b80bc4fb2740f214 =
[
    [ "camerafolderdialog.cpp", "camerafolderdialog_8cpp.html", null ],
    [ "camerafolderdialog.h", "camerafolderdialog_8h.html", [
      [ "CameraFolderDialog", "classDigikam_1_1CameraFolderDialog.html", "classDigikam_1_1CameraFolderDialog" ]
    ] ],
    [ "camerainfodialog.cpp", "camerainfodialog_8cpp.html", null ],
    [ "camerainfodialog.h", "camerainfodialog_8h.html", [
      [ "CameraInfoDialog", "classDigikam_1_1CameraInfoDialog.html", "classDigikam_1_1CameraInfoDialog" ]
    ] ],
    [ "cameramessagebox.cpp", "cameramessagebox_8cpp.html", null ],
    [ "cameramessagebox.h", "cameramessagebox_8h.html", [
      [ "CameraItem", "classDigikam_1_1CameraItem.html", "classDigikam_1_1CameraItem" ],
      [ "CameraItemList", "classDigikam_1_1CameraItemList.html", "classDigikam_1_1CameraItemList" ],
      [ "CameraMessageBox", "classDigikam_1_1CameraMessageBox.html", null ]
    ] ],
    [ "capturedlg.cpp", "capturedlg_8cpp.html", null ],
    [ "capturedlg.h", "capturedlg_8h.html", [
      [ "CaptureDlg", "classDigikam_1_1CaptureDlg.html", "classDigikam_1_1CaptureDlg" ]
    ] ]
];