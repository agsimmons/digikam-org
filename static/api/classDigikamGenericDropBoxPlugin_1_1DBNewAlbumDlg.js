var classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg =
[
    [ "DBNewAlbumDlg", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a3cf4849fd9e64c28cea47a9da8817b95", null ],
    [ "~DBNewAlbumDlg", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a2216d19f2a0067c019208e7070d429e0", null ],
    [ "addToMainLayout", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#add6de4ea3bfd2840ec32de4b3ec3e62d", null ],
    [ "getAlbumBox", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a2a796c4f2807c1e8ed6cf40affa5afa0", null ],
    [ "getBaseAlbumProperties", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#ab06d1922af37d55a29df704167231037", null ],
    [ "getButtonBox", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a17db77c1fc6054529b012766af159b55", null ],
    [ "getDateTimeEdit", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#af7f30d95ea761e4c49e7970894690591", null ],
    [ "getDescEdit", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a48787b212858a75b47331abf230f4412", null ],
    [ "getFolderTitle", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#accffc26dc8a6be9d72367f6fa72cad27", null ],
    [ "getLocEdit", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a3578365a1d3f6662decfb76b077611d2", null ],
    [ "getMainWidget", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a12076a20f061f220d81a951734d29a67", null ],
    [ "getTitleEdit", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a356426352dc81b5419852be6b742397b", null ],
    [ "hideDateTime", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a7d7d1c674a00240ed5628a621dd1471d", null ],
    [ "hideDesc", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a90e69c3d0fbae903d3e3db5268aa8df1", null ],
    [ "hideLocation", "classDigikamGenericDropBoxPlugin_1_1DBNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370", null ]
];