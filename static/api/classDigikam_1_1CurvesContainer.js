var classDigikam_1_1CurvesContainer =
[
    [ "CurvesContainer", "classDigikam_1_1CurvesContainer.html#a2ab4afa503b0ba3ff99ab6d508a09817", null ],
    [ "CurvesContainer", "classDigikam_1_1CurvesContainer.html#a87399e04aa8584eee21091f5016c0a8c", null ],
    [ "initialize", "classDigikam_1_1CurvesContainer.html#ad4eb36a1a4648b880b122bb62e8d674b", null ],
    [ "isEmpty", "classDigikam_1_1CurvesContainer.html#aa2ba47b3d9541aa8035dbad06cb83bcc", null ],
    [ "isStoredLosslessly", "classDigikam_1_1CurvesContainer.html#ad0cbae1c645bd56603765e414039094b", null ],
    [ "operator==", "classDigikam_1_1CurvesContainer.html#ab73eb2b5697f912bfede63b9060ee6e5", null ],
    [ "writeToFilterAction", "classDigikam_1_1CurvesContainer.html#afe4d8970e66438360718b54882562af8", null ],
    [ "curvesType", "classDigikam_1_1CurvesContainer.html#ad27e02ef75c34e936c1673eb1a033862", null ],
    [ "sixteenBit", "classDigikam_1_1CurvesContainer.html#ae0b385ae4d9b62c33bf4e2e4a23bdfc6", null ],
    [ "values", "classDigikam_1_1CurvesContainer.html#a2afaa8836bd7ac5a6f69cebf7582a309", null ]
];