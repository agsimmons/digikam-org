var namespaceDigikamGenericOneDrivePlugin =
[
    [ "ODFolder", "classDigikamGenericOneDrivePlugin_1_1ODFolder.html", "classDigikamGenericOneDrivePlugin_1_1ODFolder" ],
    [ "ODMPForm", "classDigikamGenericOneDrivePlugin_1_1ODMPForm.html", "classDigikamGenericOneDrivePlugin_1_1ODMPForm" ],
    [ "ODNewAlbumDlg", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg.html", "classDigikamGenericOneDrivePlugin_1_1ODNewAlbumDlg" ],
    [ "ODPhoto", "classDigikamGenericOneDrivePlugin_1_1ODPhoto.html", "classDigikamGenericOneDrivePlugin_1_1ODPhoto" ],
    [ "ODPlugin", "classDigikamGenericOneDrivePlugin_1_1ODPlugin.html", "classDigikamGenericOneDrivePlugin_1_1ODPlugin" ],
    [ "ODTalker", "classDigikamGenericOneDrivePlugin_1_1ODTalker.html", "classDigikamGenericOneDrivePlugin_1_1ODTalker" ],
    [ "ODWidget", "classDigikamGenericOneDrivePlugin_1_1ODWidget.html", "classDigikamGenericOneDrivePlugin_1_1ODWidget" ],
    [ "ODWindow", "classDigikamGenericOneDrivePlugin_1_1ODWindow.html", "classDigikamGenericOneDrivePlugin_1_1ODWindow" ]
];