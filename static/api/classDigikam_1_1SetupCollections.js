var classDigikam_1_1SetupCollections =
[
    [ "CollectionsTab", "classDigikam_1_1SetupCollections.html#a8a8aea8d03f5e60bc275235bd78c6a8a", [
      [ "Collections", "classDigikam_1_1SetupCollections.html#a8a8aea8d03f5e60bc275235bd78c6a8aacfad4266dc7edf0837fb8c6fe796ae75", null ],
      [ "IgnoreDirs", "classDigikam_1_1SetupCollections.html#a8a8aea8d03f5e60bc275235bd78c6a8aab7af15a2a11895b1d342f00b8fd7f437", null ]
    ] ],
    [ "SetupCollections", "classDigikam_1_1SetupCollections.html#a0ee9015bf999367e6a3d6ae3c7573936", null ],
    [ "~SetupCollections", "classDigikam_1_1SetupCollections.html#a6fba9ab5c5f230f0d34ca898b32b6e26", null ],
    [ "applySettings", "classDigikam_1_1SetupCollections.html#a3f29699fed234702cb4e9a80d20843df", null ]
];