var classDigikamGenericImageShackPlugin_1_1ImageShackTalker =
[
    [ "ImageShackTalker", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a0d79c813c2d32c9b212b257fb96c07b4", null ],
    [ "~ImageShackTalker", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a222cdb72a2cc0e626bda49065cb9df87", null ],
    [ "authenticate", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a1a365efbbd554ac5d138b823a45d7b8e", null ],
    [ "cancel", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#ad5ad9bb20d96b02478d4c5d9fe421b22", null ],
    [ "cancelLogIn", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a1fa81a2e9c498fa8f7d274deba8c3daf", null ],
    [ "getGalleries", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#abcef78921ef2a9fbe5b83f12474ee7dc", null ],
    [ "signalAddPhotoDone", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a259dd70085be6dbf2b14b41e3ea11ea7", null ],
    [ "signalBusy", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a5d1848e17f697129a98ac4684699943c", null ],
    [ "signalGetGalleriesDone", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a1f95d560de5f594a78f406ad3e6d64ad", null ],
    [ "signalJobInProgress", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a73b305cd92ce30fa0574cf2c6d4cbfca", null ],
    [ "signalLoginDone", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a10ca3350cb979fdbadebfc5f66b3ed8b", null ],
    [ "signalUpdateGalleries", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#ae742a1f0154aa041833c9b3c11cf792e", null ],
    [ "uploadItem", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a44758dc00dde345b95966af144eddb7d", null ],
    [ "uploadItemToGallery", "classDigikamGenericImageShackPlugin_1_1ImageShackTalker.html#a4b596aa98f71ae875def81e3e44a045e", null ]
];