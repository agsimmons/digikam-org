var dir_5a99070ddcecbb1503194f5405759d79 =
[
    [ "ditemslist.cpp", "ditemslist_8cpp.html", null ],
    [ "ditemslist.h", "ditemslist_8h.html", "ditemslist_8h" ],
    [ "ditemslist_item.cpp", "ditemslist__item_8cpp.html", null ],
    [ "ditemslist_p.h", "ditemslist__p_8h.html", [
      [ "CtrlButton", "classDigikam_1_1CtrlButton.html", "classDigikam_1_1CtrlButton" ]
    ] ],
    [ "ditemslist_view.cpp", "ditemslist__view_8cpp.html", null ],
    [ "dplugindialog.cpp", "dplugindialog_8cpp.html", null ],
    [ "dplugindialog.h", "dplugindialog_8h.html", [
      [ "DPluginDialog", "classDigikam_1_1DPluginDialog.html", "classDigikam_1_1DPluginDialog" ]
    ] ],
    [ "dpreviewimage.cpp", "dpreviewimage_8cpp.html", null ],
    [ "dpreviewimage.h", "dpreviewimage_8h.html", [
      [ "DPreviewImage", "classDigikam_1_1DPreviewImage.html", "classDigikam_1_1DPreviewImage" ],
      [ "DSelectionItem", "classDigikam_1_1DSelectionItem.html", "classDigikam_1_1DSelectionItem" ]
    ] ],
    [ "dpreviewmanager.cpp", "dpreviewmanager_8cpp.html", null ],
    [ "dpreviewmanager.h", "dpreviewmanager_8h.html", [
      [ "DPreviewManager", "classDigikam_1_1DPreviewManager.html", "classDigikam_1_1DPreviewManager" ]
    ] ],
    [ "dsavesettingswidget.cpp", "dsavesettingswidget_8cpp.html", null ],
    [ "dsavesettingswidget.h", "dsavesettingswidget_8h.html", [
      [ "DSaveSettingsWidget", "classDigikam_1_1DSaveSettingsWidget.html", "classDigikam_1_1DSaveSettingsWidget" ]
    ] ],
    [ "dwizarddlg.cpp", "dwizarddlg_8cpp.html", null ],
    [ "dwizarddlg.h", "dwizarddlg_8h.html", [
      [ "DWizardDlg", "classDigikam_1_1DWizardDlg.html", "classDigikam_1_1DWizardDlg" ]
    ] ],
    [ "dwizardpage.cpp", "dwizardpage_8cpp.html", null ],
    [ "dwizardpage.h", "dwizardpage_8h.html", [
      [ "DWizardPage", "classDigikam_1_1DWizardPage.html", "classDigikam_1_1DWizardPage" ]
    ] ]
];