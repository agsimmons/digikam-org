var dir_baa823fd47e8a2874290a610247ce11f =
[
    [ "backend-geonames-rg.cpp", "backend-geonames-rg_8cpp.html", null ],
    [ "backend-geonames-rg.h", "backend-geonames-rg_8h.html", [
      [ "BackendGeonamesRG", "classDigikam_1_1BackendGeonamesRG.html", "classDigikam_1_1BackendGeonamesRG" ]
    ] ],
    [ "backend-geonamesUS-rg.cpp", "backend-geonamesUS-rg_8cpp.html", null ],
    [ "backend-geonamesUS-rg.h", "backend-geonamesUS-rg_8h.html", [
      [ "BackendGeonamesUSRG", "classDigikam_1_1BackendGeonamesUSRG.html", "classDigikam_1_1BackendGeonamesUSRG" ]
    ] ],
    [ "backend-osm-rg.cpp", "backend-osm-rg_8cpp.html", null ],
    [ "backend-osm-rg.h", "backend-osm-rg_8h.html", [
      [ "BackendOsmRG", "classDigikam_1_1BackendOsmRG.html", "classDigikam_1_1BackendOsmRG" ]
    ] ],
    [ "backend-rg.cpp", "backend-rg_8cpp.html", null ],
    [ "backend-rg.h", "backend-rg_8h.html", [
      [ "RGBackend", "classDigikam_1_1RGBackend.html", "classDigikam_1_1RGBackend" ]
    ] ],
    [ "backendgooglemaps.cpp", "backendgooglemaps_8cpp.html", null ],
    [ "backendgooglemaps.h", "backendgooglemaps_8h.html", [
      [ "BackendGoogleMaps", "classDigikam_1_1BackendGoogleMaps.html", "classDigikam_1_1BackendGoogleMaps" ]
    ] ],
    [ "backendmarble.cpp", "backendmarble_8cpp.html", null ],
    [ "backendmarble.h", "backendmarble_8h.html", [
      [ "BackendMarble", "classDigikam_1_1BackendMarble.html", "classDigikam_1_1BackendMarble" ]
    ] ],
    [ "backendmarblelayer.cpp", "backendmarblelayer_8cpp.html", null ],
    [ "backendmarblelayer.h", "backendmarblelayer_8h.html", [
      [ "BackendMarbleLayer", "classDigikam_1_1BackendMarbleLayer.html", "classDigikam_1_1BackendMarbleLayer" ]
    ] ],
    [ "mapbackend.cpp", "mapbackend_8cpp.html", null ],
    [ "mapbackend.h", "mapbackend_8h.html", [
      [ "MapBackend", "classDigikam_1_1MapBackend.html", "classDigikam_1_1MapBackend" ]
    ] ]
];