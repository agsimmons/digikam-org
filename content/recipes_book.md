---
date: "2017-03-21"
title: "digiKam Recipes Book"
author: "Dmitri Popov"
description: "Master digiKam with the digiKam Recipes book!"
category: "documentation"
aliases: "digikamrecipes"
---

digiKam is an immensely powerful photo management application, and mastering it requires time and effort. This book can help you to learn the ropes in the most efficient manner. Instead of going through each and every menu item and feature, the book provides a task-oriented description of digiKam's functionality that can help you to get the most out of this versatile tool. The book offers easy-to-follow instructions on how to organize and manage photos, process RAW files, edit images and apply various effects, export and publish photos, and much more.

![digiKam Recipes Reader](/img/digikam-recipes-reader.png)

Facts about the digiKam Recipes book:

- This is the first and only ebook about digiKam.
- The book was written in close cooperation with the digiKam developers.
- 50% of all book sales go to the digiKam project.</li>
- The book is DRM-free, so you can read it using any ebook reader or software that supports the EPUB or AZW3 format.
- You'll receive all future editions of the book free of charge.

## What readers say about digiKam Recipes

_Dmitri's book is a great guidebook for those new to digiKam, as well as those already using it. I found that out for myself when I purchased the digiKam Recipes recently. It's a thorough overview, with examples, of all of the features that digiKam provides. There are tips for doing things that even those of us who have been using the software for years may not have stumbled upon. **Highly recommended!**_ --Scott Gomez

_I really like the regular updates you get, that alone was worth every penny. A living document, as they should all be!_ --Peter Teuben

_This is worth far more that the purchase price. I was struggling with digiKam prior to buying this book. Now I'm back to enjoying the computer work associated with digital photography._ --Joh H (Amazon.com)

_This book is a must-have if you use digiKam to edit, manage and organize your digital photos. For two good reasons: firstly, because it is well written, is getting better and better over time, and has a lot of nice tricks in it. Secondly, because buying it gives money to the project, which is a great thing to do._ --Romano Giannetti (Amazon.com)

_DigiKam is an extremely powerful tool; this ebook helps the user to get to grips with its sometimes complex interface and processes; also the project - free and open-source - gets a few quid from your purchase. So if there's something about the monopoly of Photoshop that puts you off, then why not give DigiKam a go? This will help to get you started._ --M. Pat (Amazon.co.uk)

## Buy digiKam Recipes

Buy _digiKam Recipes_ on [Google Play](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) or [Gumroad](https://gumroad.com/l/digikamrecipes).

The ebook released under the <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> license, so you are free to share the ebook and modify it as long as you share your modifications.

I hope you'll find the ebook useful, and if you have any comments, ideas, and suggestions, feel free to contact me at <a href="mailto:dmpop@linux.com">dmpop@linux.com</a>.
