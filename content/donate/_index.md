---
date: "2017-03-21"
title: "Donate"
author: "digiKam Team"
description: "Help support the continued development of digiKam"
category: "donations"
aliases: "/donation"
menu: "navbar"
---

Do you love digiKam?
Would you like to see the project grow and become better in the future?

![Donate Menu](/img/content/donate/donate_menu.png)

One of the best ways to help and support our work is to donate to the project.
We use funds to purchase equipment relevant to photography that we want to support in digiKam.
We also find value in being able to attend open source events such as meetings and coding sprints.
Donations help to offset the costs that team members incur for attending these events.

As described in [The Economics of Open Source Donations](http://www.packtpub.com/article/the-economics-of-open-source-donations),
financial contributions play a crucial role in support Free/Libre and Open Source Software projects.

Almost every person and every organization can support the digiKam project. Whether you are an individual,
or represent a company, an administration or a University, your contribution can take many forms:


### For individuals

#### Make a Donation

If you would like to help digiKam project to promote the development and usage of the digiKam photo manager and its by-products,
you can use Paypal buttons listed below. Making donations with **PayPal**, is fast, free and secure.

<table>
<tbody><tr>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="amount" value="20.00" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

<td width="10">&nbsp;</td>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="amount" value="30.00" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

<td width="10">&nbsp;</td>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="amount" value="40.00" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

<td width="10">&nbsp;</td>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="amount" value="50.00" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

<td width="10">&nbsp;</td>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="amount" value="100.00" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

<td width="10">&nbsp;</td>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="amount" value="200.00" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

<td width="10">&nbsp;</td>

<td align="center">
<form action="https://www.paypal.com/en/cgi-bin/webscr" method="post">
<input name="cmd" value="_xclick" type="hidden">
<input name="business" value="caulier.gilles@gmail.com" type="hidden">
<input name="item_name" value="Donation to the developers of digiKam" type="hidden">
<input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden">
<input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden">
<input name="bn" value="PP-DonationsBF" type="hidden">
<input src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" name="submit" alt="Donation" border="0" type="image">
</form>
</td>

</tr>

<tr>
<td align="center">20 euro</td>
<td width="10">&nbsp;</td>
<td align="center">30 euro</td>
<td width="10">&nbsp;</td>
<td align="center">40 euro</td>
<td width="10">&nbsp;</td>
<td align="center">50 euro</td>
<td width="10">&nbsp;</td>
<td align="center">100 euro</td>
<td width="10">&nbsp;</td>
<td align="center">200 euro</td>
<td width="10">&nbsp;</td>
<td align="center">Other</td>
</tr>
</tbody></table>

Your name will be added to the list below.

### For companies and administrations

Companies or administrations willing to become a new sponsor of digiKam project can reach us <a href="mailto:caulier dot gilles at gmail dot com">by e-mail</a> for details.

Thank you in advance for your generous donation...

### Thanks to Our Sponsors:

#### 2022

* Karl Ove Hufthammer
* Julian Tomasini
* Alberto Ponti
* Tobias Karch
* Daniel A Rankin
* Henrik Hemrin
* International Beauty Salon
* Frank Record
* Kristina Englert
* Dirk Schoettle
* Timothy Edwards
* Jose Luis Lopez Carrasco
* Manfred Tetzlaff
* Gerhard Scheikl
* Paul Bush
* Florian Steffens
* Ian Litton
* Evan Wolf
* Franz Voelker
* Thorsten Kuster
* Martin Zahnd
* Scott Kruger
* Hans-Ulrich Röscheisen
* Benjamin Teicher
* Andrew Lorien
* Dave Symington
* Didier Luthi
* Jonas Friedrich
* Peter Crosby
* Tuomas Koponen
* Philipp Wundrack
* Cory Clark
* Frank Reher
* Christian Pietzsch
* Esben Søndergård
* Kamil Stepinski
* Edward Wayt
* Axel Weißenberger
* Ancheng Deng
* Michael Hähnel
* Mario F.P. Vennen
* Mathias Lueg
* Henrik Hemrin
* Serge Muscat
* Jacob Mikkelsen
* Freek Dijkstra
* Jiří Čepelka
* Roman Adrian Spirgi
* Paruyr Gevorgyan
* Frode Gjedrem
* Sven Tappeser
* Trinity3 Design and Al
* Jean Puyravaud
* Lukas Brunnader
* David Hurst
* Richard Wiersma

#### 2021

* Julian Tomasini
* David Orchard
* Olof Söderström
* Karl Ove Hufthammer
* Bruno Freitag
* Tobias Karch
* Timm Lichte
* Andreas Weeber
* Ulrich Vogt
* Stephan Kranz
* Björn Fischer
* Michael Bräm
* Katharina Bös
* Martin Zahnd
* Mario F.P. Vennen
* Randall Wolf
* Frank Heessels
* Rich Camlin
* Hans-Ulrich Röscheisen
* Anne Moindrot
* Evan Wolf
* Lukas Brunnader
* Thomas Seymour
* Richard Newson
* Jan Morgils
* Peter Candaele
* Liokumovich Gregory
* Jose Oliver
* James McKeown
* BucketList Industries
* Raffaello Cigna
* Jana Läufer
* Peter Freitag
* Thorsten Kuster
* Michael Körner
* André Fabiani
* Andrea Dublaski
* Jean Paul Eby
* Michael Weber
* Filip Lobík
* Christian Ries
* Thomas Seymour
* Markus Kempf
* Gordon Lyman
* Goetz Becker
* Christian Griebl
* Dana Correia
* Sebastian Beer
* Hans-Peter Huth
* Kristina Englert
* Andrew Viquerat
* Alexander Boch
* Roberto Pagliara
* Jonas Friedrich
* Christian Ziegler
* Basil Rowe
* Daniel Westrich
* Andrew Snyder
* Michal Mak
* Martin Gruhn
* Lukas Matasovsky
* Jose Antonio García Leiva
* Marcus Turner
* Felix Widmaier
* Esben Søndergård
* Janice Freeland
* Norm Butler
* Wilfried Trautmann
* Tristan Stern
* Mihai Gui
* Chris Davis
* Robert Markovics
* Richard Wass
* Yann Peron
* William Wilson
* Michael Kessler
* AuxMemes
* Peter Johnson
* Bernd Dahmen
* Vojtech Zachar
* Dan Kettmann
* Paul Bush
* Harold Marietta
* Charles Conrade
* Franz Seidl
* Ludvig Nybogård
* Frank von Keutz
* James Yamasaki
* John Kaltenecker
* Laurent Pouvreau
* Lukas Spring
* Zachary Mathe
* Marcin Walter
* Christopher Weis
* Prabhakar Kasi
* Jag Talon
* Green Homes
* Patrick Coray
* Martin Seidel
* Ander Genua Trullos
* Renato Rolando
* Davolio Roberto
* Øystein Sund
* Thomas Mendoza
* Frank Haarmeier
* Martin Stich
* Thierry Moy
* Martin Kopecky
* Jan Jarrell
* Markus Liebscher
* Matthias Kiechle
* Gerome Meyer
* John Grubb
* Lennart Kaulmann
* Patrick Strick
* Richard Birney
* Horacio Atencio
* Anthoney Gerdes
* Andrea Lucchini
* Bret DesErmia
* Michael Schweizer
* Sasaki Ryuji
* Shantikumar Kulkarni
* Christoph Toussaint
* Hans-Ulrich Röscheisen
* Roland Schirling
* Geoffrey King
* Thomas Dr. Franke
* Jörg Rosenbauer
* Patrick Uhlmann
* Dick Saarloos
* Ilan Gleichman
* Robert Church
* Henrik Hemrin
* Artur Leśniewicz
* Pierre Kerff
* Laurent Espitallier
* Dejan Milosavljevic
* Alberto Fortini
* Bastian Bechtold
* Florian Diwald
* Ribes Rossiñol
* Valencic Andrej
* Marco Maffezzoli
* Gabi Sarkis
* John Dennison
* Mattia Verga
* Mauro Taraborelli
* Nicole Rosset
* Martin Süßkraut
* Frank Hürlimann
* David Manning
* Lorenz Insermini
* Arne Hansson
* Robert Helewka
* Ben Schoepski
* Christian Roth
* Otto Dämon
* Glen Scott
* Frederico Pflug
* Arnd Heider
* Olaf Müller
* Miles Frederik Delwig
* Stefan Bredenbroeker
* Pedro Bandeira

#### 2020

* Harm Berntsen
* Martin Zahnd
* Karl Ove Hufthammer
* Julian Tomasini
* Oguz Piri
* Fabio Monastero
* Michael Zabel
* Laurenz Notter
* Olof Söderström
* Robert Evert
* Andreas Nikoloudis
* Sebastian Schmitt
* Thorsten Kuster
* Jonas Friedrich
* Uwe van Olfen
* Shogo Hirota
* STEER PTY LTD
* Andrey Goreev
* Roberto Pagliara
* Matthias Guenther
* Karin Pütz
* Rodney MC Brindamour
* Emanuele Colò
* Zachary Cogswell
* Yves Gwenael
* Sven Illberger
* Georg Macku
* Eric Göpel
* Hans Munkejord
* Julian Hüsselmann
* štefan emrich
* David Kolöchter
* John Dennison
* Arne Hamann
* Peter Spielbauer
* Jose Cifuentes
* Alberto Garciaburgos Vijande
* Thomas Steffen
* Tom Browne
* ערן מרסה
* Michael Körner
* Roberta Rogers
* Florian Pospischil
* Andy Pollari
* Thierry Zundel
* Serge Muscat
* Hernán Schmidt
* Jean Puyravaud
* Falk Meyer
* Donald Unter Ecker
* Ulrich Rettmar
* Kelly Retzlaff
* Rwky.net
* Vishal Vishvanath
* Jože Vicman
* Jeroen Leijen
* Christian Comes
* Samuel Greiner
* Daan Wynen
* Bryan Walton
* Joseph Bushong
* Tomas Kuchta
* Dan Åkesson
* Horacio Atencio
* Michael Schweizer
* Dan Åkesson
* Robert Evert
* Marcus Seiler
* Jaap van den Dries
* Lothar Müller
* Raffaello Cigna
* Robert Hehenberger
* Arne Brüning
* asitemade4u
* Hans-Ulrich Röscheisen
* Eran Tromer
* Olaf Sterger
* Denis Vrac
* Dan Åkesson
* Michael Phelps
* Julien Lerouge
* BITS - Böller IT Solutions
* Rainer Krebber
* Sébastien Lemoine
* Philip Jocks
* Ben Schoepski
* Ian McCarthy
* Hans-Joachim Baader
* Nicolas Martinez
* Marco Hoppstädter
* Oliver Copeley-Williams
* Karel Veselý
* Adam Foster
* Harald Guse
* Frank Haarmeier
* Alexander Mehlhorn
* Aleksi Klasila
* Geoffrey King
* Christian Roth
* Vincent Petry
* Alexander Steinhöfer
* Luis Ribes Rossiñol
* Manuel Meinel
* Andreas Hölscher
* Olivier Croquette
* Jan Janzen
* Yves DuVerger
* Mark Christensen
* Martin Neuss
* Markku Vähäaho
* Balduin Metz
* Leon Loberman
* Kasper Winther
* Gary Teixeira
* Paruyr Gevorgyan
* Lorenz Insermini
* Dietmar Heitbrink
* 吴 凯
* Robert Moorehead
* Roland Rothe
* Kashif Khan
* Stefan Simik
* Daniel Lechner
* Jörg Rosenbauer
* Shantikumar Kulkarni
* Robert Helewka
* Emilio Srougo Alfille
* Hendricus Penning de Vries
* Stefan Bredenbroeker
* Frank Kluthe
* Maddalosso Dienstleistungen
* Raffaele Bressanutti

#### 2019

* Franco Feruglio
* Paul Mihai
* Adriano Armandi
* Xavier Arnau
* Horacio Atencio
* Jan Morgils
* Steve Scott
* Andreas Nikoloudis
* Peter Böhm
* Dirk Ziegelmeier
* Hans-Joachim Lange
* Tom Piel
* Didier Boschmans
* Tono Riesco
* HJ Hiddinga
* Kurt Liebezeit
* Serge Muscat
* Frank DeMarco
* Olof Söderström
* Aleksandr Patsekin
* James Elliott
* Marcin Wolcendorf
* Patrick van Elk
* Alberto Ponti
* Hans-Ulrich Röscheisen
* Gunnar Liedtke
* Miroslav Maric
* Alain Pouchard
* Marcin Wolcendorf
* Frank Haarmeier
* Cesare Gregorelli
* Shantikumar Kulkarni
* Jonathan Klimt
* Karl Ove Hufthammer
* Dirk Varding
* Massimiliano Arioli
* Patrick Coray
* Benjamin Quest
* Alexandre Belz
* Peter Jessup
* Mark Hofmann
* Otto Hakstetter
* Ruth Thomas
* Julian Tomasini
* Jeremy Clulow
* Timothy Edwards
* Martin Seidel
* Hrvoje Klasnić
* Christian Griebl
* Frederick Yahn
* Steven Robbins
* Bernd Pawliczek
* Raffaele Salmaso
* Harald Krieg
* Florian Darsy
* Jeremy Clulow
* Bozidar Avramovic
* Patrick McKenzie
* Raffaello Cigna
* Demian Korotchenko
* Mike Nishi
* Hans-Ulrich Röscheisen
* Sasha Vincic
* Karel Veselý
* Marty Mapes
* Shantikumar Kulkarni
* Lloyd Walls
* Igor Krivenko
* Yifei He
* Raul Antoniano Mateos
* Luis Ribes Rossiñol
* Chris Niewiarowski
* NAT Neuberger Anlagen-Technik AG
* Brandon Maynard
* Anna Lisa Melis
* Christoph Künzel
* Michael Schweizer
* Marcin Janczyk
* Raul Antoniano Mateos
* Andreas Löwenhagen
* Marcus Lucke
* Pedro Oliveira
* Thomas Wagner
* Geoffrey King
* Roger Foss
* Herve Le Roy
* Christian Wisén
* Robert Helewka
* Stefan Bredenbroeker
* Jonas Friedrich
* Jean-yves Leogane
* Pierre Kerff
* David Manning
* Laurent Espitallier
* Paul A Schroeter
* Florian Pospischil

---

#### 2018

* Craig Rhombs
* Richard Clayton
* Ivan Kral
* Matthias Liebich
* Patrick van Elk
* Andrey Goreev
* Tim Herinckx
* Jean-Pierre Imhof
* Robert Smith
* Timothy Edwards
* Saso Petrovic
* Robert Evert
* Anton Huber
* Stefan Sollner
* Bruce Gordon
* Serge Muscat
* Carlo Greggio
* Nicolas Kuenzler
* Jean Puyravaud
* Ralf Grimme
* Robert Powell
* Karl Ove Hufthammer
* Bernd Stahn
* Hetesi Tams
* Michael Rollnik
* Nikolay Cherkasov
* Torsten Schulz
* Oriol Jimenez Cilleruelo
* Karel Vesely
* Martin Grobe
* Hans-Ulrich Röscheisen
* Jose Miguel Sanchez Garcia
* Arne Brüning
* Ries Rommens Computer Support
* Olof Söderström
* Hans-Ulrich Röscheisen
* Graham Gibby
* Иващенко Тарас
* Tim Besard
* Shantikumar Kulkarni
* Franz Reich
* Elias Gabrielsson
* Paul Christophe
* Thomas Kolb
* Hendricus Penning de Vries
* Wes Williams
* Elmar Winkler
* Goetz Becker
* Martin Veverka
* heddy boubaker
* Markus Kempf
* David Mcinnis
* Rowan Wookey
* Damien De Mulatier
* John Rigg
* Matthias Wieser
* Jean-Philippe Lebrat
* Kurosch Sadjadi Nasab
* Ulrich Maurus
* Franck Quélain
* Michael Schweizer
* Andreas Lowenhagen
* David Manning
* Edward Lewis
* Arne Vob
* Lynda Metref
* Ronny Hermans
* Martin Zahnd
* Robert West
* Joseph Bushong
* Jorg Rosenbauer
* Robert Helewka
* Frederico Pflug
* Martin Stary

---

#### 2017

* Mike Bing
* Patrick van Elk
* Umberto Valentinotti
* Karl Ove Hufthammer
* Richard Clayton
* Francesco Bertoldi
* Stefan Grosse
* Prateek Panjla
* Frank Cook
* Jakub Svoboda
* Werner Lauckner
* Alberto Ponti
* Serge Muscat
* Karsten Defreese
* Derek Langsford
* Eric Gopel
* Michael Toussaint
* Martin Kaletsch
* Frank DeMarco
* Michael Ast
* Vojtech Zachar
* Sascha Holzel
* Fabrizio Borin
* Dejan Milosavljevic
* Laurent Espitallier
* Robert Peter
* Matthias Karl
* Geoffrey King
* Ilan Gleichman
* Ruben Begert
* David Hurst
* Jonas Kemmer
* Roman Riabenko
* Nils Pooth
* Reinhard Staudinger
* Jean Jacques Blanchard
* Matthias Borgers
* Bjorn Lindstrom
* David Baron
* Heinz Georg Scheel
* Martin Seidel
* Michael Krumkuhler
* Gabor Gyergyoi
* Jerry Shea
* Magnus Wild
* Manfred Tetzlaff
* Martin Virtel
* Vincent Petry
* Christoph Gerstlauer
* Benjamin Bachmann
* Martin Zahnd
* Stefan Bredenbroeker
* Etienne Dysli
* Christian Treber
* Jens P Benecke
* Frank Haarmeier
* Peter Mueller
* Klaus Wünschel
* Robert Rader
* Jorg Rosenbauer
* Marcus Lucke

---

#### 2016

* Richard Clayton
* Alexandre Alapetite
* Lukas Matasovsky
* Michael Krumnow
* Michael Ast
* Karl Ove Hufthammer
* Damian Rychlinski
* James Russell
* Christian Comes
* Dusan Pavlik
* Hugo Costelha
* bradley noyes
* Nikolaj Petersen
* Govindarajan Soundararajan
* Laurent Espitallier
* Deborah Swarts
* Geoffrey King
* Maria Calzetti
* Julio Rodriguez
* Elena Botoeva
* Heinz Herger
* Werner Lauckner
* Phil Landmeier
* Harald Knierim
* Kjetil Kjernsmo
* Jonas Kemmer
* Dzmitry Lahoda
* Guillermo Perez Hernandez
* Graham Monkman
* Varfolomeeva Marina
* Franck Quélain
* Lars-Goran Larsson
* Juan Zamanillo
* Olaf Scheel
* Rainer Schaffer
* Martin Zahnd
* Peter McBride
* Ivan Dragomereckyj
* Roland Bohrer
* Mattia Verga
* Klaus Wünschel
* Danie van der Merwe
* Michael Scholz
* Jens P Benecke
* Laurent Espitallier

---

#### 2015

* Benno Hansen
* Filippo Valeri
* Liokumovich Gregory
* Mike Bing
* Robert Berg
* Umberto Valentinotti
* Chrysoula Kotsanidou
* Jan Ott
* Udo Lembke
* Jakob Nielsen
* Casper Blom
* Balthas Seibold
* Jari Kirvesoja
* Jerry Argetsinger
* Hans Becker
* James Lunn
* keith lindsay
* Eberhard Hoyer
* Karl Ove Hufthammer
* Wayne Powell
* Fabien Maussion
* Bradley Noyes
* Marcus Lucke
* Koen Bailleul
* Sven Braner
* Richard Clayton
* Ulf Rompe
* Andrew Kingwell
* Casper Blom
* Vicente Salvador Cubedo
* Steffen Ullrich
* Felix Keppmann
* Martin Stolpe
* Andreas Berntsen
* Sander Boer
* Anton Vigura
* Xavier Arnau
* Randolf Mock
* Jean Frédéric Jolimaitre
* Stefanos Charchalakis
* Shantikumar Kulkarni
* Alberto Ponti
* Julian Weißgerber
* Dietmar Neuss
* Michael Schweizer
* Michael Zabel
* Martin Zahnd
* Graham Osborne
* Ivo Jossart

---

#### 2014

* Richard Clayton
* Dominik Keller
* Bradley Noyes
* Julien Morot
* Martin Lubich
* Ralf Duhlmeyer
* Ryan Henderson
* Christoph Baldauf
* Sergey Romanov
* Dominique Denis
* Piter Dias
* Anna Timm
* Daisy Vogt
* Tim Kozusko
* Martina Huber
* Gunner Gewiss
* Michael Mason
* Sven Ehlert
* Alexander Bryan
* Anders Lund
* Otto Dämon
* Holger Hampe
* Martin Mohr
* Arnaud Labro
* Jochen Siebert
* John F Moore
* Smit Mehta
* Christopher Burkey
* Aldo Ellena
* Christian Graesser
* Stefan Ronnecke
* Rainer Schaffer
* Robert MacKenzie
* Ute Stolzer
* Martin Zahnd
* Bernd Holzum
* Lukasz Mierzwa

---

#### 2013

* Thierry Nemes
* Richard Clayton
* Vincent Petry
* Nikolaj Petersen
* Christian González-Schiller
* Christoph Baldauf
* Jose Antonio Diaz Romero
* Christian jacobsen
* Kjetil Kjernsmo
* Dana Ross
* Georg Fries
* Laurent Espitallier
* Mónica Mora Guerra
* Michael Marending
* Axel Hengemuehle
* Alberto Ponti
* Jean Fréderic Jolimaitre
* Frederik Bertling
* Christoph Zimmermann
* Christoph Baldauf
* Christian Storzer
* Glen Thomson
* Bjorn Olsson
* Lukasz Mierzwa
* Jean-Paul Assouvie
* Michael Mason
* Jason Robinson
* Andreas Blumer
* Marc Winkelmann
* Ulf Berwaldt
* Alex Ruddick
* Christian Comes
* Donald Fredricks
* Christoph Baldauf
* Yevgeniy Ivanisenko
* Martin Zahnd
* Danny Schmarsel
* Mike Bing
* Antonio Trincone
* Michael Schweizer

---

#### 2012

* Richard Clayton
* Laurent Espitallier
* Oswaldo J Asprino Brine
* Martin Senftleben
* Thierry Nemes
* Tim Taylor
* Martin Hunger
* Christian Hugues
* Raul Sampe
* Christian Karitnig
* Slonska Vladimir
* Kevin O'Brien
* Ralph Scharpf
* Marios Andreopoulos
* Klaus Binder
* Carlos Mazon
* Stephan Kuhnert
* Dotan Cohen
* Carlos Mazon
* Michael Marending
* Christine Nicoll
* Ben vant Ende
* Filippo Valeri
* Christoph Baldauf
* Gregory Dawes
* Nicolas Martinez Frances
* Ivan Ruano
* Krishna G S
* Philippe Quaglia
* Stefan Habetz
* Robert Evert
* Michael Zabel
* Ilko Brauch
* Alessandro Di Renzo
* Anrypa Ahtoh
* Jean-Pierre Imhof
* Martin Zahnd
* Roronba Orbra
* Christer Wendel
* Mark Fraser
* Michael Klitgaard
* Martin Senftleben
* Osvaldo de Dona

---

#### 2011

* Per Jensen
* Joel Hallklint
* George Shuklin
* John Bestevaar
* Norbert Eisinger
* Thomas Arnold
* John Bestevaar
* Matti Rintala
* Eric Keller
* Philip M Thomas
* Dmitri Popov
* Thierry Nemes
* Thomas Guettler
* Stephan Martin
* Philippe Quaglia
* Sankarshan Mukhopadhyay
* Alexey Chelmodeev
* Steffen Eibicht
* Rinus Bakker
* Carla Schroder
* Daniel Baer
* Vincent Audibert
* Sam Shefrin
* Chieh Teng
* Robert Zmigrodzki
* Fabrizio Arias
* Manfred Mislik
* Chambers Tristan
* Radek Jun
* Philippe Dezac
* Orlando Gonzalez
* Alan Alpert
* Robert Rader
* Jean-Baptiste Rouquier
* Martin Senftleben
* Jörg Platte
* Moritz Klingmann
* Sam Shefrin
* Vincent Körber
* Michael Gajda
* Roger Larsson
* Mark Fraser
* Francois Guay
* Sylvain Leterreur
* Harald Ulver
* Denis Zalevskiy
* Krzysztof Onak
* Raymond Meijer
* Kamil Stepinski
* Christoph Baldauf
* Martin Zahnd
* Andreas Schroeder
* Michael Beckmann
* Franck Costa
* Harald Wilmersdorf
* Bernhard Hennlich

---

#### 2010

* Fabio Adamo
* Antonio Cunyat
* Andreas Floeter
* Fabio Borgatta
* Gaetan Hache
* Will Becker
* Jan Ott
* Benoit Courty
* Andreas Bank
* Martin Senftleben
* Hubert Furey
* Dmitri Popov
* Michael Hagmann
* Andreas Weigl
* Branislav Bozgai
* Moritz Klingmann
* Andreas Wolff
* Geoff Whale
* Michael Sacher
* Christian Müller
* Christophe Muller
* Richard Crosby
* Piombetti Claudio
* Jakob Löwen jun
* Joachim Pottkamp
* Cristiano Dri
* Andreas Tiemeyer
* Dietrich Scheller
* Alexander Wauck
* Gary Toth
* Thierry Moy
* Michael Beckmann
* Agostino Marconi
* Waldemar Andrukiewicz
* Kamil Stepinski
* Yngve Inntjore Levinsen
* Stefan Kofler
* Leonid Mutti
* Sheikh Tuhin
* Odd Karsten Hanken
* Hanns Weschta
* Thomas Kloss
* Mattias Wenz
* Donald Mac Laughlin
* Christopher Ludwig
* Kent Vander Velden
* Gregor Tätzner
* Daniel de Kok
* Michael Neubauer
* Dietmar Neuss
* Carla Schroder
* Lorenzo Ciani
* Reilly Butler
* Marek Otahal
* Milan Zamazal
* Roger Larsson
* Salvatore Brigaglia
* Martin Zahnd
* Dominik Klüter
* Nikolaj Petersen
* Ilias Bartolini
* Michael Sacher
* Alan Swartz
* Bruce Lamb
* Christian Weiske

---

#### 2009

* Manfred Mislik
* Jean-François Begot
* Salvatore Brigaglia
* Richard Crosby
* Gwendal Blanchard
* Serguei Romanov
* Giorgio Cioni
* Markus Lang
* Thomas Moertel
* Yair Kavalerchik
* Ewald Müller
* Vasileios Lourdas
* Oscar Haeger
* Jaroslaw Nikoniuk
* Vladimir Kulev
* Odin Hørthe Omdal
* Gys van Zyl
* Andrea Doglioni Majer
* Herve Le Roy
* Bastiaan Hovestreijdt
* Jonathan Dubovsky
* Claus Rebler
* Per Jensen
* Andreas Scherf
* Aleksey Konovalov
* Kamil Stepinski
* Franz Senftl
* Teemu Autto
* Carla Schroder
* Bruce Press
* Geoffrey King
* Thierry Moy
* Martin Banszel
* Xavier Mazellier
* Chris Fabre
* Branislav Bozgai
* Nick Alderweireldt
* Felix Michel
* Simon Marti
* Martin Schögler
* Timothy Edwards
* Daniel Scharrer
* Philippe Arnone
* Jochen Bleis
* Klaas Buitenkamp
* Dmitri Popov
* Martin Zahnd
* Gregory Dawes
* Jakob Lowen jun
* Nikolaj Petersen
* Arnoud Assenberg
* Stefan Lombaard
* Hallvard Indgjerd
* bradley noyes

---

#### 2008

* Malte Rohs
* Nick Alderweireldt
* Hubert Furey
* Mark Ashworth
* Stoyan Dafov
* Sebastian Pentenrieder
* Georg Kovalcik
* Mikolaj Machowski
* Christian Weiske
* Lawrence Plug
* Lorenzo Ciani
* Wilko Quak
* Arnfinn Forness
* Henri Soufarapis
* Carla Schroder
* Jens Albers
* Gerard Milhaud
* Emanuele Caratti
* Dotan Cohen
* Christian Imiela
* Karl Ove Hufthammer
* Dorian Okrucinski
* Gaizka Villate
* Nikolaj Petersen
* Jordan Levy
* Paul Radford
* Robert Hartl
* Ralph Pastel
* Peter Schopen
* Piotr Mienkowski
* Sylvain Leterreur
* Milan Zamazal
* Jean-Pierre Imhof
* Julien Morot
* Frank Schneider
* Marny Reed
* Will Stephenson

---

#### 2007

* Lars Mathiassen
* Odd Karsten Hanken
* Marcus Popp
* Florent Remeuf
* Christian Imiela
* Milan Zamazal
* Volker Faisst
* Giuseppe Mureddu
* Marek Wawrzyczny
* Axel Franke
* Martin Seidel
* Herve Le Roy
* Stefan Grosse
* Nikolaj Petersen
* Zoltan Meggyesi
* Paul Worrall
* Aaron Kushner
* Ravi Swamy
* Martin Zahnd
* Jean-Pierre Imhof

---

#### 2006

* S.Alexander
* Christian Kiewiet
* Julien Narboux
* David Nagy
* J.E Shepley
* Nikolaj Petersen
* Ralph Pastel
* juergen flosbach
* Martin Zahnd
* Goncalo Valverde
* David Preuss
* Martin Lubich
* Frédéric Coiffier

---

#### 2005

* Tom Albers
* Tung Nguyen
* Lionel Nicolas
* Martin Zahnd
* Duncan Hill
* Peter Neubauer
* Jason Harris
* Stephen Koermer
* Maxime Delorme
* Christian Weiske
* Dirk Bergstrom
* Julien Narboux
* Thorsten Schnebeck
* Raffaele Borrelli
* Danilo Barbuio
