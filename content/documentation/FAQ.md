---
date: "2017-03-21"
title: "FAQs"
author: "digiKam Team"
description: "Frequently asked questions about digiKam"
category: "documentation"
aliases: "/faq/digikam"
---

### My camera is not in the list of supported cameras in digiKam.

Can you add drivers for my camera ?

digiKam doesn't included any camera drivers. It makes use of <a href="http://www.gphoto.org/">gphoto2</a> to do camera operations. If your camera is not in the supported list, i would recommend doing some <a href="http://www.google.com">searching</a> around. Many of the new cameras actually provide a USB MassStorage Device Interface, so that you can access the camera like a hard disk. (See, the FAQ about using a USB MassStorage Camera with digiKam).

If you have still have no luck, i would recommend contacting the gphoto2 people.



### How can I inform you about bugs and wishes?


##### Reporting bugs and wishes



Please use the KDE bug tracking system for all bugreports and new feature wishlists. You can checkout the current bugreports and wishlists at these urls

* <a href="http://bugs.kde.org/buglist.cgi?product=digikam&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">digiKam bugs and wishes</a>



### I have a USB Mass Storage Camera. How do I use it with digiKam ?


#### The Easy Way!


If you have a relatively modern Linux distribution that is pre-configured to work with USB Mass Storage Devices, there's nothing you need to set up!. It should be truly "plug and play":


#### Manual Setup


If you're running a Linux distribution that doesn't set up USB Mass Storage devices automatically, here are the steps you need to follow:


### Thumbnail generation fails on large files

Q: On all my albums, digiKam fails to generate thumbnails on some images. The only pattern I can spot is that it fails on largish files (bigger than 1MB). When the thumnails are generated, the ones that fail appear at first and then are replaced by the broken picture icon.

A: Check if the application generating the file include an IPTC or XMP preview as JPEG. digiKam use this embeded image to render quickly the thumbnail without to load whole image.


### Which file format should I use?


Most digital cameras store images in jpeg format on the memory card. JPEG is a compressed format, whose compression causes loss of quality. Heavy compression may make the loss of quality visible to the eye. Every time you open a JPEG image, edit it, save the file and close it, the image is compressed and quality is lost. Lost quality can not be regained.


To avoid this repeated loss of quality you should save your edited files in a lossless format. digiKam understands two of these: Tiff and png. In digiKam it is advisable to use png. digiKam supports the exif data in png files, whereas (currently) for tiff files, exif data are lost.

So the recommendation is to save any edited files as png.


You have now finished editing your photos and are content with the results. Should you keep the png-files or convert them to the far smaller JPEG files for storage?. If you are very concerned about storage space, you can do the latter. You could, however rather keep the lossless png files and only create JPEG copies in the moment you want to take your images to the print shop, send them by email or post them on the web.

Note: a format with better compression named HEIF can be used in place of PNG to gain compression ratio.


### Why are my RAW images so dark?

In 16-bit mode, many are surprised that a relatively dark photograph shows up after RAW conversion with digiKam, even so the 'ColorManagement' and 'RAW Conversion' options have been meticulously set-up for the intended purpose. The simple reason is that digiKam uses <a href="http://www.libraw.org">Libraw</a> demosaisic method with linear conversion of RAW data, and if the ICC profile does not include gamma correction and/or tone mapping (most Canon profiles), the result is dark (in 8-bit mode the gamma correction is applied automatically). But nothing is lost, read on!

A RAW file is a container including the raw sensor data, EXIF metadata, and often a JPEG preview thumbnail. It does normally not include an ICC profile. The embedded JPEG file is used by digiKam to display RAW image thumbnails or in slide shows, and it can be used for TV display from the camera. This is fast, but a preview only, no raw data.

RAW conversion requires a couple of steps in order to produce a satisfying result, better adapted to your needs than JPEG out-of-the-camera images:

1. Choice of the internal working space in regard to the intended use
2. Adjust tonal range - define thresholds for highlights and shadows
3. Adjust gradation - adjust mid tones, for example using numerical numbers in the histogram or curves like features
4. Alter white balance if necessary
5. Color cast removal ideally using color temperature (or an according eyedropper tool) as well as a tint / tone slider. An according eye-dropper tool can be applied simply by clicking inside the image on neutral grey subject tones. This procedure doesn't always create the expected result, because quite often the light situation while capturing the image wasn't neutral either.
6. Smoothing and noise removal (if not done in the conversion process already)
7. Apply sharpening (refocus, unsharpmask)
8. Choose final color bitdepth (saving as JPEG always reverts to 8-bit depth, use PNG or TIFF for 16-bit)
9. Select output and pixel dimensions â€“ upsampling on the RAW file often gives better results in comparison to already processed files
10. Continue editing for example retouching or small enhancements in the favorite imaging application

Now, most profiles that come with the camera OEM software are not so suited for linear conversion. If you'd like a good profile, do it yourself or via a service. It will be more precise than the standard profile and it will <b>include gamma correction</b> - no dark images anymore in 16-bit mode!

Some good reading on the subject:


* [digiKam documentation](https://docs.kde.org/trunk5/en/extragear-graphics/digikam/using-setup.html#using-setup-editor)
* [Primer for ColorManagement](http://www.scribus.net/index.php?name=Web_Links&amp;req=viewlink&amp;cid=4)
* [Wikipedia](http://en.wikipedia.org/wiki/Color_management)
* [OpenRAW.org](http://www.openraw.org/actnow/)
* [European Color Initative ECI](http://eci.org/eci/en/020_eci.php)



### digiKam doesn't work when album library is on a network share (nfs, samba)

digiKam freezes when I store my album library on a NFS server, NAS, windows shared folder, etc. What can I do ?

digiKam uses sqlite to store informations about images (tags, comments, rates, ...). Sqlite relies on file locking features provided by filesystems (check <a href="http://www.sqlite.org/faq.html#q5">this FAQ entry</a>). But, network file systems often have troubles with locking and this can cause unexpected problems with sqlite.

In order to solve this problem, try one of the following workarounds :


* Use a <a href="http://thoughtsonrails.wordpress.com/2007/03/03/digikam-albums-on-network-filesystem/">symlink for some albums stored on a network path</a>
* Use a <a href="http://lists.kde.org/?l=kde-bugs-dist&amp;m=117536777106366&amp;w=2">symlink for the sqlite database file</a>. This method is preferred if you want to have all your album library on a network path.

Note: the digiKam team is aware of this problem and is working on it. In the future, it will be possible to use another database backend to solve it.


### How to create a digiKam theme?

Color Scheme are based on INI text file format separated with sections and attributes defined by key value pairs.
The color specification is done by using standard HTML color format (RGB/hexadecimal).
System level installation of color schemes is done in $(INSTALLATION_PREFIX)/share/apps/digikam/colorschemes and user level installation in $(HOME)/.local/share/apps/digikam/colorschemes under Linux.
Theme names appear in the menu with the filename. Make sure to capitalize the first letter of the filename
Its not necessary to restart digiKam if you modify the theme. switch to a different theme and switch back. But if you add a new theme, you will have to restart digiKam for the new theme to be recognized.

Color scheme file are the same than ones used for Plasma desktop. You can find all details [at this url](https://docs.kde.org/trunk5/en/kde-workspace/kcontrol/colors/index.html)

### Color Management with X on Linux

Color management of your monitor is possible with digiKam. The better and more fundamental way is to include the display color management in your X server using profiles. Then all other applications will profit as well.


This <a href="http://linux.vilars.com/">site</a> explains it all, go there for the installation procedure and the How-To's.


Note: the best way to color-manage the display is to establish a custom profile and use that one, a procedure to be repeated every other year or so for aging reasons. But if you go the easier way and use the profiles delivered from the manufacturer, it is paramount to do a normalization as described for example on the<a href="https://www.cambridgeincolour.com/tutorials/monitor-calibration.htm">cambridgeincolour</a> web site.



### Thumbnail generation fails on video files


#### Why don't I have thumbnails for my video files?
#### Why thumbnail generation stops at the first video file I have ?

digiKam uses [QtAV framework](http://www.qtav.org/) and [FFMpeg codecs](https://www.ffmpeg.org/) to generate thumbnails in background. Also, video support is optional at application compilation time.


So, you should check first if Video Support is present in the list from digiKam Help/Components Info dialog. If yes, you can install the stand alone QtAV media player 
on your system and check if playing video work as expected. If it do not work, probably a ffmpeg codec is missing on your system.


### How can I change the default video player ?

#### When I double-click on a video thumbnail, it opens -unwanted video player-, how could I change it to another video player ?


digiKam uses an embeded version of QtAV video player by default for a better portability and prevent external dependency. You can call an extra video player by the Open With context menu entry or ALT+double click over thumbnail.

Under Linux, start Plasma system settings then go to Applications/File Associations, then click on video. Here you have a list of file types with the list of
applications that can open them.


E.g. if you want another video player for your .avi files, go to x-msvideo. The first application listed here will be the default one.


### How can I change the text font size ?

#### Size of fonts for text and menus is too big/too small in digiKam. How can I adjust it ?


Start Plasma system settings then go to Appearance/Fonts. Here you can adjust the font size but also choose another font.

Please be warned that this will change the font behavior of all Plasma applications !

Note: digiKam also includes fonts settings to customize icon-view, tree-view, and tooltips text size. Rendez-vous to the digiKam control pannel to change these settings.


### Single-click on a thumbnail opens digiKam editor

#### When I click on a thumbnail, it opens the digiKam editor. How can I disable that ?


Go to digiKam control pannel in Views/Icons section to change this behavior. Under Linux, Plasma desktop permit to change the simple to double click and vis-versa
into Workspace/Desktop Behavior and choose "double-click to open files and folders"


### Some "Properties" in digiKam sidebar are unknown or unavailable

#### Why can't I get Image Properties Dimensions or other items ?


digiKam uses [Exiv2 shared library](http://www.exiv2.org/) to display the file properties.
If the "Dimensions" value is "unknown", it means that relevant file format is not yet fully supported by the library.

Note: even if you have the "Dimensions" information, some other properties (Bits depth, Sensitivity, Compression, ...) can still be unavailable depending on the image file (and your digital camera).


#### In Map sidebar, when I click on "More Info...", it opens a Web browser, how could I change it to another browser (firefox or anything else) ?


Under Linux, start Plasma system settings), then go to "Application/Default", then click on "Web Browser" and choose "in the following browser" where you can put any command.


### How to deal with utf-8 encoding ?

##### I just upgraded my preferred linux distribution but now I have warnings about utf-8. I have a warning when starting digiKam about new character encoding. What should I do ?


Nowadays, most linux distributions use utf8 by default for the character encoding. It includes the content of files, but also the file names.

Look here to learn more about utf-8 and unicode :

<a href="http://en.wikipedia.org/wiki/UTF-8">http://en.wikipedia.org/wiki/UTF-8</a>


digiKam will warn you when you change your locale with a new encoding.
Eg, if you switched from iso latin1 (iso8859-1) to utf8, digiKam will send you this warning :


<pre><small>Your locale has changed from the previous time this album was opened.
Old Locale : ISO 8859-1, New Locale : UTF-8
This can cause unexpected problems. If you are sure that you want to
continue, click on 'Yes' to work with this album. Otherwise, click on 'No'
and correct your locale setting before restarting digiKam
</small></pre>


Before accepting this modification, you should migrate at least the file names in your albums to utf-8 or you might have troubles because digiKam won't be able to access directories which contains accents non encoded in utf-8.


For that purpose, linux distributions propose various solutions and tools, but mainly based on convmv :

</p>


###### Gentoo


Gentoo has a very nice page about using UTF-8 :


<a href="http://www.gentoo.org/doc/en/utf-8.xml">Using UTF-8 with Gentoo</a>


This could also be useful for users of other distributions.


###### Suse


There's a FAQ entry about that :
<a href="http://www.novell.com/coolsolutions/qna/1786.html">Converting filenames to UTF-8 encoding</a>


There are also some informations here :
<a href="http://www.suse.de/~mfabian/suse-cjk/encodings-file-names.html">Converting file names</a> (part of CJK Support in SuSE Linux)


###### Ubuntu


With ubuntu, there's a migration tool written in python to do the job :
<a href="http://packages.ubuntulinux.org/dapper/misc/utf8-migration-tool">utf8-migration-tool package</a>


###### Other unix or linux operating systems

The most used tool to do that is <a href="http://j3e.de/linux/convmv/man/">convmv</a> (written in perl)


Eg (try it first in a test directory and backup everything before!) :
<small>
<pre>convmv --notest -r -f latin1 -t utf-8
</pre>
</small>


### Is digiKam available in my language ?

##### How can I use another language instead of the default (english) one ?


###### Distribution package

If you installed digiKam using your distribution package, language files should be included with it and you can run digiKam in any supported language.

digiKam must follow your desktop language settings, but you can override the default language by using the Settings/Configure Language menu. digiKam come with more than 30 different translations.


###### Pre-Compiled Bundles

If you use a pre-compiled bundle, digiKam comes with more than 30 different translations integrated as ressources, and you can use digiKam in any supported language.

See above for details about how to switch to another language.


### digiKam doesn't compile


##### I can't compile digikam. What can I do ?


First, check that you have development files installed in your linux system. digiKam depends on a lot of libs and you must have the headers to compile it.


Usually, linux distributions have <b>-dev</b> packages when you want to compile something against a specific library. For a list of required dependencies, read the README file. Here's the <a href="http://websvn.kde.org/branches/stable/extragear/graphics/digikam/README?view=markup">current README (svn version)</a>.


On debian-based distribution (eg Debian, Ubuntu), you can install the required packages with :

<pre>apt-get build-dep digikam</pre>


If there are some warnings about unsermake, set the $UNSERMAKE environment variable as "no". Eg :

<pre>export UNSERMAKE=no</pre>

Look also at the [download page](/download) for more informations (tarball and svn sections). If you still have problems, check the [support page](/support).


### digiKam always crashes when doing xxxx. What can I do ?


##### When editing IPTC, playing with tags, assigning keywords or doing xxxx, digiKam crashes. What's the problem ?


It could be a problem with Exiv2. It's mandatory to use at the last stable version with digikam. With all pre-compiled bundle provided by the project, we always use the last Exiv2 version tested by the team.

If digiKam still crash with the most recent Exiv2 version,  check the [support](/support) and the [contrib](/contrib) pages to provide a suitbale backtrace for developers.

Try to see if your problem has already been reported by another user in bugs.kde.org, to prevent dupplicates reports, which is time consuming for the team to triage.

If it's not the case, check the contrib page, you will find instructions on how to compile digiKam with full debug and how to send us a backtrace.
