---
date: "2017-03-22T13:13:45-06:00"
title: "Download from git"
author: "digiKam Team"
description: "How to download, compile, and install the digiKam source code from git under Linux."
category: "download"
aliases: "/download/GIT"
---

### How to get digiKam from git master repository

#### Stage 1: Install external dependencies

digiKam requires a lot of libraries. So, before compiling yourself digiKam, you'll have to download and install them.</p>
The up to date dependencies list is given in the developer documentation [available in this section](https://www.digikam.org/api/index.html#externaldeps)

Most of these dependencies will be available as developement packages from your Linux distribution,
generally with a "-devel" postfix (as libgphoto2-devel for ex), and can be installated separatly using
the system packages manager as apt-get (Ubuntu, Debian, ...), yum (RedHat, Fedora, CentOS, ...),
urpmi (Mageia), zipper (Suse, OpenSuse, ...), etc.


#### Stage 2: Checkout digiKam source code

To checkout digiKam code from the [git repository](https://invent.kde.org/graphics/digikam),
enter these command lines in a directory where you have enough space disk:

```
# git clone https://invent.kde.org/graphics/digikam.git digikam
# cd digikam
```

This will checkout the master branch where new features will be added. This code is compatible with Qt5 and Qt6 and will targeted to future 8.0.0 release.

A dedicated git branch named qt5-maintenance is used to publish the mature and stable qt5 based releases, aka 7.6, 7.7, 7.8, etc.
This branch do not accept new features, only bug fixes. To get this branch instead master code, use this command line:

```
# git checkout qt5-maintenance
```

Note: for developers who whant to contribute, create a fork from GiLab repository web interface and make your
changes in your own repository. When all is ready, from web interface send a Pull Request (PR) for future
codes review and backport to master repository.

#### Stage 3: digiKam Compilation and installation in your Linux system

To compile the source code under linux you have to give these commands in the source code folder.
You should use a separate build folder to help cleaning up sources if something goes wrong.
All configuration tasks can be performed by `bootstrap.linux` script.


```
# ./bootstrap.linux
# cd build
# make -j4
# su
# make install/fast
```

Note: `make -j4` will use 4 CPU cores to compile all source code. If more cores are available on your computer,
just change this value accordingly to speed-up compilation.

Files will be installed to the standard path used by your Linux system to host applications (usually /usr).
