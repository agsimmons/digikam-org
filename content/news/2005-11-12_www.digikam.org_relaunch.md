---
date: "2005-11-12T20:58:00Z"
title: "www.digikam.org relaunch"
author: "site-admin"
description: "After a lot of hard work, I'm happy to announce the relaunch of our website. If you encounter any problems or have any suggestions for"
category: "news"
aliases: "/node/37"

---

<p>After a lot of hard work, I'm happy to announce the relaunch of our website. </p>
<p>If you encounter any problems or have any suggestions for the site, please<br>
give us your feedback.</p>
<p>I would like to thank our sponsor <a href="http://www.kovoks.nl/">KovoKs</a> for their assistence!</p>

<div class="legacy-comments">

</div>