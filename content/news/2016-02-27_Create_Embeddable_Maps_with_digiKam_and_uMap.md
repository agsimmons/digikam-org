---
date: "2016-02-27T14:40:00Z"
title: "Create Embeddable Maps with digiKam and uMap"
author: "Dmitri Popov"
description: "The Map view in digiKam is handy for displaying photos on a map directly in the application. But what if you want to create a"
category: "news"
aliases: "/node/753"

---

<p>The Map view in digiKam is handy for displaying&nbsp;photos on a map directly in the application. But what if you want to create a shareable and embeddable map with pins that mark all places where you took photos?&nbsp;A simple Bash script and the&nbsp;<a href="http://umap.openstreetmap.fr/en/" target="_blank">uMap</a> service can help you with that.</p>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2016/01/digikam-umap.png" alt="digikam-umap" width="600"></p>
<p><a href="http://scribblesandsnaps.com/2016/02/27/create-embeddable-maps-with-digikam-and-umap/">Continue reading</a></p>

<div class="legacy-comments">

</div>