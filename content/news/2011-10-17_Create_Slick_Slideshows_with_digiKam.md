---
date: "2011-10-17T08:37:00Z"
title: "Create Slick Slideshows with digiKam"
author: "Dmitri Popov"
description: "It's easy to dismiss digiKam's slideshow functionality as a feature of no particular use. After all, most photographers prefer to publish their photos using the"
category: "news"
aliases: "/node/630"

---

<p>It's easy to dismiss digiKam's slideshow functionality as a feature of no particular use. After all, most photographers prefer to publish their photos using the photo sharing service of their choice. But the slideshow feature can come in handy when showcasing photos on your machine is the only option.</p>
<p><img class="alignnone size-medium wp-image-2061" title="digikam_slideshow" src="http://scribblesandsnaps.files.wordpress.com/2011/10/digikam_slideshow.png?w=500" alt="" width="500" height="389"></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/10/17/create-slick-slideshows-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>