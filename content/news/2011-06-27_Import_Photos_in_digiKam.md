---
date: "2011-06-27T09:04:00Z"
title: "Import Photos in digiKam"
author: "Dmitri Popov"
description: "Using the commands tucked under the Import menu, you can offload photos from your camera, storage card, or USB stick directly into the application. What's"
category: "news"
aliases: "/node/609"

---

<p>Using the commands tucked under the Import menu, you can offload photos from your camera, storage card, or USB stick directly into the application. What's more, the Import dialog box offers a few clever features that allow you to configure the import operation. <a href="https://scribblesandsnaps.wordpress.com/2011/06/27/import-photos-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>