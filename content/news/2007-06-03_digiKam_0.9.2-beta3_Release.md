---
date: "2007-06-03T18:52:00Z"
title: "digiKam 0.9.2-beta3 Release"
author: "gerhard"
description: "Dear all digiKam fans! Some minor regression bugs led us to another release, the 0.9.2-beta3 release including an improved light table (full size images). The"
category: "news"
aliases: "/node/224"

---

<p>Dear all digiKam fans!<br>
Some minor regression bugs led us to another release, the <a href="http://digikam3rdparty.free.fr/0.9.2_release">0.9.2-beta3</a> release including an improved light table (full size images). The 0.9.2 final release is on track for mid June publishing. This beta3 version is rock-solid, don't hesitate to use it for production. </p>
<p>In order to make digiKam 0.9.2-beta3 compile you need to compile and install libkexiv2 and libkdcraw first (unless you complied them for beta[1,2] already).</p>
<p>The library tarballs can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=149779">(1) SourceForge</a>.<br>
The digiKam tarball + documentation can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">(2) SourceForge</a> as well.</p>
<p><em>Changes</em><br>
--------------------------------<br>
General       : Light Table and Preview Mode can work with full image size instead a reduced one.</p>
<p>----------------------------------------------------------------------------</p>
<p>digiKam BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</p>
<p>059 ==&gt; 145198 : Light-table should also work with the full image.<br>
060 ==&gt; 146072 : Slideshow shows black screen.<br>
061 ==&gt; 146184 : Showfoto no filename specified.</p>

<div class="legacy-comments">

</div>