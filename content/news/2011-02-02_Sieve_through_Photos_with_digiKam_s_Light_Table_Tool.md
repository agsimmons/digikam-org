---
date: "2011-02-02T09:21:00Z"
title: "Sieve through Photos with digiKam’s Light Table Tool"
author: "Dmitri Popov"
description: "If you have a handful of storage cards in your photo bag, there is no need to worry about how many photos you can take"
category: "news"
aliases: "/node/572"

---

<p>If you have a handful of storage cards in your photo bag, there is no need to worry about how many photos you can take before your digital camera runs out of memory. This means that you can easily take dozen of shots of the same subject trying different angles, composition, and lighting. But this also makes the task of picking the best photo from the batch a bit of a challenge. Fortunately, digiKam provides a simple yet efficient tool that can help you to compare and analyze multiple photos side by side and pick the one you like most. <a href="http://scribblesandsnaps.wordpress.com/2011/02/02/sieve-through-photos-with-digikams-light-table-tool/">Continue to read</a></p>

<div class="legacy-comments">

</div>