---
date: "2013-02-22T10:30:00Z"
title: "digiKam Recipes 3.11.01 Released"
author: "Dmitri Popov"
description: "A new version of the digiKam Recipes ebook is available for download. The following material has been added since version 3.9.39: Create a Bleach Bypass"
category: "news"
aliases: "/node/684"

---

<p>A new version of the <a href="http://dmpop.homelinux.com/digikamrecipes/">digiKam Recipes</a> ebook is available for download. The following material has been added since version 3.9.39:</p>
<ul>
<li>Create a Bleach Bypass Effect</li>
<li>Export Photos via DLNA</li>
<li>Process Film Negatives</li>
<li>Calibrate and Profile Monitor for Use with digiKam</li>
<li>Use Photoshop Curve Presets with digiKam</li>
<li>Import Photos from a Remote Server into digiKam</li>
</ul>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2012/06/digikamrecipes-3935-fbreader.png?w=281" width="281" height="500"></p>
<p>As usual, the new release features minor fixes and improvements. <a href="http://scribblesandsnaps.com/2013/02/22/digikam-recipes-3-11-01-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>