---
date: "2013-06-05T06:57:00Z"
title: "digiKam Software Collection 3.3.0-beta1 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the first beta release of digiKam Software Collection 3.3.0. This version currently under"
category: "news"
aliases: "/node/695"

---

<a href="http://www.flickr.com/photos/digikam/8744026064/" title="digikam 3.3.0 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7286/8744026064_3150c2dcb0.jpg" width="500" height="307" alt="digikam 3.3.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the first beta release of digiKam Software Collection 3.3.0.
This version currently under development, including a new core implementation to manage faces, especially face recognition feature which have never been completed with previous release. <a href="http://en.wikipedia.org/wiki/Face_detection">Face detection</a> feature still always here and work as expected.</p>

<p><a href="https://plus.google.com/113704327590506304403/posts">Mahesh Hegde</a> who has work on <a href="http://en.wikipedia.org/wiki/Facial_recognition_system">Face Recognition</a> implementation has published a proof of concept demo on YouTube.</p>

<iframe width="560" height="315" src="http://www.youtube.com/embed/iaFGy0n0R-g" frameborder="0" allowfullscreen=""></iframe>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.3.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.3.0-beta1.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b> Release plan <a href="http://www.digikam.org/about/releaseplan">can be seen here...</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20553"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/695#comment-20553" class="active">Great! Very exciting news :-D</a></h3>    <div class="submitted">Submitted by Michael (not verified) on Wed, 2013-06-05 07:29.</div>
    <div class="content">
     <p>now I can try and play with face recognition on the thousands of photos I tagged in the last years with "face to be detected" :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20554"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/695#comment-20554" class="active">Face Detection</a></h3>    <div class="submitted">Submitted by newHippie (not verified) on Wed, 2013-06-05 20:53.</div>
    <div class="content">
     <p>Hello, I have build the beta with no errors but still no face are recognized. Only detected. Even the libkface database shown in the video is not there. What do I wrong?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20555"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/695#comment-20555" class="active">Windows version?</a></h3>    <div class="submitted">Submitted by photoorg (not verified) on Fri, 2013-06-07 08:10.</div>
    <div class="content">
     <p>Windows version coming anytime soon?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20556"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/695#comment-20556" class="active">It's out now!</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2013-06-08 15:39.</div>
    <div class="content">
     <p>At long last I am pleased to announce the simultaneous release of digiKam 3.0.0 and 3.1.0 for Windows built against KDE 4.10.2. If you have a short attention span and want to download it immediately see the attached link.</p>
<p>There were numerous bugs I had to fix in digiKam and KDE before release, and fixing them took more time than expected. Extremely sorry for the delay! Rest assured, it was worth it thanks to a major enhancement on the Windows side of things: a new 'solid' interface thanks to the KDE windows team which will drastically speed up time before album display when switching between albums and when viewing digiKam after some minutes away.</p>
<p>Bugs fixed for Windows include:<br>
1. Inability to use UUIDs for volumeids on Windows<br>
2. Loss of collection during upgrade for certain collection paths<br>
3. JPEG rotation from album viewer failed to work correctly and often crashed (for some types of collections you will need to press F5 to refresh the thumbnails, I will try to fix this for 3.2.0 or 3.3.0)<br>
4. Temporary thumbnails are were not cleaned up correctly<br>
5. File paths were not displayed in native Windows format (i.e. a backslash - if you see any paths that are not in the correct format please file a bug report or reply to this)<br>
6. KDE did not handles mixed case files on Windows in most cases correctly (i.e. you could not save mixed case files properly and filename comparison did not always work properly - there may be some lingering places where it does not)<br>
7. Misc. compile time fixes and code clean-up</p>
<p>Known problems<br>
1. After rotating an image using the rotate icons in the thumbnail view, the user may have to press F5 to see the change<br>
2. Bugs listed on <a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=CONFIRMED&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;list_id=676982&amp;op_sys=MS%20Windows&amp;product=digikam&amp;product=digikamimageplugins&amp;product=showfoto&amp;query_format=advanced&amp;query_based_on=&amp;columnlist=bug_severity%2Cpriority%2Copendate%2Cbug_status%2Cresolution%2Ccomponent%2Cop_sys%2Crep_platform%2Cshort_desc">https://bugs.kde.org</a></p>
<p>Regardless, without further ado I give you Digikam 3.0.0:</p>
<p>   <a href="http://download.kde.org/stable/digikam/digiKam-installer-3.0.0-win32.exe">http://download.kde.org/stable/digikam/digiKam-installer-3.0.0-win32.exe</a></p>
<p>and digiKam 3.1.0:</p>
<p>   <a href="http://download.kde.org/stable/digikam/digiKam-installer-3.1.0-win32.exe">http://download.kde.org/stable/digikam/digiKam-installer-3.1.0-win32.exe</a></p>
<p>Please let me know if you encounter any problems or file a bug report.</p>
<p>Enjoy!</p>
<p>P.S. I hope to release 3.2.0 in the next week or two.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
