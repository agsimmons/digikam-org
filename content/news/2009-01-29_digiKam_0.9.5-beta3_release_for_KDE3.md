---
date: "2009-01-29T10:02:00Z"
title: "digiKam 0.9.5-beta3 release for KDE3"
author: "digiKam"
description: "Dear all digiKam fans and users! A new digiKam beta release is out. for KDE3, It's a bug fix and translations updates. digiKam 0.9.5-beta3 tarball"
category: "news"
aliases: "/node/423"

---

<p>Dear all digiKam fans and users!</p>

<p>A new digiKam beta release is out. for KDE3, It's a bug fix and translations updates.</p>

<a href="http://www.flickr.com/photos/digikam/3231469460/" title="digikam0.9.5-beta3-kde3 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3463/3231469460_a821530d2c.jpg" width="500" height="313" alt="digikam0.9.5-beta3-kde3"></a>

<p>digiKam 0.9.5-beta3 tarball can be downloaded from SourceForge <a href="https://sourceforge.net/project/showfiles.php?group_id=42641&amp;package_id=34800">at this url</a></p>

<p>Compared to digiKam 0.9.4, closed files and new features are listed below:</p>

<p>

<b>NEW FEATURES:</b><br><br>

General        : libkdcraw dependency updated to 0.1.6.<br>
General        : TIFF/PNG/JPEG2000 metadata can be edited or added (require Exiv2 &gt;= 0.18).<br><br>

Image Editor   : All image plugin tool settings provide default buttons to reset values.<br>
Image Editor   : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
Image Editor   : All image plugin dialogs are removed. All tools are embedded in editor window.<br>
Image Editor   : New composition guide based on Diagonal Rules.<br><br>

<b>BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</b><br><br>

001 ==&gt; 166867 : digiKam crashes when starting.<br>
002 ==&gt; 167026 : Crash on startup Directory ImageSubIfd0 not valid.<br>
003 ==&gt; 146870 : Don't reduce size of image when rotating.<br>
004 ==&gt; 167528 : Remove hotlinking URL from tips.<br>
005 ==&gt; 167343 : Albums view corrupted and unusable.<br>
006 ==&gt; 146033 : When switching with Page* image jumps.<br>
007 ==&gt; 127242 : Flashing 'histogram calculation in progress' is a bit annoying.<br>
008 ==&gt; 150457 : More RAW controls in color management pop-up please.<br>
009 ==&gt; 155074 : Edit Image should allow chance to adjust RAW conversion parameters.<br>
010 ==&gt; 155076 : RAW Conversion UI Needs to be more generic.<br>
011 ==&gt; 142975 : Better support for RAW photo handling.<br>
012 ==&gt; 147136 : When selecting pictures from folder showfoto got closed.<br>
013 ==&gt; 168780 : Loose the raw import.<br>
014 ==&gt; 160564 : No refresh of the number of pictures assigned to a tag after removing the tag from some pictures.<br>
015 ==&gt; 158144 : Showfoto crashes in settings window.<br>
016 ==&gt; 162845 : 'Ctrl+F6' Conflict with KDE global shortcut.<br>
017 ==&gt; 159523 : digiKam crashes while downloading images from Olympus MJU 810/Stylus 810 camera no xD card.<br>
018 ==&gt; 161369 : Quick filter indicator lamp is not working properly in recursive image folder view mode.<br>
019 ==&gt; 147314 : Renaming like crazy with F2 slows down my system.<br>
020 ==&gt; 164622 : Crash using timeline when switching from month to week to month view.<br>
021 ==&gt; 141951 : Blank empty context right-click menus.<br>
022 ==&gt; 116886 : Proposal for adding a new destripe/denoise technique.<br>
023 ==&gt; 163602 : Race condition during image download from Camera/ USB device: image corruption and/or loss.<br>
024 ==&gt; 165857 : Unreliable import when photo root is on network share.<br>
025 ==&gt; 147435 : Resize slider in sharpness dialog doesn't work correct.<br>
026 ==&gt; 168844 : Make the selection rectangle draggable over the image (not only resizable).<br>
027 ==&gt; 147151 : Compile error: multiple definition of `jpeg_suppress_tables'.<br>
028 ==&gt; 170758 : High load when tagging.<br>
029 ==&gt; 168003 : Drag&amp;dropping a photo to a folder in Dolphin begins copying the whole system:/media.<br>
030 ==&gt; 142457 : Temp files not cleaned up after crashes.<br>
031 ==&gt; 164573 : Better support for small screens.<br>
032 ==&gt; 175970 : digitaglinktree merges tags with same name in different subfolders.<br>
033 ==&gt; 108760 : Use collection image (or part of) as Tag/Album icon.<br>
034 ==&gt; 144078 : very slow avi startup.<br>
035 ==&gt; 146258 : Moving an album into waste basket didn't remove.<br>
036 ==&gt; 149165 : cannot edit anymore - sqlite lock?<br>
037 ==&gt; 146025 : Wrong image name when using info from EXIF.<br>
038 ==&gt; 167056 : Updating tags is slow when thumbnails are visible.<br>
039 ==&gt; 141960 : Problems with photos without EXIV data when updating me.<br>
040 ==&gt; 129379 : Renamed Album is shown multiple times although there is only on related picture directory.<br>
041 ==&gt; 171247 : Album creation error when name start by a number.<br>
042 ==&gt; 150906 : digiKam unable to connect to Panasonic LUMIX DMC-TZ3.<br>
043 ==&gt; 148812 : "Auto Rotate/Flip Using Exif Orientation" fails with some images.<br>
044 ==&gt; 150342 : Camera image window keeps scrolling to the currently downloaded picture.<br>
045 ==&gt; 147475 : digiKam Slideshow - Pause button does not stay sticky / work.<br>
046 ==&gt; 148899 : Image Editor does not get the focus after clicking on an image.<br>
047 ==&gt; 148596 : Empty entries in the "back" drop-down when changing month (date view).<br>
048 ==&gt; 161387 : Unable to import photos with digiKam even though it's detected.<br>
049 ==&gt; 165229 : Thumbnail complete update does not work reliably, even for jpgs.<br>
050 ==&gt; 176477 : Files disappear while importing.<br>
051 ==&gt; 179134 : Compile-error with libkdcraw &gt; 0.1.5.<br>
052 ==&gt; 162535 : Startup is extremely slow when famd is running.<br>
053 ==&gt; 179413 : Support restoring blown out highlights.<br>
054 ==&gt; 157313 : Option to reposition lighttable slide-list.<br>
055 ==&gt; 180671 : Scanner import does not work.<br>
056 ==&gt; 175387 : digikam-doc 0.9.4 sf.net tarball is outdated.<br>
057 ==&gt; 181712 : Ubuntu 8.10 - digiKam finds not the correct images.<br>
</p>


<div class="legacy-comments">

  <a id="comment-18193"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18193" class="active">Can't wait for the final</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-01-29 17:19.</div>
    <div class="content">
     <p>Can't wait for the final release on the...eh...30th of February!</p>
<p>Which versions of Digikan are capable of writing tags to Canon CR2<br>
raw files these days? Any of them? I'm eagerly awaiting that<br>
feature and all the releases have me confused about what features<br>
are in what versions, particular when writing to CR2 seems to be<br>
related to the exiv2 version and not just the Digikam version. The<br>
release notes for both do not make it clear, as they mention Nikon<br>
raw formats and "TIFF"-like formats, but not specifically Canon CR2.</p>
<p>Keep up the great work. I might migrate to KDE 4.x this year, so<br>
I'm looking forward to Digikam 0.10.x. However, I've had to work<br>
on a "plan B" to back up my metadata, as Digikam 0.9.4 lost the<br>
tags for 1,200 of my JPEG images for some reason (could have been<br>
a PEBKAC, as I was moving the files around and I still cannot<br>
understand which directions the metadata and database syncs<br>
go in; I'd love if there was a dialog box that explained exactly<br>
what was going to happen and asked for confirmation).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18194"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18194" class="active">final...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2009-01-29 17:32.</div>
    <div class="content">
     <p>&gt;30th of February! </p>
<p>Fixed (:=)))</p>
<p>&gt;Which versions of digiKam are capable of writing tags to Canon CR2<br>
&gt;raw files these days?</p>
<p>None... but it's not relevant of digiKam but Exiv2.</p>
<p>In 0.9.5, i have disable RAW metadata writting support, because this feature still experimental in Exiv2.<br>
In 0.10.0, i have added a new option in metadata settings panel to enable RAW writting mode support (disable by default). But currently, Exiv2 only support NEF, PEF, and DNG. CR2 is planed.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18200"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18200" class="active">Salut Gilles,
&gt; In 0.9.5, i</a></h3>    <div class="submitted">Submitted by Guenther (not verified) on Fri, 2009-01-30 12:33.</div>
    <div class="content">
     <p>Salut Gilles,</p>
<p>&gt; In 0.9.5, i have disable RAW metadata writting support,<br>
&gt; because this feature still experimental in Exiv2.<br>
&gt;<br>
I use the 0.9.5 version from svn for some weeks now. Exiv2 etc. are all out of svn, but I had no success in TIFF metadata write support. It is still readonly. No old or duplicate versions of exiv2 and kexiv2 are on my system.</p>
<p>Is TIFF write also disabled or do I something wrong? The info dialog in digiKam shows the correct versions.</p>
<p>Merci !<br>
Guenther</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18201"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18201" class="active">libkexiv2...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-01-30 14:25.</div>
    <div class="content">
     <p>Checkout current code of libkexiv2 for KDE3 and recompile it. I just fixed a stupid bug. </p>
<p>Gilles</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18210"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18210" class="active">About metadata preservation,</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-01-31 14:59.</div>
    <div class="content">
     <p>About metadata preservation, 0.9.0 do not support multiple root album path. Changing root album or removing albums, and digiKam will ask to clean-up database.</p>

<p>But with 0.10.0, it's completly different: we support multiple root albums as : local, remote (shared by NFS or SAMBA), and removable media. digiKam do not cleanup database if a root album disapears.</p>

<a href="http://www.flickr.com/photos/digikam/3038025046/" title="setup-collections by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3164/3038025046_a628cf3b29.jpg" width="500" height="415" alt="setup-collections"></a>

<p>digiKam</p>         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18197"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18197" class="active">Thank You</a></h3>    <div class="submitted">Submitted by <a href="http://jjorge.free.fr" rel="nofollow">Zézinho</a> (not verified) on Fri, 2009-01-30 08:57.</div>
    <div class="content">
     <p>for not forgetting the mass of users still using Digikam 0.9.4. I've packaged it for Mandriva, in an unofficial repository as their staff is too busy to put it on backports.<br>
Now for 0.9.5 final, is there a planned release date, or do you think I should package this Beta 3?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18205"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18205" class="active">Good work on the beta.  I try</a></h3>    <div class="submitted">Submitted by <a href="http://vw.homelinux.net" rel="nofollow">Anonymous</a> (not verified) on Sat, 2009-01-31 01:34.</div>
    <div class="content">
     <p>Good work on the beta.  I try to test the betas, but usually there is so much wrong with them and so many dependency issues, that I don't have the time.  I try to be patient and wait until the first RC.  Typically I use the RC for production work (everything backed up of course), and have not had any problems. </p>
<p>I had installed .9.5 beta1 and beta2, but neither worked well enough for me to even do much testing on.  Beta3 is working pretty well for me in testing.  Still some issues with resizing windows on my 1280x1024 LCD, but the scrollbars appear and everything is at least usable.  Looking forward to the RC and final release.  I've tested 10 RC1 too, but I'm not 100% sure yet if I'll finally commit to kde4 with ver.4.2 and I want to leave my options open.  Glad to see that at least for this release, ports are available for both kde versions.</p>
<p>-----<br>
testing .9.5.b3 via my own ebuild on Gentoo...also used my own ebuilds for the most up-to-date dependencies.<br>
testing .10.0 rc1 on Ubuntu and Gentoo</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18218"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18218" class="active">PTP mode problem</a></h3>    <div class="submitted">Submitted by Ralph (not verified) on Sun, 2009-02-01 19:26.</div>
    <div class="content">
     <p>My digital camera is a Nikon Coolpix 4300 (USB 1.x).</p>
<p>The camera works perfectly (in PTP mode and in Mass Storage mode) with DigiKam 0.9.4.<br>
With DigiKam 0.9.5 beta3, the camera works in Mass Storage mode only; if the camera is in PTP mode, DigiKam finds the first photo only.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18219"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18219" class="active">PTP =&gt; libgphoto2</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2009-02-01 19:55.</div>
    <div class="content">
     <p>PTP mode is managed by driver from GPhoto2 library. Report this problem to GPhoto2 project.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18223"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18223" class="active">The camera works perfectly</a></h3>    <div class="submitted">Submitted by mpcoc (not verified) on Wed, 2009-02-04 21:34.</div>
    <div class="content">
     <p>The camera works perfectly (in PTP mode and in Mass Storage mode) with DigiKam 0.9.4.<br>
With DigiKam 0.9.5 beta3, the camera works in Mass Storage mode only; if the camera is in PTP mode, DigiKam finds the first photo only.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18264"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18264" class="active">video transfer disappeared ?</a></h3>    <div class="submitted">Submitted by spade (not verified) on Wed, 2009-02-18 10:06.</div>
    <div class="content">
     <p>AVI Video files (MJPEG codec) does not appear anymore in the transfer window. I am using this feature in v0.9.4, and it is quite convenient, particularly to use the rename functionality. Please keep these files !<br>
Concerning the rename function, even in the v0.9.4, I haven't found how to get a new name matching the pattern : yyyy.MM.dd_hh"h"mm"m"ss"s like 2009.02.18_09h56m30s. The only way I found to get this so far is to rename in yyyy.MM.dd_hhgmmlssq ("g", "l" and "q" are randomly choosen) in digikam, then to yyyy.MM.dd_hh"h"mm"m"ss"s using krename.</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18265"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18265" class="active">renaming</a></h3>    <div class="submitted">Submitted by spade (not verified) on Wed, 2009-02-18 10:16.</div>
    <div class="content">
     <p>The following and underlying question was : Is there a way to rename in one shot in digikam, using this pattern ?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18320"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/423#comment-18320" class="active">A big THANKS</a></h3>    <div class="submitted">Submitted by blueget (not verified) on Fri, 2009-03-06 17:58.</div>
    <div class="content">
     <p>Digikam is really great, and it's absolutely superb that navigating between the images with the arrow keys is now possible again. BIG, BIG thanks to whoever fixed this bug, and generally to the whole digikam team!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
