---
date: "2008-07-16T22:10:00Z"
title: "digikam 0.9.4 release"
author: "Marcel"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release version 0.9.4. The digiKam tarball can be downloaded from SourceForge. Noteworthy"
category: "news"
aliases: "/node/359"

---

<p>Dear all digiKam fans and users!<br></p>
<p>The digiKam development team is happy to release version 0.9.4. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.<br>
Noteworthy features added since the last stable release include auto-gamma and auto-whitebalance support for RAW files in 16bit mode, displaying the number of contained items in the overview for all albums, usability improvements, as usual a lot of bug fixes - see below for a complete list of new features and fixed bugs.</p>
<p><br><br>
<a href="http://www.flickr.com/photos/digikam/2644896417/" title="digikam-0.9.4-rc2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3016/2644896417_96a3ac039c.jpg" width="500" height="400" alt="digikam-0.9.4-rc2"></a><br>
<br></p>
<h5>NEW FEATURES (since 0.9.3):</h5>
<p><br></p>
<p><b>General</b>  : external libsqlite3 depency removed. sqlite3 source code is now included in digiKam core. Note than for packaging, an external libsqlite3 dependency can be used instead internal version (see B.K.O #160966).<br>
<b>General</b>  : Updated internal CImg library to last stable 1.2.9 (released at 2008/06/26).<br>
<b>General</b>  : English words review in whole GUI by Oliver Dörr.<br>
<b>General</b>  : New search text filter for all metadata sidebar tabs.<br>
<b>General</b>  : New dialog to list all RAW camera supported. A search camera model tool is available to easily find a camera from list.<br>
<b>General</b>  : Creation of tags simplified everywhere. Multiple tags hierarchy can be created at the same time. Tags creation dialog re-designed.<br>
<b>General</b>  : Color theme scheme are pervasively applied in graphical interfaces, giving digiKam a real pro-look when dark schemes are selected.<br>
<b>General</b>  : Color theme scheme can be changed from either Editor or LightTable.<br>
<b>General</b>  : Add capability to display items count in all Album, Date, Tags, and Tags Filter folder views. The number of items contained in virtual or physical albums can be displayed next to its name. If a tree branch is collapsed, parent views sum-up the number of items from all undisplayed children views. Items count is performed in background by digiKam KIO-Slaves. A new option from Setup/Album dialog page can toggle on/off this feature.</p>
<p><b>ImageEditor</b> : Raw files can be decoded in 16 bits color depth without to use Color Management.  An auto-gamma and auto-white balance is processed using the same method than dcraw with 8 bits color depth RAW image decoding. This usefull to to have a speed-up RAW workflow with suitable images.<br>
<b>ImageEditor</b> : New "save as" image file dialog with photo thumbnail / information.</p>
<p><b>Showfoto</b> : Added support of color theme schemes.<br>
<b>Showfoto</b> : Added 2 options to setup images ordering with "File/Open Folder" action.<br>
<b>Showfoto</b> : New open image file dialog with photo thumbnail / information.</p>
<p><b>AlbumGUI</b> : Add a new tool to perform Date search around whole albums collection: Time-Line. Timeline is a new left sidebar tab. It's a great tool complementary to the calendar. Try it out!<br>
<b>AlbumGUI</b> : In Calendar View, selecting Year album shows all images of the full year.<br>
<b>AlbumGUI</b> : New status-bar indicator to report album icon view filtering status.<br>
<b>AlbumGUI</b> : Auto-completion in all search text filter.</p>
<h5>BUG FIXES:</h5>
<p><br><br>
001 ==&gt; 146393 : No gamma adjustment when opening RAW file.<br>
002 ==&gt; 158377 : digiKam duplicates downloaded images while overwriting existing ones.<br>
003 ==&gt; 151122 : Opening Albums Has Become Very Slow.<br>
004 ==&gt; 145252 : Umask settings used for album directory, not for image files.<br>
005 ==&gt; 144253 : Failed to update old Database to new Database format.<br>
006 ==&gt; 159467 : Misleading error message with path change in digikamrc file.<br>
007 ==&gt; 157237 : Connect the data when using the left tabs.<br>
008 ==&gt; 157309 : digiKam mouse scroll direction on lighttable.<br>
009 ==&gt; 158398 : Olympus raw images shown with wrong orientation.<br>
010 ==&gt; 152257 : Crash on saving images after editing.<br>
011 ==&gt; 153730 : Too many file format options when saving raw image from editor in digiKam.<br>
012 ==&gt; 151157 : Right and left keys goes to first picture and last picture without shortcuts.<br>
013 ==&gt; 151451 : Application crashed using edit.<br>
014 ==&gt; 139657 : Blueish tint in low saturation images converted to CMYK.<br>
015 ==&gt; 147600 : Showfoto Open folder - files shown in reverse order.<br>
016 ==&gt; 149851 : Showfoto asks if you want to save changes when deleting photo, if changes have been made.<br>
017 ==&gt; 138378 : Showfoto opens file browser window to last the folder used upon startup.<br>
018 ==&gt; 148964 : images are blur only inside showFoto.<br>
019 ==&gt; 146938 : Themes Don't Apply To All Panels.<br>
020 ==&gt; 135378 : Single-click on picture open it in editor.<br>
021 ==&gt; 142469 : Splash screen to be the very first thing.<br>
022 ==&gt; 147619 : Crash when viewing video.<br>
023 ==&gt; 136583 : Album icon does not show preview-image after selecting given image as preview.<br>
024 ==&gt; 129357 : Thumbnails not rotated for (D200)-NEF.<br>
025 ==&gt; 132047 : Faster display of images and/or prefetch wished for.<br>
026 ==&gt; 150259 : media-gfx/digikam-0.9.2 error/typo in showfoto.desktop.<br>
027 ==&gt; 150674 : Turn of live update of preview when moving points in curves dialog or in histogram.<br>
028 ==&gt; 141703 : Show file creation date in tooltip.<br>
029 ==&gt; 128377 : Adjust levels: histograms don't match, bad "auto level" function.<br>
030 ==&gt; 141755 : Sidebar: Hide unused tabs, Add tooltips.<br>
031 ==&gt; 146068 : Mixer does not work for green and blue selected as main channel and monochrome mode.<br>
032 ==&gt; 135931 : Reorganize Batch Processing tools from 'Image' and 'Tools' menus.<br>
033 ==&gt; 159664 : Background color of live/quick filter not updated on folder/date change.<br>
034 ==&gt; 096388 : Show number of images in the album.<br>
035 ==&gt; 155271 : Configure suggests wrong parameter for libjasper.<br>
036 ==&gt; 155105 : Broken png image present in albums folder causes digiKam to crash when starting.<br>
037 ==&gt; 146760 : Providing a Timeline-View for quickly narrowing down the date of photos.<br>
038 ==&gt; 146635 : Ratio crop doesn't remember orientation.<br>
039 ==&gt; 144337 : There should be no "empty folders".<br>
040 ==&gt; 128293 : Aspect ratio crop does not respect aspect ratio.<br>
041 ==&gt; 157149 : digiKam crash at startup.<br>
042 ==&gt; 141037 : Search using Tag Name does not work.<br>
043 ==&gt; 158174 : Precise aspect ratio crop feature.<br>
044 ==&gt; 142055 : Which whitebalance is used.<br>
045 ==&gt; 158558 : Delete Function in Tag Filters panel needs to make sure that the tag is unselected when the tag is deleted.<br>
046 ==&gt; 120309 : Change screen backgroundcolor of image tools.<br>
047 ==&gt; 153775 : Download from camera: Renaming because of already existing file does not work.<br>
048 ==&gt; 154346 : Can't rename in digiKam.<br>
049 ==&gt; 154625 : Image-files are not renamed before they are saved to disk.<br>
050 ==&gt; 154746 : Album selection window's size is wrong.<br>
051 ==&gt; 159806 : Cannot display Nikon d3 raw files.<br>
052 ==&gt; 148935 : digiKam hot pixel plugin can not read canon RAW files.<br>
053 ==&gt; 158776 : Confirm tag delete if tag assigned to photos.<br>
054 ==&gt; 148520 : Change of rating causes high CPU load.<br>
055 ==&gt; 140227 : Remove or modify D&amp;D tags menu from icon view area.<br>
056 ==&gt; 154421 : Live search doesn't search filenames.<br>
057 ==&gt; 118209 : digiKam hotplug script kde user detection problem.<br>
058 ==&gt; 138766 : JPEG Rotations: Give a warning when doing Lossy rotations (and offer to do lossless when applicable).<br>
059 ==&gt; 156007 : Canon Raw+Jpg Locks up download image window during thumbnail retrieval.<br>
060 ==&gt; 114465 : Simpler entry of tags.<br>
061 ==&gt; 159236 : Corrupted file when downloading from CF card via digiKam.<br>
062 ==&gt; 121309 : Camera list sorted by brand.<br>
063 ==&gt; 137836 : Usability: Configure camera gui add and auto-detect camera.<br>
064 ==&gt; 160323 : Changing the album name from sth to #sth and then to zsth causes the album to be deleted along with the files.<br>
065 ==&gt; 145083 : Space and Shift-Space isn't used for navigation between images.<br>
066 ==&gt; 158696 : digiKam image viewer crashes when "Save As" is clicked.<br>
067 ==&gt; 156564 : Light table crashes when choosing channel.<br>
068 ==&gt; 147466 : digiKam crashes on termination if an audio file played.<br>
069 ==&gt; 146973 : Crashes when clicking on preview image.<br>
070 ==&gt; 148976 : digiKam Deleted 2.3 g of Pictures.<br>
071 ==&gt; 145252 : Umask settings used for album directory, not for image files.<br>
072 ==&gt; 125775 : digiKam way to save and delete "Searches".<br>
073 ==&gt; 150908 : Canon g3 not recognized works version 0.9.1.<br>
074 ==&gt; 152092 : Showfoto crashes ( signal 11 ) on exit.<br>
075 ==&gt; 160840 : Wrong filtering on Album-Subtree-View.<br>
076 ==&gt; 160846 : Blurred preview of pics.<br>
077 ==&gt; 157314 : Zoom-slider has no steps.<br>
078 ==&gt; 161047 : Shift and wheel mouse doesn't work.<br>
079 ==&gt; 161087 : Conflicting directions of mouse scroll when zooming.<br>
080 ==&gt; 161084 : Not properly updates status bar info.<br>
081 ==&gt; 162132 : Crash while saving tiff (possibly caused by present alpha channel).<br>
082 ==&gt; 155046 : light-table usability, possible improvements.<br>
083 ==&gt; 161085 : Zoom steps in image editor.<br>
084 ==&gt; 162247 : The photos thumbnails list would be better if in the end of an album/date/tag start the next.<br>
085 ==&gt; 160523 : Crash when saving picture as new name after resizing.<br>
086 ==&gt; 162428 : debug info needed ?<br>
087 ==&gt; 147597 : Typos in the digiKam KDE3 PO file.<br>
088 ==&gt; 162496 : When Digikam saves file as JPEG - JPG options are not always presented.<br>
089 ==&gt; 160740 : Handbook says that libgphoto2 is required but does not say whether it should be compiled with exif support.<br>
090 ==&gt; 148400 : Problem loading 16bit Grayscale TIFF image.<br>
091 ==&gt; 160925 : digiKam crash, I don't know why, but I have the bugreport.<br>
092 ==&gt; 162691 : 0.9.4 beta5 compile error imagehistogram.cpp.<br>
093 ==&gt; 160966 : Some searches don't work properly with the new sqlite3-3.5.8.<br>
094 ==&gt; 162245 : Assigned tag list not updated correctly when "show assigned tags" filter is active.<br>
095 ==&gt; 158866 : Advanced Search on Tags a mess.<br>
096 ==&gt; 162814 : digiKam crashes when using light table (page down).<br>
097 ==&gt; 163151 : Red eye correction doesn't work fine.<br>
098 ==&gt; 163227 : Searching for tag names in simple and advanced search doesn't work.<br>
099 ==&gt; 163474 : Config UI using external media.<br>
100 ==&gt; 156420 : Canon EOS 400D, no thumbnails displayed then digikam segfaults.<br>
101 ==&gt; 163118 : digikam-0.9.4_beta5 compilation hangs with gcc 4.3.<br>
102 ==&gt; 164301 : Filter Albums sub-tree names too, when using quickfilter on icon view.<br>
103 ==&gt; 164482 : Place filename of image in klipper/paste buffer from thumbnail right mouse button context menu.<br>
104 ==&gt; 164536 : Add adjust mid range color levels.<br>
105 ==&gt; 164615 : Image detail info about geometry (X-pixel / Y-pixel) missed.<br>
106 ==&gt; 149654 : Fullscreen icon changes image's zoom to 100% (should fit to screen).<br>
107 ==&gt; 162688 : Digikam RC5, can't see scroll bar sliders in dark theme.<br>
108 ==&gt; 161212 : Switch to full screen resets X11.<br>
109 ==&gt; 164432 : Compilation fails with gcc 3.<br>
110 ==&gt; 164392 : Can't compile digikam with missing libkdcraw.<br>
111 ==&gt; 160617 : Same image gets opened to both sides of view when draggin image to right side.<br>
112 ==&gt; 165823 : Digikam crashes if last picture is removed from lighttable.<br>
113 ==&gt; 165921 : Translation bug: Missing german translation.<br>
114 ==&gt; 130906 : Allow user to click text of tags to tag an image (not checkbox only).<br>
115 ==&gt; 126871 : Click on parent folder in tree should generate a reaction.<br>
116 ==&gt; 162812 : Sharpen slider enhancement refresh previews continuously.<br>
117 ==&gt; 166274 : Rename files on download from camera.<br>
118 ==&gt; 166472 : Thumbnail bar gone in image editor when switching back from fullscreen.<br>
119 ==&gt; 166540 : Showfoto always prints filename.</p>

<div class="legacy-comments">

  <a id="comment-17652"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17652" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by mutlu (not verified) on Wed, 2008-07-16 23:10.</div>
    <div class="content">
     <p>Dear digikam developers,</p>
<p>Thank you all very much for this awesome piece of software. I am using it for years and am I completely happy with it. You are doing an amazing job!</p>
<p>A fan</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17653"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17653" class="active">Yay DigiKam!</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Thu, 2008-07-17 02:54.</div>
    <div class="content">
     <p>My favourite photo app just got even better! Thanks for all the time, effort and patience spent on DK. It shows. Can't wait to see it in the Kubuntu repos. :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17656"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17656" class="active">Congratulations!</a></h3>    <div class="submitted">Submitted by paurullan (not verified) on Thu, 2008-07-17 07:28.</div>
    <div class="content">
     <p>Congratulations to everybody that made this possible!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17657"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17657" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by maimon mons (not verified) on Thu, 2008-07-17 13:02.</div>
    <div class="content">
     <p>Wanted to add my thanks.</p>
<p>Even on Gnome, this is the best photo management software and truly an iPhoto killer.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17714"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17714" class="active">Tags</a></h3>    <div class="submitted">Submitted by dismantr (not verified) on Fri, 2008-08-22 07:50.</div>
    <div class="content">
     <p>I'm so waiting the new tag system ! Digikam is, for me, the greatest photographs manipulation software on Linux :-). Thanks for all that quality work !</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17795"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17795" class="active">For me, at crashes already in</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2008-09-21 13:54.</div>
    <div class="content">
     <p>For me, at crashes already in image import. Tried recompiling all the libs and the program, to no luck. It just crashes.</p>
<p>#9  0xb5bb9d50 in std::locale::operator= () from /usr/lib/libstdc++.so.5<br>
#10 0xb5bb287b in std::ios_base::_M_init () from /usr/lib/libstdc++.so.5<br>
#11 0xb5bb0bfb in std::basic_ios &gt;::init ()<br>
   from /usr/lib/libstdc++.so.5<br>
#12 0xb7436b40 in KExiv2Iface::KExiv2::getExifTagString ()<br>
   from /usr/lib/libkexiv2.so.3<br>
#13 0xb7da06c1 in Digikam::DMetadata::getPhotographInformations ()<br>
   from /usr/lib/libdigikam.so.0<br>
#14 0xb7e8b639 in Digikam::CameraItemPropertiesTab::setCurrentItem ()<br>
   from /usr/lib/libdigikam.so.0<br>
#15 0xb7e90c60 in Digikam::ImagePropertiesSideBarCamGui::slotChangedTab ()<br>
   from /usr/lib/libdigikam.so.0<br>
...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17658"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17658" class="active">absolutely amazing program,</a></h3>    <div class="submitted">Submitted by <a href="http://www.amigib.prv.pl" rel="nofollow">amigib</a> (not verified) on Thu, 2008-07-17 18:41.</div>
    <div class="content">
     <p>absolutely amazing program, thank you</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17659"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17659" class="active">ubuntu</a></h3>    <div class="submitted">Submitted by jure (not verified) on Thu, 2008-07-17 22:14.</div>
    <div class="content">
     <p>does anyone have debs for ubuntu 8.04 i386? :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17677"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17677" class="active">+ 1 to this. I would love a</a></h3>    <div class="submitted">Submitted by gdoutch (not verified) on Mon, 2008-07-28 01:25.</div>
    <div class="content">
     <p>+ 1 to this. I would love a deb for ubuntu hardy. I've tried like crazy to build but just can't ever seem to do it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17680"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17680" class="active">Also waiting...</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-08-02 10:47.</div>
    <div class="content">
     <p>I wish the repositories would update faster. Actually, I'm not even sure how that works. Anyone have a link explaining how a new release gets packaged and then put on the official Ubuntu repositories?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17686"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17686" class="active">digikam 0.9.4 for ubuntu 8.04</a></h3>    <div class="submitted">Submitted by jure (not verified) on Sat, 2008-08-09 22:33.</div>
    <div class="content">
     <p>There is a deb posted on ubuntu forums just search for it. It works for me, you just have to watch that it doesn't get "updated" when you do a dist-upgrade</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17661"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17661" class="active">crash of 0.9.4</a></h3>    <div class="submitted">Submitted by Simon (not verified) on Sat, 2008-07-19 15:42.</div>
    <div class="content">
     <p>Hi</p>
<p>I just tried compiling the latest digikam, but I first tried the make_all.sh script for the subversion thingy. So I may have bad library versions installed...</p>
<p>Digikam crashed during startup around here:</p>
<p>digikam: ImagePluginLoader: Loaded plugin ImagePlugin_ChannelMixer<br>
digikam: ImagePlugin_Charcoal plugin loaded<br>
digikam: ImagePluginLoader: Loaded plugin ImagePlugin_Charcoal<br>
digikam: Theme file loaded: /usr/share/apps/digikam/themes/Dessert<br>
Error: Directory ImageSubIfd0 with 57952 entries considered invalid; not read.<br>
terminate called after throwing an instance of 'std::length_error'<br>
  what():  basic_string::_S_create<br>
KCrash: Application 'digikam' crashing...</p>
<p>[1]+  Exit 253                digikam</p>
<p>This was the same with the svn and sourceforge 0.9.4 tarball...</p>
<p>Should I file a bugreport or recompile the libs (please tell me which tarballs I need :-)</p>
<p>Cheers</p>
<p>Simon</p>
<p>PS, apart from the colour profile bug in the editor (which I suspect isn't fixed yet), I was already very happy with 0.9.4rc1!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17664"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17664" class="active">Sound like a problem of</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-07-19 21:53.</div>
    <div class="content">
     <p>Sound like a problem of binary compatibility with a shared library. Please check libkexiv2 and Exiv2 installation on your computer</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17667"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17667" class="active">Merci!</a></h3>    <div class="submitted">Submitted by Jochen (not verified) on Thu, 2008-07-24 11:28.</div>
    <div class="content">
     <p>Thank you very much for this release!<br>
I've been using digikam for quite some time now (since 0.7.something) and from release to release it's getting better and better. I really love working with it.<br>
Adding stacked images (RAW,JPEG and edited files stacked together under one name) in a not so distant future release would fulfill all my wishes.</p>
<p>Cheers<br>
Jochen</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17670"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17670" class="active">Absolutely bloody awesome! I</a></h3>    <div class="submitted">Submitted by <a href="http://www.dfpe.pclinuxos.nl" rel="nofollow">newmikey</a> (not verified) on Fri, 2008-07-25 19:45.</div>
    <div class="content">
     <p>Absolutely bloody awesome! I could not be happier. Great job.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17671"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17671" class="active">Right click on an image = empty popup menu!</a></h3>    <div class="submitted">Submitted by Ralph (not verified) on Fri, 2008-07-25 21:50.</div>
    <div class="content">
     <p>First of all, thank you very much for this great program!</p>
<p>With Digikam 0.9.4 on KDE 3.5.9 I have noticed that, right clicking on an image, a popoup menu appears but, that menu is empty; if I move the cursor, some flashing text appears and disappears!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17672"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17672" class="active">This menu contents is taken</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2008-07-25 22:37.</div>
    <div class="content">
     <p>This menu contents is taken from your KDE control panel settings in file mime-types/applications configurations</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17673"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17673" class="active">No problems with Digikam 0.9.3</a></h3>    <div class="submitted">Submitted by Ralph (not verified) on Fri, 2008-07-25 23:18.</div>
    <div class="content">
     <p>Digikam 0.9.3 does not have that problem.</p>
<p>With Digikam 0.9.4 I get this:</p>
<p>http://img166.imageshack.us/my.php?image=digikam1bp6.jpg</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17674"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17674" class="active">Sound like a problem with</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-07-26 10:48.</div>
    <div class="content">
     <p>Sound like a problem with your KDE theme. try another one (plastik for ex.)</p>
<p>Note : it's not reproductible here.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17675"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17675" class="active">Problem solved!</a></h3>    <div class="submitted">Submitted by Ralph (not verified) on Sat, 2008-07-26 14:06.</div>
    <div class="content">
     <p>Gilles, you are right; it is a theme/style problem!</p>
<p>Using the "Domino" style, popup menus are empty; switching to "Plastik" style solves the problem!</p>
<p>Thanks a lot.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17676"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17676" class="active">Please return this problem to</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-07-27 11:21.</div>
    <div class="content">
     <p>Please return this problem to KDE theme designers team.</p>
<p>Thanks in advance</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-17744"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17744" class="active">Many Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://www.castelli-group.net" rel="nofollow">Dets</a> (not verified) on Sat, 2008-09-13 21:01.</div>
    <div class="content">
     <p>Dear digikam developers,</p>
<p>it's really fun to work with digikam. It makes previewing, scaling, cropping and correcting of photo series much easier than any other tool I used before.</p>
<p>Thanks very much and keep on your good work,</p>
<p>Dets ...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17883"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/359#comment-17883" class="active">nice article.</a></h3>    <div class="submitted">Submitted by <a href="http://www.nakliyatankara.com" rel="nofollow">evden eve nakliyat</a> (not verified) on Thu, 2008-10-23 22:22.</div>
    <div class="content">
     <p>good working .thanks</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
