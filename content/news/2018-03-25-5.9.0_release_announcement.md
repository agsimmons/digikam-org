---
date: "2018-03-24T00:00:00"
title: "digiKam 5.9.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, following the release 5.8.0 published in January 2018, the digiKam team is proud to announce the new stable release 5.9.0."
category: "news"
---

[![](https://c1.staticflickr.com/5/4783/40984191931_ec5bc20553_c.jpg "Albums View and Light Table")](https://www.flickr.com/photos/digikam/40984191931)

[Following the release of 5.8.0](https://www.digikam.org/news/2018-01-14-5.8.0_release_announcement/) published in January,
the digiKam team is proud to announce the new release 5.9.0 of the digiKam Software Collection. In this last version of 5.x, we has focused all developements to close bugs.

### Improvements and Fixes

Many small fixes has been commited over XMP sidecar support in this release but the most important works will be tested with next main release has these requires regression tests.
Mysql support has also recieve fixes about database schema migration which can introduce errors while converting old database files generated with digiKam 4.x.

[![](https://c1.staticflickr.com/5/4785/40984193011_e899c28e21_c.jpg "Time search and Lens correction tool")](https://www.flickr.com/photos/digikam/40984193011)

Face Management have been also fixed with some special workflow cases, but lead works still in pending queue for this summer especially to introduce the new recognition algorithms.
We also apply some small fixes about group and ungroup operations done by end users in icon-view while cataloging items.

As always, you can look at [the list of 45 resolved issues](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=5.9.0)
for detailed information.

digiKam 5.9.0 Software collection source code tarball, Linux 32/64 bits AppImage bundles,
MacOS package, and Windows 32/64 bits installers can be downloaded from [this repository](http://download.kde.org/stable/digikam/).

### Next stages

[![](https://c1.staticflickr.com/1/810/39175456270_ac308365e0_c.jpg "Map Search and Raw Import tool")](https://www.flickr.com/photos/digikam/39175456270)

We started to select our next Google Summer of Code participants with few [open projects](https://community.kde.org/GSoC/2018/Ideas#digiKam) for students.

Next major version 6.0.0 will look promising, and will introduce new features as a full support of video files management working as photo, a huge factoring of
code and less external dependencies to simplify application compilation, packaging, and maintenance for the next years. But this is an another story that we talk later in this room...

[The reports already closed](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=6.0.0) due to new implementations are already important,
and nothing is completed yet, as we plan few beta releases before this end of summer.

Happy digiKaming while capturing lovely spring colours!
