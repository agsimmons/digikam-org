---
date: "2007-10-08T14:29:00Z"
title: "digiKam 0.9.3-beta1 Release"
author: "gerhard"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release 0.9.3-beta1. The digiKam tarball can be downloaded from SourceForge as well."
category: "news"
aliases: "/node/255"

---

<p>Dear all digiKam fans and users!</p>
<p>The digiKam development team is happy to release <a href="http://digikam3rdparty.free.fr/0.9 releases/digikam-0.9.3-beta1.tar.bz2">0.9.3-beta1</a>. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a> as well.<br>
(0.9.3 will probably be the last release before we will switch over to KDE4 which will see a totally revamped digiKam.)</p>
<p>New features of the camera interface:</p>
<ul>
<li>The camera interface is now used to import new images into collections.</li>
<li>There are new options to download pictures and delete them from the camera at the same time.</li>
<li>Support of Drag &amp; Drop to download files from camera window to album window.</li>
<li>A progress bar has been added to indicate the available space in the  Album Library Path</li>
</ul>
<p>Improvements:</p>
<ul>
<li>Updated internal CImg library to last stable 1.2.3 (released at 2007/08/24)</li>
<li>Color scheme theme are now XML based (instead X11 format).</li>
</ul>
<p>Bug-fixes of KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</p>
<p>001 ==&gt; 120450 : Strange helper lines behavior in ratio crop tool.<br>
002 ==&gt; 147248 : Image info doesn't follow image in the editor.<br>
003 ==&gt; 147147 : digiKam does not apply tags to some selected images.<br>
004 ==&gt; 143200 : Mass renaming with F2 crashes digiKam.<br>
005 ==&gt; 147347 : Showfoto crashes when opening twice multiple files.<br>
006 ==&gt; 147269 : digiKam finds but fails to use libkdcraw.<br>
007 ==&gt; 147263 : Some icons are missing.<br>
008 ==&gt; 146636 : Adjust levels plugin: helper line for sliders.<br>
009 ==&gt; 147671 : Portability problem in configure script.<br>
010 ==&gt; 147670 : Compilation problems on NetBSD in greycstoration.<br>
011 ==&gt; 147362 : Tool-tip for zoom indicator is below the screen if window is maximized.<br>
012 ==&gt; 145017 : Deleting an image from within digiKam does not update the digiKam database.<br>
013 ==&gt; 148925 : Light Table thumb bar not updated when deleting images.<br>
014 ==&gt; 148971 : run-on menu entry "digiKamThemeDesigner".<br>
015 ==&gt; 148930 : digiKam-0.9.2 does not compile with lcms-1.17.<br>
016 ==&gt; 141774 : Auto-rotate does not work however KIPI rotation works.<br>
017 ==&gt; 103350 : Original image is silently overwritten when saving.<br>
018 ==&gt; 144431 : Add option in Camera Download Dialog.<br>
019 ==&gt; 131407 : Use camera GUI also for import of images from different locations.<br>
020 ==&gt; 143934 : Cannot download all photos from camera.<br>
021 ==&gt; 147119 : Can't link against static libjasper, configure reports jasper is missing.<br>
022 ==&gt; 147439 : It is too easy to delete a search.<br>
023 ==&gt; 147687 : Error when downloading and converting images.<br>
024 ==&gt; 137590 : Be able to modify the extension of images in the interface.<br>
025 ==&gt; 139024 : Camera GUI new items selection doesn't work.<br>
026 ==&gt; 139519 : digiKam silently fails to import when out of disc space.<br>
027 ==&gt; 149469 : Excessive trash confirmation dialogs after album is deleted.<br>
028 ==&gt; 148648 : Color managed previews not working in all plugins.<br>
029 ==&gt; 126427 : In "rename file" dialog, the 2nd picture is (and can't) not be displayed.<br>
030 ==&gt; 144336 : Selecting pictures on the camera makes scroll-bar jump back to the top of the list.<br>
031 ==&gt; 136927 : Failed to download file DCP_4321.jpg. Do you want to continue? and when continue is clicked the same warning comes on for the next image and so on.<br>
032 ==&gt; 146083 : Bugs in drag and drop.<br>
033 ==&gt; 147854 : Put images into an emptied light-table.<br>
034 ==&gt; 149578 : libjpeg JPEG sub-sampling setting is not user-controllable.<br>
035 ==&gt; 149685 : Go to next photo after current photo deletion (vs. to previous photo).<br>
036 ==&gt; 148233 : Adding texture generates black image.<br>
037 ==&gt; 147311 : Light Table RAW images does not rotate as def. by EXIF info.<br>
038 ==&gt; 146773 : Metadata sources preference when sorting images.<br>
039 ==&gt; 140133 : Metadata Edit Picture Comments Syncs to IPTC and EXIF when option is de-selected .<br>
040 ==&gt; ML     : Lost toolbar after building from svn</p>

<div class="legacy-comments">

</div>