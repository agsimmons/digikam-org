---
date: "2006-10-20T21:18:00Z"
title: "I can see the light: digiKam-0.9.0-beta3 and DigikamImagePlugins-0.9.0-beta3 have been released."
author: "Anonymous"
description: "Following the policy of the digiKam community to offer a high quality product to everybody, we are proud to present the new digiKam and DigikamImagePlugins"
category: "news"
aliases: "/node/152"

---

<p>Following the policy of the digiKam community to offer a high quality product to everybody, we are proud to present the new <strong>digiKam</strong> and <strong>DigikamImagePlugins 0.9.0-beta3</strong>  release. As usual, these news packages  have a lot of <strong>bugs fixed</strong> and <strong> improved features</strong>. Here's the summary of fixes:</p>
<ul>
<li> 134013 : Tag menu extremely slow.</li>
<li> 122653 : File-dialog claims that pictures are not on the local-storage, yet they are.</li>
<li>134091 : dcraw -n option not valid for version &gt; 8.15.</li>
<li> 134224 : prefix for image filename in camera dialog not working</li>
<li> 133359 : Google maps support to show location of the photos.</li>
<li> 131347 : Comments modified in digiKam Image Editor are not saved.</li>
<li> 134351 : Error while 'make install'.</li>
<li> 132841 : Tag filtering works only on second click on the tag filter list.</li>
<li> 134924 : Patch to allow compile with LDFLAGS="-Wl, --as-needed"</li>
<li> 134841 : weird behavior of identity setup.</li>
<li> 131382 : All thumbnails of album destroyed when using Tag Filters.</li>
<li> 134869 : High CPU usage while displaying ICC Profile.</li>
<li> 134761 : a rotated RAW image gets saved straight with an inconsistent Exif orientation.</li>
<li> 135236 : Right-click menu rename function cuts to the first period (not the extention one).</li>
<li> 135307 : After deleting a file, user comments entered for pictures apply to the wrong picture.</li>
<li> 135145 : Raw image converter fails on my raw files (cr2, crw, dng).</li>
<li> 125727 : digiKam open with of raw file only shows application for octet-stream.</li>
<li> 135430 : Typo automaticly should be automatically in raw image converter.</li>
<li> 135060 : Folders without pictures in it cannot be assigned icons</li>
</ul>
<p>If you want to know more about all the changes in this release, have a look at the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/NEWS?rev=596600&amp;view=markup">NEWS</a> and <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/ChangeLog?rev=595838&amp;view=markup">CHANGELOG</a> files. Feel free to  read about ours plans for the future in the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/TODO?rev=594143&amp;view=markup">TODO</a> file.</p>
<p>As you probably know whith the beta releases we are asking users to test this version of digiKam and to report bugs, so if you find one of these, please report it to the bug tracking system at bugs.kde.org. In a first step, make sure that there is not already a bug report for your problem. A list of open bug reports for digikam can be found <a href="http://bugs.kde.org/buglist.cgi?product=digikam&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">here</a>, and for digikamimageplugins in this <a href="http://bugs.kde.org/buglist.cgi?product=digikamimageplugins&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED">URL</a>.</p>
<p>If you are a digiKam user, you know you have a great piece of software in your desktop, otherwise we invite you to test a sophisticated, but easy to use, digital photo management application.</p>
<p><strong>Download</strong> your packages <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">here</a>.</p>

<div class="legacy-comments">

</div>