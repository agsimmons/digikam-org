---
date: "2010-06-03T13:34:00Z"
title: "Renaming Photos with digiKam"
author: "Dmitri Popov"
description: "Giving your photos meaningful names makes it significantly easier to keep tabs on them. Of course, renaming each and every photo by hand is not"
category: "news"
aliases: "/node/520"

---

<p>Giving your photos meaningful names makes it significantly easier to keep tabs on them. Of course, renaming each and every photo by hand is not particularly practical, especially if you take dozens or even hundreds of photos each day. This is when digiKam's Rename feature can come in rather handy. You can use it to define rather advanced renaming rules and apply them to multiple photos in one fell swoop. <a href="http://scribblesandsnaps.wordpress.com/2010/06/03/renaming-photos-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19319"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/520#comment-19319" class="active">renaming file with digikam</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-06-22 18:08.</div>
    <div class="content">
     <p>Nice feature... Is there a way to pick up my own keywords from the picture or database?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>