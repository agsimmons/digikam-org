---
date: "2008-03-24T15:12:00Z"
title: "digiKam 0.9.4-beta2 release"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release 0.9.4-beta2. The digiKam tarball can be downloaded from SourceForge as well."
category: "news"
aliases: "/node/304"

---

Dear all digiKam fans and users!<br><br>

The digiKam development team is happy to release 0.9.4-beta2. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a> as well.<br><br>

<strong>Note!</strong> This release works only with the latest libkdcraw and libkexiv2. You either have to compile them freshly from svn or wait a few days until the tarballs become available.

<br><br>
<a href="http://www.flickr.com/photos/digikam/2584055786/" title="digikam0.9.4-beta2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3053/2584055786_8e6dbba4a9_m.jpg" width="240" height="192" alt="digikam0.9.4-beta2"></a>
<br><br>

<h5>NEW FEATURES (since 0.9.3):</h5><br>

<b>General:</b><br><br>

1. Color theme scheme are pervasively applied in graphical interfaces, giving digiKam a real <a href="http://www.digikam.org/?q=node/302">Pro-look</a> when dark schemes are selected. Color theme scheme can be changed from either Editor or LightTable.<br>
2. Updated internal CImg library to last stable 1.2.7 (released on 2008/01/23).<br>
3. Add capability to display <a href="http://www.digikam.org/?q=node/283">items count</a> in all Album, Date, Tags, and Tags Filter folder views. The number of items contained in virtual or physical albums can be displayed next to its name. If a tree branch is collapsed, parent views sum-up the number of items from all undisplayed children views. Items count is performed in background by digiKam KIO-Slaves. A new option from Setup/Album dialog page can toggle on/off this feature.<br><br>

<b>AlbumGUI:</b><br><br>

4. Add a new tool to perform Date search around whole albums collection: <a href="http://www.digikam.org/?q=node/293">Time-Line</a>.
Timeline is a new left sidebar tab. It's a great tool complementary to the calendar. Try it out!<br>
5. In Calendar View, selecting Year album shows all images of the full year.<br>
6. New <a href="http://www.digikam.org/?q=node/296">status-bar indicator</a> to report album icon view filtering status.<br><br>

<b>ImageEditor:</b><br><br>

7. Raw files can be decoded in 16 bits color depth without to use Color Management. An auto-gamma and auto-white balance is processed using the same method than dcraw with 8 bits color depth RAW image decoding. This usefull to to have a speed-up RAW workflow with suitable images.<br><br>

<b>Showfoto:</b><br><br>

8. Added support of color theme schemes.<br>
9. Added 2 options to setup images ordering with "File/Open Folder" action.<br><br>


<h5>digiKam BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</h5><br>

001 ==&gt; 146393 : No gamma adjustment when opening RAW file.<br>
002 ==&gt; 158377 : digiKam duplicates downloaded images while overwriting existing ones.<br>
003 ==&gt; 151122 : Opening Albums Has Become Very Slow.<br>
004 ==&gt; 145252 : Umask settings used for album directory, not for image files.<br>
005 ==&gt; 144253 : Failed to update old Database to new Database format.<br>
006 ==&gt; 159467 : Misleading error message with path change in digikamrc file.<br>
007 ==&gt; 157237 : Connect the data when using the left tabs.<br>
008 ==&gt; 157309 : digiKam mouse scroll direction on lighttable.<br>
009 ==&gt; 158398 : Olympus raw images shown with wrong orientation.<br>
010 ==&gt; 152257 : Crash on saving images after editing.<br>
011 ==&gt; 153730 : Too many file format options when saving raw image from editor in digiKam.<br>
012 ==&gt; 151157 : Right and left keys goes to first picture and last picture without shortcuts.<br>
013 ==&gt; 151451 : Application crashed using edit.<br>
014 ==&gt; 139657 : Blueish tint in low saturation images converted to CMYK.<br>
015 ==&gt; 147600 : Showfoto Open folder - files shown in reverse order.<br>
016 ==&gt; 149851 : Showfoto asks if you want to save changes when deleting photo, if changes have been made.<br>
017 ==&gt; 138378 : Showfoto opens file browser window to last the folder used upon startup.<br>
018 ==&gt; 148964 : Images are blur only inside showfoto.<br>
019 ==&gt; 146938 : Themes Don't Apply To All Panels.<br>
020 ==&gt; 135378 : Single-click on picture open it in editor.<br>
021 ==&gt; 142469 : Splash screen to be the very first thing.<br>
022 ==&gt; 147619 : Crash when viewing video.<br>
023 ==&gt; 136583 : Album icon does not show preview-image after selecting given image as preview.<br>
024 ==&gt; 129357 : Thumbnails not rotated for (D200)-NEF.<br>
025 ==&gt; 132047 : Faster display of images and/or prefetch wished for.<br>
026 ==&gt; 150259 : media-gfx/digikam-0.9.2 error/typo in showfoto.desktop.<br>
027 ==&gt; 150674 : Turn of live update of preview when moving points in curves dialog or in histogram.<br>
028 ==&gt; 141703 : Show file creation date in tooltip.<br>
029 ==&gt; 128377 : Adjust levels: histograms don't match, bad "auto level" function.<br>
030 ==&gt; 141755 : Sidebar: Hide unused tabs, Add tooltips.<br>
031 ==&gt; 146068 : Mixer does not work for green and blue selected as main channel and monochrome mode.<br>
032 ==&gt; 135931 : Reorganise Batch Processing tools from 'Image' and 'Tools' menus.<br>
033 ==&gt; 159664 : Background color of live/quick filter not updated on folder/date change.<br>
034 ==&gt; 096388 : Show number of images in the album.<br>
035 ==&gt; 155271 : Configure suggests wrong parameter for libjasper.<br>
036 ==&gt; 155105 : Broken png image present in albums folder causes digikam to crash when starting.<br>
037 ==&gt; 146760 : Providing a Timeline-View for quickly narrowing down the date of photos.<br>
038 ==&gt; 146635 : Ratio crop doesn't remember orientation.<br>
039 ==&gt; 144337 : There should be no "empty folders".<br>
040 ==&gt; 128293 : Aspect ratio crop does not respect aspect ratio.<br>
041 ==&gt; 157149 : digiKam crash at startup.<br>
042 ==&gt; 141037 : Search using Tag Name does not work.<br>
043 ==&gt; 158174 : Precise aspect ratio crop feature.<br>
044 ==&gt; 142055 : Which whitebalance is used.<br>
045 ==&gt; 158558 : Delete Function in Tag Filters panel needs to make sure that the tag is unselected when the tag is deleted.<br>
046 ==&gt; 120309 : Change screen backgroundcolor of image tools.<br>
047 ==&gt; 153775 : Download from camera: Renaming because of already existing file does not work.<br>
048 ==&gt; 154346 : Can't rename in digiKam.<br>
049 ==&gt; 154625 : Image-files are not renamed before they are saved to disk.<br>
050 ==&gt; 154746 : Album selection window's size is wrong.<br>

<div class="legacy-comments">

</div>
