---
date: "2012-02-08T15:03:00Z"
title: "digiKam Software Collection 2.6.0 beta1 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 1st digiKam Software Collection 2.6.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/643"

---

<a href="http://www.flickr.com/photos/digikam/6802122711/" title="digiKam-maintenance by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7156/6802122711_2d8f9d5b4a_z.jpg" width="640" height="348" alt="digiKam-maintenance"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 1st digiKam Software Collection 2.6.0 beta release!</p>

<p>With this release, digiKam include a lots of bugs fixes and new features introduced to last <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2011">Coding Sprint from Genoa</a>.</p>

<p>digiKam include now a progress manager to control all paralelized process done in background. Also, a new Maintenance Tool have been implemented to simplify all maintenance tasks to process on your whole collections.</p>


<p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/37b056237af2e84fc580844e3e1f7e3c18da9fb2/entry/NEWS">the list of digiKam file closed</a> with this release into KDE bugzilla.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/f9289e71409676d7a86cef9a877ca6af1aa3a20d/entry/NEWS">the list of Kipi-plugins file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20195"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/643#comment-20195" class="active">Links to changes are not working</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-02-10 13:41.</div>
    <div class="content">
     <p>The link to the list of digiKam bugs closed leads to a page that prompts me to log in. Is the link correct? Maybe it works for you if you have a previously remembered login.</p>
<p>The link to the list of Kipi-plugins bugs closed opens a prompt to save "NEWS" as a "BIN" file. Is the MIME type correct, or is that beyond your control?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20196"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/643#comment-20196" class="active">fixed...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2012-02-10 14:06.</div>
    <div class="content">
     <p>links fixed...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20199"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/643#comment-20199" class="active">Macs</a></h3>    <div class="submitted">Submitted by Chris Duffy (not verified) on Thu, 2012-02-16 21:16.</div>
    <div class="content">
     <p>I really really really love digikam- in fact, I'm using linux just because of digikam. Everything else I do in OS X. I prefer it that hard over iPhoto and Aperture.</p>
<p>I know digikam can run on OS X, I've had it running once or twice but found it to be unstable. Is there a digikam-on-mac effort I can be directed to / become part of? I'd love to try this beta on the OS X side.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20200"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/643#comment-20200" class="active">DVD Slideshows</a></h3>    <div class="submitted">Submitted by Arthur Dunning III (not verified) on Tue, 2012-02-21 15:17.</div>
    <div class="content">
     <p>As much as I like Digikam, I really wish it had the capability to make MPEG slideshows like it did in the days of Ubuntu 9.04. I can only hope someone has a solution for this. :(</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
