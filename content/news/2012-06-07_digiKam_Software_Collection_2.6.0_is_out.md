---
date: "2012-06-07T11:03:00Z"
title: "digiKam Software Collection 2.6.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam Software Collection 2.6.0. With this release, digiKam include a lots of bugs"
category: "news"
aliases: "/node/656"

---

<a href="http://www.flickr.com/photos/digikam/7162570191/" title="digikam2.6.0 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7087/7162570191_4d3c54e077_c.jpg" width="800" height="320" alt="digikam2.6.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam Software Collection 2.6.0.</p>

<p>With this release, digiKam include a lots of bugs fixes about XMP sidecar file supports. New features have been also introduced to last <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2011">Coding Sprint from Genoa</a>. You can read a <a href="http://dot.kde.org/2012/02/22/digikam-team-meets-genoa-italy">resume of this event</a> to dot KDE web page. Thanks again to <a href="http://ev.kde.org">KDE-ev</a> to sponsorship digiKam team...</p>

<p>digiKam include now a progress manager to control all parallelized process running in background. This progress manager is also able to follow processing from Kipi-plugins.</p>

<a href="http://www.flickr.com/photos/digikam/6812381420/" title="progressmanager by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7055/6812381420_1463394eba_z.jpg" width="640" height="256" alt="progressmanager"></a>

<p>Also, a new Maintenance Tool have been implemented to simplify all maintenance tasks to process on your whole collections.</p>

<a href="http://www.flickr.com/photos/digikam/6812384920/" title="maintenancetool01 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7062/6812384920_cb3424fd22_z.jpg" width="640" height="256" alt="maintenancetool01"></a>

<p>Kipi-plugins includes a new tool to export your collections to <a href="http://imageshack.us/">ImageShack Web Service</a>. Thanks to <b>Victor Dodon</b> to contribute.</p>

<a href="http://www.flickr.com/photos/digikam/6812381534/" title="imageshackexport by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7205/6812381534_b1282f3bcb_z.jpg" width="640" height="256" alt="imageshackexport"></a>

<p>digiKam have been ported to <a href="http://www.littlecms.com/">LCMS version 2</a> color management library by <b>Francesco Riosa</b>. Also a new tool dedicated to manage colors from scanned <a href="http://en.wikipedia.org/wiki/Reversal_film">Reversal Films</a> have been implemented by <b>Matthias Welwarsky</b>.

<a href="http://www.flickr.com/photos/digikam/6955685535/" title="negativefimtool by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7059/6955685535_48cf2bcdfc_z.jpg" width="640" height="256" alt="negativefimtool"></a>

</p><p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=2.6.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKaming...</p>
<div class="legacy-comments">

  <a id="comment-20239"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20239" class="active">Any hope for a windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2012-06-07 11:26.</div>
    <div class="content">
     <p>Any hope for a windows version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20242"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20242" class="active">just 2.5 , at this moment -</a></h3>    <div class="submitted">Submitted by Bush_Cat (not verified) on Thu, 2012-06-07 19:26.</div>
    <div class="content">
     <p>just 2.5 , at this moment - but WIN_32_bit<br>
http://sourceforge.net/projects/digikam/files/</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20257"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20257" class="active">Yeah!, when Adobe comes out</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Sun, 2012-06-17 09:11.</div>
    <div class="content">
     <p>Yeah!, when Adobe comes out with a Linux version of Lightroom 4. Just Joking!. Opensource developers are better than that.:)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20281"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20281" class="active">digiKam 2.6.0 released</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2012-07-12 19:10.</div>
    <div class="content">
     <p>I released digiKam 2.6.0 for windows last night, built against KDE 4.8.3 and Qt 4.8.0. Installer is now more robust, handling uninstall of old versions, and requiring reboot only with previous versions of digiKam in rare cases. Some old bugs in underlying KDE have been closed for Windows in this version including a bug which prevented files from being moved or deleted from within digiKam. I will try to release 2.7.0 soon. You can download the new release from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.6.0/digiKam-installer-2.6.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20240"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20240" class="active">The two bugzilla links lead to 403 forbidden</a></h3>    <div class="submitted">Submitted by davidv (not verified) on Thu, 2012-06-07 11:39.</div>
    <div class="content">
     <p>I think they should be http (not https) ?</p>
<p>thanks !</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20241"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20241" class="active">Hope for a real digikam ppa</a></h3>    <div class="submitted">Submitted by Julien Martin (not verified) on Thu, 2012-06-07 14:40.</div>
    <div class="content">
     <p>Hi<br>
Digikam is a great software, however, every update is not so straightforward to install.<br>
Compiling from source could take a while depending on the computer (1h30 on mine).<br>
The ppa suggested for ubuntu (ppa:philip5/extra) also have a wide collection of others software that most digikam user are not interested in updating via this ppa.<br>
Could we hope for a real Digikam only ppa? That will be fantastic.<br>
Julien</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20243"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20243" class="active">standalone digiKam PPA pls :-)</a></h3>    <div class="submitted">Submitted by Michael (not verified) on Fri, 2012-06-08 01:00.</div>
    <div class="content">
     <p>Full ACK!<br>
+1</p>
<p>PS: man, this captcha can be reeeeally hard...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20244"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20244" class="active">Most important!!</a></h3>    <div class="submitted">Submitted by TheRealOne (not verified) on Fri, 2012-06-08 08:55.</div>
    <div class="content">
     <p>Hi everybody,<br>
an "offical" ppa is imho the most important point - people use old versions for years because of this missing ppa (including me).<br>
I personally dont know this "philip5", maybe he is smart, a nice guy and i should meet him. But, and its a big but: i would _never_ use such a ppa on my system.</p>
<p>And i cant understand why there is no "offical" ppa.</p>
<p>PLEASE: go on with this...</p>
<p>and, of course: good work, i very much appreciate this program and the philosophy behind! Thank you, guys (and girls...)!!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20245"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20245" class="active">Please ask that from your</a></h3>    <div class="submitted">Submitted by Fri13 on Fri, 2012-06-08 22:04.</div>
    <div class="content">
     <p>Please ask that from your distributor or their unstable/testing packages to take latest. </p>
<p>Every distribution community has few great packagers who contribute to their community by packaging latest versions of specific software outside of distribution official maintenance process so users don't need to wait latest distribution release. </p>
<p>DigiKam team does not compile packages for different distributions but just source packages for distributors and their community to distribute latest digiKam for their users.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20260"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20260" class="active">@ TheRealOne
I can appreciate</a></h3>    <div class="submitted">Submitted by JustMe (not verified) on Sun, 2012-06-24 19:26.</div>
    <div class="content">
     <p>@ TheRealOne</p>
<p>I can appreciate your apprehension about using Phillip's PPA but with a quick look at his Launchpad User page (https://launchpad.net/~philip5) you can see that he has been a member since 2007, he has signed the Ubuntu Code of Conduct, has a very high Karma rating 600+ and is a member of many Ubuntu teams. I too am security conscious but after my investigation I feel good about this PPA. If my computers melts down into a puddle of goo tomorrow then I tried at least. </p>
<p>I am now running dK 2.6.0 with Gimp 2.8 :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20246"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20246" class="active">Ubuntu PPA isn't meant for</a></h3>    <div class="submitted">Submitted by Fri13 on Fri, 2012-06-08 22:14.</div>
    <div class="content">
     <p>Ubuntu PPA isn't meant for official and stable systems but for users who are willing to risk stability and security of their systems to get latest or tweaked versions of software. </p>
<p>If someone already maintains PPA for digiKam, you can install and update only the digiKam without upgrading other packages from same PPA. </p>
<p>You can do that by learning how to upgrade or install manually for PPA without doing full system upgrade.<br>
Sometimes digiKam needs latest KDE platform or libraries what have not been backported to latest Ubuntu release.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20249"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20249" class="active">Wherer is the problem?
If you</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-06-10 21:00.</div>
    <div class="content">
     <p>Wherer is the problem?</p>
<p>If you add philip5 ppa and you have bevore install digikam from the ubuntu sorce (and e.g. gimp) after update, there are only the files for digilam and (gimp) to update in the software center. After update you disable this ppa up to the next update.</p>
<p>And by the way, my gimp is now updated to the latest version 2.8 :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20247"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20247" class="active">for Windows???</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-06-09 11:12.</div>
    <div class="content">
     <p>for Windows???</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20248"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20248" class="active">I'd also love to have an</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-06-10 09:53.</div>
    <div class="content">
     <p>I'd also love to have an answer to this question, even if it is "no". Because then I could stop visiting this website almost every day, and start using another software.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20250"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20250" class="active">windows packaging is under progress...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2012-06-11 09:27.</div>
    <div class="content">
     <p>...Please wait...</p>

<a href="http://www.flickr.com/photos/digikam/7360898194/" title="digikam2.6.0-win7 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7223/7360898194_fe525762a8_c.jpg" width="800" height="640" alt="digikam2.6.0-win7"></a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20263"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20263" class="active">Any Windows 7 build updates?</a></h3>    <div class="submitted">Submitted by Ryan (not verified) on Mon, 2012-06-25 15:27.</div>
    <div class="content">
     <p>Any Windows 7 build updates?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20264"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20264" class="active">Windwos Packaging</a></h3>    <div class="submitted">Submitted by Wolfgang (not verified) on Tue, 2012-06-26 20:44.</div>
    <div class="content">
     <p>Hello,</p>
<p>when is the Windows pacjaging Process finished?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20271"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20271" class="active">Hope to see the Windows</a></h3>    <div class="submitted">Submitted by Mastergumble (not verified) on Wed, 2012-07-04 23:32.</div>
    <div class="content">
     <p>Hope to see the Windows version live soon</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20272"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20272" class="active">Any windows update?</a></h3>    <div class="submitted">Submitted by Hank (not verified) on Thu, 2012-07-05 10:06.</div>
    <div class="content">
     <p>Thanks. Can we expect a windows version in days, weeks, or months? Don't get me wrong, I'm grateful that you provide one at all. But since the older versions didn't work properly on my system, it would be really help to know when I will be able to coninue working on my archive again.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20282"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20282" class="active">digiKam 2.6.0 for Windows is released</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2012-07-12 19:12.</div>
    <div class="content">
     <p>I released digiKam 2.6.0 for windows last night, built against KDE 4.8.3 and Qt 4.8.0. Installer is now more robust, handling uninstall of old versions, and requiring reboot only with previous versions of digiKam in rare cases. Some old bugs in underlying KDE have been closed for Windows in this version including a bug which prevented files from being moved or deleted from within digiKam. I will try to release 2.7.0 soon. You can download the new release from <a href="http://sourceforge.net/projects/digikam/files/digikam/2.6.0/digiKam-installer-2.6.0-win32.exe/download">here</a>.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20253"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20253" class="active">Digikam on OSX</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-06-12 14:28.</div>
    <div class="content">
     <p>I tried several times to install digikam on OSX (SL) but never succeded.<br>
So i read about the new version these days and gave it another try but also failed.<br>
I tried using macports.<br>
So of course you do not have big interest in helping apple fanboys, so i would anyway appreciate any help, tips, or links to people who succeded in installing up to date digikam on osx</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20254"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20254" class="active">Look my MACOSX README...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2012-06-12 20:42.</div>
    <div class="content">
     <p>I personally installed recently whole digiKam under an iMac following my <a href="https://projects.kde.org/projects/extragear/graphics/digikam/digikam-software-compilation/repository/revisions/master/entry/README.MACOSX">tutorial through macports</a></p>


<a href="http://www.flickr.com/photos/digikam/7170729158/" title="digiKam MACOSX by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7244/7170729158_c4feea9276.jpg" width="500" height="281" alt="digiKam MACOSX"></a>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20265"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20265" class="active">No driver loaded</a></h3>    <div class="submitted">Submitted by <a href="http://www.mikeg.de" rel="nofollow">mikeg</a> (not verified) on Thu, 2012-06-28 09:53.</div>
    <div class="content">
     <p>In the first place i submitted an error for 2.6 RC where the mysql connection couldn't be established and results with "Driver not loaded". This was fixed in your mentioned tutorial with the command<br>
<code>sudo port -v install qt4-mac +mysql</code></p>
<p>By trying to make a clean and simple install via<br>
<code>sudo port install digikam</code></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20266"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20266" class="active">No driver loaded</a></h3>    <div class="submitted">Submitted by <a href="http://www.mikeg.de" rel="nofollow">mikeg</a> (not verified) on Thu, 2012-06-28 12:08.</div>
    <div class="content">
     <p>Wel, reinstalling mysql with the above command works fine. Anyway adding removable storages types can't be recognized on MAC OSX 10.7 (think this won't be any problem at all) … but using the file browser give me an<br>
<code>KDEInit could not launch 'dolphin'</code><br>
I'm going to submit this to the ticket system. This post was just for intel if anyones looking for a solution.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20258"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20258" class="active">The new parallel process</a></h3>    <div class="submitted">Submitted by Alphazo (not verified) on Tue, 2012-06-19 14:18.</div>
    <div class="content">
     <p>The new parallel process manager is just great, bringing the whole Digikam experience to another level. No more lags when displaying thumbnails and fingerprint generation is done in a flash.</p>
<p>Bravo!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20259"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20259" class="active">menu's bug</a></h3>    <div class="submitted">Submitted by Vince (not verified) on Sun, 2012-06-24 07:16.</div>
    <div class="content">
     <p>There is a strange thing with menu</p>
<p>menu exportation contain "import from smug smug, import from remote computer, import from picasaweb, import from facebook, and import from scanner</p>
<p>the exportation's tools are in the tools' menu</p>
<p>it's work fine but it's really strange that the importation tool are in the exportation's menu..</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20268"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20268" class="active">digikam for Mac OSX</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-07-04 18:54.</div>
    <div class="content">
     <p>I just went through the Macports installation of version 2.6.0. Everything seemed to work fine. First time I started it up, I launched dbus manually first, then went to the launchpad of the mac computer and clicked on the digikam icon. It started up and went through the first time configuration. then died. Now, every time I start it up, i scans the directories, then goes to "checking ICC repository" and dies at this point. Any ideas?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20269"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20269" class="active">ICC profile file problem...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2012-07-04 20:13.</div>
    <div class="content">
     <p>It's probably an icc profile file problem with LCMS library. Can you take a GDB backtrace please ?</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20270"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/656#comment-20270" class="active">maybe a little late for that.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-07-04 21:40.</div>
    <div class="content">
     <p>maybe a little late for that. I uninstalled all the ports I had (I'd only ever tried this one) and started fresh following your directions from a previous post on how to install. Everything went well up to the 'make' command. It failed at about 95% complete with the following:</p>
<p>/Users/corinna/2.x/core/utilities/facedetection/facepipeline.cpp:1191:5: error: no matching function for call to 'wait'<br>
    wait();<br>
    ^~~~<br>
/usr/include/sys/wait.h:255:7: note: candidate function not viable: requires 1 argument, but 0 were provided<br>
pid_t   wait(int *) __DARWIN_ALIAS_C(wait);<br>
        ^<br>
1 error generated.<br>
make[2]: *** [core/digikam/CMakeFiles/digikam.dir/__/utilities/facedetection/facepipeline.cpp.o] Error 1<br>
make[1]: *** [core/digikam/CMakeFiles/digikam.dir/all] Error 2<br>
make: *** [all] Error 2</p>
<p>so now I'm stuck here...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
