---
date: "2009-04-11T18:47:00Z"
title: "digiKam and Showfoto 0.11 splash-screen contest : best shots selection..."
author: "digiKam"
description: "I have receive a lots of candidates for digiKam and Showfoto splash-screens contest opened 2 week ago. Around 70 mails are in my gmail account."
category: "news"
aliases: "/node/438"

---

<p>

<a href="http://www.flickr.com/photos/digikam/3403534001/" title="digikam011-win32-01 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3637/3403534001_4b796fe576.jpg" width="350" height="260" alt="digikam011-win32-01"></a>

<a href="http://www.flickr.com/photos/digikam/3433463819/" title="showfoto-startup by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3619/3433463819_1145bc1e2c.jpg" width="350" height="260" alt="showfoto-startup"></a>

</p>

<p>I have receive a lots of candidates for digiKam and Showfoto splash-screens contest opened 2 week ago. Around <b>70 mails</b> are in my gmail account. So, thanks to all contributors.</p>

<p>After a first selection done by myself, only best shots still in the list. It's time now to review all candidates with digiKam.org team and choose the best one for digiKam and Showfoto. It will be not easy because all images are great to see...</p>

<a href="http://www.flickr.com/photos/digikam/3394839655/" title="IMG_2603_DxO_raw by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3555/3394839655_b5e1107b57_t.jpg" width="100" height="67" alt="IMG_2603_DxO_raw"></a>

<a href="http://www.flickr.com/photos/digikam/3395891082/" title="CIMG0406 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3573/3395891082_7419b2836b_t.jpg" width="100" height="67" alt="CIMG0406"></a>

<a href="http://www.flickr.com/photos/digikam/3395216755/" title="hkong by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3454/3395216755_29d83e9d31_t.jpg" width="100" height="67" alt="hkong"></a>

<a href="http://www.flickr.com/photos/digikam/3397424103/" title="lagon by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3618/3397424103_d41c6df2c0_t.jpg" width="100" height="67" alt="lagon"></a>

<a href="http://www.flickr.com/photos/digikam/3397653703/" title="IMG_3972 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3441/3397653703_09edba68ce_t.jpg" width="100" height="67" alt="IMG_3972"></a>

<a href="http://www.flickr.com/photos/digikam/3398463534/" title="IMG_5716 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3598/3398463534_15192d38db_t.jpg" width="100" height="67" alt="IMG_5716"></a>

<a href="http://www.flickr.com/photos/digikam/3397653905/" title="IMG_5718 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3649/3397653905_d1bc44f819_t.jpg" width="100" height="67" alt="IMG_5718"></a>

<a href="http://www.flickr.com/photos/digikam/3398463766/" title="IMG_5750 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3556/3398463766_c526431089_t.jpg" width="100" height="67" alt="IMG_5750"></a>

<a href="http://www.flickr.com/photos/digikam/3399200841/" title="rudebekia by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3626/3399200841_f509efdcd2_t.jpg" width="100" height="67" alt="rudebekia"></a>

<a href="http://www.flickr.com/photos/digikam/3399199587/" title="chevaux by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3658/3399199587_5625673fd0_t.jpg" width="100" height="67" alt="chevaux"></a>

<a href="http://www.flickr.com/photos/digikam/3400037584/" title="WR_20081102_E5_b02_2093_COPY by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3613/3400037584_90ac7e0d24_t.jpg" width="100" height="67" alt="WR_20081102_E5_b02_2093_COPY"></a>

<a href="http://www.flickr.com/photos/digikam/3400037784/" title="WR_20071104_LX_103_0875_COPY by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3634/3400037784_c0ff77bb5e_t.jpg" width="100" height="67" alt="WR_20071104_LX_103_0875_COPY"></a>

<a href="http://www.flickr.com/photos/digikam/3399229519/" title="WR_20061007_LX_101_0017_COPY by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3579/3399229519_f075356cd0_t.jpg" width="100" height="67" alt="WR_20061007_LX_101_0017_COPY"></a>

<a href="http://www.flickr.com/photos/digikam/3400920189/" title="digkam_splash_kanwar_plaha_1024 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3444/3400920189_221c1443dc_t.jpg" width="100" height="67" alt="digkam_splash_kanwar_plaha_1024"></a>

<a href="http://www.flickr.com/photos/digikam/3400949999/" title="showfoto_splash_Kanwar_Plaha by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3637/3400949999_f4242d6f98_t.jpg" width="100" height="67" alt="showfoto_splash_Kanwar_Plaha"></a>

<a href="http://www.flickr.com/photos/digikam/3401775414/" title="img_2598 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3538/3401775414_56ded51fdb_t.jpg" width="100" height="67" alt="img_2598"></a>

<a href="http://www.flickr.com/photos/digikam/3403716042/" title="goeland by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3541/3403716042_5157c46789_t.jpg" width="100" height="67" alt="goeland"></a>

<a href="http://www.flickr.com/photos/digikam/3404065573/" title="3236407312_7830cc8ba8_b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3587/3404065573_7f47c0183c_t.jpg" width="100" height="67" alt="3236407312_7830cc8ba8_b"></a>

<a href="http://www.flickr.com/photos/digikam/3404876834/" title="2425312074_344738359f_b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3602/3404876834_e052d62bb6_t.jpg" width="100" height="67" alt="2425312074_344738359f_b"></a>

<a href="http://www.flickr.com/photos/digikam/3406364858/" title="pc119328 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3614/3406364858_83636a9572_t.jpg" width="100" height="67" alt="pc119328"></a>

<a href="http://www.flickr.com/photos/digikam/3406364500/" title="pb017588 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3456/3406364500_eebbab04a8_t.jpg" width="100" height="67" alt="pb017588"></a>

<a href="http://www.flickr.com/photos/digikam/3406364120/" title="p8204737 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3554/3406364120_ed38b74428_t.jpg" width="100" height="67" alt="p8204737"></a>

<a href="http://www.flickr.com/photos/digikam/3405552009/" title="p8104501 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3472/3405552009_7c4effc0bc_t.jpg" width="100" height="67" alt="p8104501"></a>

<a href="http://www.flickr.com/photos/digikam/3405551557/" title="p2230528 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3645/3405551557_9c05c20536_t.jpg" width="100" height="67" alt="p2230528"></a>

<a href="http://www.flickr.com/photos/digikam/3405551129/" title="IMG_1151 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3557/3405551129_254191685a_t.jpg" width="100" height="67" alt="IMG_1151"></a>

<a href="http://www.flickr.com/photos/digikam/3406990975/" title="The_Alps by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3357/3406990975_aa048ef357_t.jpg" width="100" height="67" alt="The_Alps"></a>

<a href="http://www.flickr.com/photos/digikam/3406990825/" title="Lake_EveningSun by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3537/3406990825_1da1ce4e91_t.jpg" width="100" height="67" alt="Lake_EveningSun"></a>

<a href="http://www.flickr.com/photos/digikam/3410753589/" title="_MG_4585 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3372/3410753589_f2d0962f1e_t.jpg" width="100" height="67" alt="_MG_4585"></a>

<a href="http://www.flickr.com/photos/digikam/3411573374/" title="_MG_6745.CR2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3655/3411573374_e17a3742d6_t.jpg" width="100" height="67" alt="_MG_6745.CR2"></a>

<a href="http://www.flickr.com/photos/digikam/3410834507/" title="_MG_7787 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3578/3410834507_9fc13c7596_t.jpg" width="100" height="67" alt="_MG_7787"></a>

<a href="http://www.flickr.com/photos/digikam/3413865979/" title="digikam_Sebastian by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3160/3413865979_3bf2d0617d_t.jpg" width="100" height="67" alt="digikam_Sebastian"></a>

<a href="http://www.flickr.com/photos/digikam/3413882411/" title="Schottland_Skye_Tour_West__Neist_Point_2008 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3580/3413882411_23bc8d8413_t.jpg" width="100" height="67" alt="Schottland_Skye_Tour_West__Neist_Point_2008"></a>

<a href="http://www.flickr.com/photos/digikam/3415003617/" title="IMG_1273_coloured_1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3343/3415003617_bca44b4d8f_t.jpg" width="100" height="67" alt="IMG_1273_coloured_1"></a>

<a href="http://www.flickr.com/photos/digikam/3415809870/" title="IMG_1282_partly_coloured_1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3359/3415809870_3ba5d9f2d0_t.jpg" width="100" height="67" alt="IMG_1282_partly_coloured_1"></a>

<a href="http://www.flickr.com/photos/digikam/3417916600/" title="dscf2955 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3550/3417916600_2dce422cfb_t.jpg" width="100" height="67" alt="dscf2955"></a>

<a href="http://www.flickr.com/photos/digikam/3417938744/" title="dscf2255 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3349/3417938744_3c1f3d99c4_t.jpg" width="100" height="67" alt="dscf2255"></a>

<a href="http://www.flickr.com/photos/digikam/3420537867/" title="p8120257 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3646/3420537867_ba09472462_t.jpg" width="100" height="67" alt="p8120257"></a>

<a href="http://www.flickr.com/photos/digikam/3420537505/" title="p8050181 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3544/3420537505_7597e4b094_t.jpg" width="100" height="75" alt="p8050181"></a>

<a href="http://www.flickr.com/photos/digikam/3422247898/" title="IMG_4998 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3657/3422247898_2bf4e7dd4e_t.jpg" width="100" height="67" alt="IMG_4998"></a>

<a href="http://www.flickr.com/photos/digikam/3423148451/" title="2909908887_c58e0738e1_o by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3660/3423148451_8071f03a2d_t.jpg" width="100" height="67" alt="2909908887_c58e0738e1_o"></a>

<a href="http://www.flickr.com/photos/digikam/3423959084/" title="2985655159_350316c6a5_b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3311/3423959084_838a47d7e6_t.jpg" width="100" height="67" alt="2985655159_350316c6a5_b"></a>

<a href="http://www.flickr.com/photos/digikam/3423992996/" title="r0013787_landscape by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3109/3423992996_160a424f23_t.jpg" width="100" height="67" alt="r0013787_landscape"></a>

<a href="http://www.flickr.com/photos/digikam/3425029806/" title="rla_7516b by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3633/3425029806_64c2ca4c62_t.jpg" width="100" height="67" alt="rla_7516b"></a>

<a href="http://www.flickr.com/photos/digikam/3426182930/" title="002 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3645/3426182930_a8622d4c8c_t.jpg" width="100" height="67" alt="002"></a>

<a href="http://www.flickr.com/photos/digikam/3425374475/" title="001 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3543/3425374475_6983ef0c03_t.jpg" width="100" height="67" alt="001"></a>

<a href="http://www.flickr.com/photos/digikam/3426183082/" title="003 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3577/3426183082_d978c51713_t.jpg" width="100" height="67" alt="003"></a>

<a href="http://www.flickr.com/photos/digikam/3425374627/" title="004 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3411/3425374627_bab4b6572f_t.jpg" width="100" height="67" alt="004"></a>

<a href="http://www.flickr.com/photos/digikam/3425670241/" title="IMGP5820 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3650/3425670241_64dd5ecfda_t.jpg" width="100" height="67" alt="IMGP5820"></a>

<a href="http://www.flickr.com/photos/digikam/3427654426/" title="img_0604-11 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3401/3427654426_fbdeca5a31_t.jpg" width="100" height="67" alt="img_0604-11"></a>

<a href="http://www.flickr.com/photos/digikam/3426845561/" title="img_0603-1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3590/3426845561_27483f702b_t.jpg" width="100" height="67" alt="img_0603-1"></a>

<a href="http://www.flickr.com/photos/digikam/3431846455/" title="anochecer by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3597/3431846455_e57d9ba4b1_t.jpg" width="100" height="67" alt="anochecer"></a>

<a href="http://www.flickr.com/photos/digikam/3432660744/" title="sun_behind_clouds by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3659/3432660744_5942160b1a_t.jpg" width="100" height="67" alt="sun_behind_clouds"></a>

<a href="http://www.flickr.com/photos/digikam/3431845517/" title="cherry_tree_flowers by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3408/3431845517_d56ecb427c_t.jpg" width="100" height="67" alt="cherry_tree_flowers"></a>

<a href="http://www.flickr.com/photos/digikam/3434046522/" title="San Diego Noche by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3387/3434046522_876dd3779e_t.jpg" width="100" height="67" alt="San Diego Noche"></a>

<div class="legacy-comments">

  <a id="comment-18429"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18429" class="active">Random images</a></h3>    <div class="submitted">Submitted by <a href="http://solidsmoke.blogspot.com" rel="nofollow">Syam</a> (not verified) on Sun, 2009-04-12 03:33.</div>
    <div class="content">
     <p>How about selecting more than one, and showing a random image everytime digiKam/showfoto is launched?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18430"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18430" class="active">How about make digikam /</a></h3>    <div class="submitted">Submitted by ZeD (not verified) on Sun, 2009-04-12 06:08.</div>
    <div class="content">
     <p>How about make digikam / showfoto actually to do not need a splashscreen?<br>
damn, I can think digikam need to load / update the database, but showfoto it's a damned "little" app thinked just for light editing!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18431"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18431" class="active">List of selected pictures formated with logo and banner</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2009-04-12 09:30.</div>
    <div class="content">
     <p>

<a href="http://www.flickr.com/photos/digikam/3434172108/" title="digikam-bird by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3114/3434172108_e416415c41_m.jpg" width="240" height="147" alt="digikam-bird"></a>

<a href="http://www.flickr.com/photos/digikam/3433377117/" title="digikam-desert-tree by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3301/3433377117_2fd6842550_m.jpg" width="240" height="147" alt="digikam-desert-tree"></a>

<a href="http://www.flickr.com/photos/26732399@N05/3434239280/" title="fish-splash-digikam von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3361/3434239280_b75ae4f1c4_o.png" width="240" height="147" alt="fish-splash-digikam"></a>

</p><p>

<a href="http://www.flickr.com/photos/digikam/3434121758/" title="showfoto-stairs by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3352/3434121758_2f5f1fdea2_m.jpg" width="240" height="147" alt="showfoto-stairs"></a>

<a href="http://www.flickr.com/photos/digikam/3433347057/" title="showfoto-sunrize by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3363/3433347057_fb792fd38a_m.jpg" width="240" height="147" alt="showfoto-sunrize"></a>

<a href="http://www.flickr.com/photos/26732399@N05/3433432347/" title="fish-splash-showfoto von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3345/3433432347_f3c6087a17_o.png" width="240" height="147" alt="fish-splash-showfoto"></a>

<a href="http://www.flickr.com/photos/26732399@N05/3433495959/" title="bug-splash-showfoto.png von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3557/3433495959_d5bc1ecf89_o.jpg" width="240" height="147" alt="bug-splash-showfoto.png"></a>

</p><p>

I think the fish image fits well for both application logos, so I'm not sure what to use here. - Andi

</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18432"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18432" class="active">I think I would go with the</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sun, 2009-04-12 11:42.</div>
    <div class="content">
     <p>I think I would go with the peacock for digiKam and the bug for showFoto.</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18433"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18433" class="active">In my opinion the peacock</a></h3>    <div class="submitted">Submitted by Gandalf (not verified) on Mon, 2009-04-13 18:27.</div>
    <div class="content">
     <p>In my opinion the peacock looks great for digikam, and the fish for showfoto.</p>
<p>Gandalf</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18437"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18437" class="active">I think the Peacock</a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Wed, 2009-04-15 20:08.</div>
    <div class="content">
     <p>I think the Peacock (showfoto) and the staircase (digikam).  They are all pretty nice. Geoff</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18438"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18438" class="active">As none of my pictures</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2009-04-15 20:32.</div>
    <div class="content">
     <p>As none of my pictures reached the next level I would choose the fish and the peacock.</p>
<p>Regards,<br>
Sebastian</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18436"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18436" class="active">Proposal</a></h3>    <div class="submitted">Submitted by <a href="http://www.shodokan.ch" rel="nofollow">Hendric</a> (not verified) on Tue, 2009-04-14 15:31.</div>
    <div class="content">
     <p>I might suggest that for the next release you ask people to submit *pairs* of somehow related photos, one for digikam and one for showphoto. This would enhance the family-look of both applications.</p>
<p>Kind regards!<br>
Hendric</p>
<p>PS: Captcha is still almost unreadable and a real PITA! (Trial n° 8!)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18439"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18439" class="active">Final Splash selection...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2009-04-16 10:59.</div>
    <div class="content">
     <p>See below Final selection for digiKam/Showfoto 0.11.0</p>

<a href="http://www.flickr.com/photos/digikam/3441120996/" title="digikam-kde4-startup by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3328/3441120996_920428c211_m.jpg" width="240" height="192" alt="digikam-kde4-startup"></a>

<a href="http://www.flickr.com/photos/digikam/3441121168/" title="showfoto-kde4-startup by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3639/3441121168_e0df8fa9a0_m.jpg" width="240" height="192" alt="showfoto-kde4-startup"></a>

<p>By the same way, 2 others photo have been taken from the candidates list to splash the last 0.9.6 digiKam/Showfoto release for KDE3. This will be an translations package update, nothing more...</p>

<a href="http://www.flickr.com/photos/digikam/3441036456/" title="digikam-splash by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3404/3441036456_dd556022d0_m.jpg" width="240" height="157" alt="digikam-splash"></a>

<a href="http://www.flickr.com/photos/digikam/3441036458/" title="showfoto-splash by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3368/3441036458_1bbe16d0a9_m.jpg" width="240" height="157" alt="showfoto-splash"></a>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18442"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/438#comment-18442" class="active">great software wery well</a></h3>    <div class="submitted">Submitted by <a href="http://www.sinemafilmiizleme.com" rel="nofollow">sinema filmi izleme</a> (not verified) on Sat, 2009-04-25 04:03.</div>
    <div class="content">
     <p>great software wery well done. I enjoyed the pics.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
