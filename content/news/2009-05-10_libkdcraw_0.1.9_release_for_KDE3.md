---
date: "2009-05-10T16:47:00Z"
title: "libkdcraw 0.1.9 release for KDE3"
author: "digiKam"
description: "libkdcraw 0.1.9 for KDE3 has been just released. The tarball can be downloaded from SourceForge at this url. This release include last version of LibRaw"
category: "news"
aliases: "/node/447"

---

<a href="http://www.flickr.com/photos/digikam/3518176997/" title="digikam0.9.6 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3404/3518176997_7f7a69b6db.jpg" width="500" height="296" alt="digikam0.9.6"></a>

<p>libkdcraw 0.1.9 for KDE3 has been just released. The tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=230894">at this url</a>.</p>

<p>This release include last version of <a href="http://www.libraw.org">LibRaw 0.7.2 Raw decoder</a>. For more informations about digiKam playing with LibRaw , take a look on <a href="http://www.digikam.org/node/372">this previous blog entry</a></p>

<p>
Added Leica RWL raw format in list of supported files.<br>                  
Updated to LibRaw 0.7.2 :<br>                                               
 - More accurate types conversion in libraw_datastream.h to make compilers happy.<br>
 - New postprocessing parameter auto_bright_thr: set portion of clipped pixels for<br> 
   auto brightening code (instead of dcraw-derived hardcoded 1%).<br>                  
 - '-U' option for dcraw_emu sample sets auto_bright_thr parameter.<br>                
Updated to LibRaw 0.7.1 :<br>                                                          
 - Fixed broken OpenMP support.<br>                                                    
Updated to LibRaw 0.7.0 :<br>                                                          
 - Black (masked) pixels data extracted from RAW and avaliable in application.<br>     
 - Application can turn off RAW data filtering (black level subtraction,<br>           
   zero pixels removal and raw tone curve).<br>                                        
 - New 'input framework' released. Reading raw data from file and memory buffer supported from scratch.<br>
   LibRaw-using application can implement own data reading functions (e.g. reading from network stream).<br>
 - Fuji SuperCCD: raw data extracted without 45-degree rotation.<br>                                        
 - New sample applications: 4channels and unprocessed_raw.<br>                                              
 - Imported (subsequentally) new dcraw versions from 8.89 to 8.93 (up to date)<br><br>                          

Updated to libraw 0.6.15:<br> 
 - more accurate pentax dSLR support<br>
 - fixes in Kodak 620x/720x identification<br>
 - faster identification procedure for some formats.<br>
 - Fixed bug in custom gamma curve creation<br><br>

Updated to libraw 0.6.14:<br> 
 - user-specified gamma curve.<br>
 - Pentax K2000/Km support.<br>   
 - Changes in Canon sRAW processing (support for 5D2 fw 1.07).<br>
</p>

<p>Have an happy Raw workflow...</p>

<div class="legacy-comments">

</div>
