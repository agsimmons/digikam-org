---
date: "2012-04-02T08:44:00Z"
title: "digiKam Recipes 3.9.33 Released"
author: "Dmitri Popov"
description: "The new version of the digiKam Recipes ebook includes the following new material: Tethered Shooting with digiKam Soft Proofing in digiKam Continue to read"
category: "news"
aliases: "/node/649"

---

<p>The new version of the&nbsp;<a href="http://dmpop.homelinux.com/digikamrecipes/">digiKam Recipes</a>&nbsp;ebook includes the following new material:</p>
<ul>
<li>Tethered Shooting with digiKam</li>
<li>Soft Proofing in digiKam</li>
</ul>
<p><a href="http://dmpop.homelinux.com/digikamrecipes"><img class="alignnone" title="digikamrecipes" src="https://s3-eu-west-1.amazonaws.com/digikamrecipes/digikamrecipes_b.png" alt="" width="300" height="500"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/04/02/digikam-recipes-3-9-33-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>