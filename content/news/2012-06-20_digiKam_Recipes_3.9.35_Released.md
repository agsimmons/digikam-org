---
date: "2012-06-20T08:56:00Z"
title: "digiKam Recipes 3.9.35 Released"
author: "Dmitri Popov"
description: "A new version of the digiKam Recipes ebook is available for your reading pleasure. Besides a handful of minor tweaks, the new version of the"
category: "news"
aliases: "/node/657"

---

<p>A new version of the digiKam Recipes ebook is available for your reading pleasure.&nbsp;Besides a handful of minor tweaks, the new version of the&nbsp;<a href="http://dmpop.homelinux.com/digikamrecipes/">digiKam Recipes</a>&nbsp;ebook includes the following new material:</p>
<ul>
<li>Export Photos to Wikimedia Commons</li>
<li>digiKam Housekeeping with the Maintenance Tool</li>
</ul>
<p><a href="https://scribblesandsnaps.files.wordpress.com/2012/06/digikamrecipes-3935-fbreader.png"><img class="size-medium wp-image-2676" title="digikamrecipes-3935-fbreader" src="https://scribblesandsnaps.files.wordpress.com/2012/06/digikamrecipes-3935-fbreader.png?w=281" alt="" width="281" height="500"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/06/20/digikam-recipes-3-9-35-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>