---
date: "2009-02-16T09:41:00Z"
title: "Kipi-plugins 0.2.0-rc2 for KDE4 released"
author: "digiKam"
description: "The last KDE4 release candidate of digiKam plugins box is out. With this new release, Face tool is now able to import pictures from FaceBook"
category: "news"
aliases: "/node/426"

---

<p>The last KDE4 release candidate of digiKam plugins box is out.</p>

<a href="http://www.flickr.com/photos/digikam/3231017887/" title="facebookimporttool-kde4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3079/3231017887_9e55a98580.jpg" width="500" height="313" alt="facebookimporttool-kde4"></a>

<p>With this new release, <b>Face tool</b> is now able to import pictures from <a href="http://www.facebook.com">FaceBook web service</a> to your computer.</p>

<p>Since rc1, a new plugin named <b>Smug</b> is dedicated to export/import images to/from <a href="http://www.smugmug.com">SmugMug web service</a>.</p>

<p>A plugin is available to batch convert RAW camera images to Adobe DNG container (see <a href="http://en.wikipedia.org/wiki/Digital_Negative_(file_format)">this Wikipedia entry</a> for details). To be able to compile this plugin, you need last libkdcraw from svn trunk (will be released with KDE 4.2). To extract libraries source code from svn, look at <a href="http://www.digikam.org/download?q=download/svn#checkout-kde4">this page</a> for details.</p>

<p>Kipi-plugins is compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>General</b>        : Plugins now use metadata settings shared with kipi host application.<br>
<b>General</b>        : All import/export plugins now have keyboard shortcuts.<br><br>

<b>JPEGLossLess</b>   : XMP metadata support.<br><br>

<b>TimeAdjust</b>     : XMP metadata support.<br><br>

<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br><br>

<b>SendImages</b>     : Complete re-write of emailing tool.<br><br>

<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br><br>

<b>RAWConverter</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br><br>

<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>SlideShow</b>      : Normal effects are back.<br>
<b>SlideShow</b>      : New effect "Cubism".<br>
<b>SlideShow</b>      : Added support for RAW images.<br>
<b>SlideShow</b>      : New effect "Mosaic".<br><br>

<b>RemoveRedEyes</b>  : New plugin to remove red eyes in batch. Tool is based on OpenCV library.<br><br>

<b>Calendar</b>       : Ported to QT4/KDE4.<br>
<b>Calendar</b>       : Support RAW images.<br><br>

<b>AcquireImages</b>  : Under Windows, TWAIN interface is used to scan image using flat scanner.<br><br>

<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br>
<b>DNGConverter</b>   : plugin is now available as a stand alone application.<br><br>

<b>PrintWizard</b>    : Ported to QT4/KDE4.<br>
<b>PrintWizard</b>    : Added a new option to skip cropping, the image is scaled automatically.<br><br>

<b>RemoveRedEyes</b>  : Added an option to preview the correction of the currently selected image.<br><br>

<b>Facebook</b>       : New plugin to export images to a remote Facebook web service (http://www.facebook.com).<br>
<b>Facebook</b>       : Facebook plugin now support also import (download) of photos.<br><br>

<b>FlickrExport</b>   : Add support of Photosets.<br><br>

<b>Smug</b>           : New plugin to export images to a remote SmugMug web service.(http://www.smugmug.com).<br>
<b>Smug</b>           : Add support for Album Templates.<br>
<b>Smug</b>           : Import (download) of images from SmugMug web service.<br><br>


001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digiKam crashs after closing.<br>
013 ==&gt; 172910 : GalleryExport      : Gallery crash (letting crash digiKam) with valid data and url.<br>
014 ==&gt; 161855 : GalleryExport      : Remote Gallery Sync loses password.<br>
015 ==&gt; 154752 : GalleryExport      : Export to Gallery2 seems to fail, but in fact works.<br>
016 ==&gt; 157285 : SlideShow          : digiKam advanced slideshow not working with Canon RAWs.<br>
017 ==&gt; 175316 : GPSSync            : digiKam crashes when trying to enter gps data.<br>
018 ==&gt; 169637 : MetadataEdit       : EXIF Data Editing Error.<br>
019 ==&gt; 174954 : HTMLExport         : Support for remote urls.<br>
020 ==&gt; 176640 : MetadataEdit       : Wrong reference to website.<br>
021 ==&gt; 176749 : TimeAdjust         : Incorrect time/date setting with digiKam.<br>
022 ==&gt; 165370 : GPSSync            : crash when saving geocoordinates.<br>
023 ==&gt; 165486 : GPSSync            : Next and Previous photo button for editing geolocalization coordinates.<br>
024 ==&gt; 175296 : GPSSync            : Geolocalization a handy gui to manage all about geotagging.<br>
025 ==&gt; 152520 : MetadataEdit       : Unified entry of metadata.<br>
026 ==&gt; 135320 : Calendar           : Calendar year do not match locale.<br>
027 ==&gt; 116970 : Calendar           : New calendar generator plugin features.<br>
028 ==&gt; 177999 : General            : Kipi-plugins Shortcuts are not listed in the shortcuts editor from Kipi host application.<br>
029 ==&gt; 160236 : JPEGLossLess       : Image Magick convert operations return with errors.<br>
030 ==&gt; 144183 : RawConverter       : Path in RAW-converter broken with the letter "ß"<br>
031 ==&gt; 178495 : MetadataEdit       : Problem with metadata editor.<br>
032 ==&gt; 175219 : PicasaWebExport    : Album creating wrong date in picasaweb.<br>
033 ==&gt; 175222 : PicasaWebExport    : Unable to create non listed album.<br>
034 ==&gt; 166672 : Facebook           : Export to Facebook.<br>
035 ==&gt; 129623 : FlickrExport       : Option to upload to specific photoset.<br>
036 ==&gt; 161775 : FlickrExport       : Photoset support.<br>
037 ==&gt; 179439 : JPEGLossLess       : Rotation or flip do not update internal thumbnail.<br>
038 ==&gt; 180245 : TimeAdjust         : Timeadjust is not building on mac os x leopard.<br>
039 ==&gt; 180656 : PicasaWebExport    : Wrong file selection filter in export picasaweb.<br>
040 ==&gt; 175928 : GalleryExport      : digiKam crashing when trying to export to gallery2 web service.<br>
041 ==&gt; 181694 : MetadataEdit       : Edit caption should present current caption for easier modification.<br>
042 ==&gt; 181973 : RAWConverter       : Raw batch converter loses exif information.<br>
043 ==&gt; 181334 : IpodExort          : The artwork API in libgpod-0.7.0 has changed slightly, IpodExport plugin needs updated.<br>
044 ==&gt; 181853 : MetadataEdit       : Entering new xmp meta data removes old one from the image file.<br>
045 ==&gt; 133649 : Print Wizard       : Scale images instead of cropping.<br> 
046 ==&gt; 182838 : Facebook           : Change API authentication to Desktop application type.<br>

<div class="legacy-comments">

  <a id="comment-18242"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18242" class="active">Ubuntu packages</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Mon, 2009-02-16 10:46.</div>
    <div class="content">
     <p>Ubuntu 8.10 (Intrepid) packages are available in digikam-experimental repository (but you need kubuntu-experimental PPA also as we depend on KDE 4.2):</p>
<p>https://launchpad.net/~digikam-experimental/+archive/ppa</p>
<p>You need to add this to your /etc/apt/sources.list:<br>
deb http://ppa.launchpad.net/kubuntu-experimental/ppa/ubuntu intrepid main<br>
deb http://ppa.launchpad.net/digikam-experimental/ppa/ubuntu intrepid main</p>
<p>Packages for Jaunty will be available soon (have to be moved from digikam-experimental to main repository).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18253"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18253" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Mon, 2009-02-16 22:19.</div>
    <div class="content">
     <p>Really thanks for yours repository and Digikam - it's working great with KDE 4.2 on Kubuntu 8.10.<br>
Digikam for KDE4 was one of the most needed applications for me when I switched to KDE 4.x and compared to other KDE4 applications it couldn't be said that it's feature incomplete as most KDE4 applications are.<br>
It's been really worth waiting for Digikam 0.10.<br>
From the camera/imaging apps I know Digikam is the best in it's class - I mean both commercial and FLOSS apps.</p>
<p>Great work and once more - BIG THANKS.</p>
<p>( OT: This captcha it really annoing ;-) )</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18258"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18258" class="active">I am a photographer who</a></h3>    <div class="submitted">Submitted by Jon Door (not verified) on Tue, 2009-02-17 07:21.</div>
    <div class="content">
     <p>I am a photographer who primarily works in lightroom 2, and have been a long time gnome user on my other systems. Now I am setting up a freebsd netbook for easier travel and digikam alone has switched me to KDE 4. I am looking forward to testing out these RCs on freebsd in the next few weeks. The smugmug plugin is also a very welcomed feature for me. Thank You.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18269"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18269" class="active">Welcome in the KDE 4</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Wed, 2009-02-18 21:52.</div>
    <div class="content">
     <p>Welcome in the KDE 4 wonderland!</p>
<p>Glad to hear that SmugMug plugin got another user - I wrote it primarily to solve my personal needs, but it is great to see that it is useful to others.<br>
If you have any ideas/whishes for it, mail me or open bug in bugs.kde.org.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18270"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18270" class="active">facebook plugin gone!</a></h3>    <div class="submitted">Submitted by Anoosh (not verified) on Fri, 2009-02-20 10:48.</div>
    <div class="content">
     <p>Dears, </p>
<p>please see this snapshot i've taken from recently updated digiKam. as you see, the export to facebook option is gone!!!</p>
<p>http://i40.tinypic.com/2mrv67t.png</p>
<p>what should i do ? how do i reverse back to previous version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18276"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18276" class="active">yeah, same problem here</a></h3>    <div class="submitted">Submitted by zdenek (not verified) on Sat, 2009-02-21 10:35.</div>
    <div class="content">
     <p>yeah, same problem here</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18277"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18277" class="active">Investiguations under progress...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-02-21 10:47.</div>
    <div class="content">
     <p>Look in <a href="https://bugs.kde.org/show_bug.cgi?id=185010">this report</a>. Andi start to investigate about this problem...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18280"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18280" class="active">Rc2 crashes after using DNGConverter</a></h3>    <div class="submitted">Submitted by Vasily (not verified) on Sun, 2009-02-22 22:27.</div>
    <div class="content">
     <p>Installed on Ubuntu 8.10, thanks for the repositories! If standalone DNGConverter is used before Digicam, the program crashes on startup with signal 11 (SIGSEGV). After reboot DigiKam starts fine if DNGConverter is not used before it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18281"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18281" class="active">not reproducible.</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2009-02-22 23:23.</div>
    <div class="content">
     <p>Starting stand alone dng converter, converting some RAW files, and exit. Now, starting digiKam, and all work fine. Sound like a package problem on your computer, or a problem not relevant of dng converter.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18283"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18283" class="active">it'a a good news</a></h3>    <div class="submitted">Submitted by Vasily (not verified) on Mon, 2009-02-23 11:24.</div>
    <div class="content">
     <p>It's highly probable that it's a package problem since repositories are experimental. To install the program, I've added 2 repos mentioned above and installed through Synaptic. Though I do not know how to fix it, I hope that when Jaunty arrives after couple of months, everything will work fine.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18294"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/426#comment-18294" class="active">MPEG slideshow function</a></h3>    <div class="submitted">Submitted by The Liquidator (not verified) on Mon, 2009-03-02 17:53.</div>
    <div class="content">
     <p>Hello</p>
<p>I'm running Kubuntu 8.10 with KDE4.2, but with digikam 0.94 at the moment as, last time I tried Digikam for KDE4, kipi plugins had no mpeg slideshow function.</p>
<p>Is this on the horizon or am I in for along wait please?</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
