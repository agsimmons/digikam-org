---
date: "2010-11-23T09:37:00Z"
title: "kipi-plugins 1.6.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.6.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/551"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.6.0 !</p>

<a href="http://www.flickr.com/photos/digikam/5199196479/" title="101122-0002 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5206/5199196479_d297239630.jpg" width="491" height="500" alt="101122-0002"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<h5>NEW FEATURES:</h5><br>

<b>General</b> : Fix compilation under Mac OS-X through <a href="http://www.macports.org">MacPorts project</a>.<br>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; RedEyesRemoval  : 236385 : digiKam and Gwenview crash on red eye removing.<br>
002 ==&gt; PrintWizzard    : 254103 : "previous photo" and "next page" in the same context.<br>
003 ==&gt; DNGConverter    : 254205 : Compiling fails on SPARC platform because "qDNGBigEndian" macro is not defined.<br>
004 ==&gt; PicasaExport    : 232896 : Application crashed after restarting an upload to picassa with previous change of selected photos.<br>
005 ==&gt; DNGConverter    : 212125 : DNG files do not have image contents.<br>
006 ==&gt; DNGConverter    : 255137 : Some exifdata lost when converting CR2.<br>
007 ==&gt; PicasaWebExport : 256303 : Quotation marks in caption appear as " in picasaweb.<br>
008 ==&gt; PicasaWebExport : 244706 : PicasawebExport special symbol kde gui freeze.<br>
009 ==&gt; PicasaWebExport : 243877 : Picasaweb plugin crashes when active video upload removed.<br>
010 ==&gt; PicasaWebExport : 240382 : Picasaweb: pressing of "-" while photo uploading causes crash.<br>
011 ==&gt; MetadataEdit    : 256471 : Add spell checking to text areas.<br>
012 ==&gt; FlickrExport    : 248284 : After upload finishes Cancel button should turn into Close.<br>
013 ==&gt; FacebookExport  : 251860 : [patch] Cancel button should change to close once photo upload is complete.<br>
014 ==&gt; DNGConverter    : 256177 : Strange picture coloring and missing meta data when creating from CR2 file.<br>
015 ==&gt; SmungExport     : 253864 : digiKam crash when removing file from export to smugmug list.<br>


<div class="legacy-comments">

  <a id="comment-19658"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/551#comment-19658" class="active">build problems with -Wl,--as-needed -Wl,--no-copy-dt-needed-e...</a></h3>    <div class="submitted">Submitted by arekm (not verified) on Sun, 2010-12-05 11:57.</div>
    <div class="content">
     <p>Can't build it. Looks like -lgphoto2_port is missing at linking stage.<br>
Can't find where GPHOTO2_LIBRARIES is set anyway :/</p>
<p>Linking CXX executable digikam<br>
cd /home/users/arekm/rpm/BUILD/digikam-1.6.0/build/digikam &amp;&amp; /usr/bin/cmake -E cmake_link_script CMakeFiles/digikam.dir/link.txt --verbose=1<br>
/usr/bin/x86_64-pld-linux-g++   -O2 -fno-strict-aliasing -fwrapv -march=x86-64 -gdwarf-3 -g2   -Wnon-virtual-dtor -Wno-long-long -ansi -Wundef -Wcast-align -Wchar-subscripts -Wall -W -Wpointer-arith -Wformat-security -fno-exceptions -DQT_NO_EXCEPTIONS -fno-check-new -fno-common -Woverloaded-virtual -fno-threadsafe-statics -fvisibility=hidden -fvisibility-inlines-hidden -O2 -DNDEBUG -DQT_NO_DEBUG  -Wl,--enable-new-dtags -Wl,--as-needed -Wl,--no-copy-dt-needed-entries -Wl,-z,relro -Wl,-z,combreloc  CMakeFiles/digikam.dir/digikam_automoc.o CMakeFiles/digikam.dir/__/utilities/imageeditor/editor/imagewindow.o CMakeFiles/digikam.dir/__/libs/dialogs/deletedialog.o CMakeFiles/digikam.dir/__/libs/imageproperties/imagepropertiessidebarcamgui.o CMakeFiles/digikam.dir/__/libs/imageproperties/imagepropertiessidebardb.o CMakeFiles/digikam.dir/__/libs/imageproperties/cameraitempropertiestab.o CMakeFiles/digikam.dir/__/libs/imageproperties/imagedescedittab.o CMakeFiles/digikam.dir/__/libs/imageproperties/captionedit.o CMakeFiles/digikam.dir/__/utilities/setup/setupicc.o CMakeFiles/digikam.dir/__/utilities/setup/setupcollections.o CMakeFiles/digikam.dir/__/utilities/setup/setupcollectionview.o CMakeFiles/digikam.dir/__/utilities/setup/setupcategory.o CMakeFiles/digikam.dir/__/utilities/setup/setupalbumview.o CMakeFiles/digikam.dir/__/utilities/setup/setup.o CMakeFiles/digikam.dir/__/utilities/setup/setuptooltip.o CMakeFiles/digikam.dir/__/utilities/setup/setuptemplate.o CMakeFiles/digikam.dir/__/utilities/setup/setupmime.o CMakeFiles/digikam.dir/__/utilities/setup/setupslideshow.o CMakeFiles/digikam.dir/__/utilities/setup/setupmetadata.o CMakeFiles/digikam.dir/__/utilities/setup/setupeditor.o CMakeFiles/digikam.dir/__/utilities/setup/setupmisc.o CMakeFiles/digikam.dir/__/utilities/setup/setuplighttable.o CMakeFiles/digikam.dir/__/utilities/setup/setupiofiles.o CMakeFiles/digikam.dir/__/utilities/setup/setupplugins.o CMakeFiles/digikam.dir/__/utilities/setup/setupdcraw.o CMakeFiles/digikam.dir/__/utilities/setup/setupcamera.o CMakeFiles/digikam.dir/__/utilities/setup/cameraselection.o CMakeFiles/digikam.dir/__/utilities/setup/cameralist.o CMakeFiles/digikam.dir/__/utilities/setup/cameratype.o CMakeFiles/digikam.dir/__/utilities/setup/setupdatabase.o CMakeFiles/digikam.dir/__/utilities/lighttable/lighttablepreview.o CMakeFiles/digikam.dir/__/utilities/lighttable/lighttableview.o CMakeFiles/digikam.dir/__/utilities/lighttable/lighttablebar.o CMakeFiles/digikam.dir/__/utilities/lighttable/lighttablewindow.o CMakeFiles/digikam.dir/__/utilities/batch/imageinfojob.o CMakeFiles/digikam.dir/__/utilities/batch/imageinfoalbumsjob.o CMakeFiles/digikam.dir/__/utilities/batch/batchthumbsgenerator.o CMakeFiles/digikam.dir/__/utilities/batch/batchalbumssyncmetadata.o CMakeFiles/digikam.dir/__/utilities/batch/batchsyncmetadata.o CMakeFiles/digikam.dir/__/utilities/batch/fingerprintsgenerator.o CMakeFiles/digikam.dir/__/utilities/kipiiface/kipiimageinfo.o CMakeFiles/digikam.dir/__/utilities/kipiiface/kipiimagecollection.o CMakeFiles/digikam.dir/__/utilities/kipiiface/kipiimagecollectionselector.o CMakeFiles/digikam.dir/__/utilities/kipiiface/kipiuploadwidget.o CMakeFiles/digikam.dir/__/utilities/kipiiface/kipiinterface.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchwindow.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchview.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchgroup.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchfieldgroup.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchfields.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchutilities.o CMakeFiles/digikam.dir/__/utilities/searchwindow/ratingsearchutilities.o CMakeFiles/digikam.dir/__/utilities/searchwindow/choicesearchutilities.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchfolderview.o CMakeFiles/digikam.dir/__/utilities/searchwindow/searchtabheader.o CMakeFiles/digikam.dir/__/utilities/fuzzysearch/fuzzysearchview.o CMakeFiles/digikam.dir/__/utilities/fuzzysearch/findduplicatesview.o CMakeFiles/digikam.dir/__/utilities/fuzzysearch/findduplicatesalbumitem.o CMakeFiles/digikam.dir/__/utilities/fuzzysearch/sketchwidget.o CMakeFiles/digikam.dir/__/utilities/gpssearch/gpssearchview.o CMakeFiles/digikam.dir/__/utilities/gpssearch/gpssearchwidget.o CMakeFiles/digikam.dir/__/utilities/timeline/timelinewidget.o CMakeFiles/digikam.dir/__/libs/models/imagealbummodel.o CMakeFiles/digikam.dir/__/libs/models/imagealbumfiltermodel.o CMakeFiles/digikam.dir/__/libs/models/abstractalbummodel.o CMakeFiles/digikam.dir/__/libs/models/albummodel.o CMakeFiles/digikam.dir/__/libs/models/albumfiltermodel.o CMakeFiles/digikam.dir/__/libs/models/albummodeldragdrophandler.o CMakeFiles/digikam.dir/addtagslineedit.o CMakeFiles/digikam.dir/album.o CMakeFiles/digikam.dir/albumdragdrop.o CMakeFiles/digikam.dir/albumhistory.o CMakeFiles/digikam.dir/albumiconviewfilter.o CMakeFiles/digikam.dir/albummanager.o CMakeFiles/digikam.dir/albummodificationhelper.o CMakeFiles/digikam.dir/albumpropsedit.o CMakeFiles/digikam.dir/albumselectcombobox.o CMakeFiles/digikam.dir/albumselectdialog.o CMakeFiles/digikam.dir/albumselectiontreeview.o CMakeFiles/digikam.dir/albumselectwidget.o CMakeFiles/digikam.dir/albumsettings.o CMakeFiles/digikam.dir/albumthumbnailloader.o CMakeFiles/digikam.dir/albumtreeview.o CMakeFiles/digikam.dir/albumwidgetstack.o CMakeFiles/digikam.dir/contextmenuhelper.o CMakeFiles/digikam.dir/datefolderview.o CMakeFiles/digikam.dir/dbstatdlg.o CMakeFiles/digikam.dir/ddragobjects.o CMakeFiles/digikam.dir/digikamapp.o CMakeFiles/digikam.dir/digikamimageview.o CMakeFiles/digikam.dir/digikamimageview_p.o CMakeFiles/digikam.dir/digikamimagedelegate.o CMakeFiles/digikam.dir/digikammodelcollection.o CMakeFiles/digikam.dir/digikamview.o CMakeFiles/digikam.dir/dio.o CMakeFiles/digikam.dir/editablesearchtreeview.o CMakeFiles/digikam.dir/icongroupitem.o CMakeFiles/digikam.dir/iconitem.o CMakeFiles/digikam.dir/iconview.o CMakeFiles/digikam.dir/imageattributeswatch.o CMakeFiles/digikam.dir/imagecategorizedview.o CMakeFiles/digikam.dir/imagecategorydrawer.o CMakeFiles/digikam.dir/imagedelegate.o CMakeFiles/digikam.dir/imagedragdrop.o CMakeFiles/digikam.dir/imagepreviewbar.o CMakeFiles/digikam.dir/imagepreviewview.o CMakeFiles/digikam.dir/imageratingoverlay.o CMakeFiles/digikam.dir/imagerotationoverlay.o CMakeFiles/digikam.dir/imagethumbnailbar.o CMakeFiles/digikam.dir/imagethumbnaildelegate.o CMakeFiles/digikam.dir/imageselectionoverlay.o CMakeFiles/digikam.dir/imageviewutilities.o CMakeFiles/digikam.dir/kdateedit.o CMakeFiles/digikam.dir/kdatepickerpopup.o CMakeFiles/digikam.dir/kdatetimeedit.o CMakeFiles/digikam.dir/leftsidebarwidgets.o CMakeFiles/digikam.dir/mediaplayerview.o CMakeFiles/digikam.dir/metadatahub.o CMakeFiles/digikam.dir/metadatamanager.o CMakeFiles/digikam.dir/mimefilter.o CMakeFiles/digikam.dir/monthwidget.o CMakeFiles/digikam.dir/ratingfilter.o CMakeFiles/digikam.dir/ratingpopupmenu.o CMakeFiles/digikam.dir/scancontroller.o CMakeFiles/digikam.dir/sidebarwidget.o CMakeFiles/digikam.dir/syncjob.o CMakeFiles/digikam.dir/tagcheckview.o CMakeFiles/digikam.dir/tagdragdrop.o CMakeFiles/digikam.dir/tageditdlg.o CMakeFiles/digikam.dir/tagfiltersidebarwidget.o CMakeFiles/digikam.dir/tagfolderview.o CMakeFiles/digikam.dir/tagmodificationhelper.o CMakeFiles/digikam.dir/tagspopupmenu.o CMakeFiles/digikam.dir/tooltipfiller.o CMakeFiles/digikam.dir/searchmodificationhelper.o CMakeFiles/digikam.dir/welcomepageview.o CMakeFiles/digikam.dir/databaseguierrorhandler.o CMakeFiles/digikam.dir/__/utilities/firstrun/assistantdlg.o CMakeFiles/digikam.dir/__/utilities/firstrun/assistantdlgpage.o CMakeFiles/digikam.dir/__/utilities/firstrun/welcomepage.o CMakeFiles/digikam.dir/__/utilities/firstrun/collectionpage.o CMakeFiles/digikam.dir/__/utilities/firstrun/rawpage.o CMakeFiles/digikam.dir/__/utilities/firstrun/tooltipspage.o CMakeFiles/digikam.dir/__/utilities/firstrun/previewpage.o CMakeFiles/digikam.dir/__/utilities/firstrun/openfilepage.o CMakeFiles/digikam.dir/__/utilities/firstrun/metadatapage.o CMakeFiles/digikam.dir/__/utilities/firstrun/startscanpage.o CMakeFiles/digikam.dir/__/libs/template/templatemanager.o CMakeFiles/digikam.dir/__/libs/template/templatelist.o CMakeFiles/digikam.dir/__/libs/template/templateselector.o CMakeFiles/digikam.dir/__/libs/template/templateviewer.o CMakeFiles/digikam.dir/__/libs/template/templatepanel.o CMakeFiles/digikam.dir/__/libs/template/subjectedit.o CMakeFiles/digikam.dir/__/libs/widgets/common/databasewidget.o CMakeFiles/digikam.dir/__/libs/dialogs/migrationdlg.o CMakeFiles/digikam.dir/main.o CMakeFiles/digikam.dir/digikamadaptor.o  -o digikam -rdynamic -L/home/users/arekm/rpm/BUILD/digikam-1.6.0/build/lib /usr/lib64/libkparts.so.4.5.0 -lphonon /usr/lib64/libkhtml.so.5.5.0 /usr/lib64/libkutils.so.4.5.0 /usr/lib64/libknotifyconfig.so.4.5.0 /usr/lib64/libsolid.so.4.5.0 -lkipi -lkexiv2 -lkdcraw /usr/lib64/libQtSql.so /usr/lib64/libQt3Support.so -llcms -L/usr/lib64 -lgphoto2_port -L/usr/lib64 -lgphoto2 -lgphoto2_port -lm ../lib/libdigikamcore.so.1.0.0 ../lib/libdigikamdatabase.so.1.0.0 ../lib/libadvancedrename.a ../lib/libqueuemanager.a ../lib/libcameragui.a /usr/lib64/libkabc.so.4.5.0 -lmarblewidget -lkexiv2 -lkdcraw ../lib/libdigikamdatabase.so.1.0.0 ../lib/libdigikamcore.so.1.0.0 /usr/lib64/libkhtml.so.5.5.0 /usr/lib64/libkjs.so.4.5.0 /usr/lib64/libkutils.so.4.5.0 /usr/lib64/libkemoticons.so.4.5.0 /usr/lib64/libkidletime.so.4.5.0 /usr/lib64/libkcmutils.so.4.5.0 /usr/lib64/libkprintutils.so.4.5.0 /usr/lib64/libkparts.so.4.5.0 /usr/lib64/libknotifyconfig.so.4.5.0 /usr/lib64/libkresources.so.4.5.0 /usr/lib64/libkio.so.5.5.0 /usr/lib64/libsolid.so.4.5.0 /usr/lib64/libnepomuk.so.4.5.0 /usr/lib64/libkdeui.so.5.5.0 /usr/lib64/libQtGui.so /usr/lib64/libQtSvg.so /usr/lib64/libkdecore.so.5.5.0 /usr/lib64/libQtCore.so -lpthread /usr/lib64/libQtDBus.so -lsoprano /usr/lib64/libQtNetwork.so /usr/lib64/libQtXml.so -Wl,-rpath,/home/users/arekm/rpm/BUILD/digikam-1.6.0/build/lib:<br>
/usr/bin/ld: /usr/lib64/libgphoto2.so: undefined reference to symbol 'gp_port_free'<br>
/usr/bin/ld: note: 'gp_port_free' is defined in DSO /usr/lib64/libgphoto2_port.so so try adding it to the linker command line<br>
/usr/lib64/libgphoto2_port.so: could not read symbols: Invalid operation<br>
collect2: ld returned 1 exit status<br>
make[2]: *** [digikam/digikam] Error 1</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19661"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/551#comment-19661" class="active">This comment should be at</a></h3>    <div class="submitted">Submitted by arekm (not verified) on Sun, 2010-12-05 18:13.</div>
    <div class="content">
     <p>This comment should be at digikam post of course :/</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
