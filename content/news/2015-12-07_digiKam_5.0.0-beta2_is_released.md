---
date: "2015-12-07T13:08:00Z"
title: "digiKam 5.0.0-beta2 is released"
author: "digiKam"
description: "Dear digiKam fans and users, digiKam team is proud to announce the release of digiKam Software Collection 5.0.0 beta2. This version is the second public"
category: "news"
aliases: "/node/749"

---

<a href="https://www.flickr.com/photos/digikam/23417789741/in/dateposted-public/" title="digiKam5.0.0-beta2"><img src="https://farm1.staticflickr.com/601/23417789741_00aa1aa09e_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta2"></a>

<p>Dear digiKam fans and users,</p>

<p>
digiKam team is proud to announce the release of digiKam Software Collection 5.0.0 beta2. This version is the second public release of the new main digiKam version and is a result of the long development process by the team.
</p>

<p>
This release marks almost complete port of the application to Qt5 and KF5 API. All Qt4/KDE4 code has been removed and many parts have been re-written, reviewed, and tested. Porting to Qt5 required a lot of work, as many important APIs had to be changed or replaced by new ones.
</p>

<p>
In addition to code porting, we introduced several changes and optimizations, especially regarding dependencies on the KDE project. Although digiKam is still a KDE desktop application, it now uses many Qt dependencies instead of KDE dependencies. This  simplifies the porting job on other operating systems, code maintenance, while reducing sensitivity of API changes from KDE project.
</p>

<p>
The DNG convert tool has been migrated into digiKam Batch Queue Manager. This will simplify the user workflow when performing DNG conversions. Metadata Editor, Geolocator, and tool to import from scanner are now available in the Image editor, Showfoto, and Light table.
</p>

<a href="https://www.flickr.com/photos/digikam/22933260133/in/dateposted-public/" title="digiKam5.0.0-beta2-06"><img src="https://farm1.staticflickr.com/571/22933260133_921f608e18_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta2-06"></a>

<p>
As a part of code optimization, an important task was done by <a href="https://plus.google.com/u/0/+MohamedAnwer">Mohamed Anwer</a>, a long-term contributor to digiKam. He worked last summer during “Google Summer of Code” to remove all digiKam KIO-slaves used to query the database. Instead, a robust multi-core/multi-threaded implementation is now used. digiKam had introduced KIO-slaves when the first SQLite database support was implemented. The goal was to perform database-related actions (processing searches, listing albums, tags and dates, storing detected faces, etc) in the background without rendering the graphical interface unresponsive. As the first version of SQLite did not support multi-threading, use of KIO technology was the best solution. Although this worked fine, it became evident that using separate processes was very sensitive with each systems update and was not portable to non-Linux systems. The new solution is also faster because it does not use data serialization shared between digiKam and KIO-Slaves.
</p>

<p>
Since removing KIO-Slaves was faster than planned, Mohamed also implemented a new important feature: the virtual digiKam trash folder.<br> Previously, digiKam used the KDE desktop trash, which couldn’t be ported to Mac OS X and Windows. Also, in case of the KDE desktop trash, deleted items were removed permanently from the computer instead of being moved into the desktop trash -- which was not an acceptable solution. Now, like all commercial photo management software, digiKam uses a hidden sub-directory in each main collection (including removable ones) to store deleted items. The trash is accessible from the album tree view, and user can restore or delete items permanently as they wishe. This is a simple, user-friendly, and portable solution.
</p>

<a href="https://www.flickr.com/photos/digikam/23132813629/in/dateposted-public/" title="digiKam5.0.0-beta2-02"><img src="https://farm1.staticflickr.com/644/23132813629_461e2816fe_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta2-02"></a>

<p>
Last summer, another long-term digiKam contributor, <a href="https://plus.google.com/u/0/+VeaceslavMunteanu">Veaceslav Munteanu</a>, worked to improve the metadata workflow in digiKam, especially for synchronizing photo metadata with the database contents. A typical use case here is to queue all changes to process files when you change metadata in the digiKam interface. The database is patched, but not the metadata. As this last operation can take a while, we delegate pending operations to a new tool named <b>Lazy Synchronization tool</b>, which will do the job when the user decides, or at the end of digiKam session. In the status bar, you can see if pending items need to be processed by the tool.</p>

<a href="https://www.flickr.com/photos/digikam/23475437246/in/dateposted-public/" title="digiKam5.0.0-beta2-03"><img src="https://farm6.staticflickr.com/5795/23475437246_2950a16814_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta2-03"></a>

<p>
in addition to a new settings panel, where the user can tune the Exif/Iptc/Xmp tags to handle database population with key image information, like date, comments, keywords, rating, etc., Veaceslav also worked on another part of metadata management. It’s now possible to order tags to parse while items parsing and specify which tags will be updated in the image using the Lazy Synchronization tool.
</p>

<a href="https://www.flickr.com/photos/digikam/23393065632/in/dateposted-public/" title="digiKam5.0.0-beta2-04"><img src="https://farm6.staticflickr.com/5754/23393065632_73c905252d_z.jpg" width="640" height="637" alt="digiKam5.0.0-beta2-04"></a>

<p>
<a href="”https://plus.google.com/u/0/100157321534338890446”">Shourya Singh Gupta</a> from India worked exclusively on porting export tools to Qt5, factorizing lots of duplicate code everywhere, optimizing the implementations, performing tests, and reviving old plugins which have not had maintainers for a very long time. He helped to quickly have a suitable code working with Qt5. This allowed the team to drop the last Qt4 dependencies.
</p>

<a href="https://www.flickr.com/photos/digikam/23227692830/in/dateposted-public/" title="digiKam5.0.0-beta2-05"><img src="https://farm6.staticflickr.com/5688/23227692830_cb8cbd7810_c.jpg" width="800" height="450" alt="digiKam5.0.0-beta2-05"></a>

<p>
Although kipi-plugins has come a long way, they are not yet complete, as some tools are not yet fully ported due to some dependencies not available in Qt5. A lot of regression tests to asses the quality of the new code are also remaining. But that’s exactly what beta releases are for.
</p>

<p>
Another important part on which the team is working for the next 5.0.0 is MySQL/MariaDB interface.<br> The whole database code has been reviewed, polished, cleaned, and documented. The face recognition database is now integrated into digiKam core and is stored in SQLite or MySQL.
</p>

<p>
The MySQL interface was written 5 years ago by a contributor who had left the team since then. This code was placed in quarantine due to the lack of support and bugs. But thanks to the new MySQL expert on the team, we are now able to restore, fix, and improve this functionality.
</p>

<p>
<a href="https://plus.google.com/u/0/+RichardMortimer">Richard Mortimer</a> has proposed lots of patches to fix and rewrite MySQL database schemas aiming to have a compatible digiKam database with the last MySQL or MariaDB default engine. New database features have been introduced and the database configuration panel has been re-written to ensure safety during database server configuration stage.
</p>

<a href="https://www.flickr.com/photos/digikam/22713177134/in/dateposted-public/" title="digikam5.0.0-firstrun-mysql"><img src="https://farm1.staticflickr.com/590/22713177134_154f7d451d_z.jpg" width="591" height="640" alt="digikam5.0.0-firstrun-mysql"></a>

<p>It’s now possible to setup a MySQL database at first run, instead of using SQLite first and migrating to MySQL later.<br> Two Mysql servers are available:
</p><ul>
<li>Local one to replace local SQLite storage</li>
<li>Remote one to use a shared computer through the network.</li>
</ul>
MySQL offers many advantages for storing digiKam data, especially when collections include more than 100,000 items. With such large collections, SQLite introduces latency which slows down the application. With this new release, you will be able to host MySQL internal server database files at a dedicated place, as with SQLite. Keep in mind that MySQL support is still under development and is not yet fully suitable for production. The database schemas are not yet fully patched, and lots of regression tests are yet to be performed.<p></p>

<p>
Next summer, students will focus on working exclusively on database interface, to improve MySQL support in case of multi-accounts (photo agency use case), or to introduce new database engine support like PostgreSQL for example.
</p>

<p>For furher information, take a look into the list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=5.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">files currently closed</a> in KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-5.0.0-beta2.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes.<b> It’s not currently advised to use it in production.</b></p>

<p>Thanks in advance for your feedback.

</p><p>Happy digiKaming!</p>

<p>Note: All screenshots have been done with <a href="https://wiki.mageia.org/en/Cauldron">Linux Mageia Cauldron</a> (next release 6)</p>

<div class="legacy-comments">

  <a id="comment-21143"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21143" class="active">Nicely done.
Seems like a lot</a></h3>    <div class="submitted">Submitted by Chris (not verified) on Mon, 2015-12-07 13:42.</div>
    <div class="content">
     <p>Nicely done.<br>
Seems like a lot of useful improvement :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21144"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21144" class="active">Awesome guys!</a></h3>    <div class="submitted">Submitted by <a href="http://blog.haifischma.de" rel="nofollow">tuxflo</a> (not verified) on Mon, 2015-12-07 14:19.</div>
    <div class="content">
     <p>Can't wait for the stable version to use it with my Image-Database.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21145"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21145" class="active">Dark Style</a></h3>    <div class="submitted">Submitted by <a href="http://www.damonlynch.net" rel="nofollow">Damon Lynch</a> (not verified) on Mon, 2015-12-07 16:58.</div>
    <div class="content">
     <p>Many congrats to the team for all the hard work done thus far! Is the dark style custom-made for digikam, or was it picked up from the default desktop style? It looks pretty nice.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21146"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21146" class="active">all from Plasma...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2015-12-07 17:30.</div>
    <div class="content">
     <p>Icons and color schemes come from Plasma desktop...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21147"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21147" class="active">Looks great</a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Mon, 2015-12-07 17:46.</div>
    <div class="content">
     <p>Glad to hear of the progress.  Looking great and the "under the hood" stuff is nice.<br>
I'll test on Mac El Cap when you have a version ready for that.<br>
Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21148"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21148" class="active">Operating systems </a></h3>    <div class="submitted">Submitted by Aidan Badder (not verified) on Thu, 2015-12-10 11:14.</div>
    <div class="content">
     <p>Hi, could anyone tell me if the new release will work with Windows 8.1 operating system (and in a virtual environment?)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21149"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21149" class="active">yes, but...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2015-12-10 11:25.</div>
    <div class="content">
     <p>...nothing have been tested yet (MAC/WIN).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21150"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21150" class="active">Nothing to test yet?</a></h3>    <div class="submitted">Submitted by MBB (not verified) on Sat, 2015-12-12 22:57.</div>
    <div class="content">
     <p>Maybe some of the nice people that come here wondering about WIndows versions could be encouraged to test then? Perhaps by asking them in these posts..</p>
<p>Currently there is no 5.0.exe version for windows in the UNstable directory - (it stops at 3.0 rc even though some 4.x have been released) - unless I'm looking at the wrong place?<br>
http://download.kde.org/unstable/digikam/</p>
<p>And as it seems it can it be tested/debugged with Windbg, LibreOffice has a nice installation guide:<br>
https://wiki.documentfoundation.org/How_to_get_a_backtrace_with_WinDbg</p>
<p>Here are the Digikam links I found<br>
https://techbase.kde.org/Projects/KDE_on_Windows/Tools#WinDbg<br>
https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;product=digikam<br>
https://userbase.kde.org/Digikam/Bugs</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21153"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21153" class="active">That is a good suggestion :-)</a></h3>    <div class="submitted">Submitted by Manu (not verified) on Sat, 2015-12-19 10:44.</div>
    <div class="content">
     <p>That is a good suggestion :-)  I'm waiting for the exe.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21156"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21156" class="active">Ps.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2015-12-26 17:46.</div>
    <div class="content">
     <p>you can compare with darktable:<br>
http://www.darktable.org/2015/07/why-dont-you-provide-a-windows-build/</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21151"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21151" class="active">Thanks for this comprehensive</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Mon, 2015-12-14 14:26.</div>
    <div class="content">
     <p>Thanks for this comprehensive update on the Digikam project. I'm glad lots of people are interested in it, and are collaborating, ensuring it is top-notch.</p>
<p>But the happiest of the updates, for me, is the decoupling of many of the libs from kdelibs to qtlibs. That, by itself, would mean that the non-Linux ports will become a reality soon.<br>
And it'll also make more non-KDE users happy.</p>
<p>A big thank you to all of you. Digikam rocks!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21152"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21152" class="active">Can you guys, maybe, share a</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Mon, 2015-12-14 14:30.</div>
    <div class="content">
     <p>Can you guys, maybe, share a write-up on the Lazy Synchronization Tool ?</p>
<p>The biggest pain running Digikam was the enormous amount of I/O it could generate, the memory consumption/leak, and Linux's inefficiency under both these scenarios.</p>
<p>Today, as we speak, I've been able to make a usable setup of Digikam + Linux [1], with the help of Control Groups, but I'd be interested to know more about your approach.</p>
<p>[1] http://www.researchut.com/blog/controlling-apps-cgroups</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21154"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21154" class="active">Nice work!</a></h3>    <div class="submitted">Submitted by Anders (not verified) on Tue, 2015-12-22 10:17.</div>
    <div class="content">
     <p>Great work - as always! </p>
<p>I’m especially looking forward to the improved mySQL support and hope it will let me share the same database through 2-3 clients (Ubuntu and Windows). Without having to have the same absolute path to the collection :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21155"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21155" class="active">Agreed! Excellent work (as</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Tue, 2015-12-22 23:13.</div>
    <div class="content">
     <p>Agreed! Excellent work (as usual) and I also am excited about being able to share a single database across multiple operating systems. I also like the idea of PostgreSQL support : )</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21157"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21157" class="active">MySQL &amp; multi user / OS support</a></h3>    <div class="submitted">Submitted by Mathew (not verified) on Mon, 2015-12-28 09:51.</div>
    <div class="content">
     <p>Being able to catalog photos on multiple computers is the key feature of Digikam for me that separates the application from the rest. Specifying an absolute path to the collection is a show stopper for letting windows clients connect because the absolute path is different.</p>
<p>Either specifying a base path on each client or adding in a client name when connecting could resolve this. Ideally I would like to create users with passwords so that I can restrict access by user. For example the kids can add tags, but not delete photos.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21158"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21158" class="active">ppa:philip5/extra has 5.0.0-beta2</a></h3>    <div class="submitted">Submitted by Mathew (not verified) on Mon, 2015-12-28 12:00.</div>
    <div class="content">
     <p>Philip has published a <a href="https://launchpad.net/~philip5/+archive/ubuntu/extra">PPA for Ubuntu</a>, but only for wily. I'm hopeful that he might publish a PPA for trusty as I'm currently using Linux Mint.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21159"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21159" class="active">It's very unlikely that I</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Mon, 2015-12-28 16:23.</div>
    <div class="content">
     <p>It's very unlikely that I will backport all updates of packages of QT5 and KF5 for Ubuntu trusty that Digikam 5 need as those packages are not that new in that Ubuntu release.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21160"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21160" class="active">QMYSQL driver not loaded</a></h3>    <div class="submitted">Submitted by Mathew (not verified) on Sat, 2016-01-02 03:41.</div>
    <div class="content">
     <p>Thanks for the response. I appreciate the effort to prepare it for trusty, I've installed ubuntu trusty (15.10) and added ppa:philip5/extras.</p>
<p>Unfortunately I've encountered an issue with MySQL support. The following message appears in the terminal window:</p>
<p><code>QSqlDatabase: QMYSQL driver not loaded<br>
QSqlDatabase: available drivers: QSQLITE<br>
digikam.database: Testing DB connection ( "ConnectionTest" ) with these settings:<br>
digikam.database: Database Parameters:<br>
   Type:                 "QMYSQL"<br>
   DB Core Name:         "digikam"<br>
   DB Thumbs Name:       "digikam"<br>
   DB Face Name:         "digikam"<br>
   Connect Options:      ""<br>
   Host Name:            "bluefoot"<br>
   Host port:            3306<br>
   Internal Server:      false<br>
   Internal Server Path: ""<br>
   Username:             "digikam"<br>
   Password:             "XXXXXXX"</code></p>
<p>QSqlDatabasePrivate::removeDatabase: connection 'ConnectionTest' is still in use, all queries will cease to work.</p>
<p>I've searched for <a href="https://duckduckgo.com/?q=QMYSQL+driver+not+loaded&amp;ia=qa">QMYSQL driver not loaded</a>. I found the error mentioned a few times but the results are aimed at a programmer not someone installing the application.</p>
<p>Any thoughts on how to resolve this?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21161"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21161" class="active">Solved: QMYSQL driver not loaded by installing libqt5sql5-mysql</a></h3>    <div class="submitted">Submitted by Mathew (not verified) on Sat, 2016-01-02 03:54.</div>
    <div class="content">
     <p>I was able to solve the <code>QSqlDatabase: QMYSQL driver not loaded</code> error by installing libqt5sql5-mysql using:<br>
<code>sudo apt-get install libqt5sql5-mysql</code></p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21162"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21162" class="active">Importing photos at start is painful</a></h3>    <div class="submitted">Submitted by Mathew (not verified) on Sat, 2016-01-02 08:38.</div>
    <div class="content">
     <p>Importing images when starting is a very painful process. If I hadn't started digikam from the command line and/or looked at the database, I would have assumed that the program had crashed because it has paused at the splash screen for several hours.</p>
<p>The second issue I have encountered is that the program appears not to support My%20Pictures appearing in the directory for the AlbumRoot. After entering the Album in settings, I wasn't able to edit the path. I worked around this by creating a symbolic link to the directory and updating the row in the AlbumRoot directory.</p>
<p>Should this be logged in KDE bugzilla?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21319"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/749#comment-21319" class="active">Planned features</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2016-12-07 08:15.</div>
    <div class="content">
     <p>Great progress and good software. The updated handbook is one of the best news since there are so many functions and options. <a href="https://www.netpal.co.uk/">NetPal</a></p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
