---
date: "2008-05-02T13:07:00Z"
title: "digiKam wins TUX 2008 Readers Choice Award"
author: "digiKam"
description: "digiKam has been awarded again as the TUX 2008 Readers' Choice Award in the category Favorite Digital Photo Management Tool. We are very proud we"
category: "news"
aliases: "/node/314"

---

<img border="0" align="right" id="newlogo" src="http://websvn.kde.org/trunk/extragear/graphics/digikam/data/pics/logo-digikam.png?revision=803308" alt="New digiKam icon designed by Risto SaukonpÃ¤Ã¤">

digiKam has been <a href="http://www.linuxjournal.com/article/10065">awarded again</a> as the TUX 2008 Readers' Choice Award in the category <b>Favorite Digital Photo Management Tool</b>.<p></p>

We are very proud we received this prize, especially because it is a prize for which the users have voted. <p></p>

<a href="http://www.linuxjournal.com">TUX</a> is the a magazine for the new Linux user and is dedicated to promoting and simplifying the use of Linux on the PC.<p></p>

<b>The team thanks all peoples who has voted for digiKam.</b>



<div class="legacy-comments">

</div>