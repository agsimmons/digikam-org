---
date: "2013-05-02T22:26:00Z"
title: "digiKam Software Collection 3.2.0-beta2 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the second beta release of digiKam Software Collection 3.2.0. This version currently under"
category: "news"
aliases: "/node/692"

---

<a href="http://www.flickr.com/photos/digikam/8702166507/" title="aspectratiosearch2 by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8274/8702166507_2269ae46ed_z.jpg" width="640" height="360" alt="aspectratiosearch2"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the second beta release of digiKam Software Collection 3.2.0.
This version currently under development, include a new album interface display mode named list-view. Icon view can be switched to a flat item list, where items can  
be sorted by properties columns as in a simple file manager. Columns can be customized to show file, image, metadata, or digiKam properties. This list-view mode 
is not yet fully completed and need to be review by users.</p>

<p>Another important feature introduced in this release is the capability to filter and search items based on aspect ratio property. This information can be displayed over icon-view and item tool-tip.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.2.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.2.0-beta2.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b> Release plan <a href="http://www.digikam.org/about/releaseplan">can be seen here...</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20532"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/692#comment-20532" class="active">Face Recogintion</a></h3>    <div class="submitted">Submitted by Hermann (not verified) on Mon, 2013-05-06 13:08.</div>
    <div class="content">
     <p>Hi,<br>
Thanks for the good work. But will digikam be able to sort faces to other faces? For Now It can only detect Faces. It doesn't sort them to the faces already named.</p>
<p>Thanks,<br>
Hermann</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20534"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/692#comment-20534" class="active">Face Recognition</a></h3>    <div class="submitted">Submitted by Frattale (not verified) on Tue, 2013-05-07 23:26.</div>
    <div class="content">
     <p>I'm looking forward at that feature too.<br>
They had some issue with the latest GSoC and now it's planned in later release, hopefully 3.3 or 3.4.</p>
<p>Thanks for this invaluable software!</p>
<p>Regards,<br>
Frattale</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
