---
date: "2016-01-18T10:15:00Z"
title: "digiKam Recipes 4.9.9 Released"
author: "Dmitri Popov"
description: "A new release of digiKam Recipes is ready for your reading pleasure. This version adds two new recipes: Chroma Subsampling Explained (tucked under Configure digiKam)"
category: "news"
aliases: "/node/750"

---

<p>A new release of <a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a> is ready for your reading pleasure. This version adds two new recipes: <em>Chroma Subsampling Explained</em> (tucked under <em>Configure digiKam</em>) and <em>Create Embeddable Maps with digiKam and uMap</em>.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img src="https://scribblesandsnaps.files.wordpress.com/2015/11/digikamprecipes-4-9-7.png" alt="digikamprecipes-4.9.7" width="375" height="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2016/01/15/digikam-recipes-4-9-9-released/">Continue reading</a></p>

<div class="legacy-comments">

</div>