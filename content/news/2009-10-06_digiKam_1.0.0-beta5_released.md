---
date: "2009-10-06T08:23:00Z"
title: "digiKam 1.0.0-beta5 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! 5th beta release of digiKam 1.0.0 is out... digiKam tarball can be downloaded from SourceForge at this url digiKam"
category: "news"
aliases: "/node/482"

---

<p>Dear all digiKam fans and users!</p>

<p>5th beta release of digiKam 1.0.0 is out...</p>

<a href="http://www.flickr.com/photos/digikam/3986687364/" title="digikam-1.0.0-beta5 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2593/3986687364_d51f2d1714.jpg" width="500" height="400" alt="digikam-1.0.0-beta5"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>digiKam will be also available for Windows. Pre-compiled packages can be downloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>
 
<h5>NEW FEATURES (since 0.10.x series):</h5><br>

<b>General</b>        : Exif, Makernotes, Iptc, and Xmp metadata tags filtering can be customized in setup config dialog.<br>
<b>General</b>        : Metadata Template: add support of Contact, Location, and Subjects IPTC information.<br>
<b>General</b>        : Add support of Author properties to multi-language Captions. User can set an author name for each caption value.<br>
<b>General</b>        : <a href="http://www.digikam.org/node/427">Add new Batch Queue Manager</a>.<br>
<b>General</b>        : New tool bar animation banner.<br>                                
<b>General</b>        : Fix compilation under MSVC 9.<br>                                 
<b>General</b>        : <a href="http://www.digikam.org/node/442">New first run assistant</a>.<br>    
<b>General</b>        : New dialog to show Database statistics.<br>                       
<b>General</b>        : Add support of wavelets image file <a href="http://www.libpgf.org">format PGF</a>.<br>  
<b>General</b>        : Added support of Metadata Template to set easily image copyright.<br>
<b>General</b>        : Added support of LightRoom keywords path for interoperability purpose.<br>
<b>General</b>        : Added Multi-languages support to Caption and Tags right side-bar.<br>
<b>General</b>        : digiKam use a new database to cache thumbnails instead ~./thumbnails. 
                        File format used to store image is PGF (http://www.libpgf.org).       
                        PGF is a wavelets based image compression format and give space optimizations.<br>
<b>General</b>        : Add OpenStreetMap support in Geolocation panels.<br>
<b>General</b>        : Showfoto, Editor, Preview, LightTable: Thumbbar is now dockable everywhere.<br>
<b>General</b>        : Complete re-write of Color Management code. Add support of thumbs and preview CM.<br>
<b>General</b>        : AdvancedRename utility is used throughout digiKam now (AlbumUI, CameraUI and BQM).

<br><br>

<b>AlbumGui</b>       : Icon view is now based on pure Qt4 model/view.<br>
<b>AlbumGui</b>       : New overlay button over icon view items to perform lossless rotations.<br>
<b>AlbumGui</b>       : Find Duplicates search can be limited to an album and tag range.<br><br>

<b>ImageEditor</b>    : Close tools by pressing ESC.<br>
<b>ImageEditor</b>    : <a href="http://www.digikam.org/node/439">New Liquid Rescale tool.</a><br>
<b>ImageEditor</b>    : New menu option to switch image color space quickly.<br>
<b>ImageEditor</b>    : Black and White converter: add standard Blue and Yellow-Green color filters.<br><br>

<b>CameraGUI</b>      : New history of camera actions processed.<br><br>

BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):<br>

001 ==&gt; 091742 : Batch process auto levels.<br>
002 ==&gt; 149361 : Not all edit actions are available as batch commands (for example auto-levels and gamma correction).<br>
003 ==&gt; 187523 : digiKam crashes on startup (undefined symbol).<br>
004 ==&gt; 187569 : Crash on star removing when filtering by stars.<br>
005 ==&gt; 186549 : digiKam crashes when hovering the bottom part of its.<br>
006 ==&gt; 187641 : Error while loading shared libraries: libkdcraw.so.5.<br>
007 ==&gt; 187429 : Crash on Launch after disable KIPI plugins rotate.<br>
008 ==&gt; 187937 : Writing tags to files doesn't update modification timestamp.<br>
009 ==&gt; 184687 : Crash after deleting an empty album.<br>
010 ==&gt; 186766 : Crash after finish resize and rename of images in folder.<br>
011 ==&gt; 184953 : Zooming and paning in slideshow / without toolbars.<br>
012 ==&gt; 187508 : Spontaneous crash whilst doing nothing in particular.<br>
013 ==&gt; 188235 : Assigning a color profile to TIFF images does not work anymore with 0.10.0.<br>
014 ==&gt; 187748 : Install application icons to right path for non-KDE desktops.<br>
015 ==&gt; 186255 : digiKam crashes when switching collection database.<br>
016 ==&gt; 178292 : AspectRatioCrop is slow and jumps around in first second.<br>
017 ==&gt; 181892 : AspectRatioCrop: some properties should be disabled.<br>
018 ==&gt; 188573 : RW2 converter broken for low light images.<br>
019 ==&gt; 188642 : GPS data in Digikam shows the wrong places on the map.<br>
020 ==&gt; 188907 : Album view should display subfolders, like tag view does.<br>
021 ==&gt; 183038 : "Select All" action interrupts other widgets.<br>
022 ==&gt; 189022 : Better guessing of proportions with 'inverse transformation' option.<br>
023 ==&gt; 189286 : EXIF info lost while converting from NEF to TIFF.<br>
024 ==&gt; 189336 : Preview of RAW look different than actual image.<br>
025 ==&gt; 189413 : Images without GPS-data are shown on the equator in the geolocation-view.<br>
026 ==&gt; 184954 : Unable to delete items from camera - canon sx10is.<br>
027 ==&gt; 189168 : You do not have write access to your home directory base path.<br>
028 ==&gt; 185617 : Images are invalid / not found when an album is moved.<br>
029 ==&gt; 187265 : Advanced search crashes if a selected album has been deleted.<br>
030 ==&gt; 185177 : Searches are not updated when changes are made to the resulting image set.<br>
031 ==&gt; 189542 : Crash on opening download dialog.<br>
032 ==&gt; 114225 : Free rotation using an horizontal line.<br>
033 ==&gt; 189843 : Crash on ubuntu jaunty beta.<br>
034 ==&gt; 167836 : Image preview is too large.<br>
035 ==&gt; 189250 : Editing a canon camera jpeg adds broken sRGB profile.<br>
036 ==&gt; 187733 : Cannot turn off sidebar after Infrared filter usage ?<br>
037 ==&gt; 188755 : Strange behaviour on selection.<br>
038 ==&gt; 185884 : Empty view after clicking the left thumbnail image in Image Editor filter view.<br>
039 ==&gt; 188062 : Synchronize Images with Database does not save Copyright, Credit, Source, By-line, By-line Title in IPTC.<br>
040 ==&gt; 186823 : Several bugs in lens defect auto-correction plugin.<br>
041 ==&gt; 190612 : digiKam and Showfoto crash.<br>
042 ==&gt; 191092 : No images shown in digiKam albums.<br>
043 ==&gt; 191203 : digiKam crashed on first load, after being installed on ubuntu 9.04.<br>
044 ==&gt; 151321 : Remaining space incorrectly calculated when using symlinks (import from camera).<br>
045 ==&gt; 189742 : Thumbnails not saved between sessions.<br>
046 ==&gt; 180507 : Crash when dragging picture to another app with kwin_composite enabled.<br>
047 ==&gt; 190296 : Can not save images on Windows.<br>
048 ==&gt; 141240 : Camera download: file rename with regexps.<br>
049 ==&gt; 181951 : digiKam crashes (SIGABRT) during the importing of pictures from Kodak V530 camera.<br>
050 ==&gt; 166392 : File-Renaming: custom sequence-number doesn't work.<br>
051 ==&gt; 182332 : Can't stop generating of thumbnails when import from a card reader.<br>
052 ==&gt; 152382 : Unable to view photo.<br>
053 ==&gt; 161744 : Filenames are lower case when imported via sd card reader, upper case when imported via usb cable directly from camera.<br>
054 ==&gt; 187560 : Iptc data for author are missing while downloading images.<br>
055 ==&gt; 192294 : digiKam crash when i try to import photos from my card reader.<br>
056 ==&gt; 192413 : Missing items from right-click search menu.<br>
057 ==&gt; 131551 : Camera port info deleted when editing camera title.<br>
058 ==&gt; 181726 : digiKam collect video, music when I connect usb key.<br>
059 ==&gt; 154626 : Autodetect starting number for "appending numbers" when downloading images.<br>
060 ==&gt; 188316 : Missing NEF files when importing from USB disk.<br>
061 ==&gt; 193163 : digiKam ignores settings on the rating stars.<br>
062 ==&gt; 189168 : You do not have write access to your home directory base path.<br>
063 ==&gt; 193226 : Data loss on downloading photos from camera.<br>
064 ==&gt; 189460 : digiKam crash when goelocating image in the marble tab.<br>
065 ==&gt; 186046 : Crash after calling settings.<br>
066 ==&gt; 193348 : Can't build digiKam on kde 4.2.3.<br>
067 ==&gt; 150072 : Download with digiKam doesn't work with the first start of digiKam.<br>
068 ==&gt; 193738 : Have the image editor status bar show the current color depth.<br>
069 ==&gt; 191678 : Exif rotation for Pentax Pef not working.<br>
070 ==&gt; 146455 : Image rotation and auto-rotate don't work.<br>
071 ==&gt; 152877 : Thumbnails: URI does not follow Thumbnail Managing Standard.<br>
072 ==&gt; 140615 : Pre-Creating of image thumbnails in whole digiKam/all subfolders.<br>
073 ==&gt; 193967 : VERY fast loading of thumbnails [PATCH].<br>
074 ==&gt; 191492 : digiKam hangs on startup "reading database".<br>
075 ==&gt; 171950 : Lost pictures after 'download/delete all' when target dir had same file names.<br>
076 ==&gt; 191774 : digiKam download sorting slow, and wrong with multiple folders.<br>
077 ==&gt; 191842 : digiKam download forgets to download some pictures (multiple big folders).<br>
078 ==&gt; 187902 : Status bar gives wrong picture filename and position in list after "Save As".<br>
079 ==&gt; 193894 : Pop up "Copy Finished!" dialogue when done downloading from camera.<br>
080 ==&gt; 193870 : digiKam freezes after second exif rotation on same file.<br>
081 ==&gt; 194342 : What is the right rename pattern to get names like 2009.02.18_09h56m30s ?<br>
082 ==&gt; 194177 : digiKam crashes always on startup and causes signal 11 (SIGSEGSV) - app not useable.<br>
083 ==&gt; 169213 : digiKam should not enter new album after creating it.<br>
084 ==&gt; 191634 : Statistics of the database, and all pictures ?<br>
085 ==&gt; 193228 : Experimental option "write metadata to RAW files" corrupts Nikon NEFs.<br>
086 ==&gt; 194116 : digiKam crashed for no obvious reason.<br>
087 ==&gt; 179766 : Auto crop does not fully remove black corners created by free rotation.<br>
088 ==&gt; 134308 : Add arrow buttons on thumbnail to perform lossless rotation.<br>
089 ==&gt; 194804 : Tag, rate, rotate and delete via slideshow.<br>
090 ==&gt; 192425 : Star rating setting under thumbnails is a nuisance.<br>
091 ==&gt; 194330 : digiKam crashed when deleting image.<br>
092 ==&gt; 188959 : On first use, digiKam should not scan for images without user confirmation.<br>
093 ==&gt; 195202 : Thumbnail rename fails (cross-device link) and so cache doesn't work.<br>
094 ==&gt; 161749 : Raw Converter needs more options.<br>
095 ==&gt; 150598 : File rename during image upload fails silently.<br>
096 ==&gt; 161865 : Image not placed in New Tag category.<br>
097 ==&gt; 136897 : THM files are not uploaded.<br>
098 ==&gt; 195494 : Deleting and cancelling a picture with the keyboard still delete the picture.<br>
099 ==&gt; 120994 : Print preview generates HUGE postscript files.<br>
100 ==&gt; 191633 : Rename a folder, press F5 and see thumbnails go away.<br>
101 ==&gt; 195565 : Loading next image while in curves tool does not confirm and loads nothing.<br>
102 ==&gt; 189046 : digiKam crashes finding duplicates in fingerprints.<br>
103 ==&gt; 185726 : SIGSERV 11 by choosing an image for fullscreen.<br>
104 ==&gt; 193616 : Location not shown digiKam right sidebar after geolocating in external program.<br>
105 ==&gt; 178189 : Do not create preview automatically for large pictures.<br>
106 ==&gt; 172807 : Warning: JPEG format error, rc = 5.<br>
107 ==&gt; 188936 : Not finding all pictures on camera.<br>                    
108 ==&gt; 195812 : Crash when rotating image in album overlay (rightest rotate icon).<br>
109 ==&gt; 094562 : Renaming an album recreates all thumbnails.<br>                       
110 ==&gt; 193124 : Import image should allow both direct and reverse ordering.<br>
111 ==&gt; 188051 : Sort order of filenames in camera - no way to set or choose.<br>      
112 ==&gt; 183435 : No more file manager at media:/camera.<br>                            
113 ==&gt; 189979 : All displays of the same caption don't show the same text when it contains "éèà".<br>
114 ==&gt; 195902 : digiKam crashes on first startup if album folder is not empty.<br>                   
115 ==&gt; 154606 : Changing the date for multiple pictures simultaneously changes all of the times too.<br>
116 ==&gt; 143932 : Utf-8 displayed with wrong encoding when restoring photo to digiKam.<br>                
117 ==&gt; 179227 : Generation of XMP tags crashes the application.<br>                                     
118 ==&gt; 188988 : digiKam crash while scanning images.<br>                                                
119 ==&gt; 192085 : digiKam suddenly crashed when managing pictures.<br>                                    
120 ==&gt; 196465 : Missing reset button for curve in monochrome conversion tool.<br>                       
121 ==&gt; 196329 : Renaming album causes problems (until re-opening).                                  
122 ==&gt; 191288 : CRC error in chunk zTXt.<br>                                                            
123 ==&gt; 189982 : digiKam crashes during start-up.<br>                                                    
124 ==&gt; 098462 : Support for comments in different languages.<br>                                        
125 ==&gt; 159158 : Tags moved in tree are duplicated in the IPTC metadata.<br>                             
126 ==&gt; 186308 : Moving tags in hierarchy not reflected in XMP metadata "TagsList".<br>                  
127 ==&gt; 141912 : Sync Metadata Batch Tool does not reflect changes in tags properly.<br>                 
128 ==&gt; 175321 : Synchronize tags to iptc keywords adds instead of copying.<br>                          
129 ==&gt; 195663 : Crash while adding new tag.<br>                                                         
130 ==&gt; 195735 : digiKam-svn fails to compile on Mac OS X.<br>                                           
131 ==&gt; 196768 : With 4.2.90 digikam always crashes on startup - worked fine with 4.2.3.<br>             
132 ==&gt; 190719 : Compiling digikam-0.10.0 on gentoo fails.<br>                                           
133 ==&gt; 197129 : Hierarchical setting of keywords.<br>                                                   
134 ==&gt; 195079 : digiKam albums appear empty.<br>                                                        
135 ==&gt; 195494 : Deleting and cancelling a picture with the keyboard still delete the picture.<br>       
136 ==&gt; 195975 : Images are not visible.<br>                                                             
137 ==&gt; 196462 : IconView (Qt4 Model/view based): Navigation broken after delete.<br>                    
138 ==&gt; 192055 : Typo digiKam tip of the day.<br>                                                        
139 ==&gt; 164026 : Wish: use embedded thumbs within jpegs to speedup album display.<br>                    
140 ==&gt; 197152 : Startup wizard has problems creating directories.<br>                                   
141 ==&gt; 196726 : digiKam crashes while creating thumbnails.<br>                                         
142 ==&gt; 182852 : Timeline: click area small an unintuitive.<br>                                          
143 ==&gt; 173899 : Timeline does not show any photos.<br>                                                  
144 ==&gt; 196994 : digiKam 1.0.0-0.1.beta1.f11 from kde-redhat crash on startup.<br>                       
145 ==&gt; 180076 : Moving a big folder to another subfolder folder does ... nothing.<br>                  
146 ==&gt; 191557 : Previous and next image is showing weird results.<br>                                   
147 ==&gt; 197360 : In the "edit captions" sidebar moving a tag to white space crashes digiKam.<br>         
148 ==&gt; 195799 : digiKam takes 100% CPU when exiting and finally crashes.<br>                            
149 ==&gt; 152219 : Impossible to clear comments in metadata.<br>                                           
150 ==&gt; 197198 : Libpng not detected during configure.<br>                                               
151 ==&gt; 152199 : Deleting tags can leave orphaned IPTC keywords inside files.<br>                        
152 ==&gt; 197285 : Understand keyword hierarchies with different delimitors.<br>                           
153 ==&gt; 197640 : Add the "New folder" Option while dragging items to folders.<br>                        
154 ==&gt; 197744 : Lists of languages miss en-US.<br>                                                      
155 ==&gt; 197622 : digiKam crashes on exit.<br>                                                            
156 ==&gt; 197808 : Bad images sort order when starting digiKam.<br>                                        
157 ==&gt; 197641 : Can't delete exif x-default caption once set.<br>                                       
158 ==&gt; 196437 : digiKam crashes at startup.<br>                                                         
159 ==&gt; 197868 : digiKam crashes on startup (digikam-0.10.0-2/x86_64/Archlinux).<br>                     
160 ==&gt; 196686 : Iconview (Qt4 Model/View based) : Select multiple images with Ctrl + Mouse not working anymore in 1.0.0-beta1.<br>
161 ==&gt; 197961 : Cameragui freezes digikam when starting to download images from camera.<br>
162 ==&gt; 197445 : digiKam freezes while attempting to import images from canon powershot a540.<br>
163 ==&gt; 198067 : digiKam does not show any pictures.<br> 
164 ==&gt; 186638 : Canon 450D import issues.<br>                                   
165 ==&gt; 195809 : Radio buttons in "Search Group" "Options" in "Advanced search" dialog do not show selection status.<br>
166 ==&gt; 177686 : Menu text is invisible.<br>
167 ==&gt; 198509 : Tagging by drag and drop not possible anymore.<br>
168 ==&gt; 198531 : Compilation error in libpgf [PATCH].<br>
169 ==&gt; 198530 : Compilation error in imagecategorizedview.cpp.<br>
170 ==&gt; 139361 : Templates for meta data.<br>
171 ==&gt; 175923 : digiKam uses wrong album root path/wrong disk uuid.<br>
172 ==&gt; 185065 : Image editor loads new image without being asked to.<br>
173 ==&gt; 188017 : Video colours inverted after digiKam started.<br>
174 ==&gt; 194950 : digiKam endlessly spewing text to stderr.<br>
175 ==&gt; 189080 : Comments not saved with digikam 0.10.0 on KDE 4.2.2.<br>
176 ==&gt; 198868 : Pictures are not displayed in the main view i just can see them in the batch menu.<br>
177 ==&gt; 199168 : Tags in db but not in IPTC.<br>
178 ==&gt; 184445 : Deleting multiple files in folder view gives me multiple errors.<br>
179 ==&gt; 151749 : External URLs from pictures for related information.<br>
180 ==&gt; 192701 : Application: digiKam (digiKam), signal SIGSEGV --Crash under Gnome (Ubuntu 9.04).<br>
181 ==&gt; 199394 : Request: "regenerate folder thumbnails" menu item.<br>
182 ==&gt; 199420 : Duplication of Albums when naming them using upper case letters.<br>
183 ==&gt; 192535 : digiKam collections on removable media are treated as local collections.<br>
184 ==&gt; 199497 : Open camera rejected with: "Fehler beim Auflisten der Dateien in /store_00010001/DCIM/100CANON.".<br>
185 ==&gt; 199602 : Exif rotation not interpreted the same way in different tools.<br>
186 ==&gt; 191522 : Crash on use default action "Download Photos with digiKam".<br>
187 ==&gt; 196463 : Crash on device action Download Photos with digiKam.<br>
188 ==&gt; 199617 : When clicking on "My Tags", the view switches automatically after 2 seconds.<br>
189 ==&gt; 199076 : Sudden crash of digiKam during work.<br>
190 ==&gt; 195121 : digiKam crashes on view image (exiv2).<br>
191 ==&gt; 142566 : Update metadata doesn't work correctly for creation date.<br>
192 ==&gt; 199251 : digiKam crashes when setting exif orientation tag.<br>
193 ==&gt; 199482 : Caption can't be canged.<br>
194 ==&gt; 174586 : Loading video from camera consumes ll memory.<br>
195 ==&gt; 199667 : Make writing of metadata to picture enabled by default.<br>
196 ==&gt; 199981 : XMP synchronization of digiKam namespace occurs for second tag.<br>
197 ==&gt; 199967 : Creation date newer than last modified date.<br>
198 ==&gt; 200190 : Tag filters: unlike older versions AND-condition doesn't match anymore.<br>
199 ==&gt; 193616 : Location not shown digiKam right sidebar after geolocating in external program.<br>
200 ==&gt; 189156 : digikam crashes on importing directories with some (cyrillic?) file names.<br>
201 ==&gt; 191963 : Calender sort by wrong dates on images without properties.<br>
202 ==&gt; 189072 : Right click popup menu.<br>
203 ==&gt; 200223 : Default language of spellchecker is ignored.<br>
204 ==&gt; 126086 : Besides the basic and full exif info I would like a page with selectable fields.<br>
205 ==&gt; 200637 : Too small fonts in collections setup page.<br>
206 ==&gt; 135476 : Support for multiple comment authors.<br>
207 ==&gt; 200246 : digiKam does not recognize GPS EXIF information.<br>
208 ==&gt; 188999 : XMP tags not imported properly.<br>
209 ==&gt; 199148 : Iptc ascii vs unicode pb.<br>
210 ==&gt; 169685 : Tags lost on copy via context menu.<br>
211 ==&gt; 200162 : EXIF tags not updated if digiKam tag is renamed.<br>
212 ==&gt; 200854 : Option to clear light table on close.<br>
213 ==&gt; 200805 : List of supported file types can not be edited, just extended.<br>
214 ==&gt; 200187 : Save and restore selected filter tags.<br>
215 ==&gt; 196999 : Calendar is ignoring selection of date/week but respecting month.<br>
216 ==&gt; 190795 : USB PTP Class does not work anymore with the KDE4 version.<br>
217 ==&gt; 188905 : Edit view goes 100% instead of fit window.<br>
218 ==&gt; 187810 : In import window images are sorted in reverse order but folders are not.<br>
219 ==&gt; 199619 : Deleting image and switching to next fastly seems to confuse album list.<br>
220 ==&gt; 121804 : Image overwritten with blank file when importing into same folder.<br>
221 ==&gt; 200783 : digiKam crashes while loading JPEG images.<br>
222 ==&gt; 200979 : Trust 715 LCD camera  no driver.<br>
223 ==&gt; 179902 : Picture numbering fails if pics taken at more than 1 image per second.<br>
224 ==&gt; 184541 : Resize error when sharpening in image editor.<br>
225 ==&gt; 134389 : Sort images by date - even when spanning albums.<br>
226 ==&gt; 195109 : Integrate lighttable in digiKam.<br>
227 ==&gt; 199373 : LightTable Crash with Image Drag and Drop.<br>
228 ==&gt; 192563 : Zooming into the earth map buggy.<br>
229 ==&gt; 192534 : Rename a folder, then rename a photo and you get an error.<br>
230 ==&gt; 193522 : First run (with existing v3 database) fails, "No album library path...".<br>
231 ==&gt; 147885 : Update metadata doesn't work correctly for creation date.<br>
232 ==&gt; 196462 : IconView (Qt4 Model/view based): Navigation broken after delete.<br>
233 ==&gt; 195871 : Image tool tip does not show all tags if too long.<br>
234 ==&gt; 201560 : Crash at start up when scanning images.<br>
235 ==&gt; 197508 : Geo-localisation integration missing, but marble is installed.<br>
236 ==&gt; 201640 : Opportunity to compile digiKam without Nepomuk service.<br>
237 ==&gt; 201692 : No thumbnails in album view.<br>
238 ==&gt; 185098 : digiKam craches sometimes on raw files selection.<br>
239 ==&gt; 201624 : Showfoto =&gt; color-management =&gt; input-profile =&gt; search =&gt; crash.<br>
240 ==&gt; 151712 : Adjust Exif orientation tag - wrong permission interpretation.<br>
241 ==&gt; 171983 : Enable display of Openstreetmap maps in the Map Widget.<br>
242 ==&gt; 155097 : Performance problems with recursive view and filtering.<br>
243 ==&gt; 200392 : Slideshow displays "Headline" field as "image caption" and has no way to display "Caption" field.<br>
244 ==&gt; 201302 : Crash due to memory access problem on Mac OS X.<br>
245 ==&gt; 133094 : Exif-changes by external prog has no affect inside digiKam.<br>
246 ==&gt; 201746 : Invalid TIFF output in Editor and Batch queue manager.<br>
247 ==&gt; 196692 : Red and blue channels in images are reversed when saving.<br>
248 ==&gt; 202332 : View mode: Zooming using Ctrl+Scroll is broken.<br>
249 ==&gt; 197254 : digikam crash at startup.<br>
250 ==&gt; 202779 : When pressing 'enter', thumbnails should be enlarged, also when a collection is selected.<br>
251 ==&gt; 203574 : Showfoto crashes when opening jpeg file.<br>
252 ==&gt; 203498 : No drop-down menu for export.<br>
253 ==&gt; 203589 : Crash in the noise reduction plugin when 'Threshold' equals zero.<br>
254 ==&gt; 198006 : digiKam crash after assistant finished.<br>
255 ==&gt; 203083 : Crash starting digikam (after kde 4.3 upgrade).<br>
256 ==&gt; 202954 : digiKam crashes on start while scaning picture directory.<br>
257 ==&gt; 203031 : Album list doesn't show pictures even after tons of new pictures were imported.<br>
258 ==&gt; 201810 : Segfault moving files to trash.<br>
259 ==&gt; 193763 : digiKam doesn'd display any photos.<br>
260 ==&gt; 202532 : Wrong rename sequence when downloading.<br>
261 ==&gt; 163161 : Adding ICC profiles confuses digiKam.<br>
262 ==&gt; 148151 : Problem with icc profle Color Management in digiKam with feisty fawn.<br>
263 ==&gt; 162944 : Cms/editor: converting colorspace doesn't work.<br>
264 ==&gt; 165650 : digiKam appears to add a color profile for printing?<br>
265 ==&gt; 182272 : Color profile and white balance.<br>
266 ==&gt; 204656 : Import: "Device Information": linebreaks not HTMLified in KTextEdits [patch].<br>
267 ==&gt; 189073 : Does not save embedded color profile.<br>
268 ==&gt; 195144 : Editor shows wrong colors when loading image without color profile and color management is on.<br>
269 ==&gt; 152521 : digiKam Edit / Showfoto: Use Color Managed View setting in config page is ignored.<br>
270 ==&gt; 163160 : Remember color management status between sessions.<br>
271 ==&gt; 204797 : Click lock icon crash digiKam.<br>
272 ==&gt; 204871 : digiKam crash when rating a picture on the lighttable.<br>
273 ==&gt; 203594 : Option to display a vertical thumb bar.<br>
274 ==&gt; 195050 : Color management doesn't work in thumbnails and view mode.<br>
275 ==&gt; 179802 : After raw import image not shown in editor.<br>
276 ==&gt; 172196 : Color managed view is inaccurate.<br>
277 ==&gt; 197537 : Program crashes when starting import from usb camera (canon 450d).<br>
278 ==&gt; 194950 : digiKam endlessly spewing text to stderr.<br>
279 ==&gt; 193482 : XMP/IPTC fields in Batch Rename tool.<br>
280 ==&gt; 183435 : No more file manager at media:/camera.<br>
281 ==&gt; 200758 : digiKam, dolphin, dc is not in peripherals list.<br>
282 ==&gt; 206071 : Not reading image properties.<br>
283 ==&gt; 191113 : digiKam doesn't recognize images with colorspace adobergb.<br>
284 ==&gt; 206487 : Unsharp mask algorithm in batch processing should equal the one in kipi.<br>
285 ==&gt; 206570 : Crashed when scrolling through thumbnails in edit mode.<br>
286 ==&gt; 206712 : Raw Import Tool: RGB (linear) provides gamma corrected output and should be named as such.<br>
287 ==&gt; 206857 : Unclear English in sentence.<br>
288 ==&gt; 149485 : Advanced image resize for the digiKam editor.<br>
289 ==&gt; 206426 : Default ICC profile locations reads the same directory twice.<br>
290 ==&gt; 203176 : Deleting 2 files still shows one afterwards.<br>
291 ==&gt; 205059 : The default path for image library is not localizable.<br>
292 ==&gt; 204084 : Showfoto claims that there are no images in directory that contains symlinks to images.<br>
293 ==&gt; 197817 : digiKam should utilize _icc_profile X property.<br>
294 ==&gt; 205756 : Copy and paste fails silently when pasting file back into the same folder.<br>
295 ==&gt; 207239 : When pressing Enter thumbnails should be enlarged, also after changing from Tags to Collection or similar.<br>
296 ==&gt; 200830 : 100 % CPU usage without obvious reason.<br>
297 ==&gt; 204480 : Unable to access albums on drives other than C:.<br>
298 ==&gt; 154463 : Autorename while transferring pictures from cam to PC adds lots of '0'.<br>
299 ==&gt; 207882 : Manual: RAW decoder settings: 16 bit not clearly explained.<br>
300 ==&gt; 208004 : Geolocation tool needs higher zoom levels.<br>
301 ==&gt; 207293 : when showing whole collection status bar filters don't work.<br>
302 ==&gt; 206670 : after deleting an image the image before the deleted image is displayed/selected instead of the image after the deleted one.<br>
303 ==&gt; 208160 : After edition image has different colors in QT nad GTK+ applications.<br>
304 ==&gt; 207332 : editor works on the wrong image if a new one was chosen while processing.<br>
305 ==&gt; 206666 : thumbnail borders are drawn inside instead of outside the images thus hiding parts of the image.<br>
306 ==&gt; 195050 : Color management doesn't work in thumbnails and view mode.<br>
307 ==&gt; 200357 : digiKam, exif rotation, thumbnail not rotated.<br>
308 ==&gt; 203485 : consider using KGlobalSettings::picturesPath() instead of KGlobalSettings::documentPath().<br>
309 ==&gt; 204053 : Remember cursor position in album view after refresh / sync.<br>
310 ==&gt; 197413 : Changing the name of an album does not update album list.<br>
311 ==&gt; 202780 : Channel Mixer: Preview not updated when opening dialog.<br>
312 ==&gt; 191854 : digiKam crashed when scanning newly added folders to local collections.<br>
313 ==&gt; 204237 : Attempting Add HPE427 Photosmart Camera To Import Pictures.<br>
314 ==&gt; 196480 : Crash on starting digiKam.<br>
315 ==&gt; 207053 : Replace current renaming in AlbumUI with AdvancedRename utility.<br>
316 ==&gt; 206094 : Camera GUI: Selection with Shift+Pos1/End does not work.<br>
317 ==&gt; 092098 : Plugin for automatically adding copyright marks to pictures.<br>
318 ==&gt; 192242 : Album rename sometimes doesn't show existing album name in dialog.<br>
319 ==&gt; 208851 : Collections: Detection of duplicate collections too strict.<br>
320 ==&gt; 179370 : Crashed while downloading a video file from camera.<br>
321 ==&gt; 141238 : Album tree lacks horizontal scroll bar.<br>
322 ==&gt; 207486 : With 16-bit images, adjustlevels acts weird when inserting manually values into input fields.<br>
323 ==&gt; 193748 : Crash upon importing images from camera.<br>
324 ==&gt; 206109 : digiKam marble map theme not selectable.<br>
325 ==&gt; 207994 : Refresh meta data view on right side after exif import.<br>
326 ==&gt; 207911 : Crash after trying to re-open Image Editor containing an unsaved RAW image.<br>
327 ==&gt; 204080 : It crashed when it was closing.<br>
328 ==&gt; 198554 : Crash on making thumbnails, every time.<br>
329 ==&gt; 207330 : digiKam crashes if the editor has to open a new image while working on another.<br>
330 ==&gt; 209189 : Make albumview rotation buttons optional.<br>
331 ==&gt; 209343 : Mouse-wheel should jump to next/previous picture if used when viewing a picture.<br>
332 ==&gt; 193879 : Crash while rating pictures.<br>
333 ==&gt; 209437 : Allow to use mouse click and drag to move around the image in View mode.<br>

<div class="legacy-comments">

  <a id="comment-18729"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18729" class="active">Great! Especially that "my"</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-10-06 10:57.</div>
    <div class="content">
     <p>Great! Especially that "my" bugs got fixed that fast - digikam is really amazing!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18740"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18740" class="active">ubuntu repositories</a></h3>    <div class="submitted">Submitted by sitositos (not verified) on Fri, 2009-10-16 11:40.</div>
    <div class="content">
     <p>For kubuntu 9.04 or 9.10, are there repositories? Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18730"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18730" class="active">Digikam developers are really</a></h3>    <div class="submitted">Submitted by FSFmember (not verified) on Tue, 2009-10-06 13:05.</div>
    <div class="content">
     <p>Digikam developers are really extraordinary. So many bugfixes and new features in every *beta* release! Great work, you deserve all the best from community. Thank you!</p>
<p>(but this captcha is killing me)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18733"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18733" class="active">The changelog might be a</a></h3>    <div class="submitted">Submitted by Andi Clemens on Fri, 2009-10-09 23:13.</div>
    <div class="content">
     <p>The changelog might be a "little bit" misleading ;-) ... these are the changes from digiKam-0.10 until now.<br>
Anyway thanks for the thanks :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18731"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18731" class="active">Availability for Opensuse</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-10-08 18:02.</div>
    <div class="content">
     <p>Hello is your great piece of software already available for OpenSuse?</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18734"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18734" class="active">The best would be to ask in</a></h3>    <div class="submitted">Submitted by Andi Clemens on Fri, 2009-10-09 23:15.</div>
    <div class="content">
     <p>The best would be to ask in some OpenSUSE mailinglist or forum, we don't create packages for all the various distributions.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18737"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18737" class="active">Here you should find</a></h3>    <div class="submitted">Submitted by Jonas (not verified) on Wed, 2009-10-14 11:02.</div>
    <div class="content">
     <p>Here you should find something: http://software.opensuse.org/search</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18735"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18735" class="active">win32 problem</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-10-10 13:50.</div>
    <div class="content">
     <p>I have installed fresh new kde win32 - just to see digikam. End user, compiler mode:msvc. Digikam won't run: unable to load kipi.dll (no such file kipi.dll on hdd, kipi-plugins installed, kipiplugins.dll exist). </p>
<p>Could you look at that or relay info to package maintainer?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18736"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18736" class="active">win32 problem</a></h3>    <div class="submitted">Submitted by <a href="http://windows.kde.org" rel="nofollow">SaroEngels</a> (not verified) on Sat, 2009-10-10 14:20.</div>
    <div class="content">
     <p>Hi,<br>
I have fixed this problem some minutes ago, can you try to update/reinstall digikam then from the server winkde.org? Sorry for the error.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18741"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/482#comment-18741" class="active">Title tag</a></h3>    <div class="submitted">Submitted by PaulL (not verified) on Sun, 2009-10-18 11:23.</div>
    <div class="content">
     <p>Is it possible to add new tags?  I'm interested in the Title tag, as my web publishing stream uses this in generating web pages.  It'd be great to be able to edit it.  I think it is in the XMP area.</p>
<p>Is it possible to generalise digikam so that any arbitrary attributes can be edited - so if there was a config file somewhere that listed the tags to display?  That would be pretty good.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
