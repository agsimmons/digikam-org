---
date: "2010-08-25T16:27:00Z"
title: "digiKam 1.4.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam 1.4.0 release! digiKam tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/535"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam 1.4.0 release!</p>

<a href="http://www.flickr.com/photos/digikam/4918626069/" title="digikam1.4.0 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4122/4918626069_5c0a9ba032.jpg" width="500" height="298" alt="digikam1.4.0"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>See the list of new features below and bug-fixes coming with this release since 1.3.0</p>

<p>Enjoy digiKam.</p>

<h5>NEW FEATURES:</h5><br>

<b>AdvancedRename</b> : New modifier "Remove Doubles".<br>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 238285 : Nikkor 18-200mm not detected by lens autocorrection.<br>
002 ==&gt; 232233 : Databaseext-branch does not compile on Windows with mingw4.<br>
003 ==&gt; 241782 : Tags, comments, author, etc. not written to metadata.<br>
004 ==&gt; 232792 : digiKam crash when opening configuration.<br>
005 ==&gt; 242863 : Caption field layout can be confused by pasted text.<br>
006 ==&gt; 242937 : When using raw import tool, showfoto cannot export the processed image.<br>
007 ==&gt; 243696 : digiKam didn't detect my local collection anymore after replacing internal harddisk.<br>
008 ==&gt; 243747 : Comment which is stored in database can't be accessed via digiKam.<br>
009 ==&gt; 216273 : digiKam exif gps not in xmp.<br>
010 ==&gt; 244114 : "Find similar" works for a single image, but "Find duplicates" doesn't work for a directory.<br>
011 ==&gt; 244151 : Renaming image files causes them to vanish from an album.<br>
012 ==&gt; 241418 : LZW compressed TIFFs from RawTherapee are won't display.<br>
013 ==&gt; 212848 : Crashed in slideshow while viewing.<br>
014 ==&gt; 245061 : [patch] "Restore Tag Filters" is broken, works only for top level nodes.<br>
015 ==&gt; 245336 : Image editor does not remember camera profile under Custom.<br>
016 ==&gt; 243692 : digiKam uses all memory with thumbnails.<br>
017 ==&gt; 226544 : Suspicious QModelIndex messages at startup.<br>
018 ==&gt; 240945 : Picture size does not report correctly after gimp.<br>
019 ==&gt; 245380 : Improve the ergonomics of the bwsepia filter ui.<br>
020 ==&gt; 204568 : Metadata not being written to files.<br>
021 ==&gt; 246938 : Extension aware rename (maintain raw file and jpg association by basename).<br>
022 ==&gt; 247850 : digiKam 1.3.0 crashes.<br>
023 ==&gt; 247590 : digiKam not checking if libexiv2 was compiled with xmp support.<br>
024 ==&gt; 246774 : Create New Tag vs Create New Tag in.<br>
025 ==&gt; 246675 : Crash on startup/scanning folders.<br>
026 ==&gt; 225471 : Change digiKam to follow usability guidelines.<br>
027 ==&gt; 248549 : digiKam crash at startup (suse's backport repo).<br>

<div class="legacy-comments">

  <a id="comment-19447"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19447" class="active">Thanks DigiDev's!</a></h3>    <div class="submitted">Submitted by <a href="http://krita.org" rel="nofollow">Kubuntiac</a> (not verified) on Wed, 2010-08-25 19:03.</div>
    <div class="content">
     <p>Looks like another great release of everyone's favourite photo manager! Particularly greatful for fix 241782.</p>
<p>Rock on Team DigiKam™!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19450"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19450" class="active">Great news!</a></h3>    <div class="submitted">Submitted by Claudio (not verified) on Thu, 2010-08-26 07:34.</div>
    <div class="content">
     <p>I also have to say that the pace of digikam development is amazing. For its task, this program is the best, ever. </p>
<p>I am also happy about 241782. I left it overnight writing ~12000 tags, to realise that just few were written. Now I am back to business (i.e. to captioning images :) ).</p>
<p>Best</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19480"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19480" class="active">Digikam 1.4.0 with Fedora F12/F13?</a></h3>    <div class="submitted">Submitted by Spip (not verified) on Fri, 2010-08-27 19:33.</div>
    <div class="content">
     <p>Does anyone have any pointers how it would be possible to have digikam 1.4.0 on Fedora F12 or F13?</p>
<p>I have tried to find backport rpm without success, and have tried also to recompile/regenerate the digikam.spec, but I encountered non-trivial errors (missing stuff inside kdegraphics-devel package).</p>
<p>So does anyone have any idea how to upgrade from 1.2.0 to 1.4.0 on Fedora?</p>
<p>P.S. I really like digikam, thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19482"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19482" class="active">I want to install digikam on</a></h3>    <div class="submitted">Submitted by Stephan (not verified) on Sat, 2010-08-28 11:06.</div>
    <div class="content">
     <p>I want to install digikam on Win7 with KDE on Windows, but in the installer I can't find digikam. Does it have another name there? I used stable 4.4.4. and tried all compilers.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19483"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19483" class="active">Regarding bug 225471, was it</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2010-08-29 23:14.</div>
    <div class="content">
     <p>Regarding bug 225471, was it fixed or simply ignored? Although Lightroom 3 isn't perfect, it is lightyears ahead of digiKam in terms of usability. I realize that now after having used Adobe's product recently for the first time after having used digiKam for three years.</p>
<p>Therefore I'll say it again: digiKam doesn't need any more new features, it needs to be easier to use, have a more predictable user interface, a more efficient user interface (i.e. require less mouse clicks to achieve something) and a cleaner user interface. For the sake of your users, please spend much more time on improving the user interface.</p>
<p>Thank you.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19487"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19487" class="active">Secifically?</a></h3>    <div class="submitted">Submitted by Logi (not verified) on Mon, 2010-08-30 20:32.</div>
    <div class="content">
     <p>You say the UI needs improvement, which may be absolutely correct. But I'll say the same thing I'd say if you were commenting on my own project: What *specifically* should be improved?</p>
<p>How about writing out, say, 5 specific suggestions and see if they are well received, and if so, write out more?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19492"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19492" class="active">Perhaps I'll have some time</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2010-09-02 04:04.</div>
    <div class="content">
     <p>Perhaps I'll have some time in the future to hack together a proposal in Qt Designer. I have just been burned before by having a project ignore my contributions, so I am more happy ranting than investing a lot of time ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19500"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19500" class="active">compiling/installing under Ubuntu Lucid</a></h3>    <div class="submitted">Submitted by Dahaniel (not verified) on Sat, 2010-09-04 13:44.</div>
    <div class="content">
     <p>Hi there,</p>
<p>did somebody successfully build 1.4 under Ubuntu Lucid? Found this ppa but there are many dependencies which are not fulfilled...<br>
http://launchpad.net/~nuovodna/+archive/nuovodna-stuff/+packages</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19503"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19503" class="active">Try adding</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2010-09-05 18:29.</div>
    <div class="content">
     <p>Try adding "ppa:nuovodna/nuovodna-stuff" to your system's Software Sources. I'm running Kubuntu Lucid with kde 4.5 and it worked for me. Good luck</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19509"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/535#comment-19509" class="active">No, this does not solve the</a></h3>    <div class="submitted">Submitted by <a href="http://www.yiannakos.gr" rel="nofollow">Bill Yiannakos</a> (not verified) on Tue, 2010-09-07 10:59.</div>
    <div class="content">
     <p>No, this does not solve the problem..... In Ubuntu 10.04 it gets lots of not installable dependencies...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
