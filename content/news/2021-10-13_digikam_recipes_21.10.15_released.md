---
date: "2021-10-13T00:00:00"
title: "digiKam Recipes 21.10.15 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

It has been a while since the last update of [digiKam Recipes](https://dmpop.gumroad.com/l/digikamrecipes). But that doesn't mean I neglected the book. In the past few months, I've been doing a complete language review and adding new material. The new revision of _digiKam Recipes_ features detailed information on how to move digiKam library and databases from one machine to another, how to access digiKam remotely from any machine, and how to import photos from an iOS device. The book now uses the Barlow font for better legibility along with a slightly improved layout.

As always, all _digiKam Recipes_ readers will receive the updated version of the book automatically and free of charge. The _digiKam Recipes_ book is available from [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) and [Gumroad](https://gumroad.com/l/digikamrecipes/).